﻿Imports System.Data.EntityClient

Public Class cDAL
#Region " DECLARACIONES "
    Private Shared _Instancia As cDAL = Nothing
#End Region
#Region " PROPIEDADES "
    Public Shared ReadOnly Property Instancia As cDAL
        Get
            If _Instancia Is Nothing Then _Instancia = New cDAL
            Return _Instancia
        End Get
    End Property

    Public Property CadenaConexion As String = String.Empty
    Public Property Provider As String = String.Empty
#End Region
#Region " METODOS PUBLICOS "
    Public Function getContext() As Entidades
        Dim DatosConexion As New EntityConnectionStringBuilder()
        DatosConexion.Metadata = "res://*/Modelo.csdl|res://*/Modelo.ssdl|res://*/Modelo.msl"
        DatosConexion.Provider = Provider
        DatosConexion.ProviderConnectionString = CadenaConexion
        Return New Entidades(DatosConexion.ConnectionString)
    End Function

    Public Function GetFactory() As Common.DbProviderFactory
        Try
            Return Common.DbProviderFactories.GetFactory(Provider)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
#End Region
End Class
