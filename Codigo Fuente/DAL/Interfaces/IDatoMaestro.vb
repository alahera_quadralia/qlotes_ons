﻿Public Interface IDatoMaestro
    Enum TamanhoColumna As Integer
        AllCells = 6
        AllCellsExceptHeader = 4
        ColumnHeader = 2
        DisplayedCells = 10
        DisplayedCellsExceptHeader = 8
        Fill = 16
        None = 1
        NotSet = 0
    End Enum

    Structure DatosColumna
        Public Resize As TamanhoColumna
        Public Propiedad As String
        Public Nombre As String
    End Structure

    ReadOnly Property Columnas As List(Of DatosColumna)
    ReadOnly Property NombreEntidad As String
    Function ObtenerDatos(ByVal Contexto As Entidades) As IQueryable(Of IDatoMaestro)
End Interface