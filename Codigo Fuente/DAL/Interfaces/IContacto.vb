﻿Public Interface IContacto
    Property Nombre As String
    Property Apellidos As String
    Property Cargo As String
    Property Telefono As String
    Property Extension As String
    Property Movil As String
    Property Fax As String
    Property Email As String
    Property Observaciones As String
    Property Principal As Nullable(Of Boolean)
    ReadOnly Property NombreCompleto As String
    ReadOnly Property TelefonoCompleto As String
End Interface