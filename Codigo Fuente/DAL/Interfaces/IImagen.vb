﻿Public Interface IImagen
    Property Imagen As System.Drawing.Image
    ReadOnly Property TieneImagen As Boolean
    ReadOnly Property NombreCampoTieneImagen As String
    ReadOnly Property Identificador As Integer
End Interface