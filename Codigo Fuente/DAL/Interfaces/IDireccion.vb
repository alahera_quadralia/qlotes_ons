﻿Public Interface IDireccion
    Enum TiposDireccion As Short
        General = 0
        Envio = 1
        Fiscal = 2
    End Enum

    Property Descripcion As String
    Property Direccion As String
    Property Poblacion As String
    Property Provincia As String
    Property CodigoPostal As String
    Property Pais As String
    Property Tipo As TiposDireccion
    Property Principal As Nullable(Of Boolean)
    ReadOnly Property DireccionCompleta As String
End Interface
