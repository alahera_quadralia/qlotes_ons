﻿Public Interface IAccionesEntidad
    Function AntesGuardar(Estado As EntityState, ByRef Contexto As DAL.Entidades, Entrada As Objects.ObjectStateEntry) As Boolean
    Function DespuesGuardar(Estado As EntityState, ByRef Contexto As DAL.Entidades, Entrada As Objects.ObjectStateEntry) As Boolean
End Interface
