﻿Imports System.Drawing
Imports System.IO

Namespace Utilidades.Imagenes
    Module Imagenes
        Public Function Imagen2Byte(ByVal Imagen As Image, Optional ByVal Formato As System.Drawing.Imaging.ImageFormat = Nothing) As Byte()
            ' Validaciones
            If Imagen Is Nothing Then Return Nothing
            If Formato Is Nothing Then Formato = System.Drawing.Imaging.ImageFormat.Png

            Dim ImageStream As MemoryStream = New MemoryStream()

            Try
                Imagen.Save(ImageStream, Formato)
                Return ImageStream.ToArray()
            Catch ex As Exception
                Return Nothing
            Finally
                If ImageStream IsNot Nothing Then
                    ImageStream.Close()
                    ImageStream.Dispose()
                End If
            End Try

        End Function

        Public Function Byte2Imagen(ByVal Binario As Byte()) As Image
            ' Validaciones
            If Binario Is Nothing OrElse Binario.Length = 0 Then Return Nothing

            Dim ImageStream As MemoryStream = Nothing

            Try
                ImageStream = New MemoryStream(Binario)
                Return Image.FromStream(ImageStream)
            Catch ex As System.Exception
                Return Nothing
            Finally
                If ImageStream IsNot Nothing Then
                    ImageStream.Close()
                    ImageStream.Dispose()
                End If
            End Try

        End Function
    End Module
End Namespace
