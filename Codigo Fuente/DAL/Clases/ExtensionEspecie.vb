﻿Partial Public Class Especie
    Implements IDatoMaestro
    Implements IImagen
    Implements IAccionesEntidad

#Region " FORMULARIO DE DATOS MAESTROS "
    Public ReadOnly Property Columnas As System.Collections.Generic.List(Of IDatoMaestro.DatosColumna) Implements IDatoMaestro.Columnas
        Get
            Dim Resultado As New List(Of IDatoMaestro.DatosColumna)
            Resultado.Add(New IDatoMaestro.DatosColumna With {.Propiedad = "denominacionCientifica", .Nombre = "Denominación Científica", .Resize = IDatoMaestro.TamanhoColumna.Fill})
            Resultado.Add(New IDatoMaestro.DatosColumna With {.Propiedad = "denominacionComercial", .Nombre = "Denominación Comercial", .Resize = IDatoMaestro.TamanhoColumna.Fill})
            Resultado.Add(New IDatoMaestro.DatosColumna With {.Propiedad = "alfa3", .Nombre = "Alfa 3", .Resize = IDatoMaestro.TamanhoColumna.AllCells})

            Return Resultado
        End Get
    End Property

    Public Function ObtenerDatos(ByVal Contexto As Entidades) As IQueryable(Of IDatoMaestro) Implements IDatoMaestro.ObtenerDatos
        If Contexto Is Nothing Then Return Nothing

        Return (From it As Especie In Contexto.Especies Select it)
    End Function

    Public ReadOnly Property NombreEntidad As String Implements IDatoMaestro.NombreEntidad
        Get
            Return "Especies"
        End Get
    End Property

    Public Overrides Function ToString() As String
        Return String.Format("{0} - {1}", alfa3, denominacionComercial)
    End Function

    Public ReadOnly Property EspecieNombreCodigo As String
        Get
            Return String.Format("{0} - {1}", alfa3, denominacionComercial)
        End Get
    End Property



    Public Property Imagen As Drawing.Image Implements IImagen.Imagen
        Get
            Return Utilidades.Imagenes.Byte2Imagen(Me.ImagenBinario)
        End Get
        Set(value As Drawing.Image)
            ImagenBinario = Utilidades.Imagenes.Imagen2Byte(value)
        End Set
    End Property

    Public ReadOnly Property TieneImagen As Boolean Implements IImagen.TieneImagen
        Get
            Return Me.ImagenBinario IsNot Nothing AndAlso Me.ImagenBinario.Length > 0
        End Get
    End Property

    Public ReadOnly Property NombreCampoTieneImagen As String Implements IImagen.NombreCampoTieneImagen
        Get
            Return "TieneImagen"
        End Get
    End Property

    Public ReadOnly Property Identificador As Integer Implements IImagen.Identificador
        Get
            Return Me.id
        End Get
    End Property
#End Region
#Region " SINCRONIZADO "
    Public Property Sincronizando As Boolean = False ' Indica si se está en mitad de una operación de sincronizado

    Public Function AntesGuardar(Estado As EntityState, ByRef Contexto As Entidades, Entrada As Objects.ObjectStateEntry) As Boolean Implements IAccionesEntidad.AntesGuardar
        Select Case Estado
            Case Data.EntityState.Added, Data.EntityState.Modified, Data.EntityState.Detached
                If Not Sincronizando Then Me.sincronizadoWeb = False
        End Select

        Return True
    End Function

    Public Function DespuesGuardar(Estado As EntityState, ByRef Contexto As Entidades, Entrada As Objects.ObjectStateEntry) As Boolean Implements IAccionesEntidad.DespuesGuardar
        Sincronizando = False

        Return True
    End Function
#End Region
End Class