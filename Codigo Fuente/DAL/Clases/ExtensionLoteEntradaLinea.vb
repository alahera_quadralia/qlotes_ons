﻿Partial Public Class LoteEntradalinea
    Public Property ImportacionCorrecta As Boolean = False

    Public Property nombreEspecie As String
        Get
            If Especie Is Nothing Then Return String.Empty
            Return Especie.ToString
        End Get
        Set(ByVal value As String)
        End Set
    End Property

    Public Property nombreCientificoEspecie As String
        Get
            If Especie Is Nothing Then Return String.Empty
            Return Especie.denominacionCientifica
        End Get
        Set(ByVal value As String)
        End Set
    End Property

    Public ReadOnly Property codigoAlbaranEntrada As String
        Get
            If Me.LoteEntrada Is Nothing Then Return String.Empty
            Return LoteEntrada.codigo
        End Get
    End Property

    Public ReadOnly Property puertoDesembarco As String
        Get
            If LoteEntrada Is Nothing OrElse LoteEntrada.Proveedor Is Nothing Then Return String.Empty
            Return LoteEntrada.Proveedor.puerto
        End Get
    End Property

    Public ReadOnly Property nombrePresentacion As String
        Get
            If Presentacion Is Nothing Then Return String.Empty
            Return Presentacion.descripcion
        End Get
    End Property

    Public ReadOnly Property nombreProduccion As String
        Get
            If Produccion Is Nothing Then Return String.Empty
            Return Produccion.descripcion
        End Get
    End Property

    Public ReadOnly Property nombreFAO As String
        Get
            If ZonaFao Is Nothing Then Return String.Empty
            Return ZonaFao.descripcion
        End Get
    End Property

    Public ReadOnly Property nombreZona As String
        Get
            If ZonaFao Is Nothing Then Return String.Empty
            Return ZonaFao.descripcion
        End Get
    End Property

    Public ReadOnly Property nombreSubZona As String
        Get
            If SubZona Is Nothing Then Return String.Empty
            Return SubZona.descripcion
        End Get
    End Property

    Public ReadOnly Property TotalPesoEtiquetas As Decimal
        Get
            If Etiquetas Is Nothing OrElse Etiquetas.Count = 0 Then Return 0
            Return Etiquetas.Where(Function(etq) Not etq.fechaBaja.HasValue).Sum(Function(etq) etq.cantidad)
        End Get
    End Property

    Public ReadOnly Property cantidadRestante As Decimal
        Get
            If Etiquetas Is Nothing OrElse Etiquetas.Count = 0 Then Return TotalPesoEtiquetas
            Return TotalPesoEtiquetas - Etiquetas.Sum(Function(x) x.cantidad)
        End Get
    End Property

    Public ReadOnly Property cantidadRestanteEtiquetas As Decimal
        Get
            If Etiquetas Is Nothing OrElse Etiquetas.Count = 0 Then Return cantidad
            Return cantidad - Etiquetas.Where(Function(etq) Not etq.fechaBaja.HasValue).Sum(Function(etq) etq.cantidad)
        End Get
    End Property

    Public ReadOnly Property nombreProveedor As String
        Get
            If LoteEntrada Is Nothing OrElse LoteEntrada.Proveedor Is Nothing Then Return String.Empty
            Return LoteEntrada.Proveedor.razonSocial
        End Get
    End Property

    Public ReadOnly Property codigoProveedor As String
        Get
            If LoteEntrada Is Nothing OrElse LoteEntrada.Proveedor Is Nothing Then Return String.Empty
            Return LoteEntrada.Proveedor.codigo
        End Get
    End Property

    Public ReadOnly Property NombreEmbarcacion As String
        Get
            If Barco Is Nothing Then Return String.Empty
            Return Barco.descripcion
        End Get
    End Property

    Public ReadOnly Property codigoCompleto As String
        Get
            If LoteEntrada Is Nothing Then Return numeroLote
            Return String.Format("{0}/{1}", LoteEntrada.codigo, numeroLote)
        End Get
    End Property

    ''' <summary>
    ''' Devuelve el id del Albarán y el número de lote formato "idAlbarán-lote" 
    ''' Es lote de salida en frmEtiquetas 
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property Numero_Lote As String
        Get
            If LoteEntrada Is Nothing Then Return String.Empty
            ''Se cambia a petición de Fernando para que no tener tantos lotes en la etiqueta
            ''Return String.Format("{0}-{1}", Me.LoteEntrada.id.ToString(), numeroLote.Trim())
            Return Me.LoteEntrada.id.ToString()
        End Get
    End Property

    Public ReadOnly Property LotesSalida As IEnumerable(Of LoteSalida)
        Get
            If Etiquetas Is Nothing OrElse Etiquetas.Count = 0 Then Return New List(Of LoteSalida)
            Return (From etq As Etiqueta In Etiquetas _
                    Where etq.Empaquetado IsNot Nothing AndAlso etq.Empaquetado.LoteSalida IsNot Nothing
                    Select etq.Empaquetado.LoteSalida).Distinct
        End Get
    End Property

    Public ReadOnly Property Descripcion
        Get
            Return String.Format("{2}: {1} [{0:dd/MM/yyyy}]", fechaDesembarco, nombreEspecie, nombreProveedor)
        End Get
    End Property
End Class