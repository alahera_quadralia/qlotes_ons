﻿Partial Public Class Etiqueta
    Implements IAccionesEntidad

    Public Delegate Sub ImpresionEtiqueta(ByVal Etiqueta As Etiqueta)
    Public Property FuncionImpresion As ImpresionEtiqueta = Nothing

    Public Property Sincronizando As Boolean = False ' Indica si se está en mitad de una operación de sincronizado

    Public Function AntesGuardar(Estado As EntityState, ByRef Contexto As Entidades, Entrada As Objects.ObjectStateEntry) As Boolean Implements IAccionesEntidad.AntesGuardar
        Select Case Estado
            Case Data.EntityState.Added, Data.EntityState.Modified, Data.EntityState.Detached
                If Not Sincronizando Then Me.sincronizadoWeb = False
        End Select

        Return True
    End Function

    Public Function DespuesGuardar(Estado As EntityState, ByRef Contexto As Entidades, Entrada As Objects.ObjectStateEntry) As Boolean Implements IAccionesEntidad.DespuesGuardar
        Sincronizando = False
        If FuncionImpresion Is Nothing Then Return False

        FuncionImpresion.Invoke(Me)
        Return True
    End Function

#Region " INFORMACION "
    Public ReadOnly Property Codigo As String
        Get
            Return id.ToString.PadLeft(6, "0")
        End Get
    End Property

    Public ReadOnly Property CodigoAlbaranEntrada As String
        Get
            If LoteEntradaLinea Is Nothing Then Return String.Empty
            Return LoteEntradaLinea.codigoAlbaranEntrada
        End Get
    End Property

    Public ReadOnly Property CodigoLineaAlbaranEntrada As String
        Get
            If LoteEntradaLinea Is Nothing Then Return String.Empty
            Return LoteEntradaLinea.numeroLote
        End Get
    End Property

    Public ReadOnly Property CodigoCompletoLineaAlbaranEntrada As String
        Get
            If LoteEntradaLinea Is Nothing Then Return String.Empty
            Return LoteEntradaLinea.codigoCompleto
        End Get
    End Property

    Public ReadOnly Property RegistroOCifExpedidor As String
        Get
            If Expedidor Is Nothing Then Return String.Empty

            If Not String.IsNullOrEmpty(Expedidor.registro) Then
                Return Expedidor.registro
            Else
                Return Expedidor.cif
            End If
        End Get
    End Property

    Public ReadOnly Property NombreEspecie As String
        Get
            If LoteEntradaLinea Is Nothing Then Return String.Empty
            Return LoteEntradaLinea.nombreEspecie
        End Get
    End Property

    Public ReadOnly Property NombreCientificoEspecie As String
        Get
            If LoteEntradaLinea Is Nothing Then Return String.Empty
            If LoteEntradaLinea.Especie Is Nothing Then Return String.Empty
            Return LoteEntradaLinea.Especie.denominacionCientifica
        End Get
    End Property

    Public ReadOnly Property IdentificadorEmpaquetado As Long
        Get
            If Empaquetado Is Nothing Then Return 9
            Return Empaquetado.id
        End Get
    End Property

    Public ReadOnly Property IdBarco As Integer
        Get
            If LoteEntradaLinea Is Nothing OrElse LoteEntradaLinea.Barco Is Nothing Then Return -1
            Return LoteEntradaLinea.Barco.id
        End Get
    End Property

    Public ReadOnly Property IdEspecie As Integer
        Get
            If LoteEntradaLinea Is Nothing OrElse LoteEntradaLinea.Especie Is Nothing Then Return -1
            Return LoteEntradaLinea.Especie.id
        End Get
    End Property

    Public ReadOnly Property NombreComercialEspecie As String
        Get
            If LoteEntradaLinea Is Nothing Then Return String.Empty
            If LoteEntradaLinea.Especie Is Nothing Then Return String.Empty
            Return LoteEntradaLinea.Especie.denominacionComercial
        End Get
    End Property

    Public ReadOnly Property CodigoEspecie As String
        Get
            If LoteEntradaLinea Is Nothing Then Return String.Empty
            If LoteEntradaLinea.Especie Is Nothing Then Return String.Empty
            Return LoteEntradaLinea.Especie.alfa3
        End Get
    End Property

    Public ReadOnly Property NombreProveedor As String
        Get
            If LoteEntradaLinea Is Nothing Then Return String.Empty
            Return LoteEntradaLinea.nombreProveedor
        End Get
    End Property
    Public ReadOnly Property CodigoProveedor As String
        Get
            If LoteEntradaLinea Is Nothing Then Return String.Empty
            Return LoteEntradaLinea.codigoProveedor
        End Get
    End Property

    Public ReadOnly Property NombreExpedidor As String
        Get
            If Expedidor Is Nothing Then Return String.Empty
            Return Expedidor.razonSocial
        End Get
    End Property

    Public ReadOnly Property NombreDireccionExpedidor As String
        Get
            If Expedidor Is Nothing Then Return String.Empty
            Return Expedidor.razonSocial & vbCrLf & Expedidor.direccion
        End Get
    End Property

    Public ReadOnly Property NombrePresentacion As String
        Get
            If LoteEntradaLinea Is Nothing Then Return String.Empty
            Return LoteEntradaLinea.nombrePresentacion
        End Get
    End Property

    Public ReadOnly Property NombreProduccion As String
        Get
            If LoteEntradaLinea Is Nothing Then Return String.Empty
            Return LoteEntradaLinea.nombreProduccion
        End Get
    End Property

    Public ReadOnly Property NombreZona As String
        Get
            If LoteEntradaLinea Is Nothing Then Return String.Empty
            Return LoteEntradaLinea.nombreZona
        End Get
    End Property

    Public ReadOnly Property NombreSubzona As String
        Get
            If LoteEntradaLinea Is Nothing Then Return String.Empty
            Return LoteEntradaLinea.nombreSubZona
        End Get
    End Property

    Public ReadOnly Property IdZonaTrazamare As String
        Get
            If LoteEntradaLinea Is Nothing Then Return String.Empty
            Return LoteEntradaLinea.ZonaFao.idTrazamare
        End Get
    End Property

    Public ReadOnly Property IdSubzonaTrazamare As String
        Get
            If LoteEntradaLinea Is Nothing Then Return String.Empty
            Return LoteEntradaLinea.SubZona.idTrazamare
        End Get
    End Property

    Public ReadOnly Property logotipoExpedidor As Byte()
        Get
            If Expedidor Is Nothing Then Return Nothing
            Return Expedidor.logotipoBinario
        End Get
    End Property

    Public ReadOnly Property Descripcion As String
        Get
            Return String.Format("[{0}] - {2} {1}", Codigo, NombreEspecie, cantidad)
        End Get
    End Property

    Public ReadOnly Property CodigoEmpaquetado As String
        Get
            If Empaquetado Is Nothing Then Return String.Empty
            Return Empaquetado.Codigo
        End Get
    End Property

    Public ReadOnly Property LoteSalida As String
        Get
            If Me.LoteEntradaLinea Is Nothing Then Return String.Empty
            Return String.Format("{0}{1}", fecha.ToString("yyyyMMdd"), Me.CodigoProveedor)
        End Get
    End Property

    ''' <summary>
    ''' Devuelve el id del Albarán y el número de lote formato "idAlbarán-lote"
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property Numero_Lote As String
        Get
            If Me.LoteEntradaLinea Is Nothing Then Return String.Empty
            Return Me.LoteEntradaLinea.Numero_Lote
        End Get
    End Property


    ''' <summary>
    ''' Devuelve el tamaño según el peso
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property Tamaño As String
        Get
            Select Case Me.cantidad
                Case Is < 1.5
                    Return "P"
                Case Is < 2
                    Return "M"
                Case Is < 3
                    Return "G"
                Case Else
                    Return "E"
            End Select
        End Get
    End Property

#End Region
#Region " METODOS PUBLICOS "
    Public Function ObtenerURI(Configuracion As Configuracion) As String
        Dim Resultado As String = Configuracion.TrazamareFTPServidor
        If Configuracion IsNot Nothing Then Resultado = String.Format("{0}/trazabilidad/{1}{2}", Resultado, Configuracion.CodigoTrazamare, Codigo)

        Return Resultado
    End Function

    Public Function ObtenerCodigo(Configuracion As Configuracion) As String
        Dim Resultado As String = String.Empty
        If Configuracion IsNot Nothing Then Resultado = String.Format("{0}{1}", Configuracion.CodigoTrazamare, Codigo)

        Return Resultado
    End Function
#End Region
End Class