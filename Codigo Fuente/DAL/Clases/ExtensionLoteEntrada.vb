﻿Partial Public Class LoteEntrada
    Public ReadOnly Property nombreProveedor As String
        Get
            If Proveedor Is Nothing Then Return String.Empty
            Return Proveedor.nombreComercial
        End Get
    End Property

    Public ReadOnly Property razonSocialProveedor As String
        Get
            If Proveedor Is Nothing Then Return String.Empty
            Return Proveedor.razonSocial
        End Get
    End Property

    Public ReadOnly Property LotesSalida As IEnumerable(Of LoteSalida)
        Get
            If Lineas Is Nothing OrElse Lineas.Count = 0 Then Return Nothing

            ' Devuelvo los albaranes de salida vinculados
            Return (From lin In Lineas _
                    From etq As Etiqueta In lin.Etiquetas.DefaultIfEmpty
                    Where etq IsNot Nothing AndAlso etq.Empaquetado IsNot Nothing AndAlso etq.Empaquetado.LoteSalida IsNot Nothing
                    Select etq.Empaquetado.LoteSalida).Distinct
        End Get
    End Property

    Public ReadOnly Property TotalPeso As Decimal
        Get
            If Lineas Is Nothing OrElse Lineas.Count = 0 Then Return 0
            Return Lineas.Sum(Function(lin) lin.cantidad)
        End Get
    End Property

    Public ReadOnly Property TotalPesoRestante As Decimal
        Get
            If Lineas Is Nothing OrElse Lineas.Count = 0 Then Return 0
            Return Lineas.Sum(Function(lin) lin.cantidadRestanteEtiquetas)
        End Get
    End Property

    Public ReadOnly Property Descripcion As String
        Get
            Return String.Format("[{0:d}] {1}", fecha, nombreProveedor)
        End Get
    End Property

    Public Function ObtenerLineaParaEtiqueta(Peso As Double) As LoteEntradalinea
        If Lineas Is Nothing OrElse Lineas.Count = 0 Then Return Nothing

        Return (From lin As LoteEntradalinea In Lineas Where lin.cantidadRestanteEtiquetas >= Peso Order By lin.cantidadRestanteEtiquetas Ascending Select lin).FirstOrDefault
    End Function
End Class