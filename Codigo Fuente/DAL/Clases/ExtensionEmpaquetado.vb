﻿Partial Public Class Empaquetado
    Implements IAccionesEntidad

    Public ReadOnly Property Codigo As String
        Get
            Return id.ToString.PadLeft(8, "0")
        End Get
    End Property

    ''' <summary>
    ''' Obtiene los albaranes de entrada presentes en las etiquetas
    ''' </summary>
    Public ReadOnly Property AlbaranesEntrada As List(Of LoteEntrada)
        Get
            Dim Registros = (From etq As Etiqueta In Etiquetas Where etq.LoteEntradaLinea IsNot Nothing AndAlso etq.LoteEntradaLinea.LoteEntrada IsNot Nothing AndAlso Not etq.LoteEntradaLinea.LoteEntrada.fechaBaja.HasValue Select etq.LoteEntradaLinea.LoteEntrada).Distinct
            Return Registros.OrderBy(Function(ent) ent.fecha).ToList
        End Get
    End Property

    Public ReadOnly Property NumeroEtiquetas As Integer
        Get
            If Etiquetas Is Nothing Then Return 0
            'Return Etiquetas.Where(Function(p) p.fechaBaja Is Nothing).Count
            Return Etiquetas.Count
        End Get
    End Property

    Public ReadOnly Property Peso As Decimal
        Get
            If Etiquetas Is Nothing Then Return 0
            'Return Math.Round(Etiquetas.Where(Function(p) p.fechaBaja Is Nothing).Sum(Function(p) p.cantidad), 3)
            Return Math.Round(Etiquetas.Sum(Function(p) p.cantidad), 3)
        End Get
    End Property

    Public ReadOnly Property codigoLoteSalida As String
        Get
            If LoteSalida Is Nothing Then Return String.Empty
            'Return Etiquetas.Where(Function(p) p.fechaBaja Is Nothing).Count
            Return LoteSalida.codigo
        End Get
    End Property

#Region " METODOS PUBLICOS "
    Public Function ObtenerURI(Configuracion As Configuracion) As String
        Dim Resultado As String = "http://lotesbd.trazamare.com/"
        If Configuracion IsNot Nothing Then Resultado = String.Format("{0}?id={1}{2}", Resultado, Configuracion.CodigoTrazamare, Codigo)

        Return Resultado
    End Function

    Public Function ObtenerCodigo(Configuracion As Configuracion) As String
        Dim Resultado As String = String.Empty
        If Configuracion IsNot Nothing Then Resultado = String.Format("{0}{1}", Configuracion.CodigoTrazamare, Codigo)

        Return Resultado
    End Function
#End Region
#Region " SINCRONIZADO "
    Public Property Sincronizando As Boolean = False ' Indica si se está en mitad de una operación de sincronizado

    Public Function AntesGuardar(Estado As EntityState, ByRef Contexto As Entidades, Entrada As Objects.ObjectStateEntry) As Boolean Implements IAccionesEntidad.AntesGuardar
        Select Case Estado
            Case Data.EntityState.Added, Data.EntityState.Modified, Data.EntityState.Detached
                If Not Sincronizando Then Me.sincronizadoWeb = False
        End Select

        Return True
    End Function

    Public Function DespuesGuardar(Estado As EntityState, ByRef Contexto As Entidades, Entrada As Objects.ObjectStateEntry) As Boolean Implements IAccionesEntidad.DespuesGuardar
        Sincronizando = False

        Return True
    End Function
#End Region
End Class