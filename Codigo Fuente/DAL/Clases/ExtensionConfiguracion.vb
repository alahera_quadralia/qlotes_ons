﻿Partial Public Class Configuracion
    Implements IAccionesEntidad

#Region " EVENTOS "
    Public Event ConfiguracionCambiada(ByRef Configuracion As Configuracion)
#End Region

    Public Function AntesGuardar(Estado As EntityState, ByRef Contexto As Entidades, Entrada As Objects.ObjectStateEntry) As Boolean Implements IAccionesEntidad.AntesGuardar
        Return True
    End Function

    Public Function DespuesGuardar(Estado As EntityState, ByRef Contexto As Entidades, Entrada As Objects.ObjectStateEntry) As Boolean Implements IAccionesEntidad.DespuesGuardar
        RaiseEvent ConfiguracionCambiada(Me)
        Return True
    End Function
End Class