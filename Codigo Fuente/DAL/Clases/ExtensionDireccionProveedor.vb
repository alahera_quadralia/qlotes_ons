﻿Partial Public Class DireccionProveedor
    Implements IDireccion

    Public Property CodigoPostal1 As String Implements IDireccion.CodigoPostal
        Get
            Return codigoPostal
        End Get
        Set(ByVal value As String)
            codigoPostal = value
        End Set
    End Property

    Public Property Descripcion1 As String Implements IDireccion.Descripcion
        Get
            Return descripcion
        End Get
        Set(ByVal value As String)
            descripcion = value
        End Set
    End Property

    Public Property Direccion1 As String Implements IDireccion.Direccion
        Get
            Return direccion
        End Get
        Set(ByVal value As String)
            direccion = value
        End Set
    End Property

    Public ReadOnly Property DireccionCompleta As String Implements IDireccion.DireccionCompleta
        Get
            Dim Resultado As String = Me.Proveedor.razonSocial & vbCrLf
            If Not String.IsNullOrEmpty(direccion) Then Resultado &= direccion
            If Not String.IsNullOrEmpty(codigoPostal.Trim()) Then Resultado &= vbCrLf
            If Not String.IsNullOrEmpty(codigoPostal.Trim()) Then Resultado &= "C.P: " & codigoPostal & " "

            If Not String.IsNullOrEmpty(poblacion.Trim()) OrElse Not String.IsNullOrEmpty(provincia.Trim()) Then Resultado &= vbCrLf
            If Not String.IsNullOrEmpty(poblacion.Trim()) Then Resultado &= poblacion.Trim().ToUpper()
            If Not String.IsNullOrEmpty(provincia.Trim()) AndAlso Not String.IsNullOrEmpty(poblacion.Trim()) Then
                Resultado &= " (" & provincia.Trim().ToUpper() & ")"
            ElseIf Not String.IsNullOrEmpty(provincia.Trim()) Then
                Resultado &= provincia.Trim().ToUpper()
            End If

            If pais IsNot Nothing Then Resultado &= vbCrLf & pais.Trim().ToUpper()

            Return Resultado
        End Get
    End Property

    Public ReadOnly Property DireccionCompletaLineaUnica As String
        Get

            Dim Resultado As String = String.Empty
            If Not String.IsNullOrEmpty(direccion) Then Resultado &= direccion
            If Not String.IsNullOrEmpty(codigoPostal) AndAlso Not String.IsNullOrEmpty(codigoPostal.Trim()) Then Resultado &= ", "
            If Not String.IsNullOrEmpty(codigoPostal) AndAlso Not String.IsNullOrEmpty(codigoPostal.Trim()) Then Resultado &= "C.P: " & codigoPostal

            If Not String.IsNullOrEmpty(poblacion) AndAlso Not String.IsNullOrEmpty(poblacion.Trim()) OrElse Not String.IsNullOrEmpty(provincia.Trim()) Then Resultado &= " "
            If Not String.IsNullOrEmpty(poblacion) AndAlso Not String.IsNullOrEmpty(poblacion.Trim()) Then Resultado &= poblacion.Trim().ToUpper()
            If Not String.IsNullOrEmpty(provincia) AndAlso Not String.IsNullOrEmpty(provincia.Trim()) AndAlso Not String.IsNullOrEmpty(poblacion.Trim()) Then
                Resultado &= " (" & provincia.Trim().ToUpper() & ")"
            ElseIf Not String.IsNullOrEmpty(provincia) AndAlso Not String.IsNullOrEmpty(provincia.Trim()) Then
                Resultado &= provincia.Trim().ToUpper()
            End If

            If Not pais Is Nothing Then Resultado &= " - " & pais.Trim().ToUpper()

            Return Resultado
        End Get
    End Property

    Public Property Pais1 As String Implements IDireccion.Pais
        Get
            Return pais
        End Get
        Set(ByVal value As String)
            pais = value
        End Set
    End Property

    Public Property Poblacion1 As String Implements IDireccion.Poblacion
        Get
            Return poblacion
        End Get
        Set(ByVal value As String)
            poblacion = value
        End Set
    End Property

    Public Property Provincia1 As String Implements IDireccion.Provincia
        Get
            Return provincia
        End Get
        Set(ByVal value As String)
            provincia = value
        End Set
    End Property

    Public Property Principal1 As Boolean? Implements IDireccion.Principal
        Get
            Return principal
        End Get
        Set(ByVal value As Boolean?)
            principal = value
        End Set
    End Property

    Public Property Tipo As IDireccion.TiposDireccion Implements IDireccion.Tipo
        Get
            Return IDireccion.TiposDireccion.General
        End Get
        Set(ByVal value As IDireccion.TiposDireccion)
        End Set
    End Property
End Class