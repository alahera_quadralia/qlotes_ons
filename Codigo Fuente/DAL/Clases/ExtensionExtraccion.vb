﻿Partial Public Class Extraccion
    Implements IDatoMaestro

    Public ReadOnly Property Columnas As System.Collections.Generic.List(Of IDatoMaestro.DatosColumna) Implements IDatoMaestro.Columnas
        Get
            Return New List(Of IDatoMaestro.DatosColumna) From {New IDatoMaestro.DatosColumna With {.Propiedad = "descripcion", .Nombre = "Descripción", .Resize = IDatoMaestro.TamanhoColumna.Fill}}
        End Get
    End Property

    Public Function ObtenerDatos(ByVal Contexto As Entidades) As IQueryable(Of IDatoMaestro) Implements IDatoMaestro.ObtenerDatos
        If Contexto Is Nothing Then Return Nothing

        Return (From it As Extraccion In Contexto.MetodosExtraccion Select it)
    End Function

    Public ReadOnly Property NombreEntidad As String Implements IDatoMaestro.NombreEntidad
        Get
            Return "Métodos de Extracción"
        End Get
    End Property

    Public ReadOnly Property Self As Extraccion
        Get
            Return Me
        End Get
    End Property
End Class