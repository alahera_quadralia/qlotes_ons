﻿Partial Public Class Cliente
    Public ReadOnly Property Telefonos As String
        Get
            If String.IsNullOrEmpty(telefono1) AndAlso String.IsNullOrEmpty(telefono2) Then
                Return String.Empty
            ElseIf Not String.IsNullOrEmpty(telefono1) AndAlso Not String.IsNullOrEmpty(telefono2) Then
                Return String.Format("{0} / {1}", telefono1, telefono2)
            Else
                Return telefono1 & telefono2
            End If
        End Get
    End Property

    Public ReadOnly Property DireccionPredeterminada As String
        Get
            If Direcciones Is Nothing OrElse Direcciones.Count = 0 Then Return String.Empty

            ' Busco inicialmente la predeterminada
            Dim Dire As DireccionCliente = (From dir As DireccionCliente In Direcciones Where Not dir.fechaBaja.HasValue AndAlso dir.principal.HasValue AndAlso dir.principal.Value Select dir).FirstOrDefault
            If Dire IsNot Nothing Then Return Dire.DireccionCompletaLineaUnica

            ' Tomo la primera
            Dire = (From dir As DireccionCliente In Direcciones Where Not dir.fechaBaja.HasValue Select dir).FirstOrDefault
            If Dire IsNot Nothing Then Return Dire.DireccionCompletaLineaUnica

            Return String.Empty
        End Get
    End Property
End Class