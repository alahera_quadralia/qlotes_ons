﻿Partial Public Class ContactoCliente
    Implements IContacto

    Private Property Apellidos1 As String Implements IContacto.Apellidos
        Get
            Return apellidos
        End Get
        Set(ByVal value As String)
            apellidos = value
        End Set
    End Property

    Private Property Cargo1 As String Implements IContacto.Cargo
        Get
            Return cargo
        End Get
        Set(ByVal value As String)
            cargo = value
        End Set
    End Property

    Private Property Email1 As String Implements IContacto.Email
        Get
            Return email
        End Get
        Set(ByVal value As String)
            email = value
        End Set
    End Property

    Private Property Extension1 As String Implements IContacto.Extension
        Get
            Return extension
        End Get
        Set(ByVal value As String)
            extension = value
        End Set
    End Property

    Private Property Fax1 As String Implements IContacto.Fax
        Get
            Return fax
        End Get
        Set(ByVal value As String)
            fax = value
        End Set
    End Property

    Private Property Movil1 As String Implements IContacto.Movil
        Get
            Return movil
        End Get
        Set(ByVal value As String)
            movil = value
        End Set
    End Property

    Private Property Nombre1 As String Implements IContacto.Nombre
        Get
            Return nombre
        End Get
        Set(ByVal value As String)
            nombre = value
        End Set
    End Property

    Private Property Observaciones1 As String Implements IContacto.Observaciones
        Get
            Return observaciones
        End Get
        Set(ByVal value As String)
            observaciones = value
        End Set
    End Property

    Private Property Principal1 As Nullable(Of Boolean) Implements IContacto.Principal
        Get
            Return esPrincipal
        End Get
        Set(ByVal value As Nullable(Of Boolean))
            esPrincipal = value
        End Set
    End Property

    Private Property Telefono1 As String Implements IContacto.Telefono
        Get
            Return telefono
        End Get
        Set(ByVal value As String)
            telefono = value
        End Set
    End Property

    Public ReadOnly Property NombreCompleto As String
        Get
            If String.IsNullOrEmpty(nombre) OrElse String.IsNullOrEmpty(apellidos) Then
                Return String.Format("{0}{1}", apellidos, nombre)
            Else
                Return String.Format("{0}, {1}", apellidos, nombre)
            End If
        End Get
    End Property

    Public ReadOnly Property NombreCompleto1 As String Implements IContacto.NombreCompleto
        Get
            Return NombreCompleto
        End Get
    End Property

    Public ReadOnly Property TelefonoCompleto() As String Implements IContacto.TelefonoCompleto
        Get
            If Not String.IsNullOrEmpty(extension) Then
                Return String.Format("{0} Ext: {1}", _telefono, _extension)
            Else
                Return _telefono
            End If
        End Get
    End Property
End Class