﻿Imports System.Data.Objects

Partial Public Class Entidades
    ''' <summary>
    ''' Se generan acciones una vez finalizado el proceso de guardado
    ''' </summary>
    Public Overrides Function SaveChanges(options As SaveOptions) As Integer
        Dim Resultado As Integer = 0
        Dim DiccionarioEntradas As New Dictionary(Of EntityState, IEnumerable(Of ObjectStateEntry))

        Try
            DiccionarioEntradas.Add(EntityState.Added, ObjectStateManager.GetObjectStateEntries(EntityState.Added))
            DiccionarioEntradas.Add(EntityState.Modified, ObjectStateManager.GetObjectStateEntries(EntityState.Modified))
            DiccionarioEntradas.Add(EntityState.Deleted, ObjectStateManager.GetObjectStateEntries(EntityState.Deleted))

            ' Miro si tengo que invocar a algún metodo de pre procesado
            For Each Par In DiccionarioEntradas
                For Each Entrada As ObjectStateEntry In Par.Value

                    ' Miro si es del tipo que me interesa
                    If Entrada Is Nothing Then Continue For

                    If Entrada.IsRelationship Then ' Relaciones
                    ElseIf Entrada.Entity IsNot Nothing AndAlso TypeOf (Entrada.Entity) Is IAccionesEntidad Then ' Entidades
                        DirectCast(Entrada.Entity, IAccionesEntidad).AntesGuardar(Par.Key, Me, Entrada)
                    End If
                Next
            Next
        Catch ex As Exception
        End Try

        ' Guardamos los datos
        Resultado = MyBase.SaveChanges(options)

        Try
            ' Miro si tengo que invocar a algún metodo de post procesado
            For Each Par In DiccionarioEntradas
                For Each Entrada As ObjectStateEntry In Par.Value

                    ' Miro si es del tipo que me interesa
                    If Entrada Is Nothing Then Continue For

                    If Entrada.IsRelationship Then ' Relaciones
                    ElseIf Entrada.Entity IsNot Nothing AndAlso TypeOf (Entrada.Entity) Is IAccionesEntidad Then ' Entidades
                        DirectCast(Entrada.Entity, IAccionesEntidad).DespuesGuardar(Par.Key, Me, Entrada)
                    End If
                Next
            Next
        Catch e As Exception
        End Try

        Return Resultado
    End Function

    ''' <summary>
    ''' Crea un objeto del tipo deseado con los valores dados
    ''' </summary>
    Private Function CrearConValores(Of T As New)(Valores As Common.DbDataRecord) As T
        Try
            Dim Resultado As New T()

            For i As Integer = 0 To Valores.FieldCount - 1
                If Valores.IsDBNull(i) Then
                    CallByName(Resultado, Valores.GetName(i), CallType.Set, New Object() {Nothing})
                Else
                    CallByName(Resultado, Valores.GetName(i), CallType.Set, Valores.GetValue(i))
                End If
            Next

            Return Resultado
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Crea un objeto con los valores originales
    ''' </summary>
    Friend Function CrearConValoresOriginales(Of T As New)(Entrada As ObjectStateEntry) As T
        If Entrada Is Nothing Then Return Nothing
        Return CrearConValores(Of T)(Entrada.OriginalValues)
    End Function

    ''' <summary>
    ''' Crea un objeto con los valores originales
    ''' </summary>
    Friend Function CrearConValoresActuales(Of T As New)(Entrada As ObjectStateEntry) As T
        If Entrada Is Nothing Then Return Nothing
        Return CrearConValores(Of T)(Entrada.CurrentValues)
    End Function
End Class