﻿Partial Public Class LoteSalida
    Public ReadOnly Property nombreCliente As String
        Get
            If Cliente Is Nothing Then Return String.Empty
            Return Cliente.nombreComercial
        End Get
    End Property

    Public ReadOnly Property razonSocialCliente As String
        Get
            If Cliente Is Nothing Then Return String.Empty
            Return Cliente.razonSocial
        End Get
    End Property

    Public ReadOnly Property DireccionCliente As String
        Get
            If Direccion Is Nothing Then Return String.Empty
            Return Direccion.DireccionCompleta
        End Get
    End Property

    Public ReadOnly Property nombreExpedidor As String
        Get
            If Expedidor Is Nothing Then Return String.Empty
            Return Expedidor.razonSocial
        End Get
    End Property

    Public ReadOnly Property textoCaducidad As String
        Get
            If Expedidor Is Nothing Then Return String.Empty
            Return Expedidor.caducidad
        End Get
    End Property

    Public ReadOnly Property nombreExpedidorYDireccion As String
        Get
            If Expedidor Is Nothing Then Return String.Empty
            Return String.Format("{1}{0}Tlf: {2}{0}Dir: {3}", Environment.NewLine, Expedidor.razonSocial, Expedidor.telefono1, Expedidor.direccion)
        End Get
    End Property

    Public ReadOnly Property LotesEntrada As IEnumerable(Of LoteEntrada)
        Get
            If Empaquetados Is Nothing OrElse Empaquetados.Count = 0 Then Return Nothing

            ' Devuelvo los albaranes de entrada vinculados
            Return (From emp In Empaquetados _
                    From etq As Etiqueta In emp.Etiquetas.DefaultIfEmpty _
                    Where etq IsNot Nothing AndAlso etq.LoteEntradaLinea IsNot Nothing AndAlso etq.LoteEntradaLinea.LoteEntrada IsNot Nothing
                    Select etq.LoteEntradaLinea.LoteEntrada).Distinct
        End Get
    End Property

    Public ReadOnly Property Etiquetas As IEnumerable(Of Etiqueta)
        Get
            If Empaquetados Is Nothing OrElse Empaquetados.Count = 0 Then Return Nothing

            ' Devuelvo los albaranes de entrada vinculados
            Return (From emp In Empaquetados _
                    From etq As Etiqueta In emp.Etiquetas.DefaultIfEmpty _
                    Where etq IsNot Nothing
                    Select etq).Distinct
        End Get
    End Property

    Public ReadOnly Property logotipoExpedidor As Byte()
        Get
            If Expedidor Is Nothing Then Return Nothing
            Return Expedidor.logotipoBinario
        End Get
    End Property
End Class