










































-- -----------------------------------------------------------
-- Entity Designer DDL Script for MySQL Server 4.1 and higher
-- -----------------------------------------------------------
-- Date Created: 01/03/2020 09:22:02

-- Generated from EDMX file: I:\desarrollo\escritorio\qLotesLira\Codigo Fuente\DAL\Modelo.edmx
-- Target version: 2.0.0.0

-- --------------------------------------------------



-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- NOTE: if the constraint does not exist, an ignorable error will be reported.
-- --------------------------------------------------


--    ALTER TABLE `DireccionesProveedores` DROP CONSTRAINT `FK_ProveedorDireccionProveedor`;

--    ALTER TABLE `ContactosProveedor` DROP CONSTRAINT `FK_ContactoProveedorProveedor`;

--    ALTER TABLE `LotesEntrada` DROP CONSTRAINT `FK_ProveedorLoteEntrada`;

--    ALTER TABLE `LotesEntradaLineas` DROP CONSTRAINT `FK_EspecieLoteEntradaLinea`;

--    ALTER TABLE `LotesEntradaLineas` DROP CONSTRAINT `FK_LoteEntradaLoteEntradaLinea`;

--    ALTER TABLE `LotesEntrada` DROP CONSTRAINT `FK_UsuarioLoteEntrada`;

--    ALTER TABLE `LotesEntradaLineas` DROP CONSTRAINT `FK_ZonaFaoLoteEntradaLinea`;

--    ALTER TABLE `LotesEntradaLineas` DROP CONSTRAINT `FK_PresentacionLoteEntradaLinea`;

--    ALTER TABLE `LotesEntradaLineas` DROP CONSTRAINT `FK_MetodoProduccionLoteEntradaLinea`;

--    ALTER TABLE `LotesSalida` DROP CONSTRAINT `FK_UsuarioLoteSalida`;

--    ALTER TABLE `LotesSalida` DROP CONSTRAINT `FK_ClienteLoteSalida`;

--    ALTER TABLE `LotesSalida` DROP CONSTRAINT `FK_ExpedidorLoteSalida`;

--    ALTER TABLE `ContactosCliente` DROP CONSTRAINT `FK_ClienteContactoCliente`;

--    ALTER TABLE `DireccionesClientes` DROP CONSTRAINT `FK_ClienteDireccionCliente`;

--    ALTER TABLE `LotesSalida` DROP CONSTRAINT `FK_LoteSalidaDireccionCliente`;

--    ALTER TABLE `LotesEntradaLineas` DROP CONSTRAINT `FK_SubZonaLoteEntradaLinea`;

--    ALTER TABLE `Etiquetas` DROP CONSTRAINT `FK_LoteEntradaLineaEtiqueta`;

--    ALTER TABLE `Etiquetas` DROP CONSTRAINT `FK_ExpedidorEtiqueta`;

--    ALTER TABLE `LotesEntradaLineas` DROP CONSTRAINT `FK_BarcoLoteEntradaLinea`;

--    ALTER TABLE `Empaquetados` DROP CONSTRAINT `FK_UsuarioEmpaquetado`;

--    ALTER TABLE `Etiquetas` DROP CONSTRAINT `FK_EmpaquetadoEtiqueta`;

--    ALTER TABLE `Empaquetados` DROP CONSTRAINT `FK_LoteSalidaEmpaquetado`;


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------
SET foreign_key_checks = 0;

    DROP TABLE IF EXISTS `Usuarios`;

    DROP TABLE IF EXISTS `Proveedores`;

    DROP TABLE IF EXISTS `DireccionesProveedores`;

    DROP TABLE IF EXISTS `ContactosProveedor`;

    DROP TABLE IF EXISTS `provincias`;

    DROP TABLE IF EXISTS `municipios`;

    DROP TABLE IF EXISTS `Expedidores`;

    DROP TABLE IF EXISTS `ZonasFao`;

    DROP TABLE IF EXISTS `Especies`;

    DROP TABLE IF EXISTS `MetodosProduccion`;

    DROP TABLE IF EXISTS `FormasPresentacion`;

    DROP TABLE IF EXISTS `MetodosExtraccion`;

    DROP TABLE IF EXISTS `LotesEntrada`;

    DROP TABLE IF EXISTS `LotesEntradaLineas`;

    DROP TABLE IF EXISTS `LotesSalida`;

    DROP TABLE IF EXISTS `Clientes`;

    DROP TABLE IF EXISTS `ContactosCliente`;

    DROP TABLE IF EXISTS `DireccionesClientes`;

    DROP TABLE IF EXISTS `SubZonas`;

    DROP TABLE IF EXISTS `Etiquetas`;

    DROP TABLE IF EXISTS `Configuraciones`;

    DROP TABLE IF EXISTS `Barcos`;

    DROP TABLE IF EXISTS `Empaquetados`;

SET foreign_key_checks = 1;

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------


CREATE TABLE `Usuarios`(
	`activo` bool NOT NULL, 
	`clave` varchar (32) NOT NULL, 
	`fechaBaja` datetime, 
	`id` bigint NOT NULL AUTO_INCREMENT UNIQUE, 
	`observaciones` longtext, 
	`permisos` bigint NOT NULL, 
	`usuario` varchar (20) NOT NULL, 
	`resetClave` bool NOT NULL);

ALTER TABLE `Usuarios` ADD PRIMARY KEY (`id`);





CREATE TABLE `Proveedores`(
	`cif` varchar (16), 
	`codigo` varchar (16), 
	`email` varchar (255), 
	`fax` varchar (32), 
	`fechaBaja` datetime, 
	`id` bigint NOT NULL AUTO_INCREMENT UNIQUE, 
	`nombreComercial` varchar (255), 
	`observaciones` longtext, 
	`razonSocial` varchar (255), 
	`telefono1` varchar (32), 
	`telefono2` varchar (32), 
	`web` varchar (528), 
	`RSI` longtext, 
	`puerto` longtext, 
	`alerta` longtext);

ALTER TABLE `Proveedores` ADD PRIMARY KEY (`id`);





CREATE TABLE `DireccionesProveedores`(
	`codigoPostal` varchar (10), 
	`descripcion` varchar (64), 
	`direccion` varchar (255), 
	`fechaBaja` datetime, 
	`id` bigint NOT NULL AUTO_INCREMENT UNIQUE, 
	`observaciones` longtext, 
	`poblacion` varchar (128), 
	`principal` bool, 
	`provincia` varchar (128), 
	`pais` varchar (64) NOT NULL, 
	`Proveedor_id` bigint NOT NULL);

ALTER TABLE `DireccionesProveedores` ADD PRIMARY KEY (`id`);





CREATE TABLE `ContactosProveedor`(
	`apellidos` varchar (128), 
	`cargo` varchar (64), 
	`email` varchar (255), 
	`extension` varchar (8), 
	`fax` varchar (32), 
	`id` bigint NOT NULL AUTO_INCREMENT UNIQUE, 
	`movil` varchar (32), 
	`nombre` varchar (128), 
	`observaciones` longtext, 
	`principal` bool, 
	`telefono` varchar (32), 
	`Proveedor_id` bigint NOT NULL);

ALTER TABLE `ContactosProveedor` ADD PRIMARY KEY (`id`);





CREATE TABLE `provincias`(
	`id` bigint NOT NULL AUTO_INCREMENT UNIQUE, 
	`codigo` longtext NOT NULL, 
	`nombre` longtext NOT NULL);

ALTER TABLE `provincias` ADD PRIMARY KEY (`id`);





CREATE TABLE `municipios`(
	`id` bigint NOT NULL AUTO_INCREMENT UNIQUE, 
	`provincia` longtext NOT NULL, 
	`nombre` longtext NOT NULL);

ALTER TABLE `municipios` ADD PRIMARY KEY (`id`);





CREATE TABLE `Expedidores`(
	`id` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`email` varchar (255), 
	`fax` varchar (32), 
	`razonSocial` varchar (255), 
	`telefono1` varchar (32), 
	`telefono2` varchar (32), 
	`web` varchar (528) NOT NULL, 
	`predeterminada` bool NOT NULL, 
	`fechaBaja` datetime, 
	`observaciones` longtext, 
	`codigo` varchar (16), 
	`cif` varchar (32), 
	`caducidad` varchar (528), 
	`logotipoBinario` mediumblob, 
	`direccion` varchar (500) NOT NULL, 
	`registro` varchar (20) NOT NULL);

ALTER TABLE `Expedidores` ADD PRIMARY KEY (`id`);





CREATE TABLE `ZonasFao`(
	`id` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`descripcion` longtext NOT NULL, 
	`predeterminada` bool NOT NULL, 
	`idTrazamare` int);

ALTER TABLE `ZonasFao` ADD PRIMARY KEY (`id`);





CREATE TABLE `Especies`(
	`id` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`denominacionComercial` longtext NOT NULL, 
	`denominacionCientifica` longtext NOT NULL, 
	`sincronizadoWeb` bool NOT NULL, 
	`ImagenBinario` mediumblob, 
	`alfa3` longtext NOT NULL, 
	`predeterminada` bool NOT NULL);

ALTER TABLE `Especies` ADD PRIMARY KEY (`id`);





CREATE TABLE `MetodosProduccion`(
	`id` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`ImagenBinario` mediumblob, 
	`sincronizadoWeb` bool NOT NULL, 
	`descripcion` longtext NOT NULL, 
	`predeterminada` bool NOT NULL);

ALTER TABLE `MetodosProduccion` ADD PRIMARY KEY (`id`);





CREATE TABLE `FormasPresentacion`(
	`id` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`descripcion` longtext NOT NULL, 
	`predeterminada` bool NOT NULL);

ALTER TABLE `FormasPresentacion` ADD PRIMARY KEY (`id`);





CREATE TABLE `MetodosExtraccion`(
	`id` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`descripcion` longtext NOT NULL, 
	`predeterminada` bool NOT NULL);

ALTER TABLE `MetodosExtraccion` ADD PRIMARY KEY (`id`);





CREATE TABLE `LotesEntrada`(
	`id` bigint NOT NULL AUTO_INCREMENT UNIQUE, 
	`fecha` datetime NOT NULL, 
	`fechaBaja` datetime, 
	`codigo` varchar (16), 
	`observaciones` longtext, 
	`Proveedor_id` bigint NOT NULL, 
	`Usuario_id` bigint NOT NULL);

ALTER TABLE `LotesEntrada` ADD PRIMARY KEY (`id`);





CREATE TABLE `LotesEntradaLineas`(
	`id` bigint NOT NULL AUTO_INCREMENT UNIQUE, 
	`cantidad` decimal( 10, 2 )  NOT NULL, 
	`numeroLote` longtext NOT NULL, 
	`fechaDesembarco` datetime, 
	`Especie_id` int NOT NULL, 
	`LoteEntrada_id` bigint NOT NULL, 
	`ZonaFao_id` int, 
	`Presentacion_id` int, 
	`Produccion_id` int, 
	`SubZona_id` int, 
	`Barco_id` int);

ALTER TABLE `LotesEntradaLineas` ADD PRIMARY KEY (`id`);





CREATE TABLE `LotesSalida`(
	`id` bigint NOT NULL AUTO_INCREMENT UNIQUE, 
	`fecha` datetime NOT NULL, 
	`fechaBaja` datetime, 
	`codigo` varchar (16), 
	`observaciones` longtext, 
	`Usuario_id` bigint NOT NULL, 
	`Cliente_id` bigint NOT NULL, 
	`Expedidor_id` int NOT NULL, 
	`Direccion_id` bigint);

ALTER TABLE `LotesSalida` ADD PRIMARY KEY (`id`);





CREATE TABLE `Clientes`(
	`cif` varchar (32), 
	`codigo` varchar (16), 
	`email` varchar (255), 
	`fax` varchar (32), 
	`fechaBaja` datetime, 
	`id` bigint NOT NULL AUTO_INCREMENT UNIQUE, 
	`nombreComercial` varchar (255), 
	`observaciones` longtext, 
	`razonSocial` varchar (255), 
	`telefono1` varchar (32), 
	`telefono2` varchar (32), 
	`web` varchar (528), 
	`alerta` longtext);

ALTER TABLE `Clientes` ADD PRIMARY KEY (`id`);





CREATE TABLE `ContactosCliente`(
	`apellidos` varchar (64), 
	`cargo` varchar (64), 
	`email` varchar (255), 
	`esPrincipal` bool, 
	`extension` varchar (8), 
	`fax` varchar (32), 
	`id` bigint NOT NULL AUTO_INCREMENT UNIQUE, 
	`movil` varchar (32), 
	`nombre` varchar (128), 
	`observaciones` longtext, 
	`telefono` varchar (32), 
	`Cliente_id` bigint NOT NULL);

ALTER TABLE `ContactosCliente` ADD PRIMARY KEY (`id`);





CREATE TABLE `DireccionesClientes`(
	`codigoPostal` varchar (10), 
	`descripcion` varchar (64), 
	`direccion` varchar (255), 
	`fechaBaja` datetime, 
	`id` bigint NOT NULL AUTO_INCREMENT UNIQUE, 
	`Observaciones` longtext, 
	`poblacion` varchar (128), 
	`principal` bool, 
	`provincia` varchar (128), 
	`tipo` smallint, 
	`pais` varchar (64) NOT NULL, 
	`Cliente_id` bigint NOT NULL);

ALTER TABLE `DireccionesClientes` ADD PRIMARY KEY (`id`);





CREATE TABLE `SubZonas`(
	`id` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`descripcion` longtext NOT NULL, 
	`predeterminada` bool NOT NULL, 
	`idTrazamare` int);

ALTER TABLE `SubZonas` ADD PRIMARY KEY (`id`);





CREATE TABLE `Etiquetas`(
	`id` bigint NOT NULL AUTO_INCREMENT UNIQUE, 
	`sincronizadoWeb` bool NOT NULL, 
	`cantidad` decimal( 10, 2 )  NOT NULL, 
	`fecha` datetime NOT NULL, 
	`fechaBaja` datetime, 
	`DisenhoPred` varchar (250), 
	`LoteEntradaLinea_id` bigint NOT NULL, 
	`Expedidor_id` int NOT NULL, 
	`Empaquetado_id` bigint);

ALTER TABLE `Etiquetas` ADD PRIMARY KEY (`id`);





CREATE TABLE `Configuraciones`(
	`id` bigint NOT NULL AUTO_INCREMENT UNIQUE, 
	`puertoImpresora` int NOT NULL, 
	`CodigoTrazamare` varchar (2) NOT NULL, 
	`IntervaloMuestreo` int NOT NULL, 
	`IntervaloEstable` int NOT NULL, 
	`PesoMinimo` double NOT NULL, 
	`variacionMinima` double NOT NULL, 
	`direccionImpresora` varchar (255) NOT NULL, 
	`TrazamareBBDDServidor` varchar (500) NOT NULL, 
	`TrazamareBBDDBaseDatos` varchar (500) NOT NULL, 
	`TrazamareBBDDUsuario` varchar (500) NOT NULL, 
	`TrazamareBBDDClave` longtext NOT NULL, 
	`TrazamareBBDDPuerto` int NOT NULL, 
	`TrazamareBBDDTimeOut` int NOT NULL, 
	`TrazamareFTPServidor` varchar (500) NOT NULL, 
	`TrazamareFTPRuta` varchar (500) NOT NULL, 
	`TrazamareFTPUsuario` varchar (500) NOT NULL, 
	`TrazamareFTPClave` longtext NOT NULL, 
	`TrazamareFTPPuerto` int NOT NULL, 
	`IntervaloSincronizacion` int, 
	`Version` varchar (30) NOT NULL);

ALTER TABLE `Configuraciones` ADD PRIMARY KEY (`id`);





CREATE TABLE `Barcos`(
	`id` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`descripcion` longtext NOT NULL, 
	`ImagenBinario` mediumblob, 
	`sincronizadoWeb` bool NOT NULL, 
	`predeterminada` bool NOT NULL);

ALTER TABLE `Barcos` ADD PRIMARY KEY (`id`);





CREATE TABLE `Empaquetados`(
	`id` bigint NOT NULL AUTO_INCREMENT UNIQUE, 
	`observaciones` longtext, 
	`fecha` datetime NOT NULL, 
	`fechaBaja` datetime, 
	`sincronizadoWeb` bool NOT NULL, 
	`Usuario_id` bigint NOT NULL, 
	`LoteSalida_id` bigint);

ALTER TABLE `Empaquetados` ADD PRIMARY KEY (`id`);







-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------


-- Creating foreign key on `Proveedor_id` in table 'DireccionesProveedores'

ALTER TABLE `DireccionesProveedores`
ADD CONSTRAINT `FK_ProveedorDireccionProveedor`
    FOREIGN KEY (`Proveedor_id`)
    REFERENCES `Proveedores`
        (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;


-- Creating non-clustered index for FOREIGN KEY 'FK_ProveedorDireccionProveedor'

CREATE INDEX `IX_FK_ProveedorDireccionProveedor`
    ON `DireccionesProveedores`
    (`Proveedor_id`);



-- Creating foreign key on `Proveedor_id` in table 'ContactosProveedor'

ALTER TABLE `ContactosProveedor`
ADD CONSTRAINT `FK_ContactoProveedorProveedor`
    FOREIGN KEY (`Proveedor_id`)
    REFERENCES `Proveedores`
        (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;


-- Creating non-clustered index for FOREIGN KEY 'FK_ContactoProveedorProveedor'

CREATE INDEX `IX_FK_ContactoProveedorProveedor`
    ON `ContactosProveedor`
    (`Proveedor_id`);



-- Creating foreign key on `Proveedor_id` in table 'LotesEntrada'

ALTER TABLE `LotesEntrada`
ADD CONSTRAINT `FK_ProveedorLoteEntrada`
    FOREIGN KEY (`Proveedor_id`)
    REFERENCES `Proveedores`
        (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;


-- Creating non-clustered index for FOREIGN KEY 'FK_ProveedorLoteEntrada'

CREATE INDEX `IX_FK_ProveedorLoteEntrada`
    ON `LotesEntrada`
    (`Proveedor_id`);



-- Creating foreign key on `Especie_id` in table 'LotesEntradaLineas'

ALTER TABLE `LotesEntradaLineas`
ADD CONSTRAINT `FK_EspecieLoteEntradaLinea`
    FOREIGN KEY (`Especie_id`)
    REFERENCES `Especies`
        (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;


-- Creating non-clustered index for FOREIGN KEY 'FK_EspecieLoteEntradaLinea'

CREATE INDEX `IX_FK_EspecieLoteEntradaLinea`
    ON `LotesEntradaLineas`
    (`Especie_id`);



-- Creating foreign key on `LoteEntrada_id` in table 'LotesEntradaLineas'

ALTER TABLE `LotesEntradaLineas`
ADD CONSTRAINT `FK_LoteEntradaLoteEntradaLinea`
    FOREIGN KEY (`LoteEntrada_id`)
    REFERENCES `LotesEntrada`
        (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;


-- Creating non-clustered index for FOREIGN KEY 'FK_LoteEntradaLoteEntradaLinea'

CREATE INDEX `IX_FK_LoteEntradaLoteEntradaLinea`
    ON `LotesEntradaLineas`
    (`LoteEntrada_id`);



-- Creating foreign key on `Usuario_id` in table 'LotesEntrada'

ALTER TABLE `LotesEntrada`
ADD CONSTRAINT `FK_UsuarioLoteEntrada`
    FOREIGN KEY (`Usuario_id`)
    REFERENCES `Usuarios`
        (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;


-- Creating non-clustered index for FOREIGN KEY 'FK_UsuarioLoteEntrada'

CREATE INDEX `IX_FK_UsuarioLoteEntrada`
    ON `LotesEntrada`
    (`Usuario_id`);



-- Creating foreign key on `ZonaFao_id` in table 'LotesEntradaLineas'

ALTER TABLE `LotesEntradaLineas`
ADD CONSTRAINT `FK_ZonaFaoLoteEntradaLinea`
    FOREIGN KEY (`ZonaFao_id`)
    REFERENCES `ZonasFao`
        (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;


-- Creating non-clustered index for FOREIGN KEY 'FK_ZonaFaoLoteEntradaLinea'

CREATE INDEX `IX_FK_ZonaFaoLoteEntradaLinea`
    ON `LotesEntradaLineas`
    (`ZonaFao_id`);



-- Creating foreign key on `Presentacion_id` in table 'LotesEntradaLineas'

ALTER TABLE `LotesEntradaLineas`
ADD CONSTRAINT `FK_PresentacionLoteEntradaLinea`
    FOREIGN KEY (`Presentacion_id`)
    REFERENCES `FormasPresentacion`
        (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;


-- Creating non-clustered index for FOREIGN KEY 'FK_PresentacionLoteEntradaLinea'

CREATE INDEX `IX_FK_PresentacionLoteEntradaLinea`
    ON `LotesEntradaLineas`
    (`Presentacion_id`);



-- Creating foreign key on `Produccion_id` in table 'LotesEntradaLineas'

ALTER TABLE `LotesEntradaLineas`
ADD CONSTRAINT `FK_MetodoProduccionLoteEntradaLinea`
    FOREIGN KEY (`Produccion_id`)
    REFERENCES `MetodosProduccion`
        (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;


-- Creating non-clustered index for FOREIGN KEY 'FK_MetodoProduccionLoteEntradaLinea'

CREATE INDEX `IX_FK_MetodoProduccionLoteEntradaLinea`
    ON `LotesEntradaLineas`
    (`Produccion_id`);



-- Creating foreign key on `Usuario_id` in table 'LotesSalida'

ALTER TABLE `LotesSalida`
ADD CONSTRAINT `FK_UsuarioLoteSalida`
    FOREIGN KEY (`Usuario_id`)
    REFERENCES `Usuarios`
        (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;


-- Creating non-clustered index for FOREIGN KEY 'FK_UsuarioLoteSalida'

CREATE INDEX `IX_FK_UsuarioLoteSalida`
    ON `LotesSalida`
    (`Usuario_id`);



-- Creating foreign key on `Cliente_id` in table 'LotesSalida'

ALTER TABLE `LotesSalida`
ADD CONSTRAINT `FK_ClienteLoteSalida`
    FOREIGN KEY (`Cliente_id`)
    REFERENCES `Clientes`
        (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;


-- Creating non-clustered index for FOREIGN KEY 'FK_ClienteLoteSalida'

CREATE INDEX `IX_FK_ClienteLoteSalida`
    ON `LotesSalida`
    (`Cliente_id`);



-- Creating foreign key on `Expedidor_id` in table 'LotesSalida'

ALTER TABLE `LotesSalida`
ADD CONSTRAINT `FK_ExpedidorLoteSalida`
    FOREIGN KEY (`Expedidor_id`)
    REFERENCES `Expedidores`
        (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;


-- Creating non-clustered index for FOREIGN KEY 'FK_ExpedidorLoteSalida'

CREATE INDEX `IX_FK_ExpedidorLoteSalida`
    ON `LotesSalida`
    (`Expedidor_id`);



-- Creating foreign key on `Cliente_id` in table 'ContactosCliente'

ALTER TABLE `ContactosCliente`
ADD CONSTRAINT `FK_ClienteContactoCliente`
    FOREIGN KEY (`Cliente_id`)
    REFERENCES `Clientes`
        (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;


-- Creating non-clustered index for FOREIGN KEY 'FK_ClienteContactoCliente'

CREATE INDEX `IX_FK_ClienteContactoCliente`
    ON `ContactosCliente`
    (`Cliente_id`);



-- Creating foreign key on `Cliente_id` in table 'DireccionesClientes'

ALTER TABLE `DireccionesClientes`
ADD CONSTRAINT `FK_ClienteDireccionCliente`
    FOREIGN KEY (`Cliente_id`)
    REFERENCES `Clientes`
        (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;


-- Creating non-clustered index for FOREIGN KEY 'FK_ClienteDireccionCliente'

CREATE INDEX `IX_FK_ClienteDireccionCliente`
    ON `DireccionesClientes`
    (`Cliente_id`);



-- Creating foreign key on `Direccion_id` in table 'LotesSalida'

ALTER TABLE `LotesSalida`
ADD CONSTRAINT `FK_LoteSalidaDireccionCliente`
    FOREIGN KEY (`Direccion_id`)
    REFERENCES `DireccionesClientes`
        (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;


-- Creating non-clustered index for FOREIGN KEY 'FK_LoteSalidaDireccionCliente'

CREATE INDEX `IX_FK_LoteSalidaDireccionCliente`
    ON `LotesSalida`
    (`Direccion_id`);



-- Creating foreign key on `SubZona_id` in table 'LotesEntradaLineas'

ALTER TABLE `LotesEntradaLineas`
ADD CONSTRAINT `FK_SubZonaLoteEntradaLinea`
    FOREIGN KEY (`SubZona_id`)
    REFERENCES `SubZonas`
        (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;


-- Creating non-clustered index for FOREIGN KEY 'FK_SubZonaLoteEntradaLinea'

CREATE INDEX `IX_FK_SubZonaLoteEntradaLinea`
    ON `LotesEntradaLineas`
    (`SubZona_id`);



-- Creating foreign key on `LoteEntradaLinea_id` in table 'Etiquetas'

ALTER TABLE `Etiquetas`
ADD CONSTRAINT `FK_LoteEntradaLineaEtiqueta`
    FOREIGN KEY (`LoteEntradaLinea_id`)
    REFERENCES `LotesEntradaLineas`
        (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;


-- Creating non-clustered index for FOREIGN KEY 'FK_LoteEntradaLineaEtiqueta'

CREATE INDEX `IX_FK_LoteEntradaLineaEtiqueta`
    ON `Etiquetas`
    (`LoteEntradaLinea_id`);



-- Creating foreign key on `Expedidor_id` in table 'Etiquetas'

ALTER TABLE `Etiquetas`
ADD CONSTRAINT `FK_ExpedidorEtiqueta`
    FOREIGN KEY (`Expedidor_id`)
    REFERENCES `Expedidores`
        (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;


-- Creating non-clustered index for FOREIGN KEY 'FK_ExpedidorEtiqueta'

CREATE INDEX `IX_FK_ExpedidorEtiqueta`
    ON `Etiquetas`
    (`Expedidor_id`);



-- Creating foreign key on `Barco_id` in table 'LotesEntradaLineas'

ALTER TABLE `LotesEntradaLineas`
ADD CONSTRAINT `FK_BarcoLoteEntradaLinea`
    FOREIGN KEY (`Barco_id`)
    REFERENCES `Barcos`
        (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;


-- Creating non-clustered index for FOREIGN KEY 'FK_BarcoLoteEntradaLinea'

CREATE INDEX `IX_FK_BarcoLoteEntradaLinea`
    ON `LotesEntradaLineas`
    (`Barco_id`);



-- Creating foreign key on `Usuario_id` in table 'Empaquetados'

ALTER TABLE `Empaquetados`
ADD CONSTRAINT `FK_UsuarioEmpaquetado`
    FOREIGN KEY (`Usuario_id`)
    REFERENCES `Usuarios`
        (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;


-- Creating non-clustered index for FOREIGN KEY 'FK_UsuarioEmpaquetado'

CREATE INDEX `IX_FK_UsuarioEmpaquetado`
    ON `Empaquetados`
    (`Usuario_id`);



-- Creating foreign key on `Empaquetado_id` in table 'Etiquetas'

ALTER TABLE `Etiquetas`
ADD CONSTRAINT `FK_EmpaquetadoEtiqueta`
    FOREIGN KEY (`Empaquetado_id`)
    REFERENCES `Empaquetados`
        (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;


-- Creating non-clustered index for FOREIGN KEY 'FK_EmpaquetadoEtiqueta'

CREATE INDEX `IX_FK_EmpaquetadoEtiqueta`
    ON `Etiquetas`
    (`Empaquetado_id`);



-- Creating foreign key on `LoteSalida_id` in table 'Empaquetados'

ALTER TABLE `Empaquetados`
ADD CONSTRAINT `FK_LoteSalidaEmpaquetado`
    FOREIGN KEY (`LoteSalida_id`)
    REFERENCES `LotesSalida`
        (`id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;


-- Creating non-clustered index for FOREIGN KEY 'FK_LoteSalidaEmpaquetado'

CREATE INDEX `IX_FK_LoteSalidaEmpaquetado`
    ON `Empaquetados`
    (`LoteSalida_id`);



-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------
