﻿Imports DAL
Imports System.Linq

Public Class cConfiguracionPrograma
#Region " DECLARACIONES "
    Private Shared _Contexto As Entidades = Nothing
    Private Shared _Instancia As Configuracion = Nothing
#End Region
#Region " PROPIEDADES "
    Private Shared ReadOnly Property Contexto As Entidades
        Get
            If _Contexto Is Nothing Then _Contexto = cDAL.Instancia.getContext
            Return _Contexto
        End Get
    End Property

    Public Shared ReadOnly Property Instancia As Configuracion
        Get
            If _Instancia Is Nothing Then
                _Instancia = New Configuracion
                Cargar(_Instancia)
            End If

            Return _Instancia
        End Get
    End Property
#End Region
#Region " METODOS "
    Private Shared Sub Cargar(ByRef Registro As Configuracion)
        Try
            Registro = (From It As Configuracion In Contexto.Configuraciones Where It.id = 1 Select It).FirstOrDefault

        Catch ex As Exception
            Debugger.Break()
        End Try

        If Registro Is Nothing Then

            Registro = New Configuracion
            With Registro
                .id = 1
                .direccionImpresora = "127.0.0.1"
                .IntervaloMuestreo = 50
                .PesoMinimo = 0.1
                .puertoImpresora = 1000
                .variacionMinima = 0.1
                .IntervaloEstable = 1000
            End With
            Contexto.AddToConfiguraciones(Registro)
        End If
    End Sub
#End Region
End Class
