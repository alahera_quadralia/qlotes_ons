﻿Public Class cConfiguracionBBDD
    Inherits cConfiguracionBase(Of cConfiguracionBBDD)

#Region " GENERAL "
    Public Id As Integer

    Public Overrides ReadOnly Property NombreTabla As String
        Get
            Return "tConfiguracionBBDD"
        End Get
    End Property
#End Region
#Region " BASE DE DATOS "
    Public Property BBDDServidor As String
    Public Property BBDDUsuario As String
    Public Property BBDDPassword As String
    Public Property BBDDPuerto As Integer
    Public Property BBDDNombre As String
#End Region
#Region " METODOS A IMPLEMENTAR DE LA CLASE BASE "
    Public Overrides Function ObtenerDiccionario() As List(Of Mapeado)
        Dim Diccionario As New List(Of Mapeado)
#If DEBUG Then
        Diccionario.Add(New Mapeado("BBDDServidor", "BBDDServidor", "192.168.0.110", False))
        Diccionario.Add(New Mapeado("BBDDUsuario", "BBDDUsuario", "jgallego", False))
        Diccionario.Add(New Mapeado("BBDDPassword", "BBDDPassword", "jgallegoj", False))
        Diccionario.Add(New Mapeado("BBDDNombre", "BBDDNombre", "Escritorio", False))
        Diccionario.Add(New Mapeado("BBDDPuerto", "BBDDPuerto", 3306, False))
#Else
        Diccionario.Add(New Mapeado("BBDDServidor", "BBDDServidor", "127.0.0.1", False))
        Diccionario.Add(New Mapeado("BBDDUsuario", "BBDDUsuario", "root", False))
        Diccionario.Add(New Mapeado("BBDDPassword", "BBDDPassword", "", False))
        Diccionario.Add(New Mapeado("BBDDNombre", "BBDDNombre", "Escritorio", False))
        Diccionario.Add(New Mapeado("BBDDPuerto", "BBDDPuerto", 3306, False))
#End If
        Diccionario.Add(New Mapeado("Id", "Id", 0, True))

        Return Diccionario
    End Function
#End Region
End Class

Public Class cConfiguracionAplicacion
    Inherits cConfiguracionBase(Of cConfiguracionAplicacion)

#Region " GENERAL "
    Public Id As Integer

    Public Overrides ReadOnly Property NombreTabla As String
        Get
            Return "tConfiguracionAplicacion"
        End Get
    End Property
#End Region
#Region " APLICACION "
    Public Property EnviarErrores As Boolean
    Public Property PermitirNavegabilidad As Boolean
    Public Property VentanasMaximizadas As Boolean
    Public Property FiltrosAbiertos As Boolean
    Public Property BuscadoresAbiertos As Boolean
    Public Property ClaveEncriptacionBackup As String
    Public Property ServidorErrores As String
    Public Property Impresora As String
    Public Property Formato As Integer
    Public Property Orientacion As Integer
#End Region
#Region " METODOS A IMPLEMENTAR DE LA CLASE BASE "
    Public Overrides Function ObtenerDiccionario() As List(Of Mapeado)
        Dim Diccionario As New List(Of Mapeado)

        Diccionario.Add(New Mapeado("Id", "Id", 0, True))
        Diccionario.Add(New Mapeado("EnviarErrores", "EnviarErrores", True, False))
        Diccionario.Add(New Mapeado("BuscadoresAbiertos", "BuscadoresAbiertos", True, False))
        Diccionario.Add(New Mapeado("VentanasMaximizadas", "VentanasMaximizadas", True, False))
        Diccionario.Add(New Mapeado("FiltrosAbiertos", "FiltrosAbiertos", True, False))
        Diccionario.Add(New Mapeado("Navegabilidad", "PermitirNavegabilidad", True, False))
        Diccionario.Add(New Mapeado("ClaveEncriptacionBackup", "ClaveEncriptacionBackup", "", False))
        Diccionario.Add(New Mapeado("ServidorErrores", "ServidorErrores", "http://error.quadralia.com/servicioWeb.php", False))
        Diccionario.Add(New Mapeado("Impresora", "Impresora", "", False))
        Diccionario.Add(New Mapeado("Formato", "Formato", -1, False))
        Diccionario.Add(New Mapeado("Orientacion", "Orientacion", -1, False))
        Return Diccionario
    End Function
#End Region
End Class
