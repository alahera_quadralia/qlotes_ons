﻿Imports DAL
Imports System.Linq
Imports System.Data.EntityClient
Imports System.IO

Public Class Sincronizador
#Region " ENUMERADOS "
    Private Enum CodigosEventos
        ConfiguracionCargada = 1
        ConexionBBDD = 2
        SolicitudDeParada = 3
        ParadaCompleta = 4
        Sincronizando = 5

        ' Errores
        ConfiguracionNoCargada = 1001
        SinConexionBBDD = 1002
        SinConfiguracion = 1003
        ErrorSincronizacion = 1005
    End Enum
#End Region
#Region " CONTEXTO "
    ''' <summary>
    ''' Obtiene el contexto para la conexión con BBDD
    ''' </summary>
    Private Function ObtenerContexto() As Entidades
        Dim DatosConexion As New EntityConnectionStringBuilder()
        DatosConexion.Metadata = "res://*/Modelo.csdl|res://*/Modelo.ssdl|res://*/Modelo.msl"
        DatosConexion.Provider = "MySql.Data.MySqlClient"
        DatosConexion.ProviderConnectionString = String.Format("Server={0};Database={1};Uid={2};Pwd={3};Port={4}", cConfiguracionBBDD.Instancia.BBDDServidor, cConfiguracionBBDD.Instancia.BBDDNombre, cConfiguracionBBDD.Instancia.BBDDUsuario, cConfiguracionBBDD.Instancia.BBDDPassword, cConfiguracionBBDD.Instancia.BBDDPuerto)
        Return New Entidades(DatosConexion.ConnectionString)
    End Function
#End Region
#Region " DECLARACIONES "
    ''' <summary>
    ''' Timer para ejecución de sincronizaciones
    ''' </summary>
    Private Timer As Timers.Timer

    ''' <summary>
    ''' Bandera que indica si tenemos conexión a base de datos o no
    ''' </summary>
    Private ConexionBBDD As Boolean = False

    ''' <summary>
    ''' El propio sincronizador
    ''' </summary>
    Private WithEvents Sincronizador As qSinc.Sincronizador = qSinc.Sincronizador.Instancia
#End Region
#Region " INICIO Y PARADA DEL SERVICIIO "
    ''' <summary>
    ''' Rutina de inicio del servicio
    ''' </summary>
    ''' <param name="args"></param>
    ''' <remarks></remarks>
    Protected Overrides Sub OnStart(ByVal args() As String)
        CargarConfiguracion()
        If Not ConexionBBDD Then
            ExitCode = -1
            End
        End If

        RecargarConfiguracion()
    End Sub

    Private Sub RecargarConfiguracion()
        If Timer IsNot Nothing Then Timer.Stop()

        ' Cargo la configuración
        Dim Configuracion As DAL.Configuracion = Nothing
        Try
            Dim Contexto As Entidades = ObtenerContexto()
            Configuracion = Contexto.Configuraciones.FirstOrDefault
            If Configuracion Is Nothing Then
                NotificarError("No se ha podido cargar la configuración de la base de datos", New Exception("No se ha podido cargar la configuración de la base de datos"), CodigosEventos.SinConfiguracion)
                ExitCode = -2
                End
            End If
        Catch ex As Exception
            NotificarError("No se ha podido cargar la configuración de la base de datos", ex, CodigosEventos.SinConfiguracion)
        End Try

        ' Añado la configuración del servidor
        If Not Configuracion.IntervaloSincronizacion.HasValue AndAlso Configuracion.IntervaloSincronizacion.Value <= 0 Then Exit Sub

        Timer = New Timers.Timer(Configuracion.IntervaloSincronizacion.Value * 60 * 1000)
        RemoveHandler Timer.Elapsed, AddressOf Sincronizar
        AddHandler Timer.Elapsed, AddressOf Sincronizar
        Timer.Start()
    End Sub

    Protected Overrides Sub OnStop()
        NotificarInfo("Petición de parada recibida", CodigosEventos.SolicitudDeParada)
        Timer.Stop()

        NotificarInfo("Servicio Detenido", CodigosEventos.ParadaCompleta)
        ExitCode = 1
    End Sub

    ''' <summary>
    ''' Ejecuta el comando deseado
    ''' </summary>
    Protected Overrides Sub OnCustomCommand(command As Integer)
        Select Case command
            Case 101 ' Recargar Configuracion
                RecargarConfiguracion()
            Case 102 ' Forzar Sincronizacion
                Sincronizar(Nothing, Nothing)
        End Select
    End Sub
#End Region
#Region " CONTROL DEL TIMER "
    Public Sub Sincronizar(sender As Object, e As Timers.ElapsedEventArgs)
        If qSinc.Sincronizador.Instancia.Ejecutando Then Exit Sub
        If Not ConexionBBDD Then Exit Sub

        Try
            Dim Contexto As DAL.Entidades = ObtenerContexto()
            Dim Configuracion As Configuracion = (From cnf As Configuracion In Contexto.Configuraciones Select cnf).FirstOrDefault

            ' Validaciones
            If Configuracion Is Nothing Then Exit Sub
            If Configuracion.IntervaloSincronizacion.HasValue AndAlso (Configuracion.IntervaloSincronizacion.Value * 60 * 1000 <> Timer.Interval) Then
                ' Reconfiguro el timer
                Timer.Stop()
                Timer.Interval = Configuracion.IntervaloSincronizacion.Value * 60 * 1000
                Timer.Start()
            End If

            Dim DatosBBDD = New qSinc.Sincronizador.ConexionBBDD
            With DatosBBDD
                .Puerto = Configuracion.TrazamareBBDDPuerto
                .BaseDatos = Configuracion.TrazamareBBDDBaseDatos
                .Clave = Configuracion.TrazamareBBDDClave
                .Servidor = Configuracion.TrazamareBBDDServidor
                .Timeout = Configuracion.TrazamareBBDDTimeOut
                .Usuario = Configuracion.TrazamareBBDDUsuario
            End With

            Dim DatosFTP = New qSinc.Sincronizador.ConexionFTP
            With DatosFTP
                .Puerto = Configuracion.TrazamareFTPPuerto
                .Clave = Configuracion.TrazamareFTPClave
                .Servidor = Configuracion.TrazamareFTPServidor
                .Usuario = Configuracion.TrazamareFTPUsuario
                .RutaCarpeta = Configuracion.TrazamareFTPRuta
            End With

            qSinc.Sincronizador.Instancia.DatosConexionBBDD = DatosBBDD
            qSinc.Sincronizador.Instancia.DatosConexionFTP = DatosFTP

            Dim Artes As List(Of MetodoProduccion) = (From art As MetodoProduccion In Contexto.MetodosProduccion Where Not art.sincronizadoWeb Select art).ToList
            Dim Barcos As List(Of Barco) = (From bar As Barco In Contexto.Barcos Where Not bar.sincronizadoWeb Select bar).ToList
            Dim Especies As List(Of Especie) = (From esp As Especie In Contexto.Especies Where Not esp.sincronizadoWeb Select esp).ToList
            Dim Empaquetados As List(Of Empaquetado) = (From emp As Empaquetado In Contexto.Empaquetados Where Not emp.sincronizadoWeb AndAlso
                                                        emp.fechaBaja Is Nothing Select emp).ToList

            Dim idsEmpaquetados As Int64() = (From em In Empaquetados Select em.id).ToArray()
            Dim Etiquetas As List(Of Etiqueta) = Nothing

            If (idsEmpaquetados IsNot Nothing AndAlso idsEmpaquetados.Count() > 0) Then
                Etiquetas = (From etq As Etiqueta In Contexto.Etiquetas Where idsEmpaquetados.Contains(etq.Empaquetado.id) AndAlso
                                 etq.fechaBaja Is Nothing Select etq).ToList()
            End If

            NotificarInfo(String.Format("Sincronizando Elementos:{0}{1}Etiquetas: {2}{0}{1}Artes: {3}{0}{1}Barcos: {4}{0}{1}Especies: {5}", Environment.NewLine, vbTab, Etiquetas.Count, Artes.Count, Barcos.Count, Especies.Count), CodigosEventos.Sincronizando)
            qSinc.Sincronizador.Instancia.Sincronizar(Etiquetas, Barcos, Especies, Artes, Empaquetados, True)

            Contexto.SaveChanges()

        Catch ex As Exception
            EscribirEnLog(ex.Message + "-" + ex.InnerException.ToString())
            NotificarError("Error durante el proceso de sincronización", ex, CodigosEventos.ErrorSincronizacion)
        End Try
    End Sub
#End Region
#Region " CONFIGURACIONES "
    ''' <summary>
    ''' Carga la configuracion de acceso a la base de datos y del programa en general
    ''' </summary>
    Private Sub CargarConfiguracion()
        Try
            ' Cargo la configuración del programa
            cConfiguracionAplicacion.Instancia.Cargar(1)
            cConfiguracionBBDD.Instancia.Cargar(1)

            NotificarInfo("Configuración cargada correctamente", CodigosEventos.ConfiguracionCargada)
        Catch ex As Exception
            NotificarError("No se pudo cargar la configuración del archivo de configuración", ex, CodigosEventos.ConfiguracionNoCargada)
        End Try

#If DEBUG Then
        ' Establezco valores para debug
        'cConfiguracionBBDD.Instancia.BBDDServidor = "192.168.0.110"
        'cConfiguracionBBDD.Instancia.BBDDUsuario = "root"
        'cConfiguracionBBDD.Instancia.BBDDPassword = "quadralia"

        'cConfiguracionBBDD.Instancia.BBDDServidor = "127.0.0.1"
        'cConfiguracionBBDD.Instancia.BBDDUsuario = "root"
        'cConfiguracionBBDD.Instancia.BBDDPassword = ""
        'cConfiguracionBBDD.Instancia.BBDDNombre = "qLotes"
#End If

        Using contexto = ObtenerContexto()
            Try
                ConexionBBDD = contexto.DatabaseExists
                ConexionBBDD = True
                NotificarInfo("Comunicación con base de datos establecida correctamente", CodigosEventos.ConexionBBDD)
            Catch ex As Exception
                ConexionBBDD = False
                NotificarError("No se pudo establecer una comunicación con la base de datos", ex, CodigosEventos.SinConexionBBDD)
            Finally
                contexto.Connection.Close()
            End Try
        End Using
    End Sub
#End Region
#Region " LOG "
    ''' <summary>
    ''' Escribe un mensaje de información en el log del sistema
    ''' </summary>
    Private Sub NotificarInfo(Mensaje As String, Identificador As CodigosEventos)
        LogEventos.WriteEntry(Mensaje, EventLogEntryType.Information, Identificador)
    End Sub

    ''' <summary>
    ''' Escribe un mensaje de error en el log del sistema
    ''' </summary>
    Private Sub NotificarError(Mensaje As String, ex As Exception, Identificador As CodigosEventos)
        LogEventos.WriteEntry(String.Format("{0}. Error: {1}", Mensaje, ex.Message), EventLogEntryType.Error, Identificador)
    End Sub
#End Region

    Public Sub EscribirEnLog(pMensaje As String)
        Dim strFile As String = "c:\ErroresLog.txt"
        ''Dim fileExists As Boolean = File.Exists(strFile)
        File.AppendAllText(strFile, pMensaje + Environment.NewLine)
    End Sub
End Class
