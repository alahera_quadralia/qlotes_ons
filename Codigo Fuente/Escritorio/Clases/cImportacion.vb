﻿Imports System.IO
Imports System.Data.OleDb
Imports System.Threading
Imports System.Globalization

Public Class cImportador
#Region " DECLARACIONES "
    Private Const CONNECTIONSTRING_XLS As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='Excel 8.0;HDR={1};IMEX=1'"
    ' Private Const CONNECTIONSTRING_XLS As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=""Excel 12.0;HDR={1};IMEX=1"""
    Private Const CONNECTIONSTRING_CSV As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='text;HDR={1};FMT=Delimited';"

    Private ExtensionesAdmitidas As String() = {".XLS", ".XLSX", ".CSV"} 'Extensiones de archivos admitidas
    Private _NombreHojas As New List(Of String) 'Contiene el nombre de todas las hojas del archivo Excel
    Private _RutaFichero As String = "" 'Ruta física del archivo
    Private _PrimeraFilaCabecera As Boolean = False 'Indica si la primera fila del archivo se trata como cabecera de los datos
    Private _CaracterDelimitador As String = ";"

    Private _ds As DataSet
#End Region
#Region " PROPIEDADES "
    ''' <summary>
    ''' Ruta física del archivo a importar
    ''' </summary>
    Public Property RutaFichero() As String
        Get
            Return _RutaFichero
        End Get
        Set(ByVal value As String)
            _RutaFichero = value
        End Set
    End Property

    ''' <summary>
    ''' Devuelve un dataset con los datos leidos desde el archivo XML
    ''' </summary>
    Public ReadOnly Property DataSetDatos() As DataSet
        Get
            Return _ds
        End Get
    End Property

    ''' <summary>
    ''' Indica si se tratará la primera fila del archivo como cabecera de las columnas
    ''' </summary>
    Public Property PrimeraFilaContieneCabecera() As Boolean
        Get
            Return _PrimeraFilaCabecera
        End Get
        Set(ByVal value As Boolean)
            _PrimeraFilaCabecera = value
        End Set
    End Property

    ''' <summary>
    ''' Establece el caracter que se utilizará como delimitador
    ''' </summary>
    Public Property CaracterDelimitador() As String
        Get
            Return _CaracterDelimitador
        End Get
        Set(ByVal value As String)
            If String.IsNullOrEmpty(value) Then
                _CaracterDelimitador = ";"
            Else
                _CaracterDelimitador = value
            End If
        End Set
    End Property
#End Region
#Region " CONSTRUCTORES "
    ''' <summary>
    ''' Constructor Genérico de la clases
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()
    End Sub

    ''' <summary>
    ''' Instancia un nuevo objeto de la clase y lee el archivo
    ''' </summary>
    ''' <param name="Ruta_Fichero">Ruta Física del archivo a importar</param>
    Public Sub New(ByVal Ruta_Fichero As String)
        Me.RutaFichero = Ruta_Fichero
        Leer()
    End Sub
#End Region
#Region " METODOS "
    ''' <summary>
    ''' Lee los datos del archivo establecido previamente
    ''' </summary>
    ''' <param name="Ruta_Fichero">Ruta física del fichero desde el que realizar la importación de datos</param>
    ''' <returns>Booleano que indica si se realizó correctamente la lectura del fichero</returns>
    Public Function Leer(ByVal Ruta_Fichero As String) As Boolean
        'Establecemos previamente la ruta del fichero y leemos los datos
        Me.RutaFichero = Ruta_Fichero
        Return Leer()
    End Function

    ''' <summary>
    ''' Lee los datos del archivo establecido previamente
    ''' </summary>
    ''' <returns>Booleano que indica si se realizó correctamente la lectura del fichero</returns>
    Public Function Leer() As Boolean
        'Inicializo las variables
        _ds = Nothing
        _NombreHojas.Clear()

        'Si no tengo nada que cargar, salgo de la función
        If Me.RutaFichero = "" OrElse Not IO.File.Exists(Me.RutaFichero) Then Return False

        'Obtengo la extensión del archivo y miro si está dentro de las admitidas
        Dim ExtensionArchivo As String = IO.Path.GetExtension(Me.RutaFichero).ToUpper()
        Dim ExtensionAdmitida As Integer = Array.IndexOf(ExtensionesAdmitidas, ExtensionArchivo)
        Dim CadenaConexion As String = String.Empty
        Dim Resultado As Boolean = False

        'Discrimino en función de las extensiones
        Select Case ExtensionAdmitida
            Case 0, 1
                If Not LeerConfiguracionExcel() Then Return False
                CadenaConexion = String.Format(CONNECTIONSTRING_XLS, Me.RutaFichero, IIf(_PrimeraFilaCabecera, "YES", "NO"))
                Resultado = LeerDatos(CadenaConexion)
            Case 2
                _NombreHojas.Add(Path.GetFileName(Me.RutaFichero))
                CadenaConexion = String.Format(CONNECTIONSTRING_CSV, Path.GetDirectoryName(Me.RutaFichero), IIf(_PrimeraFilaCabecera, "YES", "NO"))
                EscribirEsquema()
                Resultado = LeerDatos(CadenaConexion)
                If IO.File.Exists(IO.Path.GetDirectoryName(_RutaFichero) & "\schema.ini") Then IO.File.Delete(IO.Path.GetDirectoryName(_RutaFichero) & "\schema.ini")
        End Select

        Return Resultado
    End Function

    ''' <summary>
    ''' Lee los datos desde el fichero y rellena un dataset donde cada tabla tendrá el nombre de las hojas de excel o del nombre del fichero CSV
    ''' </summary>
    ''' <param name="CadenaDeConexion">Cadena de conexión necesaria para conectarnos al archivo</param>
    ''' <returns>Booleano que indica si realizamos correctamente la importación de datos</returns>
    Private Function LeerDatos(ByVal CadenaDeConexion As String) As Boolean
        Try
            _ds = New DataSet

            For Each Hoja As String In _NombreHojas
                If Hoja.Contains("$") And Not Hoja.EndsWith("$") Then Continue For

                Dim Rango As String = "A1:BP10000"
                If Not Hoja.EndsWith("$") Then Rango = "$" & Rango

                Dim dAdapter As New OleDbDataAdapter("Select * From [" & Hoja & Rango & "]", CadenaDeConexion)

                ' agrega los datos
                dAdapter.Fill(_ds, Hoja.Replace("$", ""))
            Next

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Accede a un fichero de excel y recupera el nombre de las hojas que contiene
    ''' </summary>
    ''' <remarks></remarks>
    Private Function LeerConfiguracionExcel() As Boolean
        Dim cn As New OleDb.OleDbConnection(String.Format(CONNECTIONSTRING_XLS, Me.RutaFichero, IIf(_PrimeraFilaCabecera, "YES", "NO")))

        Try

            cn.Open()

            For Each fila As DataRow In cn.GetSchema("Tables").Rows
                _NombreHojas.Add(fila("Table_Name").ToString())
            Next

            cn.Close()

            Return True
        Catch ex As Exception
            MessageBox.Show("Error al tratar de obtener el rango activo de la hoja de cálculo: " & ex.Message)
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Crea un fichero de texto especificando los parámetros necesarios para la importacion
    ''' </summary>
    Private Sub EscribirEsquema()
        Dim fsOutput As New FileStream(IO.Path.GetDirectoryName(_RutaFichero) & "\schema.ini", FileMode.Create, FileAccess.Write)
        Dim srOutput As StreamWriter = New StreamWriter(fsOutput)
        Dim s1, s2, s3, s4, s5 As String
        s1 = "[" & IO.Path.GetFileName(_RutaFichero) & "]"
        s2 = "ColNameHeader=" & _PrimeraFilaCabecera.ToString()
        s3 = "Format=Delimited(" & _CaracterDelimitador & ")"
        s4 = "MaxScanRows=0"
        s5 = "CharacterSet=ANSI"
        srOutput.WriteLine(s1 & vbCr & s2 & vbCr & s3 & vbCr & s4 & vbCr & s5)
        srOutput.Close()
        fsOutput.Close()
    End Sub
#End Region
End Class
