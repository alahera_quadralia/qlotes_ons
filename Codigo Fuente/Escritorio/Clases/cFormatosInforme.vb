﻿Imports CrystalDecisions.CrystalReports.Engine
Imports System.IO

''' <summary>
''' Obtiene información acerca de cada informe
''' </summary>
Public Class cInforme
    Public Property Ruta As String
    Public Property Nombre As String
    Private Property _Documento As ReportDocument = Nothing
    Public Property CarpetaInformes As String = String.Empty

    Public ReadOnly Property RutaCompleta As String
        Get
            Try
                Return My.Computer.FileSystem.CombinePath(CarpetaInformes, Ruta)
            Catch
                Return String.Empty
            End Try
        End Get
    End Property

    Public ReadOnly Property Documento As ReportDocument
        Get
            If _Documento IsNot Nothing Then Return _Documento

            If String.IsNullOrEmpty(RutaCompleta) OrElse Not My.Computer.FileSystem.FileExists(RutaCompleta) Then
                Throw New Exception("El sistema no pudo encontrar la definición del informe " & RutaCompleta)
                Return Nothing
            End If

            Dim Resultado As New ReportDocument
            Dim Contador As Integer = 0

            Do
                Resultado.Load(RutaCompleta)
                System.Threading.Thread.Sleep(200)
                Contador += 1
            Loop While Not Resultado.IsLoaded AndAlso Contador < 10

            If Resultado Is Nothing Then Throw New Exception("El sistema no pudo cargar la definición del informe " & RutaCompleta)
            _Documento = Resultado
            Return Resultado
        End Get
    End Property

    Public ReadOnly Property Self As cInforme
        Get
            Return Me
        End Get
    End Property
End Class

''' <summary>
''' Gestor de los diferentes tipos de formatos que existen tanto para las diferentes categorias
''' </summary>
Public Class cFormatosInforme
    Private _DirectorioRaiz As String = String.Empty
    Public Property Informes As New SortedDictionary(Of String, SortedList(Of String, cInforme))    ' Diccionario donde se catalogan los informes según su categoría (nombre carpeta)

    ''' <summary>
    '''  Escanea el directorio de los informes y los cataloga en función de su carpeta
    ''' </summary>
    Public Sub Init(ByVal DirectorioRaiz As String)
        _DirectorioRaiz = DirectorioRaiz
        Informes = New SortedDictionary(Of String, SortedList(Of String, cInforme))

        ' Validaciones
        If String.IsNullOrEmpty(DirectorioRaiz) Then Exit Sub
        If Not My.Computer.FileSystem.DirectoryExists(DirectorioRaiz) Then Exit Sub

        For Each Subdirectorio As DirectoryInfo In My.Computer.FileSystem.GetDirectoryInfo(DirectorioRaiz).GetDirectories()
            ' Recorro los archivos de cada carpeta y los catalogo
            For Each Archivo As FileInfo In Subdirectorio.GetFiles("*.rpt", SearchOption.TopDirectoryOnly)
                Dim Informe As cInforme = ObtenerInforme(Archivo.FullName)
                Informe.Ruta = Subdirectorio.Name & "\" & Archivo.Name
                If Informe IsNot Nothing Then AnhadirElemento(Subdirectorio.Name, Informe)
            Next
        Next
    End Sub

    ''' <summary>
    ''' Obtiene la información necesaria del documento que se le pasa como parámetro
    ''' </summary>
    ''' <param name="RutaFichero">Ruta del fichero RPT del que desamos obtener la información</param>
    Private Function ObtenerInforme(ByVal RutaFichero As String) As cInforme
        If String.IsNullOrEmpty(RutaFichero) Then Return Nothing
        If Not My.Computer.FileSystem.FileExists(RutaFichero) Then Return Nothing

        Dim Resultado As New cInforme
        Try
            Resultado.Nombre = My.Computer.FileSystem.GetFileInfo(RutaFichero).Name.Replace(".rpt", "")
        Catch ex As Exception
            Resultado = Nothing
        End Try

        Return Resultado
    End Function

    ''' <summary>
    ''' Añade un elmento al diccionario, verificando que exista la clave
    ''' </summary>
    Private Sub AnhadirElemento(ByVal Clave As String, ByVal Valor As cInforme)
        If Informes Is Nothing Then Exit Sub
        If Not Informes.ContainsKey(Clave) Then Informes(Clave) = New SortedList(Of String, cInforme)

        ' Añado el elemento
        Valor.CarpetaInformes = _DirectorioRaiz
        ' Valor.Ruta = String.Format("{0}\{1}.rpt", Clave, Valor.Nombre)
        Informes(Clave).Add(Valor.Nombre, Valor)
    End Sub

    ''' <summary>
    ''' Obtiene el informe con el nombre especificado
    ''' </summary>
    Public Function obtenerInforme(ByVal Ruta As String, ByVal Categoria As String) As cInforme
        If Not Informes.ContainsKey(Categoria) Then Return Nothing

        For Each Informe In Informes(Categoria)
            If Informe.Value.Ruta.EndsWith(Ruta, StringComparison.OrdinalIgnoreCase) Then Return Informe.Value
        Next

        ' No encontramos el informe
        Return Nothing
    End Function
End Class
