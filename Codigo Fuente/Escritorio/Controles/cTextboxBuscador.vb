﻿Public Class cTextboxBuscador
    Private _Item As Object = Nothing

    Public Overridable Property FormularioBusqueda As String = Nothing
    Public Overridable Property DisplayMember As String = String.Empty
    Public Overridable Property ValueMember As String = String.Empty
    Public Property UseUpperCase As Boolean = True ' Uso de mayuscula
    Public Property UseOnlyNumbers As Boolean = False ' Solo numeros    

    Public Event ItemChanged()
    Public Event ValueTextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

#Region " PROPIEDADES "
    Public Property Item As Object
        Get
            Return _Item
        End Get
        Set(ByVal value As Object)
            Dim RegistroValido As Boolean = False
            Dim RegistroAntiguo As Object = _Item

            ' Discrimino si viene una lista (me quedo con el primer elemento) o un único elemento
            If TypeOf (value) Is IEnumerable Then
                RegistroValido = value IsNot Nothing AndAlso value.count > 0
                If RegistroValido Then
                    _Item = value(0)
                End If
            Else
                _Item = value
                RegistroValido = Item IsNot Nothing
            End If

            ' Verifico si el registro es válido
            If Not RegistroValido Then
                _Item = Nothing
                lblDatosItem.Text = String.Empty
                txtCodigo.Text = String.Empty
                If Not iguales(RegistroAntiguo, _Item) Then RaiseEvent ItemChanged()
                Exit Property
            End If

            Dim textoLabel As String = String.Empty

            ' Obtengo el valor correcto para mostrar
            If String.IsNullOrEmpty(DisplayMember) Then
                textoLabel = _Item.ToString
            Else
                Try
                    textoLabel = CallByName(_Item, DisplayMember, CallType.Get, Nothing)
                Catch ex As Exception
                    textoLabel = _Item.ToString
                End Try
            End If

            ' Muestro el valor
            If UseUpperCase Then textoLabel = textoLabel.ToUpper
            lblDatosItem.Text = textoLabel

            ' Obtengo el valor del codigo
            Try
                txtCodigo.Text = CallByName(_Item, ValueMember, CallType.Get, Nothing)
            Catch ex As Exception
                txtCodigo.Text = _Item.ToString
            End Try

            If Not iguales(RegistroAntiguo, _Item) Then RaiseEvent ItemChanged()
        End Set
    End Property

    Public Property Validar As Boolean = True

    Public Property DisplayText As String
        Get
            Return lblDatosItem.Text
        End Get
        Set(ByVal value As String)
            If UseUpperCase Then value = value.ToUpper
            lblDatosItem.Text = value
        End Set
    End Property

    Public Property ValueText As String
        Get
            Return txtCodigo.Text
        End Get
        Set(ByVal value As String)
            If UseUpperCase Then value = value.ToUpper
            txtCodigo.Text = value
        End Set
    End Property

    Public Property DescripcionVisible As Boolean
        Get
            Return lblDatosItem.Visible
        End Get
        Set(ByVal value As Boolean)
            lblDatosItem.Visible = value

            If value Then
                tblOrganizador.ColumnStyles(0).SizeType = SizeType.AutoSize

                tblOrganizador.ColumnStyles(2).Width = 100
                tblOrganizador.ColumnStyles(2).SizeType = SizeType.Percent
            Else
                tblOrganizador.ColumnStyles(2).SizeType = SizeType.AutoSize

                tblOrganizador.ColumnStyles(0).Width = 100
                tblOrganizador.ColumnStyles(0).SizeType = SizeType.Percent
            End If
        End Set
    End Property

    Public Property DescripcionReadOnly As Boolean
        Get
            Return txtCodigo.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            txtCodigo.ReadOnly = value
        End Set
    End Property

#End Region
#Region " BUSQUEDA POR BUSCADOR "
    ''' <summary>
    ''' Busca un registro y lo carga en el formulario
    ''' </summary>
    Private Sub AbrirBuscador(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        If String.IsNullOrEmpty(FormularioBusqueda) Then Exit Sub

        Dim Tipo As Type = Type.GetType(FormularioBusqueda)
        Dim Existen As Boolean = True
        Try
            Existen = Tipo.GetMethod("Existen").Invoke(Nothing, New Object() {getContext()})
        Catch ex As Exception
        End Try
        If Existen Then
            Dim formulario As Object = Quadralia.Formularios.ObtenerFormularioPorTipo(FormularioBusqueda, getContext)
            Quadralia.Formularios.AutoTabular(formulario)
            If formulario.ShowDialog() = DialogResult.OK Then Item = formulario.Items
        End If
    End Sub
#End Region
#Region " BUSQUEDA POR CODIGO "
    Private Sub txtCodigo_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCodigo.KeyDown, lblDatosItem.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.Handled = True
            LocalizarRegistro()
        ElseIf e.KeyCode = Keys.F3 Then
            e.Handled = True
            btnBuscar.PerformClick()
        End If
    End Sub

    Private Sub BuscarCodigo(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtCodigo.Validating, lblDatosItem.Validating
        LocalizarRegistro()
    End Sub

    Private Sub LocalizarRegistro()
        If Not Validar Then Exit Sub

        Try
            Dim Tipo As Type = Type.GetType(FormularioBusqueda)
            Item = Tipo.GetMethod("BuscarPorCodigo").Invoke(Nothing, New Object() {getContext(), Item.id})
        Catch ex As Exception
            Item = Nothing
        End Try
    End Sub
#End Region
#Region " LIMPIEZA "
    Public Sub Clear()
        Item = Nothing
    End Sub
#End Region
#Region " RESTO DE FUNCIONES "
    Private Function getContext() As Entidades
        Dim Padre As Object = Me.ParentForm()
        Return Padre.contexto
    End Function

    Private Function iguales(ByVal Obj1 As Object, ByVal obj2 As Object) As Boolean
        ' Obtengo los valores correcto
        If TypeOf (Obj1) Is IEnumerable Then
            If Obj1.count > 0 Then
                Obj1 = Obj1(0)
            Else
                Obj1 = Nothing
            End If
        End If

        If TypeOf (obj2) Is IEnumerable Then
            If obj2.count > 0 Then
                obj2 = obj2(0)
            Else
                obj2 = Nothing
            End If
        End If

        If Obj1 Is Nothing AndAlso obj2 IsNot Nothing Then Return False
        If obj2 Is Nothing AndAlso Obj1 IsNot Nothing Then Return False
        If Obj1 Is Nothing AndAlso obj2 Is Nothing Then Return True
        If Not Obj1.GetType.Equals(obj2.GetType) Then Return False
        Return Obj1.Equals(obj2)
    End Function
#End Region
#Region " VALIDADORES "
    ''' <summary>
    ''' Evita que se puedan introducir caracteres numericos 
    ''' </summary>
    Private Sub txtCodigo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCodigo.KeyPress
        If UseOnlyNumbers Then Quadralia.Validadores.QuadraliaValidadores.SoloNumeros(sender, e)
    End Sub
#End Region

    Private Sub cTextboxBuscador_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ListaObtenida As String() = Nothing
        Try
            Dim Tipo As Type = Type.GetType(FormularioBusqueda)
            ListaObtenida = Tipo.GetMethod("ObetenerCodigos").Invoke(Nothing, New Object() {getContext()})
        Catch ex As Exception
        End Try

        If ListaObtenida IsNot Nothing Then
            Dim LaLista As New AutoCompleteStringCollection
            LaLista.AddRange(ListaObtenida)
            txtCodigo.AutoCompleteCustomSource = LaLista
        End If
    End Sub

    ''' <summary>
    ''' Lanza el evento del text changed
    ''' </summary>
    Private Sub LanzarEventoTextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCodigo.TextChanged
        RaiseEvent ValueTextChanged(Me, e)
    End Sub
End Class
