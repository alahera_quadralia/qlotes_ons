﻿Public MustInherit Class cFicha
    Inherits System.Windows.Forms.UserControl
#Region " DECLARACIONES "
    Private _contexto As Entidades = Nothing
#Region " EVENTOS "
    Public Event ActivoCambiado(ByVal sender As cFicha, ByVal e As EventArgs)
    Public Event DobleClick(ByVal sender As cFicha, ByVal e As EventArgs)
    Public Event PrincipalCambiado(ByVal sender As cFicha, ByVal Marcado As Boolean)
#End Region
#End Region
#Region " PROPIEDADES "
    MustOverride ReadOnly Property fichaSeleccionada As Boolean
    MustOverride ReadOnly Property fichaPrincipal As Boolean
    MustOverride ReadOnly Property fichaTipo As Object
    MustOverride Property fichaActiva() As Boolean
    MustOverride Property Item As Object


    Public Property Contexto As Entidades
        Get
            If _contexto Is Nothing Then _contexto = cDAL.Instancia.getContext
            Return _contexto
        End Get
        Set(ByVal value As Entidades)
            _contexto = value
        End Set
    End Property
#End Region
#Region " METODOS PRIVADOS "
    Protected Sub LanzarEventoDobleClick(ByRef e As EventArgs)
        RaiseEvent DobleClick(Me, e)
    End Sub

    Protected Sub LanzarEventoActivoCambiado(ByRef e As EventArgs)
        RaiseEvent ActivoCambiado(Me, e)
    End Sub

    Protected Sub LanzarEventoPrincipalCambiado(ByVal e As Boolean)
        RaiseEvent PrincipalCambiado(Me, e)
    End Sub
#End Region
#Region " ACCIONES ELIMINACION / MODIFICACION / AGREGAR "
    Public Sub Eliminar()
        ' Pido confirmación y elimino el registro
        If Quadralia.Aplicacion.MostrarMessageBox("¿Está seguro de que quiere eliminar el registro seleccionado?", "Confirmar Eliminación", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
            Contexto.DeleteObject(Item)
            Visible = False
        End If
    End Sub

    Public MustOverride Function obtenerFormularioEdicion() As IFormularioItem
    Public MustOverride Sub CambiarPrincipal()
#End Region
End Class
