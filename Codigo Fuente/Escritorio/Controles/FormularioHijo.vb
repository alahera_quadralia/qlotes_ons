﻿Imports System.Net.Mail

Public Class FormularioHijo
    Inherits ComponentFactory.Krypton.Toolkit.KryptonForm

#Region " METODOS QUE DEBE SOBRESCRIBIR LA CLASE HIJA "
    ''' <summary>
    ''' Tab del ribbon asociado al formulario
    ''' </summary>
    Public Overridable ReadOnly Property Tab() As ComponentFactory.Krypton.Ribbon.KryptonRibbonTab
        Get
            Return Nothing
        End Get
    End Property

    Public Overridable Property Estado() As Quadralia.Enumerados.EstadoFormulario
        Get
            Return Nothing
        End Get
        Set(ByVal value As Quadralia.Enumerados.EstadoFormulario)

        End Set
    End Property

    ''' <summary>
    ''' Función que se ejecutará cuando el usuario haga click en los botones de guardar del ribbon
    ''' </summary>
    ''' <param name="MostrarMensaje">Indica si mostraremos un mensaje con el resultado de la función</param>
    Public Overridable Function Guardar(Optional ByVal MostrarMensaje As Boolean = True) As Boolean
        Return True
    End Function

    ''' <summary>
    ''' Función que se ejecutará cuando el usuario haga click en el botón de nuevo registro
    ''' </summary>
    Public Overridable Sub Nuevo()

    End Sub

    ''' <summary>
    ''' Función que se ejecutará cuando el usuario haga click en el botón de modificar
    ''' </summary>
    Public Overridable Sub Modificar()

    End Sub

    ''' <summary>
    ''' Función que se ejecutará cuando el usuario haga click en el botón de eliminar
    ''' </summary>
    Public Overridable Sub Eliminar()

    End Sub

    ''' <summary>
    ''' Función que se ejecutará cuando el usuario haga click en el botón de cancelar
    ''' </summary>
    Public Overridable Sub Cancelar()

    End Sub

    ''' <summary>
    ''' Función que se ejecutará cuando el usuario haga click en el botón de cerrar
    ''' </summary>
    Public Overridable Sub Cerrar()
        Me.Close()
    End Sub
#End Region
#Region " INTERACTUAR CON EL RIBBON "
    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)

        If Me.ParentForm IsNot Nothing AndAlso Me.ParentForm Is frmPrincipal Then
            Dim FormularioPadre As frmPrincipal = Me.ParentForm

            ' Añado mi pestaña al formulario padre
            If Tab IsNot Nothing Then
                Tab.Visible = True
            End If
        End If
    End Sub

    Protected Overrides Sub OnActivated(ByVal e As System.EventArgs)
        MyBase.OnActivated(e)

        If Me.ParentForm IsNot Nothing Then
            Dim FormularioPadre As frmPrincipal = Me.ParentForm

            FormularioPadre.mnuRapidoGuardar.Enabled = (Estado <> Quadralia.Enumerados.EstadoFormulario.Espera)

            If Me.ParentForm Is frmPrincipal AndAlso Tab IsNot Nothing Then
                Tab.Visible = True
                FormularioPadre.rbbPrincipal.SelectedTab = Tab
            End If

            ' Refresco el estado para sincronizar el ribbon
            Me.Estado = Me.Estado
        End If
    End Sub
#End Region

    Public Overridable Sub ControlarCierre(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            If e.CloseReason = CloseReason.TaskManagerClosing OrElse e.CloseReason = CloseReason.WindowsShutDown Then Exit Sub
            If Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera Then Exit Sub
            If Quadralia.Aplicacion.SalidaForzada Then Exit Sub

            If Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera AndAlso Quadralia.Aplicacion.MostrarMessageBox(My.Resources.CambiosPendientes, My.Resources.TituloCambiosPendientes, MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                e.Cancel = True
            End If
        Catch ex As Exception
        Finally
            If Not e.Cancel Then
                If Me.ParentForm IsNot Nothing AndAlso Me.ParentForm Is frmPrincipal Then
                    Dim FormularioPadre As frmPrincipal = Me.ParentForm
                    Dim OcultarTab As Boolean = True

                    ' Miro si tengo que ocultar el tab
                    If Tab IsNot Nothing Then
                        For Each Formulario As Form In FormularioPadre.MdiChildren
                            If TypeOf (Formulario) Is FormularioHijo AndAlso DirectCast(Formulario, FormularioHijo).Tab Is Me.Tab AndAlso Formulario IsNot Me Then
                                OcultarTab = False
                            End If
                        Next

                        Me.Tab.Visible = Not OcultarTab
                    End If

                    FormularioPadre.mnuRapidoGuardar.Enabled = False
                    FormularioPadre.rbbPrincipal.SelectedTab = frmPrincipal.tabGeneral
                End If

                Me.Hide()
            End If
        End Try
    End Sub

    Private Sub InitializeComponent()
        Me.SuspendLayout()
        '
        'FormularioHijo
        '
        Me.ClientSize = New System.Drawing.Size(292, 266)
        Me.Name = "FormularioHijo"
        Me.ResumeLayout(False)

    End Sub
End Class
