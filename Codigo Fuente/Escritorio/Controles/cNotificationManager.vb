﻿Imports System.Runtime.InteropServices
Public Class cNotificationManager
    Public Class OcultarNoticacion
        Public Enum TipoOcultacion
            Ninguno
            Todos
            MismoTipo
            Temporal
        End Enum

        Private _hash64Imagen As String = String.Empty
        Public Property FechaFinAviso As Nullable(Of DateTime)
        Public Property Tipo As TipoOcultacion = TipoOcultacion.Ninguno
        Public WriteOnly Property Icono As Image
            Set(ByVal value As Image)
                If value Is Nothing Then
                    _hash64Imagen = String.Empty
                Else
                    _hash64Imagen = Quadralia.Imagenes.ImagenABase64(value).ToUpper
                End If
            End Set
        End Property

        Public Function TiempoTranscurrido() As Boolean
            If Not FechaFinAviso.HasValue Then Return False
            Return FechaFinAviso.HasValue AndAlso DateTime.Now >= FechaFinAviso.Value
        End Function

        Public Function EsMismoTipo(ByVal Formulario As frmNuevoAviso) As Boolean
            If Formulario Is Nothing Then Return False
            If String.IsNullOrEmpty(_hash64Imagen) AndAlso Formulario.picImagen.Image Is Nothing Then Return True

            Dim _hash64Formulario = Quadralia.Imagenes.ImagenABase64(Formulario.picImagen.Image).ToUpper
            Return _hash64Formulario = _hash64Imagen
        End Function
    End Class

#Region " APIS "
#Region "API Allways On Top"
    <DllImport("user32.dll")> _
    Private Shared Function SetWindowPos(ByVal hWnd As IntPtr, ByVal hWndInsertAfter As IntPtr, ByVal X As Integer, ByVal Y As Integer, ByVal cx As Integer, ByVal cy As Integer, _
                                            ByVal uFlags As UInteger) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

    Private Declare Function apiSHAppBarMessage Lib "shell32" Alias "SHAppBarMessage" (ByVal dwMessage As Int32, ByRef pData As APPBARDATA) As Int32

    Shared ReadOnly HWND_TOPMOST As New IntPtr(-1)
    Shared ReadOnly HWND_NOTOPMOST As New IntPtr(-2)
    Shared ReadOnly HWND_TOP As New IntPtr(0)
    Shared ReadOnly HWND_BOTTOM As New IntPtr(1)
    Const SWP_NOSIZE As UInt32 = &H1
    Const SWP_NOMOVE As UInt32 = &H2
    Const TOPMOST_FLAGS As UInt32 = SWP_NOMOVE Or SWP_NOSIZE


    ' Llamada a la API. Meter en el load
    'SetWindowPos(Me.Handle, HWND_TOPMOST, 0, 0, 0, 0, TOPMOST_FLAGS)
#End Region
#Region "API Posición Barra Tareas"
    Const ABS_AUTOHIDE As Int32 = 1
    Const ABS_ONTOP As Int32 = 2
    Const ABM_NEW As Int32 = 0
    Const ABM_REMOVE As Int32 = 1
    Const ABM_QUERYPOS As Int32 = 2
    Const ABM_SETPOS As Int32 = 3
    Const ABM_GETSTATE As Int32 = 4
    Const ABM_GETTASKBARPOS As Int32 = 5
    Const ABM_ACTIVATE As Int32 = 6
    Const ABM_GETAUTOHIDEBAR As Int32 = 7
    Const ABM_SETAUTOHIDEBAR As Int32 = 8
    Const ABM_WINDOWPOSCHANGED As Int32 = 9

    Private Structure RECT
        Public rLeft, rTop, rRight, rBottom As Int32
    End Structure

    Private Structure APPBARDATA
        Public cbSize, hwnd, uCallbackMessage, uEdge As Int32, rc As RECT, lParam As Int32
    End Structure
#End Region
#End Region
#Region " DECLARACIONES "
    Private Shared _instance As cNotificationManager = Nothing
    Private _Ventanas As New List(Of frmNuevoAviso)
    Private _Ocultaciones As New List(Of OcultarNoticacion)

    Public Enum TipoVentana
        Aviso_Cliente
        Aviso_Proveedor
        Aviso
        Ninguno
    End Enum
#End Region
#Region " PROPIEDADES "
    Public Shared ReadOnly Property Instance As cNotificationManager
        Get
            If _instance Is Nothing Then _instance = New cNotificationManager
            Return _instance
        End Get
    End Property

    Private ReadOnly Property Ventanas As List(Of frmNuevoAviso)
        Get
            If _Ventanas Is Nothing Then _Ventanas = New List(Of frmNuevoAviso)
            Return _Ventanas
        End Get
    End Property

    Private ReadOnly Property Ocultaciones As List(Of OcultarNoticacion)
        Get
            If _Ocultaciones Is Nothing Then _Ocultaciones = New List(Of OcultarNoticacion)
            Return _Ocultaciones
        End Get
    End Property
#End Region
#Region " MOSTRAR VENTANAS "
    Public Sub MostrarAviso(ByVal Ventana As frmNuevoAviso)
        Dim Mostrar As Boolean = True

        ' ¿El usuario indicó que no quiere más notificaciones?
        For i As Integer = Ocultaciones.Count - 1 To 0 Step -1
            Dim Ocult As OcultarNoticacion = Ocultaciones(i)

            ' Elimino los que no me interesan
            If (Ocult.Tipo = OcultarNoticacion.TipoOcultacion.Ninguno) OrElse Ocult.TiempoTranscurrido Then
                Ocultaciones.RemoveAt(i)
            Else
                ' Miro si tengo que mostrar el aviso
                If Ocult.Tipo = OcultarNoticacion.TipoOcultacion.Todos Then Mostrar = False
                If Ocult.Tipo = OcultarNoticacion.TipoOcultacion.MismoTipo AndAlso Ocult.EsMismoTipo(Ventana) Then Mostrar = False
                If Ocult.Tipo = OcultarNoticacion.TipoOcultacion.Temporal AndAlso Ocult.EsMismoTipo(Ventana) AndAlso Not Ocult.TiempoTranscurrido Then Mostrar = False
            End If
        Next

        If Not Mostrar Then Exit Sub

        ' ¿Tengo la ventana ya mostrada?
        If Ventanas.Contains(Ventana) Then Exit Sub

        ' Obtengo la posición en la que tengo que mostrar la ventana
        Ventanas.Add(Ventana)

        PosicionarVentana(Ventana)
        AddHandler Ventana.FormClosed, AddressOf VentanaCerrada
        Ventana.TiempoAviso = 5
        Ventana.Show()
    End Sub

    Public Sub MostrarAviso(ByVal Titulo As String, ByVal Mensaje As String, ByVal Imagen As Image)
        Dim Formulario As New frmNuevoAviso With {.Titulo = Titulo, .Cuerpo = Mensaje, .Imagen = Imagen}
        MostrarAviso(Formulario)
    End Sub

    Public Sub MostrarAviso(ByVal Titulo As String, ByVal Mensaje As String, ByVal tipo As TipoVentana)
        Dim Img As Image = Nothing
        Select Case tipo
            Case TipoVentana.Aviso_Cliente
                Img = My.Resources.Aviso_64
            Case TipoVentana.Aviso_Proveedor
                Img = My.Resources.Aviso_64
            Case TipoVentana.Aviso
                Img = My.Resources.Aviso_64
            Case Else
                Img = Nothing
        End Select

        ' Mostramos la pantalla de aviso
        MostrarAviso(Titulo, Mensaje, Img)
    End Sub

    ''' <summary>
    ''' Coloca la ventana en función de la barra de tareas del usuario
    ''' </summary>
    Private Sub PosicionarVentana(ByRef Ventana As frmNuevoAviso)
        If Not Ventanas.Contains(Ventana) Then Exit Sub

        Dim ABD As New APPBARDATA
        Dim VentanasAnteriores As Integer = Ventanas.IndexOf(Ventana) + 1
        Dim Punto As System.Drawing.Point = Nothing
        apiSHAppBarMessage(ABM_GETTASKBARPOS, ABD) 'Get the taskbar's position

        Ventana.StartPosition = FormStartPosition.Manual

        Select Case ABD.uEdge
            Case 0 ' Izquierda
                Punto = New System.Drawing.Point(ABD.rc.rRight, ABD.rc.rBottom - (Ventana.Height * VentanasAnteriores))
            Case 1 ' Superior
                Punto = New System.Drawing.Point(ABD.rc.rRight - Ventana.Width, ABD.rc.rBottom + (Ventana.Height * (VentanasAnteriores - 1)))
            Case 2 ' Derecha
                Punto = New System.Drawing.Point(ABD.rc.rLeft - Ventana.Width, ABD.rc.rBottom - (Ventana.Height * VentanasAnteriores))
            Case 3 ' Inferior
                Punto = New System.Drawing.Point(ABD.rc.rRight - Ventana.Width, ABD.rc.rTop - (Ventana.Height * VentanasAnteriores))
            Case Else
                Punto = New System.Drawing.Point(My.Computer.Screen.Bounds.Width - Ventana.Width, My.Computer.Screen.Bounds.Height - 40 - (Ventana.Height * VentanasAnteriores))
        End Select

        Ventana.Location = Punto
    End Sub
#End Region
#Region " CERRAR VENTANAS "
    Public Sub VentanaCerrada(ByVal Ventana As frmNuevoAviso, ByVal e As FormClosedEventArgs)
        If Ventana.Resultado IsNot Nothing Then Ocultaciones.Add(Ventana.Resultado)
        If Not Ventanas.Contains(Ventana) Then Exit Sub


        ' Si es el último lo elimino para aprovechar ese hueco para mostrar un elemento nuevo
        If Ventanas.IndexOf(Ventana) = Ventanas.Count - 1 Then
            Ventanas.Remove(Ventana)
        Else
            Ventanas(Ventanas.IndexOf(Ventana)) = Nothing
        End If

        ' Miro si el listado de notificaciones está vacío
        For i As Integer = 0 To Ventanas.Count - 1
            If Ventanas(i) IsNot Nothing Then Exit Sub
        Next

        _Ventanas = Nothing
    End Sub
#End Region
End Class
