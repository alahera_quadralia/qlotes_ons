﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class cTextboxBuscador
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtCodigo = New ComponentFactory.Krypton.Toolkit.KryptonTextBox()
        Me.btnBuscar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.lblDatosItem = New ComponentFactory.Krypton.Toolkit.KryptonTextBox()
        Me.tblOrganizador = New System.Windows.Forms.TableLayoutPanel()
        Me.tblOrganizador.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtCodigo
        '
        Me.txtCodigo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtCodigo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtCodigo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtCodigo.Location = New System.Drawing.Point(0, 0)
        Me.txtCodigo.Margin = New System.Windows.Forms.Padding(0)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(95, 20)
        Me.txtCodigo.StateCommon.Border.DrawBorders = CType(((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top Or ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) _
                    Or ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left), ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)
        Me.txtCodigo.TabIndex = 0
        '
        'btnBuscar
        '
        Me.btnBuscar.Location = New System.Drawing.Point(95, 0)
        Me.btnBuscar.Margin = New System.Windows.Forms.Padding(0)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(25, 20)
        Me.btnBuscar.StateCommon.Border.DrawBorders = CType((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top Or ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) _
                    Or ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) _
                    Or ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right), ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)
        Me.btnBuscar.StateCommon.Border.Rounding = 0
        Me.btnBuscar.TabIndex = 2
        Me.btnBuscar.Values.Image = Global.Escritorio.My.Resources.Buscar_16
        Me.btnBuscar.Values.Text = ""
        '
        'lblDatosItem
        '
        Me.lblDatosItem.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDatosItem.Enabled = False
        Me.lblDatosItem.Location = New System.Drawing.Point(125, 0)
        Me.lblDatosItem.Margin = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.lblDatosItem.Name = "lblDatosItem"
        Me.lblDatosItem.ReadOnly = True
        Me.lblDatosItem.Size = New System.Drawing.Size(273, 20)
        Me.lblDatosItem.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.lblDatosItem.StateCommon.Content.Color1 = System.Drawing.Color.Black
        Me.lblDatosItem.StateDisabled.Back.Color1 = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.lblDatosItem.StateDisabled.Content.Color1 = System.Drawing.Color.Black
        Me.lblDatosItem.TabIndex = 0
        Me.lblDatosItem.TabStop = False
        '
        'tblOrganizador
        '
        Me.tblOrganizador.ColumnCount = 3
        Me.tblOrganizador.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblOrganizador.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblOrganizador.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblOrganizador.Controls.Add(Me.btnBuscar, 1, 0)
        Me.tblOrganizador.Controls.Add(Me.lblDatosItem, 2, 0)
        Me.tblOrganizador.Controls.Add(Me.txtCodigo, 0, 0)
        Me.tblOrganizador.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblOrganizador.Location = New System.Drawing.Point(0, 0)
        Me.tblOrganizador.Name = "tblOrganizador"
        Me.tblOrganizador.RowCount = 1
        Me.tblOrganizador.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizador.Size = New System.Drawing.Size(398, 20)
        Me.tblOrganizador.TabIndex = 3
        '
        'cTextboxBuscador
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.tblOrganizador)
        Me.Name = "cTextboxBuscador"
        Me.Size = New System.Drawing.Size(398, 20)
        Me.tblOrganizador.ResumeLayout(False)
        Me.tblOrganizador.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtCodigo As ComponentFactory.Krypton.Toolkit.KryptonTextBox
    Friend WithEvents btnBuscar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents lblDatosItem As ComponentFactory.Krypton.Toolkit.KryptonTextBox
    Friend WithEvents tblOrganizador As System.Windows.Forms.TableLayoutPanel

End Class
