﻿Public Class VisorImagenes
#Region " PROPIEDADES "
    Public Property Titulo As String
        Get
            Return hdrThumbnail.ValuesPrimary.Heading
        End Get
        Set(value As String)
            hdrThumbnail.ValuesPrimary.Heading = value
        End Set
    End Property

    Public Overloads Property Enabled()
        Get
            Return picImagen.Enabled
        End Get
        Set(ByVal value)
            hdrThumbnail.Enabled = value
            picImagen.Enabled = value
        End Set
    End Property

    Public Property Item() As Image
        Get
            Return picImagen.Image
        End Get
        Set(ByVal value As Image)
            picImagen.Image = value
        End Set
    End Property
#End Region
#Region " METODOS PRIVADOS "
	''' <summary>
	''' Carga una imagen en el control
	''' </summary>
	Private Sub CargarImagen(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAbrirImagen.Click
		Dim opfImagenes As New OpenFileDialog

		With opfImagenes
			.CheckFileExists = True
			.CheckPathExists = True
			.Filter = "Archivos de Imagen|*.jpg;*.jpeg;*.gif;*.bmp;*.png;|Todos los archivos|*.*"
		End With

		If opfImagenes.ShowDialog <> Windows.Forms.DialogResult.OK Then Exit Sub
        If Not IO.File.Exists(opfImagenes.FileName) Then Exit Sub

        Try
            Item = Image.FromFile(opfImagenes.FileName)
        Catch ex As Exception
            Item = Nothing
        End Try
    End Sub

	''' <summary>
	''' Elimina el visor que desencadena el evento
	''' </summary>
	Private Sub EliminarImagen(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEliminarImagen.Click
        Clear()
    End Sub
#End Region
#Region " METODOS PUBLICOS "
    Public Sub Clear()
        picImagen.Image = Nothing
        picImagen.ImageLocation = String.Empty
        hdrThumbnail.ValuesPrimary.Heading = "Imagen"
    End Sub
#End Region
End Class