﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class VisorImagenes
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.hdrThumbnail = New ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup()
        Me.cmdAbrirImagen = New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup()
        Me.cmdEliminarImagen = New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup()
        Me.picImagen = New System.Windows.Forms.PictureBox()
        CType(Me.hdrThumbnail,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.hdrThumbnail.Panel,System.ComponentModel.ISupportInitialize).BeginInit
        Me.hdrThumbnail.Panel.SuspendLayout
        Me.hdrThumbnail.SuspendLayout
        CType(Me.picImagen,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'hdrThumbnail
        '
        Me.hdrThumbnail.ButtonSpecs.AddRange(New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup() {Me.cmdAbrirImagen, Me.cmdEliminarImagen})
        Me.hdrThumbnail.Dock = System.Windows.Forms.DockStyle.Fill
        Me.hdrThumbnail.HeaderStylePrimary = ComponentFactory.Krypton.Toolkit.HeaderStyle.Secondary
        Me.hdrThumbnail.HeaderVisibleSecondary = false
        Me.hdrThumbnail.Location = New System.Drawing.Point(0, 0)
        Me.hdrThumbnail.Name = "hdrThumbnail"
        '
        'hdrThumbnail.Panel
        '
        Me.hdrThumbnail.Panel.Controls.Add(Me.picImagen)
        Me.hdrThumbnail.Size = New System.Drawing.Size(122, 147)
        Me.hdrThumbnail.StateCommon.Border.DrawBorders = CType((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top Or ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom)  _
            Or ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left)  _
            Or ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right),ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)
        Me.hdrThumbnail.TabIndex = 20
        Me.hdrThumbnail.ValuesPrimary.Heading = "Imagen"
        Me.hdrThumbnail.ValuesPrimary.Image = Nothing
        '
        'cmdAbrirImagen
        '
        Me.cmdAbrirImagen.Image = Global.Escritorio.My.Resources.Resources.AbrirImagen_16
        Me.cmdAbrirImagen.UniqueName = "9869F126B54D46F7C590A6498EBC9271"
        '
        'cmdEliminarImagen
        '
        Me.cmdEliminarImagen.Image = Global.Escritorio.My.Resources.Resources.EliminarImagen_16
        Me.cmdEliminarImagen.UniqueName = "DC79390452F4432B2E82331C8AE727F3"
        '
        'picImagen
        '
        Me.picImagen.BackColor = System.Drawing.Color.Transparent
        Me.picImagen.Dock = System.Windows.Forms.DockStyle.Fill
        Me.picImagen.Location = New System.Drawing.Point(0, 0)
        Me.picImagen.Name = "picImagen"
        Me.picImagen.Size = New System.Drawing.Size(120, 120)
        Me.picImagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picImagen.TabIndex = 18
        Me.picImagen.TabStop = false
        '
        'VisorImagenes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.hdrThumbnail)
        Me.Name = "VisorImagenes"
        Me.Size = New System.Drawing.Size(122, 147)
        CType(Me.hdrThumbnail.Panel,System.ComponentModel.ISupportInitialize).EndInit
        Me.hdrThumbnail.Panel.ResumeLayout(false)
        CType(Me.hdrThumbnail,System.ComponentModel.ISupportInitialize).EndInit
        Me.hdrThumbnail.ResumeLayout(false)
        CType(Me.picImagen,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)

End Sub
	Friend WithEvents hdrThumbnail As ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup
	Friend WithEvents cmdAbrirImagen As ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup
	Friend WithEvents cmdEliminarImagen As ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup
    Friend WithEvents picImagen As System.Windows.Forms.PictureBox

End Class
