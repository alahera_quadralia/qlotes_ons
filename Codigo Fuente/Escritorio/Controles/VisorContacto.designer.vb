﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class VisorContacto
    Inherits Escritorio.cFicha

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(VisorContacto))
        Me.hdrVisor = New ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup()
        Me.btnPrincipal = New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup()
        Me.btnEliminar = New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup()
        Me.tblOrganizador = New System.Windows.Forms.TableLayoutPanel()
        Me.lblEmail = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.pObservaciones = New ComponentFactory.Krypton.Toolkit.KryptonPanel()
        Me.lblNotas = New ComponentFactory.Krypton.Toolkit.KryptonWrapLabel()
        Me.lblTelefono = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblMovil = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblFax = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblPuesto = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        CType(Me.hdrVisor, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.hdrVisor.Panel.SuspendLayout()
        Me.hdrVisor.SuspendLayout()
        Me.tblOrganizador.SuspendLayout()
        CType(Me.pObservaciones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pObservaciones.SuspendLayout()
        Me.SuspendLayout()
        '
        'hdrVisor
        '
        Me.hdrVisor.ButtonSpecs.AddRange(New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup() {Me.btnPrincipal, Me.btnEliminar})
        resources.ApplyResources(Me.hdrVisor, "hdrVisor")
        Me.hdrVisor.GroupBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.GridHeaderRowSheet
        Me.hdrVisor.HeaderStylePrimary = ComponentFactory.Krypton.Toolkit.HeaderStyle.Form
        Me.hdrVisor.Name = "hdrVisor"
        '
        'hdrVisor.Panel
        '
        Me.hdrVisor.Panel.Controls.Add(Me.tblOrganizador)
        Me.hdrVisor.StateCommon.Border.Color1 = System.Drawing.Color.White
        Me.hdrVisor.StateCommon.Border.DrawBorders = CType((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top Or ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) _
                    Or ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) _
                    Or ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right), ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)
        Me.hdrVisor.StateCommon.Border.Width = 3
        Me.hdrVisor.StateCommon.HeaderPrimary.Border.Draw = ComponentFactory.Krypton.Toolkit.InheritBool.[False]
        Me.hdrVisor.StateCommon.HeaderPrimary.Border.DrawBorders = CType((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top Or ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) _
                    Or ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) _
                    Or ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right), ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)
        Me.hdrVisor.StateCommon.HeaderPrimary.Border.Width = 3
        Me.hdrVisor.StateCommon.HeaderPrimary.Content.LongText.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.hdrVisor.StateCommon.HeaderPrimary.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near
        Me.hdrVisor.StateCommon.HeaderPrimary.Content.ShortText.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.hdrVisor.StateCommon.HeaderSecondary.Border.Color1 = System.Drawing.Color.White
        Me.hdrVisor.StateCommon.HeaderSecondary.Border.ColorStyle = ComponentFactory.Krypton.Toolkit.PaletteColorStyle.Solid
        Me.hdrVisor.StateCommon.HeaderSecondary.Border.Draw = ComponentFactory.Krypton.Toolkit.InheritBool.[True]
        Me.hdrVisor.StateCommon.HeaderSecondary.Border.DrawBorders = CType(((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom Or ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) _
                    Or ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right), ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)
        Me.hdrVisor.StateCommon.HeaderSecondary.Border.Width = 3
        Me.hdrVisor.Tag = "In guild since {0}"
        Me.hdrVisor.ValuesPrimary.Heading = resources.GetString("hdrVisor.ValuesPrimary.Heading")
        Me.hdrVisor.ValuesPrimary.Image = CType(resources.GetObject("hdrVisor.ValuesPrimary.Image"), System.Drawing.Image)
        Me.hdrVisor.ValuesSecondary.Heading = resources.GetString("hdrVisor.ValuesSecondary.Heading")
        '
        'btnPrincipal
        '
        resources.ApplyResources(Me.btnPrincipal, "btnPrincipal")
        Me.btnPrincipal.Image = Global.Escritorio.My.Resources.Resources.Favorito_16
        Me.btnPrincipal.UniqueName = "AEC6924744414DB213A0CF2E42BC3BBB"
        '
        'btnEliminar
        '
        resources.ApplyResources(Me.btnEliminar, "btnEliminar")
        Me.btnEliminar.UniqueName = "E7EFC03984D343A5559F876823B4FB2F"
        '
        'tblOrganizador
        '
        Me.tblOrganizador.BackColor = System.Drawing.Color.Transparent
        resources.ApplyResources(Me.tblOrganizador, "tblOrganizador")
        Me.tblOrganizador.Controls.Add(Me.lblEmail, 0, 3)
        Me.tblOrganizador.Controls.Add(Me.pObservaciones, 0, 4)
        Me.tblOrganizador.Controls.Add(Me.lblTelefono, 1, 0)
        Me.tblOrganizador.Controls.Add(Me.lblMovil, 0, 1)
        Me.tblOrganizador.Controls.Add(Me.lblFax, 1, 1)
        Me.tblOrganizador.Controls.Add(Me.lblPuesto, 0, 0)
        Me.tblOrganizador.Name = "tblOrganizador"
        '
        'lblEmail
        '
        Me.tblOrganizador.SetColumnSpan(Me.lblEmail, 2)
        resources.ApplyResources(Me.lblEmail, "lblEmail")
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Values.Text = resources.GetString("lblEmail.Values.Text")
        '
        'pObservaciones
        '
        Me.tblOrganizador.SetColumnSpan(Me.pObservaciones, 2)
        Me.pObservaciones.Controls.Add(Me.lblNotas)
        resources.ApplyResources(Me.pObservaciones, "pObservaciones")
        Me.pObservaciones.Name = "pObservaciones"
        Me.pObservaciones.PanelBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.GridHeaderColumnList
        '
        'lblNotas
        '
        resources.ApplyResources(Me.lblNotas, "lblNotas")
        Me.lblNotas.ForeColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(91, Byte), Integer))
        Me.lblNotas.Name = "lblNotas"
        Me.lblNotas.StateCommon.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNotas.StateCommon.Hint = ComponentFactory.Krypton.Toolkit.PaletteTextHint.AntiAliasGridFit
        '
        'lblTelefono
        '
        resources.ApplyResources(Me.lblTelefono, "lblTelefono")
        Me.lblTelefono.Name = "lblTelefono"
        Me.lblTelefono.Values.Text = resources.GetString("lblTelefono.Values.Text")
        '
        'lblMovil
        '
        resources.ApplyResources(Me.lblMovil, "lblMovil")
        Me.lblMovil.Name = "lblMovil"
        Me.lblMovil.Values.Text = resources.GetString("lblMovil.Values.Text")
        '
        'lblFax
        '
        resources.ApplyResources(Me.lblFax, "lblFax")
        Me.lblFax.Name = "lblFax"
        Me.lblFax.Values.Text = resources.GetString("lblFax.Values.Text")
        '
        'lblPuesto
        '
        resources.ApplyResources(Me.lblPuesto, "lblPuesto")
        Me.lblPuesto.Name = "lblPuesto"
        Me.lblPuesto.Values.Text = resources.GetString("lblPuesto.Values.Text")
        '
        'VisorContacto
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.hdrVisor)
        Me.Name = "VisorContacto"
        Me.hdrVisor.Panel.ResumeLayout(False)
        CType(Me.hdrVisor, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrVisor.ResumeLayout(False)
        Me.tblOrganizador.ResumeLayout(False)
        Me.tblOrganizador.PerformLayout()
        CType(Me.pObservaciones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pObservaciones.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents hdrVisor As ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup
    Friend WithEvents btnEliminar As ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup
    Friend WithEvents tblOrganizador As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblTelefono As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblFax As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblEmail As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblMovil As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents btnPrincipal As ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup
    Friend WithEvents pObservaciones As ComponentFactory.Krypton.Toolkit.KryptonPanel
    Friend WithEvents lblPuesto As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblNotas As ComponentFactory.Krypton.Toolkit.KryptonWrapLabel
End Class
