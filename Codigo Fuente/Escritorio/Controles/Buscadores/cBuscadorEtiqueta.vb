﻿Public Class cBuscadorEtiqueta
    Inherits cTextboxBuscador

    Public Overrides Property FormularioBusqueda As String
        Get
            Return "Escritorio.frmSeleccionarEtiqueta"
        End Get
        Set(value As String)
        End Set
    End Property

    Public Overrides Property ValueMember As String
        Get
            Return "Codigo"
        End Get
        Set(value As String)
        End Set
    End Property

    Public Overrides Property DisplayMember As String
        Get
            Return "Descripcion"
        End Get
        Set(value As String)
        End Set
    End Property
End Class
