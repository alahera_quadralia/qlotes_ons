﻿Public Class cBuscadorLoteEntrada
    Inherits cTextboxBuscador

    Public Overrides Property FormularioBusqueda As String
        Get
            Return "Escritorio.frmSeleccionarLoteEntrada"
        End Get
        Set(value As String)
        End Set
    End Property

    Public Overrides Property ValueMember As String
        Get
            Return "codigo"
        End Get
        Set(value As String)
        End Set
    End Property

    Public Overrides Property DisplayMember As String
        Get
            Return "Descripcion"
        End Get
        Set(value As String)
        End Set
    End Property
End Class
