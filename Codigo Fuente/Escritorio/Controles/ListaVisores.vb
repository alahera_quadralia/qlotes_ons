﻿Imports System.ComponentModel

Public Class ListaVisores    
#Region " ENUMERADOS "
    Public Enum Tipos
        Direcciones
        Contactos
    End Enum
#End Region

#Region " DECLARACIONES "
    Private WithEvents _ListaElementos As IEnumerable = Nothing
    Private _UltimoCurrent As cFicha = Nothing
    Private _Enabled As Boolean = True
    Private _Contexto As Entidades = Nothing

    Public Event CurrentChanged()
#End Region

#Region " PROPIEDADES "
    ''' <summary>
    ''' Elementos de la colección
    ''' </summary>
    Public Property Items() As IEnumerable
        Get
            Return _ListaElementos
        End Get
        Set(ByVal value As IEnumerable)
            _ListaElementos = value
            Refrescar()
        End Set
    End Property

    Public Property Tipo As Tipos

    Public Overloads Property Enabled() As Boolean
        Get
            Return _Enabled
        End Get
        Set(ByVal value As Boolean)
            _Enabled = value

            For Each Visor As cFicha In flwFichas.Controls
                Visor.fichaActiva = value
            Next
        End Set
    End Property

    ''' <summary>
    ''' Devuelve el último elemento seleccionado
    ''' </summary>
    Public Property Current() As cFicha
        Get
            Dim Resultado As cFicha = Nothing

            For Each Visor As cFicha In flwFichas.Controls
                If Visor.fichaSeleccionada AndAlso Visor.Visible Then Resultado = Visor
            Next
            Return Resultado
        End Get
        Set(ByVal value As cFicha)
            If value Is Nothing AndAlso _UltimoCurrent Is Nothing Then Exit Property

            If value Is Nothing OrElse _UltimoCurrent Is Nothing Then
                _UltimoCurrent = value
                RaiseEvent CurrentChanged()
            ElseIf value IsNot _UltimoCurrent Then
                _UltimoCurrent = value
                RaiseEvent CurrentChanged()
            End If
        End Set
    End Property

    <Browsable(False)> _
    Public Property Contexto As Entidades
        Get
#If DEBUG Then
#Else
            If _Contexto Is Nothing Then _Contexto = cDAL.Instancia.getContext
#End If
            Return _Contexto
        End Get
        Set(ByVal value As Entidades)
            _Contexto = value
        End Set
    End Property

    Private ReadOnly Property Visor As cFicha
        Get
            Select Case Tipo
                Case Tipos.Contactos
                    Return New VisorContacto
                Case Tipos.Direcciones
                    Return New VisorDireccion
                Case Else
                    Return Nothing
            End Select
        End Get
    End Property
#End Region

#Region " AÑADIR ELEMENTOS "
    ''' <summary>
    ''' Muestra los elementos en el flowlayaout panel
    ''' </summary>
    Public Sub Refrescar()
        ' Elimino todos los datos antiguos
        flwFichas.Controls.Clear()

        If _ListaElementos Is Nothing Then Exit Sub

        ' Los vuelvo a meter
        For Each Registro In _ListaElementos
            Dim NuevoVisor As cFicha = Visor
            NuevoVisor.Item = Registro
            NuevoVisor.Enabled = Me._Enabled
            NuevoVisor.Contexto = Contexto

            AddHandler NuevoVisor.ActivoCambiado, AddressOf ControlarCambio
            AddHandler NuevoVisor.DobleClick, AddressOf EditarElemento
            AddHandler NuevoVisor.PrincipalCambiado, AddressOf ControlarPrincipal
            flwFichas.Controls.Add(NuevoVisor)
        Next
    End Sub

    ''' <summary>
    ''' Controla que solo haya un elemento marcado como principal
    ''' </summary>
    Private Sub ControlarPrincipal(ByVal Visor As cFicha, ByVal Valor As Boolean)
        If Valor Then
            For Each Item As cFicha In flwFichas.Controls
                If Item IsNot Visor AndAlso Item.fichaPrincipal AndAlso Item.fichaTipo = Visor.fichaTipo Then
                    Item.CambiarPrincipal()
                End If
            Next
        End If
    End Sub

    ''' <summary>
    ''' Edita el elemento
    ''' </summary>
    Public Sub EditarElemento(ByVal sender As cFicha, ByVal e As EventArgs)
        Dim Formulario As KryptonForm = sender.obtenerFormularioEdicion
        DirectCast(Formulario, IFormularioItem).Item = sender.Item
        Formulario.ShowDialog()

        Refrescar()
    End Sub
#End Region

#Region " METODOS PUBLICOS "
    ''' <summary>
    ''' Limpia todos los elementos del control
    ''' </summary>
    Public Sub Clear()
        ' Limpio los datos
        Items = Nothing
        flwFichas.Controls.Clear()
    End Sub
#End Region
#Region " RUTINA DE INICIO / FIN "
    ''' <summary>
    ''' Iniciazación del control
    ''' </summary>
    Private Sub Inicio(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        RaiseEvent CurrentChanged()
    End Sub
#End Region
#Region " RUTINAS DE FOCUS Y CONTROL DEL CURRENT "
    Private Sub ControlarCambio(ByVal sender As Object, ByVal e As EventArgs)
        On Error Resume Next
        Dim Control As cFicha = DirectCast(sender, cFicha)

        If Control.fichaSeleccionada Then
            Current = Control
        Else
            Current = Nothing
        End If
    End Sub
#End Region
End Class
