﻿Public Class VisorDireccion
    Inherits cFicha
    ' Inherits UserControl
#Region " DECLARACIONES "
    Private _Elemento As IDireccion

    Private _ColorNoFoco As System.Drawing.Color = System.Drawing.Color.White
    Private _ColorFavorito As System.Drawing.Color = System.Drawing.Color.FromArgb(255, 196, 16)
    Private _ColorFoco As System.Drawing.Color = System.Drawing.Color.FromArgb(121, 183, 72)
#End Region
#Region " PROPIEDADES "
    Public Overrides ReadOnly Property fichaSeleccionada As Boolean
        Get
            Return hdrVisor.StateCommon.Border.Color1 = _ColorFoco
        End Get
    End Property

    Public Overrides ReadOnly Property fichaPrincipal As Boolean
        Get
            Return btnPrincipal.Checked = ButtonCheckState.Checked
        End Get
    End Property

    Public Overrides ReadOnly Property fichaTipo As Object
        Get
            Return _Elemento.Tipo
        End Get
    End Property

    Public Overrides Property fichaActiva() As Boolean
        Get
            Return (btnEliminar.Enabled = ButtonEnabled.True)
        End Get
        Set(ByVal value As Boolean)
            btnEliminar.Enabled = IIf(value, ButtonEnabled.True, ButtonEnabled.False)
            btnPrincipal.Enabled = IIf(value, ButtonEnabled.True, ButtonEnabled.False)
            Me.Enabled = value
        End Set
    End Property

    Public Overrides Property Item() As Object
        Get
            Return _Elemento
        End Get
        Set(ByVal value As Object)
            _Elemento = value

            LimpiarControl()

            ' Escribo los datos en el formulario
            If _Elemento IsNot Nothing Then
                hdrVisor.ValuesPrimary.Heading = _Elemento.Descripcion
                lblDireccion.Text = _Elemento.DireccionCompleta

                Select Case _Elemento.Tipo
                    Case IDireccion.TiposDireccion.Envio
                        hdrVisor.ValuesPrimary.Image = My.Resources.Envio_16
                    Case IDireccion.TiposDireccion.Fiscal
                        hdrVisor.ValuesPrimary.Image = My.Resources.Factuacion_16
                    Case Else
                        hdrVisor.ValuesPrimary.Image = Nothing
                End Select

                btnPrincipal.Checked = IIf(_Elemento.Principal.HasValue AndAlso _Elemento.Principal.Value, ButtonCheckState.Checked, ButtonCheckState.Unchecked)
            End If

            ' Control del color
            EstablecerColor(Nothing, Nothing)
        End Set
    End Property
#End Region
#Region " LIMPIEZA / COLORES FORMULARIO "
    ''' <summary>
    ''' Limpio todos los datos del control
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub LimpiarControl()
        hdrVisor.ValuesPrimary.Heading = String.Empty
        hdrVisor.ValuesPrimary.Image = Nothing
        hdrVisor.ValuesSecondary.Description = String.Empty
        hdrVisor.ValuesPrimary.Image = Nothing

        lblDireccion.Text = String.Empty

        Visible = True

        ObtenerFoco(Nothing, Nothing)
    End Sub

    ''' <summary>
    ''' Pinta el borde del formulario según el estado del control
    ''' </summary>
    Private Sub EstablecerColor(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Leave, MyBase.Enter
        Dim NuevoColor As System.Drawing.Color = Nothing

        ' Averiguo que color tengo que establecer
        If Me.Focused Then
            NuevoColor = _ColorFoco
        ElseIf btnPrincipal.Checked = ButtonCheckState.Checked Then
            NuevoColor = _ColorFavorito
        Else
            NuevoColor = _ColorNoFoco
        End If

        hdrVisor.StateCommon.Border.Color1 = NuevoColor
        hdrVisor.StateCommon.HeaderSecondary.Border.Color1 = NuevoColor

        MyBase.LanzarEventoActivoCambiado(Nothing)
    End Sub

    ''' <summary>
    ''' Fuerza la obtención del foco
    ''' </summary>
    Private Sub ObtenerFoco(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hdrVisor.Click, Me.Click
        MyBase.Focus()
        EstablecerColor(Nothing, Nothing)
    End Sub
#End Region
#Region " ACCIONES ELIMINACION / MODIFICACION / AGREGAR "
    ''' <summary>
    ''' Marca el registro como Eliminado
    ''' </summary>
    Private Sub EliminarRegistro(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        MyBase.Eliminar()
    End Sub

    Public Overrides Sub CambiarPrincipal()
        btnPrincipal.PerformClick()
    End Sub
#End Region
#Region " EVENTOS "
    Private Sub EstablecerPrincipal(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrincipal.Click
        _Elemento.Principal = (btnPrincipal.Checked = ButtonCheckState.Checked)
        EstablecerColor(Nothing, Nothing)
        MyBase.LanzarEventoPrincipalCambiado(_Elemento.Principal)
    End Sub

    ''' <summary>
    ''' Lanza el evento de doble click
    ''' </summary>
    Private Sub LanzarEvento(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblDireccion.DoubleClick, hdrVisor.DoubleClick
        MyBase.LanzarEventoDobleClick(e)
    End Sub

    ''' <summary>
    ''' Obtiene un nuevo formulario de edición de contactos
    ''' </summary>
    Public Overrides Function obtenerFormularioEdicion() As IFormularioItem
        Return New frmDirecciones(TypeOf (_Elemento) Is DireccionCliente, Me.Contexto)
    End Function
#End Region
End Class
