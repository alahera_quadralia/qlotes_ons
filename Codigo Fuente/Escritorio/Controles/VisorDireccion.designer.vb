﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class VisorDireccion
    Inherits Escritorio.cFicha

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(VisorDireccion))
        Me.hdrVisor = New ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup()
        Me.btnPrincipal = New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup()
        Me.btnEliminar = New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup()
        Me.lblDireccion = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        CType(Me.hdrVisor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.hdrVisor.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.hdrVisor.Panel.SuspendLayout()
        Me.hdrVisor.SuspendLayout()
        Me.SuspendLayout()
        '
        'hdrVisor
        '
        Me.hdrVisor.ButtonSpecs.AddRange(New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup() {Me.btnPrincipal, Me.btnEliminar})
        resources.ApplyResources(Me.hdrVisor, "hdrVisor")
        Me.hdrVisor.GroupBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.GridHeaderRowSheet
        Me.hdrVisor.HeaderStylePrimary = ComponentFactory.Krypton.Toolkit.HeaderStyle.Form
        Me.hdrVisor.Name = "hdrVisor"
        '
        'hdrVisor.Panel
        '
        Me.hdrVisor.Panel.Controls.Add(Me.lblDireccion)
        Me.hdrVisor.StateCommon.Border.Color1 = System.Drawing.Color.White
        Me.hdrVisor.StateCommon.Border.DrawBorders = CType((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top Or ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) _
                    Or ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) _
                    Or ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right), ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)
        Me.hdrVisor.StateCommon.Border.Width = 3
        Me.hdrVisor.StateCommon.HeaderPrimary.Border.Draw = ComponentFactory.Krypton.Toolkit.InheritBool.[False]
        Me.hdrVisor.StateCommon.HeaderPrimary.Border.DrawBorders = CType((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top Or ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) _
                    Or ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) _
                    Or ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right), ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)
        Me.hdrVisor.StateCommon.HeaderPrimary.Border.Width = 3
        Me.hdrVisor.StateCommon.HeaderPrimary.Content.LongText.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.hdrVisor.StateCommon.HeaderPrimary.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near
        Me.hdrVisor.StateCommon.HeaderPrimary.Content.ShortText.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.hdrVisor.StateCommon.HeaderSecondary.Border.Color1 = System.Drawing.Color.White
        Me.hdrVisor.StateCommon.HeaderSecondary.Border.ColorStyle = ComponentFactory.Krypton.Toolkit.PaletteColorStyle.Solid
        Me.hdrVisor.StateCommon.HeaderSecondary.Border.Draw = ComponentFactory.Krypton.Toolkit.InheritBool.[True]
        Me.hdrVisor.StateCommon.HeaderSecondary.Border.DrawBorders = CType(((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom Or ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) _
                    Or ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right), ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)
        Me.hdrVisor.StateCommon.HeaderSecondary.Border.Width = 3
        Me.hdrVisor.Tag = "In guild since {0}"
        Me.hdrVisor.ValuesPrimary.Heading = resources.GetString("hdrVisor.ValuesPrimary.Heading")
        Me.hdrVisor.ValuesPrimary.Image = CType(resources.GetObject("hdrVisor.ValuesPrimary.Image"), System.Drawing.Image)
        Me.hdrVisor.ValuesSecondary.Heading = resources.GetString("hdrVisor.ValuesSecondary.Heading")
        '
        'btnPrincipal
        '
        resources.ApplyResources(Me.btnPrincipal, "btnPrincipal")
        Me.btnPrincipal.Image = Global.Escritorio.My.Resources.Resources.Favorito_16
        Me.btnPrincipal.UniqueName = "AEC6924744414DB213A0CF2E42BC3BBB"
        '
        'btnEliminar
        '
        resources.ApplyResources(Me.btnEliminar, "btnEliminar")
        Me.btnEliminar.UniqueName = "E7EFC03984D343A5559F876823B4FB2F"
        '
        'lblDireccion
        '
        resources.ApplyResources(Me.lblDireccion, "lblDireccion")
        Me.lblDireccion.Name = "lblDireccion"
        Me.lblDireccion.Values.Text = resources.GetString("lblDireccion.Values.Text")
        '
        'VisorDireccion
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.hdrVisor)
        Me.Name = "VisorDireccion"
        CType(Me.hdrVisor.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrVisor.Panel.ResumeLayout(False)
        Me.hdrVisor.Panel.PerformLayout()
        CType(Me.hdrVisor, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrVisor.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents hdrVisor As ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup
    Friend WithEvents btnEliminar As ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup
    Friend WithEvents btnPrincipal As ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup
    Friend WithEvents lblDireccion As ComponentFactory.Krypton.Toolkit.KryptonLabel
End Class
