Imports System.Drawing.Text

Public Class frmGestionEtiquetas
#Region " DECLARACIONES "
    Private _EstadoFormulario As Quadralia.Enumerados.EstadoFormulario = Quadralia.Enumerados.EstadoFormulario.Espera  ' Indica el estado en el que se encuentra el formulario
    Private _Registro As Etiqueta = Nothing
    Private _Contexto As Entidades = Nothing
    Private _dgvDesgloseEliminando As Boolean = False

    Private _dgvLoad As Boolean = False ' Bandera para evitar que se cargue la primera fila cuando se asigna un origen de datos al DGV
#End Region
#Region " PROPIEDADES "
    Public Overrides Property Estado() As Quadralia.Enumerados.EstadoFormulario
        Get
            Return _EstadoFormulario
        End Get
        Set(ByVal value As Quadralia.Enumerados.EstadoFormulario)
            _EstadoFormulario = value

            'Para evitar refrescos raros, cuando ponemos el formulario en estado de espera, volvemos a la pesta�a de datos
            hdrBusqueda.Panel.Enabled = (value = Quadralia.Enumerados.EstadoFormulario.Espera)
            tblOrganizadorDatos.Enabled = (value <> Quadralia.Enumerados.EstadoFormulario.Espera)
            tblOrganizadorDatosEntrada.Enabled = False

            ' Tengo que establecer el foco en un control para que no quede "muerto" el ribbon
            If value = Quadralia.Enumerados.EstadoFormulario.Espera Then
                Me.ActiveControl = txtBcodigo.TextBox
            Else
                Me.ActiveControl = txtProveedor
            End If

            ControlarBotones()
        End Set
    End Property

    ''' <summary>
    ''' Registro que se muestra actualmente en el formulario
    ''' </summary>
    Public Property Registro As Etiqueta
        Get
            Return _Registro
        End Get
        Set(ByVal value As Etiqueta)
            On Error Resume Next
            _Registro = value
            CargarRegistro(value)
        End Set
    End Property

    ''' <summary>
    ''' Contexto que utilizar� el formulario
    ''' </summary>
    Public Property Contexto As Entidades
        Get
            If _Contexto Is Nothing Then
                _Contexto = cDAL.Instancia.getContext()
                _Contexto.ContextOptions.LazyLoadingEnabled = True
            End If
            Return _Contexto
        End Get
        Set(ByVal value As Entidades)
            _Contexto = value
        End Set
    End Property

    ''' <summary>
    ''' Obtiene / establece el id del elemento cargado en el formulario
    ''' </summary>
    Public Property IdRegistro As Long
        Get
            If Me.Registro Is Nothing Then Return -1 Else Return Me.Registro.id
        End Get
        Set(ByVal value As Long)
            Me.Registro = (From etq As Etiqueta In Me.Contexto.Etiquetas Where etq.id = value).FirstOrDefault
        End Set
    End Property
#End Region
#Region " IMPLEMENTACIONES NECESARIAS DEL FORMULARIO HIJO "
    ''' <summary>
    ''' Tab del ribbon asociado a este formulario
    ''' </summary>
    Public Overrides ReadOnly Property Tab() As ComponentFactory.Krypton.Ribbon.KryptonRibbonTab
        Get
            Return frmPrincipal.tabEtiquetas
        End Get
    End Property
#End Region
#Region " LIMPIEZA "
    ''' <summary>
    ''' Limpia los controles relacionados con la b�squeda de registros
    ''' </summary>
    Private Sub LimpiarBusqueda() Handles btnLimpiarBusqueda.Click
        txtBcodigo.Text = String.Empty

        dtpBFechaInicio.Value = DateTime.Now
        dtpBFechaInicio.ValueNullable = Nothing

        dtpBFechaFin.Value = DateTime.Now
        dtpBFechaFin.ValueNullable = Nothing

        txtBProveedor.Clear()
        txtBEspecie.Clear()
        txtBLoteSalida.Clear()
        cboBZonaFAO.Clear()
        cboBSubZona.Clear()

        chkBRegistrosEliminados.Checked = False

        dgvResultadosBuscador.DataSource = Nothing
        dgvResultadosBuscador.Rows.Clear()
    End Sub

    ''' <summary>
    ''' Limpia los controles relacionados con la edicion de registros
    ''' </summary>
    Private Sub LimpiarDatos()
        txtCodigo.Clear()
        txtPeso.Clear()
        cboExpedidor.Clear()
        txtLineaEntrada.Clear()
        dtpFecha.Clear()
        chkSincronizado.Checked = False
        cboDisenho.Clear()

        chkReactivarRegistro.Checked = False
        chkReactivarRegistro.Visible = False

        LimpiarDatosLinea()

        epErrores.Clear()
    End Sub

    Private Sub LimpiarDatosLinea()
        txtCodigo.Clear()
        txtLoteEntrada.Clear()
        txtLoteSalida.Clear()
        txtProveedor.Clear()
        txtEmbarcacion.Clear()
        txtEspecie.Clear()
        txtZona.Clear()
        txtSubzona.Clear()
        txtPresentacion.Clear()
        txtProduccion.Clear()

        picQR.BackgroundImage = Nothing
    End Sub

    ''' <summary>
    ''' Borra todos los controles del formulario
    ''' </summary>
    Private Sub LimpiarTodo()
        LimpiarBusqueda()
        LimpiarDatos()
    End Sub
#End Region
#Region " CONTROL DE BOTONES "
    Private Sub ControlarBotones()
        ControlarRibbon()
    End Sub

    ''' <summary>
    ''' Controla el cambio de botones del formulario principal
    ''' </summary>
    Private Sub ControlarRibbon()
        If Me.ParentForm Is Nothing Then Exit Sub
        If Not TypeOf Me.ParentForm Is frmPrincipal Then Exit Sub

        With DirectCast(Me.ParentForm, frmPrincipal)
            .krgtEtiquetasAccionesEdicion.Visible = (Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera)
            .krgtEtiquetasAccionesEspera.Visible = Not .krgtEtiquetasAccionesEdicion.Visible
            .mnuRapidoGuardar.Enabled = .krgtEtiquetasAccionesEdicion.Visible

            .btnEtiquetasAccionesModificar.Enabled = (Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera) AndAlso Registro IsNot Nothing
            .btnEtiquetasAccionesEliminar.Enabled = .btnEtiquetasAccionesModificar.Enabled

            .krgtEtiquetasMasAcciones.Visible = Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera
            .btnEtiquetasMasAccionesImprimir.Enabled = Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera AndAlso Me.Registro IsNot Nothing
        End With
    End Sub
#End Region
#Region " RUTINA DE INICIO / FIN Y CARGA DE DATOS MAESTROS "
    ''' <summary>
    ''' Rutina de inicio del formulario
    ''' </summary>
    Private Sub Inicio(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Tag = Me.Text
        tabGeneral.SelectedIndex = 0

        ' Cargamos los datos maestros
        CargarDatosMaestros()
        ConfigurarEntorno()

        ' Limpieza
        Quadralia.Formularios.AutoTabular(Me)
        LimpiarTodo()
        tabGeneral.SelectedIndex = 0
        Me.Registro = Nothing

        ' Oculto el buscador
        If Not cConfiguracionAplicacion.Instancia.BuscadoresAbiertos Then OcultarBusquedaDobleClick(hdrBusqueda, Nothing)

        ' No quiero nuevas columnas
        dgvResultadosBuscador.AutoGenerateColumns = False

        Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera
    End Sub

    ''' <summary>
    ''' Carga los datos maestros
    ''' </summary>
    Public Sub CargarDatosMaestros()
        ' Cargar Zonas FAO
        With cboExpedidor
            .DataSource = Nothing
            .Items.Clear()

            .DisplayMember = "razonSocial"
            .ValueMember = "id"
            .DataSource = (From exp As Expedidor In Contexto.Expedidores Where Not exp.fechaBaja.HasValue Order By exp.razonSocial Select exp).ToList

            .Clear()
        End With

        With cboBZonaFAO
            .DataSource = Nothing
            .Items.Clear()

            .DisplayMember = "descripcion"
            .ValueMember = "id"
            .DataSource = (From fao As ZonaFao In Contexto.ZonasFao Select fao.id, fao.descripcion).ToList

            .Clear()
        End With

        ' Cargar Subzonas
        With cboBSubZona
            .DataSource = Nothing
            .Items.Clear()

            .DisplayMember = "descripcion"
            .ValueMember = "id"
            .DataSource = (From sbz As SubZona In Contexto.SubZonas Select sbz.id, sbz.descripcion).ToList
            .Clear()
        End With

        ' Cargo los disenhos de etiquetas
        With cboDisenho
            .DataSource = Nothing
            .Items.Clear()

            .DisplayMember = "nombre"
            .ValueMember = "ruta"
            .DataSource = Quadralia.Aplicacion.GestorInformes.Informes("EtiquetasTrazabilidad").Values.ToList

            .Clear()
        End With
    End Sub
#End Region
#Region " BUSQUEDA, CARGA DE Y EDICION DE REGISTROS "
    ''' <summary>
    ''' Realiza la b�squeda de registros coincidentes con los par�metros especificados
    ''' </summary>
    Private Sub Buscar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBBuscar.Click
        Dim FechaInicio As Nullable(Of DateTime) = Nothing
        Dim FechaFin As Nullable(Of DateTime) = Nothing
        Dim IdProveedor As Nullable(Of Long) = Nothing
        Dim IdEspecie As Nullable(Of Integer) = Nothing
        Dim IdFao As Nullable(Of Integer) = Nothing
        Dim IdSubZona As Nullable(Of Integer) = Nothing
        Dim Codigo As Integer = 0

        If Not IsDBNull(dtpBFechaInicio.ValueNullable) Then FechaInicio = dtpBFechaInicio.Value.Date + New TimeSpan(0, 0, 0)
        If Not IsDBNull(dtpBFechaFin.ValueNullable) Then FechaFin = dtpBFechaFin.Value.Date + New TimeSpan(23, 59, 59)
        If txtBProveedor.Item IsNot Nothing Then IdProveedor = DirectCast(txtBProveedor.Item, Proveedor).id
        If txtBEspecie.Item IsNot Nothing Then IdEspecie = DirectCast(txtBEspecie.Item, Especie).id
        If cboBZonaFAO.SelectedIndex > -1 Then IdFao = cboBZonaFAO.SelectedValue
        If cboBSubZona.SelectedIndex > -1 Then IdSubZona = cboBSubZona.SelectedValue
        If String.IsNullOrEmpty(txtBcodigo.Text) OrElse Not Integer.TryParse(txtBcodigo.Text, Codigo) Then Codigo = 0

        Dim Registros = (From etq As Etiqueta In Contexto.Etiquetas
                         Let Linea As LoteEntradalinea = etq.LoteEntradaLinea
                         Where (chkBRegistrosEliminados.Checked OrElse Not etq.fechaBaja.HasValue) _
                         AndAlso (Codigo <= 0 OrElse etq.id = Codigo) _
                         AndAlso (Not FechaInicio.HasValue OrElse etq.fecha >= FechaInicio.Value) _
                         AndAlso (Not FechaFin.HasValue OrElse etq.fecha <= FechaFin.Value) _
                         AndAlso (Not IdProveedor.HasValue OrElse (Linea.LoteEntrada IsNot Nothing AndAlso Linea.LoteEntrada.Proveedor IsNot Nothing AndAlso Linea.LoteEntrada.Proveedor.id = IdProveedor)) _
                         AndAlso (Not IdEspecie.HasValue OrElse (Linea IsNot Nothing AndAlso Linea.Especie IsNot Nothing AndAlso Linea.Especie.id = IdEspecie.Value)) _
                         AndAlso (Not IdFao.HasValue OrElse (Linea IsNot Nothing AndAlso Linea.ZonaFao IsNot Nothing AndAlso Linea.ZonaFao.id = IdFao.Value)) _
                         AndAlso (Not IdSubZona.HasValue OrElse (Linea IsNot Nothing AndAlso Linea.SubZona IsNot Nothing AndAlso Linea.SubZona.id = IdSubZona.Value))
                         Select etq).Distinct().ToList()

        If Not String.IsNullOrEmpty(txtBLoteSalida.Text) Then
            Dim res As New List(Of Etiqueta)
            For Each r In Registros
                If r.LoteSalida = txtBLoteSalida.Text OrElse r.LoteSalida.Contains(txtBLoteSalida.Text) Then
                    res.Add(r)
                End If
            Next
            Registros = res
        End If

        If Registros IsNot Nothing Then
            _dgvLoad = (Registros.Count > 1)
            dgvResultadosBuscador.DataSource = Registros.ToList
            _dgvLoad = False
        End If

        If (Registros.Count > 1) Then dgvResultadosBuscador.ClearSelection()
    End Sub

    ''' <summary>
    ''' Se prepara el formulario para introducir un nuevo registro    
    ''' </summary>    
    Public Overrides Sub Nuevo()
        LimpiarDatos()
        Me.Estado = Quadralia.Enumerados.EstadoFormulario.Anhadiendo

        Dim Aux = New Etiqueta
        Aux.fecha = DateTime.Now()

        ' Lo a�ado al contexto
        Contexto.Etiquetas.AddObject(Aux)
        Me.Registro = Aux
    End Sub

    ''' <summary>
    ''' Se carga el registro seleccionado el formulario
    ''' </summary>
    Private Sub Cargar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvResultadosBuscador.SelectionChanged
        If dgvResultadosBuscador.SelectedRows.Count = 1 Then
            Me.Registro = dgvResultadosBuscador.SelectedRows(0).DataBoundItem
        Else
            Me.Registro = Nothing
            LimpiarDatos()
        End If
    End Sub

    ''' <summary>
    ''' Carga el registro actual del formulario
    ''' </summary>
    Private Sub CargarRegistro()
        CargarRegistro(Me.Registro)
    End Sub

    ''' <summary>
    ''' Se carga un registro en el formulario
    ''' </summary>
    Private Sub CargarRegistro(ByVal Instancia As Etiqueta)
        ' Si el DGV est� vinculando un origen de datos, no cargo nada
        If _dgvLoad Then Exit Sub

        ' Limpio el formulario y cargo el registro
        LimpiarDatos()
        ControlarBotones()

        If Instancia Is Nothing Then Exit Sub

        With Instancia
            txtCodigo.Text = .LoteEntradaLinea.LoteEntrada.codigo ''.Codigo
            dtpFecha.ValueNullable = .fecha
            txtPeso.Text = .cantidad
            cboExpedidor.SelectedItem = .Expedidor
            chkSincronizado.Checked = .sincronizadoWeb
            txtLineaEntrada.Item = .LoteEntradaLinea
            cboDisenho.SelectedValue = .DisenhoPred
            txtLoteSalida.Text = .LoteSalida
            GenerarCodigoQR(.Codigo)

            chkReactivarRegistro.Visible = .fechaBaja.HasValue

            Me.Text = String.Format("{0} [{1}]", Me.Tag, .Codigo)
        End With

        _Registro = Instancia
    End Sub

    Private Sub GenerarCodigoQR(Codigo As String)
        picQR.BackgroundImage = Nothing
        If Me.Registro Is Nothing Then Exit Sub

        Dim URL As String = Me.Registro.ObtenerURI(cConfiguracionPrograma.Instancia)

        Dim GeneradorCodigo As New QRCoder.QRCodeGenerator()
        Dim CodigoQR As QRCoder.QRCodeGenerator.QRCode = GeneradorCodigo.CreateQrCode(URL, QRCoder.QRCodeGenerator.ECCLevel.M)
        picQR.BackgroundImage = CodigoQR.GetGraphic(20)
    End Sub

    ''' <summary>
    ''' Carga los datos de la linea entrada
    ''' </summary>
    Private Sub CargarDatosLinea() Handles txtLineaEntrada.ItemChanged
        LimpiarDatosLinea()

        Dim LineaEntrada As LoteEntradaLinea = txtLineaEntrada.Item
        If LineaEntrada Is Nothing Then Exit Sub

        txtCodigo.Text = LineaEntrada.LoteEntrada.codigo ''.codigoCompleto
        txtLoteEntrada.Text = LineaEntrada.numeroLote

        txtProveedor.Text = LineaEntrada.nombreProveedor
        txtEmbarcacion.Text = LineaEntrada.NombreEmbarcacion
        txtEspecie.Text = LineaEntrada.nombreEspecie
        txtZona.Text = LineaEntrada.nombreFAO
        txtSubzona.Text = LineaEntrada.nombreSubZona
        txtPresentacion.Text = LineaEntrada.nombrePresentacion
        txtProduccion.Text = LineaEntrada.nombreProduccion
    End Sub

    ''' <summary>
    ''' Preparamos el formulario para modificar un registro cargado
    ''' </summary>
    Public Overrides Sub Modificar()
        If Me.Registro IsNot Nothing Then
            Me.Estado = Quadralia.Enumerados.EstadoFormulario.Editando
        End If
    End Sub

    ''' <summary>
    ''' Eliminaci�n l�gica del registro
    ''' </summary>
    Public Overrides Sub Eliminar()
        If Registro IsNot Nothing Then
            If Quadralia.Aplicacion.MostrarMessageBox(My.Resources.ConfirmarEliminacion, My.Resources.TituloConfirmarEliminacion, MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
                ' Lo elimino del contexto
                Me.Registro.fechaBaja = DateTime.Now
                Me.Contexto.SaveChanges()

                If dgvResultadosBuscador.SelectedRows.Count > 0 Then Buscar(Nothing, Nothing) Else Cargar(Nothing, Nothing)
            End If
        End If
    End Sub

    ''' <summary>
    ''' Cancela la edici�n del registro
    ''' </summary>
    Public Overrides Sub Cancelar()
        If Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera Then Exit Sub

        ' Cambio el estado del formulario
        Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera

        ' Limpio los datos y desecho los cambios
        Quadralia.Linq.CancelarCambios(Me.Contexto)

        ' Limpio los datos
        LimpiarDatos()

        ' Cargo el registro seleccionado
        Cargar(Nothing, Nothing)
    End Sub

    ''' <summary>
    ''' Se guardan los datos en la base de datos
    ''' </summary>
    ''' <param name="MostrarMensaje">Indica si se muestra un mensaje con el resultado de la funci�n</param>
    Public Overrides Function Guardar(Optional ByVal MostrarMensaje As Boolean = True) As Boolean
        If Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera Then Return True

        ' Valido los controles
        Me.ValidateChildren(ValidationConstraints.Visible + ValidationConstraints.Enabled)

        If epErrores.HasErrors Then
            If MostrarMensaje Then Quadralia.Aplicacion.MostrarMessageBox(My.Resources.ErroresEncontrados, My.Resources.TituloErroresEncontrados, MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        End If

        ' Guardo los datos en el registro
        With Registro
            Dim Aux As Double = 0

            If IsDBNull(dtpFecha.ValueNullable) Then .fecha = Nothing Else .fecha = dtpFecha.Value
            If Double.TryParse(txtPeso.Text, Aux) Then .cantidad = Aux Else .cantidad = 0
            .Expedidor = cboExpedidor.SelectedItem
            .LoteEntradaLinea = txtLineaEntrada.Item
            .sincronizadoWeb = chkSincronizado.Checked
            If cboDisenho.SelectedIndex = -1 Then .DisenhoPred = String.Empty Else .DisenhoPred = cboDisenho.SelectedValue

            If chkReactivarRegistro.Visible AndAlso chkReactivarRegistro.Checked Then .fechaBaja = Nothing
        End With

        Try
            ' Guardo el registro en base de datos
            Contexto.SaveChanges()

            ' Si estoy a�adiendo, obtengo un n�mero de Albar�n
            If MostrarMensaje Then Quadralia.Aplicacion.MostrarMensajeNtfIco(My.Resources.TituloDatosGuardatos, My.Resources.DatosGuardados, ToolTipIcon.Info)

            ' Restablezco el formulario
            Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera
            LimpiarDatos()
            CargarRegistro()

            Return True
        Catch ex As Exception
            If MostrarMensaje Then Quadralia.Aplicacion.InformarError(ex)
            Return False
        End Try
    End Function
#End Region
#Region " RESTO DE FUNCIONES "
    ''' <summary>
    ''' Oculta / muestra el panel de b�squeda
    ''' </summary>
    Private Sub OcultarBusqueda(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOcultarBusqueda.Click
        Quadralia.KryptonForms.EsconderPanel(splSeparador, hdrBusqueda)
    End Sub

    ''' <summary>
    ''' Ordena los resultados del DGV de Resultados   
    ''' </summary>
    Private Sub OrdenarResultados(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvResultadosBuscador.ColumnHeaderMouseClick
        Quadralia.Formularios.Funcionalidad.OrdenarDGV(Of LoteEntrada)(dgvResultadosBuscador, e)
    End Sub

    ''' <summary>
    ''' Cuando el usuario hace doble click en el panel, desencadenamos la misma funci�n que si hiciera click en el bot�n
    ''' </summary>
    Private Sub OcultarBusquedaDobleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hdrBusqueda.DoubleClick
        ' Hay que hacerlo as�, no sirve con llamar a la funci�n directamente. si no, el bot�n del header se descontrola y desaparece el interior del panel
        btnOcultarBusqueda.PerformClick()
    End Sub

    ''' <summary>
    ''' Lanzamos la b�squeda de registros si el usuario presiona ENTER en los campos de par�metros
    ''' </summary>
    Private Sub ManejadorBusquedasTexto(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtBcodigo.KeyDown, dtpBFechaFin.KeyDown, dtpBFechaInicio.KeyDown, txtBProveedor.KeyDown, txtBEspecie.KeyDown, cboBZonaFAO.KeyDown, cboBSubZona.KeyDown, dtpFecha.KeyDown, cboExpedidor.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.Handled = True
            Buscar(Nothing, Nothing)
        End If
    End Sub

    ''' <summary>
    ''' Evita que salga el molesto error del DGV
    ''' </summary>
    Private Sub EvitarErrores(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvResultadosBuscador.DataError
        e.Cancel = True
    End Sub

    ''' <summary>
    ''' Pone el formulario en modo edici�n cuando el usuario hace doble click sobre un resultado del buscador
    ''' </summary>
    Private Sub ActivarEdicionRegistro(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvResultadosBuscador.CellDoubleClick, dgvResultadosBuscador.CellContentDoubleClick
        If e.RowIndex > -1 Then
            dgvResultadosBuscador.Rows(e.RowIndex).Selected = True
            Modificar()
        End If
    End Sub

    ''' <summary>
    ''' Nos desplaza a traves de los controles al pulsar enter
    ''' </summary>
    Private Sub EnterComoTab(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.Enter AndAlso (Me.ActiveControl Is Nothing OrElse Me.ActiveControl.Parent Is Nothing OrElse Not TypeOf (Me.ActiveControl.Parent) Is KryptonTextBox OrElse Not DirectCast(Me.ActiveControl.Parent, KryptonTextBox).Multiline) Then
            e.Handled = True
            SendKeys.Send("{TAB}")
        End If
    End Sub

    ''' <summary>
    ''' Control de las teclas de acci�n del usuario
    ''' </summary>
    Private Sub ControlTeclasBuscador(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvResultadosBuscador.KeyDown
        If Me.Registro Is Nothing Then Exit Sub

        Select Case e.KeyCode
            Case Keys.Delete
                e.Handled = True
                Eliminar()
            Case Keys.F2, Keys.Enter
                e.Handled = True
                Modificar()
        End Select
    End Sub
#End Region
#Region " VALIDACIONES "
    ''' <summary>
    ''' Verifica que se haya introducido texto en un textbox marcado como obligatorio
    ''' </summary>
    Private Sub ValidarTextBoxObligatorio(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs)
        If Not TypeOf (sender) Is KryptonTextBox Then Exit Sub

        If Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera AndAlso String.IsNullOrEmpty(DirectCast(sender, KryptonTextBox).Text) Then
            epErrores.SetError(sender, My.Resources.TextBoxObligatorio)
        Else
            epErrores.SetError(sender, "")
        End If
    End Sub

    ''' <summary>
    ''' Verifica que se haya seleccionado una fecha del DTP marcado como obligatorio
    ''' </summary>
    Private Sub ValidarDTPObligatorio(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs)
        If Not TypeOf (sender) Is KryptonDateTimePicker Then Exit Sub

        If Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera AndAlso IsDBNull(DirectCast(sender, KryptonDateTimePicker).ValueNullable) Then
            epErrores.SetError(sender, My.Resources.DTPObligatorio)
        Else
            epErrores.SetError(sender, "")
        End If
    End Sub

    ''' <summary>
    ''' Obliga al usuario a seleccionar un cliente antes de continuar
    ''' </summary>
    Private Sub ValidarTxtBuscador(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs)
        If Not TypeOf (sender) Is cTextboxBuscador Then Exit Sub

        If Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera AndAlso DirectCast(sender, cTextboxBuscador).Item Is Nothing Then
            epErrores.SetError(sender, My.Resources.TextBoxObligatorio)
        Else
            epErrores.SetError(sender, "")
        End If
    End Sub

    ''' <summary>
    ''' Verifica que se haya hecho una seleccion en un combobox marcado como obligatorio
    ''' </summary>
    Private Sub ComboObligatorio(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs)
        If Not TypeOf (sender) Is KryptonComboBox Then Exit Sub

        If Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera AndAlso DirectCast(sender, KryptonComboBox).SelectedIndex = -1 Then
            epErrores.SetError(sender, My.Resources.ComboObligatorio)
        Else
            epErrores.SetError(sender, "")
        End If
    End Sub

    Private Sub SoloDecimales(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles txtPeso.KeyPress
        Quadralia.Validadores.QuadraliaValidadores.SoloNumerosYDecimales(sender, e)
    End Sub

    ''' <summary>
    ''' Valida que el usuario no pueda meter un c�digo ya existente o uno nulo
    ''' </summary>
    Private Sub ValidarCodigo(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs)
        If Me.Estado = Quadralia.Enumerados.EstadoFormulario.Editando AndAlso String.IsNullOrEmpty(txtCodigo.Text) Then
            epErrores.SetError(txtCodigo, My.Resources.TextBoxObligatorio)
        ElseIf Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera AndAlso (From ent As LoteEntrada In Contexto.LotesEntrada Where ent.codigo.Trim().ToUpper() = txtCodigo.Text.Trim().ToUpper() AndAlso ent.id <> Registro.id Select ent).Count > 0 Then
            epErrores.SetError(txtCodigo, "El n�mero introducido se encuentra actualmente en uso. Por favor, rev�selo")
        Else
            epErrores.SetError(txtCodigo, "")
        End If
    End Sub
#End Region
#Region " SEGURIDAD "
    ''' <summary>
    ''' Configura el formulario en base a los permisos del usuario
    ''' </summary>
    Private Sub ConfigurarEntorno()
    End Sub
#End Region
#Region " IMPRIMIR "
    ''' <summary>
    ''' Imprime el registro actual
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Imprimir(ByVal Informe As cInforme)
        ' Validaciones
        If Me.Registro Is Nothing Then Exit Sub
        If Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera Then Exit Sub

        ' Imprimo la lista
        Dim Lista As New List(Of Etiqueta)
        Lista.Add(Me.Registro)
        ImprimirEtiquetas(Lista, Informe)
    End Sub

    ''' <summary>
    ''' Imprime los registros especificados como par�metro de entrada
    ''' </summary>
    Public Shared Sub ImprimirEtiquetas(Etiquetas As List(Of Etiqueta), ByVal Informe As cInforme)
        If Not Quadralia.Aplicacion.PuedeAccederA(Quadralia.Aplicacion.Permisos.ETIQUETAS) Then Exit Sub
        If Etiquetas Is Nothing OrElse Etiquetas.Count = 0 Then Exit Sub

        Dim Formulario As frmVisorInforme = Nothing
#If DEBUG Then
        'Formulario = Quadralia.Aplicacion.AbrirInforme(Nothing, frmVisorInforme.Informe.EtiquetasTrazabilidad, Quadralia.Aplicacion.Permisos.ETIQUETAS)
#Else
        Formulario = Quadralia.Aplicacion.AbrirInforme(Nothing, frmVisorInforme.Informe.EtiquetasTrazabilidad, Quadralia.Aplicacion.Permisos.ETIQUETAS, Nothing, False)
#End If
        Formulario = Quadralia.Aplicacion.AbrirInforme(Nothing, frmVisorInforme.Informe.EtiquetasTrazabilidad, Quadralia.Aplicacion.Permisos.ETIQUETAS, Nothing, False)
        If Formulario Is Nothing Then Exit Sub

        If Informe Is Nothing Then
            ' Obtengo el informe asignado en la etiqueta
            If String.IsNullOrEmpty(Etiquetas(0).DisenhoPred) Or Etiquetas(0).DisenhoPred = "False" Then
                Informe = Quadralia.Aplicacion.GestorInformes.Informes("EtiquetasTrazabilidad").Values(0)
            Else
                Informe = Quadralia.Aplicacion.GestorInformes.obtenerInforme(Etiquetas(0).DisenhoPred, "EtiquetasTrazabilidad")
            End If
        End If

        Formulario.Datos = Etiquetas
        If Informe IsNot Nothing Then Formulario.Reporte = Informe.Documento

        If (Informe.Nombre.ToUpper() = "MAKRO") Then
            Formulario.RefrescarDatos(False, Informe.Nombre)
        Else
            Formulario.RefrescarDatos()
        End If
        'Formulario.Imprimir(cConfiguracionAplicacion.Instancia.Impresora, cConfiguracionAplicacion.Instancia.Orientacion, cConfiguracionAplicacion.Instancia.Formato)
        'Formulario.Close()
#If Not DEBUG Then
        Formulario.Imprimir(cConfiguracionAplicacion.Instancia.Impresora, cConfiguracionAplicacion.Instancia.Orientacion, cConfiguracionAplicacion.Instancia.Formato)
        Formulario.Close()
#Else
        Formulario.Show()
#End If
    End Sub

#End Region
End Class
