Imports ComponentFactory.Krypton.Toolkit
Public Class frmUsuario
#Region " DECLARACIONES "
    Private _EstadoFormulario As Quadralia.Enumerados.EstadoFormulario = Quadralia.Enumerados.EstadoFormulario.Espera  ' Indica el estado en el que se encuentra el formulario
    Private _Registro As Usuario = Nothing
    Private _Contexto As Entidades = Nothing

    Private _dgvLoad As Boolean = False ' Bandera para evitar que se cargue la primera fila cuando se asigna un origen de datos al DGV
#End Region
#Region " ESTRUCTURA PERMISOS "
    Private Structure Permiso
        Public Imagen As Image
        Public Permiso As Long
        Public Nombre As String
    End Structure
#End Region
#Region " PROPIEDADES "
    Public Overrides Property Estado() As Quadralia.Enumerados.EstadoFormulario
        Get
            Return _EstadoFormulario
        End Get
        Set(ByVal value As Quadralia.Enumerados.EstadoFormulario)
            _EstadoFormulario = value

            hdrBusqueda.Panel.Enabled = (value = Quadralia.Enumerados.EstadoFormulario.Espera)
            tblOrganizador.Enabled = (value <> Quadralia.Enumerados.EstadoFormulario.Espera)
            hdrPermisos.Panel.Enabled = tblOrganizador.Enabled

            ' Sincronizo los botones
            ControlarBotones()

            ' Tengo que establecer el foco en un control para que no quede "muerto" el ribbon
            If value = Quadralia.Enumerados.EstadoFormulario.Espera Then
                Me.ActiveControl = txtBUsuario.TextBox
            Else
                Me.ActiveControl = txtUsuario.TextBox
            End If
        End Set
    End Property

    ''' <summary>
    ''' Registro que se muestra actualmente en el formulario
    ''' </summary>
    Public Property Registro As Usuario
        Get
            Return _Registro
        End Get
        Set(ByVal value As Usuario)
            _Registro = value
            CargarRegistro(value)
        End Set
    End Property

    Public ReadOnly Property Contexto As Entidades
        Get
            If _Contexto Is Nothing Then
                _Contexto = cDAL.Instancia.getContext()
            End If
            Return _Contexto
        End Get
    End Property
#End Region
#Region " IMPLEMENTACIONES NECESARIAS DEL FORMULARIO HIJO "
    ''' <summary>
    ''' Tab del ribbon asociado a este formulario
    ''' </summary>
    Public Overrides ReadOnly Property Tab() As ComponentFactory.Krypton.Ribbon.KryptonRibbonTab
        Get
            Return frmPrincipal.tabUsuarios
        End Get
    End Property
#End Region
#Region " LIMPIEZA "
    ''' <summary>
    ''' Limpia los controles relacionados con la b�squeda de registros
    ''' </summary>
    Private Sub LimpiarBusqueda()
        txtBUsuario.Text = String.Empty
        chkBActivo.Checked = False

        dgvResultadosBuscador.DataSource = Nothing
        dgvResultadosBuscador.Rows.Clear()
    End Sub

    ''' <summary>
    ''' Limpia los controles relacionados con la edicion de registros
    ''' </summary>
    Private Sub LimpiarDatos()
        txtUsuario.Text = String.Empty
        chkActivo.Checked = False

        CargarPermisos()
        epErrores.Clear()

        Me.Text = Me.Tag
    End Sub

    ''' <summary>
    ''' Borra todos los controles del formulario
    ''' </summary>
    Public Sub LimpiarTodo()
        LimpiarBusqueda()
        LimpiarDatos()
    End Sub

    ''' <summary>
    ''' Limpia el textbox asociado al bot�n
    ''' </summary>
    Private Sub LimpiarControl(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Quadralia.KryptonForms.LimpiarControl(sender)
    End Sub
#End Region
#Region " CONTROL DE BOTONES "
    Private Sub ControlarBotones()
        ControlarRibbon()
    End Sub

    Private Sub ControlarRibbon()
        If Me.ParentForm Is Nothing Then Exit Sub
        If Not TypeOf Me.ParentForm Is frmPrincipal Then Exit Sub

        With DirectCast(Me.ParentForm, frmPrincipal)
            .krgtUsuariosAccionesEdicion.Visible = (Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera)
            .krgtUsuariosAccionesEspera.Visible = Not .krgtUsuariosAccionesEdicion.Visible
            .mnuRapidoGuardar.Enabled = .krgtUsuariosAccionesEdicion.Visible

            .btnUsuariosAccionesModificar.Enabled = (Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera) AndAlso Registro IsNot Nothing
            .btnUsuariosAccionesEliminar.Enabled = .btnUsuariosAccionesModificar.Enabled
        End With
    End Sub

#End Region
#Region " RUTINA DE INICIO / FIN Y CARGA DE DATOS MAESTROS "
    ''' <summary>
    ''' Rutina de inicio del formulario
    ''' </summary>
    Private Sub Inicio(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Tag = Me.Text
        Me.tabUsuario.SelectedIndex = 0

        ' Cargamos los datos maestros
        CargarDatosMaestros()

        LimpiarTodo()

        ' No quiero nuevos Datos
        dgvResultadosBuscador.AutoGenerateColumns = False

        ' Escondo el panel de b�squeda y pongo el formulario en modo de espera
        OcultarBusquedaDobleClick(hdrBusqueda, Nothing)

        Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera

        For Each UnControl As Control In tblOrganizador.Controls
            Quadralia.Formularios.Funcionalidad.ManejadorTabPorIntro(UnControl)
        Next
    End Sub

    ''' <summary>
    ''' Carga los datos maestros
    ''' </summary>
    Public Sub CargarDatosMaestros()
    End Sub

    Private Sub CargarPermisos()
        Dim Datos As Permiso() = New Permiso() {New Permiso With {.Imagen = My.Resources.Expedidor_32, .Nombre = "Expedidores", .Permiso = Quadralia.Aplicacion.Permisos.EXPEDIDORES},
                                                New Permiso With {.Imagen = My.Resources.Proveedor_32, .Nombre = "Proveedores", .Permiso = Quadralia.Aplicacion.Permisos.PROVEEDORES},
                                                New Permiso With {.Imagen = My.Resources.Cliente_32, .Nombre = "Clientes", .Permiso = Quadralia.Aplicacion.Permisos.CLIENTES},
                                                New Permiso With {.Imagen = My.Resources.Entrada_32, .Nombre = "Albaranes de Entrada", .Permiso = Quadralia.Aplicacion.Permisos.LOTES_ENTRADA},
                                                New Permiso With {.Imagen = My.Resources.Etiqueta_32, .Nombre = "Etiquetas", .Permiso = Quadralia.Aplicacion.Permisos.ETIQUETAS},
                                                New Permiso With {.Imagen = My.Resources.Empaquetado_32, .Nombre = "Empaquetado", .Permiso = Quadralia.Aplicacion.Permisos.EMPAQUETADO},
                                                New Permiso With {.Imagen = My.Resources.Salida_32, .Nombre = "Env�os", .Permiso = Quadralia.Aplicacion.Permisos.LOTES_SALIDA},
                                                New Permiso With {.Imagen = My.Resources.Usuarios_32, .Nombre = "Usuarios", .Permiso = Quadralia.Aplicacion.Permisos.USUARIOS},
                                                New Permiso With {.Imagen = My.Resources.Configurar_32, .Nombre = "Configuraci�n de la aplicaci�n", .Permiso = Quadralia.Aplicacion.Permisos.CONFIGURACION_PROGRAMA},
                                                New Permiso With {.Imagen = My.Resources.DatosMaestros_32, .Nombre = "Datos Maestros", .Permiso = Quadralia.Aplicacion.Permisos.DATOS_MAESTROS},
                                                New Permiso With {.Imagen = My.Resources.Informe_32, .Nombre = "Informes", .Permiso = Quadralia.Aplicacion.Permisos.INFORMES}
                                               }

        ' Limpio datos anteriores
        chkPermisos.Items.Clear()

        For i As Integer = 0 To Datos.Length - 1
            Dim Item As New KryptonListItem
            Item.ShortText = Datos(i).Nombre
            Item.Tag = Datos(i).Permiso
            Item.Image = Datos(i).Imagen

            chkPermisos.Items.Add(Item)
        Next
    End Sub
#End Region
#Region " BUSQUEDA, CARGA DE Y EDICION DE REGISTROS "
    ''' <summary>
    ''' Realiza la b�squeda de registros coincidentes con los par�metros especificados
    ''' </summary>
    Private Sub Buscar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBBuscar.Click
        Dim Entidades = From It As Usuario In Contexto.Usuarios _
                        Where Not It.fechaBaja.HasValue _
                        AndAlso (String.IsNullOrEmpty(txtBUsuario.Text) OrElse It.usuario.ToUpper.Contains(txtBUsuario.Text.ToUpper)) _
                        AndAlso (Not chkBActivo.Checked OrElse It.activo) _
                        Select It

        If Entidades IsNot Nothing Then
            _dgvLoad = True
            dgvResultadosBuscador.DataSource = Entidades.Distinct.ToList
            _dgvLoad = False
        End If

        dgvResultadosBuscador.ClearSelection()
        dgvResultadosBuscador.Focus()
    End Sub

    ''' <summary>
    ''' Se prepara el formulario para introducir un nuevo registro
    ''' </summary>    
    Public Overrides Sub Nuevo()
        LimpiarDatos()
        Me.Estado = Quadralia.Enumerados.EstadoFormulario.Anhadiendo

        Dim Aux = New Usuario

        ' Inicializaciones de nuevos registros
        Aux.Clave = Quadralia.Seguridad.Criptografia.EncriptarEnMD5("")
        Aux.Activo = True

        ' Lo a�ado al contexto
        Contexto.Usuarios.AddObject(Aux)
        Me.Registro = Aux
    End Sub

    ''' <summary>
    ''' Se carga el registro seleccionado el formulario
    ''' </summary>
    Private Sub Cargar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvResultadosBuscador.SelectionChanged
        If dgvResultadosBuscador.SelectedRows.Count > 0 Then
            Me.Registro = dgvResultadosBuscador.SelectedRows(0).DataBoundItem
        Else
            Me.Registro = Nothing
        End If
    End Sub

    ''' <summary>
    ''' Carga el registro actual del formulario
    ''' </summary>
    Private Sub CargarRegistro()
        CargarRegistro(Me.Registro)
    End Sub

    ''' <summary>
    ''' Se carga un registro en el formulario
    ''' </summary>
    Private Sub CargarRegistro(ByVal Instancia As Usuario)
        ' Si el DGV est� vinculando un origen de datos, no cargo nada
        If _dgvLoad Then Exit Sub

        ' Limpio el formulario y cargo el registro
        LimpiarDatos()
        ControlarBotones()

        If Instancia Is Nothing Then Exit Sub

        With Instancia
            If .usuario IsNot Nothing Then txtUsuario.Text = .usuario
            chkActivo.Checked = .Activo
            MarcarPermisos(.Permisos)
            chkRestablecerClave.Checked = .resetClave

            Me.Text = String.Format("{0} [{1}]", Me.Tag, .usuario)
        End With

        _Registro = Instancia
    End Sub

    ''' <summary>
    ''' Preparamos el formulario para modificar un registro cargado
    ''' </summary>
    Public Overrides Sub Modificar()
        If Me.Registro IsNot Nothing Then Me.Estado = Quadralia.Enumerados.EstadoFormulario.Editando
    End Sub

    ''' <summary>
    ''' Eliminaci�n l�gica del registro
    ''' </summary>
    Public Overrides Sub Eliminar()
        'If dgvResultadosBuscador.SelectedRows.Count <> 1 Then Exit Sub

        If Registro IsNot Nothing Then
            If Quadralia.Aplicacion.MostrarMessageBox(My.Resources.ConfirmarEliminacion, My.Resources.TituloConfirmarEliminacion, MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
                Me.Registro.FechaBaja = DateTime.Now
                Me.Contexto.SaveChanges()

                If dgvResultadosBuscador.SelectedRows.Count > 0 Then Buscar(Nothing, Nothing) Else Cargar(Nothing, Nothing)
            End If
        End If
    End Sub

    ''' <summary>
    ''' Cancela la edici�n del registro
    ''' </summary>
    Public Overrides Sub Cancelar()
        If Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera Then Exit Sub

        ' Cambio el estado del formulario
        Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera

        ' Limpio los datos y desecho los cambios
        Quadralia.Linq.CancelarCambios(Me.Contexto)

        ' Limpio los datos
        LimpiarDatos()

        ' Cargo el registro seleccionado
        Cargar(Nothing, Nothing)
    End Sub

    ''' <summary>
    ''' Se guardan los datos en la base de datos
    ''' </summary>
    ''' <param name="MostrarMensaje">Indica si se muestra un mensaje con el resultado de la funci�n</param>
    Public Overrides Function Guardar(Optional ByVal MostrarMensaje As Boolean = True) As Boolean
        If Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera Then Return True

        ' Valido los controles
        Me.ValidateChildren(ValidationConstraints.Visible + ValidationConstraints.Enabled)

        If epErrores.HasErrors Then
            If MostrarMensaje Then Quadralia.Aplicacion.MostrarMessageBox(My.Resources.ErroresEncontrados, My.Resources.TituloErroresEncontrados, MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        End If

        ' Guardo los datos en el registro
        With Registro
            .usuario = txtUsuario.Text
            .Activo = chkActivo.Checked
            If chkRestablecerClave.Checked Then .Clave = Quadralia.Seguridad.Criptografia.EncriptarEnMD5("")
            .Permisos = ObtenerPermisos()
            .resetClave = chkRestablecerClave.Checked
        End With

        Try
            ' Guardo el registro en base de datos
            Contexto.SaveChanges()
            If MostrarMensaje Then Quadralia.Aplicacion.MostrarMensajeNtfIco(My.Resources.TituloDatosGuardatos, My.Resources.DatosGuardados, ToolTipIcon.Info)

            ' Restablezco el formulario
            LimpiarDatos()
            CargarRegistro()
            Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera

            Return True
        Catch ex As Exception
            If MostrarMensaje Then Quadralia.Aplicacion.InformarError(ex)
            Return False
        End Try
    End Function
#End Region
#Region " RESTO DE FUNCIONES "
    ''' <summary>
    ''' Oculta / muestra el panel de b�squeda
    ''' </summary>
    Private Sub OcultarBusqueda(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOcultarBusqueda.Click
        Quadralia.KryptonForms.EsconderPanel(splSeparador, hdrBusqueda)
    End Sub

    ''' <summary>
    ''' Ordena los resultados del DGV de Resultados    
    ''' </summary>
    Private Sub OrdenarResultados(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvResultadosBuscador.ColumnHeaderMouseClick
        Quadralia.Formularios.Funcionalidad.OrdenarDGV(Of Usuario)(dgvResultadosBuscador, e)
    End Sub

    ''' <summary>
    ''' Control de las teclas de acci�n del usuario
    ''' </summary>
    Private Sub ControlTeclasBuscador(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvResultadosBuscador.KeyDown
        If Me.Registro Is Nothing Then Exit Sub

        Select Case e.KeyCode
            Case Keys.Delete
                e.Handled = True
                Eliminar()
            Case Keys.F2, Keys.Enter
                e.Handled = True
                Modificar()
        End Select
    End Sub

    ''' <summary>
    ''' Cuando el usuario hace doble click en el panel, desencadenamos la misma funci�n que si hiciera click en el bot�n
    ''' </summary>
    Private Sub OcultarBusquedaDobleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hdrBusqueda.DoubleClick
        ' Hay que hacerlo as�, no sirve con llamar a la funci�n directamente. si no, el bot�n del header se descontrola y desaparece el interior del panel
        DirectCast(sender, KryptonHeaderGroup).ButtonSpecs(0).PerformClick()
    End Sub

    ''' <summary>
    ''' Cuando el usuario hace doble click en el panel, desencadenamos la misma funci�n que si hiciera click en el bot�n
    ''' </summary>
    Private Sub OcultarDesgloseDobleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hdrPermisos.DoubleClick
        ' Hay que hacerlo as�, no sirve con llamar a la funci�n directamente. si no, el bot�n del header se descontrola y desaparece el interior del panel
        DirectCast(sender, KryptonHeaderGroup).ButtonSpecs(0).PerformClick()
    End Sub

    ''' <summary>
    ''' Lanzamos la b�squeda de registros si el usuario presiona ENTER en los campos de par�metros
    ''' </summary>
    Private Sub ManejadorBusquedasTexto(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtBUsuario.KeyUp
        If e.KeyCode = Keys.Enter Then
            e.Handled = True
            Buscar(Nothing, Nothing)
        End If
    End Sub

    ''' <summary>
    ''' Evita que salga el molesto error del DGV
    ''' </summary>
    Private Sub EvitarErrores(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvResultadosBuscador.DataError
        e.Cancel = True
    End Sub

    ''' <summary>
    ''' Pone el formulario en modo edici�n cuando el usuario hace doble click sobre un resultado del buscador
    ''' </summary>
    Private Sub ActivarEdicionRegistro(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvResultadosBuscador.CellDoubleClick
        If e.RowIndex > -1 Then Modificar()
    End Sub

    ''' <summary>
    ''' Nos desplaza a traves de los controles al pulsar enter
    ''' </summary>
    Private Sub EnterComoTab(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.Enter AndAlso (Me.ActiveControl Is Nothing OrElse Me.ActiveControl.Parent Is Nothing OrElse Not TypeOf (Me.ActiveControl.Parent) Is KryptonTextBox OrElse Not DirectCast(Me.ActiveControl.Parent, KryptonTextBox).Multiline) Then
            e.Handled = True
            SendKeys.Send("{TAB}")
        End If
    End Sub


#End Region
#Region " VALIDACIONES "
    Private Sub ValidarUsuario(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtUsuario.Validating
        If Not TypeOf (sender) Is KryptonTextBox Then Exit Sub

        If Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera AndAlso String.IsNullOrEmpty(DirectCast(sender, KryptonTextBox).Text) Then ' Que no est� vac�o
            epErrores.SetError(sender, My.Resources.TextBoxObligatorio)
        ElseIf Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera AndAlso (From Usuario In Contexto.Usuarios Where Usuario.usuario.ToUpper() = txtUsuario.Text.ToUpper() AndAlso Usuario.id <> Me.Registro.id AndAlso Not Usuario.fechaBaja.HasValue).Count > 0 Then ' que no exista un usuario
            epErrores.SetError(sender, "Ya existe otro usuario en activo con ese login")
        Else
            epErrores.SetError(sender, "")
        End If
    End Sub

    Private Sub ValidarEmail(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs)
        If Not TypeOf (sender) Is KryptonTextBox Then Exit Sub

        If Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera AndAlso Not String.IsNullOrEmpty(DirectCast(sender, KryptonTextBox).Text) AndAlso Not Quadralia.Validadores.QuadraliaValidadores.ValidarEmail(DirectCast(sender, KryptonTextBox).Text) Then ' que no exista un usuario
            epErrores.SetError(sender, "El correo electr�nico introducido no es v�lido. Por favor, rev�selo")
        Else
            epErrores.SetError(sender, "")
        End If
    End Sub

    Private Sub SoloNumeros(ByVal sender As Object, ByVal e As KeyPressEventArgs)
        Quadralia.Validadores.QuadraliaValidadores.SoloNumeros(sender, e, False)
    End Sub
#End Region
#Region " PERMISOS "
    ''' <summary>
    ''' Activa / desactiva los permisos en funci�n de la reserva seleccionada
    ''' </summary>
    Private Sub MarcarPermisos(ByVal Permisos As Integer)
        For i As Int16 = 0 To chkPermisos.Items.Count - 1
            chkPermisos.SetItemChecked(i, PuedeAccederA(Permisos, DirectCast(chkPermisos.Items(i), KryptonListItem).Tag))
        Next
    End Sub

    Private Function ObtenerPermisos() As Integer
        Dim Permisos As Integer = 0
        For Each Permiso As KryptonListItem In chkPermisos.CheckedItems
            Permisos += Permiso.Tag
        Next

        Return Permisos
    End Function

    ''' <summary>
    ''' Indica si el usuario puede acceder a determinadas partes del sistema
    ''' </summary>
    ''' <param name="Permiso">Nivel que queremos verificar que posee el usuario</param>
    Public Function PuedeAccederA(ByVal Valor As Integer, ByVal Permiso As Integer) As Boolean
        Return (Valor And Permiso) = Permiso
    End Function
#End Region
End Class
