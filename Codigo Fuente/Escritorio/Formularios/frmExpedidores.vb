Imports ComponentFactory.Krypton.Toolkit
Imports System.ComponentModel

Public Class frmExpedidores
#Region " DECLARACIONES "
    Private _EstadoFormulario As Quadralia.Enumerados.EstadoFormulario = Quadralia.Enumerados.EstadoFormulario.Espera  ' Indica el estado en el que se encuentra el formulario
    Private _Registro As Expedidor = Nothing
    Private _Contexto As Entidades = Nothing

    Private _dgvLoad As Boolean = False ' Bandera para evitar que se cargue la primera fila cuando se asigna un origen de datos al DGV
#End Region
#Region " PROPIEDADES "
    Public Overrides Property Estado() As Quadralia.Enumerados.EstadoFormulario
        Get
            Return _EstadoFormulario
        End Get
        Set(ByVal value As Quadralia.Enumerados.EstadoFormulario)
            _EstadoFormulario = value

            'Para evitar refrescos raros, cuando ponemos el formulario en estado de espera, volvemos a la pesta�a de datos
            hdrBusqueda.Panel.Enabled = (value = Quadralia.Enumerados.EstadoFormulario.Espera)
            tblOrganizadorDatos.Enabled = Not (value = Quadralia.Enumerados.EstadoFormulario.Espera)
            htmlObservaciones.Enabled = Not (value = Quadralia.Enumerados.EstadoFormulario.Espera)

            ' Tengo que establecer el foco en un control para que no quede "muerto" el ribbon
            If value = Quadralia.Enumerados.EstadoFormulario.Espera Then
                Me.ActiveControl = txtBNombre.TextBox
            Else
                Me.ActiveControl = txtCIF.TextBox
            End If

            ControlarBotones()
        End Set
    End Property

    ''' <summary>
    ''' Registro que se muestra actualmente en el formulario
    ''' </summary>
    Public Property Registro As Expedidor
        Get
            Return _Registro
        End Get
        Set(ByVal value As Expedidor)
            _Registro = value
            CargarRegistro(value)
        End Set
    End Property

    Public ReadOnly Property Contexto As Entidades
        Get
            If _Contexto Is Nothing Then
                _Contexto = cDAL.Instancia.getContext()
                _Contexto.ContextOptions.LazyLoadingEnabled = True
            End If
            Return _Contexto
        End Get
    End Property

    Public Property IdRegistro As Long
        Get
            If Me.Registro Is Nothing Then Return -1 Else Return Registro.id
        End Get
        Set(ByVal value As Long)
            If value > -1 Then Me.Registro = (From it As Expedidor In Me.Contexto.Expedidores Where it.id = value Select it).FirstOrDefault
        End Set
    End Property
#End Region
#Region " IMPLEMENTACIONES NECESARIAS DEL FORMULARIO HIJO "
    ''' <summary>
    ''' Tab del ribbon asociado a este formulario
    ''' </summary>
    Public Overrides ReadOnly Property Tab() As ComponentFactory.Krypton.Ribbon.KryptonRibbonTab
        Get
            Return frmPrincipal.tabExpedidores
        End Get
    End Property
#End Region
#Region " LIMPIEZA "
    ''' <summary>
    ''' Limpia los controles relacionados con la b�squeda de registros
    ''' </summary>
    Private Sub LimpiarBusqueda()
        txtBNombre.Clear()
        txtBCIF.Clear()
        txtBRegistro.Clear()

        dgvResultadosBuscador.DataSource = Nothing
        dgvResultadosBuscador.Rows.Clear()
    End Sub

    ''' <summary>
    ''' Limpia los controles relacionados con la edicion de registros
    ''' </summary>
    Private Sub LimpiarDatos()
        txtCIF.Clear()
        txtCodigo.Clear()
        txtRazonSocial.Clear()
        txtTelefono1.Clear()
        txtTelefono2.Clear()
        txtFax.Clear()
        txtEmail.Clear()
        txtWeb.Clear()
        txtDireccion.Clear()
        txtRegistro.Clear()
        txtCaducidad.Clear()

        vsrLogotipo.Clear()
        chkPredeterminado.Checked = False

        htmlObservaciones.Clear()

        chkReactivarRegistro.Checked = False
        chkReactivarRegistro.Visible = False

        epErrores.Clear()
    End Sub

    ''' <summary>
    ''' Borra todos los controles del formulario
    ''' </summary>
    Private Sub LimpiarTodo()
        LimpiarBusqueda()
        LimpiarDatos()
    End Sub
#End Region
#Region " CONTROL DE BOTONES "
    Private Sub ControlarBotones()
        ControlarRibbon()
    End Sub

    ''' <summary>
    ''' Controla el cambio de botones del formulario principal
    ''' </summary>
    Private Sub ControlarRibbon()
        If Me.ParentForm Is Nothing Then Exit Sub
        If Not TypeOf Me.ParentForm Is frmPrincipal Then Exit Sub

        With DirectCast(Me.ParentForm, frmPrincipal)
            .krgtExpedidoresAccionesEdicion.Visible = (Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera)
            .krgtExpedidoresAccionesEspera.Visible = Not .krgtExpedidoresAccionesEdicion.Visible
            .mnuRapidoGuardar.Enabled = .krgtExpedidoresAccionesEdicion.Visible

            .btnExpedidoresAccionesModificar.Enabled = (Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera) AndAlso Me.Registro IsNot Nothing
            .btnExpedidoresAccionesEliminar.Enabled = .btnExpedidoresAccionesModificar.Enabled
        End With
    End Sub
#End Region
#Region " RUTINA DE INICIO / FIN Y CARGA DE DATOS MAESTROS "
    ''' <summary>
    ''' Rutina de inicio del formulario
    ''' </summary>
    Private Sub Inicio(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Inicializaciones
        Me.Tag = Me.Text
        tabGeneral.SelectedIndex = 0

        ' Cargamos los datos maestros
        CargarDatosMaestros()

        LimpiarTodo()
        ConfigurarEntorno()

        ' Oculto el buscador
        If Not cConfiguracionAplicacion.Instancia.BuscadoresAbiertos Then OcultarBusquedaDobleClick(hdrBusqueda, Nothing)

        ' No quiero nuevos Datos
        dgvResultadosBuscador.AutoGenerateColumns = False

        Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera

        For Each UnControl As Control In tblOrganizadorDatos.Controls
            Quadralia.Formularios.Funcionalidad.ManejadorTabPorIntro(UnControl)
        Next
    End Sub

    ''' <summary>
    ''' Carga los datos maestros    
    ''' </summary>
    Private Sub CargarDatosMaestros()
    End Sub
#End Region
#Region " BUSQUEDA, CARGA DE Y EDICION DE REGISTROS "
    ''' <summary>
    ''' Realiza la b�squeda de registros coincidentes con los par�metros especificados
    ''' </summary>
    Private Sub Buscar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBBuscar.Click
        Dim Registros = From It As Expedidor In Contexto.Expedidores _
                        Where ((chkBEliminados.Checked OrElse Not It.fechaBaja.HasValue) _
                               AndAlso (String.IsNullOrEmpty(txtBCIF.Text) OrElse It.cif.ToUpper.Contains(txtBCIF.Text.ToUpper())) _
                               AndAlso (String.IsNullOrEmpty(txtBNombre.Text) OrElse It.razonSocial.ToUpper.Contains(txtBNombre.Text.ToUpper())) _
                               AndAlso (String.IsNullOrEmpty(txtBRegistro.Text) OrElse It.registro.ToUpper.Contains(txtBRegistro.Text.ToUpper())) _
                               ) _
                        Distinct Select It

        If Registros IsNot Nothing Then
            _dgvLoad = True
            dgvResultadosBuscador.DataSource = Registros.Distinct.ToList
            _dgvLoad = False
        End If

        dgvResultadosBuscador.ClearSelection()
        dgvResultadosBuscador.Focus()
    End Sub

    ''' <summary>
    ''' Se prepara el formulario para introducir un nuevo registro
    ''' </summary>    
    Public Overrides Sub Nuevo()
        LimpiarDatos()
        Me.Estado = Quadralia.Enumerados.EstadoFormulario.Anhadiendo

        Dim Aux = New Expedidor

        ' Lo a�ado al contexto
        Contexto.Expedidores.AddObject(Aux)
        Me.Registro = Aux
    End Sub

    ''' <summary>
    ''' Se carga el registro seleccionado el formulario
    ''' </summary>
    Private Sub Cargar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvResultadosBuscador.SelectionChanged
        If dgvResultadosBuscador.SelectedRows.Count > 0 Then
            Me.Registro = dgvResultadosBuscador.SelectedRows(0).DataBoundItem
        Else
            Me.Registro = Nothing
        End If
    End Sub

    ''' <summary>
    ''' Carga el registro actual del formulario
    ''' </summary>
    Private Sub CargarRegistro()
        CargarRegistro(Me.Registro)
    End Sub

    ''' <summary>
    ''' Se carga un registro en el formulario
    ''' </summary>
    Private Sub CargarRegistro(ByVal Instancia As Expedidor)
        ' Si el DGV est� vinculando un origen de datos, no cargo nada
        If _dgvLoad Then Exit Sub

        ' Limpio el formulario y cargo el registro
        LimpiarDatos()
        ControlarBotones()

        If Instancia Is Nothing Then Exit Sub

        With Instancia
            txtCodigo.Text = .codigo
            txtCIF.Text = .cif
            txtRazonSocial.Text = .razonSocial
            txtTelefono1.Text = .telefono1
            txtTelefono2.Text = .telefono2
            txtFax.Text = .fax
            txtEmail.Text = .email
            txtWeb.Text = .web
            txtDireccion.Text = .direccion
            chkPredeterminado.Checked = .predeterminada
            txtRegistro.Text = .registro
            txtCaducidad.Text = .caducidad
            vsrLogotipo.Item = .Logotipo

            chkReactivarRegistro.Visible = .fechaBaja.HasValue

            htmlObservaciones.Text = .observaciones

            Me.Text = String.Format("{0} [{1}]", Me.Tag, .razonSocial)
        End With

        _Registro = Instancia
    End Sub

    ''' <summary>
    ''' Preparamos el formulario para modificar un registro cargado
    ''' </summary>
    Public Overrides Sub Modificar()
        If Me.Registro IsNot Nothing Then Me.Estado = Quadralia.Enumerados.EstadoFormulario.Editando
    End Sub

    ''' <summary>
    ''' Eliminaci�n l�gica del registro
    ''' </summary>
    Public Overrides Sub Eliminar()
        'If dgvResultadosBuscador.SelectedRows.Count <> 1 Then Exit Sub

        If Registro IsNot Nothing Then
            If Quadralia.Aplicacion.MostrarMessageBox(My.Resources.ConfirmarEliminacion, My.Resources.TituloConfirmarEliminacion, MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
                Registro.fechaBaja = Date.Now
                Contexto.SaveChanges()
                If dgvResultadosBuscador.SelectedRows.Count > 0 Then Buscar(Nothing, Nothing) Else Cargar(Nothing, Nothing)
            End If
        End If
    End Sub

    ''' <summary>
    ''' Cancela la edici�n del registro
    ''' </summary>
    Public Overrides Sub Cancelar()
        If Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera Then Exit Sub

        ' Cambio el estado del formulario
        Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera

        ' Limpio los datos y desecho los cambios
        Quadralia.Linq.CancelarCambios(Me.Contexto)

        ' Limpio los datos
        LimpiarDatos()

        ' Cargo el registro seleccionado
        Cargar(Nothing, Nothing)
    End Sub

    ''' <summary>
    ''' Se guardan los datos en la base de datos
    ''' </summary>
    ''' <param name="MostrarMensaje">Indica si se muestra un mensaje con el resultado de la funci�n</param>
    Public Overrides Function Guardar(Optional ByVal MostrarMensaje As Boolean = True) As Boolean
        If Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera Then Return True

        ' Valido los controles
        Me.ValidateChildren(ValidationConstraints.Visible + ValidationConstraints.Enabled)

        If epErrores.HasErrors Then
            If MostrarMensaje Then Quadralia.Aplicacion.MostrarMessageBox(My.Resources.TituloErroresEncontrados, My.Resources.ErroresEncontrados, MessageBoxButtons.OK, MessageBoxIcon.Error)
            epErrores.Controles(0).Focus()
            Return False
        End If

        ' Guardo los datos en el registro
        With Registro
            .codigo = txtCodigo.Text
            .cif = txtCIF.Text
            .razonSocial = txtRazonSocial.Text
            .telefono1 = txtTelefono1.Text
            .telefono2 = txtTelefono2.Text
            .fax = txtFax.Text
            .email = txtEmail.Text
            .web = txtWeb.Text
            .observaciones = htmlObservaciones.Text
            .predeterminada = chkPredeterminado.Checked
            .direccion = txtDireccion.Text
            .registro = txtRegistro.Text
            .Logotipo = vsrLogotipo.Item
            .caducidad = txtCaducidad.Text

            If chkReactivarRegistro.Visible AndAlso chkReactivarRegistro.Checked Then .fechaBaja = Nothing

            ' Miro si tengo que restablecer los predeterminados
            If .predeterminada Then
                For Each Exp In (From it As Expedidor In Contexto.Expedidores Where it.predeterminada AndAlso it.id <> .id Select it)
                    Exp.predeterminada = False
                Next
            End If
        End With

        Try
            ' Guardo el registro en base de datos
            Contexto.SaveChanges()

            ' Si estoy a�adiendo, obtengo un n�mero de Albar�n
            If Me.Estado = Quadralia.Enumerados.EstadoFormulario.Anhadiendo AndAlso String.IsNullOrEmpty(txtCodigo.Text) Then
                Dim Aux As New Data.Objects.ObjectParameter("codigoExp", Registro.codigo)
                Dim Resultado = Contexto.CalcularCodigoExpedidor(Registro.id, Aux)
                Registro.codigo = Aux.Value
                Contexto.AcceptAllChanges()
            End If

            If MostrarMensaje Then Quadralia.Aplicacion.MostrarMensajeNtfIco(My.Resources.TituloDatosGuardatos, My.Resources.DatosGuardados, ToolTipIcon.Info)

            ' Restablezco el formulario
            LimpiarDatos()
            CargarRegistro()
            Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera

            Return True
        Catch ex As Exception
            If MostrarMensaje Then Quadralia.Aplicacion.InformarError(ex)
            Return False
        End Try
    End Function
#End Region
#Region " RESTO DE FUNCIONES "
    ''' <summary>
    ''' Oculta / muestra el panel de b�squeda
    ''' </summary>
    Private Sub OcultarBusqueda(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOcultarBusqueda.Click
        Quadralia.KryptonForms.EsconderPanel(splSeparador, hdrBusqueda)
    End Sub

    ''' <summary>
    ''' Ordena los resultados del DGV de Resultados    
    ''' </summary>
    Private Sub OrdenarResultados(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvResultadosBuscador.ColumnHeaderMouseClick
        Quadralia.Formularios.Funcionalidad.OrdenarDGV(Of Expedidor)(dgvResultadosBuscador, e)
    End Sub

    ''' <summary>
    ''' Cuando el usuario hace doble click en el panel, desencadenamos la misma funci�n que si hiciera click en el bot�n
    ''' </summary>
    Private Sub OcultarBusquedaDobleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hdrBusqueda.DoubleClick
        btnOcultarBusqueda.PerformClick()
    End Sub

    ''' <summary>
    ''' Lanzamos la b�squeda de registros si el usuario presiona ENTER en los campos de par�metros
    ''' </summary>
    Private Sub ManejadorBusquedasTexto(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtBNombre.KeyDown, txtBRegistro.KeyDown, txtBCIF.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.Handled = True
            Buscar(Nothing, Nothing)
        End If
    End Sub

    ''' <summary>
    ''' Evita que salga el molesto error del DGV
    ''' </summary>
    Private Sub EvitarErrores(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvResultadosBuscador.DataError
#If Not Debug Then
        e.Cancel = True
#End If
    End Sub

    ''' <summary>
    ''' Pone el formulario en modo edici�n cuando el usuario hace doble click sobre un resultado del buscador
    ''' </summary>
    Private Sub ActivarEdicionRegistro(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvResultadosBuscador.CellDoubleClick
        If e.RowIndex > -1 Then Modificar()
    End Sub
#End Region
#Region " VALIDACIONES "
    ''' <summary>
    ''' Verifica que se haya introducido texto en un textbox marcado como obligatorio
    ''' </summary>
    Private Sub ValidarTextBoxObligatorio(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtRazonSocial.Validating
        If Not TypeOf (sender) Is KryptonTextBox Then Exit Sub

        If Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera AndAlso String.IsNullOrEmpty(DirectCast(sender, KryptonTextBox).Text) Then
            epErrores.SetError(sender, My.Resources.TextBoxObligatorio)
        Else
            epErrores.SetError(sender, "")
        End If
    End Sub

    ''' <summary>
    ''' Obliga a un textbox a aceptar �nicamente valore num�ricos
    ''' </summary>
    Private Sub SoloEnteros(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtTelefono2.KeyPress, txtTelefono1.KeyPress, txtFax.KeyPress
        Quadralia.Validadores.QuadraliaValidadores.SoloNumeros(sender, e)
    End Sub

    ''' <summary>
    ''' Verifica que se haya introducido una fecha en un datetimepicker marcado como obligatorio
    ''' </summary>
    Private Sub SoloDecimales(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        Quadralia.Validadores.QuadraliaValidadores.SoloNumerosYDecimales(sender, e)
    End Sub

    ''' <summary>
    ''' Obliga a un textbox a aceptar �nicamente valore num�ricos
    ''' </summary>
    Private Sub ValidarDTPObligatorio(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs)
        If Not TypeOf (sender) Is KryptonDateTimePicker Then Exit Sub

        If Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera AndAlso IsDBNull(DirectCast(sender, KryptonDateTimePicker).ValueNullable) Then
            epErrores.SetError(sender, My.Resources.DTPObligatorio)
        Else
            epErrores.SetError(sender, "")
        End If
    End Sub

    ''' <summary>
    ''' Valida el NIF / CIF introducido por el usuario
    ''' </summary>
    Private Sub ValidarCIF(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtCIF.Validating
        If Not TypeOf (sender) Is KryptonTextBox Then Exit Sub

        Dim Ctrl As KryptonTextBox = DirectCast(sender, KryptonTextBox)
        If Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera OrElse String.IsNullOrEmpty(Ctrl.Text) Then
            epErrores.SetError(sender, "")
        ElseIf (Quadralia.Validadores.QuadraliaValidadores.esNif(Ctrl.Text) OrElse Quadralia.Validadores.QuadraliaValidadores.esCif(Ctrl.Text)) AndAlso Not Quadralia.Validadores.QuadraliaValidadores.validarNIFCIF(Ctrl.Text) Then
            epErrores.SetError(sender, "El CIF/NIF introducido no es correcto. Por favor, rev�selo")
        ElseIf Registro IsNot Nothing AndAlso (From cli As Cliente In Contexto.Clientes Where cli.cif.ToUpper = Ctrl.Text.ToUpper And cli.id <> Registro.id Select cli).Count > 0 Then ' Duplicados
            epErrores.SetError(sender, "El CIF/NIF introducido ya se encuentra actualmente en uso")
        Else
            epErrores.SetError(sender, "")
        End If
    End Sub

    ''' <summary>
    ''' Valida que el usuario introduzca una direcci�n de correo electr�nico v�lida
    ''' </summary>
    Private Sub ValidarEmail(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtEmail.Validating
        If Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera AndAlso Not String.IsNullOrEmpty(txtEmail.Text.Trim) AndAlso Not Quadralia.Validadores.QuadraliaValidadores.ValidarEmail(txtEmail.Text) Then
            epErrores.SetError(txtEmail, "El correo electr�nico introducido no es correcto. Por favor, rev�selo")
        Else
            epErrores.SetError(txtEmail, "")
        End If
    End Sub
#End Region
#Region " SEGURIDAD "
    ''' <summary>
    ''' Configura el formulario en base a los permisos del usuario
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ConfigurarEntorno()
    End Sub
#End Region
End Class
