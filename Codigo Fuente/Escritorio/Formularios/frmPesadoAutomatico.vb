﻿Public Class frmPesadoAutomatico
#Region " COLORES "
    Private Shared ColorPesoEstable As Color = Color.FromArgb(122, 142, 33)
    Private Shared ColorPesoInestable As Color = Color.FromArgb(167, 116, 22)
    Private Shared ColorPesoZero As Color = Color.FromArgb(30, 57, 91)
    Private Shared ColorPesoInvalido As Color = Color.FromArgb(197, 50, 45)
#End Region
#Region " DECLARACIONES "
    Private _EstadoFormulario As Quadralia.Enumerados.EstadoFormulario = Quadralia.Enumerados.EstadoFormulario.Espera  ' Indica el estado en el que se encuentra el formulario
    Private _Contexto As Entidades = Nothing
    Private _LoteEntrada As LoteEntrada = Nothing
    Private _Expedidor As Expedidor = Nothing
    Private _Disenho As String = String.Empty

    Private WithEvents _ListenerBascula As qBasc.ListenerBascula = Nothing
    Private Property _ImpresionAutomatica As Boolean = False
#End Region
#Region " PROPIEDADES "
    Public Overrides Property Estado() As Quadralia.Enumerados.EstadoFormulario
        Get
            Return _EstadoFormulario
        End Get
        Set(ByVal value As Quadralia.Enumerados.EstadoFormulario)
            _EstadoFormulario = value

            ' Sincronizo los botones
            Me.Activo = Me.Activo
            ControlarBotones()
        End Set
    End Property

    ''' <summary>
    ''' Registro que se muestra actualmente en el formulario
    ''' </summary>
    Public Property LoteEntrada As LoteEntrada
        Get
            Return _LoteEntrada
        End Get
        Set(ByVal value As LoteEntrada)
            _LoteEntrada = value
            CargarLoteEntrada(value)
        End Set
    End Property

    Public Property IdLoteEntrada As Integer
        Get
            If _LoteEntrada Is Nothing Then Return 0
            Return _LoteEntrada.id
        End Get
        Set(value As Integer)
            _LoteEntrada = (From lot As LoteEntrada In Me.Contexto.LotesEntrada Where lot.id = value).FirstOrDefault
        End Set
    End Property

    ''' <summary>
    ''' Registro que se muestra actualmente en el formulario
    ''' </summary>
    Public Property Expedidor As Expedidor
        Get
            Return _Expedidor
        End Get
        Set(ByVal value As Expedidor)
            _Expedidor = value
            CargarExpedidorYDisenho(value, Disenho)
        End Set
    End Property

    Public Property IdExpedidor As Integer
        Get
            If _Expedidor Is Nothing Then Return 0
            Return _Expedidor.id
        End Get
        Set(value As Integer)
            Expedidor = (From exp As Expedidor In Me.Contexto.Expedidores Where exp.id = value).FirstOrDefault
        End Set
    End Property

    Public Property Disenho As String
        Get
            Return _Disenho
        End Get
        Set(value As String)
            _Disenho = value
            CargarExpedidorYDisenho(Expedidor, value)
        End Set
    End Property

    Public ReadOnly Property Contexto As Entidades
        Get
            If _Contexto Is Nothing Then
                _Contexto = cDAL.Instancia.getContext()
            End If
            Return _Contexto
        End Get
    End Property

    ''' <summary>
    ''' Obtiene/Establece si se imprimen las etiquetas de forma automática cuando exista un peso estable
    ''' </summary>
    Public Property ImpresionAutomatica As Boolean
        Get
            Return _ImpresionAutomatica
        End Get
        Set(value As Boolean)
            _ImpresionAutomatica = value
            ControlarBotones()
        End Set
    End Property

    Public ReadOnly Property ListenerBascula As qBasc.ListenerBascula
        Get
            If _ListenerBascula Is Nothing Then _ListenerBascula = New qBasc.ListenerBascula
            Return _ListenerBascula
        End Get
    End Property

    Private Property Activo As Boolean
        Get
            Return tblOrganizadorDatosEntrada.Enabled
        End Get
        Set(value As Boolean)
            value = ListenerBascula.Conectado AndAlso LoteEntrada IsNot Nothing AndAlso Expedidor IsNot Nothing

            lblAdvetenciaEstado.Visible = Not ListenerBascula.Conectado
            dgvHistoricoEtiquetas.Enabled = value
            tblOrganizadorDatosEntrada.Enabled = value

            ControlarBotones()
        End Set
    End Property
#End Region
#Region " IMPLEMENTACIONES NECESARIAS DEL FORMULARIO HIJO "
    ''' <summary>
    ''' Tab del ribbon asociado a este formulario
    ''' </summary>
    Public Overrides ReadOnly Property Tab() As ComponentFactory.Krypton.Ribbon.KryptonRibbonTab
        Get
            Return frmPrincipal.tabGeneracionEtiquetas
        End Get
    End Property
#End Region
#Region " LIMPIEZA "
    ''' <summary>
    ''' Borra todos los controles del formulario
    ''' </summary>
    Public Sub LimpiarTodo()
        LimpiarDatosEntrada()
        LimpiarExpedidorYDisenho()
        LimpiarUltimasEtiquetas()
    End Sub

    Public Sub LimpiarDatosEntrada()
        txtEntradaNumberoAlbaran.Text = String.Empty
        txtEntradaPesoTotal.Text = String.Empty

        txtPesoRestante.Text = "0,000"

        lblAdvetenciaEstado.Visible = Not ListenerBascula.Conectado
        txtPesoRestante.StateCommon.ShortText.Color1 = ColorPesoZero
    End Sub

    Public Sub LimpiarExpedidorYDisenho()
        txtExpedidor.Text = String.Empty
        txtDisenho.Text = String.Empty
    End Sub

    Public Sub LimpiarUltimasEtiquetas()
        dgvHistoricoEtiquetas.DataSource = Nothing
        dgvHistoricoEtiquetas.Rows.Clear()
    End Sub
#End Region
#Region " CONTROL DE BOTONES "
    Private Sub ControlarBotones()
        ControlarRibbon()
    End Sub

    Private Sub ControlarRibbon()
        If Me.ParentForm Is Nothing Then Exit Sub
        If Not TypeOf Me.ParentForm Is frmPrincipal Then Exit Sub

        With DirectCast(Me.ParentForm, frmPrincipal)
            .chkGenerarEtiquetasImpresionAutomatica.Visible = True
            .chkGenerarEtiquetasImpresionAutomatica2.Visible = False

            .chkGenerarEtiquetasImpresionAutomatica.Checked = Me.ImpresionAutomatica

        End With
    End Sub
#End Region
#Region " RUTINA DE INICIO / FIN Y CARGA DE DATOS MAESTROS "
    ''' <summary>
    ''' Controla el cierre del formulario
    ''' </summary>
    Private Sub CerrarFormulario(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        If ListenerBascula.EstaEjecutando Then ListenerBascula.DetenerEscucha()
    End Sub

    ''' <summary>
    ''' Rutina de inicio del formulario
    ''' </summary>
    Private Sub Inicio(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Tag = Me.Text

        ' Establezco las opciones por defecto para evitar bucles
        _ImpresionAutomatica = frmPrincipal.chkGenerarEtiquetasImpresionAutomatica.Checked

        Quadralia.Formularios.AutoTabular(Me)
        CheckForIllegalCrossThreadCalls = False

        ' No quiero nuevos Datos
        dgvHistoricoEtiquetas.AutoGenerateColumns = False

        LimpiarTodo()


        ' Cargamos los datos maestros
        CargarDatosMaestros()

        Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera

        ' Cargo la configuración de la báscula y comienzo la escucha
        InicializarBascula()
    End Sub

    ''' <summary>
    ''' Carga los datos maestros
    ''' </summary>
    Public Sub CargarDatosMaestros()
        Dim FechaInicio As DateTime = DateTime.Now.Date + New TimeSpan(0, 0, 0)
        Dim FechaFin As DateTime = DateTime.Now.Date + New TimeSpan(23, 59, 59)

        ' Cargo las etiquetas
        With dgvHistoricoEtiquetas
            .DataSource = Nothing
            .Rows.Clear()

            .DataSource = (From etq As Etiqueta In Me.Contexto.Etiquetas Where Not etq.fechaBaja.HasValue AndAlso etq.fecha >= FechaInicio AndAlso etq.fecha <= FechaFin Order By etq.fecha Descending Select etq).Take(20).ToList
        End With
    End Sub

    Public Sub InicializarBascula()
        With ListenerBascula
            If .EstaEjecutando Then .DetenerEscucha()

            .DireccionIP = cConfiguracionPrograma.Instancia.direccionImpresora
            .IntervaloMuestreo = cConfiguracionPrograma.Instancia.IntervaloMuestreo
            .PesoMinimo = cConfiguracionPrograma.Instancia.PesoMinimo / 1000
            .Puerto = cConfiguracionPrograma.Instancia.puertoImpresora
            .VariacionPesoMinima = cConfiguracionPrograma.Instancia.variacionMinima / 1000
            .IntervaloEstable = cConfiguracionPrograma.Instancia.IntervaloEstable

            .ComenzarEscucha()
        End With
    End Sub
#End Region
#Region " BUSQUEDA, CARGA DE Y EDICION DE REGISTROS "
    ''' <summary>
    ''' Carga el registro actual del formulario
    ''' </summary>
    Private Sub CargarLoteEntrada()
        CargarLoteEntrada(Me.LoteEntrada)
    End Sub

    ''' <summary>
    ''' Se carga un registro en el formulario
    ''' </summary>
    Private Sub CargarLoteEntrada(ByVal Instancia As LoteEntrada)
        ' Si el DGV está vinculando un origen de datos, no cargo nada
        LimpiarDatosEntrada()
        ControlarBotones()
        Me.Activo = Me.Activo

        If Instancia Is Nothing Then Exit Sub

        With Instancia
            txtPesoRestante.Text = String.Format("{0:0.000}", .TotalPesoRestante)

            txtEntradaNumberoAlbaran.Text = .codigo
            txtEntradaPesoTotal.Text = String.Format("{0:0.000} Kg", .TotalPeso)
        End With

        _LoteEntrada = Instancia

        Dim numEtiquetas As Int32 = (From eti In _Contexto.Etiquetas.Where(Function(p) p.LoteEntradaLinea.LoteEntrada.id = _LoteEntrada.id AndAlso
                                         p.fechaBaja Is Nothing)).Count()
        Dim pesoEtiquetas As Decimal = 0

        If (numEtiquetas > 0) Then
            pesoEtiquetas = (From eti In _Contexto.Etiquetas.Where(Function(p) p.LoteEntradaLinea.LoteEntrada.id = _LoteEntrada.id AndAlso
                                p.fechaBaja Is Nothing)).Sum(Function(s) s.cantidad)
        End If

        lblPesado.Text = "Etiquetas: " + numEtiquetas.ToString() + "   Peso acumulado: " + pesoEtiquetas.ToString() + ""
    End Sub

    ''' <summary>
    ''' Carga el registro actual del formulario
    ''' </summary>
    Private Sub CargarExpedidorYDisenho()
        CargarExpedidorYDisenho(Me.Expedidor, Me.Disenho)
    End Sub

    ''' <summary>
    ''' Se carga un registro en el formulario
    ''' </summary>
    Private Sub CargarExpedidorYDisenho(ByVal Instancia As Expedidor, Disenho As String)
        ' Si el DGV está vinculando un origen de datos, no cargo nada
        LimpiarExpedidorYDisenho()
        ControlarBotones()
        Me.Activo = Me.Activo

        If Instancia Is Nothing Then Exit Sub

        With Instancia
            txtExpedidor.Text = .razonSocial
            txtDisenho.Text = Disenho
        End With

        _Expedidor = Instancia
        _Disenho = Disenho
    End Sub

    ''' <summary>
    ''' Refresca los datos del formulario tras una pesada estable
    ''' </summary>
    Private Sub RefrescarDatos()
        CargarDatosMaestros()
        CargarLoteEntrada()
        CargarExpedidorYDisenho()
    End Sub

    ''' <summary>
    ''' Establece los datos de expedidor y lote de entrada
    ''' </summary>
    Private Sub SeleccionarParametros(sender As Object, e As EventArgs) Handles ButtonSpecHeaderGroup1.Click
        Dim FormularioBusqueda As New frmSeleccionarExpedidorLoteEntrada() With {.Expedidor = Expedidor, .Lote = LoteEntrada, .Contexto = Contexto, .Disenho = Disenho}

        If FormularioBusqueda.ShowDialog() <> Windows.Forms.DialogResult.OK Then Exit Sub
        Expedidor = FormularioBusqueda.Expedidor
        LoteEntrada = FormularioBusqueda.Lote
        Disenho = FormularioBusqueda.Disenho
    End Sub
#End Region
#Region " RESTO DE FUNCIONES "
    ''' <summary>
    ''' Ordena los resultados del DGV de Resultados    
    ''' </summary>
    Private Sub OrdenarResultados(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvHistoricoEtiquetas.ColumnHeaderMouseClick
        Quadralia.Formularios.Funcionalidad.OrdenarDGV(Of Etiqueta)(dgvHistoricoEtiquetas, e)
    End Sub

    ''' <summary>
    ''' Evita que salga el molesto error del DGV
    ''' </summary>
    Private Sub EvitarErrores(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvHistoricoEtiquetas.DataError
        e.Cancel = True
    End Sub

    ''' <summary>
    ''' Abre la ventana para la gestión de etiquetas
    ''' </summary>
    Private Sub AbrirGestionEtiqueta(sender As Object, e As DataGridViewCellEventArgs) Handles dgvHistoricoEtiquetas.CellDoubleClick
        ' Validaciones
        If Not Quadralia.Aplicacion.PuedeAccederA(Quadralia.Aplicacion.Permisos.ETIQUETAS) Then Exit Sub
        If e.RowIndex = -1 Then Exit Sub

        Dim Etiqueta As Etiqueta = dgvHistoricoEtiquetas.Rows(e.RowIndex).DataBoundItem
        If Etiqueta Is Nothing Then Exit Sub

        Dim Formulario As frmGestionEtiquetas = Quadralia.Aplicacion.AbrirFormulario(Me.ParentForm, "Escritorio.frmGestionEtiquetas", Quadralia.Aplicacion.Permisos.ETIQUETAS, My.Resources.Etiqueta_16)
        If Formulario Is Nothing Then Exit Sub

        Formulario.IdRegistro = Etiqueta.id
    End Sub
#End Region
#Region " VALIDACIONES "

#End Region
#Region " EVENTOS BASCULA "
    ''' <summary>
    ''' Indica que se ha producido un cambio en la conexión con la báscula
    ''' </summary>
    Private Sub ConexionConBasculaDisponible(sender As Object, e As EventArgs) Handles _ListenerBascula.CambioConexion
        Me.Activo = Me.Activo
    End Sub

    ''' <summary>
    ''' Se utiliza para reflejar la lectura de la báscula en el formulario
    ''' </summary>
    Private Sub LecturaPeso(sender As Object, Lectura As qBasc.RespuestaBascula) Handles _ListenerBascula.LecturaPeso
        txtPeso.Text = String.Format("{0:0.000}", Lectura.Peso)

        ' Si me excedo lo pongo en rojo
        If LoteEntrada IsNot Nothing AndAlso LoteEntrada.TotalPesoRestante < Lectura.Peso Then
            txtPeso.StateCommon.ShortText.Color1 = ColorPesoInvalido
            Exit Sub
        End If

        ' Pongo el texto del color correspondiente
        Select Case Lectura.Estado
            Case qBasc.RespuestaBascula.EstadosPesada.Indefinido
                txtPeso.StateCommon.ShortText.Color1 = ColorPesoInvalido
            Case qBasc.RespuestaBascula.EstadosPesada.Midiendo, qBasc.RespuestaBascula.EstadosPesada.MidiendoConTara
                txtPeso.StateCommon.ShortText.Color1 = ColorPesoInestable
            Case qBasc.RespuestaBascula.EstadosPesada.PesoEstable, qBasc.RespuestaBascula.EstadosPesada.PesoEstableConTara
                txtPeso.StateCommon.ShortText.Color1 = ColorPesoEstable
            Case qBasc.RespuestaBascula.EstadosPesada.Zero
                txtPeso.StateCommon.ShortText.Color1 = ColorPesoZero
        End Select
    End Sub

    ''' <summary>
    ''' Se produce una pesada estable con los parámetros establecidos
    ''' </summary>
    'Private Sub LecturaPesoEstable(sender As Object, Lectura As qBasc.RespuestaBascula) Handles _ListenerBascula.LecturaPesoEstable
    '    GenerarEtiqueta(Lectura.Peso)
    'End Sub
#End Region
#Region " IMPRIMIR "
    ''' <summary>
    ''' Imprime el registro actual
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Imprimir(Etiqueta As Etiqueta)
        Dim Informe As cInforme = Nothing
        If Not String.IsNullOrEmpty(Etiqueta.DisenhoPred) Then Informe = Quadralia.Aplicacion.GestorInformes.ObtenerInforme(Etiqueta.DisenhoPred, "EtiquetasTrazabilidad")

        Imprimir(Etiqueta, Informe)
    End Sub

    ''' <summary>
    ''' Imprime el registro actual
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Imprimir(Etiqueta As Etiqueta, ByVal Informe As cInforme)
        ' Validaciones
        If Etiqueta Is Nothing Then Exit Sub
        If Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera Then Exit Sub

        ' Imprimo la lista
        Dim Lista As New List(Of Etiqueta)
        Lista.Add(Etiqueta)
        Imprimir(Lista, Informe)
    End Sub

    ''' <summary>
    ''' Imprime los registros especificados como parámetro de entrada
    ''' </summary>
    Private Sub Imprimir(Etiquetas As List(Of Etiqueta), ByVal Informe As cInforme)
        If Not Quadralia.Aplicacion.PuedeAccederA(Quadralia.Aplicacion.Permisos.ETIQUETAS) Then Exit Sub
        If Etiquetas Is Nothing OrElse Etiquetas.Count = 0 Then Exit Sub

        Dim Formulario As frmVisorInforme = Quadralia.Aplicacion.AbrirInforme(Nothing, frmVisorInforme.Informe.EtiquetasTrazabilidad, Quadralia.Aplicacion.Permisos.ETIQUETAS, Nothing, False)
        If Formulario Is Nothing Then Exit Sub

        If Informe Is Nothing Then
            ' Obtengo el informe asignado en la etiqueta
            If String.IsNullOrEmpty(Etiquetas(0).DisenhoPred) Then
                Informe = Quadralia.Aplicacion.GestorInformes.Informes("EtiquetasTrazabilidad").Values(0)
            Else
                Informe = Quadralia.Aplicacion.GestorInformes.ObtenerInforme(Etiquetas(0).DisenhoPred, "EtiquetasTrazabilidad")
            End If
        End If

        Formulario.Datos = Etiquetas
        If Informe IsNot Nothing Then Formulario.Reporte = Informe.Documento

        If (Informe IsNot Nothing AndAlso Informe.Nombre = "Pulpo de Lonja - Pesca de rias") Then
            Formulario.RefrescarDatos(False)
        Else
            If (_Disenho.ToUpper().Contains("MAKRO")) Then
                Formulario.RefrescarDatos(False, "MAKRO")
            Else
                Formulario.RefrescarDatos()
            End If
        End If

        Formulario.Imprimir(cConfiguracionAplicacion.Instancia.Impresora, cConfiguracionAplicacion.Instancia.Orientacion, cConfiguracionAplicacion.Instancia.Formato)

        Formulario.Close()
    End Sub
#End Region
#Region " METODOS PUBLICOS "
    ''' <summary>
    ''' Generación manual de etiquetas
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub GenerarEtiquetaManual()
        Dim Aux As Double = 0
        If Not Double.TryParse(txtPeso.Text, Aux) Then Aux = 0

        Dim Formulario As New frmDatosEtiquetaManual() With {.Peso = Aux, .Fecha = DateTime.Now, .Expedidor = Expedidor, .LoteEntrada = LoteEntrada, .Contexto = Me.Contexto, .Disenho = Disenho}
        If Not Formulario.ShowDialog = Windows.Forms.DialogResult.OK Then Exit Sub

        ' Genero la etiqueta
        GenerarEtiqueta(Formulario.Peso, Formulario.Expedidor, Formulario.LoteEntrada, Formulario.Fecha, Formulario.Disenho, ImpresionAutomatica, False)
    End Sub

    ''' <summary>
    ''' Genera una etiqueta con el peso que marca actualmente la báscula
    ''' </summary>
    Public Sub GenerarEtiquetaPesoActual(ByVal Optional pVerificarCantidadRestante As Boolean = False)
#If DEBUG Then
        'Dim miRandom As New Random
        'txtPeso.Text = Math.Round(miRandom.Next(90, 501) / 100, 2)
#End If
        Dim Aux As Double = 0
        If Double.TryParse(txtPeso.Text, Aux) Then GenerarEtiqueta(Aux, Expedidor, LoteEntrada, DateTime.Now, Disenho, ImpresionAutomatica, pVerificarCantidadRestante)
    End Sub

    ''' <summary>
    ''' Reimprime la última etiqueta generada
    ''' </summary>
    Public Sub ReimprimirUltimaEtiqueta(ByVal Informe As cInforme)
        Dim UltimaEtiqueta As Etiqueta = (From etq As Etiqueta In Contexto.Etiquetas Where Not etq.fechaBaja.HasValue Order By etq.id Descending Select etq).FirstOrDefault

        If UltimaEtiqueta IsNot Nothing Then Imprimir(UltimaEtiqueta, Informe)
    End Sub

    ''' <summary>
    ''' Descarta la última etiqueta generada
    ''' </summary>
    Public Sub DescartarUltimaEtiqueta()
        Dim UltimaEtiqueta As Etiqueta = (From etq As Etiqueta In Contexto.Etiquetas Where Not etq.fechaBaja.HasValue Order By etq.id Descending Select etq).FirstOrDefault
        If UltimaEtiqueta Is Nothing Then Exit Sub

        If Quadralia.Aplicacion.MostrarMessageBox(String.Format("La etiqueta {0} se marcará como descartada. ¿Está seguro de querer continuar?", UltimaEtiqueta.Codigo), "Confirmar descarte", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then Exit Sub
        UltimaEtiqueta.fechaBaja = DateTime.Now

        Contexto.SaveChanges()
        RefrescarDatos()
    End Sub
#End Region
#Region " GENERACION ETIQUETAS "
    Private Sub GenerarEtiqueta(Peso As Double)
        GenerarEtiqueta(Peso, Expedidor, LoteEntrada, DateTime.Now, Disenho, ImpresionAutomatica)
    End Sub

    ''' <summary>
    ''' Genera una etiqueta con la información especificada
    ''' </summary>
    Private Sub GenerarEtiqueta(Peso As Double, Expedidor As Expedidor, LoteEntrada As LoteEntrada, Fecha As DateTime, Disenho As String, ImpresionAutomatica As Boolean, Optional VerificarCantidadRestante As Boolean = True)
        If Expedidor Is Nothing Then Exit Sub
        If LoteEntrada Is Nothing Then Exit Sub
        If Peso < 0 Then Exit Sub
        ''If VerificarCantidadRestante AndAlso (LoteEntrada.TotalPesoRestante - Peso) < 0 Then Exit Sub
        If VerificarCantidadRestante AndAlso (Convert.ToDecimal(txtPesoRestante.Text) - Peso) < 0 Then
            Exit Sub
        End If

        Dim LineaEntrada As LoteEntradalinea = LoteEntrada.ObtenerLineaParaEtiqueta(Peso)
        If LineaEntrada Is Nothing Then
            If LoteEntrada.Lineas.Count() > 0 Then
                LineaEntrada = LoteEntrada.Lineas(0)
            Else
                Exit Sub
            End If
        End If

        ' Creamos una etiqueta
        Dim Etiqueta As New Etiqueta
        With Etiqueta
            .LoteEntradaLinea = LineaEntrada
            .Expedidor = Expedidor
            .cantidad = Peso
            .fecha = Fecha
            .DisenhoPred = Disenho

            If ImpresionAutomatica Then .FuncionImpresion = AddressOf Imprimir

            Contexto.Etiquetas.AddObject(Etiqueta)
            Contexto.SaveChanges()
        End With

        RefrescarDatos()
    End Sub

    Private Sub frmPesadoAutomatico_KeyUp(sender As Object, e As KeyEventArgs) Handles Me.KeyUp
        If (e.KeyCode = Keys.Space) Then
            GenerarEtiquetaPesoActual(True)
        End If
    End Sub
#End Region
End Class
