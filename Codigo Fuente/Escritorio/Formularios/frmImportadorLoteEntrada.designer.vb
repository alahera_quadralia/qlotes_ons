﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImportadorLoteEntrada
    Inherits ComponentFactory.Krypton.Toolkit.KryptonForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImportadorLoteEntrada))
        Me.pFondo = New ComponentFactory.Krypton.Toolkit.KryptonPanel()
        Me.tabAsistente = New ComponentFactory.Krypton.Navigator.KryptonNavigator()
        Me.tbpFicheros = New ComponentFactory.Krypton.Navigator.KryptonPage()
        Me.chkCabeceraExcel = New ComponentFactory.Krypton.Toolkit.KryptonCheckBox()
        Me.lblArchivoExcel = New ComponentFactory.Krypton.Toolkit.KryptonWrapLabel()
        Me.lblSubtituloFicheros = New ComponentFactory.Krypton.Toolkit.KryptonWrapLabel()
        Me.lblTituloFicheros = New ComponentFactory.Krypton.Toolkit.KryptonWrapLabel()
        Me.tbpAsignaciones = New ComponentFactory.Krypton.Navigator.KryptonPage()
        Me.tblOrganizadorAsignaciones = New System.Windows.Forms.TableLayoutPanel()
        Me.lblSubtituloAsignacion = New ComponentFactory.Krypton.Toolkit.KryptonWrapLabel()
        Me.lblTituloAsignacion = New ComponentFactory.Krypton.Toolkit.KryptonWrapLabel()
        Me.dgvArchivo = New ComponentFactory.Krypton.Toolkit.KryptonDataGridView()
        Me.lblContenidoArchivo = New ComponentFactory.Krypton.Toolkit.KryptonWrapLabel()
        Me.dgvEnlazar = New ComponentFactory.Krypton.Toolkit.KryptonDataGridView()
        Me.colGenerar = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.clmArchivo = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.cboCampo = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.lblHoja = New ComponentFactory.Krypton.Toolkit.KryptonWrapLabel()
        Me.tbpParametros = New ComponentFactory.Krypton.Navigator.KryptonPage()
        Me.tblOrganizadorParametros = New System.Windows.Forms.TableLayoutPanel()
        Me.lblSubtituloParametros = New ComponentFactory.Krypton.Toolkit.KryptonWrapLabel()
        Me.lblTituloParametros = New ComponentFactory.Krypton.Toolkit.KryptonWrapLabel()
        Me.lblTienda = New ComponentFactory.Krypton.Toolkit.KryptonWrapLabel()
        Me.lblFecha = New ComponentFactory.Krypton.Toolkit.KryptonWrapLabel()
        Me.tbpResumen = New ComponentFactory.Krypton.Navigator.KryptonPage()
        Me.tblOrganizadorResumen = New System.Windows.Forms.TableLayoutPanel()
        Me.lblSubtituloResumen = New ComponentFactory.Krypton.Toolkit.KryptonWrapLabel()
        Me.lblTituloResumen = New ComponentFactory.Krypton.Toolkit.KryptonWrapLabel()
        Me.txtResumen = New ComponentFactory.Krypton.Toolkit.KryptonRichTextBox()
        Me.tbpGenerar = New ComponentFactory.Krypton.Navigator.KryptonPage()
        Me.tblOrganizadorGeneracion = New System.Windows.Forms.TableLayoutPanel()
        Me.lblSubtituloGenerar = New ComponentFactory.Krypton.Toolkit.KryptonWrapLabel()
        Me.lblTituloGenerar = New ComponentFactory.Krypton.Toolkit.KryptonWrapLabel()
        Me.lblProgreso = New ComponentFactory.Krypton.Toolkit.KryptonWrapLabel()
        Me.pgbProgeso = New System.Windows.Forms.ProgressBar()
        Me.hdtBotonera = New ComponentFactory.Krypton.Toolkit.KryptonPanel()
        Me.btnAnterior = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.btnSiguiente = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.bgDatos = New System.ComponentModel.BackgroundWorker()
        Me.txtArchivoExcel = New Escritorio.Quadralia.Controles.aTextBox()
        Me.btnBuscarExcel = New ComponentFactory.Krypton.Toolkit.ButtonSpecAny()
        Me.cboHojas = New Escritorio.Quadralia.Controles.aComboBox()
        Me.cboProveedor = New Escritorio.Quadralia.Controles.aComboBox()
        Me.dtpFecha = New Escritorio.Quadralia.Controles.aDateTimePicker()
        Me.epErrores = New Escritorio.Quadralia.Controles.Validacion.GestorErrores()
        CType(Me.pFondo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pFondo.SuspendLayout()
        CType(Me.tabAsistente, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabAsistente.SuspendLayout()
        CType(Me.tbpFicheros, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpFicheros.SuspendLayout()
        CType(Me.tbpAsignaciones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpAsignaciones.SuspendLayout()
        Me.tblOrganizadorAsignaciones.SuspendLayout()
        CType(Me.dgvArchivo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvEnlazar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbpParametros, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpParametros.SuspendLayout()
        Me.tblOrganizadorParametros.SuspendLayout()
        CType(Me.tbpResumen, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpResumen.SuspendLayout()
        Me.tblOrganizadorResumen.SuspendLayout()
        CType(Me.tbpGenerar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpGenerar.SuspendLayout()
        Me.tblOrganizadorGeneracion.SuspendLayout()
        CType(Me.hdtBotonera, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.hdtBotonera.SuspendLayout()
        CType(Me.cboHojas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboProveedor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.epErrores, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pFondo
        '
        Me.pFondo.Controls.Add(Me.tabAsistente)
        Me.pFondo.Controls.Add(Me.hdtBotonera)
        Me.pFondo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pFondo.Location = New System.Drawing.Point(0, 0)
        Me.pFondo.Name = "pFondo"
        Me.pFondo.Size = New System.Drawing.Size(682, 367)
        Me.pFondo.TabIndex = 0
        '
        'tabAsistente
        '
        Me.tabAsistente.AllowPageReorder = False
        Me.tabAsistente.AllowTabFocus = False
        Me.tabAsistente.Bar.BarOrientation = ComponentFactory.Krypton.Toolkit.VisualOrientation.Left
        Me.tabAsistente.Bar.ItemOrientation = ComponentFactory.Krypton.Toolkit.ButtonOrientation.FixedTop
        Me.tabAsistente.Bar.TabBorderStyle = ComponentFactory.Krypton.Toolkit.TabBorderStyle.RoundedEqualLarge
        Me.tabAsistente.Bar.TabStyle = ComponentFactory.Krypton.Toolkit.TabStyle.LowProfile
        Me.tabAsistente.Button.ButtonDisplayLogic = ComponentFactory.Krypton.Navigator.ButtonDisplayLogic.ContextNextPrevious
        Me.tabAsistente.Button.CloseButtonAction = ComponentFactory.Krypton.Navigator.CloseButtonAction.HidePage
        Me.tabAsistente.Button.CloseButtonDisplay = ComponentFactory.Krypton.Navigator.ButtonDisplay.Hide
        Me.tabAsistente.Button.ContextButtonAction = ComponentFactory.Krypton.Navigator.ContextButtonAction.None
        Me.tabAsistente.Button.ContextButtonDisplay = ComponentFactory.Krypton.Navigator.ButtonDisplay.Hide
        Me.tabAsistente.Button.NextButtonAction = ComponentFactory.Krypton.Navigator.DirectionButtonAction.None
        Me.tabAsistente.Button.NextButtonDisplay = ComponentFactory.Krypton.Navigator.ButtonDisplay.Hide
        Me.tabAsistente.Button.PreviousButtonAction = ComponentFactory.Krypton.Navigator.DirectionButtonAction.None
        Me.tabAsistente.Button.PreviousButtonDisplay = ComponentFactory.Krypton.Navigator.ButtonDisplay.Hide
        Me.tabAsistente.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabAsistente.Location = New System.Drawing.Point(0, 0)
        Me.tabAsistente.Name = "tabAsistente"
        Me.tabAsistente.Pages.AddRange(New ComponentFactory.Krypton.Navigator.KryptonPage() {Me.tbpFicheros, Me.tbpAsignaciones, Me.tbpParametros, Me.tbpResumen, Me.tbpGenerar})
        Me.tabAsistente.SelectedIndex = 1
        Me.tabAsistente.Size = New System.Drawing.Size(682, 316)
        Me.tabAsistente.TabIndex = 0
        Me.tabAsistente.Text = "KryptonNavigator1"
        '
        'tbpFicheros
        '
        Me.tbpFicheros.AutoHiddenSlideSize = New System.Drawing.Size(200, 200)
        Me.tbpFicheros.Controls.Add(Me.chkCabeceraExcel)
        Me.tbpFicheros.Controls.Add(Me.txtArchivoExcel)
        Me.tbpFicheros.Controls.Add(Me.lblArchivoExcel)
        Me.tbpFicheros.Controls.Add(Me.lblSubtituloFicheros)
        Me.tbpFicheros.Controls.Add(Me.lblTituloFicheros)
        Me.tbpFicheros.Flags = 65534
        Me.tbpFicheros.LastVisibleSet = True
        Me.tbpFicheros.MinimumSize = New System.Drawing.Size(50, 50)
        Me.tbpFicheros.Name = "tbpFicheros"
        Me.tbpFicheros.Size = New System.Drawing.Size(597, 314)
        Me.tbpFicheros.Text = "Ficheros"
        Me.tbpFicheros.ToolTipTitle = "Page ToolTip"
        Me.tbpFicheros.UniqueName = "BFE4687AD30A4E5821AEA0A8ED22E4D6"
        '
        'chkCabeceraExcel
        '
        Me.chkCabeceraExcel.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl
        Me.chkCabeceraExcel.Location = New System.Drawing.Point(118, 103)
        Me.chkCabeceraExcel.Name = "chkCabeceraExcel"
        Me.chkCabeceraExcel.Size = New System.Drawing.Size(334, 20)
        Me.chkCabeceraExcel.TabIndex = 17
        Me.chkCabeceraExcel.Text = "La primera fila del archivo es la cabecera de las columnas"
        Me.chkCabeceraExcel.Values.Text = "La primera fila del archivo es la cabecera de las columnas"
        '
        'lblArchivoExcel
        '
        Me.lblArchivoExcel.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblArchivoExcel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(76, Byte), Integer), CType(CType(83, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.lblArchivoExcel.Location = New System.Drawing.Point(19, 76)
        Me.lblArchivoExcel.Margin = New System.Windows.Forms.Padding(3)
        Me.lblArchivoExcel.Name = "lblArchivoExcel"
        Me.lblArchivoExcel.Padding = New System.Windows.Forms.Padding(3)
        Me.lblArchivoExcel.Size = New System.Drawing.Size(83, 21)
        Me.lblArchivoExcel.Text = "Archivo Excel"
        '
        'lblSubtituloFicheros
        '
        Me.lblSubtituloFicheros.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic)
        Me.lblSubtituloFicheros.ForeColor = System.Drawing.Color.FromArgb(CType(CType(76, Byte), Integer), CType(CType(83, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.lblSubtituloFicheros.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.ItalicControl
        Me.lblSubtituloFicheros.Location = New System.Drawing.Point(3, 39)
        Me.lblSubtituloFicheros.Margin = New System.Windows.Forms.Padding(3)
        Me.lblSubtituloFicheros.Name = "lblSubtituloFicheros"
        Me.lblSubtituloFicheros.Padding = New System.Windows.Forms.Padding(3)
        Me.lblSubtituloFicheros.Size = New System.Drawing.Size(251, 21)
        Me.lblSubtituloFicheros.Text = "Seleccione la ubicación de los ficheros a tratar"
        '
        'lblTituloFicheros
        '
        Me.lblTituloFicheros.Font = New System.Drawing.Font("Segoe UI", 13.5!, System.Drawing.FontStyle.Bold)
        Me.lblTituloFicheros.ForeColor = System.Drawing.Color.FromArgb(CType(CType(76, Byte), Integer), CType(CType(83, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.lblTituloFicheros.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.TitleControl
        Me.lblTituloFicheros.Location = New System.Drawing.Point(3, 11)
        Me.lblTituloFicheros.Margin = New System.Windows.Forms.Padding(3)
        Me.lblTituloFicheros.Name = "lblTituloFicheros"
        Me.lblTituloFicheros.Padding = New System.Windows.Forms.Padding(3)
        Me.lblTituloFicheros.Size = New System.Drawing.Size(201, 31)
        Me.lblTituloFicheros.Text = "Ubicación de ficheros"
        '
        'tbpAsignaciones
        '
        Me.tbpAsignaciones.AutoHiddenSlideSize = New System.Drawing.Size(200, 200)
        Me.tbpAsignaciones.Controls.Add(Me.tblOrganizadorAsignaciones)
        Me.tbpAsignaciones.Flags = 65534
        Me.tbpAsignaciones.LastVisibleSet = True
        Me.tbpAsignaciones.MinimumSize = New System.Drawing.Size(50, 50)
        Me.tbpAsignaciones.Name = "tbpAsignaciones"
        Me.tbpAsignaciones.Size = New System.Drawing.Size(597, 314)
        Me.tbpAsignaciones.Text = "Asignaciones"
        Me.tbpAsignaciones.ToolTipTitle = "Page ToolTip"
        Me.tbpAsignaciones.UniqueName = "A54B90FFCDC640EB87BF3BD8FD571AC5"
        '
        'tblOrganizadorAsignaciones
        '
        Me.tblOrganizadorAsignaciones.BackColor = System.Drawing.Color.Transparent
        Me.tblOrganizadorAsignaciones.ColumnCount = 2
        Me.tblOrganizadorAsignaciones.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblOrganizadorAsignaciones.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblOrganizadorAsignaciones.Controls.Add(Me.lblSubtituloAsignacion, 0, 1)
        Me.tblOrganizadorAsignaciones.Controls.Add(Me.lblTituloAsignacion, 0, 0)
        Me.tblOrganizadorAsignaciones.Controls.Add(Me.dgvArchivo, 0, 6)
        Me.tblOrganizadorAsignaciones.Controls.Add(Me.lblContenidoArchivo, 0, 5)
        Me.tblOrganizadorAsignaciones.Controls.Add(Me.dgvEnlazar, 0, 4)
        Me.tblOrganizadorAsignaciones.Controls.Add(Me.lblHoja, 0, 3)
        Me.tblOrganizadorAsignaciones.Controls.Add(Me.cboHojas, 1, 3)
        Me.tblOrganizadorAsignaciones.Dock = System.Windows.Forms.DockStyle.Fill
        Me.epErrores.SetIconAlignment(Me.tblOrganizadorAsignaciones, System.Windows.Forms.ErrorIconAlignment.MiddleLeft)
        Me.tblOrganizadorAsignaciones.Location = New System.Drawing.Point(0, 0)
        Me.tblOrganizadorAsignaciones.Name = "tblOrganizadorAsignaciones"
        Me.tblOrganizadorAsignaciones.RowCount = 7
        Me.tblOrganizadorAsignaciones.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorAsignaciones.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorAsignaciones.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorAsignaciones.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorAsignaciones.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblOrganizadorAsignaciones.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorAsignaciones.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblOrganizadorAsignaciones.Size = New System.Drawing.Size(597, 314)
        Me.tblOrganizadorAsignaciones.TabIndex = 0
        '
        'lblSubtituloAsignacion
        '
        Me.tblOrganizadorAsignaciones.SetColumnSpan(Me.lblSubtituloAsignacion, 2)
        Me.lblSubtituloAsignacion.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic)
        Me.lblSubtituloAsignacion.ForeColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(91, Byte), Integer))
        Me.lblSubtituloAsignacion.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.ItalicControl
        Me.lblSubtituloAsignacion.Location = New System.Drawing.Point(3, 34)
        Me.lblSubtituloAsignacion.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.lblSubtituloAsignacion.Name = "lblSubtituloAsignacion"
        Me.lblSubtituloAsignacion.Padding = New System.Windows.Forms.Padding(3)
        Me.lblSubtituloAsignacion.Size = New System.Drawing.Size(437, 21)
        Me.lblSubtituloAsignacion.Text = "Establezca la relación existente entre los campos del fichero y de la Base de Dat" & _
    "os"
        '
        'lblTituloAsignacion
        '
        Me.tblOrganizadorAsignaciones.SetColumnSpan(Me.lblTituloAsignacion, 2)
        Me.lblTituloAsignacion.Font = New System.Drawing.Font("Segoe UI", 13.5!, System.Drawing.FontStyle.Bold)
        Me.lblTituloAsignacion.ForeColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(91, Byte), Integer))
        Me.epErrores.SetIconAlignment(Me.lblTituloAsignacion, System.Windows.Forms.ErrorIconAlignment.MiddleLeft)
        Me.lblTituloAsignacion.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.TitleControl
        Me.lblTituloAsignacion.Location = New System.Drawing.Point(3, 3)
        Me.lblTituloAsignacion.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblTituloAsignacion.Name = "lblTituloAsignacion"
        Me.lblTituloAsignacion.Padding = New System.Windows.Forms.Padding(3)
        Me.lblTituloAsignacion.Size = New System.Drawing.Size(210, 31)
        Me.lblTituloAsignacion.Text = "Asignación de Campos"
        '
        'dgvArchivo
        '
        Me.dgvArchivo.AllowUserToAddRows = False
        Me.dgvArchivo.AllowUserToResizeRows = False
        Me.dgvArchivo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvArchivo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.tblOrganizadorAsignaciones.SetColumnSpan(Me.dgvArchivo, 2)
        Me.dgvArchivo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvArchivo.Location = New System.Drawing.Point(3, 226)
        Me.dgvArchivo.Name = "dgvArchivo"
        Me.dgvArchivo.RowHeadersWidth = 22
        Me.dgvArchivo.ShowCellErrors = False
        Me.dgvArchivo.ShowCellToolTips = False
        Me.dgvArchivo.ShowEditingIcon = False
        Me.dgvArchivo.ShowRowErrors = False
        Me.dgvArchivo.Size = New System.Drawing.Size(591, 85)
        Me.dgvArchivo.TabIndex = 20
        '
        'lblContenidoArchivo
        '
        Me.tblOrganizadorAsignaciones.SetColumnSpan(Me.lblContenidoArchivo, 2)
        Me.lblContenidoArchivo.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblContenidoArchivo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(91, Byte), Integer))
        Me.lblContenidoArchivo.Location = New System.Drawing.Point(3, 199)
        Me.lblContenidoArchivo.Margin = New System.Windows.Forms.Padding(3)
        Me.lblContenidoArchivo.Name = "lblContenidoArchivo"
        Me.lblContenidoArchivo.Padding = New System.Windows.Forms.Padding(3)
        Me.lblContenidoArchivo.Size = New System.Drawing.Size(192, 21)
        Me.lblContenidoArchivo.Text = "Extracto del contenido del archivo"
        '
        'dgvEnlazar
        '
        Me.dgvEnlazar.AllowUserToAddRows = False
        Me.dgvEnlazar.AllowUserToDeleteRows = False
        Me.dgvEnlazar.AllowUserToResizeRows = False
        Me.dgvEnlazar.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvEnlazar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvEnlazar.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colGenerar, Me.clmArchivo, Me.cboCampo})
        Me.tblOrganizadorAsignaciones.SetColumnSpan(Me.dgvEnlazar, 2)
        Me.dgvEnlazar.Dock = System.Windows.Forms.DockStyle.Fill
        Me.epErrores.SetIconAlignment(Me.dgvEnlazar, System.Windows.Forms.ErrorIconAlignment.MiddleLeft)
        Me.dgvEnlazar.Location = New System.Drawing.Point(3, 108)
        Me.dgvEnlazar.Name = "dgvEnlazar"
        Me.dgvEnlazar.RowHeadersVisible = False
        Me.dgvEnlazar.RowHeadersWidth = 22
        Me.dgvEnlazar.ShowCellErrors = False
        Me.dgvEnlazar.ShowCellToolTips = False
        Me.dgvEnlazar.ShowEditingIcon = False
        Me.dgvEnlazar.ShowRowErrors = False
        Me.dgvEnlazar.Size = New System.Drawing.Size(591, 85)
        Me.dgvEnlazar.TabIndex = 19
        '
        'colGenerar
        '
        Me.colGenerar.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colGenerar.HeaderText = ""
        Me.colGenerar.Name = "colGenerar"
        Me.colGenerar.Width = 7
        '
        'clmArchivo
        '
        Me.clmArchivo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.clmArchivo.HeaderText = "Columna Archivo"
        Me.clmArchivo.Name = "clmArchivo"
        Me.clmArchivo.ReadOnly = True
        Me.clmArchivo.Width = 448
        '
        'cboCampo
        '
        Me.cboCampo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.cboCampo.HeaderText = "Campo Base Datos"
        Me.cboCampo.Name = "cboCampo"
        Me.cboCampo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.cboCampo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.cboCampo.Width = 135
        '
        'lblHoja
        '
        Me.lblHoja.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblHoja.ForeColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(91, Byte), Integer))
        Me.lblHoja.Location = New System.Drawing.Point(3, 81)
        Me.lblHoja.Margin = New System.Windows.Forms.Padding(3)
        Me.lblHoja.Name = "lblHoja"
        Me.lblHoja.Padding = New System.Windows.Forms.Padding(3)
        Me.lblHoja.Size = New System.Drawing.Size(87, 21)
        Me.lblHoja.Text = "Importar Hoja"
        '
        'tbpParametros
        '
        Me.tbpParametros.AutoHiddenSlideSize = New System.Drawing.Size(200, 200)
        Me.tbpParametros.Controls.Add(Me.tblOrganizadorParametros)
        Me.tbpParametros.Flags = 65534
        Me.tbpParametros.LastVisibleSet = True
        Me.tbpParametros.MinimumSize = New System.Drawing.Size(50, 50)
        Me.tbpParametros.Name = "tbpParametros"
        Me.tbpParametros.Size = New System.Drawing.Size(597, 314)
        Me.tbpParametros.Text = "Parámetros"
        Me.tbpParametros.ToolTipTitle = "Page ToolTip"
        Me.tbpParametros.UniqueName = "007E5A01EE3847150297F2E0D1255473"
        '
        'tblOrganizadorParametros
        '
        Me.tblOrganizadorParametros.BackColor = System.Drawing.Color.Transparent
        Me.tblOrganizadorParametros.ColumnCount = 2
        Me.tblOrganizadorParametros.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblOrganizadorParametros.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblOrganizadorParametros.Controls.Add(Me.lblSubtituloParametros, 0, 1)
        Me.tblOrganizadorParametros.Controls.Add(Me.lblTituloParametros, 0, 0)
        Me.tblOrganizadorParametros.Controls.Add(Me.lblTienda, 0, 4)
        Me.tblOrganizadorParametros.Controls.Add(Me.cboProveedor, 1, 4)
        Me.tblOrganizadorParametros.Controls.Add(Me.lblFecha, 0, 3)
        Me.tblOrganizadorParametros.Controls.Add(Me.dtpFecha, 1, 3)
        Me.tblOrganizadorParametros.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblOrganizadorParametros.Location = New System.Drawing.Point(0, 0)
        Me.tblOrganizadorParametros.Name = "tblOrganizadorParametros"
        Me.tblOrganizadorParametros.RowCount = 6
        Me.tblOrganizadorParametros.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorParametros.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorParametros.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorParametros.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorParametros.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorParametros.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorParametros.Size = New System.Drawing.Size(597, 314)
        Me.tblOrganizadorParametros.TabIndex = 1
        '
        'lblSubtituloParametros
        '
        Me.tblOrganizadorParametros.SetColumnSpan(Me.lblSubtituloParametros, 2)
        Me.lblSubtituloParametros.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic)
        Me.lblSubtituloParametros.ForeColor = System.Drawing.Color.FromArgb(CType(CType(76, Byte), Integer), CType(CType(83, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.lblSubtituloParametros.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.ItalicControl
        Me.lblSubtituloParametros.Location = New System.Drawing.Point(3, 34)
        Me.lblSubtituloParametros.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.lblSubtituloParametros.Name = "lblSubtituloParametros"
        Me.lblSubtituloParametros.Padding = New System.Windows.Forms.Padding(3)
        Me.lblSubtituloParametros.Size = New System.Drawing.Size(429, 21)
        Me.lblSubtituloParametros.Text = "Establezca el resto de configuración necesaria para la importación de albaranes"
        '
        'lblTituloParametros
        '
        Me.tblOrganizadorParametros.SetColumnSpan(Me.lblTituloParametros, 2)
        Me.lblTituloParametros.Font = New System.Drawing.Font("Segoe UI", 13.5!, System.Drawing.FontStyle.Bold)
        Me.lblTituloParametros.ForeColor = System.Drawing.Color.FromArgb(CType(CType(76, Byte), Integer), CType(CType(83, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.lblTituloParametros.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.TitleControl
        Me.lblTituloParametros.Location = New System.Drawing.Point(3, 3)
        Me.lblTituloParametros.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblTituloParametros.Name = "lblTituloParametros"
        Me.lblTituloParametros.Padding = New System.Windows.Forms.Padding(3)
        Me.lblTituloParametros.Size = New System.Drawing.Size(250, 31)
        Me.lblTituloParametros.Text = "Parámetros de Importación"
        '
        'lblTienda
        '
        Me.lblTienda.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblTienda.ForeColor = System.Drawing.Color.FromArgb(CType(CType(76, Byte), Integer), CType(CType(83, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.lblTienda.Location = New System.Drawing.Point(3, 108)
        Me.lblTienda.Margin = New System.Windows.Forms.Padding(3)
        Me.lblTienda.Name = "lblTienda"
        Me.lblTienda.Padding = New System.Windows.Forms.Padding(3)
        Me.lblTienda.Size = New System.Drawing.Size(67, 21)
        Me.lblTienda.Text = "Proveedor"
        '
        'lblFecha
        '
        Me.lblFecha.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblFecha.ForeColor = System.Drawing.Color.FromArgb(CType(CType(76, Byte), Integer), CType(CType(83, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.lblFecha.Location = New System.Drawing.Point(3, 81)
        Me.lblFecha.Margin = New System.Windows.Forms.Padding(3)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Padding = New System.Windows.Forms.Padding(3)
        Me.lblFecha.Size = New System.Drawing.Size(44, 21)
        Me.lblFecha.Text = "Fecha"
        '
        'tbpResumen
        '
        Me.tbpResumen.AutoHiddenSlideSize = New System.Drawing.Size(200, 200)
        Me.tbpResumen.Controls.Add(Me.tblOrganizadorResumen)
        Me.tbpResumen.Flags = 65534
        Me.tbpResumen.LastVisibleSet = True
        Me.tbpResumen.MinimumSize = New System.Drawing.Size(50, 50)
        Me.tbpResumen.Name = "tbpResumen"
        Me.tbpResumen.Size = New System.Drawing.Size(597, 314)
        Me.tbpResumen.Text = "Resumen"
        Me.tbpResumen.ToolTipTitle = "Page ToolTip"
        Me.tbpResumen.UniqueName = "720C271DCB2C459498B1F350356886C7"
        '
        'tblOrganizadorResumen
        '
        Me.tblOrganizadorResumen.BackColor = System.Drawing.Color.Transparent
        Me.tblOrganizadorResumen.ColumnCount = 1
        Me.tblOrganizadorResumen.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblOrganizadorResumen.Controls.Add(Me.lblSubtituloResumen, 0, 1)
        Me.tblOrganizadorResumen.Controls.Add(Me.lblTituloResumen, 0, 0)
        Me.tblOrganizadorResumen.Controls.Add(Me.txtResumen, 0, 3)
        Me.tblOrganizadorResumen.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblOrganizadorResumen.Location = New System.Drawing.Point(0, 0)
        Me.tblOrganizadorResumen.Name = "tblOrganizadorResumen"
        Me.tblOrganizadorResumen.RowCount = 4
        Me.tblOrganizadorResumen.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorResumen.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorResumen.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorResumen.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorResumen.Size = New System.Drawing.Size(597, 314)
        Me.tblOrganizadorResumen.TabIndex = 2
        '
        'lblSubtituloResumen
        '
        Me.lblSubtituloResumen.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic)
        Me.lblSubtituloResumen.ForeColor = System.Drawing.Color.FromArgb(CType(CType(76, Byte), Integer), CType(CType(83, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.lblSubtituloResumen.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.ItalicControl
        Me.lblSubtituloResumen.Location = New System.Drawing.Point(3, 34)
        Me.lblSubtituloResumen.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.lblSubtituloResumen.Name = "lblSubtituloResumen"
        Me.lblSubtituloResumen.Padding = New System.Windows.Forms.Padding(3)
        Me.lblSubtituloResumen.Size = New System.Drawing.Size(371, 21)
        Me.lblSubtituloResumen.Text = "Revise los parámetros establecidos para la importación de albaranes"
        '
        'lblTituloResumen
        '
        Me.lblTituloResumen.Font = New System.Drawing.Font("Segoe UI", 13.5!, System.Drawing.FontStyle.Bold)
        Me.lblTituloResumen.ForeColor = System.Drawing.Color.FromArgb(CType(CType(76, Byte), Integer), CType(CType(83, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.lblTituloResumen.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.TitleControl
        Me.lblTituloResumen.Location = New System.Drawing.Point(3, 3)
        Me.lblTituloResumen.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblTituloResumen.Name = "lblTituloResumen"
        Me.lblTituloResumen.Padding = New System.Windows.Forms.Padding(3)
        Me.lblTituloResumen.Size = New System.Drawing.Size(96, 31)
        Me.lblTituloResumen.Text = "Resumen"
        '
        'txtResumen
        '
        Me.txtResumen.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtResumen.Location = New System.Drawing.Point(3, 81)
        Me.txtResumen.Name = "txtResumen"
        Me.txtResumen.ReadOnly = True
        Me.txtResumen.Size = New System.Drawing.Size(591, 230)
        Me.txtResumen.StateCommon.Border.DrawBorders = CType((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top Or ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) _
            Or ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) _
            Or ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right), ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)
        Me.txtResumen.StateCommon.Border.Rounding = 5
        Me.txtResumen.StateCommon.Border.Width = 2
        Me.txtResumen.TabIndex = 2
        Me.txtResumen.Text = ""
        '
        'tbpGenerar
        '
        Me.tbpGenerar.AutoHiddenSlideSize = New System.Drawing.Size(200, 200)
        Me.tbpGenerar.Controls.Add(Me.tblOrganizadorGeneracion)
        Me.tbpGenerar.Flags = 65534
        Me.tbpGenerar.LastVisibleSet = True
        Me.tbpGenerar.MinimumSize = New System.Drawing.Size(50, 50)
        Me.tbpGenerar.Name = "tbpGenerar"
        Me.tbpGenerar.Size = New System.Drawing.Size(597, 314)
        Me.tbpGenerar.Text = "Importación"
        Me.tbpGenerar.ToolTipTitle = "Page ToolTip"
        Me.tbpGenerar.UniqueName = "A963978FE8F640B274B1B0DB7DAF5C83"
        '
        'tblOrganizadorGeneracion
        '
        Me.tblOrganizadorGeneracion.BackColor = System.Drawing.Color.Transparent
        Me.tblOrganizadorGeneracion.ColumnCount = 1
        Me.tblOrganizadorGeneracion.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblOrganizadorGeneracion.Controls.Add(Me.lblSubtituloGenerar, 0, 1)
        Me.tblOrganizadorGeneracion.Controls.Add(Me.lblTituloGenerar, 0, 0)
        Me.tblOrganizadorGeneracion.Controls.Add(Me.lblProgreso, 0, 2)
        Me.tblOrganizadorGeneracion.Controls.Add(Me.pgbProgeso, 0, 3)
        Me.tblOrganizadorGeneracion.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblOrganizadorGeneracion.Location = New System.Drawing.Point(0, 0)
        Me.tblOrganizadorGeneracion.Name = "tblOrganizadorGeneracion"
        Me.tblOrganizadorGeneracion.RowCount = 4
        Me.tblOrganizadorGeneracion.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorGeneracion.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorGeneracion.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblOrganizadorGeneracion.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblOrganizadorGeneracion.Size = New System.Drawing.Size(597, 314)
        Me.tblOrganizadorGeneracion.TabIndex = 3
        '
        'lblSubtituloGenerar
        '
        Me.lblSubtituloGenerar.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic)
        Me.lblSubtituloGenerar.ForeColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(91, Byte), Integer))
        Me.lblSubtituloGenerar.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.ItalicControl
        Me.lblSubtituloGenerar.Location = New System.Drawing.Point(3, 34)
        Me.lblSubtituloGenerar.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.lblSubtituloGenerar.Name = "lblSubtituloGenerar"
        Me.lblSubtituloGenerar.Padding = New System.Windows.Forms.Padding(3)
        Me.lblSubtituloGenerar.Size = New System.Drawing.Size(279, 21)
        Me.lblSubtituloGenerar.Text = "Por favor, espere mientras se realiza la importación"
        '
        'lblTituloGenerar
        '
        Me.lblTituloGenerar.Font = New System.Drawing.Font("Segoe UI", 13.5!, System.Drawing.FontStyle.Bold)
        Me.lblTituloGenerar.ForeColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(91, Byte), Integer))
        Me.lblTituloGenerar.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.TitleControl
        Me.lblTituloGenerar.Location = New System.Drawing.Point(3, 3)
        Me.lblTituloGenerar.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblTituloGenerar.Name = "lblTituloGenerar"
        Me.lblTituloGenerar.Padding = New System.Windows.Forms.Padding(3)
        Me.lblTituloGenerar.Size = New System.Drawing.Size(122, 31)
        Me.lblTituloGenerar.Text = "Importación"
        '
        'lblProgreso
        '
        Me.lblProgreso.AutoSize = False
        Me.lblProgreso.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.lblProgreso.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblProgreso.ForeColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(91, Byte), Integer))
        Me.lblProgreso.Location = New System.Drawing.Point(3, 163)
        Me.lblProgreso.Name = "lblProgreso"
        Me.lblProgreso.Size = New System.Drawing.Size(591, 23)
        Me.lblProgreso.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pgbProgeso
        '
        Me.pgbProgeso.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pgbProgeso.Location = New System.Drawing.Point(20, 189)
        Me.pgbProgeso.Margin = New System.Windows.Forms.Padding(20, 3, 20, 3)
        Me.pgbProgeso.Name = "pgbProgeso"
        Me.pgbProgeso.Size = New System.Drawing.Size(557, 23)
        Me.pgbProgeso.TabIndex = 3
        '
        'hdtBotonera
        '
        Me.hdtBotonera.Controls.Add(Me.btnAnterior)
        Me.hdtBotonera.Controls.Add(Me.btnSiguiente)
        Me.hdtBotonera.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.hdtBotonera.Location = New System.Drawing.Point(0, 316)
        Me.hdtBotonera.Name = "hdtBotonera"
        Me.hdtBotonera.Size = New System.Drawing.Size(682, 51)
        Me.hdtBotonera.TabIndex = 1
        '
        'btnAnterior
        '
        Me.btnAnterior.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAnterior.Location = New System.Drawing.Point(484, 13)
        Me.btnAnterior.Name = "btnAnterior"
        Me.btnAnterior.Size = New System.Drawing.Size(90, 25)
        Me.btnAnterior.TabIndex = 0
        Me.btnAnterior.Values.Text = "< &Anterior"
        '
        'btnSiguiente
        '
        Me.btnSiguiente.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSiguiente.Location = New System.Drawing.Point(580, 13)
        Me.btnSiguiente.Name = "btnSiguiente"
        Me.btnSiguiente.Size = New System.Drawing.Size(90, 25)
        Me.btnSiguiente.TabIndex = 0
        Me.btnSiguiente.Values.Text = "&Siguiente >"
        '
        'bgDatos
        '
        '
        'txtArchivoExcel
        '
        Me.txtArchivoExcel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtArchivoExcel.ButtonSpecs.AddRange(New ComponentFactory.Krypton.Toolkit.ButtonSpecAny() {Me.btnBuscarExcel})
        Me.txtArchivoExcel.controlarBotonBorrar = True
        Me.txtArchivoExcel.Formato = ""
        Me.epErrores.SetIconAlignment(Me.txtArchivoExcel, System.Windows.Forms.ErrorIconAlignment.MiddleLeft)
        Me.txtArchivoExcel.Location = New System.Drawing.Point(118, 77)
        Me.txtArchivoExcel.mostrarSiempreBotonBorrar = False
        Me.txtArchivoExcel.Name = "txtArchivoExcel"
        Me.txtArchivoExcel.seleccionarTodo = True
        Me.txtArchivoExcel.Size = New System.Drawing.Size(468, 20)
        Me.txtArchivoExcel.TabIndex = 6
        '
        'btnBuscarExcel
        '
        Me.btnBuscarExcel.Image = Global.Escritorio.My.Resources.Resources.Abrir_12
        Me.btnBuscarExcel.UniqueName = "3562894394AB49688DB0BB63C6127EAA"
        '
        'cboHojas
        '
        Me.cboHojas.controlarBotonBorrar = True
        Me.cboHojas.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cboHojas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboHojas.DropDownWidth = 498
        Me.epErrores.SetIconAlignment(Me.cboHojas, System.Windows.Forms.ErrorIconAlignment.MiddleLeft)
        Me.cboHojas.Location = New System.Drawing.Point(96, 81)
        Me.cboHojas.mostrarSiempreBotonBorrar = False
        Me.cboHojas.Name = "cboHojas"
        Me.cboHojas.Size = New System.Drawing.Size(498, 21)
        Me.cboHojas.TabIndex = 23
        '
        'cboProveedor
        '
        Me.cboProveedor.controlarBotonBorrar = True
        Me.cboProveedor.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cboProveedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboProveedor.DropDownWidth = 498
        Me.cboProveedor.Location = New System.Drawing.Point(76, 108)
        Me.cboProveedor.mostrarSiempreBotonBorrar = False
        Me.cboProveedor.Name = "cboProveedor"
        Me.cboProveedor.Size = New System.Drawing.Size(518, 21)
        Me.cboProveedor.TabIndex = 23
        '
        'dtpFecha
        '
        Me.dtpFecha.CalendarTodayText = "Hoy:"
        Me.dtpFecha.controlarBotonBorrar = True
        Me.dtpFecha.CustomNullText = "(Sin fecha)"
        Me.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecha.Location = New System.Drawing.Point(76, 81)
        Me.dtpFecha.mostrarSiempreBotonBorrar = False
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(119, 21)
        Me.dtpFecha.TabIndex = 25
        Me.dtpFecha.TipoLimpieza = Escritorio.Quadralia.Controles.aDateTimePicker.TiposLimpieza.ValueYValueNullable
        '
        'epErrores
        '
        Me.epErrores.Alineacion = System.Windows.Forms.ErrorIconAlignment.MiddleLeft
        Me.epErrores.ContainerControl = Me
        '
        'frmImportadorLoteEntrada
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(682, 367)
        Me.Controls.Add(Me.pFondo)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmImportadorLoteEntrada"
        Me.Text = "Asistente para la importación de albaranes de entrada"
        CType(Me.pFondo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pFondo.ResumeLayout(False)
        CType(Me.tabAsistente, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabAsistente.ResumeLayout(False)
        CType(Me.tbpFicheros, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpFicheros.ResumeLayout(False)
        Me.tbpFicheros.PerformLayout()
        CType(Me.tbpAsignaciones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpAsignaciones.ResumeLayout(False)
        Me.tblOrganizadorAsignaciones.ResumeLayout(False)
        Me.tblOrganizadorAsignaciones.PerformLayout()
        CType(Me.dgvArchivo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvEnlazar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbpParametros, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpParametros.ResumeLayout(False)
        Me.tblOrganizadorParametros.ResumeLayout(False)
        Me.tblOrganizadorParametros.PerformLayout()
        CType(Me.tbpResumen, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpResumen.ResumeLayout(False)
        Me.tblOrganizadorResumen.ResumeLayout(False)
        Me.tblOrganizadorResumen.PerformLayout()
        CType(Me.tbpGenerar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpGenerar.ResumeLayout(False)
        Me.tblOrganizadorGeneracion.ResumeLayout(False)
        Me.tblOrganizadorGeneracion.PerformLayout()
        CType(Me.hdtBotonera, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdtBotonera.ResumeLayout(False)
        CType(Me.cboHojas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboProveedor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.epErrores, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pFondo As ComponentFactory.Krypton.Toolkit.KryptonPanel
    Friend WithEvents tabAsistente As ComponentFactory.Krypton.Navigator.KryptonNavigator
    Friend WithEvents tbpFicheros As ComponentFactory.Krypton.Navigator.KryptonPage
    Friend WithEvents btnSiguiente As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents tbpAsignaciones As ComponentFactory.Krypton.Navigator.KryptonPage
    Friend WithEvents hdtBotonera As ComponentFactory.Krypton.Toolkit.KryptonPanel
    Friend WithEvents btnAnterior As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents lblArchivoExcel As ComponentFactory.Krypton.Toolkit.KryptonWrapLabel
    Friend WithEvents lblSubtituloFicheros As ComponentFactory.Krypton.Toolkit.KryptonWrapLabel
    Friend WithEvents lblTituloFicheros As ComponentFactory.Krypton.Toolkit.KryptonWrapLabel
    Friend WithEvents tbpParametros As ComponentFactory.Krypton.Navigator.KryptonPage
    Friend WithEvents txtArchivoExcel As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents btnBuscarExcel As ComponentFactory.Krypton.Toolkit.ButtonSpecAny
    Friend WithEvents epErrores As Escritorio.Quadralia.Controles.Validacion.GestorErrores
    Friend WithEvents chkCabeceraExcel As ComponentFactory.Krypton.Toolkit.KryptonCheckBox
    Friend WithEvents tblOrganizadorAsignaciones As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblContenidoArchivo As ComponentFactory.Krypton.Toolkit.KryptonWrapLabel
    Friend WithEvents dgvEnlazar As ComponentFactory.Krypton.Toolkit.KryptonDataGridView
    Friend WithEvents dgvArchivo As ComponentFactory.Krypton.Toolkit.KryptonDataGridView
    Friend WithEvents lblHoja As ComponentFactory.Krypton.Toolkit.KryptonWrapLabel
    Friend WithEvents cboHojas As Escritorio.Quadralia.Controles.aComboBox
    Friend WithEvents lblSubtituloAsignacion As ComponentFactory.Krypton.Toolkit.KryptonWrapLabel
    Friend WithEvents lblTituloAsignacion As ComponentFactory.Krypton.Toolkit.KryptonWrapLabel
    Friend WithEvents tbpResumen As ComponentFactory.Krypton.Navigator.KryptonPage
    Friend WithEvents tblOrganizadorResumen As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblSubtituloResumen As ComponentFactory.Krypton.Toolkit.KryptonWrapLabel
    Friend WithEvents lblTituloResumen As ComponentFactory.Krypton.Toolkit.KryptonWrapLabel
    Friend WithEvents txtResumen As ComponentFactory.Krypton.Toolkit.KryptonRichTextBox
    Friend WithEvents tbpGenerar As ComponentFactory.Krypton.Navigator.KryptonPage
    Friend WithEvents tblOrganizadorGeneracion As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblSubtituloGenerar As ComponentFactory.Krypton.Toolkit.KryptonWrapLabel
    Friend WithEvents lblTituloGenerar As ComponentFactory.Krypton.Toolkit.KryptonWrapLabel
    Friend WithEvents lblProgreso As ComponentFactory.Krypton.Toolkit.KryptonWrapLabel
    Friend WithEvents pgbProgeso As System.Windows.Forms.ProgressBar
    Friend WithEvents bgDatos As System.ComponentModel.BackgroundWorker
    Friend WithEvents tblOrganizadorParametros As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblSubtituloParametros As ComponentFactory.Krypton.Toolkit.KryptonWrapLabel
    Friend WithEvents lblTituloParametros As ComponentFactory.Krypton.Toolkit.KryptonWrapLabel
    Friend WithEvents lblTienda As ComponentFactory.Krypton.Toolkit.KryptonWrapLabel
    Friend WithEvents cboProveedor As Escritorio.Quadralia.Controles.aComboBox
    Friend WithEvents colGenerar As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents clmArchivo As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents cboCampo As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents lblFecha As ComponentFactory.Krypton.Toolkit.KryptonWrapLabel
    Friend WithEvents dtpFecha As Escritorio.Quadralia.Controles.aDateTimePicker
End Class
