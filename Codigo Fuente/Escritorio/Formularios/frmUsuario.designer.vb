<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUsuario
    Inherits FormularioHijo

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Dise�ador de Windows Forms.  
    'No lo modifique con el editor de c�digo.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmUsuario))
        Me.lblUsuario = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.btnBBuscar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.txtUsuario = New Escritorio.Quadralia.Controles.aTextBox()
        Me.lblBUsuario = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtBUsuario = New Escritorio.Quadralia.Controles.aTextBox()
        Me.chkActivo = New ComponentFactory.Krypton.Toolkit.KryptonCheckBox()
        Me.chkBActivo = New ComponentFactory.Krypton.Toolkit.KryptonCheckBox()
        Me.grpResultados = New ComponentFactory.Krypton.Toolkit.KryptonGroupBox()
        Me.dgvResultadosBuscador = New ComponentFactory.Krypton.Toolkit.KryptonDataGridView()
        Me.colActivo = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.colUsuario = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.splSeparador = New ComponentFactory.Krypton.Toolkit.KryptonSplitContainer()
        Me.hdrBusqueda = New ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup()
        Me.btnOcultarBusqueda = New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup()
        Me.tblOrganizadorBusqueda = New System.Windows.Forms.TableLayoutPanel()
        Me.tabUsuario = New ComponentFactory.Krypton.Navigator.KryptonNavigator()
        Me.tbpDatosUsuario = New ComponentFactory.Krypton.Navigator.KryptonPage()
        Me.tblOrganizador = New System.Windows.Forms.TableLayoutPanel()
        Me.hdrPermisos = New ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup()
        Me.chkPermisos = New ComponentFactory.Krypton.Toolkit.KryptonCheckedListBox()
        Me.chkRestablecerClave = New ComponentFactory.Krypton.Toolkit.KryptonCheckBox()
        Me.lblTitulo = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.grpCabecera = New ComponentFactory.Krypton.Toolkit.KryptonGroup()
        Me.epErrores = New Escritorio.Quadralia.Controles.Validacion.GestorErrores()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.GestorErrores1 = New Escritorio.Quadralia.Controles.Validacion.GestorErrores()
        CType(Me.grpResultados, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpResultados.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpResultados.Panel.SuspendLayout()
        Me.grpResultados.SuspendLayout()
        CType(Me.dgvResultadosBuscador, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.splSeparador, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.splSeparador.Panel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.splSeparador.Panel1.SuspendLayout()
        CType(Me.splSeparador.Panel2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.splSeparador.Panel2.SuspendLayout()
        Me.splSeparador.SuspendLayout()
        CType(Me.hdrBusqueda, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.hdrBusqueda.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.hdrBusqueda.Panel.SuspendLayout()
        Me.hdrBusqueda.SuspendLayout()
        Me.tblOrganizadorBusqueda.SuspendLayout()
        CType(Me.tabUsuario, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabUsuario.SuspendLayout()
        CType(Me.tbpDatosUsuario, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpDatosUsuario.SuspendLayout()
        Me.tblOrganizador.SuspendLayout()
        CType(Me.hdrPermisos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.hdrPermisos.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.hdrPermisos.Panel.SuspendLayout()
        Me.hdrPermisos.SuspendLayout()
        CType(Me.grpCabecera, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpCabecera.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpCabecera.Panel.SuspendLayout()
        Me.grpCabecera.SuspendLayout()
        CType(Me.epErrores, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GestorErrores1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblUsuario
        '
        Me.lblUsuario.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblUsuario.Location = New System.Drawing.Point(5, 3)
        Me.lblUsuario.Name = "lblUsuario"
        Me.lblUsuario.Size = New System.Drawing.Size(99, 20)
        Me.lblUsuario.TabIndex = 0
        Me.lblUsuario.Target = Me.btnBBuscar
        Me.lblUsuario.Values.Text = "Nombre usuario"
        '
        'btnBBuscar
        '
        Me.tblOrganizadorBusqueda.SetColumnSpan(Me.btnBBuscar, 2)
        Me.btnBBuscar.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnBBuscar.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnBBuscar.Location = New System.Drawing.Point(3, 55)
        Me.btnBBuscar.Name = "btnBBuscar"
        Me.btnBBuscar.Size = New System.Drawing.Size(230, 25)
        Me.btnBBuscar.TabIndex = 5
        Me.btnBBuscar.Values.Image = Global.Escritorio.My.Resources.Resources.Buscar_16
        Me.btnBBuscar.Values.Text = "Buscar"
        '
        'txtUsuario
        '
        Me.txtUsuario.AlwaysActive = False
        Me.txtUsuario.controlarBotonBorrar = True
        Me.txtUsuario.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtUsuario.Formato = ""
        Me.txtUsuario.Location = New System.Drawing.Point(130, 3)
        Me.txtUsuario.MaxLength = 20
        Me.txtUsuario.mostrarSiempreBotonBorrar = False
        Me.txtUsuario.Name = "txtUsuario"
        Me.txtUsuario.seleccionarTodo = True
        Me.txtUsuario.Size = New System.Drawing.Size(330, 20)
        Me.txtUsuario.TabIndex = 1
        Me.txtUsuario.Text = "usuario"
        '
        'lblBUsuario
        '
        Me.lblBUsuario.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblBUsuario.Location = New System.Drawing.Point(3, 3)
        Me.lblBUsuario.Name = "lblBUsuario"
        Me.lblBUsuario.Size = New System.Drawing.Size(52, 20)
        Me.lblBUsuario.TabIndex = 0
        Me.lblBUsuario.Values.Text = "Usuario"
        '
        'txtBUsuario
        '
        Me.txtBUsuario.AlwaysActive = False
        Me.txtBUsuario.controlarBotonBorrar = True
        Me.txtBUsuario.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtBUsuario.Formato = ""
        Me.txtBUsuario.Location = New System.Drawing.Point(61, 3)
        Me.txtBUsuario.MaxLength = 20
        Me.txtBUsuario.mostrarSiempreBotonBorrar = False
        Me.txtBUsuario.Name = "txtBUsuario"
        Me.txtBUsuario.seleccionarTodo = True
        Me.txtBUsuario.Size = New System.Drawing.Size(172, 20)
        Me.txtBUsuario.TabIndex = 1
        '
        'chkActivo
        '
        Me.chkActivo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkActivo.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl
        Me.chkActivo.Location = New System.Drawing.Point(3, 29)
        Me.chkActivo.Name = "chkActivo"
        Me.chkActivo.Size = New System.Drawing.Size(101, 20)
        Me.chkActivo.TabIndex = 4
        Me.chkActivo.Text = "Usuario activo"
        Me.chkActivo.Values.Text = "Usuario activo"
        '
        'chkBActivo
        '
        Me.tblOrganizadorBusqueda.SetColumnSpan(Me.chkBActivo, 2)
        Me.chkBActivo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.chkBActivo.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl
        Me.chkBActivo.Location = New System.Drawing.Point(3, 29)
        Me.chkBActivo.Name = "chkBActivo"
        Me.chkBActivo.Size = New System.Drawing.Size(230, 20)
        Me.chkBActivo.TabIndex = 4
        Me.chkBActivo.Text = "S�lo usuarios activos"
        Me.chkBActivo.Values.Text = "S�lo usuarios activos"
        '
        'grpResultados
        '
        Me.tblOrganizadorBusqueda.SetColumnSpan(Me.grpResultados, 2)
        Me.grpResultados.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grpResultados.Location = New System.Drawing.Point(3, 86)
        Me.grpResultados.Name = "grpResultados"
        '
        'grpResultados.Panel
        '
        Me.grpResultados.Panel.Controls.Add(Me.dgvResultadosBuscador)
        Me.grpResultados.Size = New System.Drawing.Size(230, 242)
        Me.grpResultados.TabIndex = 6
        Me.grpResultados.Text = "Resultados"
        Me.grpResultados.Values.Heading = "Resultados"
        '
        'dgvResultadosBuscador
        '
        Me.dgvResultadosBuscador.AllowUserToAddRows = False
        Me.dgvResultadosBuscador.AllowUserToDeleteRows = False
        Me.dgvResultadosBuscador.AllowUserToResizeRows = False
        Me.dgvResultadosBuscador.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colActivo, Me.colUsuario})
        Me.dgvResultadosBuscador.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvResultadosBuscador.Location = New System.Drawing.Point(0, 0)
        Me.dgvResultadosBuscador.MultiSelect = False
        Me.dgvResultadosBuscador.Name = "dgvResultadosBuscador"
        Me.dgvResultadosBuscador.ReadOnly = True
        Me.dgvResultadosBuscador.RowHeadersVisible = False
        Me.dgvResultadosBuscador.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvResultadosBuscador.Size = New System.Drawing.Size(226, 218)
        Me.dgvResultadosBuscador.TabIndex = 0
        '
        'colActivo
        '
        Me.colActivo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colActivo.DataPropertyName = "Activo"
        Me.colActivo.HeaderText = "Activo"
        Me.colActivo.Name = "colActivo"
        Me.colActivo.ReadOnly = True
        Me.colActivo.Width = 51
        '
        'colUsuario
        '
        Me.colUsuario.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colUsuario.DataPropertyName = "usuario"
        Me.colUsuario.HeaderText = "Usuario"
        Me.colUsuario.Name = "colUsuario"
        Me.colUsuario.ReadOnly = True
        Me.colUsuario.Width = 174
        '
        'splSeparador
        '
        Me.splSeparador.Cursor = System.Windows.Forms.Cursors.Default
        Me.splSeparador.Dock = System.Windows.Forms.DockStyle.Fill
        Me.splSeparador.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.splSeparador.Location = New System.Drawing.Point(0, 57)
        Me.splSeparador.Name = "splSeparador"
        '
        'splSeparador.Panel1
        '
        Me.splSeparador.Panel1.Controls.Add(Me.hdrBusqueda)
        '
        'splSeparador.Panel2
        '
        Me.splSeparador.Panel2.Controls.Add(Me.tabUsuario)
        Me.splSeparador.Size = New System.Drawing.Size(708, 368)
        Me.splSeparador.SplitterDistance = 238
        Me.splSeparador.TabIndex = 1
        '
        'hdrBusqueda
        '
        Me.hdrBusqueda.AutoSize = True
        Me.hdrBusqueda.ButtonSpecs.AddRange(New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup() {Me.btnOcultarBusqueda})
        Me.hdrBusqueda.Dock = System.Windows.Forms.DockStyle.Fill
        Me.hdrBusqueda.HeaderVisibleSecondary = False
        Me.hdrBusqueda.Location = New System.Drawing.Point(0, 0)
        Me.hdrBusqueda.Name = "hdrBusqueda"
        '
        'hdrBusqueda.Panel
        '
        Me.hdrBusqueda.Panel.Controls.Add(Me.tblOrganizadorBusqueda)
        Me.hdrBusqueda.Size = New System.Drawing.Size(238, 368)
        Me.hdrBusqueda.TabIndex = 0
        Me.ToolTip1.SetToolTip(Me.hdrBusqueda, "Configure los distintos filtros de b�squeda para localizar el elemento deseado")
        Me.hdrBusqueda.ValuesPrimary.Heading = "Buscar"
        Me.hdrBusqueda.ValuesPrimary.Image = Global.Escritorio.My.Resources.Resources.Buscar_32
        '
        'btnOcultarBusqueda
        '
        Me.btnOcultarBusqueda.Type = ComponentFactory.Krypton.Toolkit.PaletteButtonSpecStyle.ArrowLeft
        Me.btnOcultarBusqueda.UniqueName = "BE70F7722CF94C60618F65819BBF010D"
        '
        'tblOrganizadorBusqueda
        '
        Me.tblOrganizadorBusqueda.BackColor = System.Drawing.Color.Transparent
        Me.tblOrganizadorBusqueda.ColumnCount = 2
        Me.tblOrganizadorBusqueda.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblOrganizadorBusqueda.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblOrganizadorBusqueda.Controls.Add(Me.lblBUsuario, 0, 0)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.txtBUsuario, 1, 0)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.btnBBuscar, 0, 2)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.chkBActivo, 0, 1)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.grpResultados, 0, 3)
        Me.tblOrganizadorBusqueda.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblOrganizadorBusqueda.Location = New System.Drawing.Point(0, 0)
        Me.tblOrganizadorBusqueda.Name = "tblOrganizadorBusqueda"
        Me.tblOrganizadorBusqueda.RowCount = 4
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorBusqueda.Size = New System.Drawing.Size(236, 331)
        Me.tblOrganizadorBusqueda.TabIndex = 0
        '
        'tabUsuario
        '
        Me.tabUsuario.Button.CloseButtonAction = ComponentFactory.Krypton.Navigator.CloseButtonAction.None
        Me.tabUsuario.Button.CloseButtonDisplay = ComponentFactory.Krypton.Navigator.ButtonDisplay.Hide
        Me.tabUsuario.Button.ContextButtonAction = ComponentFactory.Krypton.Navigator.ContextButtonAction.None
        Me.tabUsuario.Button.ContextButtonDisplay = ComponentFactory.Krypton.Navigator.ButtonDisplay.Hide
        Me.tabUsuario.Button.NextButtonDisplay = ComponentFactory.Krypton.Navigator.ButtonDisplay.Hide
        Me.tabUsuario.Button.PreviousButtonDisplay = ComponentFactory.Krypton.Navigator.ButtonDisplay.Hide
        Me.tabUsuario.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabUsuario.Location = New System.Drawing.Point(0, 0)
        Me.tabUsuario.Name = "tabUsuario"
        Me.tabUsuario.Pages.AddRange(New ComponentFactory.Krypton.Navigator.KryptonPage() {Me.tbpDatosUsuario})
        Me.tabUsuario.SelectedIndex = 0
        Me.tabUsuario.Size = New System.Drawing.Size(465, 368)
        Me.tabUsuario.TabIndex = 0
        Me.tabUsuario.Text = "KryptonNavigator1"
        '
        'tbpDatosUsuario
        '
        Me.tbpDatosUsuario.AutoHiddenSlideSize = New System.Drawing.Size(200, 200)
        Me.tbpDatosUsuario.Controls.Add(Me.tblOrganizador)
        Me.tbpDatosUsuario.Flags = 65534
        Me.tbpDatosUsuario.LastVisibleSet = True
        Me.tbpDatosUsuario.MinimumSize = New System.Drawing.Size(50, 50)
        Me.tbpDatosUsuario.Name = "tbpDatosUsuario"
        Me.tbpDatosUsuario.Size = New System.Drawing.Size(463, 341)
        Me.tbpDatosUsuario.Text = "Datos usuario"
        Me.tbpDatosUsuario.ToolTipTitle = "Page ToolTip"
        Me.tbpDatosUsuario.UniqueName = "78A89271BFFA48717A893A32AC6D70E9"
        '
        'tblOrganizador
        '
        Me.tblOrganizador.BackColor = System.Drawing.Color.Transparent
        Me.tblOrganizador.ColumnCount = 3
        Me.tblOrganizador.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblOrganizador.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizador.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblOrganizador.Controls.Add(Me.hdrPermisos, 0, 2)
        Me.tblOrganizador.Controls.Add(Me.lblUsuario, 0, 0)
        Me.tblOrganizador.Controls.Add(Me.chkActivo, 0, 1)
        Me.tblOrganizador.Controls.Add(Me.txtUsuario, 2, 0)
        Me.tblOrganizador.Controls.Add(Me.chkRestablecerClave, 2, 1)
        Me.tblOrganizador.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblOrganizador.Location = New System.Drawing.Point(0, 0)
        Me.tblOrganizador.Name = "tblOrganizador"
        Me.tblOrganizador.RowCount = 3
        Me.tblOrganizador.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizador.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizador.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizador.Size = New System.Drawing.Size(463, 341)
        Me.tblOrganizador.TabIndex = 0
        '
        'hdrPermisos
        '
        Me.hdrPermisos.AutoSize = True
        Me.tblOrganizador.SetColumnSpan(Me.hdrPermisos, 3)
        Me.hdrPermisos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.hdrPermisos.HeaderVisibleSecondary = False
        Me.hdrPermisos.Location = New System.Drawing.Point(3, 55)
        Me.hdrPermisos.Name = "hdrPermisos"
        '
        'hdrPermisos.Panel
        '
        Me.hdrPermisos.Panel.Controls.Add(Me.chkPermisos)
        Me.hdrPermisos.Size = New System.Drawing.Size(457, 283)
        Me.hdrPermisos.TabIndex = 0
        Me.hdrPermisos.ValuesPrimary.Heading = "Permisos"
        Me.hdrPermisos.ValuesPrimary.Image = Nothing
        '
        'chkPermisos
        '
        Me.chkPermisos.CheckOnClick = True
        Me.chkPermisos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.chkPermisos.Location = New System.Drawing.Point(0, 0)
        Me.chkPermisos.Name = "chkPermisos"
        Me.chkPermisos.Size = New System.Drawing.Size(455, 251)
        Me.chkPermisos.TabIndex = 0
        '
        'chkRestablecerClave
        '
        Me.chkRestablecerClave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkRestablecerClave.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl
        Me.chkRestablecerClave.Location = New System.Drawing.Point(309, 29)
        Me.chkRestablecerClave.Name = "chkRestablecerClave"
        Me.chkRestablecerClave.Size = New System.Drawing.Size(151, 20)
        Me.chkRestablecerClave.TabIndex = 5
        Me.chkRestablecerClave.Text = "Restablecer Contrase�a"
        Me.chkRestablecerClave.Values.Text = "Restablecer Contrase�a"
        '
        'lblTitulo
        '
        Me.lblTitulo.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.Custom1
        Me.lblTitulo.Location = New System.Drawing.Point(5, 9)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(247, 34)
        Me.lblTitulo.TabIndex = 0
        Me.lblTitulo.Values.Image = CType(resources.GetObject("lblTitulo.Values.Image"), System.Drawing.Image)
        Me.lblTitulo.Values.Text = "Gesti�n de Usuarios"
        '
        'grpCabecera
        '
        Me.grpCabecera.Dock = System.Windows.Forms.DockStyle.Top
        Me.grpCabecera.GroupBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.PanelCustom1
        Me.grpCabecera.Location = New System.Drawing.Point(0, 0)
        Me.grpCabecera.Name = "grpCabecera"
        '
        'grpCabecera.Panel
        '
        Me.grpCabecera.Panel.Controls.Add(Me.lblTitulo)
        Me.grpCabecera.Size = New System.Drawing.Size(708, 57)
        Me.grpCabecera.StateCommon.Border.Color1 = System.Drawing.Color.DarkOrange
        Me.grpCabecera.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom
        Me.grpCabecera.StateCommon.Border.Width = 5
        Me.grpCabecera.TabIndex = 0
        '
        'epErrores
        '
        Me.epErrores.Alineacion = System.Windows.Forms.ErrorIconAlignment.MiddleLeft
        Me.epErrores.ContainerControl = Me
        '
        'GestorErrores1
        '
        Me.GestorErrores1.Alineacion = System.Windows.Forms.ErrorIconAlignment.TopLeft
        Me.GestorErrores1.ContainerControl = Me
        '
        'frmUsuario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(708, 425)
        Me.Controls.Add(Me.splSeparador)
        Me.Controls.Add(Me.grpCabecera)
        Me.Name = "frmUsuario"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Gesti�n de Usuarios"
        CType(Me.grpResultados.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpResultados.Panel.ResumeLayout(False)
        CType(Me.grpResultados, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpResultados.ResumeLayout(False)
        CType(Me.dgvResultadosBuscador, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.splSeparador.Panel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.splSeparador.Panel1.ResumeLayout(False)
        Me.splSeparador.Panel1.PerformLayout()
        CType(Me.splSeparador.Panel2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.splSeparador.Panel2.ResumeLayout(False)
        CType(Me.splSeparador, System.ComponentModel.ISupportInitialize).EndInit()
        Me.splSeparador.ResumeLayout(False)
        CType(Me.hdrBusqueda.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrBusqueda.Panel.ResumeLayout(False)
        CType(Me.hdrBusqueda, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrBusqueda.ResumeLayout(False)
        Me.tblOrganizadorBusqueda.ResumeLayout(False)
        Me.tblOrganizadorBusqueda.PerformLayout()
        CType(Me.tabUsuario, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabUsuario.ResumeLayout(False)
        CType(Me.tbpDatosUsuario, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpDatosUsuario.ResumeLayout(False)
        Me.tblOrganizador.ResumeLayout(False)
        Me.tblOrganizador.PerformLayout()
        CType(Me.hdrPermisos.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrPermisos.Panel.ResumeLayout(False)
        CType(Me.hdrPermisos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrPermisos.ResumeLayout(False)
        CType(Me.grpCabecera.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpCabecera.Panel.ResumeLayout(False)
        Me.grpCabecera.Panel.PerformLayout()
        CType(Me.grpCabecera, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpCabecera.ResumeLayout(False)
        CType(Me.epErrores, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GestorErrores1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents splSeparador As ComponentFactory.Krypton.Toolkit.KryptonSplitContainer
    Friend WithEvents hdrBusqueda As ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup
    Friend WithEvents btnOcultarBusqueda As ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup
    Friend WithEvents lblTitulo As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents grpCabecera As ComponentFactory.Krypton.Toolkit.KryptonGroup
    Friend WithEvents epErrores As Quadralia.Controles.Validacion.GestorErrores
    Friend WithEvents lblUsuario As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtUsuario As Quadralia.Controles.aTextBox
    Friend WithEvents lblBUsuario As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtBUsuario As Quadralia.Controles.aTextBox
    Friend WithEvents chkActivo As ComponentFactory.Krypton.Toolkit.KryptonCheckBox
    Friend WithEvents chkBActivo As ComponentFactory.Krypton.Toolkit.KryptonCheckBox
    Friend WithEvents btnBBuscar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents grpResultados As ComponentFactory.Krypton.Toolkit.KryptonGroupBox
    Friend WithEvents dgvResultadosBuscador As ComponentFactory.Krypton.Toolkit.KryptonDataGridView
    Friend WithEvents tblOrganizadorBusqueda As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents hdrPermisos As ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup
    Friend WithEvents tblOrganizador As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents chkRestablecerClave As ComponentFactory.Krypton.Toolkit.KryptonCheckBox
    Friend WithEvents chkPermisos As ComponentFactory.Krypton.Toolkit.KryptonCheckedListBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents tabUsuario As ComponentFactory.Krypton.Navigator.KryptonNavigator
    Friend WithEvents tbpDatosUsuario As ComponentFactory.Krypton.Navigator.KryptonPage
    Friend WithEvents GestorErrores1 As Quadralia.Controles.Validacion.GestorErrores
    Friend WithEvents colActivo As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colUsuario As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn

End Class
