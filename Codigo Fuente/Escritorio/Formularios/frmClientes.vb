Imports ComponentFactory.Krypton.Toolkit
Public Class frmClientes
#Region " DECLARACIONES "
    Private _EstadoFormulario As Quadralia.Enumerados.EstadoFormulario = Quadralia.Enumerados.EstadoFormulario.Espera  ' Indica el estado en el que se encuentra el formulario
    Private _Registro As Cliente = Nothing
    Private _Contexto As Entidades = Nothing

    Private _dgvLoad As Boolean = False ' Bandera para evitar que se cargue la primera fila cuando se asigna un origen de datos al DGV
#End Region
#Region " PROPIEDADES "
    Public Overrides Property Estado() As Quadralia.Enumerados.EstadoFormulario
        Get
            Return _EstadoFormulario
        End Get
        Set(ByVal value As Quadralia.Enumerados.EstadoFormulario)
            _EstadoFormulario = value

            hdrBusqueda.Panel.Enabled = (value = Quadralia.Enumerados.EstadoFormulario.Espera)
            tblOrganizadorDatos.Enabled = (value <> Quadralia.Enumerados.EstadoFormulario.Espera)
            vsrContactos.Enabled = tblOrganizadorDatos.Enabled
            vsrDirecciones.Enabled = tblOrganizadorDatos.Enabled
            htmlObservaciones.Enabled = tblOrganizadorDatos.Enabled            

            ' Tengo que establecer el foco en un control para que no quede "muerto" el ribbon
            If value = Quadralia.Enumerados.EstadoFormulario.Espera Then
                Me.ActiveControl = txtBNombre.TextBox
            Else
                Me.ActiveControl = txtCIF.TextBox
            End If

            ConfigurarEntorno()
            ControlarBotones()
        End Set
    End Property

    ''' <summary>
    ''' Registro que se muestra actualmente en el formulario
    ''' </summary>
    Public Property Registro As Cliente
        Get
            Return _Registro
        End Get
        Set(ByVal value As Cliente)
            _Registro = value
            CargarRegistro(value)

            frmPrincipal.btnClientesAccionesModificar.Enabled = (Estado = Quadralia.Enumerados.EstadoFormulario.Espera) AndAlso Registro IsNot Nothing
            frmPrincipal.btnClientesAccionesEliminar.Enabled = frmPrincipal.btnClientesAccionesModificar.Enabled
        End Set
    End Property

    Public ReadOnly Property Contexto As Entidades
        Get
            If _Contexto Is Nothing Then
                _Contexto = cDAL.Instancia.getContext()
                _Contexto.ContextOptions.LazyLoadingEnabled = True
            End If
            Return _Contexto
        End Get
    End Property
#End Region
#Region " IMPLEMENTACIONES NECESARIAS DEL FORMULARIO HIJO "
    ''' <summary>
    ''' Tab del ribbon asociado a este formulario
    ''' </summary>
    Public Overrides ReadOnly Property Tab() As ComponentFactory.Krypton.Ribbon.KryptonRibbonTab
        Get
            Return frmPrincipal.tabClientes
        End Get
    End Property
#End Region
#Region " LIMPIEZA "
    ''' <summary>
    ''' Limpia los controles relacionados con la b�squeda de registros
    ''' </summary>
    Private Sub LimpiarBusqueda() Handles btnLimpiarBusqueda.Click
        txtBCodigoDe.Clear()
        txtBCodigoA.Clear()
        txtBCif.Text = String.Empty
        txtBNombre.Text = String.Empty
        chkBEliminados.Checked = False

        dgvResultadosBuscador.DataSource = Nothing
        dgvResultadosBuscador.Rows.Clear()
    End Sub

    ''' <summary>
    ''' Limpia los controles relacionados con la edicion de registros
    ''' </summary>
    Private Sub LimpiarDatos()
        txtCIF.Clear()
        txtCodigo.Clear()
        txtRazonSocial.Clear()
        txtNombreComercial.Clear()
        txtTelefono1.Clear()
        txtTelefono2.Clear()
        txtFax.Clear()
        txtEmail.Clear()
        htmlObservaciones.Clear()
        txtWeb.Clear()
        txtAlerta.Clear()

        vsrContactos.Clear()
        vsrDirecciones.Clear()

        chkReactivarRegistro.Checked = False
        chkReactivarRegistro.Visible = False

        htmlObservaciones.Clear()

        LimpiarEnvios()
        epErrores.Clear()

        Me.Text = Me.Tag
    End Sub

    ''' <summary>
    ''' Borra todos los controles del formulario
    ''' </summary>
    Private Sub LimpiarTodo()
        LimpiarBusqueda()
        LimpiarDatos()
    End Sub

    Private Sub LimpiarEnvios()
        dgvAlbaranesSalida.DataSource = Nothing
        dgvAlbaranesSalida.Rows.Clear()

        txtFiltroSalidaCodigoDesde.Clear()
        txtFiltroSalidaCodigoHasta.Clear()
        dtpFiltroSalidaFechaDesde.Clear()
        dtpFiltroSalidaFechaHasta.Clear()

    End Sub
#End Region
#Region " CONTROL DE BOTONES "
    Private Sub ControlarBotones()
        ControlarBotonesContactos()
        ControlarBotonesDirecciones()
        ControlarRibbon()
    End Sub

    ''' <summary>
    ''' Controla el cambio de botones del formulario principal
    ''' </summary>
    Private Sub ControlarRibbon()
        If Me.ParentForm Is Nothing Then Exit Sub
        If Not TypeOf Me.ParentForm Is frmPrincipal Then Exit Sub

        With DirectCast(Me.ParentForm, frmPrincipal)
            .krgtClientesAccionesEdicion.Visible = (Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera)
            .krgtClientesAccionesEspera.Visible = Not .krgtClientesAccionesEdicion.Visible
            .mnuRapidoGuardar.Enabled = .krgtClientesAccionesEdicion.Visible

            .btnClientesAccionesModificar.Enabled = (Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera) AndAlso Registro IsNot Nothing
            .btnClientesAccionesEliminar.Enabled = .btnClientesAccionesModificar.Enabled
        End With
    End Sub

    ''' <summary>
    ''' Controla cuando se deben activar / desactivar los botones de crear/eliminar contactos
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ControlarBotonesContactos() Handles vsrContactos.CurrentChanged
        btnNuevoContacto.Enabled = IIf(vsrContactos.Enabled, ButtonEnabled.True, ButtonEnabled.False)
        btnEliminarContacto.Enabled = IIf(vsrContactos.Enabled AndAlso vsrContactos.Current IsNot Nothing, ButtonEnabled.True, ButtonEnabled.False)
    End Sub

    ''' <summary>
    ''' Controla cuando se deben activar / desactivar los botones de crear/eliminar contactos
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ControlarBotonesDirecciones() Handles vsrDirecciones.CurrentChanged
        btnNuevaDireccion.Enabled = IIf(vsrDirecciones.Enabled, ButtonEnabled.True, ButtonEnabled.False)
        btnEliminarDireccion.Enabled = IIf(vsrDirecciones.Enabled AndAlso vsrDirecciones.Current IsNot Nothing, ButtonEnabled.True, ButtonEnabled.False)
    End Sub
#End Region
#Region " RUTINA DE INICIO / FIN Y CARGA DE DATOS MAESTROS "
    ''' <summary>
    ''' Rutina de inicio del formulario
    ''' </summary>
    Private Sub Inicio(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Tag = Me.Text
        tabGeneral.SelectedIndex = 0

        ' Cargamos los datos maestros
        CargarDatosMaestros()
        ConfigurarEntorno()

        LimpiarTodo()

        ' Preparar editor RTF
        PrepararEditorRTF()

        ' Asigno contextos
        vsrContactos.Contexto = Me.Contexto

        ' No quiero nuevos Datos
        dgvResultadosBuscador.AutoGenerateColumns = False
        dgvAlbaranesSalida.AutoGenerateColumns = False

        ' Escondo el panel de b�squeda y pongo el formulario en modo de espera
        If Not cConfiguracionAplicacion.Instancia.BuscadoresAbiertos Then OcultarBusquedaDobleClick(hdrBusqueda, Nothing)
        If Not cConfiguracionAplicacion.Instancia.FiltrosAbiertos Then OcultarFiltroAlbaranesSalidaDobleClick(hdrAlbaranes, Nothing)

        Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera

        For Each UnControl As Control In tblOrganizadorDatos.Controls
            Quadralia.Formularios.Funcionalidad.ManejadorTabPorIntro(UnControl)
        Next
    End Sub

    ''' <summary>
    ''' Carga los datos maestros
    ''' </summary>
    Public Sub CargarDatosMaestros()
    End Sub

    ''' <summary>
    ''' Elimina las opciones del editor que no se van a utilizar
    ''' </summary>
    Private Sub PrepararEditorRTF()
        With htmlObservaciones
            .tsbGuardar.Visible = False
            .tsbEliminar.Visible = False
            .ConvertirImagenesABase64 = True
        End With
    End Sub
#End Region
#Region " BUSQUEDA, CARGA DE Y EDICION DE REGISTROS "
    ''' <summary>
    ''' Realiza la b�squeda de registros coincidentes con los par�metros especificados
    ''' </summary>
    Private Sub Buscar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBBuscar.Click
        Dim Registros = From Pro As Cliente In Contexto.Clientes _
                        Where ((Pro.razonSocial.ToUpper.Contains(txtBNombre.Text.ToUpper) OrElse (String.IsNullOrEmpty(txtBNombre.Text))) _
                        OrElse (Pro.nombreComercial.ToUpper.Contains(txtBNombre.Text.ToUpper) OrElse (String.IsNullOrEmpty(txtBNombre.Text)))) _
                        AndAlso (Pro.cif.ToUpper.Contains(txtBCif.Text.ToUpper) OrElse (String.IsNullOrEmpty(txtBCif.Text))) _
                        AndAlso (Pro.codigo.ToUpper >= txtBCodigoDe.Text.ToUpper OrElse String.IsNullOrEmpty(txtBCodigoDe.Text)) _
                        AndAlso (Pro.codigo.ToUpper <= txtBCodigoA.Text.ToUpper OrElse String.IsNullOrEmpty(txtBCodigoA.Text)) _
                        AndAlso (chkBEliminados.Checked OrElse Not Pro.fechaBaja.HasValue)
                        Order By Pro.razonSocial
                        Select Pro

        Contexto.Refresh(Objects.RefreshMode.StoreWins, Registros)

        If Registros IsNot Nothing Then
            _dgvLoad = (Registros.Count > 1)
            dgvResultadosBuscador.DataSource = Registros.Distinct.ToList
            _dgvLoad = False
        End If

        dgvResultadosBuscador.ClearSelection()
        dgvResultadosBuscador.Focus()
    End Sub

    ''' <summary>
    ''' Se prepara el formulario para introducir un nuevo registro
    ''' </summary>    
    Public Overrides Sub Nuevo()
        LimpiarDatos()
        Me.Estado = Quadralia.Enumerados.EstadoFormulario.Anhadiendo

        Dim Aux = New Cliente

        ' Inicializaciones de nuevos registros

        ' Lo a�ado al contexto
        Contexto.Clientes.AddObject(Aux)
        Me.Registro = Aux
    End Sub

    ''' <summary>
    ''' Se carga el registro seleccionado el formulario
    ''' </summary>
    Private Sub Cargar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvResultadosBuscador.SelectionChanged
        If dgvResultadosBuscador.SelectedRows.Count > 0 Then
            Me.Registro = dgvResultadosBuscador.SelectedRows(0).DataBoundItem
        Else
            Me.Registro = Nothing
        End If
    End Sub

    ''' <summary>
    ''' Carga el registro actual del formulario
    ''' </summary>
    Private Sub CargarRegistro()
        CargarRegistro(Me.Registro)
    End Sub

    ''' <summary>
    ''' Se carga un registro en el formulario
    ''' </summary>
    Private Sub CargarRegistro(ByVal Instancia As Cliente)
        ' Si el DGV est� vinculando un origen de datos, no cargo nada
        If _dgvLoad Then Exit Sub

        ' Limpio el formulario y cargo el registro
        LimpiarDatos()

        ControlarBotones()
        If Instancia Is Nothing Then Exit Sub

        With Instancia
            If Instancia.EntityState <> System.Data.EntityState.Added Then .Direcciones.Load()
            If Instancia.EntityState <> System.Data.EntityState.Added Then .Contactos.Load()

            txtCIF.Text = .cif
            txtCodigo.Text = .codigo
            txtRazonSocial.Text = .razonSocial
            txtNombreComercial.Text = .nombreComercial
            txtEmail.Text = .email
            txtTelefono1.Text = .telefono1
            txtTelefono2.Text = .telefono2
            txtFax.Text = .fax
            txtWeb.Text = .web
            txtAlerta.Text = .alerta

            If .Contactos IsNot Nothing Then vsrContactos.Items = .Contactos
            If .Direcciones IsNot Nothing Then vsrDirecciones.Items = .Direcciones

            htmlObservaciones.Text = .observaciones

            chkReactivarRegistro.Visible = .fechaBaja.HasValue

            Me.Text = String.Format("{0} [{1}]", Me.Tag, .nombreComercial)
        End With

        ' Cargamos la navegabilidad
        If tbpLotesSalida.Visible Then RefrescarAlbaranes()

        _Registro = Instancia
    End Sub

    ''' <summary>
    ''' Preparamos el formulario para modificar un registro cargado
    ''' </summary>
    Public Overrides Sub Modificar()
        If Me.Registro IsNot Nothing Then Me.Estado = Quadralia.Enumerados.EstadoFormulario.Editando
    End Sub

    ''' <summary>
    ''' Eliminaci�n l�gica del registro
    ''' </summary>
    Public Overrides Sub Eliminar()
        'If dgvResultadosBuscador.SelectedRows.Count <> 1 Then Exit Sub

        If Registro IsNot Nothing Then
            If Quadralia.Aplicacion.MostrarMessageBox(My.Resources.ConfirmarEliminacion, My.Resources.TituloConfirmarEliminacion, MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
                Me.Registro.fechaBaja = DateTime.Now
                Me.Contexto.SaveChanges()

                LimpiarDatos()
                If dgvResultadosBuscador.SelectedRows.Count > 0 Then Buscar(Nothing, Nothing) Else Cargar(Nothing, Nothing)
                Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera
            End If
        End If
    End Sub

    ''' <summary>
    ''' Cancela la edici�n del registro
    ''' </summary>
    Public Overrides Sub Cancelar()
        If Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera Then Exit Sub

        ' Cambio el estado del formulario
        Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera

        ' Limpio los datos y desecho los cambios
        Quadralia.Linq.CancelarCambios(Me.Contexto)

        ' Limpio los datos
        LimpiarDatos()

        ' Cargo el registro seleccionado
        Cargar(Nothing, Nothing)
    End Sub

    ''' <summary>
    ''' Se guardan los datos en la base de datos
    ''' </summary>
    ''' <param name="MostrarMensaje">Indica si se muestra un mensaje con el resultado de la funci�n</param>
    Public Overrides Function Guardar(Optional ByVal MostrarMensaje As Boolean = True) As Boolean
        If Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera Then Return True

        ' Valido los controles
        Me.ValidateChildren(ValidationConstraints.Visible + ValidationConstraints.Enabled)

        If epErrores.HasErrors Then
            If MostrarMensaje Then Quadralia.Aplicacion.MostrarMessageBox(My.Resources.ErroresEncontrados, My.Resources.TituloErroresEncontrados, MessageBoxButtons.OK, MessageBoxIcon.Error)
            epErrores.Controles(0).Focus()
            Return False
        End If

        ' Guardo los datos en el registro
        With Registro
            .cif = txtCIF.Text
            .codigo = txtCodigo.Text
            .razonSocial = txtRazonSocial.Text
            .nombreComercial = txtNombreComercial.Text
            .email = txtEmail.Text
            .telefono1 = txtTelefono1.Text
            .telefono2 = txtTelefono2.Text
            .fax = txtFax.Text
            .web = txtWeb.Text
            .alerta = txtAlerta.Text

            If chkReactivarRegistro.Visible AndAlso chkReactivarRegistro.Checked Then .fechaBaja = Nothing

            .observaciones = htmlObservaciones.Text
        End With

        Try
            ' Guardo el registro en base de datos
            Contexto.SaveChanges()

            ' Si estoy a�adiendo, obtengo un n�mero de Albar�n
            If Me.Estado = Quadralia.Enumerados.EstadoFormulario.Anhadiendo AndAlso String.IsNullOrEmpty(txtCodigo.Text) Then
                Dim Aux As New Data.Objects.ObjectParameter("codigoCli", Registro.codigo)
                Dim Resultado = Contexto.CalcularCodigoCliente(Registro.id, Aux)
                Registro.codigo = Aux.Value
                Contexto.AcceptAllChanges()
            End If

            If MostrarMensaje Then Quadralia.Aplicacion.MostrarMensajeNtfIco(My.Resources.TituloDatosGuardatos, My.Resources.DatosGuardados, ToolTipIcon.Info)

            ' Restablezco el formulario
            LimpiarDatos()
            CargarRegistro()
            Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera

            Return True
        Catch ex As Exception
            If MostrarMensaje Then Quadralia.Aplicacion.InformarError(ex)
            Return False
        End Try
    End Function
#End Region
#Region " RESTO DE FUNCIONES "
    ''' <summary>
    ''' Oculta / muestra el panel de b�squeda
    ''' </summary>
    Private Sub OcultarBusqueda(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOcultarBusqueda.Click
        Quadralia.KryptonForms.EsconderPanel(splSeparador, hdrBusqueda)
    End Sub

    ''' <summary>
    ''' Control de las teclas de acci�n del usuario
    ''' </summary>
    Private Sub ControlTeclasBuscador(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvResultadosBuscador.KeyDown
        If Me.Registro Is Nothing Then Exit Sub

        Select Case e.KeyCode
            Case Keys.Delete
                e.Handled = True
                Eliminar()
            Case Keys.F2, Keys.Enter
                e.Handled = True
                Modificar()
        End Select
    End Sub

    ''' <summary>
    ''' Ordena los resultados del DGV de Resultados    
    ''' </summary>
    Private Sub OrdenarResultados(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvResultadosBuscador.ColumnHeaderMouseClick
        Quadralia.Formularios.Funcionalidad.OrdenarDGV(Of Cliente)(dgvResultadosBuscador, e)
    End Sub

    ''' <summary>
    ''' Cuando el usuario hace doble click en el panel, desencadenamos la misma funci�n que si hiciera click en el bot�n
    ''' </summary>
    Private Sub OcultarBusquedaDobleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hdrBusqueda.DoubleClick
        ' Hay que hacerlo as�, no sirve con llamar a la funci�n directamente. si no, el bot�n del header se descontrola y desaparece el interior del panel
        DirectCast(sender, KryptonHeaderGroup).ButtonSpecs(0).PerformClick()
    End Sub

    ''' <summary>
    ''' Lanzamos la b�squeda de registros si el usuario presiona ENTER en los campos de par�metros
    ''' </summary>
    Private Sub ManejadorBusquedasTexto(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtBCif.KeyUp, txtBNombre.KeyUp, txtBCodigoA.KeyUp, txtBCodigoDe.KeyUp
        If e.KeyCode = Keys.Enter Then
            e.Handled = True
            Buscar(Nothing, Nothing)
        End If
    End Sub

    ''' <summary>
    ''' Evita que salga el molesto error del DGV
    ''' </summary>
    Private Sub EvitarErrores(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvResultadosBuscador.DataError
        e.Cancel = True
    End Sub

    ''' <summary>
    ''' Pone el formulario en modo edici�n cuando el usuario hace doble click sobre un resultado del buscador
    ''' </summary>
    Private Sub ActivarEdicionRegistro(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvResultadosBuscador.CellDoubleClick
        If e.RowIndex > -1 Then Modificar()
    End Sub

    ''' <summary>
    ''' Nos desplaza a traves de los controles al pulsar enter
    ''' </summary>
    Private Sub EnterComoTab(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.Enter AndAlso (Me.ActiveControl Is Nothing OrElse Me.ActiveControl.Parent Is Nothing OrElse Not TypeOf (Me.ActiveControl.Parent) Is KryptonTextBox OrElse Not DirectCast(Me.ActiveControl.Parent, KryptonTextBox).Multiline) Then
            e.Handled = True
            SendKeys.Send("{TAB}")
        End If
    End Sub

    Private Sub txtRazonSocial_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRazonSocial.LostFocus
        If Estado = Quadralia.Enumerados.EstadoFormulario.Anhadiendo AndAlso String.IsNullOrEmpty(txtNombreComercial.Text) Then
            txtNombreComercial.Text = txtRazonSocial.Text
            txtNombreComercial.Focus()
            txtNombreComercial.SelectAll()
        End If
    End Sub
#End Region
#Region " VALIDACIONES "
    Private Sub textboxObligatorio(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtNombreComercial.Validating, txtRazonSocial.Validating
        If Not TypeOf (sender) Is KryptonTextBox Then Exit Sub

        If Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera AndAlso String.IsNullOrEmpty(DirectCast(sender, KryptonTextBox).Text) Then ' Que no est� vac�o
            epErrores.SetError(sender, My.Resources.TextBoxObligatorio)
        Else
            epErrores.SetError(sender, "")
        End If
    End Sub

    Private Sub VerficarCodigo(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtCodigo.Validating
        If Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera Then
            If txtCodigo.Text.Trim = "" Then
                epErrores.SetError(sender, My.Resources.TextBoxObligatorio)
            Else
                If Not String.IsNullOrEmpty(txtCodigo.Text) AndAlso (From It As Cliente In Contexto.Clientes Where It.codigo.Trim().ToUpper() = txtCodigo.Text.Trim().ToUpper() AndAlso It.id <> Registro.id Select It).Count > 0 Then
                    epErrores.SetError(txtCodigo, "El c�digo introducido se encuentra actualmente en uso. Por favor, rev�selo")
                Else
                    epErrores.SetError(txtCodigo, "")
                End If
            End If
        End If
    End Sub

    ''' <summary>
    ''' Valida que el usuario introduzca una direcci�n de correo electr�nico v�lida
    ''' </summary>
    Private Sub ValidarEmail(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtEmail.Validating
        If Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera AndAlso Not String.IsNullOrEmpty(txtEmail.Text.Trim) AndAlso Not Quadralia.Validadores.QuadraliaValidadores.ValidarEmail(txtEmail.Text) Then
            epErrores.SetError(txtEmail, "El correo electr�nico introducido no es correcto. Por favor, rev�selo")
        Else
            epErrores.SetError(txtEmail, "")
        End If
    End Sub

    ''' <summary>
    ''' Obliga a un textbox a aceptar �nicamente valore num�ricos
    ''' </summary>
    Private Sub SoloNumeros(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtTelefono2.KeyPress, txtTelefono1.KeyPress, txtFax.KeyPress
        Quadralia.Validadores.QuadraliaValidadores.SoloNumeros(sender, e)
    End Sub

    Private Sub txtCuentaContable_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
        'If String.IsNullOrEmpty(txtCuentaContable.Text) OrElse (txtCuentaContable.Text <> "" AndAlso txtCuentaContable.Text.Length = 8) Then
        '    epErrores.SetError(txtCuentaContable, "")
        'Else
        '    epErrores.SetError(txtCuentaContable, "El n�mero de d�gitos para la cuenta contable tiene que ser 8.")
        'End If
    End Sub

    ''' <summary>
    ''' Valida el NIF / CIF introducido por el usuario
    ''' </summary>
    Private Sub ValidarCIF(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtCIF.Validating
        If Not TypeOf (sender) Is KryptonTextBox Then Exit Sub

        Dim Ctrl As KryptonTextBox = DirectCast(sender, KryptonTextBox)
        If Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera OrElse String.IsNullOrEmpty(Ctrl.Text) Then
            epErrores.SetError(sender, "")
        ElseIf (Quadralia.Validadores.QuadraliaValidadores.esNif(Ctrl.Text) OrElse Quadralia.Validadores.QuadraliaValidadores.esCif(Ctrl.Text)) AndAlso Not Quadralia.Validadores.QuadraliaValidadores.validarNIFCIF(Ctrl.Text) Then
            epErrores.SetError(sender, "El CIF/NIF introducido no es correcto. Por favor, rev�selo")
        ElseIf Registro IsNot Nothing AndAlso (From prov As Cliente In Contexto.Clientes Where prov.cif.ToUpper = Ctrl.Text.ToUpper And prov.id <> Registro.id Select prov).Count > 0 Then ' Duplicados
            epErrores.SetError(sender, "El CIF/NIF introducido ya se encuentra actualmente en uso")
        Else
            epErrores.SetError(sender, "")
        End If
    End Sub
#End Region
#Region " CONTACTOS "
    ''' <summary>
    ''' A�ade un nuevo contacto al Cliente actual
    ''' </summary>
    Private Sub AnhadirContacto(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevoContacto.Click
        Dim visorContacto As New frmContacto
        visorContacto.Contacto = New ContactoCliente

        If visorContacto.ShowDialog() = Windows.Forms.DialogResult.OK Then
            Registro.Contactos.Add(visorContacto.Contacto)
            vsrContactos.Refrescar()
        End If
    End Sub

    ''' <summary>
    ''' Elimina el contacto del Cliente
    ''' </summary>
    Private Sub EliminarContacto(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminarContacto.Click
        If vsrContactos.Current Is Nothing Then Exit Sub
        vsrContactos.Current.Eliminar()
    End Sub
#End Region
#Region " DIRECCIONES "
    ''' <summary>
    ''' A�ade un nuevo contacto al Cliente actual
    ''' </summary>
    Private Sub AnhadirDireccion(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevaDireccion.Click
        Dim visorDireccion As New frmDirecciones(True, Me.Contexto)
        Dim NuevaDireccion As New DireccionCliente
        NuevaDireccion.pais = "Espa�a"
        NuevaDireccion.tipo = IDireccion.TiposDireccion.General
        visorDireccion.Direccion = NuevaDireccion

        If visorDireccion.ShowDialog() = Windows.Forms.DialogResult.OK Then
            Registro.Direcciones.Add(visorDireccion.Direccion)
            vsrDirecciones.Refrescar()
        End If
    End Sub

    ''' <summary>
    ''' Elimina el contacto del Cliente
    ''' </summary>
    Private Sub EliminarDireccion(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminarDireccion.Click
        If vsrDirecciones.Current Is Nothing Then Exit Sub
        vsrDirecciones.Current.Eliminar()
    End Sub
#End Region
#Region " SEGURIDAD "
    ''' <summary>
    ''' Configura el formulario en base a los permisos del usuario
    ''' </summary>
    Private Sub ConfigurarEntorno()
        tbpLotesSalida.Visible = Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera AndAlso Quadralia.Aplicacion.UsuarioConectado IsNot Nothing AndAlso Quadralia.Aplicacion.PuedeAccederA(Quadralia.Aplicacion.Permisos.LOTES_SALIDA)
    End Sub
#End Region
#Region " NAVEGABILIDAD "
#Region " ALBARANES SALIDA"
    Private Sub RefrescarAlbaranes() Handles txtFiltroSalidaCodigoDesde.TextChanged, txtFiltroSalidaCodigoHasta.TextChanged, dtpFiltroSalidaFechaDesde.ValueNullableChanged, dtpFiltroSalidaFechaHasta.ValueNullableChanged

        dgvAlbaranesSalida.DataSource = Nothing
        dgvAlbaranesSalida.Rows.Clear()

        ' Validaciones
        If Registro Is Nothing OrElse Registro.LotesSalida Is Nothing OrElse Registro.LotesSalida.Count = 0 Then
            Exit Sub
        End If

        ' Filtro los lotes
        Dim albaranes = (From It As LoteSalida In Registro.LotesSalida
                         Where (String.IsNullOrEmpty(txtFiltroSalidaCodigoDesde.Text) OrElse It.codigo.ToUpper >= txtFiltroSalidaCodigoDesde.Text.ToUpper) _
                         AndAlso (String.IsNullOrEmpty(txtFiltroSalidaCodigoHasta.Text) OrElse It.codigo.ToUpper <= txtFiltroSalidaCodigoHasta.Text.ToUpper) _
                         AndAlso (dtpFiltroSalidaFechaDesde.ValueNullable Is DBNull.Value OrElse (It.fecha.Date >= dtpFiltroSalidaFechaDesde.Value.Date)) _
                         AndAlso (dtpFiltroSalidaFechaHasta.ValueNullable Is DBNull.Value OrElse (It.fecha.Date <= dtpFiltroSalidaFechaHasta.Value.Date))
                         Order By It.codigo, It.fecha
                         Select It).ToList

        dgvAlbaranesSalida.DataSource = albaranes
        dgvAlbaranesSalida.ClearSelection()
    End Sub

    Private Sub VerLoteSalida(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvAlbaranesSalida.DoubleClick
        If Not cConfiguracionAplicacion.Instancia.PermitirNavegabilidad Then Exit Sub

        If dgvAlbaranesSalida.SelectedRows.Count > 0 AndAlso Quadralia.Aplicacion.PuedeAccederA(Quadralia.Aplicacion.Permisos.LOTES_SALIDA) Then
            Dim FormularioAlbaran As frmLoteSalida = Quadralia.Aplicacion.AbrirFormulario(frmPrincipal, "Escritorio.frmLoteSalida", Quadralia.Aplicacion.Permisos.LOTES_SALIDA, My.Resources.Salida_32)
            FormularioAlbaran.IdRegistro = DirectCast(dgvAlbaranesSalida.SelectedRows(0).DataBoundItem, LoteSalida).id
        End If
    End Sub

    ''' <summary>
    ''' Ordena los registros por la columna deseada
    ''' </summary>
    Private Sub OrdenarAlbaranes(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvAlbaranesSalida.ColumnHeaderMouseClick
        Quadralia.Formularios.Funcionalidad.OrdenarDGV(Of LoteSalida)(dgvAlbaranesSalida, e)
    End Sub

    ''' <summary>
    ''' Oculta el filtro de albaranes
    ''' </summary>
    Private Sub OcultarFiltroAlbaranesSalida(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOcultarFiltroAlbaranes.Click
        Quadralia.KryptonForms.EsconderPanel(Me.splSeparadorAlbaranesSalida, hdrAlbaranes, True)
    End Sub

    ''' <summary>
    ''' Oculta el filtro de albaranes
    ''' </summary>
    Private Sub OcultarFiltroAlbaranesSalidaDobleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hdrAlbaranes.DoubleClick
        btnOcultarFiltroAlbaranes.PerformClick()
    End Sub
#End Region
#End Region
End Class