﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPrincipal
    Inherits ComponentFactory.Krypton.Toolkit.KryptonForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim krgExpedidoresAcciones As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPrincipal))
        Me.krgtExpedidoresAccionesEspera = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnExpedidoresAccionesNuevo = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnExpedidoresAccionesModificar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnExpedidoresAccionesEliminar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.krgtExpedidoresAccionesEdicion = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnExpedidoresAccionesGuardar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnExpedidoresAccionesCancelar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.krgtExpedidoresAccionesCerrar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnExpedidoresAccionesCerrar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.tmrReloj = New System.Windows.Forms.Timer(Me.components)
        Me.mngManager = New ComponentFactory.Krypton.Toolkit.KryptonManager(Me.components)
        Me.Paleta = New ComponentFactory.Krypton.Toolkit.KryptonPalette(Me.components)
        Me.ntfIcono = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.mnuFIxRibbon = New System.Windows.Forms.MenuStrip()
        Me.mnuOrbCabeceraConfiguracion = New ComponentFactory.Krypton.Toolkit.KryptonContextMenuHeading()
        Me.mnuOrbConfiguracionPrograma = New ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem()
        Me.mnuOrbUsuarios = New ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem()
        Me.mnuOrbDatosMaestros = New ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem()
        Me.mnuOrbCabeceraSalirAplicacion = New ComponentFactory.Krypton.Toolkit.KryptonContextMenuHeading()
        Me.mnuSalir = New ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem()
        Me.mnuConfiguracionPrograma = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.rtnConfiguracionPrograma = New ComponentFactory.Krypton.Toolkit.KryptonContextMenu()
        Me.rtnConfiguracionProgramaCabecera = New ComponentFactory.Krypton.Toolkit.KryptonContextMenuItems()
        Me.rtnConfiguracionProgramaBaseDatos = New ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem()
        Me.rtnConfiguracionProgramaParametrosGlobales = New ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem()
        Me.rtnConfiguracionProgramaCopiaSeguridad = New ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem()
        Me.rtnConfiguracionProgramaDatosMaestros = New ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem()
        Me.mnuConfiguracionUsuarios = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.krtGeneralConfiguracion1 = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnSincronizarTrazamare = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.rtnSincronizacionEtiquetas = New ComponentFactory.Krypton.Toolkit.KryptonContextMenu()
        Me.rtnSincronizacionEtiquetasElementos = New ComponentFactory.Krypton.Toolkit.KryptonContextMenuItems()
        Me.rtnSincronizacionEtiquetasVerLog = New ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem()
        Me.mnuConfiguracionAcercaDe = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.mnuAyudaAyuda2 = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.mnuConfiguracionComprobarActualizaciones = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.krgGeneralConfiguracion = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup()
        Me.tabGeneral = New ComponentFactory.Krypton.Ribbon.KryptonRibbonTab()
        Me.krgGeneralDatosAdministrativos = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup()
        Me.krtGeneralAdministrativos = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.mnuDatosAdministrativosExpedidores = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.mnuDatosAdministrativosProveedores = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.mnuDatosAdministrativosClientes = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.krgGeneralLotes = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup()
        Me.krtGeneralLotes = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.mnuDatosAdministrativosLotesEntrada = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.mnuAlbaranesEntrada = New ComponentFactory.Krypton.Toolkit.KryptonContextMenu()
        Me.mnuAlbaranesEntradaElementos = New ComponentFactory.Krypton.Toolkit.KryptonContextMenuItems()
        Me.mnuAlbaranesEntradaImportar = New ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem()
        Me.mnuDatosAdministrativosEtiquetas = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.rtnEtiquetas = New ComponentFactory.Krypton.Toolkit.KryptonContextMenu()
        Me.rtnEtiquetasElementos = New ComponentFactory.Krypton.Toolkit.KryptonContextMenuItems()
        Me.rtnEtiquetasGeneracionAutomatica = New ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem()
        Me.KryptonRibbonGroupTriple2 = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.mnuDatosAdministrativosEmpaquetado = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.mnuDatosAdministrativosLotesSalida = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.mnuAyudaAyuda = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup()
        Me.KryptonRibbonGroupTriple1 = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.mnuAyudaWebQuadralia = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.mnuAyudaAyudaSoporteRemoto = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.tabInformes = New ComponentFactory.Krypton.Ribbon.KryptonRibbonTab()
        Me.krgInformesStock = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup()
        Me.krtInformesStock1 = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.mnuInformesStock_Stock = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.krgInformesVentas = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup()
        Me.krgtInformesVentas1 = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.mnuInformesVentas_Ventas = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnUsuariosAccionesNuevo = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnUsuariosAccionesModificar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnUsuariosAccionesEliminar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.krgtUsuariosAccionesEspera = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnUsuariosAccionesGuardar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnUsuariosAccionesCancelar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.krgtUsuariosAccionesEdicion = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnUsuariosSalir = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.krgtUsuariosSalir = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.krgUsuariosAcciones = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup()
        Me.tabUsuarios = New ComponentFactory.Krypton.Ribbon.KryptonRibbonTab()
        Me.mnuRapidoCambiarContrasenha = New ComponentFactory.Krypton.Ribbon.KryptonRibbonQATButton()
        Me.mnuRapidoGuardar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonQATButton()
        Me.rbbPrincipal = New ComponentFactory.Krypton.Ribbon.KryptonRibbon()
        Me.mnuOrbDatosAdministrativos = New ComponentFactory.Krypton.Toolkit.KryptonContextMenuHeading()
        Me.mnuOrbExpedidores = New ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem()
        Me.mnuOrbProveedores = New ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem()
        Me.mnuOrbClientes = New ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem()
        Me.mnuOrbLotes = New ComponentFactory.Krypton.Toolkit.KryptonContextMenuHeading()
        Me.mnuOrbLotesEntrada = New ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem()
        Me.mnuOrbEtiquetas = New ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem()
        Me.mnuOrbEmpaquetado = New ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem()
        Me.mnuOrbLotesSalida = New ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem()
        Me.mnuOrbEtiquetasGeneracionAutomatica = New ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem()
        Me.mnuOrbCopiaSeguridad = New ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem()
        Me.mnuOrbBaseDatos = New ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem()
        Me.tabVentanas = New ComponentFactory.Krypton.Ribbon.KryptonRibbonTab()
        Me.krgVentanas = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup()
        Me.krgtVentanas1 = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnVentanasMinimizar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnVentanasRestaurar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnVentanasCerrar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.krgtVentanas2 = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnVentanasMosaicoH = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnVentanasMosaicoV = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnVentanasCascada = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.tabConfiguracion = New ComponentFactory.Krypton.Ribbon.KryptonRibbonTab()
        Me.krgConfiguracionAcciones = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup()
        Me.krgtConfiguracionAcciones = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnConfiguracionAccionesGuardar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnConfiguracionAccionesCancelar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.tabProveedores = New ComponentFactory.Krypton.Ribbon.KryptonRibbonTab()
        Me.krgProveedoresAcciones = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup()
        Me.krgtProveedoresAccionesEspera = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnProveedoresAccionesNuevo = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnProveedoresAccionesModificar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnProveedoresAccionesEliminar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.krgtProveedoresAccionesEdicion = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnProveedoresAccionesGuardar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnProveedoresAccionesCancelar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.krtgProveedoresCerrar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnProveedoresAccionesCerrar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.tabLotesEntrada = New ComponentFactory.Krypton.Ribbon.KryptonRibbonTab()
        Me.krgLotesEntradaAcciones = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup()
        Me.krgtLotesEntradaAccionesEspera = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnLotesEntradaAccionesNuevo = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnLotesEntradaAccionesModificar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnLotesEntradaAccionesEliminar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.krgtLotesEntradaAccionesEdicion = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnLotesEntradaAccionesGuardar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnLotesEntradaAccionesCancelar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.krgtLotesEntradaMasAcciones = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnLotesEntradaAccionesImprimir = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnLotesEntradaAccionesImprimirListadoSalida = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.krgtLotesEntradaAccionesCerrar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnLotesEntradaAccionesCerrar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.tabEmpaquetado = New ComponentFactory.Krypton.Ribbon.KryptonRibbonTab()
        Me.krgEmpaquetadoAcciones = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup()
        Me.krgtEmpaquetadoAccionesEspera = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnEmpaquetadoAccionesNuevo = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnEmpaquetadoAccionesModificar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnEmpaquetadoAccionesEliminar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.krgtEmpaquetadoAccionesEdicion = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnEmpaquetadoAccionesGuardar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnEmpaquetadoAccionesCancelar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.krgtEmpaquetadoMasAcciones1 = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnEmpaquetadoAccionesLectorCodigos = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.krgtEmpaquetadoMasAcciones2 = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnEmpaquetadoAccionesImprimir = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnEmpaquetadoAccionesImprimirEtiquetas = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnEmpaquetadoAccionesImprimirMakro = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.krgtEmpaquetadoAccionesCerrar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnEmpaquetadoAccionesCerrar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.tabLotesSalida = New ComponentFactory.Krypton.Ribbon.KryptonRibbonTab()
        Me.krgLotesSalidaAcciones = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup()
        Me.krgtLotesSalidaAccionesEspera = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnLotesSalidaAccionesNuevo = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnLotesSalidaAccionesModificar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnLotesSalidaAccionesEliminar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.krgtLotesSalidaAccionesEdicion = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnLotesSalidaAccionesGuardar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnLotesSalidaAccionesCancelar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.krgtLotesSalidaMasAcciones = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnLotesSalidaAccionesImprimir = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnLotesSalidaAccionesImprimirEtiquetas = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.krgtLotesSalidaAccionesCerrar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnLotesSalidaAccionesCerrar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.tabClientes = New ComponentFactory.Krypton.Ribbon.KryptonRibbonTab()
        Me.krgClientesAcciones = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup()
        Me.krgtClientesAccionesEspera = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnClientesAccionesNuevo = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnClientesAccionesModificar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnClientesAccionesEliminar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.krgtClientesAccionesEdicion = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnClientesAccionesGuardar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnClientesAccionesCancelar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.krgtClientesAccionesCerrar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnClientesAccionesCerrar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.tabExpedidores = New ComponentFactory.Krypton.Ribbon.KryptonRibbonTab()
        Me.tabEtiquetas = New ComponentFactory.Krypton.Ribbon.KryptonRibbonTab()
        Me.krgEtiquetasAcciones = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup()
        Me.krgtEtiquetasAccionesEspera = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnEtiquetasAccionesNuevo = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnEtiquetasAccionesModificar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnEtiquetasAccionesEliminar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.krgtEtiquetasAccionesEdicion = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnEtiquetasAccionesGuardar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnEtiquetasAccionesCancelar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.krgtEtiquetasMasAcciones = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnEtiquetasMasAccionesImprimir = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.krgtEtiquetasAccionesCerrar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnEtiquetasAccionesCerrar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.tabInforme = New ComponentFactory.Krypton.Ribbon.KryptonRibbonTab()
        Me.krgReporteAcciones = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup()
        Me.krgtInformesAccionesCerrar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnInformesAccionesVerOcultarLogos = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnInformesAccionesCerrar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.krgtInformesAcciones2 = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnInformesAccionesImprimir = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnInformeAccionesExportar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.krgReportePaginacion = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup()
        Me.krgtInformesPaginacion1 = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnInformesPaginaPrimera = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnInformesPaginaAnterior = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnInformesPaginaSiguiente = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.krgtInformesPaginacion2 = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnInformesPaginaUltima = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.tabGeneracionEtiquetas = New ComponentFactory.Krypton.Ribbon.KryptonRibbonTab()
        Me.mnuGenerarEtiquetasAcciones = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup()
        Me.kgtGenerarEtiquetasAcciones1 = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnGenerarEtiquetasGenerarPesoActual = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnGenerarEtiquetasGenerarPesoManual = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.btnGenerarEtiquetasRemprimirUltimaEtiqueta = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.kgtGenerarEtiquetasAcciones2 = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnGenerarEtiquetasDescartarUltimaEtiqueta = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.chkGenerarEtiquetasImpresionAutomatica = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.chkGenerarEtiquetasImpresionAutomatica2 = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupCheckBox()
        Me.kgtGenerarEtiquetasAccionesCerrar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple()
        Me.btnGenerarEtiquetasCerrar = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.tabAyuda = New ComponentFactory.Krypton.Ribbon.KryptonRibbonTab()
        Me.KryptonRibbonGroupButton3 = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        Me.upActualizador = New Kjs.AppLife.Update.Controller.UpdateController(Me.components)
        Me.ssEstado = New System.Windows.Forms.StatusStrip()
        Me.mnuInfoActualizacion = New Kjs.AppLife.Update.Controller.StatusStripUpdateDisplay()
        Me.mnuInformesStock_Entradas = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton()
        krgExpedidoresAcciones = New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup()
        CType(Me.rbbPrincipal, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ssEstado.SuspendLayout()
        CType(Me.mnuInfoActualizacion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'krgExpedidoresAcciones
        '
        krgExpedidoresAcciones.DialogBoxLauncher = False
        krgExpedidoresAcciones.Image = Nothing
        krgExpedidoresAcciones.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupContainer() {Me.krgtExpedidoresAccionesEspera, Me.krgtExpedidoresAccionesEdicion, Me.krgtExpedidoresAccionesCerrar})
        krgExpedidoresAcciones.TextLine1 = "Acciones"
        '
        'krgtExpedidoresAccionesEspera
        '
        Me.krgtExpedidoresAccionesEspera.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnExpedidoresAccionesNuevo, Me.btnExpedidoresAccionesModificar, Me.btnExpedidoresAccionesEliminar})
        '
        'btnExpedidoresAccionesNuevo
        '
        Me.btnExpedidoresAccionesNuevo.ImageLarge = Global.Escritorio.My.Resources.Resources.Nuevo_32
        Me.btnExpedidoresAccionesNuevo.ImageSmall = Global.Escritorio.My.Resources.Resources.Nuevo_16
        Me.btnExpedidoresAccionesNuevo.KeyTip = "N"
        Me.btnExpedidoresAccionesNuevo.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.btnExpedidoresAccionesNuevo.TextLine1 = "Nuevo"
        '
        'btnExpedidoresAccionesModificar
        '
        Me.btnExpedidoresAccionesModificar.ImageLarge = Global.Escritorio.My.Resources.Resources.Modificar_32
        Me.btnExpedidoresAccionesModificar.ImageSmall = Global.Escritorio.My.Resources.Resources.Modificar_16
        Me.btnExpedidoresAccionesModificar.KeyTip = "M"
        Me.btnExpedidoresAccionesModificar.ShortcutKeys = System.Windows.Forms.Keys.F2
        Me.btnExpedidoresAccionesModificar.TextLine1 = "Modificar"
        '
        'btnExpedidoresAccionesEliminar
        '
        Me.btnExpedidoresAccionesEliminar.ImageLarge = Global.Escritorio.My.Resources.Resources.Eliminar_32
        Me.btnExpedidoresAccionesEliminar.ImageSmall = Global.Escritorio.My.Resources.Resources.Eliminar_16
        Me.btnExpedidoresAccionesEliminar.KeyTip = "E"
        Me.btnExpedidoresAccionesEliminar.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Delete), System.Windows.Forms.Keys)
        Me.btnExpedidoresAccionesEliminar.TextLine1 = "Eliminar"
        '
        'krgtExpedidoresAccionesEdicion
        '
        Me.krgtExpedidoresAccionesEdicion.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnExpedidoresAccionesGuardar, Me.btnExpedidoresAccionesCancelar})
        '
        'btnExpedidoresAccionesGuardar
        '
        Me.btnExpedidoresAccionesGuardar.ImageLarge = Global.Escritorio.My.Resources.Resources.Guardar_32
        Me.btnExpedidoresAccionesGuardar.ImageSmall = Global.Escritorio.My.Resources.Resources.Guardar_16
        Me.btnExpedidoresAccionesGuardar.KeyTip = "G"
        Me.btnExpedidoresAccionesGuardar.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.G), System.Windows.Forms.Keys)
        Me.btnExpedidoresAccionesGuardar.TextLine1 = "Guardar"
        '
        'btnExpedidoresAccionesCancelar
        '
        Me.btnExpedidoresAccionesCancelar.ImageLarge = Global.Escritorio.My.Resources.Resources.Cancelar_32
        Me.btnExpedidoresAccionesCancelar.ImageSmall = Global.Escritorio.My.Resources.Resources.Cancelar_16
        Me.btnExpedidoresAccionesCancelar.KeyTip = "C"
        Me.btnExpedidoresAccionesCancelar.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.W), System.Windows.Forms.Keys)
        Me.btnExpedidoresAccionesCancelar.TextLine1 = "Cancelar"
        '
        'krgtExpedidoresAccionesCerrar
        '
        Me.krgtExpedidoresAccionesCerrar.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnExpedidoresAccionesCerrar})
        '
        'btnExpedidoresAccionesCerrar
        '
        Me.btnExpedidoresAccionesCerrar.ImageLarge = Global.Escritorio.My.Resources.Resources.Cerrar_32
        Me.btnExpedidoresAccionesCerrar.ImageSmall = Global.Escritorio.My.Resources.Resources.Cerrar_16
        Me.btnExpedidoresAccionesCerrar.KeyTip = "Q"
        Me.btnExpedidoresAccionesCerrar.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Q), System.Windows.Forms.Keys)
        Me.btnExpedidoresAccionesCerrar.TextLine1 = "Cerrar"
        '
        'tmrReloj
        '
        Me.tmrReloj.Interval = 2500
        '
        'mngManager
        '
        Me.mngManager.GlobalPalette = Me.Paleta
        Me.mngManager.GlobalPaletteMode = ComponentFactory.Krypton.Toolkit.PaletteModeManager.Custom
        '
        'Paleta
        '
        Me.Paleta.LabelStyles.LabelCustom1.StateCommon.Padding = New System.Windows.Forms.Padding(20, -1, -1, -1)
        Me.Paleta.LabelStyles.LabelCustom1.StateCommon.ShortText.Color1 = System.Drawing.Color.White
        Me.Paleta.LabelStyles.LabelCustom1.StateCommon.ShortText.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold)
        Me.Paleta.LabelStyles.LabelCustom2.StateCommon.ShortText.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Paleta.PanelStyles.PanelCustom1.StateCommon.Color1 = System.Drawing.Color.FromArgb(CType(CType(117, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(239, Byte), Integer))
        Me.Paleta.PanelStyles.PanelCustom1.StateCommon.Color2 = System.Drawing.Color.FromArgb(CType(CType(100, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(214, Byte), Integer))
        Me.Paleta.PanelStyles.PanelCustom1.StateCommon.ColorStyle = ComponentFactory.Krypton.Toolkit.PaletteColorStyle.Linear
        Me.Paleta.PanelStyles.PanelCustom1.StateCommon.GraphicsHint = ComponentFactory.Krypton.Toolkit.PaletteGraphicsHint.AntiAlias
        Me.Paleta.PanelStyles.PanelCustom1.StateCommon.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.CenterLeft
        '
        'ntfIcono
        '
        Me.ntfIcono.Icon = CType(resources.GetObject("ntfIcono.Icon"), System.Drawing.Icon)
        Me.ntfIcono.Visible = True
        '
        'mnuFIxRibbon
        '
        Me.mnuFIxRibbon.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.mnuFIxRibbon.Location = New System.Drawing.Point(0, 0)
        Me.mnuFIxRibbon.Name = "mnuFIxRibbon"
        Me.mnuFIxRibbon.Size = New System.Drawing.Size(1085, 24)
        Me.mnuFIxRibbon.TabIndex = 9
        Me.mnuFIxRibbon.Text = "MenuStrip1"
        Me.mnuFIxRibbon.Visible = False
        '
        'mnuOrbCabeceraConfiguracion
        '
        Me.mnuOrbCabeceraConfiguracion.ExtraText = ""
        Me.mnuOrbCabeceraConfiguracion.Text = "Configuración"
        '
        'mnuOrbConfiguracionPrograma
        '
        Me.mnuOrbConfiguracionPrograma.Image = Global.Escritorio.My.Resources.Resources.Configurar_16
        Me.mnuOrbConfiguracionPrograma.Text = "Configuración programa"
        '
        'mnuOrbUsuarios
        '
        Me.mnuOrbUsuarios.Image = Global.Escritorio.My.Resources.Resources.Usuarios_16
        Me.mnuOrbUsuarios.Text = "Usuarios"
        '
        'mnuOrbDatosMaestros
        '
        Me.mnuOrbDatosMaestros.Image = Global.Escritorio.My.Resources.Resources.DatosMaestros_16
        Me.mnuOrbDatosMaestros.Text = "Datos Maestros"
        '
        'mnuOrbCabeceraSalirAplicacion
        '
        Me.mnuOrbCabeceraSalirAplicacion.ExtraText = ""
        Me.mnuOrbCabeceraSalirAplicacion.Image = Global.Escritorio.My.Resources.Resources.Cerrar_16
        Me.mnuOrbCabeceraSalirAplicacion.Text = "Salir Aplicación"
        '
        'mnuSalir
        '
        Me.mnuSalir.Text = "Salir Aplicación"
        '
        'mnuConfiguracionPrograma
        '
        Me.mnuConfiguracionPrograma.ButtonType = ComponentFactory.Krypton.Ribbon.GroupButtonType.DropDown
        Me.mnuConfiguracionPrograma.ImageLarge = Global.Escritorio.My.Resources.Resources.Configurar_32
        Me.mnuConfiguracionPrograma.ImageSmall = Global.Escritorio.My.Resources.Resources.Configurar_16
        Me.mnuConfiguracionPrograma.KeyTip = "P"
        Me.mnuConfiguracionPrograma.KryptonContextMenu = Me.rtnConfiguracionPrograma
        Me.mnuConfiguracionPrograma.TextLine1 = "Programa"
        '
        'rtnConfiguracionPrograma
        '
        Me.rtnConfiguracionPrograma.Items.AddRange(New ComponentFactory.Krypton.Toolkit.KryptonContextMenuItemBase() {Me.rtnConfiguracionProgramaCabecera})
        '
        'rtnConfiguracionProgramaCabecera
        '
        Me.rtnConfiguracionProgramaCabecera.Items.AddRange(New ComponentFactory.Krypton.Toolkit.KryptonContextMenuItemBase() {Me.rtnConfiguracionProgramaBaseDatos, Me.rtnConfiguracionProgramaParametrosGlobales, Me.rtnConfiguracionProgramaCopiaSeguridad, Me.rtnConfiguracionProgramaDatosMaestros})
        '
        'rtnConfiguracionProgramaBaseDatos
        '
        Me.rtnConfiguracionProgramaBaseDatos.Image = Global.Escritorio.My.Resources.Resources.ConfiguracionBaseDatos_16
        Me.rtnConfiguracionProgramaBaseDatos.Text = "Base de Datos"
        '
        'rtnConfiguracionProgramaParametrosGlobales
        '
        Me.rtnConfiguracionProgramaParametrosGlobales.Image = Global.Escritorio.My.Resources.Resources.Configurar_16
        Me.rtnConfiguracionProgramaParametrosGlobales.Text = "Parámetros Globales"
        '
        'rtnConfiguracionProgramaCopiaSeguridad
        '
        Me.rtnConfiguracionProgramaCopiaSeguridad.Image = Global.Escritorio.My.Resources.Resources.CopiaSeguridad_16
        Me.rtnConfiguracionProgramaCopiaSeguridad.Text = "Copia Seguridad"
        '
        'rtnConfiguracionProgramaDatosMaestros
        '
        Me.rtnConfiguracionProgramaDatosMaestros.Image = Global.Escritorio.My.Resources.Resources.DatosMaestros_16
        Me.rtnConfiguracionProgramaDatosMaestros.Text = "Datos Maestros"
        '
        'mnuConfiguracionUsuarios
        '
        Me.mnuConfiguracionUsuarios.ImageLarge = Global.Escritorio.My.Resources.Resources.Usuarios_32
        Me.mnuConfiguracionUsuarios.ImageSmall = Global.Escritorio.My.Resources.Resources.Usuarios_16
        Me.mnuConfiguracionUsuarios.KeyTip = "U"
        Me.mnuConfiguracionUsuarios.TextLine1 = "Usuarios"
        '
        'krtGeneralConfiguracion1
        '
        Me.krtGeneralConfiguracion1.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.mnuConfiguracionUsuarios, Me.mnuConfiguracionPrograma, Me.btnSincronizarTrazamare})
        '
        'btnSincronizarTrazamare
        '
        Me.btnSincronizarTrazamare.ButtonType = ComponentFactory.Krypton.Ribbon.GroupButtonType.Split
        Me.btnSincronizarTrazamare.ImageLarge = Global.Escritorio.My.Resources.Resources.Sincronizar_32
        Me.btnSincronizarTrazamare.ImageSmall = Global.Escritorio.My.Resources.Resources.Sincronizar_16
        Me.btnSincronizarTrazamare.KeyTip = "S"
        Me.btnSincronizarTrazamare.KryptonContextMenu = Me.rtnSincronizacionEtiquetas
        Me.btnSincronizarTrazamare.TextLine1 = "Sincronizar"
        Me.btnSincronizarTrazamare.TextLine2 = "Etiquetas"
        '
        'rtnSincronizacionEtiquetas
        '
        Me.rtnSincronizacionEtiquetas.Items.AddRange(New ComponentFactory.Krypton.Toolkit.KryptonContextMenuItemBase() {Me.rtnSincronizacionEtiquetasElementos})
        '
        'rtnSincronizacionEtiquetasElementos
        '
        Me.rtnSincronizacionEtiquetasElementos.Items.AddRange(New ComponentFactory.Krypton.Toolkit.KryptonContextMenuItemBase() {Me.rtnSincronizacionEtiquetasVerLog})
        Me.rtnSincronizacionEtiquetasElementos.Tag = ""
        '
        'rtnSincronizacionEtiquetasVerLog
        '
        Me.rtnSincronizacionEtiquetasVerLog.Tag = ""
        Me.rtnSincronizacionEtiquetasVerLog.Text = "Ver Log"
        '
        'mnuConfiguracionAcercaDe
        '
        Me.mnuConfiguracionAcercaDe.ImageLarge = Global.Escritorio.My.Resources.Resources.AcercaDe_32
        Me.mnuConfiguracionAcercaDe.ImageSmall = Global.Escritorio.My.Resources.Resources.AcercaDe_16
        Me.mnuConfiguracionAcercaDe.KeyTip = "A"
        Me.mnuConfiguracionAcercaDe.TextLine1 = "Acerca"
        Me.mnuConfiguracionAcercaDe.TextLine2 = "de..."
        Me.mnuConfiguracionAcercaDe.ToolTipStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.Custom1
        '
        'mnuAyudaAyuda2
        '
        Me.mnuAyudaAyuda2.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.mnuConfiguracionComprobarActualizaciones, Me.mnuConfiguracionAcercaDe})
        '
        'mnuConfiguracionComprobarActualizaciones
        '
        Me.mnuConfiguracionComprobarActualizaciones.ImageLarge = Global.Escritorio.My.Resources.Resources.Actualizar_32
        Me.mnuConfiguracionComprobarActualizaciones.ImageSmall = Global.Escritorio.My.Resources.Resources.Actualizar_16
        Me.mnuConfiguracionComprobarActualizaciones.KeyTip = "A"
        Me.mnuConfiguracionComprobarActualizaciones.TextLine1 = "Verificar"
        Me.mnuConfiguracionComprobarActualizaciones.TextLine2 = "Actualizaciones"
        '
        'krgGeneralConfiguracion
        '
        Me.krgGeneralConfiguracion.DialogBoxLauncher = False
        Me.krgGeneralConfiguracion.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupContainer() {Me.krtGeneralConfiguracion1})
        Me.krgGeneralConfiguracion.TextLine1 = "Configuración"
        '
        'tabGeneral
        '
        Me.tabGeneral.Groups.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup() {Me.krgGeneralDatosAdministrativos, Me.krgGeneralLotes, Me.krgGeneralConfiguracion})
        Me.tabGeneral.KeyTip = "G"
        Me.tabGeneral.Text = "General"
        '
        'krgGeneralDatosAdministrativos
        '
        Me.krgGeneralDatosAdministrativos.AllowCollapsed = False
        Me.krgGeneralDatosAdministrativos.DialogBoxLauncher = False
        Me.krgGeneralDatosAdministrativos.Image = Nothing
        Me.krgGeneralDatosAdministrativos.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupContainer() {Me.krtGeneralAdministrativos})
        Me.krgGeneralDatosAdministrativos.TextLine1 = "Datos Administrativos"
        '
        'krtGeneralAdministrativos
        '
        Me.krtGeneralAdministrativos.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.mnuDatosAdministrativosExpedidores, Me.mnuDatosAdministrativosProveedores, Me.mnuDatosAdministrativosClientes})
        '
        'mnuDatosAdministrativosExpedidores
        '
        Me.mnuDatosAdministrativosExpedidores.ImageLarge = Global.Escritorio.My.Resources.Resources.Expedidor_32
        Me.mnuDatosAdministrativosExpedidores.ImageSmall = Global.Escritorio.My.Resources.Resources.Expedidor_16
        Me.mnuDatosAdministrativosExpedidores.TextLine1 = "Expedidores"
        '
        'mnuDatosAdministrativosProveedores
        '
        Me.mnuDatosAdministrativosProveedores.ImageLarge = Global.Escritorio.My.Resources.Resources.Proveedor_32
        Me.mnuDatosAdministrativosProveedores.ImageSmall = Global.Escritorio.My.Resources.Resources.Proveedor_16
        Me.mnuDatosAdministrativosProveedores.KeyTip = "P"
        Me.mnuDatosAdministrativosProveedores.TextLine1 = "Proveedores"
        '
        'mnuDatosAdministrativosClientes
        '
        Me.mnuDatosAdministrativosClientes.ImageLarge = Global.Escritorio.My.Resources.Resources.Cliente_32
        Me.mnuDatosAdministrativosClientes.ImageSmall = Global.Escritorio.My.Resources.Resources.Cliente_16
        Me.mnuDatosAdministrativosClientes.KeyTip = "C"
        Me.mnuDatosAdministrativosClientes.TextLine1 = "Clientes"
        '
        'krgGeneralLotes
        '
        Me.krgGeneralLotes.AllowCollapsed = False
        Me.krgGeneralLotes.DialogBoxLauncher = False
        Me.krgGeneralLotes.Image = Nothing
        Me.krgGeneralLotes.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupContainer() {Me.krtGeneralLotes, Me.KryptonRibbonGroupTriple2})
        Me.krgGeneralLotes.TextLine1 = "Lotes"
        '
        'krtGeneralLotes
        '
        Me.krtGeneralLotes.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.mnuDatosAdministrativosLotesEntrada, Me.mnuDatosAdministrativosEtiquetas})
        '
        'mnuDatosAdministrativosLotesEntrada
        '
        Me.mnuDatosAdministrativosLotesEntrada.ButtonType = ComponentFactory.Krypton.Ribbon.GroupButtonType.Split
        Me.mnuDatosAdministrativosLotesEntrada.ImageLarge = Global.Escritorio.My.Resources.Resources.Entrada_32
        Me.mnuDatosAdministrativosLotesEntrada.ImageSmall = Global.Escritorio.My.Resources.Resources.Entrada_16
        Me.mnuDatosAdministrativosLotesEntrada.KeyTip = "E"
        Me.mnuDatosAdministrativosLotesEntrada.KryptonContextMenu = Me.mnuAlbaranesEntrada
        Me.mnuDatosAdministrativosLotesEntrada.TextLine1 = "Albaranes"
        Me.mnuDatosAdministrativosLotesEntrada.TextLine2 = "Entrada"
        '
        'mnuAlbaranesEntrada
        '
        Me.mnuAlbaranesEntrada.Items.AddRange(New ComponentFactory.Krypton.Toolkit.KryptonContextMenuItemBase() {Me.mnuAlbaranesEntradaElementos})
        '
        'mnuAlbaranesEntradaElementos
        '
        Me.mnuAlbaranesEntradaElementos.Items.AddRange(New ComponentFactory.Krypton.Toolkit.KryptonContextMenuItemBase() {Me.mnuAlbaranesEntradaImportar})
        Me.mnuAlbaranesEntradaElementos.Tag = ""
        '
        'mnuAlbaranesEntradaImportar
        '
        Me.mnuAlbaranesEntradaImportar.Image = Global.Escritorio.My.Resources.Resources.Importar_32
        Me.mnuAlbaranesEntradaImportar.Tag = ""
        Me.mnuAlbaranesEntradaImportar.Text = "Importar"
        '
        'mnuDatosAdministrativosEtiquetas
        '
        Me.mnuDatosAdministrativosEtiquetas.ButtonType = ComponentFactory.Krypton.Ribbon.GroupButtonType.Split
        Me.mnuDatosAdministrativosEtiquetas.ImageLarge = Global.Escritorio.My.Resources.Resources.Etiqueta_32
        Me.mnuDatosAdministrativosEtiquetas.ImageSmall = Global.Escritorio.My.Resources.Resources.Etiqueta_16
        Me.mnuDatosAdministrativosEtiquetas.KeyTip = "E"
        Me.mnuDatosAdministrativosEtiquetas.KryptonContextMenu = Me.rtnEtiquetas
        Me.mnuDatosAdministrativosEtiquetas.TextLine1 = "Etiquetas"
        '
        'rtnEtiquetas
        '
        Me.rtnEtiquetas.Items.AddRange(New ComponentFactory.Krypton.Toolkit.KryptonContextMenuItemBase() {Me.rtnEtiquetasElementos})
        '
        'rtnEtiquetasElementos
        '
        Me.rtnEtiquetasElementos.Items.AddRange(New ComponentFactory.Krypton.Toolkit.KryptonContextMenuItemBase() {Me.rtnEtiquetasGeneracionAutomatica})
        Me.rtnEtiquetasElementos.Tag = ""
        '
        'rtnEtiquetasGeneracionAutomatica
        '
        Me.rtnEtiquetasGeneracionAutomatica.Tag = ""
        Me.rtnEtiquetasGeneracionAutomatica.Text = "Generación Automática"
        '
        'KryptonRibbonGroupTriple2
        '
        Me.KryptonRibbonGroupTriple2.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.mnuDatosAdministrativosEmpaquetado, Me.mnuDatosAdministrativosLotesSalida})
        '
        'mnuDatosAdministrativosEmpaquetado
        '
        Me.mnuDatosAdministrativosEmpaquetado.ImageLarge = Global.Escritorio.My.Resources.Resources.Empaquetado_32
        Me.mnuDatosAdministrativosEmpaquetado.ImageSmall = Global.Escritorio.My.Resources.Resources.Empaquetado_16
        Me.mnuDatosAdministrativosEmpaquetado.KeyTip = "E"
        Me.mnuDatosAdministrativosEmpaquetado.TextLine1 = "Empaquetado"
        '
        'mnuDatosAdministrativosLotesSalida
        '
        Me.mnuDatosAdministrativosLotesSalida.ImageLarge = Global.Escritorio.My.Resources.Resources.Salida_32
        Me.mnuDatosAdministrativosLotesSalida.ImageSmall = Global.Escritorio.My.Resources.Resources.Salida_16
        Me.mnuDatosAdministrativosLotesSalida.KeyTip = "S"
        Me.mnuDatosAdministrativosLotesSalida.TextLine1 = "Albaranes"
        Me.mnuDatosAdministrativosLotesSalida.TextLine2 = "de Salida"
        '
        'mnuAyudaAyuda
        '
        Me.mnuAyudaAyuda.AllowCollapsed = False
        Me.mnuAyudaAyuda.DialogBoxLauncher = False
        Me.mnuAyudaAyuda.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupContainer() {Me.KryptonRibbonGroupTriple1, Me.mnuAyudaAyuda2})
        Me.mnuAyudaAyuda.TextLine1 = "Ayuda"
        '
        'KryptonRibbonGroupTriple1
        '
        Me.KryptonRibbonGroupTriple1.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.mnuAyudaWebQuadralia, Me.mnuAyudaAyudaSoporteRemoto})
        '
        'mnuAyudaWebQuadralia
        '
        Me.mnuAyudaWebQuadralia.ImageLarge = Global.Escritorio.My.Resources.Resources.web_32
        Me.mnuAyudaWebQuadralia.ImageSmall = Global.Escritorio.My.Resources.Resources.web_16
        Me.mnuAyudaWebQuadralia.KeyTip = "Q"
        Me.mnuAyudaWebQuadralia.TextLine1 = "Web"
        Me.mnuAyudaWebQuadralia.TextLine2 = "Quadralia"
        '
        'mnuAyudaAyudaSoporteRemoto
        '
        Me.mnuAyudaAyudaSoporteRemoto.ImageLarge = Global.Escritorio.My.Resources.Resources.web_32
        Me.mnuAyudaAyudaSoporteRemoto.ImageSmall = Global.Escritorio.My.Resources.Resources.web_16
        Me.mnuAyudaAyudaSoporteRemoto.KeyTip = "S"
        Me.mnuAyudaAyudaSoporteRemoto.TextLine1 = "Soporte"
        Me.mnuAyudaAyudaSoporteRemoto.TextLine2 = "Remoto"
        '
        'tabInformes
        '
        Me.tabInformes.Groups.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup() {Me.krgInformesStock, Me.krgInformesVentas})
        Me.tabInformes.KeyTip = "I"
        Me.tabInformes.Text = "Listados"
        '
        'krgInformesStock
        '
        Me.krgInformesStock.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupContainer() {Me.krtInformesStock1})
        Me.krgInformesStock.TextLine1 = "Stock"
        '
        'krtInformesStock1
        '
        Me.krtInformesStock1.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.mnuInformesStock_Stock, Me.mnuInformesStock_Entradas})
        '
        'mnuInformesStock_Stock
        '
        Me.mnuInformesStock_Stock.ImageLarge = Global.Escritorio.My.Resources.Resources.Empaquetado_32
        Me.mnuInformesStock_Stock.ImageSmall = Global.Escritorio.My.Resources.Resources.Empaquetado_16
        Me.mnuInformesStock_Stock.TextLine1 = "Informe de"
        Me.mnuInformesStock_Stock.TextLine2 = "Stock"
        '
        'krgInformesVentas
        '
        Me.krgInformesVentas.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupContainer() {Me.krgtInformesVentas1})
        Me.krgInformesVentas.TextLine1 = "Ventas"
        '
        'krgtInformesVentas1
        '
        Me.krgtInformesVentas1.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.mnuInformesVentas_Ventas})
        '
        'mnuInformesVentas_Ventas
        '
        Me.mnuInformesVentas_Ventas.ImageLarge = Global.Escritorio.My.Resources.Resources.Salida_32
        Me.mnuInformesVentas_Ventas.ImageSmall = Global.Escritorio.My.Resources.Resources.Salida_16
        Me.mnuInformesVentas_Ventas.TextLine1 = "Informe de"
        Me.mnuInformesVentas_Ventas.TextLine2 = "Ventas"
        '
        'btnUsuariosAccionesNuevo
        '
        Me.btnUsuariosAccionesNuevo.ImageLarge = Global.Escritorio.My.Resources.Resources.Nuevo_32
        Me.btnUsuariosAccionesNuevo.ImageSmall = Global.Escritorio.My.Resources.Resources.Nuevo_16
        Me.btnUsuariosAccionesNuevo.KeyTip = "N"
        Me.btnUsuariosAccionesNuevo.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.btnUsuariosAccionesNuevo.TextLine1 = "Nuevo"
        '
        'btnUsuariosAccionesModificar
        '
        Me.btnUsuariosAccionesModificar.ImageLarge = Global.Escritorio.My.Resources.Resources.Modificar_32
        Me.btnUsuariosAccionesModificar.ImageSmall = Global.Escritorio.My.Resources.Resources.Modificar_16
        Me.btnUsuariosAccionesModificar.KeyTip = "M"
        Me.btnUsuariosAccionesModificar.ShortcutKeys = System.Windows.Forms.Keys.F2
        Me.btnUsuariosAccionesModificar.TextLine1 = "Modificar"
        '
        'btnUsuariosAccionesEliminar
        '
        Me.btnUsuariosAccionesEliminar.ImageLarge = Global.Escritorio.My.Resources.Resources.Eliminar_32
        Me.btnUsuariosAccionesEliminar.ImageSmall = Global.Escritorio.My.Resources.Resources.Eliminar_16
        Me.btnUsuariosAccionesEliminar.KeyTip = "E"
        Me.btnUsuariosAccionesEliminar.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Delete), System.Windows.Forms.Keys)
        Me.btnUsuariosAccionesEliminar.TextLine1 = "Eliminar"
        '
        'krgtUsuariosAccionesEspera
        '
        Me.krgtUsuariosAccionesEspera.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnUsuariosAccionesNuevo, Me.btnUsuariosAccionesModificar, Me.btnUsuariosAccionesEliminar})
        '
        'btnUsuariosAccionesGuardar
        '
        Me.btnUsuariosAccionesGuardar.ImageLarge = Global.Escritorio.My.Resources.Resources.Guardar_32
        Me.btnUsuariosAccionesGuardar.ImageSmall = Global.Escritorio.My.Resources.Resources.Guardar_16
        Me.btnUsuariosAccionesGuardar.KeyTip = "G"
        Me.btnUsuariosAccionesGuardar.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.G), System.Windows.Forms.Keys)
        Me.btnUsuariosAccionesGuardar.TextLine1 = "Guardar"
        '
        'btnUsuariosAccionesCancelar
        '
        Me.btnUsuariosAccionesCancelar.ImageLarge = Global.Escritorio.My.Resources.Resources.Cancelar_32
        Me.btnUsuariosAccionesCancelar.ImageSmall = Global.Escritorio.My.Resources.Resources.Cancelar_16
        Me.btnUsuariosAccionesCancelar.KeyTip = "C"
        Me.btnUsuariosAccionesCancelar.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.W), System.Windows.Forms.Keys)
        Me.btnUsuariosAccionesCancelar.TextLine1 = "Cancelar"
        '
        'krgtUsuariosAccionesEdicion
        '
        Me.krgtUsuariosAccionesEdicion.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnUsuariosAccionesGuardar, Me.btnUsuariosAccionesCancelar})
        '
        'btnUsuariosSalir
        '
        Me.btnUsuariosSalir.ImageLarge = Global.Escritorio.My.Resources.Resources.Cerrar_32
        Me.btnUsuariosSalir.ImageSmall = Global.Escritorio.My.Resources.Resources.Cerrar_16
        Me.btnUsuariosSalir.KeyTip = "Q"
        Me.btnUsuariosSalir.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Q), System.Windows.Forms.Keys)
        Me.btnUsuariosSalir.TextLine1 = "Cerrar"
        '
        'krgtUsuariosSalir
        '
        Me.krgtUsuariosSalir.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnUsuariosSalir})
        '
        'krgUsuariosAcciones
        '
        Me.krgUsuariosAcciones.DialogBoxLauncher = False
        Me.krgUsuariosAcciones.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupContainer() {Me.krgtUsuariosAccionesEspera, Me.krgtUsuariosAccionesEdicion, Me.krgtUsuariosSalir})
        Me.krgUsuariosAcciones.TextLine1 = "Acciones"
        '
        'tabUsuarios
        '
        Me.tabUsuarios.Groups.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup() {Me.krgUsuariosAcciones})
        Me.tabUsuarios.KeyTip = "U"
        Me.tabUsuarios.Text = "Usuarios"
        '
        'mnuRapidoCambiarContrasenha
        '
        Me.mnuRapidoCambiarContrasenha.Image = Global.Escritorio.My.Resources.Resources.Contrasenha_16
        '
        'mnuRapidoGuardar
        '
        Me.mnuRapidoGuardar.Enabled = False
        Me.mnuRapidoGuardar.Image = Global.Escritorio.My.Resources.Resources.Guardar_16
        Me.mnuRapidoGuardar.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.mnuRapidoGuardar.Text = "Guardar"
        '
        'rbbPrincipal
        '
        Me.rbbPrincipal.InDesignHelperMode = True
        Me.rbbPrincipal.Name = "rbbPrincipal"
        Me.rbbPrincipal.QATButtons.AddRange(New System.ComponentModel.Component() {Me.mnuRapidoCambiarContrasenha, Me.mnuRapidoGuardar})
        Me.rbbPrincipal.RibbonAppButton.AppButtonImage = Nothing
        Me.rbbPrincipal.RibbonAppButton.AppButtonMenuItems.AddRange(New ComponentFactory.Krypton.Toolkit.KryptonContextMenuItemBase() {Me.mnuOrbDatosAdministrativos, Me.mnuOrbExpedidores, Me.mnuOrbProveedores, Me.mnuOrbClientes, Me.mnuOrbLotes, Me.mnuOrbLotesEntrada, Me.mnuOrbEtiquetas, Me.mnuOrbEmpaquetado, Me.mnuOrbLotesSalida, Me.mnuOrbEtiquetasGeneracionAutomatica, Me.mnuOrbCabeceraConfiguracion, Me.mnuOrbUsuarios, Me.mnuOrbConfiguracionPrograma, Me.mnuOrbDatosMaestros, Me.mnuOrbCopiaSeguridad, Me.mnuOrbBaseDatos, Me.mnuOrbCabeceraSalirAplicacion, Me.mnuSalir})
        Me.rbbPrincipal.RibbonAppButton.AppButtonShowRecentDocs = False
        Me.rbbPrincipal.RibbonAppButton.AppButtonText = "Menú"
        Me.rbbPrincipal.RibbonStrings.CustomizeQuickAccessToolbar = "Configurar barra de accesos rápidos"
        Me.rbbPrincipal.RibbonStrings.Minimize = "Mi&nimizar el Ribbon"
        Me.rbbPrincipal.RibbonTabs.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonTab() {Me.tabGeneral, Me.tabInformes, Me.tabVentanas, Me.tabUsuarios, Me.tabConfiguracion, Me.tabProveedores, Me.tabLotesEntrada, Me.tabEmpaquetado, Me.tabLotesSalida, Me.tabClientes, Me.tabExpedidores, Me.tabEtiquetas, Me.tabInforme, Me.tabGeneracionEtiquetas, Me.tabAyuda})
        Me.rbbPrincipal.SelectedTab = Me.tabInformes
        Me.rbbPrincipal.Size = New System.Drawing.Size(1085, 115)
        Me.rbbPrincipal.TabIndex = 10
        '
        'mnuOrbDatosAdministrativos
        '
        Me.mnuOrbDatosAdministrativos.ExtraText = ""
        Me.mnuOrbDatosAdministrativos.Text = "Datos Administrativos"
        '
        'mnuOrbExpedidores
        '
        Me.mnuOrbExpedidores.Image = Global.Escritorio.My.Resources.Resources.Expedidor_16
        Me.mnuOrbExpedidores.Text = "Expedidores"
        '
        'mnuOrbProveedores
        '
        Me.mnuOrbProveedores.Image = Global.Escritorio.My.Resources.Resources.Proveedor_16
        Me.mnuOrbProveedores.Text = "Proveedores"
        '
        'mnuOrbClientes
        '
        Me.mnuOrbClientes.Image = Global.Escritorio.My.Resources.Resources.Cliente_16
        Me.mnuOrbClientes.Text = "Clientes"
        '
        'mnuOrbLotes
        '
        Me.mnuOrbLotes.ExtraText = ""
        Me.mnuOrbLotes.Text = "Lotes"
        '
        'mnuOrbLotesEntrada
        '
        Me.mnuOrbLotesEntrada.Image = Global.Escritorio.My.Resources.Resources.Entrada_16
        Me.mnuOrbLotesEntrada.Text = "Albaranes Entrada"
        '
        'mnuOrbEtiquetas
        '
        Me.mnuOrbEtiquetas.Image = Global.Escritorio.My.Resources.Resources.Etiqueta_16
        Me.mnuOrbEtiquetas.Text = "Etiquetas"
        '
        'mnuOrbEmpaquetado
        '
        Me.mnuOrbEmpaquetado.Image = Global.Escritorio.My.Resources.Resources.Empaquetado_16
        Me.mnuOrbEmpaquetado.Text = "Empaquetado"
        '
        'mnuOrbLotesSalida
        '
        Me.mnuOrbLotesSalida.Image = Global.Escritorio.My.Resources.Resources.Salida_16
        Me.mnuOrbLotesSalida.Text = "Envios Salida"
        '
        'mnuOrbEtiquetasGeneracionAutomatica
        '
        Me.mnuOrbEtiquetasGeneracionAutomatica.Image = Global.Escritorio.My.Resources.Resources.Etiqueta_16
        Me.mnuOrbEtiquetasGeneracionAutomatica.Text = "Generación Automática Etiquetas"
        '
        'mnuOrbCopiaSeguridad
        '
        Me.mnuOrbCopiaSeguridad.Image = Global.Escritorio.My.Resources.Resources.CopiaSeguridad_16
        Me.mnuOrbCopiaSeguridad.Text = "Copia Seguridad"
        '
        'mnuOrbBaseDatos
        '
        Me.mnuOrbBaseDatos.Image = Global.Escritorio.My.Resources.Resources.ConfiguracionBaseDatos_16
        Me.mnuOrbBaseDatos.Text = "Base de datos"
        '
        'tabVentanas
        '
        Me.tabVentanas.Groups.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup() {Me.krgVentanas})
        Me.tabVentanas.Text = "Ventanas"
        '
        'krgVentanas
        '
        Me.krgVentanas.AllowCollapsed = False
        Me.krgVentanas.DialogBoxLauncher = False
        Me.krgVentanas.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupContainer() {Me.krgtVentanas1, Me.krgtVentanas2})
        Me.krgVentanas.TextLine1 = "Ventanas"
        '
        'krgtVentanas1
        '
        Me.krgtVentanas1.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnVentanasMinimizar, Me.btnVentanasRestaurar, Me.btnVentanasCerrar})
        '
        'btnVentanasMinimizar
        '
        Me.btnVentanasMinimizar.ImageLarge = Global.Escritorio.My.Resources.Resources.Minimizar_32
        Me.btnVentanasMinimizar.ImageSmall = Global.Escritorio.My.Resources.Resources.Minimizar_16
        Me.btnVentanasMinimizar.TextLine1 = "Minimizar"
        Me.btnVentanasMinimizar.TextLine2 = "Todas"
        '
        'btnVentanasRestaurar
        '
        Me.btnVentanasRestaurar.ImageLarge = Global.Escritorio.My.Resources.Resources.Restaurar_32
        Me.btnVentanasRestaurar.ImageSmall = Global.Escritorio.My.Resources.Resources.Restaurar_16
        Me.btnVentanasRestaurar.TextLine1 = "Restaurar"
        Me.btnVentanasRestaurar.TextLine2 = "Todas"
        '
        'btnVentanasCerrar
        '
        Me.btnVentanasCerrar.ImageLarge = Global.Escritorio.My.Resources.Resources.CerrarTodos_32
        Me.btnVentanasCerrar.ImageSmall = Global.Escritorio.My.Resources.Resources.CerrarTodos_16
        Me.btnVentanasCerrar.TextLine1 = "Cerrar"
        Me.btnVentanasCerrar.TextLine2 = "Todas"
        '
        'krgtVentanas2
        '
        Me.krgtVentanas2.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnVentanasMosaicoH, Me.btnVentanasMosaicoV, Me.btnVentanasCascada})
        '
        'btnVentanasMosaicoH
        '
        Me.btnVentanasMosaicoH.ImageLarge = Global.Escritorio.My.Resources.Resources.Horizontal_32
        Me.btnVentanasMosaicoH.ImageSmall = Global.Escritorio.My.Resources.Resources.Horizontal_16
        Me.btnVentanasMosaicoH.TextLine1 = "Mosaico"
        Me.btnVentanasMosaicoH.TextLine2 = "Horizontal"
        '
        'btnVentanasMosaicoV
        '
        Me.btnVentanasMosaicoV.ImageLarge = Global.Escritorio.My.Resources.Resources.Vertical_32
        Me.btnVentanasMosaicoV.ImageSmall = Global.Escritorio.My.Resources.Resources.Vertical_16
        Me.btnVentanasMosaicoV.TextLine1 = "Mosaico"
        Me.btnVentanasMosaicoV.TextLine2 = "Vertical"
        '
        'btnVentanasCascada
        '
        Me.btnVentanasCascada.ImageLarge = Global.Escritorio.My.Resources.Resources.Cascada_32
        Me.btnVentanasCascada.ImageSmall = Global.Escritorio.My.Resources.Resources.Cascada_16
        Me.btnVentanasCascada.TextLine1 = "Cascada"
        '
        'tabConfiguracion
        '
        Me.tabConfiguracion.Groups.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup() {Me.krgConfiguracionAcciones})
        Me.tabConfiguracion.Text = "Configuración"
        '
        'krgConfiguracionAcciones
        '
        Me.krgConfiguracionAcciones.AllowCollapsed = False
        Me.krgConfiguracionAcciones.DialogBoxLauncher = False
        Me.krgConfiguracionAcciones.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupContainer() {Me.krgtConfiguracionAcciones})
        Me.krgConfiguracionAcciones.TextLine1 = "Acciones"
        '
        'krgtConfiguracionAcciones
        '
        Me.krgtConfiguracionAcciones.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnConfiguracionAccionesGuardar, Me.btnConfiguracionAccionesCancelar})
        '
        'btnConfiguracionAccionesGuardar
        '
        Me.btnConfiguracionAccionesGuardar.ImageLarge = Global.Escritorio.My.Resources.Resources.Guardar_32
        Me.btnConfiguracionAccionesGuardar.ImageSmall = Global.Escritorio.My.Resources.Resources.Guardar_32
        Me.btnConfiguracionAccionesGuardar.KeyTip = "G"
        Me.btnConfiguracionAccionesGuardar.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.G), System.Windows.Forms.Keys)
        Me.btnConfiguracionAccionesGuardar.TextLine1 = "Guardar"
        '
        'btnConfiguracionAccionesCancelar
        '
        Me.btnConfiguracionAccionesCancelar.ImageLarge = Global.Escritorio.My.Resources.Resources.Cancelar_32
        Me.btnConfiguracionAccionesCancelar.ImageSmall = Global.Escritorio.My.Resources.Resources.Cancelar_16
        Me.btnConfiguracionAccionesCancelar.KeyTip = "C"
        Me.btnConfiguracionAccionesCancelar.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.W), System.Windows.Forms.Keys)
        Me.btnConfiguracionAccionesCancelar.TextLine1 = "Cancelar"
        '
        'tabProveedores
        '
        Me.tabProveedores.Groups.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup() {Me.krgProveedoresAcciones})
        Me.tabProveedores.KeyTip = "P"
        Me.tabProveedores.Text = "Proveedores"
        '
        'krgProveedoresAcciones
        '
        Me.krgProveedoresAcciones.AllowCollapsed = False
        Me.krgProveedoresAcciones.DialogBoxLauncher = False
        Me.krgProveedoresAcciones.Image = Nothing
        Me.krgProveedoresAcciones.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupContainer() {Me.krgtProveedoresAccionesEspera, Me.krgtProveedoresAccionesEdicion, Me.krtgProveedoresCerrar})
        Me.krgProveedoresAcciones.TextLine1 = "Acciones"
        '
        'krgtProveedoresAccionesEspera
        '
        Me.krgtProveedoresAccionesEspera.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnProveedoresAccionesNuevo, Me.btnProveedoresAccionesModificar, Me.btnProveedoresAccionesEliminar})
        '
        'btnProveedoresAccionesNuevo
        '
        Me.btnProveedoresAccionesNuevo.ImageLarge = Global.Escritorio.My.Resources.Resources.Nuevo_32
        Me.btnProveedoresAccionesNuevo.ImageSmall = Global.Escritorio.My.Resources.Resources.Nuevo_16
        Me.btnProveedoresAccionesNuevo.KeyTip = "N"
        Me.btnProveedoresAccionesNuevo.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.btnProveedoresAccionesNuevo.TextLine1 = "Nuevo"
        '
        'btnProveedoresAccionesModificar
        '
        Me.btnProveedoresAccionesModificar.ImageLarge = Global.Escritorio.My.Resources.Resources.Modificar_32
        Me.btnProveedoresAccionesModificar.ImageSmall = Global.Escritorio.My.Resources.Resources.Modificar_16
        Me.btnProveedoresAccionesModificar.KeyTip = "M"
        Me.btnProveedoresAccionesModificar.ShortcutKeys = System.Windows.Forms.Keys.F2
        Me.btnProveedoresAccionesModificar.TextLine1 = "Modificar"
        '
        'btnProveedoresAccionesEliminar
        '
        Me.btnProveedoresAccionesEliminar.ImageLarge = Global.Escritorio.My.Resources.Resources.Eliminar_32
        Me.btnProveedoresAccionesEliminar.ImageSmall = Global.Escritorio.My.Resources.Resources.Eliminar_16
        Me.btnProveedoresAccionesEliminar.KeyTip = "E"
        Me.btnProveedoresAccionesEliminar.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Delete), System.Windows.Forms.Keys)
        Me.btnProveedoresAccionesEliminar.TextLine1 = "Eliminar"
        '
        'krgtProveedoresAccionesEdicion
        '
        Me.krgtProveedoresAccionesEdicion.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnProveedoresAccionesGuardar, Me.btnProveedoresAccionesCancelar})
        '
        'btnProveedoresAccionesGuardar
        '
        Me.btnProveedoresAccionesGuardar.ImageLarge = Global.Escritorio.My.Resources.Resources.Guardar_32
        Me.btnProveedoresAccionesGuardar.ImageSmall = Global.Escritorio.My.Resources.Resources.Guardar_16
        Me.btnProveedoresAccionesGuardar.KeyTip = "G"
        Me.btnProveedoresAccionesGuardar.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.G), System.Windows.Forms.Keys)
        Me.btnProveedoresAccionesGuardar.TextLine1 = "Guardar"
        '
        'btnProveedoresAccionesCancelar
        '
        Me.btnProveedoresAccionesCancelar.ImageLarge = Global.Escritorio.My.Resources.Resources.Cancelar_32
        Me.btnProveedoresAccionesCancelar.ImageSmall = Global.Escritorio.My.Resources.Resources.Cancelar_16
        Me.btnProveedoresAccionesCancelar.KeyTip = "C"
        Me.btnProveedoresAccionesCancelar.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.W), System.Windows.Forms.Keys)
        Me.btnProveedoresAccionesCancelar.TextLine1 = "Cancelar"
        '
        'krtgProveedoresCerrar
        '
        Me.krtgProveedoresCerrar.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnProveedoresAccionesCerrar})
        '
        'btnProveedoresAccionesCerrar
        '
        Me.btnProveedoresAccionesCerrar.ImageLarge = Global.Escritorio.My.Resources.Resources.Cerrar_32
        Me.btnProveedoresAccionesCerrar.ImageSmall = Global.Escritorio.My.Resources.Resources.Cerrar_16
        Me.btnProveedoresAccionesCerrar.KeyTip = "Q"
        Me.btnProveedoresAccionesCerrar.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Q), System.Windows.Forms.Keys)
        Me.btnProveedoresAccionesCerrar.TextLine1 = "Cerrar"
        '
        'tabLotesEntrada
        '
        Me.tabLotesEntrada.Groups.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup() {Me.krgLotesEntradaAcciones})
        Me.tabLotesEntrada.KeyTip = "E"
        Me.tabLotesEntrada.Text = "L. Entrada"
        '
        'krgLotesEntradaAcciones
        '
        Me.krgLotesEntradaAcciones.AllowCollapsed = False
        Me.krgLotesEntradaAcciones.DialogBoxLauncher = False
        Me.krgLotesEntradaAcciones.Image = Nothing
        Me.krgLotesEntradaAcciones.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupContainer() {Me.krgtLotesEntradaAccionesEspera, Me.krgtLotesEntradaAccionesEdicion, Me.krgtLotesEntradaMasAcciones, Me.krgtLotesEntradaAccionesCerrar})
        Me.krgLotesEntradaAcciones.TextLine1 = "Acciones"
        '
        'krgtLotesEntradaAccionesEspera
        '
        Me.krgtLotesEntradaAccionesEspera.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnLotesEntradaAccionesNuevo, Me.btnLotesEntradaAccionesModificar, Me.btnLotesEntradaAccionesEliminar})
        '
        'btnLotesEntradaAccionesNuevo
        '
        Me.btnLotesEntradaAccionesNuevo.ImageLarge = Global.Escritorio.My.Resources.Resources.Nuevo_32
        Me.btnLotesEntradaAccionesNuevo.ImageSmall = Global.Escritorio.My.Resources.Resources.Nuevo_16
        Me.btnLotesEntradaAccionesNuevo.KeyTip = "N"
        Me.btnLotesEntradaAccionesNuevo.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.btnLotesEntradaAccionesNuevo.TextLine1 = "Nuevo"
        '
        'btnLotesEntradaAccionesModificar
        '
        Me.btnLotesEntradaAccionesModificar.ImageLarge = Global.Escritorio.My.Resources.Resources.Modificar_32
        Me.btnLotesEntradaAccionesModificar.ImageSmall = Global.Escritorio.My.Resources.Resources.Modificar_16
        Me.btnLotesEntradaAccionesModificar.KeyTip = "M"
        Me.btnLotesEntradaAccionesModificar.ShortcutKeys = System.Windows.Forms.Keys.F2
        Me.btnLotesEntradaAccionesModificar.TextLine1 = "Modificar"
        '
        'btnLotesEntradaAccionesEliminar
        '
        Me.btnLotesEntradaAccionesEliminar.ImageLarge = Global.Escritorio.My.Resources.Resources.Eliminar_32
        Me.btnLotesEntradaAccionesEliminar.ImageSmall = Global.Escritorio.My.Resources.Resources.Eliminar_16
        Me.btnLotesEntradaAccionesEliminar.KeyTip = "E"
        Me.btnLotesEntradaAccionesEliminar.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Delete), System.Windows.Forms.Keys)
        Me.btnLotesEntradaAccionesEliminar.TextLine1 = "Eliminar"
        '
        'krgtLotesEntradaAccionesEdicion
        '
        Me.krgtLotesEntradaAccionesEdicion.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnLotesEntradaAccionesGuardar, Me.btnLotesEntradaAccionesCancelar})
        '
        'btnLotesEntradaAccionesGuardar
        '
        Me.btnLotesEntradaAccionesGuardar.ImageLarge = Global.Escritorio.My.Resources.Resources.Guardar_32
        Me.btnLotesEntradaAccionesGuardar.ImageSmall = Global.Escritorio.My.Resources.Resources.Guardar_16
        Me.btnLotesEntradaAccionesGuardar.KeyTip = "G"
        Me.btnLotesEntradaAccionesGuardar.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.G), System.Windows.Forms.Keys)
        Me.btnLotesEntradaAccionesGuardar.TextLine1 = "Guardar"
        '
        'btnLotesEntradaAccionesCancelar
        '
        Me.btnLotesEntradaAccionesCancelar.ImageLarge = Global.Escritorio.My.Resources.Resources.Cancelar_32
        Me.btnLotesEntradaAccionesCancelar.ImageSmall = Global.Escritorio.My.Resources.Resources.Cancelar_16
        Me.btnLotesEntradaAccionesCancelar.KeyTip = "C"
        Me.btnLotesEntradaAccionesCancelar.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.W), System.Windows.Forms.Keys)
        Me.btnLotesEntradaAccionesCancelar.TextLine1 = "Cancelar"
        '
        'krgtLotesEntradaMasAcciones
        '
        Me.krgtLotesEntradaMasAcciones.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnLotesEntradaAccionesImprimir, Me.btnLotesEntradaAccionesImprimirListadoSalida})
        '
        'btnLotesEntradaAccionesImprimir
        '
        Me.btnLotesEntradaAccionesImprimir.ImageLarge = Global.Escritorio.My.Resources.Resources.Impresora_32
        Me.btnLotesEntradaAccionesImprimir.ImageSmall = Global.Escritorio.My.Resources.Resources.Impresora_16
        Me.btnLotesEntradaAccionesImprimir.KeyTip = "P"
        Me.btnLotesEntradaAccionesImprimir.TextLine1 = "Imprimir"
        '
        'btnLotesEntradaAccionesImprimirListadoSalida
        '
        Me.btnLotesEntradaAccionesImprimirListadoSalida.ImageLarge = Global.Escritorio.My.Resources.Resources.Impresora_32
        Me.btnLotesEntradaAccionesImprimirListadoSalida.ImageSmall = Global.Escritorio.My.Resources.Resources.Impresora_16
        Me.btnLotesEntradaAccionesImprimirListadoSalida.KeyTip = "L"
        Me.btnLotesEntradaAccionesImprimirListadoSalida.TextLine1 = "Imprimir"
        Me.btnLotesEntradaAccionesImprimirListadoSalida.TextLine2 = "Listado Sal."
        '
        'krgtLotesEntradaAccionesCerrar
        '
        Me.krgtLotesEntradaAccionesCerrar.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnLotesEntradaAccionesCerrar})
        '
        'btnLotesEntradaAccionesCerrar
        '
        Me.btnLotesEntradaAccionesCerrar.ImageLarge = Global.Escritorio.My.Resources.Resources.Cerrar_32
        Me.btnLotesEntradaAccionesCerrar.ImageSmall = Global.Escritorio.My.Resources.Resources.Cerrar_16
        Me.btnLotesEntradaAccionesCerrar.KeyTip = "Q"
        Me.btnLotesEntradaAccionesCerrar.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Q), System.Windows.Forms.Keys)
        Me.btnLotesEntradaAccionesCerrar.TextLine1 = "Cerrar"
        '
        'tabEmpaquetado
        '
        Me.tabEmpaquetado.Groups.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup() {Me.krgEmpaquetadoAcciones})
        Me.tabEmpaquetado.KeyTip = "E"
        Me.tabEmpaquetado.Text = "Empaquetado"
        '
        'krgEmpaquetadoAcciones
        '
        Me.krgEmpaquetadoAcciones.DialogBoxLauncher = False
        Me.krgEmpaquetadoAcciones.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupContainer() {Me.krgtEmpaquetadoAccionesEspera, Me.krgtEmpaquetadoAccionesEdicion, Me.krgtEmpaquetadoMasAcciones1, Me.krgtEmpaquetadoMasAcciones2, Me.krgtEmpaquetadoAccionesCerrar})
        Me.krgEmpaquetadoAcciones.TextLine1 = "Acciones"
        '
        'krgtEmpaquetadoAccionesEspera
        '
        Me.krgtEmpaquetadoAccionesEspera.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnEmpaquetadoAccionesNuevo, Me.btnEmpaquetadoAccionesModificar, Me.btnEmpaquetadoAccionesEliminar})
        '
        'btnEmpaquetadoAccionesNuevo
        '
        Me.btnEmpaquetadoAccionesNuevo.ImageLarge = Global.Escritorio.My.Resources.Resources.Nuevo_32
        Me.btnEmpaquetadoAccionesNuevo.ImageSmall = Global.Escritorio.My.Resources.Resources.Nuevo_16
        Me.btnEmpaquetadoAccionesNuevo.KeyTip = "N"
        Me.btnEmpaquetadoAccionesNuevo.TextLine1 = "Nuevo"
        '
        'btnEmpaquetadoAccionesModificar
        '
        Me.btnEmpaquetadoAccionesModificar.ImageLarge = Global.Escritorio.My.Resources.Resources.Modificar_32
        Me.btnEmpaquetadoAccionesModificar.ImageSmall = Global.Escritorio.My.Resources.Resources.Modificar_16
        Me.btnEmpaquetadoAccionesModificar.KeyTip = "M"
        Me.btnEmpaquetadoAccionesModificar.TextLine1 = "Modificar"
        '
        'btnEmpaquetadoAccionesEliminar
        '
        Me.btnEmpaquetadoAccionesEliminar.ImageLarge = Global.Escritorio.My.Resources.Resources.Eliminar_32
        Me.btnEmpaquetadoAccionesEliminar.ImageSmall = Global.Escritorio.My.Resources.Resources.Eliminar_16
        Me.btnEmpaquetadoAccionesEliminar.KeyTip = "E"
        Me.btnEmpaquetadoAccionesEliminar.TextLine1 = "Eliminar"
        '
        'krgtEmpaquetadoAccionesEdicion
        '
        Me.krgtEmpaquetadoAccionesEdicion.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnEmpaquetadoAccionesGuardar, Me.btnEmpaquetadoAccionesCancelar})
        '
        'btnEmpaquetadoAccionesGuardar
        '
        Me.btnEmpaquetadoAccionesGuardar.ImageLarge = Global.Escritorio.My.Resources.Resources.Guardar_32
        Me.btnEmpaquetadoAccionesGuardar.ImageSmall = Global.Escritorio.My.Resources.Resources.Guardar_16
        Me.btnEmpaquetadoAccionesGuardar.KeyTip = "G"
        Me.btnEmpaquetadoAccionesGuardar.TextLine1 = "Guardar"
        '
        'btnEmpaquetadoAccionesCancelar
        '
        Me.btnEmpaquetadoAccionesCancelar.ImageLarge = Global.Escritorio.My.Resources.Resources.Cancelar_32
        Me.btnEmpaquetadoAccionesCancelar.ImageSmall = Global.Escritorio.My.Resources.Resources.Cancelar_16
        Me.btnEmpaquetadoAccionesCancelar.KeyTip = "C"
        Me.btnEmpaquetadoAccionesCancelar.TextLine1 = "Cancelar"
        '
        'krgtEmpaquetadoMasAcciones1
        '
        Me.krgtEmpaquetadoMasAcciones1.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnEmpaquetadoAccionesLectorCodigos})
        '
        'btnEmpaquetadoAccionesLectorCodigos
        '
        Me.btnEmpaquetadoAccionesLectorCodigos.ImageLarge = Global.Escritorio.My.Resources.Resources.Lector_32
        Me.btnEmpaquetadoAccionesLectorCodigos.ImageSmall = Global.Escritorio.My.Resources.Resources.Lector_16
        Me.btnEmpaquetadoAccionesLectorCodigos.KeyTip = "L"
        Me.btnEmpaquetadoAccionesLectorCodigos.TextLine1 = "Lector"
        Me.btnEmpaquetadoAccionesLectorCodigos.TextLine2 = "Códigos"
        '
        'krgtEmpaquetadoMasAcciones2
        '
        Me.krgtEmpaquetadoMasAcciones2.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnEmpaquetadoAccionesImprimir, Me.btnEmpaquetadoAccionesImprimirEtiquetas, Me.btnEmpaquetadoAccionesImprimirMakro})
        '
        'btnEmpaquetadoAccionesImprimir
        '
        Me.btnEmpaquetadoAccionesImprimir.ImageLarge = Global.Escritorio.My.Resources.Resources.Impresora_32
        Me.btnEmpaquetadoAccionesImprimir.ImageSmall = Global.Escritorio.My.Resources.Resources.Impresora_16
        Me.btnEmpaquetadoAccionesImprimir.KeyTip = "I"
        Me.btnEmpaquetadoAccionesImprimir.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.P), System.Windows.Forms.Keys)
        Me.btnEmpaquetadoAccionesImprimir.TextLine1 = "Imprimir"
        '
        'btnEmpaquetadoAccionesImprimirEtiquetas
        '
        Me.btnEmpaquetadoAccionesImprimirEtiquetas.ImageLarge = Global.Escritorio.My.Resources.Resources.Impresora_32
        Me.btnEmpaquetadoAccionesImprimirEtiquetas.ImageSmall = Global.Escritorio.My.Resources.Resources.Impresora_16
        Me.btnEmpaquetadoAccionesImprimirEtiquetas.KeyTip = "I"
        Me.btnEmpaquetadoAccionesImprimirEtiquetas.TextLine1 = "Imprimir"
        Me.btnEmpaquetadoAccionesImprimirEtiquetas.TextLine2 = "Etiquetas"
        '
        'btnEmpaquetadoAccionesImprimirMakro
        '
        Me.btnEmpaquetadoAccionesImprimirMakro.ImageLarge = Global.Escritorio.My.Resources.Resources.Impresora_32
        Me.btnEmpaquetadoAccionesImprimirMakro.ImageSmall = Global.Escritorio.My.Resources.Resources.Impresora_16
        Me.btnEmpaquetadoAccionesImprimirMakro.TextLine1 = "Imprimir"
        Me.btnEmpaquetadoAccionesImprimirMakro.TextLine2 = "Makro"
        '
        'krgtEmpaquetadoAccionesCerrar
        '
        Me.krgtEmpaquetadoAccionesCerrar.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnEmpaquetadoAccionesCerrar})
        '
        'btnEmpaquetadoAccionesCerrar
        '
        Me.btnEmpaquetadoAccionesCerrar.ImageLarge = Global.Escritorio.My.Resources.Resources.Cerrar_32
        Me.btnEmpaquetadoAccionesCerrar.ImageSmall = Global.Escritorio.My.Resources.Resources.Cerrar_16
        Me.btnEmpaquetadoAccionesCerrar.KeyTip = "C"
        Me.btnEmpaquetadoAccionesCerrar.TextLine1 = "Cerrar"
        '
        'tabLotesSalida
        '
        Me.tabLotesSalida.Groups.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup() {Me.krgLotesSalidaAcciones})
        Me.tabLotesSalida.KeyTip = "S"
        Me.tabLotesSalida.Text = "L. Salida"
        '
        'krgLotesSalidaAcciones
        '
        Me.krgLotesSalidaAcciones.AllowCollapsed = False
        Me.krgLotesSalidaAcciones.DialogBoxLauncher = False
        Me.krgLotesSalidaAcciones.Image = Nothing
        Me.krgLotesSalidaAcciones.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupContainer() {Me.krgtLotesSalidaAccionesEspera, Me.krgtLotesSalidaAccionesEdicion, Me.krgtLotesSalidaMasAcciones, Me.krgtLotesSalidaAccionesCerrar})
        Me.krgLotesSalidaAcciones.TextLine1 = "Acciones"
        '
        'krgtLotesSalidaAccionesEspera
        '
        Me.krgtLotesSalidaAccionesEspera.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnLotesSalidaAccionesNuevo, Me.btnLotesSalidaAccionesModificar, Me.btnLotesSalidaAccionesEliminar})
        '
        'btnLotesSalidaAccionesNuevo
        '
        Me.btnLotesSalidaAccionesNuevo.ImageLarge = Global.Escritorio.My.Resources.Resources.Nuevo_32
        Me.btnLotesSalidaAccionesNuevo.ImageSmall = Global.Escritorio.My.Resources.Resources.Nuevo_16
        Me.btnLotesSalidaAccionesNuevo.KeyTip = "N"
        Me.btnLotesSalidaAccionesNuevo.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.btnLotesSalidaAccionesNuevo.TextLine1 = "Nuevo"
        '
        'btnLotesSalidaAccionesModificar
        '
        Me.btnLotesSalidaAccionesModificar.ImageLarge = Global.Escritorio.My.Resources.Resources.Modificar_32
        Me.btnLotesSalidaAccionesModificar.ImageSmall = Global.Escritorio.My.Resources.Resources.Modificar_16
        Me.btnLotesSalidaAccionesModificar.KeyTip = "M"
        Me.btnLotesSalidaAccionesModificar.ShortcutKeys = System.Windows.Forms.Keys.F2
        Me.btnLotesSalidaAccionesModificar.TextLine1 = "Modificar"
        '
        'btnLotesSalidaAccionesEliminar
        '
        Me.btnLotesSalidaAccionesEliminar.ImageLarge = Global.Escritorio.My.Resources.Resources.Eliminar_32
        Me.btnLotesSalidaAccionesEliminar.ImageSmall = Global.Escritorio.My.Resources.Resources.Eliminar_16
        Me.btnLotesSalidaAccionesEliminar.KeyTip = "E"
        Me.btnLotesSalidaAccionesEliminar.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Delete), System.Windows.Forms.Keys)
        Me.btnLotesSalidaAccionesEliminar.TextLine1 = "Eliminar"
        '
        'krgtLotesSalidaAccionesEdicion
        '
        Me.krgtLotesSalidaAccionesEdicion.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnLotesSalidaAccionesGuardar, Me.btnLotesSalidaAccionesCancelar})
        '
        'btnLotesSalidaAccionesGuardar
        '
        Me.btnLotesSalidaAccionesGuardar.ImageLarge = Global.Escritorio.My.Resources.Resources.Guardar_32
        Me.btnLotesSalidaAccionesGuardar.ImageSmall = Global.Escritorio.My.Resources.Resources.Guardar_16
        Me.btnLotesSalidaAccionesGuardar.KeyTip = "G"
        Me.btnLotesSalidaAccionesGuardar.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.G), System.Windows.Forms.Keys)
        Me.btnLotesSalidaAccionesGuardar.TextLine1 = "Guardar"
        '
        'btnLotesSalidaAccionesCancelar
        '
        Me.btnLotesSalidaAccionesCancelar.ImageLarge = Global.Escritorio.My.Resources.Resources.Cancelar_32
        Me.btnLotesSalidaAccionesCancelar.ImageSmall = Global.Escritorio.My.Resources.Resources.Cancelar_16
        Me.btnLotesSalidaAccionesCancelar.KeyTip = "C"
        Me.btnLotesSalidaAccionesCancelar.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.W), System.Windows.Forms.Keys)
        Me.btnLotesSalidaAccionesCancelar.TextLine1 = "Cancelar"
        '
        'krgtLotesSalidaMasAcciones
        '
        Me.krgtLotesSalidaMasAcciones.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnLotesSalidaAccionesImprimir, Me.btnLotesSalidaAccionesImprimirEtiquetas})
        '
        'btnLotesSalidaAccionesImprimir
        '
        Me.btnLotesSalidaAccionesImprimir.ImageLarge = Global.Escritorio.My.Resources.Resources.Impresora_32
        Me.btnLotesSalidaAccionesImprimir.ImageSmall = Global.Escritorio.My.Resources.Resources.Impresora_16
        Me.btnLotesSalidaAccionesImprimir.KeyTip = "P"
        Me.btnLotesSalidaAccionesImprimir.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.P), System.Windows.Forms.Keys)
        Me.btnLotesSalidaAccionesImprimir.TextLine1 = "Imprimir"
        '
        'btnLotesSalidaAccionesImprimirEtiquetas
        '
        Me.btnLotesSalidaAccionesImprimirEtiquetas.ImageLarge = Global.Escritorio.My.Resources.Resources.Impresora_32
        Me.btnLotesSalidaAccionesImprimirEtiquetas.ImageSmall = Global.Escritorio.My.Resources.Resources.Impresora_16
        Me.btnLotesSalidaAccionesImprimirEtiquetas.KeyTip = "I"
        Me.btnLotesSalidaAccionesImprimirEtiquetas.TextLine1 = "Imprimir"
        Me.btnLotesSalidaAccionesImprimirEtiquetas.TextLine2 = "Etiquetas"
        '
        'krgtLotesSalidaAccionesCerrar
        '
        Me.krgtLotesSalidaAccionesCerrar.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnLotesSalidaAccionesCerrar})
        '
        'btnLotesSalidaAccionesCerrar
        '
        Me.btnLotesSalidaAccionesCerrar.ImageLarge = Global.Escritorio.My.Resources.Resources.Cerrar_32
        Me.btnLotesSalidaAccionesCerrar.ImageSmall = Global.Escritorio.My.Resources.Resources.Cerrar_16
        Me.btnLotesSalidaAccionesCerrar.KeyTip = "Q"
        Me.btnLotesSalidaAccionesCerrar.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Q), System.Windows.Forms.Keys)
        Me.btnLotesSalidaAccionesCerrar.TextLine1 = "Cerrar"
        '
        'tabClientes
        '
        Me.tabClientes.Groups.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup() {Me.krgClientesAcciones})
        Me.tabClientes.Text = "Clientes"
        '
        'krgClientesAcciones
        '
        Me.krgClientesAcciones.AllowCollapsed = False
        Me.krgClientesAcciones.DialogBoxLauncher = False
        Me.krgClientesAcciones.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupContainer() {Me.krgtClientesAccionesEspera, Me.krgtClientesAccionesEdicion, Me.krgtClientesAccionesCerrar})
        Me.krgClientesAcciones.TextLine1 = "Acciones"
        '
        'krgtClientesAccionesEspera
        '
        Me.krgtClientesAccionesEspera.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnClientesAccionesNuevo, Me.btnClientesAccionesModificar, Me.btnClientesAccionesEliminar})
        '
        'btnClientesAccionesNuevo
        '
        Me.btnClientesAccionesNuevo.ImageLarge = Global.Escritorio.My.Resources.Resources.Nuevo_32
        Me.btnClientesAccionesNuevo.ImageSmall = Global.Escritorio.My.Resources.Resources.Nuevo_16
        Me.btnClientesAccionesNuevo.KeyTip = "N"
        Me.btnClientesAccionesNuevo.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.btnClientesAccionesNuevo.TextLine1 = "Nuevo"
        '
        'btnClientesAccionesModificar
        '
        Me.btnClientesAccionesModificar.ImageLarge = Global.Escritorio.My.Resources.Resources.Modificar_32
        Me.btnClientesAccionesModificar.ImageSmall = Global.Escritorio.My.Resources.Resources.Modificar_16
        Me.btnClientesAccionesModificar.KeyTip = "M"
        Me.btnClientesAccionesModificar.ShortcutKeys = System.Windows.Forms.Keys.F2
        Me.btnClientesAccionesModificar.TextLine1 = "Modificar"
        '
        'btnClientesAccionesEliminar
        '
        Me.btnClientesAccionesEliminar.ImageLarge = Global.Escritorio.My.Resources.Resources.Eliminar_32
        Me.btnClientesAccionesEliminar.ImageSmall = Global.Escritorio.My.Resources.Resources.Eliminar_16
        Me.btnClientesAccionesEliminar.KeyTip = "E"
        Me.btnClientesAccionesEliminar.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Delete), System.Windows.Forms.Keys)
        Me.btnClientesAccionesEliminar.TextLine1 = "Eliminar"
        '
        'krgtClientesAccionesEdicion
        '
        Me.krgtClientesAccionesEdicion.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnClientesAccionesGuardar, Me.btnClientesAccionesCancelar})
        '
        'btnClientesAccionesGuardar
        '
        Me.btnClientesAccionesGuardar.ImageLarge = Global.Escritorio.My.Resources.Resources.Guardar_32
        Me.btnClientesAccionesGuardar.ImageSmall = Global.Escritorio.My.Resources.Resources.Guardar_16
        Me.btnClientesAccionesGuardar.KeyTip = "G"
        Me.btnClientesAccionesGuardar.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.G), System.Windows.Forms.Keys)
        Me.btnClientesAccionesGuardar.TextLine1 = "Guardar"
        '
        'btnClientesAccionesCancelar
        '
        Me.btnClientesAccionesCancelar.ImageLarge = Global.Escritorio.My.Resources.Resources.Cancelar_32
        Me.btnClientesAccionesCancelar.ImageSmall = Global.Escritorio.My.Resources.Resources.Cancelar_16
        Me.btnClientesAccionesCancelar.KeyTip = "C"
        Me.btnClientesAccionesCancelar.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.W), System.Windows.Forms.Keys)
        Me.btnClientesAccionesCancelar.TextLine1 = "Cancelar"
        '
        'krgtClientesAccionesCerrar
        '
        Me.krgtClientesAccionesCerrar.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnClientesAccionesCerrar})
        '
        'btnClientesAccionesCerrar
        '
        Me.btnClientesAccionesCerrar.ImageLarge = Global.Escritorio.My.Resources.Resources.Cerrar_32
        Me.btnClientesAccionesCerrar.ImageSmall = Global.Escritorio.My.Resources.Resources.Cerrar_16
        Me.btnClientesAccionesCerrar.KeyTip = "Q"
        Me.btnClientesAccionesCerrar.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Q), System.Windows.Forms.Keys)
        Me.btnClientesAccionesCerrar.TextLine1 = "Cerrar"
        '
        'tabExpedidores
        '
        Me.tabExpedidores.Groups.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup() {krgExpedidoresAcciones})
        Me.tabExpedidores.Text = "Expedidores"
        '
        'tabEtiquetas
        '
        Me.tabEtiquetas.Groups.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup() {Me.krgEtiquetasAcciones})
        Me.tabEtiquetas.KeyTip = "E"
        Me.tabEtiquetas.Text = "Etiquetas"
        '
        'krgEtiquetasAcciones
        '
        Me.krgEtiquetasAcciones.DialogBoxLauncher = False
        Me.krgEtiquetasAcciones.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupContainer() {Me.krgtEtiquetasAccionesEspera, Me.krgtEtiquetasAccionesEdicion, Me.krgtEtiquetasMasAcciones, Me.krgtEtiquetasAccionesCerrar})
        Me.krgEtiquetasAcciones.TextLine1 = "Acciones"
        '
        'krgtEtiquetasAccionesEspera
        '
        Me.krgtEtiquetasAccionesEspera.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnEtiquetasAccionesNuevo, Me.btnEtiquetasAccionesModificar, Me.btnEtiquetasAccionesEliminar})
        '
        'btnEtiquetasAccionesNuevo
        '
        Me.btnEtiquetasAccionesNuevo.ImageLarge = Global.Escritorio.My.Resources.Resources.Nuevo_32
        Me.btnEtiquetasAccionesNuevo.ImageSmall = Global.Escritorio.My.Resources.Resources.Nuevo_16
        Me.btnEtiquetasAccionesNuevo.KeyTip = "N"
        Me.btnEtiquetasAccionesNuevo.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.btnEtiquetasAccionesNuevo.TextLine1 = "Nuevo"
        '
        'btnEtiquetasAccionesModificar
        '
        Me.btnEtiquetasAccionesModificar.ImageLarge = Global.Escritorio.My.Resources.Resources.Modificar_32
        Me.btnEtiquetasAccionesModificar.ImageSmall = Global.Escritorio.My.Resources.Resources.Modificar_16
        Me.btnEtiquetasAccionesModificar.ShortcutKeys = System.Windows.Forms.Keys.F2
        Me.btnEtiquetasAccionesModificar.TextLine1 = "Modificar"
        '
        'btnEtiquetasAccionesEliminar
        '
        Me.btnEtiquetasAccionesEliminar.ImageLarge = Global.Escritorio.My.Resources.Resources.Eliminar_32
        Me.btnEtiquetasAccionesEliminar.ImageSmall = Global.Escritorio.My.Resources.Resources.Eliminar_16
        Me.btnEtiquetasAccionesEliminar.KeyTip = "E"
        Me.btnEtiquetasAccionesEliminar.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Delete), System.Windows.Forms.Keys)
        Me.btnEtiquetasAccionesEliminar.TextLine1 = "Eliminar"
        '
        'krgtEtiquetasAccionesEdicion
        '
        Me.krgtEtiquetasAccionesEdicion.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnEtiquetasAccionesGuardar, Me.btnEtiquetasAccionesCancelar})
        '
        'btnEtiquetasAccionesGuardar
        '
        Me.btnEtiquetasAccionesGuardar.ImageLarge = Global.Escritorio.My.Resources.Resources.Guardar_32
        Me.btnEtiquetasAccionesGuardar.ImageSmall = Global.Escritorio.My.Resources.Resources.Guardar_16
        Me.btnEtiquetasAccionesGuardar.KeyTip = "G"
        Me.btnEtiquetasAccionesGuardar.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.G), System.Windows.Forms.Keys)
        Me.btnEtiquetasAccionesGuardar.TextLine1 = "Guardar"
        '
        'btnEtiquetasAccionesCancelar
        '
        Me.btnEtiquetasAccionesCancelar.ImageLarge = Global.Escritorio.My.Resources.Resources.Cancelar_32
        Me.btnEtiquetasAccionesCancelar.ImageSmall = Global.Escritorio.My.Resources.Resources.Cancelar_16
        Me.btnEtiquetasAccionesCancelar.KeyTip = "C"
        Me.btnEtiquetasAccionesCancelar.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.W), System.Windows.Forms.Keys)
        Me.btnEtiquetasAccionesCancelar.TextLine1 = "Cancelar"
        '
        'krgtEtiquetasMasAcciones
        '
        Me.krgtEtiquetasMasAcciones.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnEtiquetasMasAccionesImprimir})
        '
        'btnEtiquetasMasAccionesImprimir
        '
        Me.btnEtiquetasMasAccionesImprimir.ImageLarge = Global.Escritorio.My.Resources.Resources.Impresora_32
        Me.btnEtiquetasMasAccionesImprimir.ImageSmall = Global.Escritorio.My.Resources.Resources.Impresora_16
        Me.btnEtiquetasMasAccionesImprimir.KeyTip = "I"
        Me.btnEtiquetasMasAccionesImprimir.TextLine1 = "Imprimir"
        '
        'krgtEtiquetasAccionesCerrar
        '
        Me.krgtEtiquetasAccionesCerrar.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnEtiquetasAccionesCerrar})
        '
        'btnEtiquetasAccionesCerrar
        '
        Me.btnEtiquetasAccionesCerrar.ImageLarge = Global.Escritorio.My.Resources.Resources.Cerrar_32
        Me.btnEtiquetasAccionesCerrar.ImageSmall = Global.Escritorio.My.Resources.Resources.Cerrar_16
        Me.btnEtiquetasAccionesCerrar.KeyTip = "C"
        Me.btnEtiquetasAccionesCerrar.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.F4), System.Windows.Forms.Keys)
        Me.btnEtiquetasAccionesCerrar.TextLine1 = "Cerrar"
        '
        'tabInforme
        '
        Me.tabInforme.Groups.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup() {Me.krgReporteAcciones, Me.krgReportePaginacion})
        Me.tabInforme.Text = "Informe"
        '
        'krgReporteAcciones
        '
        Me.krgReporteAcciones.AllowCollapsed = False
        Me.krgReporteAcciones.DialogBoxLauncher = False
        Me.krgReporteAcciones.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupContainer() {Me.krgtInformesAccionesCerrar, Me.krgtInformesAcciones2})
        Me.krgReporteAcciones.TextLine1 = "Acciones"
        '
        'krgtInformesAccionesCerrar
        '
        Me.krgtInformesAccionesCerrar.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnInformesAccionesVerOcultarLogos, Me.btnInformesAccionesCerrar})
        '
        'btnInformesAccionesVerOcultarLogos
        '
        Me.btnInformesAccionesVerOcultarLogos.ButtonType = ComponentFactory.Krypton.Ribbon.GroupButtonType.Check
        Me.btnInformesAccionesVerOcultarLogos.Checked = True
        Me.btnInformesAccionesVerOcultarLogos.ImageLarge = Global.Escritorio.My.Resources.Resources.MostrarLogos32
        Me.btnInformesAccionesVerOcultarLogos.ImageSmall = Global.Escritorio.My.Resources.Resources.MostrarLogos16
        Me.btnInformesAccionesVerOcultarLogos.KeyTip = "M"
        Me.btnInformesAccionesVerOcultarLogos.TextLine1 = "Ver/Ocultar"
        Me.btnInformesAccionesVerOcultarLogos.TextLine2 = "Logotipos"
        '
        'btnInformesAccionesCerrar
        '
        Me.btnInformesAccionesCerrar.ImageLarge = Global.Escritorio.My.Resources.Resources.Cerrar_32
        Me.btnInformesAccionesCerrar.ImageSmall = Global.Escritorio.My.Resources.Resources.Cerrar_16
        Me.btnInformesAccionesCerrar.KeyTip = "C"
        Me.btnInformesAccionesCerrar.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.W), System.Windows.Forms.Keys)
        Me.btnInformesAccionesCerrar.TextLine1 = "Cerrar"
        '
        'krgtInformesAcciones2
        '
        Me.krgtInformesAcciones2.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnInformesAccionesImprimir, Me.btnInformeAccionesExportar})
        '
        'btnInformesAccionesImprimir
        '
        Me.btnInformesAccionesImprimir.ImageLarge = Global.Escritorio.My.Resources.Resources.Impresora_32
        Me.btnInformesAccionesImprimir.ImageSmall = Global.Escritorio.My.Resources.Resources.Impresora_16
        Me.btnInformesAccionesImprimir.TextLine1 = "Imprimir"
        '
        'btnInformeAccionesExportar
        '
        Me.btnInformeAccionesExportar.ImageLarge = Global.Escritorio.My.Resources.Resources.Guardar_32
        Me.btnInformeAccionesExportar.ImageSmall = Global.Escritorio.My.Resources.Resources.Guardar_16
        Me.btnInformeAccionesExportar.TextLine1 = "Exportar"
        '
        'krgReportePaginacion
        '
        Me.krgReportePaginacion.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupContainer() {Me.krgtInformesPaginacion1, Me.krgtInformesPaginacion2})
        Me.krgReportePaginacion.TextLine1 = "Paginación"
        '
        'krgtInformesPaginacion1
        '
        Me.krgtInformesPaginacion1.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnInformesPaginaPrimera, Me.btnInformesPaginaAnterior, Me.btnInformesPaginaSiguiente})
        '
        'btnInformesPaginaPrimera
        '
        Me.btnInformesPaginaPrimera.ImageLarge = Global.Escritorio.My.Resources.Resources.Primero_Grande
        Me.btnInformesPaginaPrimera.ImageSmall = Global.Escritorio.My.Resources.Resources.Primero_Pequena
        Me.btnInformesPaginaPrimera.TextLine1 = "Primera"
        '
        'btnInformesPaginaAnterior
        '
        Me.btnInformesPaginaAnterior.ImageLarge = Global.Escritorio.My.Resources.Resources.Anterior_Grande
        Me.btnInformesPaginaAnterior.ImageSmall = Global.Escritorio.My.Resources.Resources.Anterior_Pequena
        Me.btnInformesPaginaAnterior.TextLine1 = "Anterior"
        '
        'btnInformesPaginaSiguiente
        '
        Me.btnInformesPaginaSiguiente.ImageLarge = Global.Escritorio.My.Resources.Resources.Siguiente_Grande
        Me.btnInformesPaginaSiguiente.ImageSmall = Global.Escritorio.My.Resources.Resources.Siguiente_Pequena
        Me.btnInformesPaginaSiguiente.TextLine1 = "Siguiente"
        '
        'krgtInformesPaginacion2
        '
        Me.krgtInformesPaginacion2.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnInformesPaginaUltima})
        '
        'btnInformesPaginaUltima
        '
        Me.btnInformesPaginaUltima.ImageLarge = Global.Escritorio.My.Resources.Resources.Ultimo_Grande
        Me.btnInformesPaginaUltima.ImageSmall = Global.Escritorio.My.Resources.Resources.Ultimo_Pequena
        Me.btnInformesPaginaUltima.TextLine1 = "Última"
        '
        'tabGeneracionEtiquetas
        '
        Me.tabGeneracionEtiquetas.Groups.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup() {Me.mnuGenerarEtiquetasAcciones})
        Me.tabGeneracionEtiquetas.KeyTip = "G"
        Me.tabGeneracionEtiquetas.Text = "Gen. Etiq."
        '
        'mnuGenerarEtiquetasAcciones
        '
        Me.mnuGenerarEtiquetasAcciones.AllowCollapsed = False
        Me.mnuGenerarEtiquetasAcciones.DialogBoxLauncher = False
        Me.mnuGenerarEtiquetasAcciones.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupContainer() {Me.kgtGenerarEtiquetasAcciones1, Me.kgtGenerarEtiquetasAcciones2, Me.kgtGenerarEtiquetasAccionesCerrar})
        Me.mnuGenerarEtiquetasAcciones.TextLine1 = "Acciones"
        '
        'kgtGenerarEtiquetasAcciones1
        '
        Me.kgtGenerarEtiquetasAcciones1.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnGenerarEtiquetasGenerarPesoActual, Me.btnGenerarEtiquetasGenerarPesoManual, Me.btnGenerarEtiquetasRemprimirUltimaEtiqueta})
        '
        'btnGenerarEtiquetasGenerarPesoActual
        '
        Me.btnGenerarEtiquetasGenerarPesoActual.ImageLarge = Global.Escritorio.My.Resources.Resources.Etiqueta_32
        Me.btnGenerarEtiquetasGenerarPesoActual.ImageSmall = Global.Escritorio.My.Resources.Resources.Etiqueta_16
        Me.btnGenerarEtiquetasGenerarPesoActual.KeyTip = "A"
        Me.btnGenerarEtiquetasGenerarPesoActual.TextLine1 = "Generar con"
        Me.btnGenerarEtiquetasGenerarPesoActual.TextLine2 = "Peso Actual"
        '
        'btnGenerarEtiquetasGenerarPesoManual
        '
        Me.btnGenerarEtiquetasGenerarPesoManual.ImageLarge = Global.Escritorio.My.Resources.Resources.Etiqueta_32
        Me.btnGenerarEtiquetasGenerarPesoManual.ImageSmall = Global.Escritorio.My.Resources.Resources.Etiqueta_16
        Me.btnGenerarEtiquetasGenerarPesoManual.KeyTip = "M"
        Me.btnGenerarEtiquetasGenerarPesoManual.TextLine1 = "Generar con"
        Me.btnGenerarEtiquetasGenerarPesoManual.TextLine2 = "Peso Manual"
        '
        'btnGenerarEtiquetasRemprimirUltimaEtiqueta
        '
        Me.btnGenerarEtiquetasRemprimirUltimaEtiqueta.ImageLarge = Global.Escritorio.My.Resources.Resources.Impresora_32
        Me.btnGenerarEtiquetasRemprimirUltimaEtiqueta.ImageSmall = Global.Escritorio.My.Resources.Resources.Impresora_16
        Me.btnGenerarEtiquetasRemprimirUltimaEtiqueta.KeyTip = "R"
        Me.btnGenerarEtiquetasRemprimirUltimaEtiqueta.TextLine1 = "Remprimir"
        Me.btnGenerarEtiquetasRemprimirUltimaEtiqueta.TextLine2 = "Ult. Etiqueta"
        '
        'kgtGenerarEtiquetasAcciones2
        '
        Me.kgtGenerarEtiquetasAcciones2.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnGenerarEtiquetasDescartarUltimaEtiqueta, Me.chkGenerarEtiquetasImpresionAutomatica, Me.chkGenerarEtiquetasImpresionAutomatica2})
        '
        'btnGenerarEtiquetasDescartarUltimaEtiqueta
        '
        Me.btnGenerarEtiquetasDescartarUltimaEtiqueta.ImageLarge = Global.Escritorio.My.Resources.Resources.Descartar_32
        Me.btnGenerarEtiquetasDescartarUltimaEtiqueta.ImageSmall = Global.Escritorio.My.Resources.Resources.Descartar_16
        Me.btnGenerarEtiquetasDescartarUltimaEtiqueta.KeyTip = "D"
        Me.btnGenerarEtiquetasDescartarUltimaEtiqueta.TextLine1 = "Descartar"
        Me.btnGenerarEtiquetasDescartarUltimaEtiqueta.TextLine2 = "Ult. Etiqueta"
        '
        'chkGenerarEtiquetasImpresionAutomatica
        '
        Me.chkGenerarEtiquetasImpresionAutomatica.ButtonType = ComponentFactory.Krypton.Ribbon.GroupButtonType.Check
        Me.chkGenerarEtiquetasImpresionAutomatica.ImageLarge = Global.Escritorio.My.Resources.Resources.Impresora_32
        Me.chkGenerarEtiquetasImpresionAutomatica.KeyTip = "I"
        Me.chkGenerarEtiquetasImpresionAutomatica.TextLine1 = "Impresión"
        Me.chkGenerarEtiquetasImpresionAutomatica.TextLine2 = "Automática"
        '
        'chkGenerarEtiquetasImpresionAutomatica2
        '
        Me.chkGenerarEtiquetasImpresionAutomatica2.KeyTip = "I"
        Me.chkGenerarEtiquetasImpresionAutomatica2.TextLine1 = "Impresión"
        Me.chkGenerarEtiquetasImpresionAutomatica2.TextLine2 = "Automática"
        Me.chkGenerarEtiquetasImpresionAutomatica2.Visible = False
        '
        'kgtGenerarEtiquetasAccionesCerrar
        '
        Me.kgtGenerarEtiquetasAccionesCerrar.Items.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupItem() {Me.btnGenerarEtiquetasCerrar})
        '
        'btnGenerarEtiquetasCerrar
        '
        Me.btnGenerarEtiquetasCerrar.ImageLarge = Global.Escritorio.My.Resources.Resources.Cerrar_32
        Me.btnGenerarEtiquetasCerrar.ImageSmall = Global.Escritorio.My.Resources.Resources.Cerrar_16
        Me.btnGenerarEtiquetasCerrar.KeyTip = "C"
        Me.btnGenerarEtiquetasCerrar.TextLine1 = "Cerrar"
        '
        'tabAyuda
        '
        Me.tabAyuda.Groups.AddRange(New ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup() {Me.mnuAyudaAyuda})
        Me.tabAyuda.KeyTip = "A"
        Me.tabAyuda.Text = "Ayuda"
        '
        'upActualizador
        '
        Me.upActualizador.ApplicationId = New System.Guid("569a4b63-bbfc-4ab3-9dfc-e958262d5e23")
        Me.upActualizador.BypassProxyOnLocal = True
        Me.upActualizador.ClientAccessKey = Nothing
        Me.upActualizador.EnableAutoChaining = True
        Me.upActualizador.EnableOfflineUpdates = True
        Me.upActualizador.PublicKeyToken = resources.GetString("upActualizador.PublicKeyToken")
        Me.upActualizador.UpdateLocation = "http://update.quadralia.com/qLotes/Escritorio"
        Me.upActualizador.UseHostAssemblyVersion = True
        '
        'ssEstado
        '
        Me.ssEstado.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.ssEstado.GripStyle = System.Windows.Forms.ToolStripGripStyle.Visible
        Me.ssEstado.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuInfoActualizacion})
        Me.ssEstado.Location = New System.Drawing.Point(0, 274)
        Me.ssEstado.Name = "ssEstado"
        Me.ssEstado.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode
        Me.ssEstado.Size = New System.Drawing.Size(1085, 22)
        Me.ssEstado.TabIndex = 12
        Me.ssEstado.Text = "StatusStrip1"
        '
        'mnuInfoActualizacion
        '
        Me.mnuInfoActualizacion.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.mnuInfoActualizacion.ApplicationName = "qLotes"
        Me.mnuInfoActualizacion.ApplyUpdateOptions = Kjs.AppLife.Update.Controller.ApplyUpdateOptions.AutoClose
        Me.mnuInfoActualizacion.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.mnuInfoActualizacion.Name = "mnuInfoActualizacion"
        Me.mnuInfoActualizacion.NoUpdateCustomText = "Su software está actualizado"
        Me.mnuInfoActualizacion.NoUpdateImageLarge = CType(resources.GetObject("mnuInfoActualizacion.NoUpdateImageLarge"), System.Drawing.Image)
        Me.mnuInfoActualizacion.NoUpdateImageSmall = CType(resources.GetObject("mnuInfoActualizacion.NoUpdateImageSmall"), System.Drawing.Image)
        Me.mnuInfoActualizacion.ReadyToApplyCustomText = "Actualización lista para ser aplicada"
        Me.mnuInfoActualizacion.ReadyToApplyImageLarge = CType(resources.GetObject("mnuInfoActualizacion.ReadyToApplyImageLarge"), System.Drawing.Image)
        Me.mnuInfoActualizacion.ReadyToApplyImageSmall = CType(resources.GetObject("mnuInfoActualizacion.ReadyToApplyImageSmall"), System.Drawing.Image)
        Me.mnuInfoActualizacion.Size = New System.Drawing.Size(125, 20)
        Me.mnuInfoActualizacion.SummaryCustomHtml = Nothing
        Me.mnuInfoActualizacion.SummaryCustomText = Nothing
        '
        'mnuInformesStock_Entradas
        '
        Me.mnuInformesStock_Entradas.ImageLarge = Global.Escritorio.My.Resources.Resources.Entrada_32
        Me.mnuInformesStock_Entradas.ImageSmall = Global.Escritorio.My.Resources.Resources.Entrada_16
        Me.mnuInformesStock_Entradas.TextLine1 = "Informe de"
        Me.mnuInformesStock_Entradas.TextLine2 = "Entradas"
        '
        'frmPrincipal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(74, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1085, 296)
        Me.Controls.Add(Me.ssEstado)
        Me.Controls.Add(Me.rbbPrincipal)
        Me.Controls.Add(Me.mnuFIxRibbon)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.mnuFIxRibbon
        Me.Name = "frmPrincipal"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Aplicación"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.rbbPrincipal, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ssEstado.ResumeLayout(False)
        Me.ssEstado.PerformLayout()
        CType(Me.mnuInfoActualizacion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tmrReloj As System.Windows.Forms.Timer
    Friend WithEvents mngManager As ComponentFactory.Krypton.Toolkit.KryptonManager
    Friend WithEvents Paleta As ComponentFactory.Krypton.Toolkit.KryptonPalette
    Friend WithEvents ntfIcono As System.Windows.Forms.NotifyIcon
    Friend WithEvents mnuFIxRibbon As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuOrbCabeceraConfiguracion As ComponentFactory.Krypton.Toolkit.KryptonContextMenuHeading
    Friend WithEvents mnuOrbConfiguracionPrograma As ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem
    Friend WithEvents mnuOrbUsuarios As ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem
    Friend WithEvents mnuOrbDatosMaestros As ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem
    Friend WithEvents mnuOrbCabeceraSalirAplicacion As ComponentFactory.Krypton.Toolkit.KryptonContextMenuHeading
    Friend WithEvents mnuSalir As ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem
    Friend WithEvents mnuConfiguracionPrograma As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents mnuConfiguracionUsuarios As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents krtGeneralConfiguracion1 As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents mnuConfiguracionAcercaDe As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents mnuAyudaAyuda2 As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents krgGeneralConfiguracion As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup
    Friend WithEvents tabGeneral As ComponentFactory.Krypton.Ribbon.KryptonRibbonTab
    Friend WithEvents tabInformes As ComponentFactory.Krypton.Ribbon.KryptonRibbonTab
    Friend WithEvents btnUsuariosAccionesNuevo As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents btnUsuariosAccionesModificar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents btnUsuariosAccionesEliminar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents krgtUsuariosAccionesEspera As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents btnUsuariosAccionesGuardar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents btnUsuariosAccionesCancelar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents krgtUsuariosAccionesEdicion As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents btnUsuariosSalir As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents krgtUsuariosSalir As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents krgUsuariosAcciones As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup
    Friend WithEvents tabUsuarios As ComponentFactory.Krypton.Ribbon.KryptonRibbonTab
    Friend WithEvents mnuRapidoCambiarContrasenha As ComponentFactory.Krypton.Ribbon.KryptonRibbonQATButton
    Friend WithEvents mnuRapidoGuardar As ComponentFactory.Krypton.Ribbon.KryptonRibbonQATButton
    Friend WithEvents rbbPrincipal As ComponentFactory.Krypton.Ribbon.KryptonRibbon
    Friend WithEvents tabVentanas As ComponentFactory.Krypton.Ribbon.KryptonRibbonTab
    Friend WithEvents krgVentanas As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup
    Friend WithEvents krgtVentanas1 As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents btnVentanasMinimizar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents btnVentanasRestaurar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents btnVentanasCerrar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents krgtVentanas2 As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents btnVentanasMosaicoH As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents btnVentanasMosaicoV As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents btnVentanasCascada As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents tabConfiguracion As ComponentFactory.Krypton.Ribbon.KryptonRibbonTab
    Friend WithEvents krgConfiguracionAcciones As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup
    Friend WithEvents krgtConfiguracionAcciones As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents btnConfiguracionAccionesGuardar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents btnConfiguracionAccionesCancelar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents krgGeneralDatosAdministrativos As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup
    Friend WithEvents krtGeneralAdministrativos As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents mnuDatosAdministrativosProveedores As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents mnuDatosAdministrativosLotesEntrada As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents mnuDatosAdministrativosLotesSalida As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents tabProveedores As ComponentFactory.Krypton.Ribbon.KryptonRibbonTab
    Friend WithEvents krgProveedoresAcciones As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup
    Friend WithEvents krgtProveedoresAccionesEspera As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents btnProveedoresAccionesNuevo As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents btnProveedoresAccionesModificar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents btnProveedoresAccionesEliminar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents krgtProveedoresAccionesEdicion As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents btnProveedoresAccionesGuardar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents btnProveedoresAccionesCancelar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents krtgProveedoresCerrar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents btnProveedoresAccionesCerrar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents tabLotesEntrada As ComponentFactory.Krypton.Ribbon.KryptonRibbonTab
    Friend WithEvents krgLotesEntradaAcciones As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup
    Friend WithEvents tabLotesSalida As ComponentFactory.Krypton.Ribbon.KryptonRibbonTab
    Friend WithEvents krgtLotesEntradaAccionesEspera As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents btnLotesEntradaAccionesNuevo As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents btnLotesEntradaAccionesModificar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents btnLotesEntradaAccionesEliminar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents krgtLotesEntradaAccionesEdicion As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents btnLotesEntradaAccionesGuardar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents btnLotesEntradaAccionesCancelar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents krgtLotesEntradaAccionesCerrar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents btnLotesEntradaAccionesCerrar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents krgLotesSalidaAcciones As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup
    Friend WithEvents krgtLotesSalidaAccionesEspera As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents btnLotesSalidaAccionesNuevo As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents btnLotesSalidaAccionesModificar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents btnLotesSalidaAccionesEliminar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents krgtLotesSalidaAccionesEdicion As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents btnLotesSalidaAccionesGuardar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents btnLotesSalidaAccionesCancelar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents krgtLotesSalidaAccionesCerrar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents btnLotesSalidaAccionesCerrar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents tabClientes As ComponentFactory.Krypton.Ribbon.KryptonRibbonTab
    Friend WithEvents krgClientesAcciones As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup
    Friend WithEvents krgtClientesAccionesEspera As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents btnClientesAccionesNuevo As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents btnClientesAccionesModificar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents btnClientesAccionesEliminar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents krgtClientesAccionesEdicion As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents btnClientesAccionesGuardar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents btnClientesAccionesCancelar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents krgtClientesAccionesCerrar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents btnClientesAccionesCerrar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents krgGeneralLotes As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup
    Friend WithEvents krtGeneralLotes As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents KryptonRibbonGroupButton3 As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents mnuDatosAdministrativosClientes As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents mnuDatosAdministrativosExpedidores As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents tabExpedidores As ComponentFactory.Krypton.Ribbon.KryptonRibbonTab
    Friend WithEvents krgtExpedidoresAccionesEspera As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents btnExpedidoresAccionesNuevo As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents btnExpedidoresAccionesModificar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents btnExpedidoresAccionesEliminar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents krgtExpedidoresAccionesEdicion As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents btnExpedidoresAccionesGuardar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents btnExpedidoresAccionesCancelar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents krgtExpedidoresAccionesCerrar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents btnExpedidoresAccionesCerrar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents mnuOrbDatosAdministrativos As ComponentFactory.Krypton.Toolkit.KryptonContextMenuHeading
    Friend WithEvents mnuOrbExpedidores As ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem
    Friend WithEvents mnuOrbProveedores As ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem
    Friend WithEvents mnuOrbClientes As ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem
    Friend WithEvents mnuOrbLotes As ComponentFactory.Krypton.Toolkit.KryptonContextMenuHeading
    Friend WithEvents mnuOrbLotesEntrada As ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem
    Friend WithEvents mnuOrbLotesSalida As ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem
    Friend WithEvents mnuOrbCopiaSeguridad As ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem
    Friend WithEvents rtnConfiguracionPrograma As ComponentFactory.Krypton.Toolkit.KryptonContextMenu
    Friend WithEvents rtnConfiguracionProgramaCabecera As ComponentFactory.Krypton.Toolkit.KryptonContextMenuItems
    Friend WithEvents rtnConfiguracionProgramaBaseDatos As ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem
    Friend WithEvents rtnConfiguracionProgramaParametrosGlobales As ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem
    Friend WithEvents rtnConfiguracionProgramaCopiaSeguridad As ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem
    Friend WithEvents rtnConfiguracionProgramaDatosMaestros As ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem
    Friend WithEvents mnuOrbBaseDatos As ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem
    Friend WithEvents krgtLotesSalidaMasAcciones As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents btnLotesSalidaAccionesImprimirEtiquetas As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents btnLotesSalidaAccionesImprimir As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents tabInforme As ComponentFactory.Krypton.Ribbon.KryptonRibbonTab
    Friend WithEvents krgReporteAcciones As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup
    Friend WithEvents krgtInformesAccionesCerrar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents btnInformesAccionesCerrar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents btnLotesEntradaAccionesImprimir As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents btnLotesEntradaAccionesImprimirListadoSalida As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents btnInformesAccionesVerOcultarLogos As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents mnuConfiguracionComprobarActualizaciones As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents tabGeneracionEtiquetas As ComponentFactory.Krypton.Ribbon.KryptonRibbonTab
    Friend WithEvents mnuGenerarEtiquetasAcciones As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup
    Friend WithEvents kgtGenerarEtiquetasAcciones1 As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents btnGenerarEtiquetasGenerarPesoActual As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents btnGenerarEtiquetasGenerarPesoManual As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents btnGenerarEtiquetasRemprimirUltimaEtiqueta As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents kgtGenerarEtiquetasAcciones2 As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents btnGenerarEtiquetasDescartarUltimaEtiqueta As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents chkGenerarEtiquetasImpresionAutomatica As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents kgtGenerarEtiquetasAccionesCerrar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents btnGenerarEtiquetasCerrar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents chkGenerarEtiquetasImpresionAutomatica2 As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupCheckBox
    Friend WithEvents mnuDatosAdministrativosEtiquetas As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents mnuOrbEtiquetas As ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem
    Friend WithEvents mnuOrbEtiquetasGeneracionAutomatica As ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem
    Friend WithEvents tabEtiquetas As ComponentFactory.Krypton.Ribbon.KryptonRibbonTab
    Friend WithEvents krgEtiquetasAcciones As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup
    Friend WithEvents krgtEtiquetasAccionesEspera As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents btnEtiquetasAccionesNuevo As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents btnEtiquetasAccionesModificar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents btnEtiquetasAccionesEliminar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents krgtEtiquetasAccionesEdicion As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents btnEtiquetasAccionesGuardar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents btnEtiquetasAccionesCancelar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents krgtEtiquetasAccionesCerrar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents btnEtiquetasAccionesCerrar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents krgtLotesEntradaMasAcciones As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents krgtEtiquetasMasAcciones As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents btnEtiquetasMasAccionesImprimir As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents rtnEtiquetas As ComponentFactory.Krypton.Toolkit.KryptonContextMenu
    Friend WithEvents rtnEtiquetasElementos As ComponentFactory.Krypton.Toolkit.KryptonContextMenuItems
    Friend WithEvents rtnEtiquetasGeneracionAutomatica As ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem
    Friend WithEvents btnSincronizarTrazamare As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents mnuAlbaranesEntrada As ComponentFactory.Krypton.Toolkit.KryptonContextMenu
    Friend WithEvents mnuAlbaranesEntradaElementos As ComponentFactory.Krypton.Toolkit.KryptonContextMenuItems
    Friend WithEvents mnuAlbaranesEntradaImportar As ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem
    Friend WithEvents mnuAyudaAyuda As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup
    Friend WithEvents KryptonRibbonGroupTriple1 As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents mnuAyudaWebQuadralia As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents mnuAyudaAyudaSoporteRemoto As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents tabAyuda As ComponentFactory.Krypton.Ribbon.KryptonRibbonTab
    Friend WithEvents ssEstado As System.Windows.Forms.StatusStrip
    Friend WithEvents mnuInfoActualizacion As Kjs.AppLife.Update.Controller.StatusStripUpdateDisplay
    Friend WithEvents upActualizador As Kjs.AppLife.Update.Controller.UpdateController
    Friend WithEvents tabEmpaquetado As ComponentFactory.Krypton.Ribbon.KryptonRibbonTab
    Friend WithEvents krgEmpaquetadoAcciones As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroup
    Friend WithEvents krgtEmpaquetadoAccionesEspera As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents btnEmpaquetadoAccionesNuevo As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents btnEmpaquetadoAccionesModificar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents btnEmpaquetadoAccionesEliminar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents krgtEmpaquetadoAccionesEdicion As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents btnEmpaquetadoAccionesGuardar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents btnEmpaquetadoAccionesCancelar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents krgtEmpaquetadoAccionesCerrar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents btnEmpaquetadoAccionesCerrar As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents krgtEmpaquetadoMasAcciones2 As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents btnEmpaquetadoAccionesImprimir As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents KryptonRibbonGroupTriple2 As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents mnuDatosAdministrativosEmpaquetado As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents mnuOrbEmpaquetado As ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem
    Friend WithEvents btnEmpaquetadoAccionesImprimirEtiquetas As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents krgtEmpaquetadoMasAcciones1 As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents btnEmpaquetadoAccionesLectorCodigos As ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton
    Friend WithEvents rtnSincronizacionEtiquetas As ComponentFactory.Krypton.Toolkit.KryptonContextMenu
    Friend WithEvents rtnSincronizacionEtiquetasElementos As ComponentFactory.Krypton.Toolkit.KryptonContextMenuItems
    Friend WithEvents rtnSincronizacionEtiquetasVerLog As ComponentFactory.Krypton.Toolkit.KryptonContextMenuItem
    Friend WithEvents btnEmpaquetadoAccionesImprimirMakro As Ribbon.KryptonRibbonGroupButton
    Friend WithEvents krgInformesStock As Ribbon.KryptonRibbonGroup
    Friend WithEvents krtInformesStock1 As Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents mnuInformesStock_Stock As Ribbon.KryptonRibbonGroupButton
    Friend WithEvents krgtInformesAcciones2 As Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents btnInformesAccionesImprimir As Ribbon.KryptonRibbonGroupButton
    Friend WithEvents btnInformeAccionesExportar As Ribbon.KryptonRibbonGroupButton
    Friend WithEvents krgReportePaginacion As Ribbon.KryptonRibbonGroup
    Friend WithEvents krgtInformesPaginacion1 As Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents btnInformesPaginaPrimera As Ribbon.KryptonRibbonGroupButton
    Friend WithEvents btnInformesPaginaAnterior As Ribbon.KryptonRibbonGroupButton
    Friend WithEvents btnInformesPaginaSiguiente As Ribbon.KryptonRibbonGroupButton
    Friend WithEvents krgtInformesPaginacion2 As Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents btnInformesPaginaUltima As Ribbon.KryptonRibbonGroupButton
    Friend WithEvents krgInformesVentas As Ribbon.KryptonRibbonGroup
    Friend WithEvents krgtInformesVentas1 As Ribbon.KryptonRibbonGroupTriple
    Friend WithEvents mnuInformesVentas_Ventas As Ribbon.KryptonRibbonGroupButton
    Friend WithEvents mnuInformesStock_Entradas As Ribbon.KryptonRibbonGroupButton
End Class
