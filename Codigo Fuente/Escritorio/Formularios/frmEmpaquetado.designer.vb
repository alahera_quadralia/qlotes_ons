<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmEmpaquetado
    Inherits FormularioHijo

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.lblBcodigo = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtBcodigo = New Escritorio.Quadralia.Controles.aTextBox()
        Me.lblBFechaInicio = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.dtpBFechaInicio = New Escritorio.Quadralia.Controles.aDateTimePicker()
        Me.lblobservaciones = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblFormaPago_id = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblCliente_id = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.btnBBuscar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.grpResultados = New ComponentFactory.Krypton.Toolkit.KryptonGroupBox()
        Me.dgvResultadosBuscador = New ComponentFactory.Krypton.Toolkit.KryptonDataGridView()
        Me.colResultadoCodigo = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colResultadoFecha = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.Proveedor = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.splSeparador = New ComponentFactory.Krypton.Toolkit.KryptonSplitContainer()
        Me.hdrBusqueda = New ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup()
        Me.btnOcultarBusqueda = New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup()
        Me.tblOrganizadorBusqueda = New System.Windows.Forms.TableLayoutPanel()
        Me.btnLimpiarBusqueda = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.lblBFechaFin = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.dtpBFechaFin = New Escritorio.Quadralia.Controles.aDateTimePicker()
        Me.chkBRegistrosEliminados = New ComponentFactory.Krypton.Toolkit.KryptonCheckBox()
        Me.lblBEtiqueta = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblBLoteEntrada = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtBLoteEntrada = New Escritorio.cBuscadorLoteEntrada()
        Me.txtBEtiqueta = New Escritorio.cBuscadorEtiqueta()
        Me.tabGeneral = New ComponentFactory.Krypton.Navigator.KryptonNavigator()
        Me.tbpDatos = New ComponentFactory.Krypton.Navigator.KryptonPage()
        Me.tblOrganizadorDatos = New System.Windows.Forms.TableLayoutPanel()
        Me.txtNumEtiquetas = New ComponentFactory.Krypton.Toolkit.KryptonTextBox()
        Me.KryptonLabel2 = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtPeso = New ComponentFactory.Krypton.Toolkit.KryptonTextBox()
        Me.KryptonLabel1 = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.chkReactivarRegistro = New ComponentFactory.Krypton.Toolkit.KryptonCheckBox()
        Me.lblCodigo = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtCodigo = New ComponentFactory.Krypton.Toolkit.KryptonTextBox()
        Me.lblFecha = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.dtpFecha = New ComponentFactory.Krypton.Toolkit.KryptonDateTimePicker()
        Me.hdrCabeceraEtiquetas = New ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup()
        Me.btnEtiquetasLector = New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup()
        Me.btnEtiquetasNuevo = New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup()
        Me.btnEtiquetasEliminar = New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup()
        Me.dgvEtiquetas = New ComponentFactory.Krypton.Toolkit.KryptonDataGridView()
        Me.colAlbEntrada = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.Numero_Lote = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colZonaFao = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colCantidad = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colNombreProveedor = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colEspecie = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.tbpObservaciones = New ComponentFactory.Krypton.Navigator.KryptonPage()
        Me.htmlObservaciones = New Escritorio.EditorHTML()
        Me.tbpLotesEntrada = New ComponentFactory.Krypton.Navigator.KryptonPage()
        Me.dgvLotesEntrada = New ComponentFactory.Krypton.Toolkit.KryptonDataGridView()
        Me.colFacturaCodigo = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colFacturaFecha = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colCliente = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.lblTitulo = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.grpCabecera = New ComponentFactory.Krypton.Toolkit.KryptonGroup()
        Me.epErrores = New Escritorio.Quadralia.Controles.Validacion.GestorErrores()
        Me.bgwImprimir = New Escritorio.Quadralia.Controles.Threading.cBackGroundWorker()
        Me.lblBLoteSalida = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtBLoteSAlida = New Escritorio.Quadralia.Controles.aTextBox()
        CType(Me.grpResultados, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpResultados.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpResultados.Panel.SuspendLayout()
        Me.grpResultados.SuspendLayout()
        CType(Me.dgvResultadosBuscador, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.splSeparador, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.splSeparador.Panel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.splSeparador.Panel1.SuspendLayout()
        CType(Me.splSeparador.Panel2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.splSeparador.Panel2.SuspendLayout()
        Me.splSeparador.SuspendLayout()
        CType(Me.hdrBusqueda, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.hdrBusqueda.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.hdrBusqueda.Panel.SuspendLayout()
        Me.hdrBusqueda.SuspendLayout()
        Me.tblOrganizadorBusqueda.SuspendLayout()
        CType(Me.tabGeneral, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabGeneral.SuspendLayout()
        CType(Me.tbpDatos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpDatos.SuspendLayout()
        Me.tblOrganizadorDatos.SuspendLayout()
        CType(Me.hdrCabeceraEtiquetas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.hdrCabeceraEtiquetas.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.hdrCabeceraEtiquetas.Panel.SuspendLayout()
        Me.hdrCabeceraEtiquetas.SuspendLayout()
        CType(Me.dgvEtiquetas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbpObservaciones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpObservaciones.SuspendLayout()
        CType(Me.tbpLotesEntrada, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpLotesEntrada.SuspendLayout()
        CType(Me.dgvLotesEntrada, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpCabecera, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpCabecera.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpCabecera.Panel.SuspendLayout()
        Me.grpCabecera.SuspendLayout()
        CType(Me.epErrores, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblBcodigo
        '
        Me.lblBcodigo.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblBcodigo.Location = New System.Drawing.Point(27, 31)
        Me.lblBcodigo.Name = "lblBcodigo"
        Me.lblBcodigo.Size = New System.Drawing.Size(55, 23)
        Me.lblBcodigo.TabIndex = 0
        Me.lblBcodigo.Values.Text = "Número"
        '
        'txtBcodigo
        '
        Me.txtBcodigo.AlwaysActive = False
        Me.txtBcodigo.controlarBotonBorrar = True
        Me.txtBcodigo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtBcodigo.Formato = ""
        Me.txtBcodigo.Location = New System.Drawing.Point(88, 31)
        Me.txtBcodigo.MaxLength = 48
        Me.txtBcodigo.mostrarSiempreBotonBorrar = False
        Me.txtBcodigo.Name = "txtBcodigo"
        Me.txtBcodigo.seleccionarTodo = True
        Me.txtBcodigo.Size = New System.Drawing.Size(134, 23)
        Me.txtBcodigo.TabIndex = 1
        '
        'lblBFechaInicio
        '
        Me.lblBFechaInicio.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblBFechaInicio.Location = New System.Drawing.Point(7, 60)
        Me.lblBFechaInicio.Name = "lblBFechaInicio"
        Me.lblBFechaInicio.Size = New System.Drawing.Size(75, 21)
        Me.lblBFechaInicio.TabIndex = 2
        Me.lblBFechaInicio.Values.Text = "Fecha Inicio"
        '
        'dtpBFechaInicio
        '
        Me.dtpBFechaInicio.CalendarTodayText = "Hoy:"
        Me.dtpBFechaInicio.Checked = False
        Me.dtpBFechaInicio.controlarBotonBorrar = True
        Me.dtpBFechaInicio.CustomNullText = "(Sin fecha)"
        Me.dtpBFechaInicio.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dtpBFechaInicio.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpBFechaInicio.Location = New System.Drawing.Point(88, 60)
        Me.dtpBFechaInicio.mostrarSiempreBotonBorrar = False
        Me.dtpBFechaInicio.Name = "dtpBFechaInicio"
        Me.dtpBFechaInicio.Size = New System.Drawing.Size(134, 21)
        Me.dtpBFechaInicio.TabIndex = 3
        Me.dtpBFechaInicio.TipoLimpieza = Escritorio.Quadralia.Controles.aDateTimePicker.TiposLimpieza.ValueYValueNullable
        '
        'lblobservaciones
        '
        Me.lblobservaciones.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblobservaciones.Location = New System.Drawing.Point(34, 464)
        Me.lblobservaciones.Name = "lblobservaciones"
        Me.lblobservaciones.Size = New System.Drawing.Size(88, 20)
        Me.lblobservaciones.TabIndex = 0
        Me.lblobservaciones.Values.Text = "observaciones"
        '
        'lblFormaPago_id
        '
        Me.lblFormaPago_id.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblFormaPago_id.Location = New System.Drawing.Point(34, 664)
        Me.lblFormaPago_id.Name = "lblFormaPago_id"
        Me.lblFormaPago_id.Size = New System.Drawing.Size(88, 20)
        Me.lblFormaPago_id.TabIndex = 0
        Me.lblFormaPago_id.Values.Text = "FormaPago_id"
        '
        'lblCliente_id
        '
        Me.lblCliente_id.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblCliente_id.Location = New System.Drawing.Point(34, 684)
        Me.lblCliente_id.Name = "lblCliente_id"
        Me.lblCliente_id.Size = New System.Drawing.Size(64, 20)
        Me.lblCliente_id.TabIndex = 0
        Me.lblCliente_id.Values.Text = "Cliente_id"
        '
        'btnBBuscar
        '
        Me.tblOrganizadorBusqueda.SetColumnSpan(Me.btnBBuscar, 2)
        Me.btnBBuscar.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnBBuscar.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnBBuscar.Location = New System.Drawing.Point(3, 221)
        Me.btnBBuscar.Name = "btnBBuscar"
        Me.btnBBuscar.Size = New System.Drawing.Size(219, 25)
        Me.btnBBuscar.TabIndex = 9
        Me.btnBBuscar.Values.Text = "Buscar"
        '
        'grpResultados
        '
        Me.tblOrganizadorBusqueda.SetColumnSpan(Me.grpResultados, 2)
        Me.grpResultados.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grpResultados.Location = New System.Drawing.Point(3, 252)
        Me.grpResultados.Name = "grpResultados"
        '
        'grpResultados.Panel
        '
        Me.grpResultados.Panel.Controls.Add(Me.dgvResultadosBuscador)
        Me.grpResultados.Size = New System.Drawing.Size(219, 160)
        Me.grpResultados.TabIndex = 10
        Me.grpResultados.Text = "Resultados"
        Me.grpResultados.Values.Heading = "Resultados"
        '
        'dgvResultadosBuscador
        '
        Me.dgvResultadosBuscador.AllowUserToAddRows = False
        Me.dgvResultadosBuscador.AllowUserToDeleteRows = False
        Me.dgvResultadosBuscador.AllowUserToResizeRows = False
        Me.dgvResultadosBuscador.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colResultadoCodigo, Me.colResultadoFecha, Me.Proveedor})
        Me.dgvResultadosBuscador.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvResultadosBuscador.Location = New System.Drawing.Point(0, 0)
        Me.dgvResultadosBuscador.Name = "dgvResultadosBuscador"
        Me.dgvResultadosBuscador.ReadOnly = True
        Me.dgvResultadosBuscador.RowHeadersVisible = False
        Me.dgvResultadosBuscador.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvResultadosBuscador.Size = New System.Drawing.Size(215, 136)
        Me.dgvResultadosBuscador.TabIndex = 0
        '
        'colResultadoCodigo
        '
        Me.colResultadoCodigo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colResultadoCodigo.DataPropertyName = "codigo"
        Me.colResultadoCodigo.HeaderText = "Cód."
        Me.colResultadoCodigo.Name = "colResultadoCodigo"
        Me.colResultadoCodigo.ReadOnly = True
        Me.colResultadoCodigo.Width = 61
        '
        'colResultadoFecha
        '
        Me.colResultadoFecha.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colResultadoFecha.DataPropertyName = "fecha"
        DataGridViewCellStyle1.Format = "d"
        DataGridViewCellStyle1.NullValue = Nothing
        Me.colResultadoFecha.DefaultCellStyle = DataGridViewCellStyle1
        Me.colResultadoFecha.HeaderText = "Fecha"
        Me.colResultadoFecha.Name = "colResultadoFecha"
        Me.colResultadoFecha.ReadOnly = True
        Me.colResultadoFecha.Width = 67
        '
        'Proveedor
        '
        Me.Proveedor.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Proveedor.DataPropertyName = "nombreCliente"
        Me.Proveedor.HeaderText = "Proveedor"
        Me.Proveedor.MinimumWidth = 80
        Me.Proveedor.Name = "Proveedor"
        Me.Proveedor.ReadOnly = True
        Me.Proveedor.Width = 86
        '
        'splSeparador
        '
        Me.splSeparador.Cursor = System.Windows.Forms.Cursors.Default
        Me.splSeparador.Dock = System.Windows.Forms.DockStyle.Fill
        Me.splSeparador.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.splSeparador.Location = New System.Drawing.Point(0, 57)
        Me.splSeparador.Name = "splSeparador"
        '
        'splSeparador.Panel1
        '
        Me.splSeparador.Panel1.Controls.Add(Me.hdrBusqueda)
        Me.splSeparador.Panel1MinSize = 220
        '
        'splSeparador.Panel2
        '
        Me.splSeparador.Panel2.Controls.Add(Me.tabGeneral)
        Me.splSeparador.Size = New System.Drawing.Size(881, 452)
        Me.splSeparador.SplitterDistance = 227
        Me.splSeparador.TabIndex = 1
        '
        'hdrBusqueda
        '
        Me.hdrBusqueda.AutoSize = True
        Me.hdrBusqueda.ButtonSpecs.AddRange(New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup() {Me.btnOcultarBusqueda})
        Me.hdrBusqueda.Dock = System.Windows.Forms.DockStyle.Fill
        Me.hdrBusqueda.HeaderVisibleSecondary = False
        Me.hdrBusqueda.Location = New System.Drawing.Point(0, 0)
        Me.hdrBusqueda.Name = "hdrBusqueda"
        '
        'hdrBusqueda.Panel
        '
        Me.hdrBusqueda.Panel.Controls.Add(Me.tblOrganizadorBusqueda)
        Me.hdrBusqueda.Size = New System.Drawing.Size(227, 452)
        Me.hdrBusqueda.TabIndex = 0
        Me.hdrBusqueda.ValuesPrimary.Heading = "Buscar"
        Me.hdrBusqueda.ValuesPrimary.Image = Global.Escritorio.My.Resources.Resources.Buscar_32
        '
        'btnOcultarBusqueda
        '
        Me.btnOcultarBusqueda.Type = ComponentFactory.Krypton.Toolkit.PaletteButtonSpecStyle.ArrowLeft
        Me.btnOcultarBusqueda.UniqueName = "BE70F7722CF94C60618F65819BBF010D"
        '
        'tblOrganizadorBusqueda
        '
        Me.tblOrganizadorBusqueda.BackColor = System.Drawing.Color.White
        Me.tblOrganizadorBusqueda.ColumnCount = 2
        Me.tblOrganizadorBusqueda.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblOrganizadorBusqueda.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblOrganizadorBusqueda.Controls.Add(Me.txtBLoteSAlida, 1, 6)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.lblBLoteSalida, 0, 6)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.btnLimpiarBusqueda, 0, 0)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.lblBcodigo, 0, 1)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.dtpBFechaInicio, 1, 2)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.grpResultados, 0, 9)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.btnBBuscar, 0, 8)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.lblBFechaInicio, 0, 2)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.txtBcodigo, 1, 1)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.lblBFechaFin, 0, 3)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.dtpBFechaFin, 1, 3)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.chkBRegistrosEliminados, 0, 7)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.lblBEtiqueta, 0, 4)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.lblBLoteEntrada, 0, 5)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.txtBLoteEntrada, 1, 5)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.txtBEtiqueta, 1, 4)
        Me.tblOrganizadorBusqueda.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblOrganizadorBusqueda.Location = New System.Drawing.Point(0, 0)
        Me.tblOrganizadorBusqueda.Name = "tblOrganizadorBusqueda"
        Me.tblOrganizadorBusqueda.RowCount = 10
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorBusqueda.Size = New System.Drawing.Size(225, 415)
        Me.tblOrganizadorBusqueda.TabIndex = 0
        '
        'btnLimpiarBusqueda
        '
        Me.btnLimpiarBusqueda.AutoSize = True
        Me.btnLimpiarBusqueda.ButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.ButtonSpec
        Me.tblOrganizadorBusqueda.SetColumnSpan(Me.btnLimpiarBusqueda, 2)
        Me.btnLimpiarBusqueda.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnLimpiarBusqueda.Location = New System.Drawing.Point(97, 3)
        Me.btnLimpiarBusqueda.Name = "btnLimpiarBusqueda"
        Me.btnLimpiarBusqueda.Size = New System.Drawing.Size(125, 22)
        Me.btnLimpiarBusqueda.TabIndex = 13
        Me.btnLimpiarBusqueda.Values.Image = Global.Escritorio.My.Resources.Resources.Limpiar_16
        Me.btnLimpiarBusqueda.Values.Text = "Limpiar Búsqueda"
        '
        'lblBFechaFin
        '
        Me.lblBFechaFin.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblBFechaFin.Location = New System.Drawing.Point(21, 87)
        Me.lblBFechaFin.Name = "lblBFechaFin"
        Me.lblBFechaFin.Size = New System.Drawing.Size(61, 21)
        Me.lblBFechaFin.TabIndex = 4
        Me.lblBFechaFin.Values.Text = "Fecha Fin"
        '
        'dtpBFechaFin
        '
        Me.dtpBFechaFin.CalendarTodayText = "Hoy:"
        Me.dtpBFechaFin.Checked = False
        Me.dtpBFechaFin.controlarBotonBorrar = True
        Me.dtpBFechaFin.CustomNullText = "(Sin fecha)"
        Me.dtpBFechaFin.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dtpBFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpBFechaFin.Location = New System.Drawing.Point(88, 87)
        Me.dtpBFechaFin.mostrarSiempreBotonBorrar = False
        Me.dtpBFechaFin.Name = "dtpBFechaFin"
        Me.dtpBFechaFin.Size = New System.Drawing.Size(134, 21)
        Me.dtpBFechaFin.TabIndex = 5
        Me.dtpBFechaFin.TipoLimpieza = Escritorio.Quadralia.Controles.aDateTimePicker.TiposLimpieza.ValueYValueNullable
        '
        'chkBRegistrosEliminados
        '
        Me.tblOrganizadorBusqueda.SetColumnSpan(Me.chkBRegistrosEliminados, 2)
        Me.chkBRegistrosEliminados.Dock = System.Windows.Forms.DockStyle.Fill
        Me.chkBRegistrosEliminados.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl
        Me.chkBRegistrosEliminados.Location = New System.Drawing.Point(3, 195)
        Me.chkBRegistrosEliminados.Name = "chkBRegistrosEliminados"
        Me.chkBRegistrosEliminados.Size = New System.Drawing.Size(219, 20)
        Me.chkBRegistrosEliminados.TabIndex = 8
        Me.chkBRegistrosEliminados.Text = "Incluir registros eliminados"
        Me.chkBRegistrosEliminados.Values.Text = "Incluir registros eliminados"
        '
        'lblBEtiqueta
        '
        Me.lblBEtiqueta.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblBEtiqueta.Location = New System.Drawing.Point(27, 114)
        Me.lblBEtiqueta.Name = "lblBEtiqueta"
        Me.lblBEtiqueta.Size = New System.Drawing.Size(55, 20)
        Me.lblBEtiqueta.TabIndex = 0
        Me.lblBEtiqueta.Values.Text = "Etiqueta"
        '
        'lblBLoteEntrada
        '
        Me.lblBLoteEntrada.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblBLoteEntrada.Location = New System.Drawing.Point(3, 140)
        Me.lblBLoteEntrada.Name = "lblBLoteEntrada"
        Me.lblBLoteEntrada.Size = New System.Drawing.Size(79, 20)
        Me.lblBLoteEntrada.TabIndex = 0
        Me.lblBLoteEntrada.Values.Text = "Lote Entrada"
        '
        'txtBLoteEntrada
        '
        Me.txtBLoteEntrada.DescripcionReadOnly = False
        Me.txtBLoteEntrada.DescripcionVisible = False
        Me.txtBLoteEntrada.DisplayMember = "Descripcion"
        Me.txtBLoteEntrada.DisplayText = ""
        Me.txtBLoteEntrada.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtBLoteEntrada.FormularioBusqueda = "Escritorio.frmSeleccionarLoteEntrada"
        Me.txtBLoteEntrada.Item = Nothing
        Me.txtBLoteEntrada.Location = New System.Drawing.Point(88, 140)
        Me.txtBLoteEntrada.Name = "txtBLoteEntrada"
        Me.txtBLoteEntrada.Size = New System.Drawing.Size(134, 20)
        Me.txtBLoteEntrada.TabIndex = 14
        Me.txtBLoteEntrada.UseOnlyNumbers = False
        Me.txtBLoteEntrada.UseUpperCase = True
        Me.txtBLoteEntrada.Validar = True
        Me.txtBLoteEntrada.ValueMember = "codigo"
        Me.txtBLoteEntrada.ValueText = ""
        '
        'txtBEtiqueta
        '
        Me.txtBEtiqueta.DescripcionReadOnly = False
        Me.txtBEtiqueta.DescripcionVisible = False
        Me.txtBEtiqueta.DisplayMember = "Descripcion"
        Me.txtBEtiqueta.DisplayText = ""
        Me.txtBEtiqueta.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtBEtiqueta.FormularioBusqueda = "Escritorio.frmSeleccionarEtiqueta"
        Me.txtBEtiqueta.Item = Nothing
        Me.txtBEtiqueta.Location = New System.Drawing.Point(88, 114)
        Me.txtBEtiqueta.Name = "txtBEtiqueta"
        Me.txtBEtiqueta.Size = New System.Drawing.Size(134, 20)
        Me.txtBEtiqueta.TabIndex = 15
        Me.txtBEtiqueta.UseOnlyNumbers = False
        Me.txtBEtiqueta.UseUpperCase = True
        Me.txtBEtiqueta.Validar = True
        Me.txtBEtiqueta.ValueMember = "Codigo"
        Me.txtBEtiqueta.ValueText = ""
        '
        'tabGeneral
        '
        Me.tabGeneral.AllowPageReorder = False
        Me.tabGeneral.Button.CloseButtonAction = ComponentFactory.Krypton.Navigator.CloseButtonAction.None
        Me.tabGeneral.Button.CloseButtonDisplay = ComponentFactory.Krypton.Navigator.ButtonDisplay.Hide
        Me.tabGeneral.Button.ContextButtonAction = ComponentFactory.Krypton.Navigator.ContextButtonAction.None
        Me.tabGeneral.Button.ContextButtonDisplay = ComponentFactory.Krypton.Navigator.ButtonDisplay.Hide
        Me.tabGeneral.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabGeneral.Location = New System.Drawing.Point(0, 0)
        Me.tabGeneral.Name = "tabGeneral"
        Me.tabGeneral.Pages.AddRange(New ComponentFactory.Krypton.Navigator.KryptonPage() {Me.tbpDatos, Me.tbpObservaciones, Me.tbpLotesEntrada})
        Me.tabGeneral.SelectedIndex = 0
        Me.tabGeneral.Size = New System.Drawing.Size(649, 452)
        Me.tabGeneral.TabIndex = 0
        Me.tabGeneral.Text = "KryptonNavigator1"
        '
        'tbpDatos
        '
        Me.tbpDatos.AutoHiddenSlideSize = New System.Drawing.Size(200, 200)
        Me.tbpDatos.Controls.Add(Me.tblOrganizadorDatos)
        Me.tbpDatos.Controls.Add(Me.lblCliente_id)
        Me.tbpDatos.Controls.Add(Me.lblFormaPago_id)
        Me.tbpDatos.Controls.Add(Me.lblobservaciones)
        Me.tbpDatos.Flags = 65534
        Me.tbpDatos.LastVisibleSet = True
        Me.tbpDatos.MinimumSize = New System.Drawing.Size(50, 50)
        Me.tbpDatos.Name = "tbpDatos"
        Me.tbpDatos.Size = New System.Drawing.Size(647, 425)
        Me.tbpDatos.Text = "Datos"
        Me.tbpDatos.ToolTipTitle = "Page ToolTip"
        Me.tbpDatos.UniqueName = "A7344070A3E64BA24893A75E8104A8EE"
        '
        'tblOrganizadorDatos
        '
        Me.tblOrganizadorDatos.BackColor = System.Drawing.Color.FromArgb(CType(CType(187, Byte), Integer), CType(CType(206, Byte), Integer), CType(CType(230, Byte), Integer))
        Me.tblOrganizadorDatos.ColumnCount = 6
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblOrganizadorDatos.Controls.Add(Me.txtNumEtiquetas, 5, 1)
        Me.tblOrganizadorDatos.Controls.Add(Me.KryptonLabel2, 3, 1)
        Me.tblOrganizadorDatos.Controls.Add(Me.txtPeso, 2, 1)
        Me.tblOrganizadorDatos.Controls.Add(Me.KryptonLabel1, 0, 1)
        Me.tblOrganizadorDatos.Controls.Add(Me.chkReactivarRegistro, 0, 2)
        Me.tblOrganizadorDatos.Controls.Add(Me.lblCodigo, 0, 0)
        Me.tblOrganizadorDatos.Controls.Add(Me.txtCodigo, 2, 0)
        Me.tblOrganizadorDatos.Controls.Add(Me.lblFecha, 3, 0)
        Me.tblOrganizadorDatos.Controls.Add(Me.dtpFecha, 5, 0)
        Me.tblOrganizadorDatos.Controls.Add(Me.hdrCabeceraEtiquetas, 0, 3)
        Me.tblOrganizadorDatos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblOrganizadorDatos.Location = New System.Drawing.Point(0, 0)
        Me.tblOrganizadorDatos.Name = "tblOrganizadorDatos"
        Me.tblOrganizadorDatos.RowCount = 4
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblOrganizadorDatos.Size = New System.Drawing.Size(647, 425)
        Me.tblOrganizadorDatos.TabIndex = 0
        '
        'txtNumEtiquetas
        '
        Me.txtNumEtiquetas.AlwaysActive = False
        Me.txtNumEtiquetas.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtNumEtiquetas.Enabled = False
        Me.txtNumEtiquetas.Location = New System.Drawing.Point(410, 32)
        Me.txtNumEtiquetas.MaxLength = 48
        Me.txtNumEtiquetas.MinimumSize = New System.Drawing.Size(80, 20)
        Me.txtNumEtiquetas.Name = "txtNumEtiquetas"
        Me.txtNumEtiquetas.ReadOnly = True
        Me.txtNumEtiquetas.Size = New System.Drawing.Size(234, 23)
        Me.txtNumEtiquetas.TabIndex = 3
        '
        'KryptonLabel2
        '
        Me.KryptonLabel2.Location = New System.Drawing.Point(324, 32)
        Me.KryptonLabel2.Name = "KryptonLabel2"
        Me.KryptonLabel2.Size = New System.Drawing.Size(60, 20)
        Me.KryptonLabel2.TabIndex = 2
        Me.KryptonLabel2.Values.Text = "Etiquetas"
        '
        'txtPeso
        '
        Me.txtPeso.AlwaysActive = False
        Me.txtPeso.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtPeso.Enabled = False
        Me.txtPeso.Location = New System.Drawing.Point(84, 32)
        Me.txtPeso.MaxLength = 48
        Me.txtPeso.MinimumSize = New System.Drawing.Size(80, 20)
        Me.txtPeso.Name = "txtPeso"
        Me.txtPeso.ReadOnly = True
        Me.txtPeso.Size = New System.Drawing.Size(234, 23)
        Me.txtPeso.TabIndex = 2
        '
        'KryptonLabel1
        '
        Me.KryptonLabel1.Dock = System.Windows.Forms.DockStyle.Right
        Me.KryptonLabel1.Location = New System.Drawing.Point(22, 32)
        Me.KryptonLabel1.Name = "KryptonLabel1"
        Me.KryptonLabel1.Size = New System.Drawing.Size(36, 23)
        Me.KryptonLabel1.TabIndex = 1
        Me.KryptonLabel1.Values.Text = "Peso"
        '
        'chkReactivarRegistro
        '
        Me.tblOrganizadorDatos.SetColumnSpan(Me.chkReactivarRegistro, 6)
        Me.chkReactivarRegistro.Dock = System.Windows.Forms.DockStyle.Fill
        Me.chkReactivarRegistro.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl
        Me.chkReactivarRegistro.Location = New System.Drawing.Point(3, 61)
        Me.chkReactivarRegistro.Name = "chkReactivarRegistro"
        Me.chkReactivarRegistro.Size = New System.Drawing.Size(641, 20)
        Me.chkReactivarRegistro.TabIndex = 26
        Me.chkReactivarRegistro.Text = "Reactivar el registro"
        Me.chkReactivarRegistro.Values.Text = "Reactivar el registro"
        Me.chkReactivarRegistro.Visible = False
        '
        'lblCodigo
        '
        Me.lblCodigo.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblCodigo.Location = New System.Drawing.Point(3, 3)
        Me.lblCodigo.Name = "lblCodigo"
        Me.lblCodigo.Size = New System.Drawing.Size(55, 23)
        Me.lblCodigo.TabIndex = 0
        Me.lblCodigo.Values.Text = "Número"
        '
        'txtCodigo
        '
        Me.txtCodigo.AlwaysActive = False
        Me.txtCodigo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtCodigo.Enabled = False
        Me.txtCodigo.Location = New System.Drawing.Point(84, 3)
        Me.txtCodigo.MaxLength = 48
        Me.txtCodigo.MinimumSize = New System.Drawing.Size(80, 20)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(234, 23)
        Me.txtCodigo.TabIndex = 1
        Me.txtCodigo.Text = "codigo"
        '
        'lblFecha
        '
        Me.lblFecha.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblFecha.Location = New System.Drawing.Point(324, 3)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(42, 23)
        Me.lblFecha.TabIndex = 2
        Me.lblFecha.Values.Text = "Fecha"
        '
        'dtpFecha
        '
        Me.dtpFecha.CalendarTodayDate = New Date(2020, 10, 27, 0, 0, 0, 0)
        Me.dtpFecha.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecha.Location = New System.Drawing.Point(410, 3)
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(234, 23)
        Me.dtpFecha.TabIndex = 3
        Me.dtpFecha.ValueNullable = New Date(CType(0, Long))
        '
        'hdrCabeceraEtiquetas
        '
        Me.hdrCabeceraEtiquetas.ButtonSpecs.AddRange(New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup() {Me.btnEtiquetasLector, Me.btnEtiquetasNuevo, Me.btnEtiquetasEliminar})
        Me.tblOrganizadorDatos.SetColumnSpan(Me.hdrCabeceraEtiquetas, 6)
        Me.hdrCabeceraEtiquetas.Dock = System.Windows.Forms.DockStyle.Fill
        Me.hdrCabeceraEtiquetas.HeaderVisibleSecondary = False
        Me.hdrCabeceraEtiquetas.Location = New System.Drawing.Point(3, 87)
        Me.hdrCabeceraEtiquetas.Name = "hdrCabeceraEtiquetas"
        '
        'hdrCabeceraEtiquetas.Panel
        '
        Me.hdrCabeceraEtiquetas.Panel.Controls.Add(Me.dgvEtiquetas)
        Me.hdrCabeceraEtiquetas.Size = New System.Drawing.Size(641, 335)
        Me.hdrCabeceraEtiquetas.StateCommon.Back.Color1 = System.Drawing.Color.Transparent
        Me.hdrCabeceraEtiquetas.StateCommon.Back.Color2 = System.Drawing.Color.Transparent
        Me.hdrCabeceraEtiquetas.StateCommon.HeaderPrimary.Back.Color1 = System.Drawing.Color.Transparent
        Me.hdrCabeceraEtiquetas.StateCommon.HeaderPrimary.Back.Color2 = System.Drawing.Color.Transparent
        Me.hdrCabeceraEtiquetas.TabIndex = 27
        Me.hdrCabeceraEtiquetas.ValuesPrimary.Heading = "Etiquetas"
        Me.hdrCabeceraEtiquetas.ValuesPrimary.Image = Nothing
        '
        'btnEtiquetasLector
        '
        Me.btnEtiquetasLector.Image = Global.Escritorio.My.Resources.Resources.Lector_16
        Me.btnEtiquetasLector.ToolTipBody = "Asigne las etiquetas desde un lector de código de barras"
        Me.btnEtiquetasLector.UniqueName = "9BE9D9632284423B2ABB2FD12217E154"
        '
        'btnEtiquetasNuevo
        '
        Me.btnEtiquetasNuevo.Image = Global.Escritorio.My.Resources.Resources.Nuevo_16
        Me.btnEtiquetasNuevo.ToolTipBody = "Añade una nueva etiqueta al empaquetado actual"
        Me.btnEtiquetasNuevo.UniqueName = "2610D5F6A66A4839F296F2CECC529D8B"
        '
        'btnEtiquetasEliminar
        '
        Me.btnEtiquetasEliminar.Image = Global.Escritorio.My.Resources.Resources.Eliminar_16
        Me.btnEtiquetasEliminar.ToolTipBody = "Elimina la etiqueta seleccionada del empaquetado actual"
        Me.btnEtiquetasEliminar.UniqueName = "D043D1EDBA0140AF06A17DFF7EB93779"
        '
        'dgvEtiquetas
        '
        Me.dgvEtiquetas.AllowUserToAddRows = False
        Me.dgvEtiquetas.AllowUserToDeleteRows = False
        Me.dgvEtiquetas.AllowUserToResizeRows = False
        Me.dgvEtiquetas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colAlbEntrada, Me.Numero_Lote, Me.colZonaFao, Me.colCantidad, Me.colNombreProveedor, Me.colEspecie})
        Me.dgvEtiquetas.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvEtiquetas.Location = New System.Drawing.Point(0, 0)
        Me.dgvEtiquetas.Name = "dgvEtiquetas"
        Me.dgvEtiquetas.ReadOnly = True
        Me.dgvEtiquetas.RowHeadersVisible = False
        Me.dgvEtiquetas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEtiquetas.Size = New System.Drawing.Size(639, 303)
        Me.dgvEtiquetas.TabIndex = 1
        '
        'colAlbEntrada
        '
        Me.colAlbEntrada.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colAlbEntrada.DataPropertyName = "codigo"
        Me.colAlbEntrada.HeaderText = "Código"
        Me.colAlbEntrada.Name = "colAlbEntrada"
        Me.colAlbEntrada.ReadOnly = True
        Me.colAlbEntrada.Width = 75
        '
        'Numero_Lote
        '
        Me.Numero_Lote.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.Numero_Lote.DataPropertyName = "loteSalida"
        Me.Numero_Lote.HeaderText = "Nº-Lote"
        Me.Numero_Lote.Name = "Numero_Lote"
        Me.Numero_Lote.ReadOnly = True
        Me.Numero_Lote.Width = 78
        '
        'colZonaFao
        '
        Me.colZonaFao.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colZonaFao.DataPropertyName = "fecha"
        DataGridViewCellStyle2.Format = "d"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.colZonaFao.DefaultCellStyle = DataGridViewCellStyle2
        Me.colZonaFao.HeaderText = "Fecha"
        Me.colZonaFao.Name = "colZonaFao"
        Me.colZonaFao.ReadOnly = True
        Me.colZonaFao.Width = 67
        '
        'colCantidad
        '
        Me.colCantidad.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colCantidad.DataPropertyName = "cantidad"
        DataGridViewCellStyle3.Format = "N4"
        Me.colCantidad.DefaultCellStyle = DataGridViewCellStyle3
        Me.colCantidad.HeaderText = "Ctdad."
        Me.colCantidad.Name = "colCantidad"
        Me.colCantidad.ReadOnly = True
        Me.colCantidad.Width = 71
        '
        'colNombreProveedor
        '
        Me.colNombreProveedor.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colNombreProveedor.DataPropertyName = "nombreProveedor"
        Me.colNombreProveedor.FillWeight = 100.3135!
        Me.colNombreProveedor.HeaderText = "Proveedor"
        Me.colNombreProveedor.Name = "colNombreProveedor"
        Me.colNombreProveedor.ReadOnly = True
        Me.colNombreProveedor.Width = 174
        '
        'colEspecie
        '
        Me.colEspecie.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colEspecie.DataPropertyName = "NombreEspecie"
        Me.colEspecie.FillWeight = 99.68652!
        Me.colEspecie.HeaderText = "Especie"
        Me.colEspecie.Name = "colEspecie"
        Me.colEspecie.ReadOnly = True
        Me.colEspecie.Width = 173
        '
        'tbpObservaciones
        '
        Me.tbpObservaciones.AutoHiddenSlideSize = New System.Drawing.Size(200, 200)
        Me.tbpObservaciones.Controls.Add(Me.htmlObservaciones)
        Me.tbpObservaciones.Flags = 65534
        Me.tbpObservaciones.LastVisibleSet = True
        Me.tbpObservaciones.MinimumSize = New System.Drawing.Size(50, 50)
        Me.tbpObservaciones.Name = "tbpObservaciones"
        Me.tbpObservaciones.Size = New System.Drawing.Size(647, 425)
        Me.tbpObservaciones.Text = "Observaciones"
        Me.tbpObservaciones.ToolTipTitle = "Page ToolTip"
        Me.tbpObservaciones.UniqueName = "7E9D077196D249599B9E2159EC0B7EDD"
        '
        'htmlObservaciones
        '
        Me.htmlObservaciones.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.htmlObservaciones.ConvertirImagenesABase64 = False
        Me.htmlObservaciones.Dock = System.Windows.Forms.DockStyle.Fill
        Me.htmlObservaciones.Location = New System.Drawing.Point(0, 0)
        Me.htmlObservaciones.MostrarBarrasEdicion = True
        Me.htmlObservaciones.Name = "htmlObservaciones"
        Me.htmlObservaciones.ReadOnly = False
        Me.htmlObservaciones.Size = New System.Drawing.Size(647, 425)
        Me.htmlObservaciones.TabIndex = 0
        '
        'tbpLotesEntrada
        '
        Me.tbpLotesEntrada.AutoHiddenSlideSize = New System.Drawing.Size(200, 200)
        Me.tbpLotesEntrada.Controls.Add(Me.dgvLotesEntrada)
        Me.tbpLotesEntrada.Flags = 65534
        Me.tbpLotesEntrada.LastVisibleSet = True
        Me.tbpLotesEntrada.MinimumSize = New System.Drawing.Size(50, 50)
        Me.tbpLotesEntrada.Name = "tbpLotesEntrada"
        Me.tbpLotesEntrada.Size = New System.Drawing.Size(647, 425)
        Me.tbpLotesEntrada.Text = "Albaranes Entrada"
        Me.tbpLotesEntrada.ToolTipTitle = "Page ToolTip"
        Me.tbpLotesEntrada.UniqueName = "8795A1C22875417A22AF7CEB7A67B981"
        '
        'dgvLotesEntrada
        '
        Me.dgvLotesEntrada.AllowUserToAddRows = False
        Me.dgvLotesEntrada.AllowUserToDeleteRows = False
        Me.dgvLotesEntrada.AllowUserToResizeRows = False
        Me.dgvLotesEntrada.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colFacturaCodigo, Me.colFacturaFecha, Me.colCliente})
        Me.dgvLotesEntrada.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvLotesEntrada.Location = New System.Drawing.Point(0, 0)
        Me.dgvLotesEntrada.MultiSelect = False
        Me.dgvLotesEntrada.Name = "dgvLotesEntrada"
        Me.dgvLotesEntrada.ReadOnly = True
        Me.dgvLotesEntrada.RowHeadersVisible = False
        Me.dgvLotesEntrada.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvLotesEntrada.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLotesEntrada.Size = New System.Drawing.Size(647, 425)
        Me.dgvLotesEntrada.TabIndex = 14
        '
        'colFacturaCodigo
        '
        Me.colFacturaCodigo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colFacturaCodigo.DataPropertyName = "codigo"
        Me.colFacturaCodigo.HeaderText = "Codigo"
        Me.colFacturaCodigo.MinimumWidth = 80
        Me.colFacturaCodigo.Name = "colFacturaCodigo"
        Me.colFacturaCodigo.ReadOnly = True
        Me.colFacturaCodigo.Width = 80
        '
        'colFacturaFecha
        '
        Me.colFacturaFecha.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colFacturaFecha.DataPropertyName = "fecha"
        DataGridViewCellStyle4.Format = "d"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.colFacturaFecha.DefaultCellStyle = DataGridViewCellStyle4
        Me.colFacturaFecha.HeaderText = "Fecha"
        Me.colFacturaFecha.Name = "colFacturaFecha"
        Me.colFacturaFecha.ReadOnly = True
        Me.colFacturaFecha.Width = 67
        '
        'colCliente
        '
        Me.colCliente.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colCliente.DataPropertyName = "nombreProveedor"
        Me.colCliente.HeaderText = "Proveedor"
        Me.colCliente.Name = "colCliente"
        Me.colCliente.ReadOnly = True
        Me.colCliente.Width = 499
        '
        'lblTitulo
        '
        Me.lblTitulo.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.Custom1
        Me.lblTitulo.Location = New System.Drawing.Point(5, 9)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(120, 34)
        Me.lblTitulo.TabIndex = 0
        Me.lblTitulo.Values.Image = Global.Escritorio.My.Resources.Resources.Empaquetado_32
        Me.lblTitulo.Values.Text = "Empaquetado"
        '
        'grpCabecera
        '
        Me.grpCabecera.Dock = System.Windows.Forms.DockStyle.Top
        Me.grpCabecera.GroupBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.PanelCustom1
        Me.grpCabecera.Location = New System.Drawing.Point(0, 0)
        Me.grpCabecera.Name = "grpCabecera"
        '
        'grpCabecera.Panel
        '
        Me.grpCabecera.Panel.Controls.Add(Me.lblTitulo)
        Me.grpCabecera.Size = New System.Drawing.Size(881, 57)
        Me.grpCabecera.StateCommon.Border.Color1 = System.Drawing.Color.DarkOrange
        Me.grpCabecera.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom
        Me.grpCabecera.StateCommon.Border.Width = 5
        Me.grpCabecera.TabIndex = 0
        '
        'epErrores
        '
        Me.epErrores.Alineacion = System.Windows.Forms.ErrorIconAlignment.TopLeft
        Me.epErrores.ContainerControl = Me
        '
        'bgwImprimir
        '
        Me.bgwImprimir.MostrarFormulario = True
        Me.bgwImprimir.TipoBarra = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.bgwImprimir.WorkerReportsProgress = True
        Me.bgwImprimir.WorkerSupportsCancellation = True
        '
        'lblBLoteSalida
        '
        Me.lblBLoteSalida.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblBLoteSalida.Location = New System.Drawing.Point(12, 166)
        Me.lblBLoteSalida.Name = "lblBLoteSalida"
        Me.lblBLoteSalida.Size = New System.Drawing.Size(70, 23)
        Me.lblBLoteSalida.TabIndex = 16
        Me.lblBLoteSalida.Values.Text = "Lote Salida"
        '
        'txtBLoteSAlida
        '
        Me.txtBLoteSAlida.AlwaysActive = False
        Me.txtBLoteSAlida.controlarBotonBorrar = True
        Me.txtBLoteSAlida.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtBLoteSAlida.Formato = ""
        Me.txtBLoteSAlida.Location = New System.Drawing.Point(88, 166)
        Me.txtBLoteSAlida.MaxLength = 48
        Me.txtBLoteSAlida.mostrarSiempreBotonBorrar = False
        Me.txtBLoteSAlida.Name = "txtBLoteSAlida"
        Me.txtBLoteSAlida.seleccionarTodo = True
        Me.txtBLoteSAlida.Size = New System.Drawing.Size(134, 23)
        Me.txtBLoteSAlida.TabIndex = 17
        '
        'frmEmpaquetado
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(881, 509)
        Me.Controls.Add(Me.splSeparador)
        Me.Controls.Add(Me.grpCabecera)
        Me.Name = "frmEmpaquetado"
        Me.Text = "Empaquetado"
        CType(Me.grpResultados.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpResultados.Panel.ResumeLayout(False)
        CType(Me.grpResultados, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpResultados.ResumeLayout(False)
        CType(Me.dgvResultadosBuscador, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.splSeparador.Panel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.splSeparador.Panel1.ResumeLayout(False)
        Me.splSeparador.Panel1.PerformLayout()
        CType(Me.splSeparador.Panel2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.splSeparador.Panel2.ResumeLayout(False)
        CType(Me.splSeparador, System.ComponentModel.ISupportInitialize).EndInit()
        Me.splSeparador.ResumeLayout(False)
        CType(Me.hdrBusqueda.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrBusqueda.Panel.ResumeLayout(False)
        CType(Me.hdrBusqueda, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrBusqueda.ResumeLayout(False)
        Me.tblOrganizadorBusqueda.ResumeLayout(False)
        Me.tblOrganizadorBusqueda.PerformLayout()
        CType(Me.tabGeneral, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabGeneral.ResumeLayout(False)
        CType(Me.tbpDatos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpDatos.ResumeLayout(False)
        Me.tbpDatos.PerformLayout()
        Me.tblOrganizadorDatos.ResumeLayout(False)
        Me.tblOrganizadorDatos.PerformLayout()
        CType(Me.hdrCabeceraEtiquetas.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrCabeceraEtiquetas.Panel.ResumeLayout(False)
        CType(Me.hdrCabeceraEtiquetas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrCabeceraEtiquetas.ResumeLayout(False)
        CType(Me.dgvEtiquetas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbpObservaciones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpObservaciones.ResumeLayout(False)
        CType(Me.tbpLotesEntrada, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpLotesEntrada.ResumeLayout(False)
        CType(Me.dgvLotesEntrada, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grpCabecera.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpCabecera.Panel.ResumeLayout(False)
        Me.grpCabecera.Panel.PerformLayout()
        CType(Me.grpCabecera, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpCabecera.ResumeLayout(False)
        CType(Me.epErrores, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents splSeparador As ComponentFactory.Krypton.Toolkit.KryptonSplitContainer
    Friend WithEvents hdrBusqueda As ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup
    Friend WithEvents btnOcultarBusqueda As ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup
    Friend WithEvents lblTitulo As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents grpCabecera As ComponentFactory.Krypton.Toolkit.KryptonGroup
    Friend WithEvents epErrores As Quadralia.Controles.Validacion.GestorErrores
    Friend WithEvents lblBcodigo As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtBcodigo As Quadralia.Controles.aTextBox
    Friend WithEvents lblBFechaInicio As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents dtpBFechaInicio As Quadralia.Controles.aDateTimePicker
    Friend WithEvents lblobservaciones As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblFormaPago_id As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblCliente_id As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents btnBBuscar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents grpResultados As ComponentFactory.Krypton.Toolkit.KryptonGroupBox
    Friend WithEvents dgvResultadosBuscador As ComponentFactory.Krypton.Toolkit.KryptonDataGridView
    Friend WithEvents tblOrganizadorBusqueda As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblBFechaFin As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents dtpBFechaFin As Quadralia.Controles.aDateTimePicker
    Friend WithEvents tabGeneral As ComponentFactory.Krypton.Navigator.KryptonNavigator
    Friend WithEvents tbpDatos As ComponentFactory.Krypton.Navigator.KryptonPage
    Friend WithEvents tblOrganizadorDatos As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblCodigo As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtCodigo As ComponentFactory.Krypton.Toolkit.KryptonTextBox
    Friend WithEvents lblFecha As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents dtpFecha As ComponentFactory.Krypton.Toolkit.KryptonDateTimePicker
    Friend WithEvents htmlObservaciones As Escritorio.EditorHTML
    Friend WithEvents tbpObservaciones As ComponentFactory.Krypton.Navigator.KryptonPage
    Friend WithEvents tbpLotesEntrada As ComponentFactory.Krypton.Navigator.KryptonPage
    Friend WithEvents chkBRegistrosEliminados As ComponentFactory.Krypton.Toolkit.KryptonCheckBox
    Friend WithEvents btnLimpiarBusqueda As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents colResultadoCodigo As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents colResultadoFecha As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents Proveedor As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents bgwImprimir As Escritorio.Quadralia.Controles.Threading.cBackGroundWorker
    Friend WithEvents chkReactivarRegistro As ComponentFactory.Krypton.Toolkit.KryptonCheckBox
    Friend WithEvents dgvLotesEntrada As ComponentFactory.Krypton.Toolkit.KryptonDataGridView
    Friend WithEvents colFacturaCodigo As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents colFacturaFecha As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents colCliente As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents hdrCabeceraEtiquetas As ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup
    Friend WithEvents btnEtiquetasNuevo As ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup
    Friend WithEvents btnEtiquetasEliminar As ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup
    Friend WithEvents dgvEtiquetas As ComponentFactory.Krypton.Toolkit.KryptonDataGridView
    Friend WithEvents btnEtiquetasLector As ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup
    Friend WithEvents lblBEtiqueta As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents ATextBox1 As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents lblBLoteEntrada As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtBLoteEntrada As Escritorio.cBuscadorLoteEntrada
    Friend WithEvents txtBEtiqueta As Escritorio.cBuscadorEtiqueta
    Friend WithEvents colAlbEntrada As KryptonDataGridViewTextBoxColumn
    Friend WithEvents Numero_Lote As DataGridViewTextBoxColumn
    Friend WithEvents colZonaFao As KryptonDataGridViewTextBoxColumn
    Friend WithEvents colCantidad As KryptonDataGridViewTextBoxColumn
    Friend WithEvents colNombreProveedor As KryptonDataGridViewTextBoxColumn
    Friend WithEvents colEspecie As KryptonDataGridViewTextBoxColumn
    Friend WithEvents txtPeso As KryptonTextBox
    Friend WithEvents KryptonLabel1 As KryptonLabel
    Friend WithEvents txtNumEtiquetas As KryptonTextBox
    Friend WithEvents KryptonLabel2 As KryptonLabel
    Friend WithEvents txtBLoteSAlida As Quadralia.Controles.aTextBox
    Friend WithEvents lblBLoteSalida As KryptonLabel
End Class
