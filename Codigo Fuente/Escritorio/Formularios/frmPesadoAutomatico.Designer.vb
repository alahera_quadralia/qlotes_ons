﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPesadoAutomatico
    Inherits Escritorio.FormularioHijo

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.pFondo = New ComponentFactory.Krypton.Toolkit.KryptonPanel()
        Me.tblOrganizador = New System.Windows.Forms.TableLayoutPanel()
        Me.lblPesado = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.hdrDatosLinea = New ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup()
        Me.ButtonSpecHeaderGroup1 = New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup()
        Me.tblOrganizadorDatosEntrada = New System.Windows.Forms.TableLayoutPanel()
        Me.txtDisenho = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblEntradaPesoTotal = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblEntradaNumberoAlbaran = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtEntradaNumberoAlbaran = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtEntradaPesoTotal = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblExpedidor = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtExpedidor = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblDisenho = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.hdrHistoricoEtiquetas = New ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup()
        Me.dgvHistoricoEtiquetas = New ComponentFactory.Krypton.Toolkit.KryptonDataGridView()
        Me.colEtiquetaCodigo = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colUltimasEtiquetasAlbaranEntrada = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colUltimasEtiquetasLinea = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colUltimasEtiquetasEspecie = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colUltimasEtiquetasFecha = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colUltimasEtiquetasPeso = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.lblPesoRestante = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblKgRestantes = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtPeso = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblKg = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtPesoRestante = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblAdvetenciaEstado = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        CType(Me.pFondo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pFondo.SuspendLayout()
        Me.tblOrganizador.SuspendLayout()
        CType(Me.hdrDatosLinea, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.hdrDatosLinea.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.hdrDatosLinea.Panel.SuspendLayout()
        Me.hdrDatosLinea.SuspendLayout()
        Me.tblOrganizadorDatosEntrada.SuspendLayout()
        CType(Me.hdrHistoricoEtiquetas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.hdrHistoricoEtiquetas.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.hdrHistoricoEtiquetas.Panel.SuspendLayout()
        Me.hdrHistoricoEtiquetas.SuspendLayout()
        CType(Me.dgvHistoricoEtiquetas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pFondo
        '
        Me.pFondo.Controls.Add(Me.tblOrganizador)
        Me.pFondo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pFondo.Location = New System.Drawing.Point(0, 0)
        Me.pFondo.Name = "pFondo"
        Me.pFondo.Size = New System.Drawing.Size(658, 651)
        Me.pFondo.TabIndex = 0
        '
        'tblOrganizador
        '
        Me.tblOrganizador.BackColor = System.Drawing.Color.Transparent
        Me.tblOrganizador.ColumnCount = 3
        Me.tblOrganizador.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblOrganizador.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblOrganizador.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblOrganizador.Controls.Add(Me.lblPesado, 0, 7)
        Me.tblOrganizador.Controls.Add(Me.hdrDatosLinea, 0, 5)
        Me.tblOrganizador.Controls.Add(Me.hdrHistoricoEtiquetas, 0, 7)
        Me.tblOrganizador.Controls.Add(Me.lblPesoRestante, 0, 2)
        Me.tblOrganizador.Controls.Add(Me.lblKgRestantes, 2, 2)
        Me.tblOrganizador.Controls.Add(Me.txtPeso, 0, 3)
        Me.tblOrganizador.Controls.Add(Me.lblKg, 2, 3)
        Me.tblOrganizador.Controls.Add(Me.txtPesoRestante, 1, 2)
        Me.tblOrganizador.Controls.Add(Me.lblAdvetenciaEstado, 0, 1)
        Me.tblOrganizador.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblOrganizador.Location = New System.Drawing.Point(0, 0)
        Me.tblOrganizador.Name = "tblOrganizador"
        Me.tblOrganizador.RowCount = 8
        Me.tblOrganizador.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10.0!))
        Me.tblOrganizador.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizador.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizador.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizador.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10.0!))
        Me.tblOrganizador.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizador.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8.0!))
        Me.tblOrganizador.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizador.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblOrganizador.Size = New System.Drawing.Size(658, 651)
        Me.tblOrganizador.TabIndex = 15
        '
        'lblPesado
        '
        Me.tblOrganizador.SetColumnSpan(Me.lblPesado, 3)
        Me.lblPesado.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblPesado.Location = New System.Drawing.Point(3, 375)
        Me.lblPesado.Name = "lblPesado"
        Me.lblPesado.Size = New System.Drawing.Size(652, 29)
        Me.lblPesado.StateCommon.ShortText.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPesado.TabIndex = 17
        Me.lblPesado.Values.Text = "Pesado"
        '
        'hdrDatosLinea
        '
        Me.hdrDatosLinea.ButtonSpecs.AddRange(New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup() {Me.ButtonSpecHeaderGroup1})
        Me.tblOrganizador.SetColumnSpan(Me.hdrDatosLinea, 3)
        Me.hdrDatosLinea.Dock = System.Windows.Forms.DockStyle.Fill
        Me.hdrDatosLinea.HeaderVisibleSecondary = False
        Me.hdrDatosLinea.Location = New System.Drawing.Point(3, 167)
        Me.hdrDatosLinea.Name = "hdrDatosLinea"
        '
        'hdrDatosLinea.Panel
        '
        Me.hdrDatosLinea.Panel.Controls.Add(Me.tblOrganizadorDatosEntrada)
        Me.hdrDatosLinea.Size = New System.Drawing.Size(652, 194)
        Me.hdrDatosLinea.TabIndex = 14
        Me.hdrDatosLinea.ValuesPrimary.Heading = "Datos Línea Entrada / Expedidor"
        Me.hdrDatosLinea.ValuesPrimary.Image = Global.Escritorio.My.Resources.Resources.Entrada_32
        '
        'ButtonSpecHeaderGroup1
        '
        Me.ButtonSpecHeaderGroup1.Edge = ComponentFactory.Krypton.Toolkit.PaletteRelativeEdgeAlign.Far
        Me.ButtonSpecHeaderGroup1.Image = Global.Escritorio.My.Resources.Resources.Modificar_32
        Me.ButtonSpecHeaderGroup1.Style = ComponentFactory.Krypton.Toolkit.PaletteButtonStyle.Standalone
        Me.ButtonSpecHeaderGroup1.Text = "Cambiar"
        Me.ButtonSpecHeaderGroup1.UniqueName = "4F4224389E8A44F60AAEA3215801C7F5"
        '
        'tblOrganizadorDatosEntrada
        '
        Me.tblOrganizadorDatosEntrada.BackColor = System.Drawing.Color.Transparent
        Me.tblOrganizadorDatosEntrada.ColumnCount = 2
        Me.tblOrganizadorDatosEntrada.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblOrganizadorDatosEntrada.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblOrganizadorDatosEntrada.Controls.Add(Me.txtDisenho, 0, 5)
        Me.tblOrganizadorDatosEntrada.Controls.Add(Me.lblEntradaPesoTotal, 0, 2)
        Me.tblOrganizadorDatosEntrada.Controls.Add(Me.lblEntradaNumberoAlbaran, 0, 0)
        Me.tblOrganizadorDatosEntrada.Controls.Add(Me.txtEntradaNumberoAlbaran, 1, 0)
        Me.tblOrganizadorDatosEntrada.Controls.Add(Me.txtEntradaPesoTotal, 1, 2)
        Me.tblOrganizadorDatosEntrada.Controls.Add(Me.lblExpedidor, 0, 4)
        Me.tblOrganizadorDatosEntrada.Controls.Add(Me.txtExpedidor, 1, 4)
        Me.tblOrganizadorDatosEntrada.Controls.Add(Me.lblDisenho, 0, 5)
        Me.tblOrganizadorDatosEntrada.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblOrganizadorDatosEntrada.Location = New System.Drawing.Point(0, 0)
        Me.tblOrganizadorDatosEntrada.Name = "tblOrganizadorDatosEntrada"
        Me.tblOrganizadorDatosEntrada.RowCount = 6
        Me.tblOrganizadorDatosEntrada.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatosEntrada.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatosEntrada.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatosEntrada.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorDatosEntrada.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatosEntrada.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorDatosEntrada.Size = New System.Drawing.Size(650, 149)
        Me.tblOrganizadorDatosEntrada.TabIndex = 13
        '
        'txtDisenho
        '
        Me.txtDisenho.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtDisenho.Location = New System.Drawing.Point(163, 119)
        Me.txtDisenho.Name = "txtDisenho"
        Me.txtDisenho.Size = New System.Drawing.Size(484, 27)
        Me.txtDisenho.StateCommon.ShortText.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDisenho.TabIndex = 7
        Me.txtDisenho.Values.Text = "Etiqueta"
        '
        'lblEntradaPesoTotal
        '
        Me.lblEntradaPesoTotal.Location = New System.Drawing.Point(3, 35)
        Me.lblEntradaPesoTotal.Name = "lblEntradaPesoTotal"
        Me.lblEntradaPesoTotal.Size = New System.Drawing.Size(106, 26)
        Me.lblEntradaPesoTotal.StateCommon.ShortText.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEntradaPesoTotal.TabIndex = 6
        Me.lblEntradaPesoTotal.Values.Text = "Peso Total"
        '
        'lblEntradaNumberoAlbaran
        '
        Me.lblEntradaNumberoAlbaran.Location = New System.Drawing.Point(3, 3)
        Me.lblEntradaNumberoAlbaran.Name = "lblEntradaNumberoAlbaran"
        Me.lblEntradaNumberoAlbaran.Size = New System.Drawing.Size(154, 26)
        Me.lblEntradaNumberoAlbaran.StateCommon.ShortText.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEntradaNumberoAlbaran.TabIndex = 3
        Me.lblEntradaNumberoAlbaran.Values.Text = "Albarán Entrada"
        '
        'txtEntradaNumberoAlbaran
        '
        Me.txtEntradaNumberoAlbaran.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtEntradaNumberoAlbaran.Location = New System.Drawing.Point(163, 3)
        Me.txtEntradaNumberoAlbaran.Name = "txtEntradaNumberoAlbaran"
        Me.txtEntradaNumberoAlbaran.Size = New System.Drawing.Size(484, 26)
        Me.txtEntradaNumberoAlbaran.StateCommon.ShortText.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEntradaNumberoAlbaran.TabIndex = 3
        Me.txtEntradaNumberoAlbaran.Values.Text = "E2015/000001"
        '
        'txtEntradaPesoTotal
        '
        Me.txtEntradaPesoTotal.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtEntradaPesoTotal.Location = New System.Drawing.Point(163, 35)
        Me.txtEntradaPesoTotal.Name = "txtEntradaPesoTotal"
        Me.txtEntradaPesoTotal.Size = New System.Drawing.Size(484, 26)
        Me.txtEntradaPesoTotal.StateCommon.ShortText.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEntradaPesoTotal.TabIndex = 6
        Me.txtEntradaPesoTotal.Values.Text = "300 Kg"
        '
        'lblExpedidor
        '
        Me.lblExpedidor.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblExpedidor.Location = New System.Drawing.Point(3, 87)
        Me.lblExpedidor.Name = "lblExpedidor"
        Me.lblExpedidor.Size = New System.Drawing.Size(101, 26)
        Me.lblExpedidor.StateCommon.ShortText.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExpedidor.TabIndex = 5
        Me.lblExpedidor.Values.Text = "Expedidor"
        '
        'txtExpedidor
        '
        Me.txtExpedidor.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtExpedidor.Location = New System.Drawing.Point(163, 87)
        Me.txtExpedidor.Name = "txtExpedidor"
        Me.txtExpedidor.Size = New System.Drawing.Size(484, 26)
        Me.txtExpedidor.StateCommon.ShortText.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtExpedidor.TabIndex = 5
        Me.txtExpedidor.Values.Text = "Expedidor"
        '
        'lblDisenho
        '
        Me.lblDisenho.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblDisenho.Location = New System.Drawing.Point(3, 119)
        Me.lblDisenho.Name = "lblDisenho"
        Me.lblDisenho.Size = New System.Drawing.Size(85, 26)
        Me.lblDisenho.StateCommon.ShortText.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDisenho.TabIndex = 5
        Me.lblDisenho.Values.Text = "Etiqueta"
        '
        'hdrHistoricoEtiquetas
        '
        Me.tblOrganizador.SetColumnSpan(Me.hdrHistoricoEtiquetas, 3)
        Me.hdrHistoricoEtiquetas.Dock = System.Windows.Forms.DockStyle.Fill
        Me.hdrHistoricoEtiquetas.HeaderVisibleSecondary = False
        Me.hdrHistoricoEtiquetas.Location = New System.Drawing.Point(3, 410)
        Me.hdrHistoricoEtiquetas.Name = "hdrHistoricoEtiquetas"
        '
        'hdrHistoricoEtiquetas.Panel
        '
        Me.hdrHistoricoEtiquetas.Panel.Controls.Add(Me.dgvHistoricoEtiquetas)
        Me.hdrHistoricoEtiquetas.Size = New System.Drawing.Size(652, 238)
        Me.hdrHistoricoEtiquetas.TabIndex = 15
        Me.hdrHistoricoEtiquetas.ValuesPrimary.Heading = "Últimas 20 Etiquetas Generadas Hoy"
        Me.hdrHistoricoEtiquetas.ValuesPrimary.Image = Global.Escritorio.My.Resources.Resources.Etiqueta_32
        '
        'dgvHistoricoEtiquetas
        '
        Me.dgvHistoricoEtiquetas.AllowUserToAddRows = False
        Me.dgvHistoricoEtiquetas.AllowUserToDeleteRows = False
        Me.dgvHistoricoEtiquetas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvHistoricoEtiquetas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colEtiquetaCodigo, Me.colUltimasEtiquetasAlbaranEntrada, Me.colUltimasEtiquetasLinea, Me.colUltimasEtiquetasEspecie, Me.colUltimasEtiquetasFecha, Me.colUltimasEtiquetasPeso})
        Me.dgvHistoricoEtiquetas.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvHistoricoEtiquetas.Location = New System.Drawing.Point(0, 0)
        Me.dgvHistoricoEtiquetas.Name = "dgvHistoricoEtiquetas"
        Me.dgvHistoricoEtiquetas.ReadOnly = True
        Me.dgvHistoricoEtiquetas.RowHeadersVisible = False
        Me.dgvHistoricoEtiquetas.Size = New System.Drawing.Size(650, 201)
        Me.dgvHistoricoEtiquetas.TabIndex = 10
        '
        'colEtiquetaCodigo
        '
        Me.colEtiquetaCodigo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colEtiquetaCodigo.DataPropertyName = "Codigo"
        Me.colEtiquetaCodigo.HeaderText = "Código"
        Me.colEtiquetaCodigo.Name = "colEtiquetaCodigo"
        Me.colEtiquetaCodigo.ReadOnly = True
        Me.colEtiquetaCodigo.Width = 75
        '
        'colUltimasEtiquetasAlbaranEntrada
        '
        Me.colUltimasEtiquetasAlbaranEntrada.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colUltimasEtiquetasAlbaranEntrada.DataPropertyName = "CodigoAlbaranEntrada"
        Me.colUltimasEtiquetasAlbaranEntrada.HeaderText = "Alb. Ent"
        Me.colUltimasEtiquetasAlbaranEntrada.Name = "colUltimasEtiquetasAlbaranEntrada"
        Me.colUltimasEtiquetasAlbaranEntrada.ReadOnly = True
        Me.colUltimasEtiquetasAlbaranEntrada.Width = 77
        '
        'colUltimasEtiquetasLinea
        '
        Me.colUltimasEtiquetasLinea.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colUltimasEtiquetasLinea.DataPropertyName = "CodigoLineaAlbaranEntrada"
        Me.colUltimasEtiquetasLinea.HeaderText = "Lin."
        Me.colUltimasEtiquetasLinea.Name = "colUltimasEtiquetasLinea"
        Me.colUltimasEtiquetasLinea.ReadOnly = True
        Me.colUltimasEtiquetasLinea.Width = 55
        '
        'colUltimasEtiquetasEspecie
        '
        Me.colUltimasEtiquetasEspecie.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colUltimasEtiquetasEspecie.DataPropertyName = "NombreEspecie"
        Me.colUltimasEtiquetasEspecie.HeaderText = "Especie"
        Me.colUltimasEtiquetasEspecie.Name = "colUltimasEtiquetasEspecie"
        Me.colUltimasEtiquetasEspecie.ReadOnly = True
        Me.colUltimasEtiquetasEspecie.Width = 294
        '
        'colUltimasEtiquetasFecha
        '
        Me.colUltimasEtiquetasFecha.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colUltimasEtiquetasFecha.DataPropertyName = "Fecha"
        DataGridViewCellStyle1.Format = "g"
        DataGridViewCellStyle1.NullValue = Nothing
        Me.colUltimasEtiquetasFecha.DefaultCellStyle = DataGridViewCellStyle1
        Me.colUltimasEtiquetasFecha.HeaderText = "Fecha/Hora"
        Me.colUltimasEtiquetasFecha.Name = "colUltimasEtiquetasFecha"
        Me.colUltimasEtiquetasFecha.ReadOnly = True
        Me.colUltimasEtiquetasFecha.Width = 98
        '
        'colUltimasEtiquetasPeso
        '
        Me.colUltimasEtiquetasPeso.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colUltimasEtiquetasPeso.DataPropertyName = "cantidad"
        DataGridViewCellStyle2.Format = "N3"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.colUltimasEtiquetasPeso.DefaultCellStyle = DataGridViewCellStyle2
        Me.colUltimasEtiquetasPeso.HeaderText = "Kg"
        Me.colUltimasEtiquetasPeso.Name = "colUltimasEtiquetasPeso"
        Me.colUltimasEtiquetasPeso.ReadOnly = True
        Me.colUltimasEtiquetasPeso.Width = 50
        '
        'lblPesoRestante
        '
        Me.lblPesoRestante.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblPesoRestante.Location = New System.Drawing.Point(225, 48)
        Me.lblPesoRestante.Name = "lblPesoRestante"
        Me.lblPesoRestante.Size = New System.Drawing.Size(274, 46)
        Me.lblPesoRestante.StateCommon.ShortText.Font = New System.Drawing.Font("Microsoft Sans Serif", 26.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPesoRestante.TabIndex = 7
        Me.lblPesoRestante.Values.Text = "Peso Restante:"
        '
        'lblKgRestantes
        '
        Me.lblKgRestantes.Location = New System.Drawing.Point(591, 48)
        Me.lblKgRestantes.Name = "lblKgRestantes"
        Me.lblKgRestantes.Size = New System.Drawing.Size(64, 46)
        Me.lblKgRestantes.StateCommon.ShortText.Font = New System.Drawing.Font("Microsoft Sans Serif", 26.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblKgRestantes.TabIndex = 8
        Me.lblKgRestantes.Values.Text = "Kg"
        '
        'txtPeso
        '
        Me.tblOrganizador.SetColumnSpan(Me.txtPeso, 2)
        Me.txtPeso.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtPeso.Location = New System.Drawing.Point(3, 100)
        Me.txtPeso.Name = "txtPeso"
        Me.txtPeso.Size = New System.Drawing.Size(582, 51)
        Me.txtPeso.StateCommon.Padding = New System.Windows.Forms.Padding(0, 0, 0, -10)
        Me.txtPeso.StateCommon.ShortText.Font = New System.Drawing.Font("Microsoft Sans Serif", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPeso.StateCommon.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Far
        Me.txtPeso.TabIndex = 7
        Me.txtPeso.Values.Text = "0,000"
        '
        'lblKg
        '
        Me.lblKg.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblKg.Location = New System.Drawing.Point(591, 111)
        Me.lblKg.Name = "lblKg"
        Me.lblKg.Size = New System.Drawing.Size(64, 40)
        Me.lblKg.StateCommon.Padding = New System.Windows.Forms.Padding(-1, -1, -1, -5)
        Me.lblKg.StateCommon.ShortText.Font = New System.Drawing.Font("Microsoft Sans Serif", 26.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblKg.TabIndex = 8
        Me.lblKg.Values.Text = "Kg"
        '
        'txtPesoRestante
        '
        Me.txtPesoRestante.Location = New System.Drawing.Point(505, 48)
        Me.txtPesoRestante.Name = "txtPesoRestante"
        Me.txtPesoRestante.Size = New System.Drawing.Size(80, 46)
        Me.txtPesoRestante.StateCommon.ShortText.Font = New System.Drawing.Font("Microsoft Sans Serif", 26.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPesoRestante.TabIndex = 7
        Me.txtPesoRestante.Values.Text = "300"
        '
        'lblAdvetenciaEstado
        '
        Me.tblOrganizador.SetColumnSpan(Me.lblAdvetenciaEstado, 3)
        Me.lblAdvetenciaEstado.Location = New System.Drawing.Point(3, 13)
        Me.lblAdvetenciaEstado.Name = "lblAdvetenciaEstado"
        Me.lblAdvetenciaEstado.Size = New System.Drawing.Size(584, 29)
        Me.lblAdvetenciaEstado.StateCommon.ShortText.Color1 = System.Drawing.Color.FromArgb(CType(CType(197, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(45, Byte), Integer))
        Me.lblAdvetenciaEstado.StateCommon.ShortText.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAdvetenciaEstado.TabIndex = 16
        Me.lblAdvetenciaEstado.Values.Text = "No se ha podido establecer comunicación con la báscula."
        '
        'frmPesadoAutomatico
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(658, 651)
        Me.Controls.Add(Me.pFondo)
        Me.Name = "frmPesadoAutomatico"
        Me.Text = "Generación automática de Etiquetas"
        CType(Me.pFondo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pFondo.ResumeLayout(False)
        Me.tblOrganizador.ResumeLayout(False)
        Me.tblOrganizador.PerformLayout()
        CType(Me.hdrDatosLinea.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrDatosLinea.Panel.ResumeLayout(False)
        CType(Me.hdrDatosLinea, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrDatosLinea.ResumeLayout(False)
        Me.tblOrganizadorDatosEntrada.ResumeLayout(False)
        Me.tblOrganizadorDatosEntrada.PerformLayout()
        CType(Me.hdrHistoricoEtiquetas.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrHistoricoEtiquetas.Panel.ResumeLayout(False)
        CType(Me.hdrHistoricoEtiquetas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrHistoricoEtiquetas.ResumeLayout(False)
        CType(Me.dgvHistoricoEtiquetas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pFondo As ComponentFactory.Krypton.Toolkit.KryptonPanel
    Friend WithEvents tblOrganizador As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblKgRestantes As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtPeso As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents hdrDatosLinea As ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup
    Friend WithEvents tblOrganizadorDatosEntrada As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblEntradaPesoTotal As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblEntradaNumberoAlbaran As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtEntradaNumberoAlbaran As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtEntradaPesoTotal As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents hdrHistoricoEtiquetas As ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup
    Friend WithEvents dgvHistoricoEtiquetas As ComponentFactory.Krypton.Toolkit.KryptonDataGridView
    Friend WithEvents lblPesoRestante As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblKg As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtPesoRestante As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblAdvetenciaEstado As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents ButtonSpecHeaderGroup1 As ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup
    Friend WithEvents lblExpedidor As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtExpedidor As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents colEtiquetaCodigo As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents colUltimasEtiquetasAlbaranEntrada As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents colUltimasEtiquetasLinea As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents colUltimasEtiquetasEspecie As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents colUltimasEtiquetasFecha As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents colUltimasEtiquetasPeso As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents txtDisenho As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblDisenho As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblPesado As KryptonLabel
End Class
