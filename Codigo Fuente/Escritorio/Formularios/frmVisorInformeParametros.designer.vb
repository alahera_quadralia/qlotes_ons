<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmVisorInformeParametros
    Inherits FormularioHijo

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmVisorInformeParametros))
        Me.grpCabecera = New ComponentFactory.Krypton.Toolkit.KryptonGroup()
        Me.lblTitulo = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.rptVisor = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.splSeparador = New ComponentFactory.Krypton.Toolkit.KryptonSplitContainer()
        Me.hdrParametros = New ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup()
        Me.btnOcultarParametros = New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup()
        Me.tblOrganizadorParametros = New System.Windows.Forms.TableLayoutPanel()
        Me.txtCliente = New Escritorio.cTextboxBuscador()
        Me.chkCliente = New ComponentFactory.Krypton.Toolkit.KryptonCheckButton()
        Me.txtLoteSalida = New Escritorio.Quadralia.Controles.aTextBox()
        Me.chkInicio = New ComponentFactory.Krypton.Toolkit.KryptonCheckButton()
        Me.chkFin = New ComponentFactory.Krypton.Toolkit.KryptonCheckButton()
        Me.btnRefrescarDatos = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.dtpInicio = New Escritorio.Quadralia.Controles.aDateTimePicker()
        Me.dtpFin = New Escritorio.Quadralia.Controles.aDateTimePicker()
        Me.chkEspecie = New ComponentFactory.Krypton.Toolkit.KryptonCheckButton()
        Me.txtEspecie = New Escritorio.cTextboxBuscador()
        Me.chkLoteSalida = New ComponentFactory.Krypton.Toolkit.KryptonCheckButton()
        CType(Me.grpCabecera, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpCabecera.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpCabecera.Panel.SuspendLayout()
        Me.grpCabecera.SuspendLayout()
        CType(Me.splSeparador, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.splSeparador.Panel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.splSeparador.Panel1.SuspendLayout()
        CType(Me.splSeparador.Panel2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.splSeparador.Panel2.SuspendLayout()
        Me.splSeparador.SuspendLayout()
        CType(Me.hdrParametros, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.hdrParametros.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.hdrParametros.Panel.SuspendLayout()
        Me.hdrParametros.SuspendLayout()
        Me.tblOrganizadorParametros.SuspendLayout()
        Me.SuspendLayout()
        '
        'grpCabecera
        '
        resources.ApplyResources(Me.grpCabecera, "grpCabecera")
        Me.grpCabecera.GroupBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.PanelCustom1
        Me.grpCabecera.Name = "grpCabecera"
        '
        'grpCabecera.Panel
        '
        Me.grpCabecera.Panel.Controls.Add(Me.lblTitulo)
        Me.grpCabecera.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(CType(CType(154, Byte), Integer), CType(CType(156, Byte), Integer), CType(CType(177, Byte), Integer))
        Me.grpCabecera.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom
        Me.grpCabecera.StateCommon.Border.Width = 5
        '
        'lblTitulo
        '
        Me.lblTitulo.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.Custom1
        resources.ApplyResources(Me.lblTitulo, "lblTitulo")
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Values.Image = Global.Escritorio.My.Resources.Resources.Informe_32
        Me.lblTitulo.Values.Text = resources.GetString("lblTitulo.Values.Text")
        '
        'rptVisor
        '
        Me.rptVisor.ActiveViewIndex = -1
        Me.rptVisor.AutoValidate = System.Windows.Forms.AutoValidate.Disable
        Me.rptVisor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.rptVisor.Cursor = System.Windows.Forms.Cursors.Default
        resources.ApplyResources(Me.rptVisor, "rptVisor")
        Me.rptVisor.EnableToolTips = False
        Me.rptVisor.Name = "rptVisor"
        '
        'splSeparador
        '
        Me.splSeparador.Cursor = System.Windows.Forms.Cursors.Default
        resources.ApplyResources(Me.splSeparador, "splSeparador")
        Me.splSeparador.Name = "splSeparador"
        '
        'splSeparador.Panel1
        '
        Me.splSeparador.Panel1.Controls.Add(Me.hdrParametros)
        '
        'splSeparador.Panel2
        '
        Me.splSeparador.Panel2.Controls.Add(Me.rptVisor)
        '
        'hdrParametros
        '
        Me.hdrParametros.ButtonSpecs.AddRange(New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup() {Me.btnOcultarParametros})
        resources.ApplyResources(Me.hdrParametros, "hdrParametros")
        Me.hdrParametros.HeaderVisibleSecondary = False
        Me.hdrParametros.Name = "hdrParametros"
        '
        'hdrParametros.Panel
        '
        Me.hdrParametros.Panel.Controls.Add(Me.tblOrganizadorParametros)
        Me.hdrParametros.ValuesPrimary.Heading = resources.GetString("hdrParametros.ValuesPrimary.Heading")
        Me.hdrParametros.ValuesPrimary.Image = Global.Escritorio.My.Resources.Resources.Paremetros_16
        '
        'btnOcultarParametros
        '
        resources.ApplyResources(Me.btnOcultarParametros, "btnOcultarParametros")
        Me.btnOcultarParametros.UniqueName = "CBBA734E50AC45051DABD63DC98F5901"
        '
        'tblOrganizadorParametros
        '
        Me.tblOrganizadorParametros.BackColor = System.Drawing.Color.Transparent
        resources.ApplyResources(Me.tblOrganizadorParametros, "tblOrganizadorParametros")
        Me.tblOrganizadorParametros.Controls.Add(Me.txtCliente, 1, 4)
        Me.tblOrganizadorParametros.Controls.Add(Me.chkCliente, 0, 4)
        Me.tblOrganizadorParametros.Controls.Add(Me.txtLoteSalida, 1, 3)
        Me.tblOrganizadorParametros.Controls.Add(Me.chkInicio, 0, 0)
        Me.tblOrganizadorParametros.Controls.Add(Me.chkFin, 0, 1)
        Me.tblOrganizadorParametros.Controls.Add(Me.btnRefrescarDatos, 0, 18)
        Me.tblOrganizadorParametros.Controls.Add(Me.dtpInicio, 1, 0)
        Me.tblOrganizadorParametros.Controls.Add(Me.dtpFin, 1, 1)
        Me.tblOrganizadorParametros.Controls.Add(Me.chkEspecie, 0, 2)
        Me.tblOrganizadorParametros.Controls.Add(Me.txtEspecie, 1, 2)
        Me.tblOrganizadorParametros.Controls.Add(Me.chkLoteSalida, 0, 3)
        Me.tblOrganizadorParametros.Name = "tblOrganizadorParametros"
        '
        'txtCliente
        '
        resources.ApplyResources(Me.txtCliente, "txtCliente")
        Me.txtCliente.DescripcionReadOnly = False
        Me.txtCliente.DescripcionVisible = False
        Me.txtCliente.DisplayMember = "nombreComercial"
        Me.txtCliente.DisplayText = ""
        Me.txtCliente.FormularioBusqueda = "Escritorio.frmSeleccionarCliente"
        Me.txtCliente.Item = Nothing
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.UseOnlyNumbers = False
        Me.txtCliente.UseUpperCase = True
        Me.txtCliente.Validar = False
        Me.txtCliente.ValueMember = "codigo"
        Me.txtCliente.ValueText = ""
        '
        'chkCliente
        '
        resources.ApplyResources(Me.chkCliente, "chkCliente")
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Values.Text = resources.GetString("chkCliente.Values.Text")
        '
        'txtLoteSalida
        '
        Me.txtLoteSalida.AlwaysActive = False
        Me.txtLoteSalida.controlarBotonBorrar = True
        resources.ApplyResources(Me.txtLoteSalida, "txtLoteSalida")
        Me.txtLoteSalida.Formato = ""
        Me.txtLoteSalida.mostrarSiempreBotonBorrar = False
        Me.txtLoteSalida.Name = "txtLoteSalida"
        Me.txtLoteSalida.seleccionarTodo = True
        '
        'chkInicio
        '
        resources.ApplyResources(Me.chkInicio, "chkInicio")
        Me.chkInicio.Name = "chkInicio"
        Me.chkInicio.Values.Text = resources.GetString("chkInicio.Values.Text")
        '
        'chkFin
        '
        resources.ApplyResources(Me.chkFin, "chkFin")
        Me.chkFin.Name = "chkFin"
        Me.chkFin.Values.Text = resources.GetString("chkFin.Values.Text")
        '
        'btnRefrescarDatos
        '
        Me.tblOrganizadorParametros.SetColumnSpan(Me.btnRefrescarDatos, 2)
        resources.ApplyResources(Me.btnRefrescarDatos, "btnRefrescarDatos")
        Me.btnRefrescarDatos.Name = "btnRefrescarDatos"
        Me.btnRefrescarDatos.Values.Text = resources.GetString("btnRefrescarDatos.Values.Text")
        '
        'dtpInicio
        '
        resources.ApplyResources(Me.dtpInicio, "dtpInicio")
        Me.dtpInicio.CalendarTodayDate = New Date(2020, 10, 27, 0, 0, 0, 0)
        Me.dtpInicio.controlarBotonBorrar = True
        Me.dtpInicio.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpInicio.mostrarSiempreBotonBorrar = False
        Me.dtpInicio.Name = "dtpInicio"
        Me.dtpInicio.TipoLimpieza = Escritorio.Quadralia.Controles.aDateTimePicker.TiposLimpieza.ValueYValueNullable
        '
        'dtpFin
        '
        resources.ApplyResources(Me.dtpFin, "dtpFin")
        Me.dtpFin.CalendarTodayDate = New Date(2020, 10, 27, 0, 0, 0, 0)
        Me.dtpFin.controlarBotonBorrar = True
        Me.dtpFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFin.mostrarSiempreBotonBorrar = False
        Me.dtpFin.Name = "dtpFin"
        Me.dtpFin.TipoLimpieza = Escritorio.Quadralia.Controles.aDateTimePicker.TiposLimpieza.ValueYValueNullable
        '
        'chkEspecie
        '
        resources.ApplyResources(Me.chkEspecie, "chkEspecie")
        Me.chkEspecie.Name = "chkEspecie"
        Me.chkEspecie.Values.Text = resources.GetString("chkEspecie.Values.Text")
        '
        'txtEspecie
        '
        resources.ApplyResources(Me.txtEspecie, "txtEspecie")
        Me.txtEspecie.DescripcionReadOnly = False
        Me.txtEspecie.DescripcionVisible = False
        Me.txtEspecie.DisplayMember = "EspecieNombreCodigo"
        Me.txtEspecie.DisplayText = ""
        Me.txtEspecie.FormularioBusqueda = "Escritorio.frmSeleccionarEspecies"
        Me.txtEspecie.Item = Nothing
        Me.txtEspecie.Name = "txtEspecie"
        Me.txtEspecie.UseOnlyNumbers = False
        Me.txtEspecie.UseUpperCase = True
        Me.txtEspecie.Validar = False
        Me.txtEspecie.ValueMember = "alfa3"
        Me.txtEspecie.ValueText = ""
        '
        'chkLoteSalida
        '
        resources.ApplyResources(Me.chkLoteSalida, "chkLoteSalida")
        Me.chkLoteSalida.Name = "chkLoteSalida"
        Me.chkLoteSalida.Values.Text = resources.GetString("chkLoteSalida.Values.Text")
        '
        'frmVisorInformeParametros
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.splSeparador)
        Me.Controls.Add(Me.grpCabecera)
        Me.Name = "frmVisorInformeParametros"
        CType(Me.grpCabecera.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpCabecera.Panel.ResumeLayout(False)
        Me.grpCabecera.Panel.PerformLayout()
        CType(Me.grpCabecera, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpCabecera.ResumeLayout(False)
        CType(Me.splSeparador.Panel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.splSeparador.Panel1.ResumeLayout(False)
        CType(Me.splSeparador.Panel2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.splSeparador.Panel2.ResumeLayout(False)
        CType(Me.splSeparador, System.ComponentModel.ISupportInitialize).EndInit()
        Me.splSeparador.ResumeLayout(False)
        CType(Me.hdrParametros.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrParametros.Panel.ResumeLayout(False)
        CType(Me.hdrParametros, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrParametros.ResumeLayout(False)
        Me.tblOrganizadorParametros.ResumeLayout(False)
        Me.tblOrganizadorParametros.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
    Private WithEvents rptVisor As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Private WithEvents grpCabecera As ComponentFactory.Krypton.Toolkit.KryptonGroup
    Private WithEvents splSeparador As ComponentFactory.Krypton.Toolkit.KryptonSplitContainer
    Private WithEvents hdrParametros As ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup
    Friend WithEvents btnOcultarParametros As ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup
    Friend WithEvents tblOrganizadorParametros As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents chkInicio As ComponentFactory.Krypton.Toolkit.KryptonCheckButton
    Friend WithEvents chkFin As ComponentFactory.Krypton.Toolkit.KryptonCheckButton
    Friend WithEvents btnRefrescarDatos As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents dtpInicio As Quadralia.Controles.aDateTimePicker
    Friend WithEvents dtpFin As Quadralia.Controles.aDateTimePicker
    Public WithEvents lblTitulo As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents chkEspecie As ComponentFactory.Krypton.Toolkit.KryptonCheckButton
    Friend WithEvents chkLoteSalida As ComponentFactory.Krypton.Toolkit.KryptonCheckButton
    Friend WithEvents txtEspecie As cTextboxBuscador
    Friend WithEvents txtLoteSalida As Quadralia.Controles.aTextBox
    Friend WithEvents txtCliente As cTextboxBuscador
    Friend WithEvents chkCliente As KryptonCheckButton
End Class
