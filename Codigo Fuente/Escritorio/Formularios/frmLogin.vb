﻿Imports System.Runtime.InteropServices

Public Class frmLogin
#Region " DECLARACIONES "
    Private _Contexto As Entidades = Nothing
    Private _modoOffline As Boolean = False
    Private Const HASH_PASSWORD_MAESTRO As String = "0247798be13651177e6b740b2bc78ef6"
#End Region
#Region " PROPIEDADES "
    Public ReadOnly Property Contexto As Entidades
        Get
            If _Contexto Is Nothing Then
                _Contexto = cDAL.Instancia.getContext()
            End If

            Return _Contexto
        End Get
    End Property
#End Region
    ''' <summary>
    ''' Lanza la rutina de validación de usuario
    ''' </summary>
    Private Sub Aceptar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Me.Cursor = Cursors.WaitCursor
        If _modoOffline Then
#If DEBUG Then
            If True Then
#Else
            If String.Equals(Quadralia.Seguridad.Criptografia.EncriptarEnMD5(txtClave.Text), HASH_PASSWORD_MAESTRO, StringComparison.OrdinalIgnoreCase) Then
#End If
                Quadralia.Aplicacion.UsuarioConectado = New Usuario()
                Quadralia.Aplicacion.UsuarioConectado.Id = 0
                Me.Cursor = Cursors.Default
                Me.DialogResult = Windows.Forms.DialogResult.OK
                Me.Close()
            Else ' Contraseña incorrecta
                Quadralia.Aplicacion.MostrarMessageBox("La contraseña introducida no es correcta", "Autenticación fallida", MessageBoxButtons.OK, MessageBoxIcon.Error)
                txtClave.Clear()
                txtClave.Focus()
            End If
        Else ' MODO ONLINE
            ' Compruebo el usuario
            Dim Hash As String = Quadralia.Seguridad.Criptografia.EncriptarEnMD5(txtClave.Text)
            Dim Usu = (From us In Contexto.Usuarios _
                           Where us.usuario.Equals(txtUsuario.Text, StringComparison.OrdinalIgnoreCase) _
                           AndAlso (us.clave.Equals(Hash, StringComparison.OrdinalIgnoreCase) OrElse (us.resetClave)) _
                           AndAlso Not us.fechaBaja.HasValue _
                           Select us).FirstOrDefault
            'Dim Usu = (From us In Contexto.Usuarios _
            '           Where us.id = 1
            '           Select us).FirstOrDefault

            If Usu Is Nothing Then ' Datos incorrectos
                Quadralia.Aplicacion.MostrarMessageBox("Los datos introducidos no son correctos. Por favor, verifíquelos", "Autenticación fallida", MessageBoxButtons.OK, MessageBoxIcon.Error)
                txtUsuario.Clear()
                txtClave.Clear()
                txtUsuario.Focus()
            ElseIf Not Usu.Activo Then ' Usuario desactivado
                Quadralia.Aplicacion.MostrarMessageBox("El usuario introducido se encuentra actualmente desactivado y no puede iniciar sesión en el sistema", "Usuario desactivado", MessageBoxButtons.OK, MessageBoxIcon.Error)
                txtUsuario.Clear()
                txtClave.Clear()
                txtUsuario.Focus()
            Else ' Autenticación correcta                
                Quadralia.Aplicacion.UsuarioConectado = Usu

                If Usu.resetClave Then ' ¿Cambio Clave?
                    Dim VentanaCambio As frmCambioClave = Quadralia.Aplicacion.AbrirFormulario(Nothing, "Escritorio.frmCambioClave", Quadralia.Aplicacion.Permisos.USUARIO_AUTENTICADO, My.Resources.Contrasenha_16, False)

                    VentanaCambio.Tipo = frmCambioClave.TipoFormularo.ResetClave
                    VentanaCambio.StartPosition = FormStartPosition.CenterScreen

                    If VentanaCambio.ShowDialog() = Windows.Forms.DialogResult.OK Then
                        Usu.resetClave = False
                        Contexto.SaveChanges()
                    End If
                End If

                Me.Cursor = Cursors.Default
                Me.DialogResult = Windows.Forms.DialogResult.OK
                Me.Close()
            End If
        End If

        Me.Cursor = Cursors.Default
    End Sub

    ''' <summary>
    ''' Cancela el inicio de sesión
    ''' </summary>
    Private Sub Cancelar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    ''' <summary>
    ''' Muestra el formulario de acorde ha si existe conexión con la base de datos o no
    ''' </summary>
    Private Sub Inicio(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Quadralia.Aplicacion.ConexionBBDD Then
            Dim numeroUsuarios As Integer = 0
            numeroUsuarios = (From usuario In Contexto.Usuarios Select usuario).Count()

            ' Si no tengo usuarios activo el modo offline
            _modoOffline = (numeroUsuarios = 0)
        Else
            _modoOffline = True
        End If

        If _modoOffline Then
            txtUsuario.Text = "Administrador local"
            txtUsuario.Enabled = False
            txtClave.Focus()
        End If
    End Sub
End Class