Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.IO
Imports System.Linq
Imports System.Text
Imports System.Data.EntityClient

Public Class frmVisorInformeParametros
    Inherits FormularioHijo
    Implements IVisorInforme

#Region " DECLARACIONES "
    Private _TipoInforme As Informe = Informe.InformeStock
    Private _Reporte As ReportDocument = Nothing
    Private _ReporteLimpio As ReportDocument = Nothing
    Private _dsDatos As DataSet = Nothing
    Private _Contexto As Entidades = Nothing
    Private _MostrarLogos As Boolean = True
    Private _MostrarTelefonos As Boolean = False
    Private _MostrarNombre As Boolean = False
    Private _MostrarAtencion As Boolean = False
    Private _CambioControlado As Boolean = False
    Private _NumeroPaginas As Integer = 0
    Private _MostrarBuscador As Boolean = True
#End Region
#Region " ENUMERADOS "
    Public Enum Informe
        InformeStock
        InformeEntradas
        InformeVentas
    End Enum
#End Region
#Region " PROPIEDADES "
    Public ReadOnly Property Contexto As Entidades
        Get
            If _Contexto Is Nothing Then _Contexto = cDAL.Instancia.getContext
            Return _Contexto
        End Get
    End Property

    Public Property Datos As IEnumerable
    Public Property MostrarBuscador As Boolean
        Get
            Return _MostrarBuscador
        End Get
        Set(ByVal value As Boolean)
            _MostrarBuscador = value
            splSeparador.Panel1Collapsed = Not _MostrarBuscador
        End Set
    End Property

    Public Property numeroCopias As Integer = -1

    Public Property TipoInforme As Informe
        Get
            Return _TipoInforme
        End Get
        Set(ByVal value As Informe)
            _TipoInforme = value

            ' Oculto los par�metros
            chkInicio.Visible = True
            dtpInicio.Visible = True

            chkFin.Visible = True
            dtpFin.Visible = True



            chkEspecie.Visible = False
            txtEspecie.Visible = False

            chkLoteSalida.Visible = False
            txtLoteSalida.Visible = False

            chkCliente.Visible = False
            txtCliente.Visible = False


            rptVisor.EnableDrillDown = True

            Me.Text &= "Informe"

            ' Tipo de informe
            Select Case value

                Case Informe.InformeStock
                    chkEspecie.Visible = True
                    txtEspecie.Visible = True

                    chkLoteSalida.Visible = True
                    txtLoteSalida.Visible = True

                    Me.Text &= " - Stock"
                    _Reporte = New rptEmpaquetadosPendientes
                Case Informe.InformeEntradas
                    chkEspecie.Visible = True
                    txtEspecie.Visible = True



                    Me.Text &= " - Entradas"
                    _Reporte = New rptListadoEntradas
                Case Informe.InformeVentas
                    chkEspecie.Visible = True
                    txtEspecie.Visible = True

                    chkLoteSalida.Visible = True
                    txtLoteSalida.Visible = True

                    chkCliente.Visible = True
                    txtCliente.Visible = True

                    Me.Text &= " - Ventas"
                    _Reporte = New rptInformeVentas
            End Select

            _ReporteLimpio = _Reporte
        End Set
    End Property
#End Region
#Region " CONTROL DE BOTONES "
    Private Sub ControlarBotones()
        ControlarRibbon()
    End Sub

    ''' <summary>
    ''' Controla el cambio de botones del formulario principal
    ''' </summary>
    Private Sub ControlarRibbon()
        If Me.ParentForm Is Nothing Then Exit Sub
        If Not TypeOf Me.ParentForm Is frmPrincipal Then Exit Sub

        _CambioControlado = True

        With DirectCast(Me.ParentForm, frmPrincipal)
            _CambioControlado = True
            .btnInformesAccionesVerOcultarLogos.Visible = True
            .btnInformesAccionesVerOcultarLogos.Checked = _MostrarLogos

            .btnInformesAccionesCerrar.Visible = True
            .btnInformeAccionesExportar.Visible = _Reporte IsNot Nothing
            .btnInformesAccionesImprimir.Visible = _Reporte IsNot Nothing

            .btnInformesPaginaAnterior.Enabled = _Reporte IsNot Nothing AndAlso rptVisor.GetCurrentPageNumber > 1
            .btnInformesPaginaPrimera.Enabled = _Reporte IsNot Nothing AndAlso rptVisor.GetCurrentPageNumber > 1
            .btnInformesPaginaSiguiente.Enabled = _Reporte IsNot Nothing AndAlso rptVisor.GetCurrentPageNumber < _NumeroPaginas
            .btnInformesPaginaUltima.Enabled = _Reporte IsNot Nothing AndAlso rptVisor.GetCurrentPageNumber < _NumeroPaginas
            _CambioControlado = False
        End With

        _CambioControlado = False
    End Sub
#End Region
#Region " CONSTRUCTORES "
    Public Sub New()
        InitializeComponent()
    End Sub

    Public Sub New(ByVal tipoInforme As Informe)
        MyClass.New()
        Me.TipoInforme = tipoInforme
    End Sub
#End Region
#Region " RUTINA DE INICIO / FIN "
    ''' <summary>
    ''' Rutina de inicio del formulario
    ''' </summary>
    Private Sub Inicio(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ActivarParametros(Nothing, Nothing)
        ControlarRibbon()

        CargarDatosMaestros()
    End Sub

    ''' <summary>
    ''' Carga los combos con los datos de la BBDD
    ''' </summary>
    Private Sub CargarDatosMaestros()

    End Sub
#End Region
#Region " REFRESCAR DATOS "
    ''' <summary>
    ''' Se genera un dataset a trav�s de una consulta y se asigna al informe cargado en el visor
    ''' </summary>
    Public Sub RefrescarDatos() Handles btnRefrescarDatos.Click
        Try
            Select Case TipoInforme


                Case Informe.InformeStock
                    _dsDatos = New dsEmpaquetadosPendientes
                    Dim Registros As List(Of Etiqueta) = ObtenerDatos(TipoInforme)

                    ' Traspaso los datos
                    If Registros IsNot Nothing AndAlso Registros.Count > 0 Then

                        For Each etq As Etiqueta In Registros
                            Dim Fila As dsEmpaquetadosPendientes.EtiquetaRow = _dsDatos.Tables("Etiqueta").NewRow

                            With Fila
                                .id = etq.id
                                .codigo = etq.Codigo
                                .fecha = etq.fecha
                                .CodigoEmpaquetado = IIf(String.IsNullOrEmpty(etq.CodigoEmpaquetado), "Sin Empaquetar", etq.CodigoEmpaquetado)
                                .CodigoLote = etq.LoteSalida
                                .especie = etq.LoteEntradaLinea.nombreEspecie

                                .kilos = etq.cantidad
                            End With

                            _dsDatos.Tables("Etiqueta").Rows.Add(Fila)
                        Next
                    End If


                Case Informe.InformeEntradas
                    _dsDatos = New dsEntradas
                    Dim Registros As List(Of LoteEntradalinea) = ObtenerDatos(TipoInforme)

                    ' Traspaso los datos
                    If Registros IsNot Nothing AndAlso Registros.Count > 0 Then

                        For Each lin As LoteEntradalinea In Registros
                            Dim Fila As dsEntradas.dsDatosRow = _dsDatos.Tables("dsDatos").NewRow

                            With Fila
                                .codigoAlbaran = lin.codigoAlbaranEntrada
                                .fecha = lin.LoteEntrada.fecha.ToShortDateString
                                .cantidad = lin.cantidad
                                .numEtiquetas = lin.Etiquetas.Count
                                .pesoEtiquetado = lin.Etiquetas.Sum(Function(x) x.cantidad)
                            End With

                            _dsDatos.Tables("dsDatos").Rows.Add(Fila)
                        Next
                    End If
                Case Informe.InformeVentas
                    _dsDatos = New dsVentas
                    Dim Registros As List(Of Etiqueta) = ObtenerDatos(TipoInforme)

                    ' Traspaso los datos
                    If Registros IsNot Nothing AndAlso Registros.Count > 0 Then


                        For Each etq As Etiqueta In Registros
                                Dim Fila As dsVentas.EtiquetaRow = _dsDatos.Tables("Etiqueta").NewRow

                                With Fila
                                .idSalida = etq.id
                                .codigo = etq.Codigo
                                    .fecha = etq.fecha
                                    .CodigoEmpaquetado = etq.CodigoEmpaquetado
                                    .CodigoLote = etq.LoteSalida
                                .especie = etq.LoteEntradaLinea.nombreEspecie
                                .codigoAlbaran = etq.Empaquetado.LoteSalida.codigo
                                .fechaAlbaran = etq.Empaquetado.LoteSalida.fecha
                                .clienteAlbaran = etq.Empaquetado.LoteSalida.nombreCliente

                                .kilos = etq.cantidad
                                End With

                                _dsDatos.Tables("Etiqueta").Rows.Add(Fila)
                            Next


                    End If

            End Select

            ' Nuevo informe
            AsignarOrigenDatos(_dsDatos)
            AsignarParametros()
            rptVisor.ReportSource = _Reporte
            rptVisor.Refresh()

            ' Obtengo el n�mero de paginas
            rptVisor.ShowLastPage()
            _NumeroPaginas = rptVisor.GetCurrentPageNumber
            rptVisor.ShowFirstPage()
        Catch ex As Exception
            Quadralia.Aplicacion.InformarError(ex)
            _NumeroPaginas = 0
        Finally
            ControlarRibbon()
        End Try
    End Sub

    ''' <summary>
    ''' Se asigna un dataset al informe cargado en el visor
    ''' </summary>
    Private Sub AsignarOrigenDatos(ByVal dsDatos As DataSet)
        AsignarOrigenDatos(_Reporte, dsDatos)
    End Sub

    ''' <summary>
    ''' Se asigna un origen de datos a un informe
    ''' </summary>
    Private Sub AsignarOrigenDatos(ByVal informe As ReportDocument, ByVal dsDatos As DataSet)

        ' Now assign the dataset to all tables in the main report
        For Each tabla As CrystalDecisions.CrystalReports.Engine.Table In informe.Database.Tables
            tabla.SetDataSource(dsDatos)
        Next

        ' Now loop through all the sections and its objects to do the same for the subreports
        For Each seccion In informe.ReportDefinition.Sections
            ' In each section we need to loop through all the reporting objects
            For Each Objeto In seccion.ReportObjects
                If Objeto.Kind = ReportObjectKind.SubreportObject Then
                    Dim subInforme As SubreportObject = DirectCast(Objeto, SubreportObject)
                    Dim subDocumento As ReportDocument = subInforme.OpenSubreport(subInforme.SubreportName)

                    AsignarOrigenDatos(subDocumento, dsDatos)
                End If
            Next
        Next
    End Sub

    ''' <summary>
    ''' Asigna los par�metros al informe    
    ''' </summary>
    Private Sub AsignarParametros()
        Dim Parametros As New Dictionary(Of String, Object)

        Parametros.Add("Inicio", Nothing)
        Parametros.Add("Fin", Nothing)
        Parametros.Add("Especie", Nothing)
        Parametros.Add("CodigoLote", Nothing)
        Parametros.Add("Cliente", Nothing)
        Parametros.Add("MostrarLogos", _MostrarLogos)

        If chkInicio.Visible AndAlso chkInicio.Checked AndAlso Not IsDBNull(dtpInicio.ValueNullable) Then Parametros("Inicio") = dtpInicio.Value.Date
        If chkFin.Visible AndAlso chkFin.Checked AndAlso Not IsDBNull(dtpFin.ValueNullable) Then Parametros("Fin") = dtpFin.Value.Date
        If chkCliente.Visible AndAlso chkCliente.Checked AndAlso Not String.IsNullOrEmpty(txtCliente.ValueText) Then Parametros("Cliente") = txtCliente.DisplayText
        If chkEspecie.Visible AndAlso chkEspecie.Checked AndAlso Not String.IsNullOrEmpty(txtEspecie.ValueText) Then Parametros("Especie") = txtEspecie.DisplayText
        If chkLoteSalida.Visible AndAlso chkLoteSalida.Checked AndAlso Not String.IsNullOrEmpty(txtLoteSalida.Text) Then Parametros("CodigoLote") = txtLoteSalida.Text




        For Each Param In Parametros
            EstablecerParametro(Param.Key, Param.Value)
        Next
    End Sub

    ''' <summary>
    ''' Estable el valor de un parametro en el informe solicitado
    ''' </summary>
    Private Sub EstablecerParametro(ByVal nombreParametro As String, ByVal valor As Object)
        Dim parametro As CrystalDecisions.Shared.ParameterField

        Try
            parametro = _Reporte.ParameterFields.Item(nombreParametro)
            parametro.CurrentValues.Clear()

            Dim ValorParametro As New CrystalDecisions.Shared.ParameterDiscreteValue()

            parametro.CurrentValues.IsNoValue = valor Is Nothing
            ValorParametro.Value = valor
            parametro.CurrentValues.Add(ValorParametro)
        Catch ex As Exception
        End Try
    End Sub

    Public Function ObtenerDatos(ByVal Tipo As Integer, Optional ByRef Aux As Object = Nothing) As IEnumerable
        If Datos IsNot Nothing Then Return Datos

        Select Case TipoInforme



            Case Informe.InformeStock
                Dim FechaInicio As Nullable(Of DateTime) = Nothing
                Dim FechaFin As Nullable(Of DateTime) = Nothing
                Dim Especie As String = String.Empty
                Dim CodigoLote As String = String.Empty

                If chkInicio.Checked AndAlso Not IsDBNull(dtpInicio.ValueNullable) Then FechaInicio = dtpInicio.Value.Date + New TimeSpan(0, 0, 0)
                If chkFin.Checked AndAlso Not IsDBNull(dtpFin.ValueNullable) Then FechaFin = dtpFin.Value.Date + New TimeSpan(23, 59, 59)
                If chkEspecie.Checked Then Especie = txtEspecie.ValueText
                If chkLoteSalida.Checked Then CodigoLote = txtLoteSalida.Text

                ' Obtengo los datos
                Dim Registros = (From Etq As Etiqueta In Contexto.Etiquetas
                                 Where Etq.fechaBaja Is Nothing _
                                     AndAlso (Etq.Empaquetado.LoteSalida Is Nothing) _
                                     AndAlso (Not FechaInicio.HasValue OrElse Etq.fecha >= FechaInicio.Value) _
                                     AndAlso (Not FechaFin.HasValue OrElse Etq.fecha <= FechaFin.Value) _
                                     AndAlso (String.IsNullOrEmpty(Especie) OrElse (Etq.LoteEntradaLinea IsNot Nothing AndAlso Etq.LoteEntradaLinea.Especie IsNot Nothing AndAlso Etq.LoteEntradaLinea.Especie.alfa3 = Especie))
                                 Order By Etq.id
                                 Select Etq).ToList

                If Not String.IsNullOrEmpty(CodigoLote) Then
                    Dim res As New List(Of Etiqueta)

                    For Each r In Registros
                        If r.LoteSalida = CodigoLote OrElse r.LoteSalida.Contains(CodigoLote) Then
                            res.Add(r)
                        End If
                    Next
                    Return res
                Else
                    Return Registros
                End If

            Case Informe.InformeEntradas
                Dim FechaInicio As Nullable(Of DateTime) = Nothing
                Dim FechaFin As Nullable(Of DateTime) = Nothing
                Dim Especie As String = String.Empty


                If chkInicio.Checked AndAlso Not IsDBNull(dtpInicio.ValueNullable) Then FechaInicio = dtpInicio.Value.Date + New TimeSpan(0, 0, 0)
                If chkFin.Checked AndAlso Not IsDBNull(dtpFin.ValueNullable) Then FechaFin = dtpFin.Value.Date + New TimeSpan(23, 59, 59)
                If chkEspecie.Checked Then Especie = txtEspecie.ValueText


                ' Obtengo los datos
                Dim Registros = (From Alb As LoteEntrada In Contexto.LotesEntrada
                                 From linea As LoteEntradalinea In Alb.Lineas
                                 Where Alb.fechaBaja Is Nothing _
                                     AndAlso (Not FechaInicio.HasValue OrElse Alb.fecha >= FechaInicio.Value) _
                                     AndAlso (Not FechaFin.HasValue OrElse Alb.fecha <= FechaFin.Value) _
                                     AndAlso (String.IsNullOrEmpty(Especie) OrElse (linea.Especie IsNot Nothing AndAlso linea.Especie.alfa3 = Especie))
                                 Order By Alb.fecha
                                 Select linea).ToList


                Return Registros

            Case Informe.InformeVentas
                Dim FechaInicio As Nullable(Of DateTime) = Nothing
                Dim FechaFin As Nullable(Of DateTime) = Nothing
                Dim Especie As String = String.Empty
                Dim CodigoLote As String = String.Empty
                Dim CodigoCliente As String = String.Empty


                If chkInicio.Checked AndAlso Not IsDBNull(dtpInicio.ValueNullable) Then FechaInicio = dtpInicio.Value.Date + New TimeSpan(0, 0, 0)
                If chkFin.Checked AndAlso Not IsDBNull(dtpFin.ValueNullable) Then FechaFin = dtpFin.Value.Date + New TimeSpan(23, 59, 59)
                If chkEspecie.Checked Then Especie = txtEspecie.ValueText
                If chkCliente.Checked Then CodigoCliente = txtCliente.ValueText
                If chkLoteSalida.Checked Then CodigoLote = txtLoteSalida.Text

                ' Obtengo los datos
                Dim Registros = (From sal As LoteSalida In Contexto.LotesSalida
                                 From emp As Empaquetado In sal.Empaquetados
                                 From Etq As Etiqueta In emp.Etiquetas
                                 Where Etq.fechaBaja Is Nothing _
                                     AndAlso (emp.LoteSalida IsNot Nothing) _
                                     AndAlso (Not FechaInicio.HasValue OrElse sal.fecha >= FechaInicio.Value) _
                                     AndAlso (Not FechaFin.HasValue OrElse sal.fecha <= FechaFin.Value) _
                                     AndAlso (String.IsNullOrEmpty(CodigoCliente) OrElse sal.Cliente IsNot Nothing AndAlso sal.Cliente.codigo = CodigoCliente) _
                                     AndAlso (String.IsNullOrEmpty(Especie) OrElse (Etq.LoteEntradaLinea IsNot Nothing AndAlso Etq.LoteEntradaLinea.Especie IsNot Nothing AndAlso Etq.LoteEntradaLinea.Especie.alfa3 = Especie))
                                 Order By Etq.id
                                 Select Etq).ToList

                If Not String.IsNullOrEmpty(CodigoLote) Then
                    Dim res As New List(Of Etiqueta)

                    For Each r In Registros
                        If r.LoteSalida = CodigoLote OrElse r.LoteSalida.Contains(CodigoLote) Then
                            res.Add(r)
                        End If
                    Next
                    Return res
                Else
                    Return Registros
                End If

        End Select

        Return Nothing
    End Function
#End Region
#Region " RESTO DE FUNCIONES "
    Private Sub rptVisor_Error(ByVal source As System.Object, ByVal e As CrystalDecisions.Windows.Forms.ExceptionEventArgs) Handles rptVisor.Error
#If DEBUG Then
#Else
        e.Handled = True
#End If
    End Sub

    ''' <summary>
    ''' Activa / desactiva par�metros    
    ''' </summary>
    Private Sub ActivarParametros(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkInicio.CheckedChanged, chkFin.CheckedChanged, chkLoteSalida.CheckedChanged, chkEspecie.CheckedChanged, chkCliente.CheckedChanged
        dtpInicio.Enabled = chkInicio.Checked
        dtpFin.Enabled = chkFin.Checked
        txtEspecie.Enabled = chkEspecie.Checked
        txtLoteSalida.Enabled = chkLoteSalida.Checked
        txtCliente.Enabled = chkCliente.Checked
    End Sub

    ''' <summary>
    ''' Muestra / Oculta las cabeceras del informe
    ''' </summary>
    ''' <param name="Mostrar"></param>
    Public Sub MostrarCabeceras(ByVal Mostrar As Boolean) Implements IVisorInforme.MostrarCabeceras
        If _CambioControlado Then Exit Sub
        _MostrarLogos = Mostrar
        RefrescarDatos()
    End Sub

    ''' <summary>
    ''' Muestra / Oculta las cabeceras del informe
    ''' </summary>
    ''' <param name="Mostrar"></param>
    Public Sub MostrarTelefonos(ByVal Mostrar As Boolean) Implements IVisorInforme.MostrarTelefonos
        If _CambioControlado Then Exit Sub

        _MostrarTelefonos = Mostrar
        RefrescarDatos()
    End Sub

    ''' <summary>
    ''' Muestra / Oculta las cabeceras del informe
    ''' </summary>
    ''' <param name="Mostrar"></param>
    Public Sub MostrarNombre(ByVal Mostrar As Boolean) Implements IVisorInforme.MostrarNombre
        If _CambioControlado Then Exit Sub

        _MostrarNombre = Mostrar
        RefrescarDatos()
    End Sub

    ''' <summary>
    ''' Muestra / Oculta las cabeceras del informe
    ''' </summary>
    ''' <param name="Mostrar"></param>
    Public Sub MostrarAtencion(ByVal Mostrar As Boolean) Implements IVisorInforme.MostrarAtencion
        If _CambioControlado Then Exit Sub

        _MostrarAtencion = Mostrar
        RefrescarDatos()
    End Sub

    ''' <summary>
    '''  Oculta el panel de par�metros cuando el usuario hace click en el bot�n
    ''' </summary>
    Private Sub OcultarParametros(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOcultarParametros.Click
        Quadralia.KryptonForms.EsconderPanel(splSeparador, hdrParametros)
    End Sub

    ''' <summary>
    '''  Oculta el panel de par�metros cuando el usuario hace doble click en la cabecera
    ''' </summary>
    Private Sub OcultarParametrosDobleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hdrParametros.DoubleClick
        btnOcultarParametros.PerformClick()
    End Sub
#End Region
#Region " IMPLEMENTACION INTERFAZ "
    ''' <summary>
    ''' Imprime el informe
    ''' </summary>    
    Private Sub Exportar() Implements IVisorInforme.Exportar
        rptVisor.ExportReport()
    End Sub

    ''' <summary>
    ''' Imprime el informe    
    ''' </summary>    
    Private Sub Imprimir() Implements IVisorInforme.Imprimir
        rptVisor.PrintReport()
    End Sub

    Private Sub Anterior() Implements IVisorInforme.Anterior
        If _Reporte Is Nothing Then Exit Sub
        rptVisor.ShowPreviousPage()
        ControlarRibbon()
    End Sub

    Private Sub Primera() Implements IVisorInforme.Primera
        If _Reporte Is Nothing Then Exit Sub
        rptVisor.ShowFirstPage()
        ControlarRibbon()
    End Sub

    Private Sub Siguiente() Implements IVisorInforme.Siguiente
        If _Reporte Is Nothing Then Exit Sub
        rptVisor.ShowNextPage()
        ControlarRibbon()
    End Sub

    Private Sub Ultima() Implements IVisorInforme.Ultima
        If _Reporte Is Nothing Then Exit Sub
        rptVisor.ShowLastPage()
        ControlarRibbon()
    End Sub
#End Region
#Region " IMPLEMENTACIONES NECESARIAS DEL FORMULARIO HIJO "
    ''' <summary>
    ''' Tab del ribbon asociado a este formulario
    ''' </summary>
    Public Overrides ReadOnly Property Tab() As ComponentFactory.Krypton.Ribbon.KryptonRibbonTab
        Get
            Return frmPrincipal.tabInforme
        End Get
    End Property

    ''' <summary>
    ''' Permitimos el env�o de emails    
    ''' </summary>
    'Public Overrides ReadOnly Property PermiteEnvioMail As Boolean
    '    Get
    '        Return True
    '    End Get
    'End Property

    ''' <summary>
    ''' Obtiene el listado de direcciones de correos electr�nicos a los que se les va a enviar los correos
    ''' </summary>
    'Public Overrides ReadOnly Property DireccionesEmail As System.Collections.Generic.List(Of System.Net.Mail.MailAddress)
    '    Get
    '        Return New List(Of System.Net.Mail.MailAddress)
    '    End Get
    'End Property

    'Public Overrides Function ObtenerVentanaMail(Optional ByVal mostrar As Boolean = True) As frmMail
    '    ' Validaciones
    '    If _Reporte Is Nothing Then Return Nothing
    '    If rptVisor.GetCurrentPageNumber < 1 Then Return Nothing

    '    Dim NombreArchivo As String = String.Format("{0}_{1:yyyyMMddHHmmss}.pdf", lblTitulo.Text, DateTime.Now)
    '    NombreArchivo = My.Computer.FileSystem.CombinePath(IO.Path.GetTempPath(), NombreArchivo)

    '    ' Verifico que no exista el archivo
    '    If My.Computer.FileSystem.FileExists(NombreArchivo) Then My.Computer.FileSystem.DeleteFile(NombreArchivo)

    '    ' Exporto
    '    _Reporte.ExportToDisk(ExportFormatType.PortableDocFormat, NombreArchivo)

    '    ' Muestro los datos
    '    Dim Ventana As frmMail = MyBase.ObtenerVentanaMail(mostrar)
    '    If Ventana Is Nothing Then Return Nothing
    '    Ventana.AnhadirAdjunto(NombreArchivo)

    '    Return Ventana
    'End Function
#End Region
End Class
