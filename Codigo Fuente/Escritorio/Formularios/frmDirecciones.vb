﻿Public Class frmDirecciones
    Implements IFormularioItem

    Dim _Direccion As IDireccion = Nothing
    Dim _Contexto As Entidades = Nothing

#Region " PROPIEDADES "
    ''' <summary>
    ''' Contacto con los datos establecidos por el usuario
    ''' </summary>
    Public Property Direccion() As IDireccion
        Get
            Return _Direccion
        End Get
        Set(ByVal value As IDireccion)
            _Direccion = value
            CargarDireccion(value)
        End Set
    End Property

    ''' <summary>
    ''' Llamada genérica para la interfaz
    ''' </summary>
    Public Property Item As Object Implements IFormularioItem.Item
        Get
            Return _Direccion
        End Get
        Set(ByVal value As Object)
            If value Is Nothing OrElse TypeOf (value) Is IDireccion Then
                Direccion = value
            End If
        End Set
    End Property

    Public Property Items As IEnumerable Implements IFormularioItem.Items
        Get
            Dim Aux As New ArrayList
            If Item IsNot Nothing Then Aux.Add(Item)

            Return Aux
        End Get
        Set(ByVal value As IEnumerable)
            If value Is Nothing Then
                Item = Nothing
            Else
                Item = value(0)
            End If
        End Set
    End Property

    Private Property Contexto As Entidades
        Get
            If _Contexto Is Nothing Then _Contexto = cDAL.Instancia.getContext
            Return _Contexto
        End Get
        Set(ByVal value As Entidades)
            value = _Contexto
        End Set
    End Property
#End Region
#Region " LIMPIEZA "
    ''' <summary>
    ''' Limpia todos los controles
    ''' </summary>
    Private Sub Limpiar()
        txtCP.Clear()
        txtDescripcion.Clear()
        txtDireccion.Clear()
        txtPoblacion.Clear()
        txtProvincia.Clear()
        txtPais.Text = "España"

        cboTipo.SelectedItem = Nothing
        cboTipo.SelectedIndex = -1

        epErrores.Clear()
    End Sub
#End Region
#Region " CARGAR / GUARDAR / CANCELAR "
    ''' <summary>
    ''' Carga el contacto en el formulario
    ''' </summary>
    Private Sub CargarDireccion(ByVal Direccion As IDireccion)
        Limpiar()

        With Direccion
            txtCP.Text = .CodigoPostal
            txtDescripcion.Text = .Descripcion
            txtDireccion.Text = .Direccion
            txtPais.Text = .Pais
            txtPoblacion.Text = .Poblacion
            txtProvincia.Text = .Provincia

            cboTipo.SelectedItem = .Tipo
        End With
    End Sub

    ''' <summary>
    ''' Guarda los datos y sale del formulario
    ''' </summary>
    Private Sub GuardarYSalir(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If Guardar() Then
            Me.DialogResult = Windows.Forms.DialogResult.OK
            Me.Close()
        End If
    End Sub

    Public Function Guardar(Optional ByVal MostrarMensaje As Boolean = True) As Boolean
        ' Valido los datos
        Me.ValidateChildren(ValidationConstraints.Enabled + ValidationConstraints.Visible)

        ' Si hay errores, salgo de la función
        If epErrores.HasErrors Then
            If MostrarMensaje Then Quadralia.Aplicacion.MostrarMessageBox("Corrija los errores antes de continuar", "Errores Encontrados", MessageBoxButtons.OK, MessageBoxIcon.Error)
            epErrores.Controles(0).Focus()
            Return False
        End If

        With _Direccion
            .CodigoPostal = txtCP.Text
            .Descripcion = txtDescripcion.Text
            .Direccion = txtDireccion.Text
            .Poblacion = txtPoblacion.Text
            .Provincia = txtProvincia.Text
            .Pais = txtPais.Text

            If cboTipo.SelectedIndex > -1 Then .Tipo = cboTipo.SelectedItem Else .Tipo = 0
        End With

        Return True
    End Function

    ''' <summary>
    ''' El usuario indica que no desea crear un nuevo contacto
    ''' </summary>
    Private Sub Cancelar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        _Direccion = Nothing
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub
#End Region
#Region " RUTINA DE INICIO "
    Public Sub New(ByVal MostrarTipos As Boolean, ByRef Contexto As Entidades)
        ' Llamada necesaria para el Diseñador de Windows Forms.
        InitializeComponent()

        _Contexto = Contexto
        lblTipo.Visible = MostrarTipos
        cboTipo.Visible = MostrarTipos

        ' Limpio los controles
        Limpiar()
        CargarDatosMaestros()

        For Each UnControl As Control In tblOrganizador.Controls
            Quadralia.Formularios.Funcionalidad.ManejadorTabPorIntro(UnControl)
        Next

        Quadralia.Formularios.AutoTabular(Me)
    End Sub

    ''' <summary>
    ''' Carga los datos maestros del formulario
    ''' </summary>
    Private Sub CargarDatosMaestros()
        cboTipo.DisplayMember = "Key"
        cboTipo.ValueMember = "Value"
        cboTipo.DataSource = [Enum].GetValues(GetType(IDireccion.TiposDireccion))
        cboTipo.SelectedItem = Nothing
        cboTipo.SelectedIndex = -1
    End Sub
#End Region

    Private Sub txtCP_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCP.KeyUp
        If txtPais.Text.Trim.ToUpper = "ESPAÑA" AndAlso txtCP.Text.Length = 2 Then
            Dim SuProvincia As provincia = (From It As provincia In Contexto.provincias Where It.codigo = txtCP.Text Select It).FirstOrDefault
            If SuProvincia IsNot Nothing Then txtProvincia.Text = SuProvincia.nombre

            Dim ListaAuto As New AutoCompleteStringCollection
            ListaAuto.AddRange((From It As municipio In Contexto.municipios Where It.provincia = txtCP.Text Select It.nombre).ToList.ToArray)
            txtPoblacion.AutoCompleteCustomSource = ListaAuto
            txtPoblacion.AutoCompleteSource = AutoCompleteSource.CustomSource
        End If
    End Sub
End Class