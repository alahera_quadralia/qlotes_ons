<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmExpedidores
    Inherits FormularioHijo

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Dise�ador de Windows Forms.  
    'No lo modifique con el editor de c�digo.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblBNombre = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtBNombre = New Escritorio.Quadralia.Controles.aTextBox()
        Me.btnBBuscar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.grpResultados = New ComponentFactory.Krypton.Toolkit.KryptonGroupBox()
        Me.dgvResultadosBuscador = New ComponentFactory.Krypton.Toolkit.KryptonDataGridView()
        Me.colResultadoCIF = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colResultadoTarifa = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.splSeparador = New ComponentFactory.Krypton.Toolkit.KryptonSplitContainer()
        Me.hdrBusqueda = New ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup()
        Me.btnOcultarBusqueda = New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup()
        Me.tblBuscador = New System.Windows.Forms.TableLayoutPanel()
        Me.chkBEliminados = New ComponentFactory.Krypton.Toolkit.KryptonCheckBox()
        Me.txtBCIF = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtBRegistro = New Escritorio.Quadralia.Controles.aTextBox()
        Me.lblBCIF = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblBRegistro = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.tabGeneral = New ComponentFactory.Krypton.Navigator.KryptonNavigator()
        Me.tbpDatosGenerales = New ComponentFactory.Krypton.Navigator.KryptonPage()
        Me.tblOrganizadorDatos = New System.Windows.Forms.TableLayoutPanel()
        Me.txtDireccion = New Escritorio.Quadralia.Controles.aTextBox()
        Me.lblDireccion = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblRazonSocial = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblFax = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblTelefono1 = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblTelefono2 = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtRazonSocial = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtTelefono1 = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtTelefono2 = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtFax = New Escritorio.Quadralia.Controles.aTextBox()
        Me.chkReactivarRegistro = New ComponentFactory.Krypton.Toolkit.KryptonCheckBox()
        Me.txtEmail = New Escritorio.Quadralia.Controles.aTextBox()
        Me.lblEmail = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblWeb = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtWeb = New Escritorio.Quadralia.Controles.aTextBox()
        Me.chkPredeterminado = New ComponentFactory.Krypton.Toolkit.KryptonCheckBox()
        Me.lblCIF = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtCIF = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtCodigo = New Escritorio.Quadralia.Controles.aTextBox()
        Me.lblCodigo = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblRegistro = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtRegistro = New Escritorio.Quadralia.Controles.aTextBox()
        Me.vsrLogotipo = New Escritorio.VisorImagenes()
        Me.lblCaducidad = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtCaducidad = New Escritorio.Quadralia.Controles.aTextBox()
        Me.tbpObservaciones = New ComponentFactory.Krypton.Navigator.KryptonPage()
        Me.htmlObservaciones = New Escritorio.EditorHTML()
        Me.lblTitulo = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.grpCabecera = New ComponentFactory.Krypton.Toolkit.KryptonGroup()
        Me.epErrores = New Escritorio.Quadralia.Controles.Validacion.GestorErrores()
        Me.ttpInformaci�n = New System.Windows.Forms.ToolTip(Me.components)
        Me.GestorErrores1 = New Escritorio.Quadralia.Controles.Validacion.GestorErrores()
        CType(Me.grpResultados, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpResultados.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpResultados.Panel.SuspendLayout()
        Me.grpResultados.SuspendLayout()
        CType(Me.dgvResultadosBuscador, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.splSeparador, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.splSeparador.Panel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.splSeparador.Panel1.SuspendLayout()
        CType(Me.splSeparador.Panel2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.splSeparador.Panel2.SuspendLayout()
        Me.splSeparador.SuspendLayout()
        CType(Me.hdrBusqueda, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.hdrBusqueda.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.hdrBusqueda.Panel.SuspendLayout()
        Me.hdrBusqueda.SuspendLayout()
        Me.tblBuscador.SuspendLayout()
        CType(Me.tabGeneral, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabGeneral.SuspendLayout()
        CType(Me.tbpDatosGenerales, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpDatosGenerales.SuspendLayout()
        Me.tblOrganizadorDatos.SuspendLayout()
        CType(Me.tbpObservaciones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpObservaciones.SuspendLayout()
        CType(Me.grpCabecera, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpCabecera.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpCabecera.Panel.SuspendLayout()
        Me.grpCabecera.SuspendLayout()
        CType(Me.epErrores, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GestorErrores1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblBNombre
        '
        Me.lblBNombre.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblBNombre.Location = New System.Drawing.Point(3, 29)
        Me.lblBNombre.Name = "lblBNombre"
        Me.lblBNombre.Size = New System.Drawing.Size(56, 20)
        Me.lblBNombre.TabIndex = 0
        Me.lblBNombre.Values.Text = "Nombre"
        '
        'txtBNombre
        '
        Me.txtBNombre.AlwaysActive = False
        Me.txtBNombre.controlarBotonBorrar = True
        Me.txtBNombre.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtBNombre.Formato = ""
        Me.txtBNombre.Location = New System.Drawing.Point(65, 29)
        Me.txtBNombre.MaxLength = 765
        Me.txtBNombre.mostrarSiempreBotonBorrar = False
        Me.txtBNombre.Name = "txtBNombre"
        Me.txtBNombre.seleccionarTodo = True
        Me.txtBNombre.Size = New System.Drawing.Size(167, 20)
        Me.txtBNombre.TabIndex = 1
        Me.ttpInformaci�n.SetToolTip(Me.txtBNombre, "Filtre el agente por su nombre")
        '
        'btnBBuscar
        '
        Me.tblBuscador.SetColumnSpan(Me.btnBBuscar, 2)
        Me.btnBBuscar.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnBBuscar.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnBBuscar.Location = New System.Drawing.Point(3, 107)
        Me.btnBBuscar.Name = "btnBBuscar"
        Me.btnBBuscar.Size = New System.Drawing.Size(229, 25)
        Me.btnBBuscar.TabIndex = 9
        Me.ttpInformaci�n.SetToolTip(Me.btnBBuscar, "Realiza una b�squeda aplicando los filtros establecidos")
        Me.btnBBuscar.Values.Text = "Buscar"
        '
        'grpResultados
        '
        Me.tblBuscador.SetColumnSpan(Me.grpResultados, 2)
        Me.grpResultados.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grpResultados.Location = New System.Drawing.Point(3, 138)
        Me.grpResultados.Name = "grpResultados"
        '
        'grpResultados.Panel
        '
        Me.grpResultados.Panel.Controls.Add(Me.dgvResultadosBuscador)
        Me.grpResultados.Size = New System.Drawing.Size(229, 274)
        Me.grpResultados.TabIndex = 10
        Me.grpResultados.Text = "Resultados"
        Me.grpResultados.Values.Heading = "Resultados"
        '
        'dgvResultadosBuscador
        '
        Me.dgvResultadosBuscador.AllowUserToAddRows = False
        Me.dgvResultadosBuscador.AllowUserToDeleteRows = False
        Me.dgvResultadosBuscador.AllowUserToResizeRows = False
        Me.dgvResultadosBuscador.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colResultadoCIF, Me.colResultadoTarifa})
        Me.dgvResultadosBuscador.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvResultadosBuscador.Location = New System.Drawing.Point(0, 0)
        Me.dgvResultadosBuscador.MultiSelect = False
        Me.dgvResultadosBuscador.Name = "dgvResultadosBuscador"
        Me.dgvResultadosBuscador.ReadOnly = True
        Me.dgvResultadosBuscador.RowHeadersVisible = False
        Me.dgvResultadosBuscador.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvResultadosBuscador.Size = New System.Drawing.Size(225, 250)
        Me.dgvResultadosBuscador.TabIndex = 0
        Me.ttpInformaci�n.SetToolTip(Me.dgvResultadosBuscador, "Resultado de la b�squeda tras aplicar los filtros")
        '
        'colResultadoCIF
        '
        Me.colResultadoCIF.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colResultadoCIF.DataPropertyName = "CIF"
        Me.colResultadoCIF.HeaderText = "CIF"
        Me.colResultadoCIF.Name = "colResultadoCIF"
        Me.colResultadoCIF.ReadOnly = True
        Me.colResultadoCIF.Width = 53
        '
        'colResultadoTarifa
        '
        Me.colResultadoTarifa.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colResultadoTarifa.DataPropertyName = "razonSocial"
        Me.colResultadoTarifa.HeaderText = "Expedidor"
        Me.colResultadoTarifa.MinimumWidth = 80
        Me.colResultadoTarifa.Name = "colResultadoTarifa"
        Me.colResultadoTarifa.ReadOnly = True
        Me.colResultadoTarifa.Width = 171
        '
        'splSeparador
        '
        Me.splSeparador.Cursor = System.Windows.Forms.Cursors.Default
        Me.splSeparador.Dock = System.Windows.Forms.DockStyle.Fill
        Me.splSeparador.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.splSeparador.Location = New System.Drawing.Point(0, 57)
        Me.splSeparador.Name = "splSeparador"
        '
        'splSeparador.Panel1
        '
        Me.splSeparador.Panel1.Controls.Add(Me.hdrBusqueda)
        '
        'splSeparador.Panel2
        '
        Me.splSeparador.Panel2.Controls.Add(Me.tabGeneral)
        Me.splSeparador.Size = New System.Drawing.Size(826, 429)
        Me.splSeparador.SplitterDistance = 237
        Me.splSeparador.TabIndex = 1
        '
        'hdrBusqueda
        '
        Me.hdrBusqueda.AutoSize = True
        Me.hdrBusqueda.ButtonSpecs.AddRange(New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup() {Me.btnOcultarBusqueda})
        Me.hdrBusqueda.Dock = System.Windows.Forms.DockStyle.Fill
        Me.hdrBusqueda.HeaderVisibleSecondary = False
        Me.hdrBusqueda.Location = New System.Drawing.Point(0, 0)
        Me.hdrBusqueda.Name = "hdrBusqueda"
        '
        'hdrBusqueda.Panel
        '
        Me.hdrBusqueda.Panel.Controls.Add(Me.tblBuscador)
        Me.hdrBusqueda.Size = New System.Drawing.Size(237, 429)
        Me.hdrBusqueda.TabIndex = 0
        Me.hdrBusqueda.ValuesPrimary.Heading = "Buscar"
        Me.hdrBusqueda.ValuesPrimary.Image = Global.Escritorio.My.Resources.Resources.Buscar_32
        '
        'btnOcultarBusqueda
        '
        Me.btnOcultarBusqueda.Type = ComponentFactory.Krypton.Toolkit.PaletteButtonSpecStyle.ArrowLeft
        Me.btnOcultarBusqueda.UniqueName = "BE70F7722CF94C60618F65819BBF010D"
        '
        'tblBuscador
        '
        Me.tblBuscador.BackColor = System.Drawing.Color.Transparent
        Me.tblBuscador.ColumnCount = 2
        Me.tblBuscador.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblBuscador.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblBuscador.Controls.Add(Me.chkBEliminados, 0, 3)
        Me.tblBuscador.Controls.Add(Me.grpResultados, 0, 5)
        Me.tblBuscador.Controls.Add(Me.btnBBuscar, 0, 4)
        Me.tblBuscador.Controls.Add(Me.txtBNombre, 1, 1)
        Me.tblBuscador.Controls.Add(Me.lblBNombre, 0, 1)
        Me.tblBuscador.Controls.Add(Me.txtBCIF, 1, 0)
        Me.tblBuscador.Controls.Add(Me.txtBRegistro, 1, 2)
        Me.tblBuscador.Controls.Add(Me.lblBCIF, 0, 0)
        Me.tblBuscador.Controls.Add(Me.lblBRegistro, 0, 2)
        Me.tblBuscador.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblBuscador.Location = New System.Drawing.Point(0, 0)
        Me.tblBuscador.Name = "tblBuscador"
        Me.tblBuscador.RowCount = 6
        Me.tblBuscador.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblBuscador.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblBuscador.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblBuscador.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblBuscador.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblBuscador.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblBuscador.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblBuscador.Size = New System.Drawing.Size(235, 392)
        Me.tblBuscador.TabIndex = 0
        '
        'chkBEliminados
        '
        Me.tblBuscador.SetColumnSpan(Me.chkBEliminados, 2)
        Me.chkBEliminados.Dock = System.Windows.Forms.DockStyle.Fill
        Me.chkBEliminados.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl
        Me.chkBEliminados.Location = New System.Drawing.Point(3, 81)
        Me.chkBEliminados.Name = "chkBEliminados"
        Me.chkBEliminados.Size = New System.Drawing.Size(229, 20)
        Me.chkBEliminados.TabIndex = 8
        Me.chkBEliminados.Text = "Incluir registros eliminados"
        Me.ttpInformaci�n.SetToolTip(Me.chkBEliminados, "En el resultado de la b�squeda se mostrar�n los registros que se han eliminado co" & _
        "n aterioridad")
        Me.chkBEliminados.Values.Text = "Incluir registros eliminados"
        '
        'txtBCIF
        '
        Me.txtBCIF.AlwaysActive = False
        Me.txtBCIF.controlarBotonBorrar = True
        Me.txtBCIF.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtBCIF.Formato = ""
        Me.txtBCIF.Location = New System.Drawing.Point(65, 3)
        Me.txtBCIF.MaxLength = 765
        Me.txtBCIF.mostrarSiempreBotonBorrar = False
        Me.txtBCIF.Name = "txtBCIF"
        Me.txtBCIF.seleccionarTodo = True
        Me.txtBCIF.Size = New System.Drawing.Size(167, 20)
        Me.txtBCIF.TabIndex = 1
        '
        'txtBRegistro
        '
        Me.txtBRegistro.AlwaysActive = False
        Me.txtBRegistro.controlarBotonBorrar = True
        Me.txtBRegistro.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtBRegistro.Formato = ""
        Me.txtBRegistro.Location = New System.Drawing.Point(65, 55)
        Me.txtBRegistro.MaxLength = 765
        Me.txtBRegistro.mostrarSiempreBotonBorrar = False
        Me.txtBRegistro.Name = "txtBRegistro"
        Me.txtBRegistro.seleccionarTodo = True
        Me.txtBRegistro.Size = New System.Drawing.Size(167, 20)
        Me.txtBRegistro.TabIndex = 1
        '
        'lblBCIF
        '
        Me.lblBCIF.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblBCIF.Location = New System.Drawing.Point(32, 3)
        Me.lblBCIF.Name = "lblBCIF"
        Me.lblBCIF.Size = New System.Drawing.Size(27, 20)
        Me.lblBCIF.TabIndex = 0
        Me.lblBCIF.Values.Text = "CIF"
        '
        'lblBRegistro
        '
        Me.lblBRegistro.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblBRegistro.Location = New System.Drawing.Point(3, 55)
        Me.lblBRegistro.Name = "lblBRegistro"
        Me.lblBRegistro.Size = New System.Drawing.Size(56, 20)
        Me.lblBRegistro.TabIndex = 0
        Me.lblBRegistro.Values.Text = "Registro"
        '
        'tabGeneral
        '
        Me.tabGeneral.Button.ButtonDisplayLogic = ComponentFactory.Krypton.Navigator.ButtonDisplayLogic.None
        Me.tabGeneral.Button.CloseButtonAction = ComponentFactory.Krypton.Navigator.CloseButtonAction.None
        Me.tabGeneral.Button.CloseButtonDisplay = ComponentFactory.Krypton.Navigator.ButtonDisplay.Hide
        Me.tabGeneral.Button.ContextButtonAction = ComponentFactory.Krypton.Navigator.ContextButtonAction.None
        Me.tabGeneral.Button.ContextButtonDisplay = ComponentFactory.Krypton.Navigator.ButtonDisplay.Hide
        Me.tabGeneral.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabGeneral.Location = New System.Drawing.Point(0, 0)
        Me.tabGeneral.Name = "tabGeneral"
        Me.tabGeneral.Pages.AddRange(New ComponentFactory.Krypton.Navigator.KryptonPage() {Me.tbpDatosGenerales, Me.tbpObservaciones})
        Me.tabGeneral.SelectedIndex = 0
        Me.tabGeneral.Size = New System.Drawing.Size(584, 429)
        Me.tabGeneral.TabIndex = 0
        '
        'tbpDatosGenerales
        '
        Me.tbpDatosGenerales.AutoHiddenSlideSize = New System.Drawing.Size(200, 200)
        Me.tbpDatosGenerales.Controls.Add(Me.tblOrganizadorDatos)
        Me.tbpDatosGenerales.Flags = 65534
        Me.tbpDatosGenerales.LastVisibleSet = True
        Me.tbpDatosGenerales.MinimumSize = New System.Drawing.Size(50, 50)
        Me.tbpDatosGenerales.Name = "tbpDatosGenerales"
        Me.tbpDatosGenerales.Size = New System.Drawing.Size(582, 402)
        Me.tbpDatosGenerales.Text = "General"
        Me.ttpInformaci�n.SetToolTip(Me.tbpDatosGenerales, "Datos generales del agente")
        Me.tbpDatosGenerales.ToolTipTitle = "Page ToolTip"
        Me.tbpDatosGenerales.UniqueName = "D06970FC739F41C817941E4528AF0685"
        '
        'tblOrganizadorDatos
        '
        Me.tblOrganizadorDatos.BackColor = System.Drawing.Color.Transparent
        Me.tblOrganizadorDatos.ColumnCount = 6
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblOrganizadorDatos.Controls.Add(Me.txtDireccion, 2, 3)
        Me.tblOrganizadorDatos.Controls.Add(Me.lblDireccion, 0, 3)
        Me.tblOrganizadorDatos.Controls.Add(Me.lblRazonSocial, 0, 2)
        Me.tblOrganizadorDatos.Controls.Add(Me.lblFax, 0, 5)
        Me.tblOrganizadorDatos.Controls.Add(Me.lblTelefono1, 0, 4)
        Me.tblOrganizadorDatos.Controls.Add(Me.lblTelefono2, 3, 4)
        Me.tblOrganizadorDatos.Controls.Add(Me.txtRazonSocial, 2, 2)
        Me.tblOrganizadorDatos.Controls.Add(Me.txtTelefono1, 2, 4)
        Me.tblOrganizadorDatos.Controls.Add(Me.txtTelefono2, 5, 4)
        Me.tblOrganizadorDatos.Controls.Add(Me.txtFax, 2, 5)
        Me.tblOrganizadorDatos.Controls.Add(Me.chkReactivarRegistro, 0, 13)
        Me.tblOrganizadorDatos.Controls.Add(Me.txtEmail, 5, 5)
        Me.tblOrganizadorDatos.Controls.Add(Me.lblEmail, 3, 5)
        Me.tblOrganizadorDatos.Controls.Add(Me.lblWeb, 0, 9)
        Me.tblOrganizadorDatos.Controls.Add(Me.txtWeb, 2, 9)
        Me.tblOrganizadorDatos.Controls.Add(Me.chkPredeterminado, 0, 12)
        Me.tblOrganizadorDatos.Controls.Add(Me.lblCIF, 0, 1)
        Me.tblOrganizadorDatos.Controls.Add(Me.txtCIF, 2, 1)
        Me.tblOrganizadorDatos.Controls.Add(Me.txtCodigo, 5, 0)
        Me.tblOrganizadorDatos.Controls.Add(Me.lblCodigo, 3, 0)
        Me.tblOrganizadorDatos.Controls.Add(Me.lblRegistro, 3, 1)
        Me.tblOrganizadorDatos.Controls.Add(Me.txtRegistro, 5, 1)
        Me.tblOrganizadorDatos.Controls.Add(Me.vsrLogotipo, 3, 10)
        Me.tblOrganizadorDatos.Controls.Add(Me.lblCaducidad, 0, 11)
        Me.tblOrganizadorDatos.Controls.Add(Me.txtCaducidad, 2, 11)
        Me.tblOrganizadorDatos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblOrganizadorDatos.Location = New System.Drawing.Point(0, 0)
        Me.tblOrganizadorDatos.Name = "tblOrganizadorDatos"
        Me.tblOrganizadorDatos.RowCount = 15
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.Size = New System.Drawing.Size(582, 402)
        Me.tblOrganizadorDatos.TabIndex = 1
        '
        'txtDireccion
        '
        Me.txtDireccion.AlwaysActive = False
        Me.tblOrganizadorDatos.SetColumnSpan(Me.txtDireccion, 4)
        Me.txtDireccion.controlarBotonBorrar = True
        Me.txtDireccion.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtDireccion.Formato = ""
        Me.txtDireccion.Location = New System.Drawing.Point(131, 81)
        Me.txtDireccion.MaxLength = 500
        Me.txtDireccion.mostrarSiempreBotonBorrar = False
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.seleccionarTodo = True
        Me.txtDireccion.Size = New System.Drawing.Size(448, 20)
        Me.txtDireccion.TabIndex = 22
        '
        'lblDireccion
        '
        Me.lblDireccion.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblDireccion.Location = New System.Drawing.Point(43, 81)
        Me.lblDireccion.Name = "lblDireccion"
        Me.lblDireccion.Size = New System.Drawing.Size(62, 20)
        Me.lblDireccion.TabIndex = 21
        Me.lblDireccion.Values.Text = "Direcci�n"
        '
        'lblRazonSocial
        '
        Me.lblRazonSocial.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblRazonSocial.Location = New System.Drawing.Point(27, 55)
        Me.lblRazonSocial.Name = "lblRazonSocial"
        Me.lblRazonSocial.Size = New System.Drawing.Size(78, 20)
        Me.lblRazonSocial.TabIndex = 4
        Me.lblRazonSocial.Values.Text = "Raz�n social"
        '
        'lblFax
        '
        Me.lblFax.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblFax.Location = New System.Drawing.Point(77, 133)
        Me.lblFax.Name = "lblFax"
        Me.lblFax.Size = New System.Drawing.Size(28, 20)
        Me.lblFax.TabIndex = 12
        Me.lblFax.Values.Text = "Fax"
        '
        'lblTelefono1
        '
        Me.lblTelefono1.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblTelefono1.Location = New System.Drawing.Point(37, 107)
        Me.lblTelefono1.Name = "lblTelefono1"
        Me.lblTelefono1.Size = New System.Drawing.Size(68, 20)
        Me.lblTelefono1.TabIndex = 8
        Me.lblTelefono1.Values.Text = "Tel�fono 1"
        '
        'lblTelefono2
        '
        Me.lblTelefono2.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblTelefono2.Location = New System.Drawing.Point(332, 107)
        Me.lblTelefono2.Name = "lblTelefono2"
        Me.lblTelefono2.Size = New System.Drawing.Size(68, 20)
        Me.lblTelefono2.TabIndex = 10
        Me.lblTelefono2.Values.Text = "Tel�fono 2"
        '
        'txtRazonSocial
        '
        Me.txtRazonSocial.AlwaysActive = False
        Me.tblOrganizadorDatos.SetColumnSpan(Me.txtRazonSocial, 4)
        Me.txtRazonSocial.controlarBotonBorrar = True
        Me.txtRazonSocial.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtRazonSocial.Formato = ""
        Me.txtRazonSocial.Location = New System.Drawing.Point(131, 55)
        Me.txtRazonSocial.MaxLength = 255
        Me.txtRazonSocial.mostrarSiempreBotonBorrar = False
        Me.txtRazonSocial.Name = "txtRazonSocial"
        Me.txtRazonSocial.seleccionarTodo = True
        Me.txtRazonSocial.Size = New System.Drawing.Size(448, 20)
        Me.txtRazonSocial.TabIndex = 5
        '
        'txtTelefono1
        '
        Me.txtTelefono1.AlwaysActive = False
        Me.txtTelefono1.controlarBotonBorrar = True
        Me.txtTelefono1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtTelefono1.Formato = ""
        Me.txtTelefono1.Location = New System.Drawing.Point(131, 107)
        Me.txtTelefono1.MaxLength = 32
        Me.txtTelefono1.mostrarSiempreBotonBorrar = False
        Me.txtTelefono1.Name = "txtTelefono1"
        Me.txtTelefono1.seleccionarTodo = True
        Me.txtTelefono1.Size = New System.Drawing.Size(152, 20)
        Me.txtTelefono1.TabIndex = 9
        '
        'txtTelefono2
        '
        Me.txtTelefono2.AlwaysActive = False
        Me.txtTelefono2.controlarBotonBorrar = True
        Me.txtTelefono2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtTelefono2.Formato = ""
        Me.txtTelefono2.Location = New System.Drawing.Point(426, 107)
        Me.txtTelefono2.MaxLength = 32
        Me.txtTelefono2.mostrarSiempreBotonBorrar = False
        Me.txtTelefono2.Name = "txtTelefono2"
        Me.txtTelefono2.seleccionarTodo = True
        Me.txtTelefono2.Size = New System.Drawing.Size(153, 20)
        Me.txtTelefono2.TabIndex = 11
        '
        'txtFax
        '
        Me.txtFax.AlwaysActive = False
        Me.txtFax.controlarBotonBorrar = True
        Me.txtFax.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtFax.Formato = ""
        Me.txtFax.Location = New System.Drawing.Point(131, 133)
        Me.txtFax.MaxLength = 32
        Me.txtFax.mostrarSiempreBotonBorrar = False
        Me.txtFax.Name = "txtFax"
        Me.txtFax.seleccionarTodo = True
        Me.txtFax.Size = New System.Drawing.Size(152, 20)
        Me.txtFax.TabIndex = 13
        '
        'chkReactivarRegistro
        '
        Me.tblOrganizadorDatos.SetColumnSpan(Me.chkReactivarRegistro, 4)
        Me.chkReactivarRegistro.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl
        Me.chkReactivarRegistro.Location = New System.Drawing.Point(3, 363)
        Me.chkReactivarRegistro.Name = "chkReactivarRegistro"
        Me.chkReactivarRegistro.Size = New System.Drawing.Size(144, 20)
        Me.chkReactivarRegistro.TabIndex = 20
        Me.chkReactivarRegistro.Text = "Reactivar el expedidor"
        Me.chkReactivarRegistro.Values.Text = "Reactivar el expedidor"
        Me.chkReactivarRegistro.Visible = False
        '
        'txtEmail
        '
        Me.txtEmail.AlwaysActive = False
        Me.txtEmail.controlarBotonBorrar = True
        Me.txtEmail.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtEmail.Formato = ""
        Me.txtEmail.Location = New System.Drawing.Point(426, 133)
        Me.txtEmail.MaxLength = 255
        Me.txtEmail.mostrarSiempreBotonBorrar = False
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.seleccionarTodo = True
        Me.txtEmail.Size = New System.Drawing.Size(153, 20)
        Me.txtEmail.TabIndex = 15
        '
        'lblEmail
        '
        Me.lblEmail.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblEmail.Location = New System.Drawing.Point(289, 133)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(111, 20)
        Me.lblEmail.TabIndex = 14
        Me.lblEmail.Values.Text = "Correo electr�nico"
        '
        'lblWeb
        '
        Me.lblWeb.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblWeb.Location = New System.Drawing.Point(29, 159)
        Me.lblWeb.Name = "lblWeb"
        Me.lblWeb.Size = New System.Drawing.Size(76, 20)
        Me.lblWeb.TabIndex = 16
        Me.lblWeb.Values.Text = "P�gina Web"
        '
        'txtWeb
        '
        Me.txtWeb.AlwaysActive = False
        Me.tblOrganizadorDatos.SetColumnSpan(Me.txtWeb, 4)
        Me.txtWeb.controlarBotonBorrar = True
        Me.txtWeb.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtWeb.Formato = ""
        Me.txtWeb.Location = New System.Drawing.Point(131, 159)
        Me.txtWeb.MaxLength = 255
        Me.txtWeb.mostrarSiempreBotonBorrar = False
        Me.txtWeb.Name = "txtWeb"
        Me.txtWeb.seleccionarTodo = True
        Me.txtWeb.Size = New System.Drawing.Size(448, 20)
        Me.txtWeb.TabIndex = 17
        '
        'chkPredeterminado
        '
        Me.tblOrganizadorDatos.SetColumnSpan(Me.chkPredeterminado, 3)
        Me.chkPredeterminado.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl
        Me.chkPredeterminado.Location = New System.Drawing.Point(3, 337)
        Me.chkPredeterminado.Name = "chkPredeterminado"
        Me.chkPredeterminado.Size = New System.Drawing.Size(169, 20)
        Me.chkPredeterminado.TabIndex = 20
        Me.chkPredeterminado.Text = "Expedidor Predeterminado"
        Me.chkPredeterminado.Values.Text = "Expedidor Predeterminado"
        '
        'lblCIF
        '
        Me.lblCIF.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblCIF.Location = New System.Drawing.Point(78, 29)
        Me.lblCIF.Name = "lblCIF"
        Me.lblCIF.Size = New System.Drawing.Size(27, 20)
        Me.lblCIF.TabIndex = 0
        Me.lblCIF.Values.Text = "CIF"
        '
        'txtCIF
        '
        Me.txtCIF.AlwaysActive = False
        Me.txtCIF.controlarBotonBorrar = True
        Me.txtCIF.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtCIF.Formato = ""
        Me.txtCIF.Location = New System.Drawing.Point(131, 29)
        Me.txtCIF.MaxLength = 32
        Me.txtCIF.mostrarSiempreBotonBorrar = False
        Me.txtCIF.Name = "txtCIF"
        Me.txtCIF.seleccionarTodo = True
        Me.txtCIF.Size = New System.Drawing.Size(152, 20)
        Me.txtCIF.TabIndex = 1
        '
        'txtCodigo
        '
        Me.txtCodigo.AlwaysActive = False
        Me.txtCodigo.controlarBotonBorrar = True
        Me.txtCodigo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtCodigo.Enabled = False
        Me.txtCodigo.Formato = ""
        Me.txtCodigo.Location = New System.Drawing.Point(426, 3)
        Me.txtCodigo.MaxLength = 16
        Me.txtCodigo.mostrarSiempreBotonBorrar = False
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.seleccionarTodo = True
        Me.txtCodigo.Size = New System.Drawing.Size(153, 20)
        Me.txtCodigo.TabIndex = 3
        Me.txtCodigo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblCodigo
        '
        Me.lblCodigo.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblCodigo.Location = New System.Drawing.Point(350, 3)
        Me.lblCodigo.Name = "lblCodigo"
        Me.lblCodigo.Size = New System.Drawing.Size(50, 20)
        Me.lblCodigo.TabIndex = 2
        Me.lblCodigo.Values.Text = "C�digo"
        '
        'lblRegistro
        '
        Me.lblRegistro.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblRegistro.Location = New System.Drawing.Point(344, 29)
        Me.lblRegistro.Name = "lblRegistro"
        Me.lblRegistro.Size = New System.Drawing.Size(56, 20)
        Me.lblRegistro.TabIndex = 0
        Me.lblRegistro.Values.Text = "Registro"
        '
        'txtRegistro
        '
        Me.txtRegistro.AlwaysActive = False
        Me.txtRegistro.controlarBotonBorrar = True
        Me.txtRegistro.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtRegistro.Formato = ""
        Me.txtRegistro.Location = New System.Drawing.Point(426, 29)
        Me.txtRegistro.MaxLength = 20
        Me.txtRegistro.mostrarSiempreBotonBorrar = False
        Me.txtRegistro.Name = "txtRegistro"
        Me.txtRegistro.seleccionarTodo = True
        Me.txtRegistro.Size = New System.Drawing.Size(153, 20)
        Me.txtRegistro.TabIndex = 1
        '
        'vsrLogotipo
        '
        Me.tblOrganizadorDatos.SetColumnSpan(Me.vsrLogotipo, 3)
        Me.vsrLogotipo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.vsrLogotipo.Item = Nothing
        Me.vsrLogotipo.Location = New System.Drawing.Point(289, 185)
        Me.vsrLogotipo.Name = "vsrLogotipo"
        Me.vsrLogotipo.Size = New System.Drawing.Size(290, 120)
        Me.vsrLogotipo.TabIndex = 23
        Me.vsrLogotipo.Titulo = "Logotipo"
        '
        'lblCaducidad
        '
        Me.lblCaducidad.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblCaducidad.Location = New System.Drawing.Point(3, 311)
        Me.lblCaducidad.Name = "lblCaducidad"
        Me.lblCaducidad.Size = New System.Drawing.Size(102, 20)
        Me.lblCaducidad.TabIndex = 4
        Me.lblCaducidad.Values.Text = "Texto Caducidad"
        '
        'txtCaducidad
        '
        Me.txtCaducidad.AlwaysActive = False
        Me.tblOrganizadorDatos.SetColumnSpan(Me.txtCaducidad, 4)
        Me.txtCaducidad.controlarBotonBorrar = True
        Me.txtCaducidad.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtCaducidad.Formato = ""
        Me.txtCaducidad.Location = New System.Drawing.Point(131, 311)
        Me.txtCaducidad.MaxLength = 255
        Me.txtCaducidad.mostrarSiempreBotonBorrar = False
        Me.txtCaducidad.Name = "txtCaducidad"
        Me.txtCaducidad.seleccionarTodo = True
        Me.txtCaducidad.Size = New System.Drawing.Size(448, 20)
        Me.txtCaducidad.TabIndex = 5
        '
        'tbpObservaciones
        '
        Me.tbpObservaciones.AutoHiddenSlideSize = New System.Drawing.Size(200, 200)
        Me.tbpObservaciones.Controls.Add(Me.htmlObservaciones)
        Me.tbpObservaciones.Flags = 65534
        Me.tbpObservaciones.LastVisibleSet = True
        Me.tbpObservaciones.MinimumSize = New System.Drawing.Size(50, 50)
        Me.tbpObservaciones.Name = "tbpObservaciones"
        Me.tbpObservaciones.Size = New System.Drawing.Size(582, 373)
        Me.tbpObservaciones.Text = "Observaciones"
        Me.ttpInformaci�n.SetToolTip(Me.tbpObservaciones, "Observaciones del agente")
        Me.tbpObservaciones.ToolTipTitle = "Page ToolTip"
        Me.tbpObservaciones.UniqueName = "4760253F410B4E2D7DA1458F5A89ABCF"
        '
        'htmlObservaciones
        '
        Me.htmlObservaciones.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.htmlObservaciones.ConvertirImagenesABase64 = False
        Me.htmlObservaciones.Dock = System.Windows.Forms.DockStyle.Fill
        Me.htmlObservaciones.Location = New System.Drawing.Point(0, 0)
        Me.htmlObservaciones.MostrarBarrasEdicion = True
        Me.htmlObservaciones.Name = "htmlObservaciones"
        Me.htmlObservaciones.ReadOnly = False
        Me.htmlObservaciones.Size = New System.Drawing.Size(582, 373)
        Me.htmlObservaciones.TabIndex = 0
        '
        'lblTitulo
        '
        Me.lblTitulo.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.Custom1
        Me.lblTitulo.Location = New System.Drawing.Point(5, 9)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(110, 34)
        Me.lblTitulo.TabIndex = 0
        Me.lblTitulo.Values.Image = Global.Escritorio.My.Resources.Resources.Expedidor_32
        Me.lblTitulo.Values.Text = "Expedidores"
        '
        'grpCabecera
        '
        Me.grpCabecera.Dock = System.Windows.Forms.DockStyle.Top
        Me.grpCabecera.GroupBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.PanelCustom1
        Me.grpCabecera.Location = New System.Drawing.Point(0, 0)
        Me.grpCabecera.Name = "grpCabecera"
        '
        'grpCabecera.Panel
        '
        Me.grpCabecera.Panel.Controls.Add(Me.lblTitulo)
        Me.grpCabecera.Size = New System.Drawing.Size(826, 57)
        Me.grpCabecera.StateCommon.Border.Color1 = System.Drawing.Color.DarkOrange
        Me.grpCabecera.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom
        Me.grpCabecera.StateCommon.Border.Width = 5
        Me.grpCabecera.TabIndex = 0
        '
        'epErrores
        '
        Me.epErrores.Alineacion = System.Windows.Forms.ErrorIconAlignment.TopLeft
        Me.epErrores.ContainerControl = Me
        '
        'GestorErrores1
        '
        Me.GestorErrores1.Alineacion = System.Windows.Forms.ErrorIconAlignment.TopLeft
        Me.GestorErrores1.ContainerControl = Me
        '
        'frmExpedidores
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(826, 486)
        Me.Controls.Add(Me.splSeparador)
        Me.Controls.Add(Me.grpCabecera)
        Me.Name = "frmExpedidores"
        Me.Text = "Expedidores"
        CType(Me.grpResultados.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpResultados.Panel.ResumeLayout(False)
        CType(Me.grpResultados, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpResultados.ResumeLayout(False)
        CType(Me.dgvResultadosBuscador, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.splSeparador.Panel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.splSeparador.Panel1.ResumeLayout(False)
        Me.splSeparador.Panel1.PerformLayout()
        CType(Me.splSeparador.Panel2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.splSeparador.Panel2.ResumeLayout(False)
        CType(Me.splSeparador, System.ComponentModel.ISupportInitialize).EndInit()
        Me.splSeparador.ResumeLayout(False)
        CType(Me.hdrBusqueda.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrBusqueda.Panel.ResumeLayout(False)
        CType(Me.hdrBusqueda, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrBusqueda.ResumeLayout(False)
        Me.tblBuscador.ResumeLayout(False)
        Me.tblBuscador.PerformLayout()
        CType(Me.tabGeneral, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabGeneral.ResumeLayout(False)
        CType(Me.tbpDatosGenerales, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpDatosGenerales.ResumeLayout(False)
        Me.tblOrganizadorDatos.ResumeLayout(False)
        Me.tblOrganizadorDatos.PerformLayout()
        CType(Me.tbpObservaciones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpObservaciones.ResumeLayout(False)
        CType(Me.grpCabecera.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpCabecera.Panel.ResumeLayout(False)
        Me.grpCabecera.Panel.PerformLayout()
        CType(Me.grpCabecera, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpCabecera.ResumeLayout(False)
        CType(Me.epErrores, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GestorErrores1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents splSeparador As ComponentFactory.Krypton.Toolkit.KryptonSplitContainer
    Friend WithEvents hdrBusqueda As ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup
    Friend WithEvents btnOcultarBusqueda As ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup
    Friend WithEvents lblTitulo As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents grpCabecera As ComponentFactory.Krypton.Toolkit.KryptonGroup
    Friend WithEvents epErrores As Quadralia.Controles.Validacion.GestorErrores
    Friend WithEvents lblBNombre As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtBNombre As Quadralia.Controles.aTextBox
    Friend WithEvents btnBBuscar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents grpResultados As ComponentFactory.Krypton.Toolkit.KryptonGroupBox
    Friend WithEvents dgvResultadosBuscador As ComponentFactory.Krypton.Toolkit.KryptonDataGridView
    Friend WithEvents tblBuscador As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents tabGeneral As ComponentFactory.Krypton.Navigator.KryptonNavigator
    Friend WithEvents tbpDatosGenerales As ComponentFactory.Krypton.Navigator.KryptonPage
    Friend WithEvents tbpObservaciones As ComponentFactory.Krypton.Navigator.KryptonPage
    Friend WithEvents htmlObservaciones As Escritorio.EditorHTML
    Friend WithEvents ttpInformaci�n As System.Windows.Forms.ToolTip
    Friend WithEvents chkBEliminados As ComponentFactory.Krypton.Toolkit.KryptonCheckBox
    Friend WithEvents GestorErrores1 As Escritorio.Quadralia.Controles.Validacion.GestorErrores
    Friend WithEvents tblOrganizadorDatos As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblRazonSocial As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblFax As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblTelefono1 As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblTelefono2 As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtRazonSocial As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents txtTelefono1 As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents txtTelefono2 As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents txtFax As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents chkReactivarRegistro As ComponentFactory.Krypton.Toolkit.KryptonCheckBox
    Friend WithEvents txtEmail As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents lblEmail As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblWeb As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtWeb As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents lblCIF As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtCIF As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents txtCodigo As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents lblCodigo As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents chkPredeterminado As ComponentFactory.Krypton.Toolkit.KryptonCheckBox
    Friend WithEvents txtDireccion As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents lblDireccion As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblRegistro As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtRegistro As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents vsrLogotipo As Escritorio.VisorImagenes
    Friend WithEvents lblCaducidad As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtCaducidad As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents txtBCIF As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents txtBRegistro As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents lblBCIF As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblBRegistro As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents colResultadoCIF As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents colResultadoTarifa As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn

End Class
