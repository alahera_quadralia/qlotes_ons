<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoteSalida
    Inherits FormularioHijo

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.lblobservaciones = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblFormaPago_id = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblCliente_id = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.splSeparador = New ComponentFactory.Krypton.Toolkit.KryptonSplitContainer()
        Me.hdrBusqueda = New ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup()
        Me.btnOcultarBusqueda = New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup()
        Me.tblOrganizadorBusqueda = New System.Windows.Forms.TableLayoutPanel()
        Me.txtBLoteSalida = New Escritorio.Quadralia.Controles.aTextBox()
        Me.btnLimpiarBusqueda = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.lblBcodigo = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.dtpBFechaInicio = New Escritorio.Quadralia.Controles.aDateTimePicker()
        Me.grpResultados = New ComponentFactory.Krypton.Toolkit.KryptonGroupBox()
        Me.dgvResultadosBuscador = New ComponentFactory.Krypton.Toolkit.KryptonDataGridView()
        Me.colResultadoCodigo = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colResultadoFecha = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.Proveedor = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.btnBBuscar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.lblBFechaInicio = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtBcodigo = New Escritorio.Quadralia.Controles.aTextBox()
        Me.lblBFechaFin = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.dtpBFechaFin = New Escritorio.Quadralia.Controles.aDateTimePicker()
        Me.chkBRegistrosEliminados = New ComponentFactory.Krypton.Toolkit.KryptonCheckBox()
        Me.lblBLoteSalida = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtBEtiqueta = New Escritorio.cBuscadorEtiqueta()
        Me.lblBEtiqueta = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblBCliente = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtBCliente = New Escritorio.cBuscadorCliente()
        Me.tabGeneral = New ComponentFactory.Krypton.Navigator.KryptonNavigator()
        Me.tbpDatos = New ComponentFactory.Krypton.Navigator.KryptonPage()
        Me.tblOrganizadorDatos = New System.Windows.Forms.TableLayoutPanel()
        Me.hdrCabeceraEmpaquetado = New ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup()
        Me.btnEtiquetasLector = New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup()
        Me.btnEtiquetasNuevo = New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup()
        Me.btnEtiquetasEliminar = New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup()
        Me.dgvEmpaquetados = New ComponentFactory.Krypton.Toolkit.KryptonDataGridView()
        Me.cboDireccionCliente = New Escritorio.Quadralia.Controles.aComboBox()
        Me.lblDireccionCliente = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.chkReactivarRegistro = New ComponentFactory.Krypton.Toolkit.KryptonCheckBox()
        Me.lblCodigo = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtCodigo = New ComponentFactory.Krypton.Toolkit.KryptonTextBox()
        Me.lblFecha = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblCliente = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.dtpFecha = New ComponentFactory.Krypton.Toolkit.KryptonDateTimePicker()
        Me.txtCliente = New Escritorio.cTextboxBuscador()
        Me.lblExpedidor = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.cboExpedidor = New Escritorio.Quadralia.Controles.aComboBox()
        Me.tbpObservaciones = New ComponentFactory.Krypton.Navigator.KryptonPage()
        Me.htmlObservaciones = New Escritorio.EditorHTML()
        Me.tbpEtiquetas = New ComponentFactory.Krypton.Navigator.KryptonPage()
        Me.dgvEtiquetas = New ComponentFactory.Krypton.Toolkit.KryptonDataGridView()
        Me.KryptonDataGridViewTextBoxColumn1 = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.Numero_Lote = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.KryptonDataGridViewTextBoxColumn2 = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colEtiquetasEmpaquetado = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.KryptonDataGridViewTextBoxColumn3 = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.tbpLotesEntrada = New ComponentFactory.Krypton.Navigator.KryptonPage()
        Me.dgvLotesEntrada = New ComponentFactory.Krypton.Toolkit.KryptonDataGridView()
        Me.colFacturaCodigo = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colFacturaFecha = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colCliente = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.lblTitulo = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.grpCabecera = New ComponentFactory.Krypton.Toolkit.KryptonGroup()
        Me.epErrores = New Escritorio.Quadralia.Controles.Validacion.GestorErrores()
        Me.bgwImprimir = New Escritorio.Quadralia.Controles.Threading.cBackGroundWorker()
        Me.colAlbEntrada = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colZonaFao = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colEspecie = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colPedo = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        CType(Me.splSeparador, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.splSeparador.Panel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.splSeparador.Panel1.SuspendLayout()
        CType(Me.splSeparador.Panel2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.splSeparador.Panel2.SuspendLayout()
        Me.splSeparador.SuspendLayout()
        CType(Me.hdrBusqueda, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.hdrBusqueda.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.hdrBusqueda.Panel.SuspendLayout()
        Me.hdrBusqueda.SuspendLayout()
        Me.tblOrganizadorBusqueda.SuspendLayout()
        CType(Me.grpResultados, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpResultados.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpResultados.Panel.SuspendLayout()
        Me.grpResultados.SuspendLayout()
        CType(Me.dgvResultadosBuscador, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tabGeneral, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabGeneral.SuspendLayout()
        CType(Me.tbpDatos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpDatos.SuspendLayout()
        Me.tblOrganizadorDatos.SuspendLayout()
        CType(Me.hdrCabeceraEmpaquetado, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.hdrCabeceraEmpaquetado.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.hdrCabeceraEmpaquetado.Panel.SuspendLayout()
        Me.hdrCabeceraEmpaquetado.SuspendLayout()
        CType(Me.dgvEmpaquetados, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboDireccionCliente, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboExpedidor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbpObservaciones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpObservaciones.SuspendLayout()
        CType(Me.tbpEtiquetas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpEtiquetas.SuspendLayout()
        CType(Me.dgvEtiquetas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbpLotesEntrada, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpLotesEntrada.SuspendLayout()
        CType(Me.dgvLotesEntrada, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpCabecera, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpCabecera.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpCabecera.Panel.SuspendLayout()
        Me.grpCabecera.SuspendLayout()
        CType(Me.epErrores, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblobservaciones
        '
        Me.lblobservaciones.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblobservaciones.Location = New System.Drawing.Point(34, 464)
        Me.lblobservaciones.Name = "lblobservaciones"
        Me.lblobservaciones.Size = New System.Drawing.Size(88, 20)
        Me.lblobservaciones.TabIndex = 0
        Me.lblobservaciones.Values.Text = "observaciones"
        '
        'lblFormaPago_id
        '
        Me.lblFormaPago_id.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblFormaPago_id.Location = New System.Drawing.Point(34, 664)
        Me.lblFormaPago_id.Name = "lblFormaPago_id"
        Me.lblFormaPago_id.Size = New System.Drawing.Size(88, 20)
        Me.lblFormaPago_id.TabIndex = 0
        Me.lblFormaPago_id.Values.Text = "FormaPago_id"
        '
        'lblCliente_id
        '
        Me.lblCliente_id.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblCliente_id.Location = New System.Drawing.Point(34, 684)
        Me.lblCliente_id.Name = "lblCliente_id"
        Me.lblCliente_id.Size = New System.Drawing.Size(64, 20)
        Me.lblCliente_id.TabIndex = 0
        Me.lblCliente_id.Values.Text = "Cliente_id"
        '
        'splSeparador
        '
        Me.splSeparador.Cursor = System.Windows.Forms.Cursors.Default
        Me.splSeparador.Dock = System.Windows.Forms.DockStyle.Fill
        Me.splSeparador.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.splSeparador.Location = New System.Drawing.Point(0, 57)
        Me.splSeparador.Name = "splSeparador"
        '
        'splSeparador.Panel1
        '
        Me.splSeparador.Panel1.Controls.Add(Me.hdrBusqueda)
        Me.splSeparador.Panel1MinSize = 220
        '
        'splSeparador.Panel2
        '
        Me.splSeparador.Panel2.Controls.Add(Me.tabGeneral)
        Me.splSeparador.Size = New System.Drawing.Size(881, 452)
        Me.splSeparador.SplitterDistance = 227
        Me.splSeparador.TabIndex = 1
        '
        'hdrBusqueda
        '
        Me.hdrBusqueda.AutoSize = True
        Me.hdrBusqueda.ButtonSpecs.AddRange(New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup() {Me.btnOcultarBusqueda})
        Me.hdrBusqueda.Dock = System.Windows.Forms.DockStyle.Fill
        Me.hdrBusqueda.HeaderVisibleSecondary = False
        Me.hdrBusqueda.Location = New System.Drawing.Point(0, 0)
        Me.hdrBusqueda.Name = "hdrBusqueda"
        '
        'hdrBusqueda.Panel
        '
        Me.hdrBusqueda.Panel.Controls.Add(Me.tblOrganizadorBusqueda)
        Me.hdrBusqueda.Size = New System.Drawing.Size(227, 452)
        Me.hdrBusqueda.TabIndex = 0
        Me.hdrBusqueda.ValuesPrimary.Heading = "Buscar"
        Me.hdrBusqueda.ValuesPrimary.Image = Global.Escritorio.My.Resources.Resources.Buscar_32
        '
        'btnOcultarBusqueda
        '
        Me.btnOcultarBusqueda.Type = ComponentFactory.Krypton.Toolkit.PaletteButtonSpecStyle.ArrowLeft
        Me.btnOcultarBusqueda.UniqueName = "BE70F7722CF94C60618F65819BBF010D"
        '
        'tblOrganizadorBusqueda
        '
        Me.tblOrganizadorBusqueda.BackColor = System.Drawing.Color.White
        Me.tblOrganizadorBusqueda.ColumnCount = 2
        Me.tblOrganizadorBusqueda.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblOrganizadorBusqueda.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblOrganizadorBusqueda.Controls.Add(Me.txtBLoteSalida, 1, 6)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.btnLimpiarBusqueda, 0, 0)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.lblBcodigo, 0, 1)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.dtpBFechaInicio, 1, 2)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.grpResultados, 0, 9)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.btnBBuscar, 0, 8)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.lblBFechaInicio, 0, 2)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.txtBcodigo, 1, 1)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.lblBFechaFin, 0, 3)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.dtpBFechaFin, 1, 3)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.chkBRegistrosEliminados, 0, 7)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.lblBLoteSalida, 0, 6)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.txtBEtiqueta, 1, 5)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.lblBEtiqueta, 0, 5)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.lblBCliente, 0, 4)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.txtBCliente, 1, 4)
        Me.tblOrganizadorBusqueda.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblOrganizadorBusqueda.Location = New System.Drawing.Point(0, 0)
        Me.tblOrganizadorBusqueda.Name = "tblOrganizadorBusqueda"
        Me.tblOrganizadorBusqueda.RowCount = 10
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorBusqueda.Size = New System.Drawing.Size(225, 415)
        Me.tblOrganizadorBusqueda.TabIndex = 1
        '
        'txtBLoteSalida
        '
        Me.txtBLoteSalida.AlwaysActive = False
        Me.txtBLoteSalida.controlarBotonBorrar = True
        Me.txtBLoteSalida.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtBLoteSalida.Formato = ""
        Me.txtBLoteSalida.Location = New System.Drawing.Point(88, 166)
        Me.txtBLoteSalida.MaxLength = 48
        Me.txtBLoteSalida.mostrarSiempreBotonBorrar = False
        Me.txtBLoteSalida.Name = "txtBLoteSalida"
        Me.txtBLoteSalida.seleccionarTodo = True
        Me.txtBLoteSalida.Size = New System.Drawing.Size(134, 23)
        Me.txtBLoteSalida.TabIndex = 17
        '
        'btnLimpiarBusqueda
        '
        Me.btnLimpiarBusqueda.AutoSize = True
        Me.btnLimpiarBusqueda.ButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.ButtonSpec
        Me.tblOrganizadorBusqueda.SetColumnSpan(Me.btnLimpiarBusqueda, 2)
        Me.btnLimpiarBusqueda.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnLimpiarBusqueda.Location = New System.Drawing.Point(97, 3)
        Me.btnLimpiarBusqueda.Name = "btnLimpiarBusqueda"
        Me.btnLimpiarBusqueda.Size = New System.Drawing.Size(125, 22)
        Me.btnLimpiarBusqueda.TabIndex = 13
        Me.btnLimpiarBusqueda.Values.Image = Global.Escritorio.My.Resources.Resources.Limpiar_16
        Me.btnLimpiarBusqueda.Values.Text = "Limpiar Búsqueda"
        '
        'lblBcodigo
        '
        Me.lblBcodigo.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblBcodigo.Location = New System.Drawing.Point(34, 31)
        Me.lblBcodigo.Name = "lblBcodigo"
        Me.lblBcodigo.Size = New System.Drawing.Size(48, 23)
        Me.lblBcodigo.TabIndex = 0
        Me.lblBcodigo.Values.Text = "Cliente"
        '
        'dtpBFechaInicio
        '
        Me.dtpBFechaInicio.CalendarTodayText = "Hoy:"
        Me.dtpBFechaInicio.Checked = False
        Me.dtpBFechaInicio.controlarBotonBorrar = True
        Me.dtpBFechaInicio.CustomNullText = "(Sin fecha)"
        Me.dtpBFechaInicio.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dtpBFechaInicio.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpBFechaInicio.Location = New System.Drawing.Point(88, 60)
        Me.dtpBFechaInicio.mostrarSiempreBotonBorrar = False
        Me.dtpBFechaInicio.Name = "dtpBFechaInicio"
        Me.dtpBFechaInicio.Size = New System.Drawing.Size(134, 21)
        Me.dtpBFechaInicio.TabIndex = 3
        Me.dtpBFechaInicio.TipoLimpieza = Escritorio.Quadralia.Controles.aDateTimePicker.TiposLimpieza.ValueYValueNullable
        '
        'grpResultados
        '
        Me.tblOrganizadorBusqueda.SetColumnSpan(Me.grpResultados, 2)
        Me.grpResultados.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grpResultados.Location = New System.Drawing.Point(3, 252)
        Me.grpResultados.Name = "grpResultados"
        '
        'grpResultados.Panel
        '
        Me.grpResultados.Panel.Controls.Add(Me.dgvResultadosBuscador)
        Me.grpResultados.Size = New System.Drawing.Size(219, 160)
        Me.grpResultados.TabIndex = 10
        Me.grpResultados.Text = "Resultados"
        Me.grpResultados.Values.Heading = "Resultados"
        '
        'dgvResultadosBuscador
        '
        Me.dgvResultadosBuscador.AllowUserToAddRows = False
        Me.dgvResultadosBuscador.AllowUserToDeleteRows = False
        Me.dgvResultadosBuscador.AllowUserToResizeRows = False
        Me.dgvResultadosBuscador.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colResultadoCodigo, Me.colResultadoFecha, Me.Proveedor})
        Me.dgvResultadosBuscador.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvResultadosBuscador.Location = New System.Drawing.Point(0, 0)
        Me.dgvResultadosBuscador.Name = "dgvResultadosBuscador"
        Me.dgvResultadosBuscador.ReadOnly = True
        Me.dgvResultadosBuscador.RowHeadersVisible = False
        Me.dgvResultadosBuscador.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvResultadosBuscador.Size = New System.Drawing.Size(215, 136)
        Me.dgvResultadosBuscador.TabIndex = 0
        '
        'colResultadoCodigo
        '
        Me.colResultadoCodigo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colResultadoCodigo.DataPropertyName = "codigo"
        Me.colResultadoCodigo.HeaderText = "Cód."
        Me.colResultadoCodigo.Name = "colResultadoCodigo"
        Me.colResultadoCodigo.ReadOnly = True
        Me.colResultadoCodigo.Width = 61
        '
        'colResultadoFecha
        '
        Me.colResultadoFecha.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colResultadoFecha.DataPropertyName = "fecha"
        DataGridViewCellStyle1.Format = "d"
        DataGridViewCellStyle1.NullValue = Nothing
        Me.colResultadoFecha.DefaultCellStyle = DataGridViewCellStyle1
        Me.colResultadoFecha.HeaderText = "Fecha"
        Me.colResultadoFecha.Name = "colResultadoFecha"
        Me.colResultadoFecha.ReadOnly = True
        Me.colResultadoFecha.Width = 67
        '
        'Proveedor
        '
        Me.Proveedor.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Proveedor.DataPropertyName = "nombreCliente"
        Me.Proveedor.HeaderText = "Proveedor"
        Me.Proveedor.MinimumWidth = 80
        Me.Proveedor.Name = "Proveedor"
        Me.Proveedor.ReadOnly = True
        Me.Proveedor.Width = 86
        '
        'btnBBuscar
        '
        Me.tblOrganizadorBusqueda.SetColumnSpan(Me.btnBBuscar, 2)
        Me.btnBBuscar.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnBBuscar.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnBBuscar.Location = New System.Drawing.Point(3, 221)
        Me.btnBBuscar.Name = "btnBBuscar"
        Me.btnBBuscar.Size = New System.Drawing.Size(219, 25)
        Me.btnBBuscar.TabIndex = 9
        Me.btnBBuscar.Values.Text = "Buscar"
        '
        'lblBFechaInicio
        '
        Me.lblBFechaInicio.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblBFechaInicio.Location = New System.Drawing.Point(7, 60)
        Me.lblBFechaInicio.Name = "lblBFechaInicio"
        Me.lblBFechaInicio.Size = New System.Drawing.Size(75, 21)
        Me.lblBFechaInicio.TabIndex = 2
        Me.lblBFechaInicio.Values.Text = "Fecha Inicio"
        '
        'txtBcodigo
        '
        Me.txtBcodigo.AlwaysActive = False
        Me.txtBcodigo.controlarBotonBorrar = True
        Me.txtBcodigo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtBcodigo.Formato = ""
        Me.txtBcodigo.Location = New System.Drawing.Point(88, 31)
        Me.txtBcodigo.MaxLength = 48
        Me.txtBcodigo.mostrarSiempreBotonBorrar = False
        Me.txtBcodigo.Name = "txtBcodigo"
        Me.txtBcodigo.seleccionarTodo = True
        Me.txtBcodigo.Size = New System.Drawing.Size(134, 23)
        Me.txtBcodigo.TabIndex = 1
        '
        'lblBFechaFin
        '
        Me.lblBFechaFin.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblBFechaFin.Location = New System.Drawing.Point(21, 87)
        Me.lblBFechaFin.Name = "lblBFechaFin"
        Me.lblBFechaFin.Size = New System.Drawing.Size(61, 21)
        Me.lblBFechaFin.TabIndex = 4
        Me.lblBFechaFin.Values.Text = "Fecha Fin"
        '
        'dtpBFechaFin
        '
        Me.dtpBFechaFin.CalendarTodayText = "Hoy:"
        Me.dtpBFechaFin.Checked = False
        Me.dtpBFechaFin.controlarBotonBorrar = True
        Me.dtpBFechaFin.CustomNullText = "(Sin fecha)"
        Me.dtpBFechaFin.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dtpBFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpBFechaFin.Location = New System.Drawing.Point(88, 87)
        Me.dtpBFechaFin.mostrarSiempreBotonBorrar = False
        Me.dtpBFechaFin.Name = "dtpBFechaFin"
        Me.dtpBFechaFin.Size = New System.Drawing.Size(134, 21)
        Me.dtpBFechaFin.TabIndex = 5
        Me.dtpBFechaFin.TipoLimpieza = Escritorio.Quadralia.Controles.aDateTimePicker.TiposLimpieza.ValueYValueNullable
        '
        'chkBRegistrosEliminados
        '
        Me.tblOrganizadorBusqueda.SetColumnSpan(Me.chkBRegistrosEliminados, 2)
        Me.chkBRegistrosEliminados.Dock = System.Windows.Forms.DockStyle.Fill
        Me.chkBRegistrosEliminados.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl
        Me.chkBRegistrosEliminados.Location = New System.Drawing.Point(3, 195)
        Me.chkBRegistrosEliminados.Name = "chkBRegistrosEliminados"
        Me.chkBRegistrosEliminados.Size = New System.Drawing.Size(219, 20)
        Me.chkBRegistrosEliminados.TabIndex = 8
        Me.chkBRegistrosEliminados.Text = "Incluir registros eliminados"
        Me.chkBRegistrosEliminados.Values.Text = "Incluir registros eliminados"
        '
        'lblBLoteSalida
        '
        Me.lblBLoteSalida.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblBLoteSalida.Location = New System.Drawing.Point(3, 166)
        Me.lblBLoteSalida.Name = "lblBLoteSalida"
        Me.lblBLoteSalida.Size = New System.Drawing.Size(79, 23)
        Me.lblBLoteSalida.TabIndex = 0
        Me.lblBLoteSalida.Values.Text = "Lote Entrada"
        '
        'txtBEtiqueta
        '
        Me.txtBEtiqueta.DescripcionReadOnly = False
        Me.txtBEtiqueta.DescripcionVisible = False
        Me.txtBEtiqueta.DisplayMember = "Descripcion"
        Me.txtBEtiqueta.DisplayText = ""
        Me.txtBEtiqueta.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtBEtiqueta.FormularioBusqueda = "Escritorio.frmSeleccionarEtiqueta"
        Me.txtBEtiqueta.Item = Nothing
        Me.txtBEtiqueta.Location = New System.Drawing.Point(88, 140)
        Me.txtBEtiqueta.Name = "txtBEtiqueta"
        Me.txtBEtiqueta.Size = New System.Drawing.Size(134, 20)
        Me.txtBEtiqueta.TabIndex = 15
        Me.txtBEtiqueta.UseOnlyNumbers = False
        Me.txtBEtiqueta.UseUpperCase = True
        Me.txtBEtiqueta.Validar = True
        Me.txtBEtiqueta.ValueMember = "Codigo"
        Me.txtBEtiqueta.ValueText = ""
        '
        'lblBEtiqueta
        '
        Me.lblBEtiqueta.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblBEtiqueta.Location = New System.Drawing.Point(27, 140)
        Me.lblBEtiqueta.Name = "lblBEtiqueta"
        Me.lblBEtiqueta.Size = New System.Drawing.Size(55, 20)
        Me.lblBEtiqueta.TabIndex = 0
        Me.lblBEtiqueta.Values.Text = "Etiqueta"
        '
        'lblBCliente
        '
        Me.lblBCliente.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblBCliente.Location = New System.Drawing.Point(34, 114)
        Me.lblBCliente.Name = "lblBCliente"
        Me.lblBCliente.Size = New System.Drawing.Size(48, 20)
        Me.lblBCliente.TabIndex = 0
        Me.lblBCliente.Values.Text = "Cliente"
        '
        'txtBCliente
        '
        Me.txtBCliente.DescripcionReadOnly = False
        Me.txtBCliente.DescripcionVisible = False
        Me.txtBCliente.DisplayMember = "nombreComercial"
        Me.txtBCliente.DisplayText = ""
        Me.txtBCliente.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtBCliente.FormularioBusqueda = "Escritorio.frmSeleccionarCliente"
        Me.txtBCliente.Item = Nothing
        Me.txtBCliente.Location = New System.Drawing.Point(88, 114)
        Me.txtBCliente.Name = "txtBCliente"
        Me.txtBCliente.Size = New System.Drawing.Size(134, 20)
        Me.txtBCliente.TabIndex = 16
        Me.txtBCliente.UseOnlyNumbers = False
        Me.txtBCliente.UseUpperCase = True
        Me.txtBCliente.Validar = True
        Me.txtBCliente.ValueMember = "codigo"
        Me.txtBCliente.ValueText = ""
        '
        'tabGeneral
        '
        Me.tabGeneral.AllowPageReorder = False
        Me.tabGeneral.Button.CloseButtonAction = ComponentFactory.Krypton.Navigator.CloseButtonAction.None
        Me.tabGeneral.Button.CloseButtonDisplay = ComponentFactory.Krypton.Navigator.ButtonDisplay.Hide
        Me.tabGeneral.Button.ContextButtonAction = ComponentFactory.Krypton.Navigator.ContextButtonAction.None
        Me.tabGeneral.Button.ContextButtonDisplay = ComponentFactory.Krypton.Navigator.ButtonDisplay.Hide
        Me.tabGeneral.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabGeneral.Location = New System.Drawing.Point(0, 0)
        Me.tabGeneral.Name = "tabGeneral"
        Me.tabGeneral.Pages.AddRange(New ComponentFactory.Krypton.Navigator.KryptonPage() {Me.tbpDatos, Me.tbpObservaciones, Me.tbpEtiquetas, Me.tbpLotesEntrada})
        Me.tabGeneral.SelectedIndex = 0
        Me.tabGeneral.Size = New System.Drawing.Size(649, 452)
        Me.tabGeneral.TabIndex = 0
        Me.tabGeneral.Text = "KryptonNavigator1"
        '
        'tbpDatos
        '
        Me.tbpDatos.AutoHiddenSlideSize = New System.Drawing.Size(200, 200)
        Me.tbpDatos.Controls.Add(Me.tblOrganizadorDatos)
        Me.tbpDatos.Controls.Add(Me.lblCliente_id)
        Me.tbpDatos.Controls.Add(Me.lblFormaPago_id)
        Me.tbpDatos.Controls.Add(Me.lblobservaciones)
        Me.tbpDatos.Flags = 65534
        Me.tbpDatos.LastVisibleSet = True
        Me.tbpDatos.MinimumSize = New System.Drawing.Size(50, 50)
        Me.tbpDatos.Name = "tbpDatos"
        Me.tbpDatos.Size = New System.Drawing.Size(647, 425)
        Me.tbpDatos.Text = "Datos"
        Me.tbpDatos.ToolTipTitle = "Page ToolTip"
        Me.tbpDatos.UniqueName = "A7344070A3E64BA24893A75E8104A8EE"
        '
        'tblOrganizadorDatos
        '
        Me.tblOrganizadorDatos.BackColor = System.Drawing.Color.FromArgb(CType(CType(187, Byte), Integer), CType(CType(206, Byte), Integer), CType(CType(230, Byte), Integer))
        Me.tblOrganizadorDatos.ColumnCount = 6
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblOrganizadorDatos.Controls.Add(Me.hdrCabeceraEmpaquetado, 0, 5)
        Me.tblOrganizadorDatos.Controls.Add(Me.cboDireccionCliente, 2, 3)
        Me.tblOrganizadorDatos.Controls.Add(Me.lblDireccionCliente, 0, 3)
        Me.tblOrganizadorDatos.Controls.Add(Me.chkReactivarRegistro, 0, 4)
        Me.tblOrganizadorDatos.Controls.Add(Me.lblCodigo, 0, 0)
        Me.tblOrganizadorDatos.Controls.Add(Me.txtCodigo, 2, 0)
        Me.tblOrganizadorDatos.Controls.Add(Me.lblFecha, 3, 0)
        Me.tblOrganizadorDatos.Controls.Add(Me.lblCliente, 0, 2)
        Me.tblOrganizadorDatos.Controls.Add(Me.dtpFecha, 5, 0)
        Me.tblOrganizadorDatos.Controls.Add(Me.txtCliente, 2, 2)
        Me.tblOrganizadorDatos.Controls.Add(Me.lblExpedidor, 0, 1)
        Me.tblOrganizadorDatos.Controls.Add(Me.cboExpedidor, 2, 1)
        Me.tblOrganizadorDatos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblOrganizadorDatos.Location = New System.Drawing.Point(0, 0)
        Me.tblOrganizadorDatos.Name = "tblOrganizadorDatos"
        Me.tblOrganizadorDatos.RowCount = 6
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorDatos.Size = New System.Drawing.Size(647, 425)
        Me.tblOrganizadorDatos.TabIndex = 0
        '
        'hdrCabeceraEmpaquetado
        '
        Me.hdrCabeceraEmpaquetado.ButtonSpecs.AddRange(New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup() {Me.btnEtiquetasLector, Me.btnEtiquetasNuevo, Me.btnEtiquetasEliminar})
        Me.tblOrganizadorDatos.SetColumnSpan(Me.hdrCabeceraEmpaquetado, 6)
        Me.hdrCabeceraEmpaquetado.Dock = System.Windows.Forms.DockStyle.Fill
        Me.hdrCabeceraEmpaquetado.HeaderVisibleSecondary = False
        Me.hdrCabeceraEmpaquetado.Location = New System.Drawing.Point(3, 138)
        Me.hdrCabeceraEmpaquetado.Name = "hdrCabeceraEmpaquetado"
        '
        'hdrCabeceraEmpaquetado.Panel
        '
        Me.hdrCabeceraEmpaquetado.Panel.Controls.Add(Me.dgvEmpaquetados)
        Me.hdrCabeceraEmpaquetado.Size = New System.Drawing.Size(641, 284)
        Me.hdrCabeceraEmpaquetado.TabIndex = 29
        Me.hdrCabeceraEmpaquetado.ValuesPrimary.Heading = "Empaquetados"
        Me.hdrCabeceraEmpaquetado.ValuesPrimary.Image = Nothing
        '
        'btnEtiquetasLector
        '
        Me.btnEtiquetasLector.Image = Global.Escritorio.My.Resources.Resources.Lector_16
        Me.btnEtiquetasLector.ToolTipBody = "Asigne los empaquetados desde un lector de código de barras"
        Me.btnEtiquetasLector.UniqueName = "9BE9D9632284423B2ABB2FD12217E154"
        '
        'btnEtiquetasNuevo
        '
        Me.btnEtiquetasNuevo.Image = Global.Escritorio.My.Resources.Resources.Nuevo_16
        Me.btnEtiquetasNuevo.ToolTipBody = "Añade una nuevo empaquetado al lote actual"
        Me.btnEtiquetasNuevo.UniqueName = "2610D5F6A66A4839F296F2CECC529D8B"
        '
        'btnEtiquetasEliminar
        '
        Me.btnEtiquetasEliminar.Image = Global.Escritorio.My.Resources.Resources.Eliminar_16
        Me.btnEtiquetasEliminar.ToolTipBody = "Elimina el empaquetado seleccionado del lote actual"
        Me.btnEtiquetasEliminar.UniqueName = "D043D1EDBA0140AF06A17DFF7EB93779"
        '
        'dgvEmpaquetados
        '
        Me.dgvEmpaquetados.AllowUserToAddRows = False
        Me.dgvEmpaquetados.AllowUserToDeleteRows = False
        Me.dgvEmpaquetados.AllowUserToResizeRows = False
        Me.dgvEmpaquetados.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colAlbEntrada, Me.colZonaFao, Me.colEspecie, Me.colPedo})
        Me.dgvEmpaquetados.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvEmpaquetados.Location = New System.Drawing.Point(0, 0)
        Me.dgvEmpaquetados.Name = "dgvEmpaquetados"
        Me.dgvEmpaquetados.RowHeadersVisible = False
        Me.dgvEmpaquetados.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEmpaquetados.Size = New System.Drawing.Size(639, 252)
        Me.dgvEmpaquetados.TabIndex = 1
        '
        'cboDireccionCliente
        '
        Me.tblOrganizadorDatos.SetColumnSpan(Me.cboDireccionCliente, 4)
        Me.cboDireccionCliente.controlarBotonBorrar = True
        Me.cboDireccionCliente.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cboDireccionCliente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDireccionCliente.DropDownWidth = 131
        Me.cboDireccionCliente.Location = New System.Drawing.Point(136, 85)
        Me.cboDireccionCliente.mostrarSiempreBotonBorrar = False
        Me.cboDireccionCliente.Name = "cboDireccionCliente"
        Me.cboDireccionCliente.Size = New System.Drawing.Size(508, 21)
        Me.cboDireccionCliente.TabIndex = 28
        '
        'lblDireccionCliente
        '
        Me.lblDireccionCliente.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblDireccionCliente.Location = New System.Drawing.Point(3, 85)
        Me.lblDireccionCliente.Name = "lblDireccionCliente"
        Me.lblDireccionCliente.Size = New System.Drawing.Size(107, 21)
        Me.lblDireccionCliente.TabIndex = 27
        Me.lblDireccionCliente.Values.Text = "Dirección entrega"
        '
        'chkReactivarRegistro
        '
        Me.tblOrganizadorDatos.SetColumnSpan(Me.chkReactivarRegistro, 6)
        Me.chkReactivarRegistro.Dock = System.Windows.Forms.DockStyle.Fill
        Me.chkReactivarRegistro.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl
        Me.chkReactivarRegistro.Location = New System.Drawing.Point(3, 112)
        Me.chkReactivarRegistro.Name = "chkReactivarRegistro"
        Me.chkReactivarRegistro.Size = New System.Drawing.Size(641, 20)
        Me.chkReactivarRegistro.TabIndex = 26
        Me.chkReactivarRegistro.Text = "Reactivar el registro"
        Me.chkReactivarRegistro.Values.Text = "Reactivar el registro"
        Me.chkReactivarRegistro.Visible = False
        '
        'lblCodigo
        '
        Me.lblCodigo.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblCodigo.Location = New System.Drawing.Point(55, 3)
        Me.lblCodigo.Name = "lblCodigo"
        Me.lblCodigo.Size = New System.Drawing.Size(55, 23)
        Me.lblCodigo.TabIndex = 0
        Me.lblCodigo.Values.Text = "Número"
        '
        'txtCodigo
        '
        Me.txtCodigo.AlwaysActive = False
        Me.txtCodigo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtCodigo.Enabled = False
        Me.txtCodigo.Location = New System.Drawing.Point(136, 3)
        Me.txtCodigo.MaxLength = 48
        Me.txtCodigo.MinimumSize = New System.Drawing.Size(80, 20)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(217, 23)
        Me.txtCodigo.TabIndex = 1
        Me.txtCodigo.Text = "codigo"
        '
        'lblFecha
        '
        Me.lblFecha.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblFecha.Location = New System.Drawing.Point(359, 3)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(42, 23)
        Me.lblFecha.TabIndex = 2
        Me.lblFecha.Values.Text = "Fecha"
        '
        'lblCliente
        '
        Me.lblCliente.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblCliente.Location = New System.Drawing.Point(62, 59)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(48, 20)
        Me.lblCliente.TabIndex = 4
        Me.lblCliente.Values.Text = "Cliente"
        '
        'dtpFecha
        '
        Me.dtpFecha.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecha.Location = New System.Drawing.Point(427, 3)
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(217, 23)
        Me.dtpFecha.TabIndex = 3
        Me.dtpFecha.ValueNullable = New Date(CType(0, Long))
        '
        'txtCliente
        '
        Me.tblOrganizadorDatos.SetColumnSpan(Me.txtCliente, 4)
        Me.txtCliente.DescripcionReadOnly = False
        Me.txtCliente.DescripcionVisible = True
        Me.txtCliente.DisplayMember = "nombreComercial"
        Me.txtCliente.DisplayText = ""
        Me.txtCliente.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtCliente.FormularioBusqueda = "Escritorio.frmSeleccionarCliente"
        Me.txtCliente.Item = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(136, 59)
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Size = New System.Drawing.Size(508, 20)
        Me.txtCliente.TabIndex = 5
        Me.txtCliente.UseOnlyNumbers = False
        Me.txtCliente.UseUpperCase = True
        Me.txtCliente.Validar = True
        Me.txtCliente.ValueMember = "codigo"
        Me.txtCliente.ValueText = ""
        '
        'lblExpedidor
        '
        Me.lblExpedidor.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblExpedidor.Location = New System.Drawing.Point(45, 32)
        Me.lblExpedidor.Name = "lblExpedidor"
        Me.lblExpedidor.Size = New System.Drawing.Size(65, 21)
        Me.lblExpedidor.TabIndex = 4
        Me.lblExpedidor.Values.Text = "Expedidor"
        '
        'cboExpedidor
        '
        Me.tblOrganizadorDatos.SetColumnSpan(Me.cboExpedidor, 4)
        Me.cboExpedidor.controlarBotonBorrar = True
        Me.cboExpedidor.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cboExpedidor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExpedidor.DropDownWidth = 131
        Me.cboExpedidor.Location = New System.Drawing.Point(136, 32)
        Me.cboExpedidor.mostrarSiempreBotonBorrar = False
        Me.cboExpedidor.Name = "cboExpedidor"
        Me.cboExpedidor.Size = New System.Drawing.Size(508, 21)
        Me.cboExpedidor.TabIndex = 12
        '
        'tbpObservaciones
        '
        Me.tbpObservaciones.AutoHiddenSlideSize = New System.Drawing.Size(200, 200)
        Me.tbpObservaciones.Controls.Add(Me.htmlObservaciones)
        Me.tbpObservaciones.Flags = 65534
        Me.tbpObservaciones.LastVisibleSet = True
        Me.tbpObservaciones.MinimumSize = New System.Drawing.Size(50, 50)
        Me.tbpObservaciones.Name = "tbpObservaciones"
        Me.tbpObservaciones.Size = New System.Drawing.Size(647, 425)
        Me.tbpObservaciones.Text = "Observaciones"
        Me.tbpObservaciones.ToolTipTitle = "Page ToolTip"
        Me.tbpObservaciones.UniqueName = "7E9D077196D249599B9E2159EC0B7EDD"
        '
        'htmlObservaciones
        '
        Me.htmlObservaciones.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.htmlObservaciones.ConvertirImagenesABase64 = False
        Me.htmlObservaciones.Dock = System.Windows.Forms.DockStyle.Fill
        Me.htmlObservaciones.Location = New System.Drawing.Point(0, 0)
        Me.htmlObservaciones.MostrarBarrasEdicion = True
        Me.htmlObservaciones.Name = "htmlObservaciones"
        Me.htmlObservaciones.ReadOnly = False
        Me.htmlObservaciones.Size = New System.Drawing.Size(647, 425)
        Me.htmlObservaciones.TabIndex = 0
        '
        'tbpEtiquetas
        '
        Me.tbpEtiquetas.AutoHiddenSlideSize = New System.Drawing.Size(200, 200)
        Me.tbpEtiquetas.Controls.Add(Me.dgvEtiquetas)
        Me.tbpEtiquetas.Flags = 65534
        Me.tbpEtiquetas.LastVisibleSet = True
        Me.tbpEtiquetas.MinimumSize = New System.Drawing.Size(50, 50)
        Me.tbpEtiquetas.Name = "tbpEtiquetas"
        Me.tbpEtiquetas.Size = New System.Drawing.Size(647, 425)
        Me.tbpEtiquetas.Text = "Etiquetas"
        Me.tbpEtiquetas.ToolTipTitle = "Page ToolTip"
        Me.tbpEtiquetas.UniqueName = "BEF30F3987B54F2B2E969736244398F1"
        '
        'dgvEtiquetas
        '
        Me.dgvEtiquetas.AllowUserToAddRows = False
        Me.dgvEtiquetas.AllowUserToDeleteRows = False
        Me.dgvEtiquetas.AllowUserToResizeRows = False
        Me.dgvEtiquetas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.KryptonDataGridViewTextBoxColumn1, Me.Numero_Lote, Me.KryptonDataGridViewTextBoxColumn2, Me.colEtiquetasEmpaquetado, Me.KryptonDataGridViewTextBoxColumn3})
        Me.dgvEtiquetas.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvEtiquetas.Location = New System.Drawing.Point(0, 0)
        Me.dgvEtiquetas.Name = "dgvEtiquetas"
        Me.dgvEtiquetas.ReadOnly = True
        Me.dgvEtiquetas.RowHeadersVisible = False
        Me.dgvEtiquetas.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvEtiquetas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEtiquetas.Size = New System.Drawing.Size(647, 425)
        Me.dgvEtiquetas.TabIndex = 15
        '
        'KryptonDataGridViewTextBoxColumn1
        '
        Me.KryptonDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.KryptonDataGridViewTextBoxColumn1.DataPropertyName = "codigo"
        Me.KryptonDataGridViewTextBoxColumn1.HeaderText = "Codigo"
        Me.KryptonDataGridViewTextBoxColumn1.MinimumWidth = 80
        Me.KryptonDataGridViewTextBoxColumn1.Name = "KryptonDataGridViewTextBoxColumn1"
        Me.KryptonDataGridViewTextBoxColumn1.ReadOnly = True
        Me.KryptonDataGridViewTextBoxColumn1.Width = 80
        '
        'Numero_Lote
        '
        Me.Numero_Lote.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.Numero_Lote.DataPropertyName = "Numero_Lote"
        Me.Numero_Lote.HeaderText = "Nº-Lote"
        Me.Numero_Lote.Name = "Numero_Lote"
        Me.Numero_Lote.ReadOnly = True
        Me.Numero_Lote.Width = 78
        '
        'KryptonDataGridViewTextBoxColumn2
        '
        Me.KryptonDataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.KryptonDataGridViewTextBoxColumn2.DataPropertyName = "fecha"
        DataGridViewCellStyle3.Format = "d"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.KryptonDataGridViewTextBoxColumn2.DefaultCellStyle = DataGridViewCellStyle3
        Me.KryptonDataGridViewTextBoxColumn2.HeaderText = "Fecha"
        Me.KryptonDataGridViewTextBoxColumn2.Name = "KryptonDataGridViewTextBoxColumn2"
        Me.KryptonDataGridViewTextBoxColumn2.ReadOnly = True
        Me.KryptonDataGridViewTextBoxColumn2.Width = 67
        '
        'colEtiquetasEmpaquetado
        '
        Me.colEtiquetasEmpaquetado.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colEtiquetasEmpaquetado.DataPropertyName = "CodigoEmpaquetado"
        Me.colEtiquetasEmpaquetado.HeaderText = "Empaquetado"
        Me.colEtiquetasEmpaquetado.Name = "colEtiquetasEmpaquetado"
        Me.colEtiquetasEmpaquetado.ReadOnly = True
        Me.colEtiquetasEmpaquetado.Width = 110
        '
        'KryptonDataGridViewTextBoxColumn3
        '
        Me.KryptonDataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.KryptonDataGridViewTextBoxColumn3.DataPropertyName = "nombreProveedor"
        Me.KryptonDataGridViewTextBoxColumn3.HeaderText = "Proveedor"
        Me.KryptonDataGridViewTextBoxColumn3.Name = "KryptonDataGridViewTextBoxColumn3"
        Me.KryptonDataGridViewTextBoxColumn3.ReadOnly = True
        Me.KryptonDataGridViewTextBoxColumn3.Width = 311
        '
        'tbpLotesEntrada
        '
        Me.tbpLotesEntrada.AutoHiddenSlideSize = New System.Drawing.Size(200, 200)
        Me.tbpLotesEntrada.Controls.Add(Me.dgvLotesEntrada)
        Me.tbpLotesEntrada.Flags = 65534
        Me.tbpLotesEntrada.LastVisibleSet = True
        Me.tbpLotesEntrada.MinimumSize = New System.Drawing.Size(50, 50)
        Me.tbpLotesEntrada.Name = "tbpLotesEntrada"
        Me.tbpLotesEntrada.Size = New System.Drawing.Size(647, 425)
        Me.tbpLotesEntrada.Text = "Albaranes Entrada"
        Me.tbpLotesEntrada.ToolTipTitle = "Page ToolTip"
        Me.tbpLotesEntrada.UniqueName = "8795A1C22875417A22AF7CEB7A67B981"
        '
        'dgvLotesEntrada
        '
        Me.dgvLotesEntrada.AllowUserToAddRows = False
        Me.dgvLotesEntrada.AllowUserToDeleteRows = False
        Me.dgvLotesEntrada.AllowUserToResizeRows = False
        Me.dgvLotesEntrada.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colFacturaCodigo, Me.colFacturaFecha, Me.colCliente})
        Me.dgvLotesEntrada.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvLotesEntrada.Location = New System.Drawing.Point(0, 0)
        Me.dgvLotesEntrada.MultiSelect = False
        Me.dgvLotesEntrada.Name = "dgvLotesEntrada"
        Me.dgvLotesEntrada.ReadOnly = True
        Me.dgvLotesEntrada.RowHeadersVisible = False
        Me.dgvLotesEntrada.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvLotesEntrada.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLotesEntrada.Size = New System.Drawing.Size(647, 425)
        Me.dgvLotesEntrada.TabIndex = 14
        '
        'colFacturaCodigo
        '
        Me.colFacturaCodigo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colFacturaCodigo.DataPropertyName = "codigo"
        Me.colFacturaCodigo.HeaderText = "Codigo"
        Me.colFacturaCodigo.MinimumWidth = 80
        Me.colFacturaCodigo.Name = "colFacturaCodigo"
        Me.colFacturaCodigo.ReadOnly = True
        Me.colFacturaCodigo.Width = 80
        '
        'colFacturaFecha
        '
        Me.colFacturaFecha.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colFacturaFecha.DataPropertyName = "fecha"
        DataGridViewCellStyle4.Format = "d"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.colFacturaFecha.DefaultCellStyle = DataGridViewCellStyle4
        Me.colFacturaFecha.HeaderText = "Fecha"
        Me.colFacturaFecha.Name = "colFacturaFecha"
        Me.colFacturaFecha.ReadOnly = True
        Me.colFacturaFecha.Width = 67
        '
        'colCliente
        '
        Me.colCliente.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colCliente.DataPropertyName = "nombreProveedor"
        Me.colCliente.HeaderText = "Proveedor"
        Me.colCliente.Name = "colCliente"
        Me.colCliente.ReadOnly = True
        Me.colCliente.Width = 499
        '
        'lblTitulo
        '
        Me.lblTitulo.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.Custom1
        Me.lblTitulo.Location = New System.Drawing.Point(5, 9)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(175, 34)
        Me.lblTitulo.TabIndex = 0
        Me.lblTitulo.Values.Image = Global.Escritorio.My.Resources.Resources.Salida_32
        Me.lblTitulo.Values.Text = "Lotes Salida"
        '
        'grpCabecera
        '
        Me.grpCabecera.Dock = System.Windows.Forms.DockStyle.Top
        Me.grpCabecera.GroupBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.PanelCustom1
        Me.grpCabecera.Location = New System.Drawing.Point(0, 0)
        Me.grpCabecera.Name = "grpCabecera"
        '
        'grpCabecera.Panel
        '
        Me.grpCabecera.Panel.Controls.Add(Me.lblTitulo)
        Me.grpCabecera.Size = New System.Drawing.Size(881, 57)
        Me.grpCabecera.StateCommon.Border.Color1 = System.Drawing.Color.DarkOrange
        Me.grpCabecera.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom
        Me.grpCabecera.StateCommon.Border.Width = 5
        Me.grpCabecera.TabIndex = 0
        '
        'epErrores
        '
        Me.epErrores.Alineacion = System.Windows.Forms.ErrorIconAlignment.TopLeft
        Me.epErrores.ContainerControl = Me
        '
        'bgwImprimir
        '
        Me.bgwImprimir.MostrarFormulario = True
        Me.bgwImprimir.TipoBarra = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.bgwImprimir.WorkerReportsProgress = True
        Me.bgwImprimir.WorkerSupportsCancellation = True
        '
        'colAlbEntrada
        '
        Me.colAlbEntrada.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colAlbEntrada.DataPropertyName = "codigo"
        Me.colAlbEntrada.HeaderText = "Código"
        Me.colAlbEntrada.Name = "colAlbEntrada"
        Me.colAlbEntrada.ReadOnly = True
        Me.colAlbEntrada.Width = 75
        '
        'colZonaFao
        '
        Me.colZonaFao.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colZonaFao.DataPropertyName = "fecha"
        DataGridViewCellStyle2.Format = "d"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.colZonaFao.DefaultCellStyle = DataGridViewCellStyle2
        Me.colZonaFao.HeaderText = "Fecha"
        Me.colZonaFao.Name = "colZonaFao"
        Me.colZonaFao.ReadOnly = True
        Me.colZonaFao.Width = 67
        '
        'colEspecie
        '
        Me.colEspecie.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colEspecie.DataPropertyName = "NumeroEtiquetas"
        Me.colEspecie.FillWeight = 99.68652!
        Me.colEspecie.HeaderText = "Productos Etiquetados"
        Me.colEspecie.Name = "colEspecie"
        Me.colEspecie.ReadOnly = True
        Me.colEspecie.Width = 396
        '
        'colPedo
        '
        Me.colPedo.DataPropertyName = "Peso"
        Me.colPedo.HeaderText = "Peso"
        Me.colPedo.Name = "colPedo"
        Me.colPedo.Width = 100
        '
        'frmLoteSalida
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(881, 509)
        Me.Controls.Add(Me.splSeparador)
        Me.Controls.Add(Me.grpCabecera)
        Me.Name = "frmLoteSalida"
        Me.Text = "Lotes Salida"
        CType(Me.splSeparador.Panel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.splSeparador.Panel1.ResumeLayout(False)
        Me.splSeparador.Panel1.PerformLayout()
        CType(Me.splSeparador.Panel2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.splSeparador.Panel2.ResumeLayout(False)
        CType(Me.splSeparador, System.ComponentModel.ISupportInitialize).EndInit()
        Me.splSeparador.ResumeLayout(False)
        CType(Me.hdrBusqueda.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrBusqueda.Panel.ResumeLayout(False)
        CType(Me.hdrBusqueda, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrBusqueda.ResumeLayout(False)
        Me.tblOrganizadorBusqueda.ResumeLayout(False)
        Me.tblOrganizadorBusqueda.PerformLayout()
        CType(Me.grpResultados.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpResultados.Panel.ResumeLayout(False)
        CType(Me.grpResultados, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpResultados.ResumeLayout(False)
        CType(Me.dgvResultadosBuscador, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tabGeneral, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabGeneral.ResumeLayout(False)
        CType(Me.tbpDatos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpDatos.ResumeLayout(False)
        Me.tbpDatos.PerformLayout()
        Me.tblOrganizadorDatos.ResumeLayout(False)
        Me.tblOrganizadorDatos.PerformLayout()
        CType(Me.hdrCabeceraEmpaquetado.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrCabeceraEmpaquetado.Panel.ResumeLayout(False)
        CType(Me.hdrCabeceraEmpaquetado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrCabeceraEmpaquetado.ResumeLayout(False)
        CType(Me.dgvEmpaquetados, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboDireccionCliente, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboExpedidor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbpObservaciones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpObservaciones.ResumeLayout(False)
        CType(Me.tbpEtiquetas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpEtiquetas.ResumeLayout(False)
        CType(Me.dgvEtiquetas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbpLotesEntrada, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpLotesEntrada.ResumeLayout(False)
        CType(Me.dgvLotesEntrada, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grpCabecera.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpCabecera.Panel.ResumeLayout(False)
        Me.grpCabecera.Panel.PerformLayout()
        CType(Me.grpCabecera, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpCabecera.ResumeLayout(False)
        CType(Me.epErrores, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents splSeparador As ComponentFactory.Krypton.Toolkit.KryptonSplitContainer
    Friend WithEvents hdrBusqueda As ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup
    Friend WithEvents btnOcultarBusqueda As ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup
    Friend WithEvents lblTitulo As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents grpCabecera As ComponentFactory.Krypton.Toolkit.KryptonGroup
    Friend WithEvents epErrores As Quadralia.Controles.Validacion.GestorErrores
    Friend WithEvents lblobservaciones As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblFormaPago_id As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblCliente_id As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents tabGeneral As ComponentFactory.Krypton.Navigator.KryptonNavigator
    Friend WithEvents tbpDatos As ComponentFactory.Krypton.Navigator.KryptonPage
    Friend WithEvents tblOrganizadorDatos As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblCodigo As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtCodigo As ComponentFactory.Krypton.Toolkit.KryptonTextBox
    Friend WithEvents lblFecha As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents dtpFecha As ComponentFactory.Krypton.Toolkit.KryptonDateTimePicker
    Friend WithEvents lblCliente As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtCliente As Escritorio.cTextboxBuscador
    Friend WithEvents htmlObservaciones As Escritorio.EditorHTML
    Friend WithEvents tbpObservaciones As ComponentFactory.Krypton.Navigator.KryptonPage
    Friend WithEvents tbpLotesEntrada As ComponentFactory.Krypton.Navigator.KryptonPage
    Friend WithEvents lblExpedidor As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents cboExpedidor As Escritorio.Quadralia.Controles.aComboBox
    Friend WithEvents dgvLotesEntrada As ComponentFactory.Krypton.Toolkit.KryptonDataGridView
    Friend WithEvents colFacturaCodigo As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents colFacturaFecha As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents colCliente As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents bgwImprimir As Escritorio.Quadralia.Controles.Threading.cBackGroundWorker
    Friend WithEvents cboDireccionCliente As Escritorio.Quadralia.Controles.aComboBox
    Friend WithEvents lblDireccionCliente As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents chkReactivarRegistro As ComponentFactory.Krypton.Toolkit.KryptonCheckBox
    Friend WithEvents tbpEtiquetas As ComponentFactory.Krypton.Navigator.KryptonPage
    Friend WithEvents dgvEtiquetas As ComponentFactory.Krypton.Toolkit.KryptonDataGridView
    Friend WithEvents tblOrganizadorBusqueda As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents btnLimpiarBusqueda As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents lblBcodigo As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents dtpBFechaInicio As Escritorio.Quadralia.Controles.aDateTimePicker
    Friend WithEvents grpResultados As ComponentFactory.Krypton.Toolkit.KryptonGroupBox
    Friend WithEvents dgvResultadosBuscador As ComponentFactory.Krypton.Toolkit.KryptonDataGridView
    Friend WithEvents colResultadoCodigo As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents colResultadoFecha As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents Proveedor As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents btnBBuscar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents lblBFechaInicio As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtBcodigo As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents lblBFechaFin As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents dtpBFechaFin As Escritorio.Quadralia.Controles.aDateTimePicker
    Friend WithEvents chkBRegistrosEliminados As ComponentFactory.Krypton.Toolkit.KryptonCheckBox
    Friend WithEvents lblBEtiqueta As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblBLoteSalida As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtBEtiqueta As Escritorio.cBuscadorEtiqueta
    Friend WithEvents lblBCliente As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtBCliente As Escritorio.cBuscadorCliente
    Friend WithEvents hdrCabeceraEmpaquetado As ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup
    Friend WithEvents btnEtiquetasLector As ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup
    Friend WithEvents btnEtiquetasNuevo As ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup
    Friend WithEvents btnEtiquetasEliminar As ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup
    Friend WithEvents dgvEmpaquetados As ComponentFactory.Krypton.Toolkit.KryptonDataGridView
    Friend WithEvents KryptonDataGridViewTextBoxColumn1 As KryptonDataGridViewTextBoxColumn
    Friend WithEvents Numero_Lote As DataGridViewTextBoxColumn
    Friend WithEvents KryptonDataGridViewTextBoxColumn2 As KryptonDataGridViewTextBoxColumn
    Friend WithEvents colEtiquetasEmpaquetado As KryptonDataGridViewTextBoxColumn
    Friend WithEvents KryptonDataGridViewTextBoxColumn3 As KryptonDataGridViewTextBoxColumn
    Friend WithEvents txtBLoteSalida As Quadralia.Controles.aTextBox
    Friend WithEvents colAlbEntrada As KryptonDataGridViewTextBoxColumn
    Friend WithEvents colZonaFao As KryptonDataGridViewTextBoxColumn
    Friend WithEvents colEspecie As KryptonDataGridViewTextBoxColumn
    Friend WithEvents colPedo As KryptonDataGridViewTextBoxColumn
End Class
