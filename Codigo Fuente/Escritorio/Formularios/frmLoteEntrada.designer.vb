<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoteEntrada
    Inherits FormularioHijo

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.lblBcodigo = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtBcodigo = New Escritorio.Quadralia.Controles.aTextBox()
        Me.lblBFechaInicio = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.dtpBFechaInicio = New Escritorio.Quadralia.Controles.aDateTimePicker()
        Me.lblobservaciones = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblFormaPago_id = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblCliente_id = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblBProveedor = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.btnBBuscar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.grpResultados = New ComponentFactory.Krypton.Toolkit.KryptonGroupBox()
        Me.dgvResultadosBuscador = New ComponentFactory.Krypton.Toolkit.KryptonDataGridView()
        Me.colResultadoCodigo = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colResultadoFecha = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.Proveedor = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.splSeparador = New ComponentFactory.Krypton.Toolkit.KryptonSplitContainer()
        Me.hdrBusqueda = New ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup()
        Me.btnOcultarBusqueda = New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup()
        Me.tblOrganizadorBusqueda = New System.Windows.Forms.TableLayoutPanel()
        Me.cboBSubZona = New Escritorio.Quadralia.Controles.aComboBox()
        Me.txtBNumeroLote = New Escritorio.Quadralia.Controles.aTextBox()
        Me.btnLimpiarBusqueda = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.lblBFechaFin = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.dtpBFechaFin = New Escritorio.Quadralia.Controles.aDateTimePicker()
        Me.txtBProveedor = New Escritorio.cTextboxBuscador()
        Me.chkBRegistrosEliminados = New ComponentFactory.Krypton.Toolkit.KryptonCheckBox()
        Me.lblBEspecie = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblBZonaFAO = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtBEspecie = New Escritorio.cTextboxBuscador()
        Me.cboBZonaFAO = New Escritorio.Quadralia.Controles.aComboBox()
        Me.lblBNumeroLote = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblBSubZona = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.tabGeneral = New ComponentFactory.Krypton.Navigator.KryptonNavigator()
        Me.tbpDatos = New ComponentFactory.Krypton.Navigator.KryptonPage()
        Me.tblOrganizadorDatos = New System.Windows.Forms.TableLayoutPanel()
        Me.KryptonLabel1 = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtTotal = New Escritorio.Quadralia.Controles.aTextBox()
        Me.chkReactivarRegistro = New ComponentFactory.Krypton.Toolkit.KryptonCheckBox()
        Me.lblCodigo = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtCodigo = New ComponentFactory.Krypton.Toolkit.KryptonTextBox()
        Me.dgvLineas = New ComponentFactory.Krypton.Toolkit.KryptonDataGridView()
        Me.colRefEspecie = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colRefEntrada = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colCantidad = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewNumericUpDownColumn()
        Me.colEspecie = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colZonaFao = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colEntradaSubzona = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colPresentacion = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colProduccion = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colEntradaBarco = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colNumeroLote = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colFechaDesembarco = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewDateTimePickerColumn()
        Me.lblFecha = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblCliente = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.dtpFecha = New ComponentFactory.Krypton.Toolkit.KryptonDateTimePicker()
        Me.txtProveedor = New Escritorio.cTextboxBuscador()
        Me.hdrInfo = New ComponentFactory.Krypton.Toolkit.KryptonHeader()
        Me.tbpObservaciones = New ComponentFactory.Krypton.Navigator.KryptonPage()
        Me.htmlObservaciones = New Escritorio.EditorHTML()
        Me.tbpFacturas = New ComponentFactory.Krypton.Navigator.KryptonPage()
        Me.dgvLotesSalida = New ComponentFactory.Krypton.Toolkit.KryptonDataGridView()
        Me.colFacturaCodigo = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colFacturaFecha = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colCliente = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.lblTitulo = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.grpCabecera = New ComponentFactory.Krypton.Toolkit.KryptonGroup()
        Me.epErrores = New Escritorio.Quadralia.Controles.Validacion.GestorErrores()
        CType(Me.grpResultados, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpResultados.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpResultados.Panel.SuspendLayout()
        Me.grpResultados.SuspendLayout()
        CType(Me.dgvResultadosBuscador, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.splSeparador, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.splSeparador.Panel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.splSeparador.Panel1.SuspendLayout()
        CType(Me.splSeparador.Panel2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.splSeparador.Panel2.SuspendLayout()
        Me.splSeparador.SuspendLayout()
        CType(Me.hdrBusqueda, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.hdrBusqueda.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.hdrBusqueda.Panel.SuspendLayout()
        Me.hdrBusqueda.SuspendLayout()
        Me.tblOrganizadorBusqueda.SuspendLayout()
        CType(Me.cboBSubZona, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboBZonaFAO, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tabGeneral, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabGeneral.SuspendLayout()
        CType(Me.tbpDatos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpDatos.SuspendLayout()
        Me.tblOrganizadorDatos.SuspendLayout()
        CType(Me.dgvLineas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbpObservaciones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpObservaciones.SuspendLayout()
        CType(Me.tbpFacturas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpFacturas.SuspendLayout()
        CType(Me.dgvLotesSalida, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpCabecera, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpCabecera.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpCabecera.Panel.SuspendLayout()
        Me.grpCabecera.SuspendLayout()
        CType(Me.epErrores, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblBcodigo
        '
        Me.lblBcodigo.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblBcodigo.Location = New System.Drawing.Point(3, 32)
        Me.lblBcodigo.Name = "lblBcodigo"
        Me.lblBcodigo.Size = New System.Drawing.Size(71, 20)
        Me.lblBcodigo.TabIndex = 0
        Me.lblBcodigo.Values.Text = "Nº Albarán"
        '
        'txtBcodigo
        '
        Me.txtBcodigo.AlwaysActive = False
        Me.txtBcodigo.controlarBotonBorrar = True
        Me.txtBcodigo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtBcodigo.Formato = ""
        Me.txtBcodigo.Location = New System.Drawing.Point(84, 31)
        Me.txtBcodigo.MaxLength = 48
        Me.txtBcodigo.mostrarSiempreBotonBorrar = False
        Me.txtBcodigo.Name = "txtBcodigo"
        Me.txtBcodigo.seleccionarTodo = True
        Me.txtBcodigo.Size = New System.Drawing.Size(138, 23)
        Me.txtBcodigo.TabIndex = 1
        '
        'lblBFechaInicio
        '
        Me.lblBFechaInicio.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblBFechaInicio.Location = New System.Drawing.Point(3, 60)
        Me.lblBFechaInicio.Name = "lblBFechaInicio"
        Me.lblBFechaInicio.Size = New System.Drawing.Size(75, 20)
        Me.lblBFechaInicio.TabIndex = 2
        Me.lblBFechaInicio.Values.Text = "Fecha Inicio"
        '
        'dtpBFechaInicio
        '
        Me.dtpBFechaInicio.CalendarTodayText = "Hoy:"
        Me.dtpBFechaInicio.Checked = False
        Me.dtpBFechaInicio.controlarBotonBorrar = True
        Me.dtpBFechaInicio.CustomNullText = "(Sin fecha)"
        Me.dtpBFechaInicio.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dtpBFechaInicio.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpBFechaInicio.Location = New System.Drawing.Point(84, 60)
        Me.dtpBFechaInicio.mostrarSiempreBotonBorrar = False
        Me.dtpBFechaInicio.Name = "dtpBFechaInicio"
        Me.dtpBFechaInicio.Size = New System.Drawing.Size(138, 21)
        Me.dtpBFechaInicio.TabIndex = 3
        Me.dtpBFechaInicio.TipoLimpieza = Escritorio.Quadralia.Controles.aDateTimePicker.TiposLimpieza.ValueYValueNullable
        '
        'lblobservaciones
        '
        Me.lblobservaciones.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblobservaciones.Location = New System.Drawing.Point(34, 464)
        Me.lblobservaciones.Name = "lblobservaciones"
        Me.lblobservaciones.Size = New System.Drawing.Size(88, 20)
        Me.lblobservaciones.TabIndex = 0
        Me.lblobservaciones.Values.Text = "observaciones"
        '
        'lblFormaPago_id
        '
        Me.lblFormaPago_id.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblFormaPago_id.Location = New System.Drawing.Point(34, 664)
        Me.lblFormaPago_id.Name = "lblFormaPago_id"
        Me.lblFormaPago_id.Size = New System.Drawing.Size(88, 20)
        Me.lblFormaPago_id.TabIndex = 0
        Me.lblFormaPago_id.Values.Text = "FormaPago_id"
        '
        'lblCliente_id
        '
        Me.lblCliente_id.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblCliente_id.Location = New System.Drawing.Point(34, 684)
        Me.lblCliente_id.Name = "lblCliente_id"
        Me.lblCliente_id.Size = New System.Drawing.Size(64, 20)
        Me.lblCliente_id.TabIndex = 0
        Me.lblCliente_id.Values.Text = "Cliente_id"
        '
        'lblBProveedor
        '
        Me.lblBProveedor.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblBProveedor.Location = New System.Drawing.Point(3, 114)
        Me.lblBProveedor.Name = "lblBProveedor"
        Me.lblBProveedor.Size = New System.Drawing.Size(67, 20)
        Me.lblBProveedor.TabIndex = 6
        Me.lblBProveedor.Values.Text = "Proveedor"
        '
        'btnBBuscar
        '
        Me.tblOrganizadorBusqueda.SetColumnSpan(Me.btnBBuscar, 2)
        Me.btnBBuscar.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnBBuscar.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnBBuscar.Location = New System.Drawing.Point(3, 275)
        Me.btnBBuscar.Name = "btnBBuscar"
        Me.btnBBuscar.Size = New System.Drawing.Size(219, 25)
        Me.btnBBuscar.TabIndex = 9
        Me.btnBBuscar.Values.Text = "Buscar"
        '
        'grpResultados
        '
        Me.tblOrganizadorBusqueda.SetColumnSpan(Me.grpResultados, 2)
        Me.grpResultados.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grpResultados.Location = New System.Drawing.Point(3, 306)
        Me.grpResultados.Name = "grpResultados"
        '
        'grpResultados.Panel
        '
        Me.grpResultados.Panel.Controls.Add(Me.dgvResultadosBuscador)
        Me.grpResultados.Size = New System.Drawing.Size(219, 106)
        Me.grpResultados.TabIndex = 10
        Me.grpResultados.Text = "Resultados"
        Me.grpResultados.Values.Heading = "Resultados"
        '
        'dgvResultadosBuscador
        '
        Me.dgvResultadosBuscador.AllowUserToAddRows = False
        Me.dgvResultadosBuscador.AllowUserToDeleteRows = False
        Me.dgvResultadosBuscador.AllowUserToResizeRows = False
        Me.dgvResultadosBuscador.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colResultadoCodigo, Me.colResultadoFecha, Me.Proveedor})
        Me.dgvResultadosBuscador.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvResultadosBuscador.Location = New System.Drawing.Point(0, 0)
        Me.dgvResultadosBuscador.Name = "dgvResultadosBuscador"
        Me.dgvResultadosBuscador.ReadOnly = True
        Me.dgvResultadosBuscador.RowHeadersVisible = False
        Me.dgvResultadosBuscador.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvResultadosBuscador.Size = New System.Drawing.Size(215, 82)
        Me.dgvResultadosBuscador.TabIndex = 0
        '
        'colResultadoCodigo
        '
        Me.colResultadoCodigo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colResultadoCodigo.DataPropertyName = "codigo"
        Me.colResultadoCodigo.HeaderText = "Cód."
        Me.colResultadoCodigo.Name = "colResultadoCodigo"
        Me.colResultadoCodigo.ReadOnly = True
        Me.colResultadoCodigo.Width = 61
        '
        'colResultadoFecha
        '
        Me.colResultadoFecha.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colResultadoFecha.DataPropertyName = "fecha"
        DataGridViewCellStyle1.Format = "d"
        DataGridViewCellStyle1.NullValue = Nothing
        Me.colResultadoFecha.DefaultCellStyle = DataGridViewCellStyle1
        Me.colResultadoFecha.HeaderText = "Fecha"
        Me.colResultadoFecha.Name = "colResultadoFecha"
        Me.colResultadoFecha.ReadOnly = True
        Me.colResultadoFecha.Width = 67
        '
        'Proveedor
        '
        Me.Proveedor.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Proveedor.DataPropertyName = "nombreProveedor"
        Me.Proveedor.HeaderText = "Proveedor"
        Me.Proveedor.MinimumWidth = 80
        Me.Proveedor.Name = "Proveedor"
        Me.Proveedor.ReadOnly = True
        Me.Proveedor.Width = 86
        '
        'splSeparador
        '
        Me.splSeparador.Cursor = System.Windows.Forms.Cursors.Default
        Me.splSeparador.Dock = System.Windows.Forms.DockStyle.Fill
        Me.splSeparador.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.splSeparador.Location = New System.Drawing.Point(0, 57)
        Me.splSeparador.Name = "splSeparador"
        '
        'splSeparador.Panel1
        '
        Me.splSeparador.Panel1.Controls.Add(Me.hdrBusqueda)
        Me.splSeparador.Panel1MinSize = 220
        '
        'splSeparador.Panel2
        '
        Me.splSeparador.Panel2.Controls.Add(Me.tabGeneral)
        Me.splSeparador.Size = New System.Drawing.Size(902, 452)
        Me.splSeparador.SplitterDistance = 227
        Me.splSeparador.TabIndex = 1
        '
        'hdrBusqueda
        '
        Me.hdrBusqueda.AutoSize = True
        Me.hdrBusqueda.ButtonSpecs.AddRange(New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup() {Me.btnOcultarBusqueda})
        Me.hdrBusqueda.Dock = System.Windows.Forms.DockStyle.Fill
        Me.hdrBusqueda.HeaderVisibleSecondary = False
        Me.hdrBusqueda.Location = New System.Drawing.Point(0, 0)
        Me.hdrBusqueda.Name = "hdrBusqueda"
        '
        'hdrBusqueda.Panel
        '
        Me.hdrBusqueda.Panel.Controls.Add(Me.tblOrganizadorBusqueda)
        Me.hdrBusqueda.Size = New System.Drawing.Size(227, 452)
        Me.hdrBusqueda.TabIndex = 0
        Me.hdrBusqueda.ValuesPrimary.Heading = "Buscar"
        Me.hdrBusqueda.ValuesPrimary.Image = Global.Escritorio.My.Resources.Resources.Buscar_32
        '
        'btnOcultarBusqueda
        '
        Me.btnOcultarBusqueda.Type = ComponentFactory.Krypton.Toolkit.PaletteButtonSpecStyle.ArrowLeft
        Me.btnOcultarBusqueda.UniqueName = "BE70F7722CF94C60618F65819BBF010D"
        '
        'tblOrganizadorBusqueda
        '
        Me.tblOrganizadorBusqueda.BackColor = System.Drawing.Color.White
        Me.tblOrganizadorBusqueda.ColumnCount = 2
        Me.tblOrganizadorBusqueda.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblOrganizadorBusqueda.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblOrganizadorBusqueda.Controls.Add(Me.cboBSubZona, 1, 7)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.txtBNumeroLote, 1, 8)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.btnLimpiarBusqueda, 0, 0)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.lblBcodigo, 0, 1)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.dtpBFechaInicio, 1, 2)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.grpResultados, 0, 11)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.btnBBuscar, 0, 10)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.lblBProveedor, 0, 4)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.lblBFechaInicio, 0, 2)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.txtBcodigo, 1, 1)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.lblBFechaFin, 0, 3)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.dtpBFechaFin, 1, 3)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.txtBProveedor, 1, 4)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.chkBRegistrosEliminados, 0, 9)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.lblBEspecie, 0, 5)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.lblBZonaFAO, 0, 6)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.txtBEspecie, 1, 5)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.cboBZonaFAO, 1, 6)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.lblBNumeroLote, 0, 8)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.lblBSubZona, 0, 7)
        Me.tblOrganizadorBusqueda.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblOrganizadorBusqueda.Location = New System.Drawing.Point(0, 0)
        Me.tblOrganizadorBusqueda.Name = "tblOrganizadorBusqueda"
        Me.tblOrganizadorBusqueda.RowCount = 12
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorBusqueda.Size = New System.Drawing.Size(225, 415)
        Me.tblOrganizadorBusqueda.TabIndex = 0
        '
        'cboBSubZona
        '
        Me.cboBSubZona.controlarBotonBorrar = True
        Me.cboBSubZona.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cboBSubZona.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBSubZona.DropDownWidth = 131
        Me.cboBSubZona.Location = New System.Drawing.Point(84, 193)
        Me.cboBSubZona.mostrarSiempreBotonBorrar = False
        Me.cboBSubZona.Name = "cboBSubZona"
        Me.cboBSubZona.Size = New System.Drawing.Size(138, 21)
        Me.cboBSubZona.TabIndex = 19
        '
        'txtBNumeroLote
        '
        Me.txtBNumeroLote.AlwaysActive = False
        Me.txtBNumeroLote.controlarBotonBorrar = True
        Me.txtBNumeroLote.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtBNumeroLote.Formato = ""
        Me.txtBNumeroLote.Location = New System.Drawing.Point(84, 220)
        Me.txtBNumeroLote.MaxLength = 48
        Me.txtBNumeroLote.mostrarSiempreBotonBorrar = False
        Me.txtBNumeroLote.Name = "txtBNumeroLote"
        Me.txtBNumeroLote.seleccionarTodo = True
        Me.txtBNumeroLote.Size = New System.Drawing.Size(138, 23)
        Me.txtBNumeroLote.TabIndex = 18
        '
        'btnLimpiarBusqueda
        '
        Me.btnLimpiarBusqueda.AutoSize = True
        Me.btnLimpiarBusqueda.ButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.ButtonSpec
        Me.tblOrganizadorBusqueda.SetColumnSpan(Me.btnLimpiarBusqueda, 2)
        Me.btnLimpiarBusqueda.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnLimpiarBusqueda.Location = New System.Drawing.Point(97, 3)
        Me.btnLimpiarBusqueda.Name = "btnLimpiarBusqueda"
        Me.btnLimpiarBusqueda.Size = New System.Drawing.Size(125, 22)
        Me.btnLimpiarBusqueda.TabIndex = 13
        Me.btnLimpiarBusqueda.Values.Image = Global.Escritorio.My.Resources.Resources.Limpiar_16
        Me.btnLimpiarBusqueda.Values.Text = "Limpiar Búsqueda"
        '
        'lblBFechaFin
        '
        Me.lblBFechaFin.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblBFechaFin.Location = New System.Drawing.Point(3, 87)
        Me.lblBFechaFin.Name = "lblBFechaFin"
        Me.lblBFechaFin.Size = New System.Drawing.Size(61, 20)
        Me.lblBFechaFin.TabIndex = 4
        Me.lblBFechaFin.Values.Text = "Fecha Fin"
        '
        'dtpBFechaFin
        '
        Me.dtpBFechaFin.CalendarTodayText = "Hoy:"
        Me.dtpBFechaFin.Checked = False
        Me.dtpBFechaFin.controlarBotonBorrar = True
        Me.dtpBFechaFin.CustomNullText = "(Sin fecha)"
        Me.dtpBFechaFin.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dtpBFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpBFechaFin.Location = New System.Drawing.Point(84, 87)
        Me.dtpBFechaFin.mostrarSiempreBotonBorrar = False
        Me.dtpBFechaFin.Name = "dtpBFechaFin"
        Me.dtpBFechaFin.Size = New System.Drawing.Size(138, 21)
        Me.dtpBFechaFin.TabIndex = 5
        Me.dtpBFechaFin.TipoLimpieza = Escritorio.Quadralia.Controles.aDateTimePicker.TiposLimpieza.ValueYValueNullable
        '
        'txtBProveedor
        '
        Me.txtBProveedor.DescripcionReadOnly = False
        Me.txtBProveedor.DescripcionVisible = False
        Me.txtBProveedor.DisplayMember = "nombreComercial"
        Me.txtBProveedor.DisplayText = ""
        Me.txtBProveedor.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtBProveedor.FormularioBusqueda = "Escritorio.frmSeleccionarProveedor"
        Me.txtBProveedor.Item = Nothing
        Me.txtBProveedor.Location = New System.Drawing.Point(84, 114)
        Me.txtBProveedor.Name = "txtBProveedor"
        Me.txtBProveedor.Size = New System.Drawing.Size(138, 20)
        Me.txtBProveedor.TabIndex = 7
        Me.txtBProveedor.UseOnlyNumbers = False
        Me.txtBProveedor.UseUpperCase = True
        Me.txtBProveedor.Validar = True
        Me.txtBProveedor.ValueMember = "codigo"
        Me.txtBProveedor.ValueText = ""
        '
        'chkBRegistrosEliminados
        '
        Me.tblOrganizadorBusqueda.SetColumnSpan(Me.chkBRegistrosEliminados, 2)
        Me.chkBRegistrosEliminados.Dock = System.Windows.Forms.DockStyle.Fill
        Me.chkBRegistrosEliminados.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl
        Me.chkBRegistrosEliminados.Location = New System.Drawing.Point(3, 249)
        Me.chkBRegistrosEliminados.Name = "chkBRegistrosEliminados"
        Me.chkBRegistrosEliminados.Size = New System.Drawing.Size(219, 20)
        Me.chkBRegistrosEliminados.TabIndex = 8
        Me.chkBRegistrosEliminados.Text = "Incluir lotes eliminados"
        Me.chkBRegistrosEliminados.Values.Text = "Incluir lotes eliminados"
        '
        'lblBEspecie
        '
        Me.lblBEspecie.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblBEspecie.Location = New System.Drawing.Point(3, 140)
        Me.lblBEspecie.Name = "lblBEspecie"
        Me.lblBEspecie.Size = New System.Drawing.Size(51, 20)
        Me.lblBEspecie.TabIndex = 6
        Me.lblBEspecie.Values.Text = "Especie"
        '
        'lblBZonaFAO
        '
        Me.lblBZonaFAO.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblBZonaFAO.Location = New System.Drawing.Point(3, 166)
        Me.lblBZonaFAO.Name = "lblBZonaFAO"
        Me.lblBZonaFAO.Size = New System.Drawing.Size(65, 20)
        Me.lblBZonaFAO.TabIndex = 6
        Me.lblBZonaFAO.Values.Text = "Zona FAO"
        '
        'txtBEspecie
        '
        Me.txtBEspecie.DescripcionReadOnly = False
        Me.txtBEspecie.DescripcionVisible = False
        Me.txtBEspecie.DisplayMember = "denominacionComercial"
        Me.txtBEspecie.DisplayText = ""
        Me.txtBEspecie.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtBEspecie.FormularioBusqueda = "Escritorio.frmSeleccionarEspecies"
        Me.txtBEspecie.Item = Nothing
        Me.txtBEspecie.Location = New System.Drawing.Point(84, 140)
        Me.txtBEspecie.Name = "txtBEspecie"
        Me.txtBEspecie.Size = New System.Drawing.Size(138, 20)
        Me.txtBEspecie.TabIndex = 11
        Me.txtBEspecie.UseOnlyNumbers = False
        Me.txtBEspecie.UseUpperCase = True
        Me.txtBEspecie.Validar = True
        Me.txtBEspecie.ValueMember = "alfa3"
        Me.txtBEspecie.ValueText = ""
        '
        'cboBZonaFAO
        '
        Me.cboBZonaFAO.controlarBotonBorrar = True
        Me.cboBZonaFAO.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cboBZonaFAO.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBZonaFAO.DropDownWidth = 131
        Me.cboBZonaFAO.Location = New System.Drawing.Point(84, 166)
        Me.cboBZonaFAO.mostrarSiempreBotonBorrar = False
        Me.cboBZonaFAO.Name = "cboBZonaFAO"
        Me.cboBZonaFAO.Size = New System.Drawing.Size(138, 21)
        Me.cboBZonaFAO.TabIndex = 12
        '
        'lblBNumeroLote
        '
        Me.lblBNumeroLote.Location = New System.Drawing.Point(3, 220)
        Me.lblBNumeroLote.Name = "lblBNumeroLote"
        Me.lblBNumeroLote.Size = New System.Drawing.Size(67, 20)
        Me.lblBNumeroLote.TabIndex = 16
        Me.lblBNumeroLote.Values.Text = "Num. Lote"
        '
        'lblBSubZona
        '
        Me.lblBSubZona.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblBSubZona.Location = New System.Drawing.Point(3, 193)
        Me.lblBSubZona.Name = "lblBSubZona"
        Me.lblBSubZona.Size = New System.Drawing.Size(62, 20)
        Me.lblBSubZona.TabIndex = 6
        Me.lblBSubZona.Values.Text = "Sub Zona"
        '
        'tabGeneral
        '
        Me.tabGeneral.AllowPageReorder = False
        Me.tabGeneral.Button.CloseButtonAction = ComponentFactory.Krypton.Navigator.CloseButtonAction.None
        Me.tabGeneral.Button.CloseButtonDisplay = ComponentFactory.Krypton.Navigator.ButtonDisplay.Hide
        Me.tabGeneral.Button.ContextButtonAction = ComponentFactory.Krypton.Navigator.ContextButtonAction.None
        Me.tabGeneral.Button.ContextButtonDisplay = ComponentFactory.Krypton.Navigator.ButtonDisplay.Hide
        Me.tabGeneral.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabGeneral.Location = New System.Drawing.Point(0, 0)
        Me.tabGeneral.Name = "tabGeneral"
        Me.tabGeneral.Pages.AddRange(New ComponentFactory.Krypton.Navigator.KryptonPage() {Me.tbpDatos, Me.tbpObservaciones, Me.tbpFacturas})
        Me.tabGeneral.SelectedIndex = 0
        Me.tabGeneral.Size = New System.Drawing.Size(670, 452)
        Me.tabGeneral.TabIndex = 0
        Me.tabGeneral.Text = "KryptonNavigator1"
        '
        'tbpDatos
        '
        Me.tbpDatos.AutoHiddenSlideSize = New System.Drawing.Size(200, 200)
        Me.tbpDatos.Controls.Add(Me.tblOrganizadorDatos)
        Me.tbpDatos.Controls.Add(Me.lblCliente_id)
        Me.tbpDatos.Controls.Add(Me.lblFormaPago_id)
        Me.tbpDatos.Controls.Add(Me.lblobservaciones)
        Me.tbpDatos.Controls.Add(Me.hdrInfo)
        Me.tbpDatos.Flags = 65534
        Me.tbpDatos.LastVisibleSet = True
        Me.tbpDatos.MinimumSize = New System.Drawing.Size(50, 50)
        Me.tbpDatos.Name = "tbpDatos"
        Me.tbpDatos.Size = New System.Drawing.Size(668, 425)
        Me.tbpDatos.Text = "Datos"
        Me.tbpDatos.ToolTipTitle = "Page ToolTip"
        Me.tbpDatos.UniqueName = "A7344070A3E64BA24893A75E8104A8EE"
        '
        'tblOrganizadorDatos
        '
        Me.tblOrganizadorDatos.BackColor = System.Drawing.Color.FromArgb(CType(CType(187, Byte), Integer), CType(CType(206, Byte), Integer), CType(CType(230, Byte), Integer))
        Me.tblOrganizadorDatos.ColumnCount = 6
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblOrganizadorDatos.Controls.Add(Me.KryptonLabel1, 1, 4)
        Me.tblOrganizadorDatos.Controls.Add(Me.txtTotal, 0, 4)
        Me.tblOrganizadorDatos.Controls.Add(Me.chkReactivarRegistro, 0, 2)
        Me.tblOrganizadorDatos.Controls.Add(Me.lblCodigo, 0, 0)
        Me.tblOrganizadorDatos.Controls.Add(Me.txtCodigo, 2, 0)
        Me.tblOrganizadorDatos.Controls.Add(Me.dgvLineas, 0, 3)
        Me.tblOrganizadorDatos.Controls.Add(Me.lblFecha, 3, 0)
        Me.tblOrganizadorDatos.Controls.Add(Me.lblCliente, 0, 1)
        Me.tblOrganizadorDatos.Controls.Add(Me.dtpFecha, 5, 0)
        Me.tblOrganizadorDatos.Controls.Add(Me.txtProveedor, 2, 1)
        Me.tblOrganizadorDatos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblOrganizadorDatos.Location = New System.Drawing.Point(0, 0)
        Me.tblOrganizadorDatos.Name = "tblOrganizadorDatos"
        Me.tblOrganizadorDatos.RowCount = 5
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.Size = New System.Drawing.Size(668, 403)
        Me.tblOrganizadorDatos.TabIndex = 0
        '
        'KryptonLabel1
        '
        Me.tblOrganizadorDatos.SetColumnSpan(Me.KryptonLabel1, 2)
        Me.KryptonLabel1.Location = New System.Drawing.Point(80, 377)
        Me.KryptonLabel1.Name = "KryptonLabel1"
        Me.KryptonLabel1.Size = New System.Drawing.Size(38, 20)
        Me.KryptonLabel1.TabIndex = 17
        Me.KryptonLabel1.Values.Text = "Total"
        '
        'txtTotal
        '
        Me.txtTotal.AlwaysActive = False
        Me.txtTotal.controlarBotonBorrar = True
        Me.txtTotal.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtTotal.Formato = ""
        Me.txtTotal.Location = New System.Drawing.Point(3, 377)
        Me.txtTotal.MaxLength = 48
        Me.txtTotal.mostrarSiempreBotonBorrar = False
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.ReadOnly = True
        Me.txtTotal.seleccionarTodo = True
        Me.txtTotal.Size = New System.Drawing.Size(71, 23)
        Me.txtTotal.TabIndex = 19
        '
        'chkReactivarRegistro
        '
        Me.tblOrganizadorDatos.SetColumnSpan(Me.chkReactivarRegistro, 6)
        Me.chkReactivarRegistro.Dock = System.Windows.Forms.DockStyle.Fill
        Me.chkReactivarRegistro.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl
        Me.chkReactivarRegistro.Location = New System.Drawing.Point(3, 58)
        Me.chkReactivarRegistro.Name = "chkReactivarRegistro"
        Me.chkReactivarRegistro.Size = New System.Drawing.Size(662, 20)
        Me.chkReactivarRegistro.TabIndex = 26
        Me.chkReactivarRegistro.Text = "Reactivar el registro"
        Me.chkReactivarRegistro.Values.Text = "Reactivar el registro"
        Me.chkReactivarRegistro.Visible = False
        '
        'lblCodigo
        '
        Me.lblCodigo.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblCodigo.Location = New System.Drawing.Point(3, 3)
        Me.lblCodigo.Name = "lblCodigo"
        Me.lblCodigo.Size = New System.Drawing.Size(71, 23)
        Me.lblCodigo.TabIndex = 0
        Me.lblCodigo.Values.Text = "Nº Albarán"
        '
        'txtCodigo
        '
        Me.txtCodigo.AlwaysActive = False
        Me.txtCodigo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtCodigo.Enabled = False
        Me.txtCodigo.Location = New System.Drawing.Point(100, 3)
        Me.txtCodigo.MaxLength = 48
        Me.txtCodigo.MinimumSize = New System.Drawing.Size(80, 20)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(245, 23)
        Me.txtCodigo.TabIndex = 1
        Me.txtCodigo.Text = "codigo"
        '
        'dgvLineas
        '
        Me.dgvLineas.AllowUserToOrderColumns = True
        Me.dgvLineas.AllowUserToResizeRows = False
        Me.dgvLineas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colRefEspecie, Me.colRefEntrada, Me.colCantidad, Me.colEspecie, Me.colZonaFao, Me.colEntradaSubzona, Me.colPresentacion, Me.colProduccion, Me.colEntradaBarco, Me.colNumeroLote, Me.colFechaDesembarco})
        Me.tblOrganizadorDatos.SetColumnSpan(Me.dgvLineas, 6)
        Me.dgvLineas.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvLineas.Location = New System.Drawing.Point(3, 84)
        Me.dgvLineas.MultiSelect = False
        Me.dgvLineas.Name = "dgvLineas"
        Me.dgvLineas.RowHeadersVisible = False
        Me.dgvLineas.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvLineas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvLineas.ShowCellToolTips = False
        Me.dgvLineas.Size = New System.Drawing.Size(662, 287)
        Me.dgvLineas.TabIndex = 25
        '
        'colRefEspecie
        '
        Me.colRefEspecie.DataPropertyName = "Especie"
        Me.colRefEspecie.HeaderText = "Especie"
        Me.colRefEspecie.Name = "colRefEspecie"
        Me.colRefEspecie.ReadOnly = True
        Me.colRefEspecie.Visible = False
        Me.colRefEspecie.Width = 100
        '
        'colRefEntrada
        '
        Me.colRefEntrada.DataPropertyName = "LoteEntrada"
        Me.colRefEntrada.HeaderText = "Lote Entrada"
        Me.colRefEntrada.Name = "colRefEntrada"
        Me.colRefEntrada.Visible = False
        Me.colRefEntrada.Width = 100
        '
        'colCantidad
        '
        Me.colCantidad.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colCantidad.DataPropertyName = "cantidad"
        Me.colCantidad.DecimalPlaces = 2
        Me.colCantidad.HeaderText = "Uds."
        Me.colCantidad.Increment = New Decimal(New Integer() {1, 0, 0, 0})
        Me.colCantidad.Maximum = New Decimal(New Integer() {1879048191, -1291644761, -500259693, 0})
        Me.colCantidad.Minimum = New Decimal(New Integer() {1879048191, -1291644761, -500259693, -2147483648})
        Me.colCantidad.Name = "colCantidad"
        Me.colCantidad.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colCantidad.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.colCantidad.Width = 59
        '
        'colEspecie
        '
        Me.colEspecie.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colEspecie.DataPropertyName = "nombreEspecie"
        Me.colEspecie.HeaderText = "Especie"
        Me.colEspecie.MinimumWidth = 100
        Me.colEspecie.Name = "colEspecie"
        Me.colEspecie.Width = 105
        '
        'colZonaFao
        '
        Me.colZonaFao.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colZonaFao.DataPropertyName = "ZonaFao"
        Me.colZonaFao.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.[Nothing]
        Me.colZonaFao.HeaderText = "Zona FAO"
        Me.colZonaFao.Name = "colZonaFao"
        Me.colZonaFao.Width = 69
        '
        'colEntradaSubzona
        '
        Me.colEntradaSubzona.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colEntradaSubzona.DataPropertyName = "Subzona"
        Me.colEntradaSubzona.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.[Nothing]
        Me.colEntradaSubzona.HeaderText = "Subzona"
        Me.colEntradaSubzona.Name = "colEntradaSubzona"
        Me.colEntradaSubzona.Width = 62
        '
        'colPresentacion
        '
        Me.colPresentacion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colPresentacion.DataPropertyName = "Presentacion"
        Me.colPresentacion.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.[Nothing]
        Me.colPresentacion.HeaderText = "Presentacion"
        Me.colPresentacion.Name = "colPresentacion"
        Me.colPresentacion.Width = 85
        '
        'colProduccion
        '
        Me.colProduccion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colProduccion.DataPropertyName = "Produccion"
        Me.colProduccion.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.[Nothing]
        Me.colProduccion.HeaderText = "Produccion"
        Me.colProduccion.Name = "colProduccion"
        Me.colProduccion.Width = 78
        '
        'colEntradaBarco
        '
        Me.colEntradaBarco.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colEntradaBarco.DataPropertyName = "Barco"
        Me.colEntradaBarco.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.[Nothing]
        Me.colEntradaBarco.HeaderText = "Barco"
        Me.colEntradaBarco.Name = "colEntradaBarco"
        Me.colEntradaBarco.Width = 47
        '
        'colNumeroLote
        '
        Me.colNumeroLote.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colNumeroLote.DataPropertyName = "numeroLote"
        DataGridViewCellStyle2.NullValue = " "
        Me.colNumeroLote.DefaultCellStyle = DataGridViewCellStyle2
        Me.colNumeroLote.HeaderText = "Lote"
        Me.colNumeroLote.Name = "colNumeroLote"
        Me.colNumeroLote.Width = 59
        '
        'colFechaDesembarco
        '
        Me.colFechaDesembarco.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colFechaDesembarco.CalendarTodayDate = New Date(CType(0, Long))
        Me.colFechaDesembarco.Checked = False
        Me.colFechaDesembarco.DataPropertyName = "fechaDesembarco"
        Me.colFechaDesembarco.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.colFechaDesembarco.HeaderText = "Fecha Desemb."
        Me.colFechaDesembarco.Name = "colFechaDesembarco"
        Me.colFechaDesembarco.Width = 97
        '
        'lblFecha
        '
        Me.lblFecha.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblFecha.Location = New System.Drawing.Point(351, 3)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(42, 23)
        Me.lblFecha.TabIndex = 2
        Me.lblFecha.Values.Text = "Fecha"
        '
        'lblCliente
        '
        Me.lblCliente.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblCliente.Location = New System.Drawing.Point(3, 32)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(67, 20)
        Me.lblCliente.TabIndex = 4
        Me.lblCliente.Values.Text = "Proveedor"
        '
        'dtpFecha
        '
        Me.dtpFecha.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecha.Location = New System.Drawing.Point(419, 3)
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(246, 23)
        Me.dtpFecha.TabIndex = 3
        Me.dtpFecha.ValueNullable = New Date(CType(0, Long))
        '
        'txtProveedor
        '
        Me.tblOrganizadorDatos.SetColumnSpan(Me.txtProveedor, 4)
        Me.txtProveedor.DescripcionReadOnly = False
        Me.txtProveedor.DescripcionVisible = True
        Me.txtProveedor.DisplayMember = "nombreComercial"
        Me.txtProveedor.DisplayText = ""
        Me.txtProveedor.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtProveedor.FormularioBusqueda = "Escritorio.frmSeleccionarProveedor"
        Me.txtProveedor.Item = Nothing
        Me.txtProveedor.Location = New System.Drawing.Point(100, 32)
        Me.txtProveedor.Name = "txtProveedor"
        Me.txtProveedor.Size = New System.Drawing.Size(565, 20)
        Me.txtProveedor.TabIndex = 5
        Me.txtProveedor.UseOnlyNumbers = False
        Me.txtProveedor.UseUpperCase = True
        Me.txtProveedor.Validar = True
        Me.txtProveedor.ValueMember = "codigo"
        Me.txtProveedor.ValueText = ""
        '
        'hdrInfo
        '
        Me.hdrInfo.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.hdrInfo.HeaderStyle = ComponentFactory.Krypton.Toolkit.HeaderStyle.Secondary
        Me.hdrInfo.Location = New System.Drawing.Point(0, 403)
        Me.hdrInfo.Margin = New System.Windows.Forms.Padding(0)
        Me.hdrInfo.Name = "hdrInfo"
        Me.hdrInfo.Size = New System.Drawing.Size(668, 22)
        Me.hdrInfo.TabIndex = 27
        Me.hdrInfo.Values.Description = ""
        Me.hdrInfo.Values.Heading = "Supr: Eliminar Fila | F2: Editar contenido | F3: Buscar | F6: Ver Envio Asignado"
        Me.hdrInfo.Values.Image = Nothing
        '
        'tbpObservaciones
        '
        Me.tbpObservaciones.AutoHiddenSlideSize = New System.Drawing.Size(200, 200)
        Me.tbpObservaciones.Controls.Add(Me.htmlObservaciones)
        Me.tbpObservaciones.Flags = 65534
        Me.tbpObservaciones.LastVisibleSet = True
        Me.tbpObservaciones.MinimumSize = New System.Drawing.Size(50, 50)
        Me.tbpObservaciones.Name = "tbpObservaciones"
        Me.tbpObservaciones.Size = New System.Drawing.Size(668, 425)
        Me.tbpObservaciones.Text = "Observaciones"
        Me.tbpObservaciones.ToolTipTitle = "Page ToolTip"
        Me.tbpObservaciones.UniqueName = "7E9D077196D249599B9E2159EC0B7EDD"
        '
        'htmlObservaciones
        '
        Me.htmlObservaciones.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.htmlObservaciones.ConvertirImagenesABase64 = False
        Me.htmlObservaciones.Dock = System.Windows.Forms.DockStyle.Fill
        Me.htmlObservaciones.Location = New System.Drawing.Point(0, 0)
        Me.htmlObservaciones.MostrarBarrasEdicion = True
        Me.htmlObservaciones.Name = "htmlObservaciones"
        Me.htmlObservaciones.ReadOnly = False
        Me.htmlObservaciones.Size = New System.Drawing.Size(668, 425)
        Me.htmlObservaciones.TabIndex = 0
        '
        'tbpFacturas
        '
        Me.tbpFacturas.AutoHiddenSlideSize = New System.Drawing.Size(200, 200)
        Me.tbpFacturas.Controls.Add(Me.dgvLotesSalida)
        Me.tbpFacturas.Flags = 65534
        Me.tbpFacturas.LastVisibleSet = True
        Me.tbpFacturas.MinimumSize = New System.Drawing.Size(50, 50)
        Me.tbpFacturas.Name = "tbpFacturas"
        Me.tbpFacturas.Size = New System.Drawing.Size(668, 425)
        Me.tbpFacturas.Text = "Envíos salida"
        Me.tbpFacturas.ToolTipTitle = "Page ToolTip"
        Me.tbpFacturas.UniqueName = "8795A1C22875417A22AF7CEB7A67B981"
        '
        'dgvLotesSalida
        '
        Me.dgvLotesSalida.AllowUserToAddRows = False
        Me.dgvLotesSalida.AllowUserToDeleteRows = False
        Me.dgvLotesSalida.AllowUserToResizeRows = False
        Me.dgvLotesSalida.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colFacturaCodigo, Me.colFacturaFecha, Me.colCliente})
        Me.dgvLotesSalida.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvLotesSalida.Location = New System.Drawing.Point(0, 0)
        Me.dgvLotesSalida.MultiSelect = False
        Me.dgvLotesSalida.Name = "dgvLotesSalida"
        Me.dgvLotesSalida.ReadOnly = True
        Me.dgvLotesSalida.RowHeadersVisible = False
        Me.dgvLotesSalida.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvLotesSalida.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLotesSalida.Size = New System.Drawing.Size(668, 425)
        Me.dgvLotesSalida.TabIndex = 13
        '
        'colFacturaCodigo
        '
        Me.colFacturaCodigo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colFacturaCodigo.DataPropertyName = "codigo"
        Me.colFacturaCodigo.HeaderText = "Codigo"
        Me.colFacturaCodigo.MinimumWidth = 80
        Me.colFacturaCodigo.Name = "colFacturaCodigo"
        Me.colFacturaCodigo.ReadOnly = True
        Me.colFacturaCodigo.Width = 80
        '
        'colFacturaFecha
        '
        Me.colFacturaFecha.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colFacturaFecha.DataPropertyName = "fecha"
        DataGridViewCellStyle3.Format = "d"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.colFacturaFecha.DefaultCellStyle = DataGridViewCellStyle3
        Me.colFacturaFecha.HeaderText = "Fecha"
        Me.colFacturaFecha.Name = "colFacturaFecha"
        Me.colFacturaFecha.ReadOnly = True
        Me.colFacturaFecha.Width = 67
        '
        'colCliente
        '
        Me.colCliente.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colCliente.DataPropertyName = "nombreCliente"
        Me.colCliente.HeaderText = "Cliente"
        Me.colCliente.Name = "colCliente"
        Me.colCliente.ReadOnly = True
        Me.colCliente.Width = 520
        '
        'lblTitulo
        '
        Me.lblTitulo.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.Custom1
        Me.lblTitulo.Location = New System.Drawing.Point(5, 9)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(259, 34)
        Me.lblTitulo.TabIndex = 0
        Me.lblTitulo.Values.Image = Global.Escritorio.My.Resources.Resources.Entrada_32
        Me.lblTitulo.Values.Text = "Albaranes de Entrada"
        '
        'grpCabecera
        '
        Me.grpCabecera.Dock = System.Windows.Forms.DockStyle.Top
        Me.grpCabecera.GroupBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.PanelCustom1
        Me.grpCabecera.Location = New System.Drawing.Point(0, 0)
        Me.grpCabecera.Name = "grpCabecera"
        '
        'grpCabecera.Panel
        '
        Me.grpCabecera.Panel.Controls.Add(Me.lblTitulo)
        Me.grpCabecera.Size = New System.Drawing.Size(902, 57)
        Me.grpCabecera.StateCommon.Border.Color1 = System.Drawing.Color.DarkOrange
        Me.grpCabecera.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom
        Me.grpCabecera.StateCommon.Border.Width = 5
        Me.grpCabecera.TabIndex = 0
        '
        'epErrores
        '
        Me.epErrores.Alineacion = System.Windows.Forms.ErrorIconAlignment.TopLeft
        Me.epErrores.ContainerControl = Me
        '
        'frmLoteEntrada
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(902, 509)
        Me.Controls.Add(Me.splSeparador)
        Me.Controls.Add(Me.grpCabecera)
        Me.Name = "frmLoteEntrada"
        Me.Text = "Albaranes de Entrada"
        CType(Me.grpResultados.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpResultados.Panel.ResumeLayout(False)
        CType(Me.grpResultados, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpResultados.ResumeLayout(False)
        CType(Me.dgvResultadosBuscador, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.splSeparador.Panel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.splSeparador.Panel1.ResumeLayout(False)
        Me.splSeparador.Panel1.PerformLayout()
        CType(Me.splSeparador.Panel2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.splSeparador.Panel2.ResumeLayout(False)
        CType(Me.splSeparador, System.ComponentModel.ISupportInitialize).EndInit()
        Me.splSeparador.ResumeLayout(False)
        CType(Me.hdrBusqueda.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrBusqueda.Panel.ResumeLayout(False)
        CType(Me.hdrBusqueda, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrBusqueda.ResumeLayout(False)
        Me.tblOrganizadorBusqueda.ResumeLayout(False)
        Me.tblOrganizadorBusqueda.PerformLayout()
        CType(Me.cboBSubZona, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboBZonaFAO, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tabGeneral, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabGeneral.ResumeLayout(False)
        CType(Me.tbpDatos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpDatos.ResumeLayout(False)
        Me.tbpDatos.PerformLayout()
        Me.tblOrganizadorDatos.ResumeLayout(False)
        Me.tblOrganizadorDatos.PerformLayout()
        CType(Me.dgvLineas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbpObservaciones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpObservaciones.ResumeLayout(False)
        CType(Me.tbpFacturas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpFacturas.ResumeLayout(False)
        CType(Me.dgvLotesSalida, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grpCabecera.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpCabecera.Panel.ResumeLayout(False)
        Me.grpCabecera.Panel.PerformLayout()
        CType(Me.grpCabecera, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpCabecera.ResumeLayout(False)
        CType(Me.epErrores, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents splSeparador As ComponentFactory.Krypton.Toolkit.KryptonSplitContainer
    Friend WithEvents hdrBusqueda As ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup
    Friend WithEvents btnOcultarBusqueda As ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup
    Friend WithEvents lblTitulo As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents grpCabecera As ComponentFactory.Krypton.Toolkit.KryptonGroup
    Friend WithEvents epErrores As Quadralia.Controles.Validacion.GestorErrores
    Friend WithEvents lblBcodigo As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtBcodigo As Quadralia.Controles.aTextBox
    Friend WithEvents lblBFechaInicio As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents dtpBFechaInicio As Quadralia.Controles.aDateTimePicker
    Friend WithEvents lblobservaciones As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblFormaPago_id As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblCliente_id As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblBProveedor As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents btnBBuscar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents grpResultados As ComponentFactory.Krypton.Toolkit.KryptonGroupBox
    Friend WithEvents dgvResultadosBuscador As ComponentFactory.Krypton.Toolkit.KryptonDataGridView
    Friend WithEvents tblOrganizadorBusqueda As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblBFechaFin As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents dtpBFechaFin As Quadralia.Controles.aDateTimePicker
    Friend WithEvents tabGeneral As ComponentFactory.Krypton.Navigator.KryptonNavigator
    Friend WithEvents tbpDatos As ComponentFactory.Krypton.Navigator.KryptonPage
    Friend WithEvents tblOrganizadorDatos As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblCodigo As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtCodigo As ComponentFactory.Krypton.Toolkit.KryptonTextBox
    Friend WithEvents lblFecha As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents dtpFecha As ComponentFactory.Krypton.Toolkit.KryptonDateTimePicker
    Friend WithEvents dgvLineas As ComponentFactory.Krypton.Toolkit.KryptonDataGridView
    Friend WithEvents lblCliente As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtProveedor As Escritorio.cTextboxBuscador
    Friend WithEvents htmlObservaciones As Escritorio.EditorHTML
    Friend WithEvents tbpObservaciones As ComponentFactory.Krypton.Navigator.KryptonPage
    Friend WithEvents txtBProveedor As Escritorio.cTextboxBuscador
    Friend WithEvents tbpFacturas As ComponentFactory.Krypton.Navigator.KryptonPage
    Friend WithEvents dgvLotesSalida As ComponentFactory.Krypton.Toolkit.KryptonDataGridView
    Friend WithEvents chkBRegistrosEliminados As ComponentFactory.Krypton.Toolkit.KryptonCheckBox
    Friend WithEvents chkReactivarRegistro As ComponentFactory.Krypton.Toolkit.KryptonCheckBox
    Friend WithEvents lblBEspecie As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblBZonaFAO As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtBEspecie As Escritorio.cTextboxBuscador
    Friend WithEvents cboBZonaFAO As Escritorio.Quadralia.Controles.aComboBox
    Friend WithEvents btnLimpiarBusqueda As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents colResultadoCodigo As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents colResultadoFecha As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents Proveedor As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents colFacturaCodigo As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents colFacturaFecha As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents colCliente As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents hdrInfo As ComponentFactory.Krypton.Toolkit.KryptonHeader
    Friend WithEvents txtBNumeroLote As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents lblBNumeroLote As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents cboBSubZona As Escritorio.Quadralia.Controles.aComboBox
    Friend WithEvents lblBSubZona As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents colRefEspecie As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents colRefEntrada As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents colCantidad As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewNumericUpDownColumn
    Friend WithEvents colEspecie As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents colZonaFao As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colEntradaSubzona As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colPresentacion As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colProduccion As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colEntradaBarco As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colNumeroLote As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents colFechaDesembarco As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewDateTimePickerColumn
    Friend WithEvents txtTotal As Quadralia.Controles.aTextBox
    Friend WithEvents KryptonLabel1 As KryptonLabel
End Class
