<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGestionEtiquetas
    Inherits FormularioHijo

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.lblBcodigo = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtBcodigo = New Escritorio.Quadralia.Controles.aTextBox()
        Me.lblBFechaInicio = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.dtpBFechaInicio = New Escritorio.Quadralia.Controles.aDateTimePicker()
        Me.lblobservaciones = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblFormaPago_id = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblCliente_id = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblBProveedor = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.btnBBuscar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.grpResultados = New ComponentFactory.Krypton.Toolkit.KryptonGroupBox()
        Me.dgvResultadosBuscador = New ComponentFactory.Krypton.Toolkit.KryptonDataGridView()
        Me.colResultadoCodigo = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colResultadoFecha = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.Proveedor = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.splSeparador = New ComponentFactory.Krypton.Toolkit.KryptonSplitContainer()
        Me.hdrBusqueda = New ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup()
        Me.btnOcultarBusqueda = New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup()
        Me.tblOrganizadorBusqueda = New System.Windows.Forms.TableLayoutPanel()
        Me.cboBSubZona = New Escritorio.Quadralia.Controles.aComboBox()
        Me.txtBLoteSalida = New Escritorio.Quadralia.Controles.aTextBox()
        Me.btnLimpiarBusqueda = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.lblBFechaFin = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.dtpBFechaFin = New Escritorio.Quadralia.Controles.aDateTimePicker()
        Me.txtBProveedor = New Escritorio.cTextboxBuscador()
        Me.chkBRegistrosEliminados = New ComponentFactory.Krypton.Toolkit.KryptonCheckBox()
        Me.lblBEspecie = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblBZonaFAO = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtBEspecie = New Escritorio.cTextboxBuscador()
        Me.cboBZonaFAO = New Escritorio.Quadralia.Controles.aComboBox()
        Me.lblBLoteSalida = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblBSubZona = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.tabGeneral = New ComponentFactory.Krypton.Navigator.KryptonNavigator()
        Me.tbpDatos = New ComponentFactory.Krypton.Navigator.KryptonPage()
        Me.tblOrganizadorDatos = New System.Windows.Forms.TableLayoutPanel()
        Me.cboDisenho = New Escritorio.Quadralia.Controles.aComboBox()
        Me.lblExpedidor = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblLineaEntrada = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.hdrLineaEntrada = New ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup()
        Me.tblOrganizadorDatosEntrada = New System.Windows.Forms.TableLayoutPanel()
        Me.txtLoteSalida = New Escritorio.Quadralia.Controles.aTextBox()
        Me.KryptonLabel1 = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblProveedor = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblEspecie = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblZona = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblSubzona = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblEmbarcacion = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblPresentacion = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblProduccion = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtProveedor = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtEmbarcacion = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtEspecie = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtZona = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtPresentacion = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtSubzona = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtProduccion = New Escritorio.Quadralia.Controles.aTextBox()
        Me.picQR = New System.Windows.Forms.PictureBox()
        Me.t = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtCodigo = New Escritorio.Quadralia.Controles.aTextBox()
        Me.lblLote = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtLoteEntrada = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtPeso = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtLineaEntrada = New Escritorio.cBuscadorLineaEntrada()
        Me.dtpFecha = New Escritorio.Quadralia.Controles.aDateTimePicker()
        Me.cboExpedidor = New Escritorio.Quadralia.Controles.aComboBox()
        Me.chkSincronizado = New ComponentFactory.Krypton.Toolkit.KryptonCheckBox()
        Me.chkReactivarRegistro = New ComponentFactory.Krypton.Toolkit.KryptonCheckBox()
        Me.lblPeso = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblFecha = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblDisenho = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblTitulo = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.grpCabecera = New ComponentFactory.Krypton.Toolkit.KryptonGroup()
        Me.epErrores = New Escritorio.Quadralia.Controles.Validacion.GestorErrores()
        CType(Me.grpResultados, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpResultados.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpResultados.Panel.SuspendLayout()
        Me.grpResultados.SuspendLayout()
        CType(Me.dgvResultadosBuscador, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.splSeparador, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.splSeparador.Panel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.splSeparador.Panel1.SuspendLayout()
        CType(Me.splSeparador.Panel2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.splSeparador.Panel2.SuspendLayout()
        Me.splSeparador.SuspendLayout()
        CType(Me.hdrBusqueda, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.hdrBusqueda.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.hdrBusqueda.Panel.SuspendLayout()
        Me.hdrBusqueda.SuspendLayout()
        Me.tblOrganizadorBusqueda.SuspendLayout()
        CType(Me.cboBSubZona, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboBZonaFAO, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tabGeneral, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabGeneral.SuspendLayout()
        CType(Me.tbpDatos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpDatos.SuspendLayout()
        Me.tblOrganizadorDatos.SuspendLayout()
        CType(Me.cboDisenho, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.hdrLineaEntrada, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.hdrLineaEntrada.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.hdrLineaEntrada.Panel.SuspendLayout()
        Me.hdrLineaEntrada.SuspendLayout()
        Me.tblOrganizadorDatosEntrada.SuspendLayout()
        CType(Me.picQR, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboExpedidor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpCabecera, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpCabecera.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpCabecera.Panel.SuspendLayout()
        Me.grpCabecera.SuspendLayout()
        CType(Me.epErrores, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblBcodigo
        '
        Me.lblBcodigo.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblBcodigo.Location = New System.Drawing.Point(3, 32)
        Me.lblBcodigo.Name = "lblBcodigo"
        Me.lblBcodigo.Size = New System.Drawing.Size(90, 20)
        Me.lblBcodigo.TabIndex = 0
        Me.lblBcodigo.Values.Text = "Código Etique."
        '
        'txtBcodigo
        '
        Me.txtBcodigo.AlwaysActive = False
        Me.txtBcodigo.controlarBotonBorrar = True
        Me.txtBcodigo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtBcodigo.Formato = ""
        Me.txtBcodigo.Location = New System.Drawing.Point(99, 31)
        Me.txtBcodigo.MaxLength = 48
        Me.txtBcodigo.mostrarSiempreBotonBorrar = False
        Me.txtBcodigo.Name = "txtBcodigo"
        Me.txtBcodigo.seleccionarTodo = True
        Me.txtBcodigo.Size = New System.Drawing.Size(123, 23)
        Me.txtBcodigo.TabIndex = 1
        '
        'lblBFechaInicio
        '
        Me.lblBFechaInicio.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblBFechaInicio.Location = New System.Drawing.Point(3, 60)
        Me.lblBFechaInicio.Name = "lblBFechaInicio"
        Me.lblBFechaInicio.Size = New System.Drawing.Size(75, 20)
        Me.lblBFechaInicio.TabIndex = 2
        Me.lblBFechaInicio.Values.Text = "Fecha Inicio"
        '
        'dtpBFechaInicio
        '
        Me.dtpBFechaInicio.CalendarTodayText = "Hoy:"
        Me.dtpBFechaInicio.Checked = False
        Me.dtpBFechaInicio.controlarBotonBorrar = True
        Me.dtpBFechaInicio.CustomNullText = "(Sin fecha)"
        Me.dtpBFechaInicio.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dtpBFechaInicio.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpBFechaInicio.Location = New System.Drawing.Point(99, 60)
        Me.dtpBFechaInicio.mostrarSiempreBotonBorrar = False
        Me.dtpBFechaInicio.Name = "dtpBFechaInicio"
        Me.dtpBFechaInicio.Size = New System.Drawing.Size(123, 21)
        Me.dtpBFechaInicio.TabIndex = 3
        Me.dtpBFechaInicio.TipoLimpieza = Escritorio.Quadralia.Controles.aDateTimePicker.TiposLimpieza.ValueYValueNullable
        '
        'lblobservaciones
        '
        Me.lblobservaciones.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblobservaciones.Location = New System.Drawing.Point(34, 495)
        Me.lblobservaciones.Name = "lblobservaciones"
        Me.lblobservaciones.Size = New System.Drawing.Size(88, 20)
        Me.lblobservaciones.TabIndex = 0
        Me.lblobservaciones.Values.Text = "observaciones"
        '
        'lblFormaPago_id
        '
        Me.lblFormaPago_id.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblFormaPago_id.Location = New System.Drawing.Point(34, 695)
        Me.lblFormaPago_id.Name = "lblFormaPago_id"
        Me.lblFormaPago_id.Size = New System.Drawing.Size(88, 20)
        Me.lblFormaPago_id.TabIndex = 0
        Me.lblFormaPago_id.Values.Text = "FormaPago_id"
        '
        'lblCliente_id
        '
        Me.lblCliente_id.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblCliente_id.Location = New System.Drawing.Point(34, 715)
        Me.lblCliente_id.Name = "lblCliente_id"
        Me.lblCliente_id.Size = New System.Drawing.Size(64, 20)
        Me.lblCliente_id.TabIndex = 0
        Me.lblCliente_id.Values.Text = "Cliente_id"
        '
        'lblBProveedor
        '
        Me.lblBProveedor.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblBProveedor.Location = New System.Drawing.Point(3, 114)
        Me.lblBProveedor.Name = "lblBProveedor"
        Me.lblBProveedor.Size = New System.Drawing.Size(67, 20)
        Me.lblBProveedor.TabIndex = 6
        Me.lblBProveedor.Values.Text = "Proveedor"
        '
        'btnBBuscar
        '
        Me.tblOrganizadorBusqueda.SetColumnSpan(Me.btnBBuscar, 2)
        Me.btnBBuscar.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnBBuscar.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnBBuscar.Location = New System.Drawing.Point(3, 275)
        Me.btnBBuscar.Name = "btnBBuscar"
        Me.btnBBuscar.Size = New System.Drawing.Size(219, 25)
        Me.btnBBuscar.TabIndex = 9
        Me.btnBBuscar.Values.Text = "Buscar"
        '
        'grpResultados
        '
        Me.tblOrganizadorBusqueda.SetColumnSpan(Me.grpResultados, 2)
        Me.grpResultados.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grpResultados.Location = New System.Drawing.Point(3, 306)
        Me.grpResultados.Name = "grpResultados"
        '
        'grpResultados.Panel
        '
        Me.grpResultados.Panel.Controls.Add(Me.dgvResultadosBuscador)
        Me.grpResultados.Size = New System.Drawing.Size(219, 168)
        Me.grpResultados.TabIndex = 10
        Me.grpResultados.Text = "Resultados"
        Me.grpResultados.Values.Heading = "Resultados"
        '
        'dgvResultadosBuscador
        '
        Me.dgvResultadosBuscador.AllowUserToAddRows = False
        Me.dgvResultadosBuscador.AllowUserToDeleteRows = False
        Me.dgvResultadosBuscador.AllowUserToResizeRows = False
        Me.dgvResultadosBuscador.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colResultadoCodigo, Me.colResultadoFecha, Me.Proveedor})
        Me.dgvResultadosBuscador.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvResultadosBuscador.Location = New System.Drawing.Point(0, 0)
        Me.dgvResultadosBuscador.Name = "dgvResultadosBuscador"
        Me.dgvResultadosBuscador.ReadOnly = True
        Me.dgvResultadosBuscador.RowHeadersVisible = False
        Me.dgvResultadosBuscador.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvResultadosBuscador.Size = New System.Drawing.Size(215, 144)
        Me.dgvResultadosBuscador.TabIndex = 0
        '
        'colResultadoCodigo
        '
        Me.colResultadoCodigo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colResultadoCodigo.DataPropertyName = "codigo"
        Me.colResultadoCodigo.HeaderText = "Cód."
        Me.colResultadoCodigo.Name = "colResultadoCodigo"
        Me.colResultadoCodigo.ReadOnly = True
        Me.colResultadoCodigo.Width = 61
        '
        'colResultadoFecha
        '
        Me.colResultadoFecha.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colResultadoFecha.DataPropertyName = "fecha"
        DataGridViewCellStyle1.Format = "d"
        DataGridViewCellStyle1.NullValue = Nothing
        Me.colResultadoFecha.DefaultCellStyle = DataGridViewCellStyle1
        Me.colResultadoFecha.HeaderText = "Fecha"
        Me.colResultadoFecha.Name = "colResultadoFecha"
        Me.colResultadoFecha.ReadOnly = True
        Me.colResultadoFecha.Width = 67
        '
        'Proveedor
        '
        Me.Proveedor.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Proveedor.DataPropertyName = "nombreProveedor"
        Me.Proveedor.HeaderText = "Proveedor"
        Me.Proveedor.MinimumWidth = 80
        Me.Proveedor.Name = "Proveedor"
        Me.Proveedor.ReadOnly = True
        Me.Proveedor.Width = 86
        '
        'splSeparador
        '
        Me.splSeparador.Cursor = System.Windows.Forms.Cursors.Default
        Me.splSeparador.Dock = System.Windows.Forms.DockStyle.Fill
        Me.splSeparador.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.splSeparador.Location = New System.Drawing.Point(0, 57)
        Me.splSeparador.Name = "splSeparador"
        '
        'splSeparador.Panel1
        '
        Me.splSeparador.Panel1.Controls.Add(Me.hdrBusqueda)
        Me.splSeparador.Panel1MinSize = 220
        '
        'splSeparador.Panel2
        '
        Me.splSeparador.Panel2.Controls.Add(Me.tabGeneral)
        Me.splSeparador.Size = New System.Drawing.Size(881, 514)
        Me.splSeparador.SplitterDistance = 227
        Me.splSeparador.TabIndex = 1
        '
        'hdrBusqueda
        '
        Me.hdrBusqueda.AutoSize = True
        Me.hdrBusqueda.ButtonSpecs.AddRange(New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup() {Me.btnOcultarBusqueda})
        Me.hdrBusqueda.Dock = System.Windows.Forms.DockStyle.Fill
        Me.hdrBusqueda.HeaderVisibleSecondary = False
        Me.hdrBusqueda.Location = New System.Drawing.Point(0, 0)
        Me.hdrBusqueda.Name = "hdrBusqueda"
        '
        'hdrBusqueda.Panel
        '
        Me.hdrBusqueda.Panel.Controls.Add(Me.tblOrganizadorBusqueda)
        Me.hdrBusqueda.Size = New System.Drawing.Size(227, 514)
        Me.hdrBusqueda.TabIndex = 0
        Me.hdrBusqueda.ValuesPrimary.Heading = "Buscar"
        Me.hdrBusqueda.ValuesPrimary.Image = Global.Escritorio.My.Resources.Resources.Buscar_32
        '
        'btnOcultarBusqueda
        '
        Me.btnOcultarBusqueda.Type = ComponentFactory.Krypton.Toolkit.PaletteButtonSpecStyle.ArrowLeft
        Me.btnOcultarBusqueda.UniqueName = "BE70F7722CF94C60618F65819BBF010D"
        '
        'tblOrganizadorBusqueda
        '
        Me.tblOrganizadorBusqueda.BackColor = System.Drawing.Color.White
        Me.tblOrganizadorBusqueda.ColumnCount = 2
        Me.tblOrganizadorBusqueda.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblOrganizadorBusqueda.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblOrganizadorBusqueda.Controls.Add(Me.cboBSubZona, 1, 7)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.txtBLoteSalida, 1, 8)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.btnLimpiarBusqueda, 0, 0)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.lblBcodigo, 0, 1)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.dtpBFechaInicio, 1, 2)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.grpResultados, 0, 11)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.btnBBuscar, 0, 10)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.lblBProveedor, 0, 4)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.lblBFechaInicio, 0, 2)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.txtBcodigo, 1, 1)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.lblBFechaFin, 0, 3)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.dtpBFechaFin, 1, 3)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.txtBProveedor, 1, 4)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.chkBRegistrosEliminados, 0, 9)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.lblBEspecie, 0, 5)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.lblBZonaFAO, 0, 6)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.txtBEspecie, 1, 5)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.cboBZonaFAO, 1, 6)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.lblBLoteSalida, 0, 8)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.lblBSubZona, 0, 7)
        Me.tblOrganizadorBusqueda.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblOrganizadorBusqueda.Location = New System.Drawing.Point(0, 0)
        Me.tblOrganizadorBusqueda.Name = "tblOrganizadorBusqueda"
        Me.tblOrganizadorBusqueda.RowCount = 12
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorBusqueda.Size = New System.Drawing.Size(225, 477)
        Me.tblOrganizadorBusqueda.TabIndex = 0
        '
        'cboBSubZona
        '
        Me.cboBSubZona.controlarBotonBorrar = True
        Me.cboBSubZona.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cboBSubZona.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBSubZona.DropDownWidth = 131
        Me.cboBSubZona.Location = New System.Drawing.Point(99, 193)
        Me.cboBSubZona.mostrarSiempreBotonBorrar = False
        Me.cboBSubZona.Name = "cboBSubZona"
        Me.cboBSubZona.Size = New System.Drawing.Size(123, 21)
        Me.cboBSubZona.TabIndex = 19
        '
        'txtBLoteSalida
        '
        Me.txtBLoteSalida.AlwaysActive = False
        Me.txtBLoteSalida.controlarBotonBorrar = True
        Me.txtBLoteSalida.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtBLoteSalida.Formato = ""
        Me.txtBLoteSalida.Location = New System.Drawing.Point(99, 220)
        Me.txtBLoteSalida.MaxLength = 48
        Me.txtBLoteSalida.mostrarSiempreBotonBorrar = False
        Me.txtBLoteSalida.Name = "txtBLoteSalida"
        Me.txtBLoteSalida.seleccionarTodo = True
        Me.txtBLoteSalida.Size = New System.Drawing.Size(123, 23)
        Me.txtBLoteSalida.TabIndex = 18
        '
        'btnLimpiarBusqueda
        '
        Me.btnLimpiarBusqueda.AutoSize = True
        Me.btnLimpiarBusqueda.ButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.ButtonSpec
        Me.tblOrganizadorBusqueda.SetColumnSpan(Me.btnLimpiarBusqueda, 2)
        Me.btnLimpiarBusqueda.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnLimpiarBusqueda.Location = New System.Drawing.Point(97, 3)
        Me.btnLimpiarBusqueda.Name = "btnLimpiarBusqueda"
        Me.btnLimpiarBusqueda.Size = New System.Drawing.Size(125, 22)
        Me.btnLimpiarBusqueda.TabIndex = 13
        Me.btnLimpiarBusqueda.Values.Image = Global.Escritorio.My.Resources.Resources.Limpiar_16
        Me.btnLimpiarBusqueda.Values.Text = "Limpiar Búsqueda"
        '
        'lblBFechaFin
        '
        Me.lblBFechaFin.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblBFechaFin.Location = New System.Drawing.Point(3, 87)
        Me.lblBFechaFin.Name = "lblBFechaFin"
        Me.lblBFechaFin.Size = New System.Drawing.Size(61, 20)
        Me.lblBFechaFin.TabIndex = 4
        Me.lblBFechaFin.Values.Text = "Fecha Fin"
        '
        'dtpBFechaFin
        '
        Me.dtpBFechaFin.CalendarTodayText = "Hoy:"
        Me.dtpBFechaFin.Checked = False
        Me.dtpBFechaFin.controlarBotonBorrar = True
        Me.dtpBFechaFin.CustomNullText = "(Sin fecha)"
        Me.dtpBFechaFin.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dtpBFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpBFechaFin.Location = New System.Drawing.Point(99, 87)
        Me.dtpBFechaFin.mostrarSiempreBotonBorrar = False
        Me.dtpBFechaFin.Name = "dtpBFechaFin"
        Me.dtpBFechaFin.Size = New System.Drawing.Size(123, 21)
        Me.dtpBFechaFin.TabIndex = 5
        Me.dtpBFechaFin.TipoLimpieza = Escritorio.Quadralia.Controles.aDateTimePicker.TiposLimpieza.ValueYValueNullable
        '
        'txtBProveedor
        '
        Me.txtBProveedor.DescripcionReadOnly = False
        Me.txtBProveedor.DescripcionVisible = False
        Me.txtBProveedor.DisplayMember = "nombreComercial"
        Me.txtBProveedor.DisplayText = ""
        Me.txtBProveedor.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtBProveedor.FormularioBusqueda = "Escritorio.frmSeleccionarProveedor"
        Me.txtBProveedor.Item = Nothing
        Me.txtBProveedor.Location = New System.Drawing.Point(99, 114)
        Me.txtBProveedor.Name = "txtBProveedor"
        Me.txtBProveedor.Size = New System.Drawing.Size(123, 20)
        Me.txtBProveedor.TabIndex = 7
        Me.txtBProveedor.UseOnlyNumbers = False
        Me.txtBProveedor.UseUpperCase = True
        Me.txtBProveedor.Validar = True
        Me.txtBProveedor.ValueMember = "codigo"
        Me.txtBProveedor.ValueText = ""
        '
        'chkBRegistrosEliminados
        '
        Me.tblOrganizadorBusqueda.SetColumnSpan(Me.chkBRegistrosEliminados, 2)
        Me.chkBRegistrosEliminados.Dock = System.Windows.Forms.DockStyle.Fill
        Me.chkBRegistrosEliminados.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl
        Me.chkBRegistrosEliminados.Location = New System.Drawing.Point(3, 249)
        Me.chkBRegistrosEliminados.Name = "chkBRegistrosEliminados"
        Me.chkBRegistrosEliminados.Size = New System.Drawing.Size(219, 20)
        Me.chkBRegistrosEliminados.TabIndex = 8
        Me.chkBRegistrosEliminados.Text = "Incluir etiquetas descartadas"
        Me.chkBRegistrosEliminados.Values.Text = "Incluir etiquetas descartadas"
        '
        'lblBEspecie
        '
        Me.lblBEspecie.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblBEspecie.Location = New System.Drawing.Point(3, 140)
        Me.lblBEspecie.Name = "lblBEspecie"
        Me.lblBEspecie.Size = New System.Drawing.Size(51, 20)
        Me.lblBEspecie.TabIndex = 6
        Me.lblBEspecie.Values.Text = "Especie"
        '
        'lblBZonaFAO
        '
        Me.lblBZonaFAO.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblBZonaFAO.Location = New System.Drawing.Point(3, 166)
        Me.lblBZonaFAO.Name = "lblBZonaFAO"
        Me.lblBZonaFAO.Size = New System.Drawing.Size(65, 20)
        Me.lblBZonaFAO.TabIndex = 6
        Me.lblBZonaFAO.Values.Text = "Zona FAO"
        '
        'txtBEspecie
        '
        Me.txtBEspecie.DescripcionReadOnly = False
        Me.txtBEspecie.DescripcionVisible = False
        Me.txtBEspecie.DisplayMember = "denominacionComercial"
        Me.txtBEspecie.DisplayText = ""
        Me.txtBEspecie.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtBEspecie.FormularioBusqueda = "Escritorio.frmSeleccionarEspecies"
        Me.txtBEspecie.Item = Nothing
        Me.txtBEspecie.Location = New System.Drawing.Point(99, 140)
        Me.txtBEspecie.Name = "txtBEspecie"
        Me.txtBEspecie.Size = New System.Drawing.Size(123, 20)
        Me.txtBEspecie.TabIndex = 11
        Me.txtBEspecie.UseOnlyNumbers = False
        Me.txtBEspecie.UseUpperCase = True
        Me.txtBEspecie.Validar = True
        Me.txtBEspecie.ValueMember = "alfa3"
        Me.txtBEspecie.ValueText = ""
        '
        'cboBZonaFAO
        '
        Me.cboBZonaFAO.controlarBotonBorrar = True
        Me.cboBZonaFAO.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cboBZonaFAO.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBZonaFAO.DropDownWidth = 131
        Me.cboBZonaFAO.Location = New System.Drawing.Point(99, 166)
        Me.cboBZonaFAO.mostrarSiempreBotonBorrar = False
        Me.cboBZonaFAO.Name = "cboBZonaFAO"
        Me.cboBZonaFAO.Size = New System.Drawing.Size(123, 21)
        Me.cboBZonaFAO.TabIndex = 12
        '
        'lblBLoteSalida
        '
        Me.lblBLoteSalida.Location = New System.Drawing.Point(3, 220)
        Me.lblBLoteSalida.Name = "lblBLoteSalida"
        Me.lblBLoteSalida.Size = New System.Drawing.Size(70, 20)
        Me.lblBLoteSalida.TabIndex = 16
        Me.lblBLoteSalida.Values.Text = "Lote Salida"
        '
        'lblBSubZona
        '
        Me.lblBSubZona.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblBSubZona.Location = New System.Drawing.Point(3, 193)
        Me.lblBSubZona.Name = "lblBSubZona"
        Me.lblBSubZona.Size = New System.Drawing.Size(62, 20)
        Me.lblBSubZona.TabIndex = 6
        Me.lblBSubZona.Values.Text = "Sub Zona"
        '
        'tabGeneral
        '
        Me.tabGeneral.AllowPageReorder = False
        Me.tabGeneral.Button.CloseButtonAction = ComponentFactory.Krypton.Navigator.CloseButtonAction.None
        Me.tabGeneral.Button.CloseButtonDisplay = ComponentFactory.Krypton.Navigator.ButtonDisplay.Hide
        Me.tabGeneral.Button.ContextButtonAction = ComponentFactory.Krypton.Navigator.ContextButtonAction.None
        Me.tabGeneral.Button.ContextButtonDisplay = ComponentFactory.Krypton.Navigator.ButtonDisplay.Hide
        Me.tabGeneral.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabGeneral.Location = New System.Drawing.Point(0, 0)
        Me.tabGeneral.Name = "tabGeneral"
        Me.tabGeneral.Pages.AddRange(New ComponentFactory.Krypton.Navigator.KryptonPage() {Me.tbpDatos})
        Me.tabGeneral.SelectedIndex = 0
        Me.tabGeneral.Size = New System.Drawing.Size(649, 514)
        Me.tabGeneral.TabIndex = 0
        Me.tabGeneral.Text = "KryptonNavigator1"
        '
        'tbpDatos
        '
        Me.tbpDatos.AutoHiddenSlideSize = New System.Drawing.Size(200, 200)
        Me.tbpDatos.Controls.Add(Me.tblOrganizadorDatos)
        Me.tbpDatos.Controls.Add(Me.lblCliente_id)
        Me.tbpDatos.Controls.Add(Me.lblFormaPago_id)
        Me.tbpDatos.Controls.Add(Me.lblobservaciones)
        Me.tbpDatos.Flags = 65534
        Me.tbpDatos.LastVisibleSet = True
        Me.tbpDatos.MinimumSize = New System.Drawing.Size(50, 50)
        Me.tbpDatos.Name = "tbpDatos"
        Me.tbpDatos.Size = New System.Drawing.Size(647, 487)
        Me.tbpDatos.Text = "Datos"
        Me.tbpDatos.ToolTipTitle = "Page ToolTip"
        Me.tbpDatos.UniqueName = "A7344070A3E64BA24893A75E8104A8EE"
        '
        'tblOrganizadorDatos
        '
        Me.tblOrganizadorDatos.BackColor = System.Drawing.Color.FromArgb(CType(CType(187, Byte), Integer), CType(CType(206, Byte), Integer), CType(CType(230, Byte), Integer))
        Me.tblOrganizadorDatos.ColumnCount = 6
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorDatos.Controls.Add(Me.cboDisenho, 2, 3)
        Me.tblOrganizadorDatos.Controls.Add(Me.lblExpedidor, 0, 1)
        Me.tblOrganizadorDatos.Controls.Add(Me.lblLineaEntrada, 0, 2)
        Me.tblOrganizadorDatos.Controls.Add(Me.hdrLineaEntrada, 0, 6)
        Me.tblOrganizadorDatos.Controls.Add(Me.txtPeso, 5, 0)
        Me.tblOrganizadorDatos.Controls.Add(Me.txtLineaEntrada, 2, 2)
        Me.tblOrganizadorDatos.Controls.Add(Me.dtpFecha, 2, 0)
        Me.tblOrganizadorDatos.Controls.Add(Me.cboExpedidor, 2, 1)
        Me.tblOrganizadorDatos.Controls.Add(Me.chkSincronizado, 3, 4)
        Me.tblOrganizadorDatos.Controls.Add(Me.chkReactivarRegistro, 0, 4)
        Me.tblOrganizadorDatos.Controls.Add(Me.lblPeso, 3, 0)
        Me.tblOrganizadorDatos.Controls.Add(Me.lblFecha, 0, 0)
        Me.tblOrganizadorDatos.Controls.Add(Me.lblDisenho, 0, 3)
        Me.tblOrganizadorDatos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblOrganizadorDatos.Location = New System.Drawing.Point(0, 0)
        Me.tblOrganizadorDatos.Name = "tblOrganizadorDatos"
        Me.tblOrganizadorDatos.RowCount = 7
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorDatos.Size = New System.Drawing.Size(647, 487)
        Me.tblOrganizadorDatos.TabIndex = 0
        '
        'cboDisenho
        '
        Me.tblOrganizadorDatos.SetColumnSpan(Me.cboDisenho, 4)
        Me.cboDisenho.controlarBotonBorrar = True
        Me.cboDisenho.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cboDisenho.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDisenho.DropDownWidth = 131
        Me.cboDisenho.Location = New System.Drawing.Point(113, 85)
        Me.cboDisenho.mostrarSiempreBotonBorrar = False
        Me.cboDisenho.Name = "cboDisenho"
        Me.cboDisenho.Size = New System.Drawing.Size(531, 21)
        Me.cboDisenho.TabIndex = 13
        '
        'lblExpedidor
        '
        Me.lblExpedidor.Location = New System.Drawing.Point(3, 32)
        Me.lblExpedidor.Name = "lblExpedidor"
        Me.lblExpedidor.Size = New System.Drawing.Size(65, 20)
        Me.lblExpedidor.TabIndex = 0
        Me.lblExpedidor.Values.Text = "Expedidor"
        '
        'lblLineaEntrada
        '
        Me.lblLineaEntrada.Location = New System.Drawing.Point(3, 59)
        Me.lblLineaEntrada.Name = "lblLineaEntrada"
        Me.lblLineaEntrada.Size = New System.Drawing.Size(84, 20)
        Me.lblLineaEntrada.TabIndex = 0
        Me.lblLineaEntrada.Values.Text = "Linea Entrada"
        '
        'hdrLineaEntrada
        '
        Me.tblOrganizadorDatos.SetColumnSpan(Me.hdrLineaEntrada, 6)
        Me.hdrLineaEntrada.Dock = System.Windows.Forms.DockStyle.Fill
        Me.hdrLineaEntrada.HeaderVisibleSecondary = False
        Me.hdrLineaEntrada.Location = New System.Drawing.Point(3, 158)
        Me.hdrLineaEntrada.Name = "hdrLineaEntrada"
        '
        'hdrLineaEntrada.Panel
        '
        Me.hdrLineaEntrada.Panel.Controls.Add(Me.tblOrganizadorDatosEntrada)
        Me.hdrLineaEntrada.Size = New System.Drawing.Size(641, 326)
        Me.hdrLineaEntrada.TabIndex = 2
        Me.hdrLineaEntrada.ValuesPrimary.Heading = "Datos Linea Entrada"
        Me.hdrLineaEntrada.ValuesPrimary.Image = Nothing
        '
        'tblOrganizadorDatosEntrada
        '
        Me.tblOrganizadorDatosEntrada.BackColor = System.Drawing.Color.Transparent
        Me.tblOrganizadorDatosEntrada.ColumnCount = 5
        Me.tblOrganizadorDatosEntrada.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblOrganizadorDatosEntrada.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblOrganizadorDatosEntrada.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblOrganizadorDatosEntrada.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorDatosEntrada.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblOrganizadorDatosEntrada.Controls.Add(Me.txtLoteSalida, 4, 1)
        Me.tblOrganizadorDatosEntrada.Controls.Add(Me.KryptonLabel1, 2, 1)
        Me.tblOrganizadorDatosEntrada.Controls.Add(Me.lblProveedor, 0, 2)
        Me.tblOrganizadorDatosEntrada.Controls.Add(Me.lblEspecie, 0, 4)
        Me.tblOrganizadorDatosEntrada.Controls.Add(Me.lblZona, 0, 5)
        Me.tblOrganizadorDatosEntrada.Controls.Add(Me.lblSubzona, 2, 5)
        Me.tblOrganizadorDatosEntrada.Controls.Add(Me.lblEmbarcacion, 0, 3)
        Me.tblOrganizadorDatosEntrada.Controls.Add(Me.lblPresentacion, 0, 6)
        Me.tblOrganizadorDatosEntrada.Controls.Add(Me.lblProduccion, 2, 6)
        Me.tblOrganizadorDatosEntrada.Controls.Add(Me.txtProveedor, 1, 2)
        Me.tblOrganizadorDatosEntrada.Controls.Add(Me.txtEmbarcacion, 1, 3)
        Me.tblOrganizadorDatosEntrada.Controls.Add(Me.txtEspecie, 1, 4)
        Me.tblOrganizadorDatosEntrada.Controls.Add(Me.txtZona, 1, 5)
        Me.tblOrganizadorDatosEntrada.Controls.Add(Me.txtPresentacion, 1, 6)
        Me.tblOrganizadorDatosEntrada.Controls.Add(Me.txtSubzona, 4, 5)
        Me.tblOrganizadorDatosEntrada.Controls.Add(Me.txtProduccion, 4, 6)
        Me.tblOrganizadorDatosEntrada.Controls.Add(Me.picQR, 0, 7)
        Me.tblOrganizadorDatosEntrada.Controls.Add(Me.t, 0, 0)
        Me.tblOrganizadorDatosEntrada.Controls.Add(Me.txtCodigo, 1, 0)
        Me.tblOrganizadorDatosEntrada.Controls.Add(Me.lblLote, 0, 1)
        Me.tblOrganizadorDatosEntrada.Controls.Add(Me.txtLoteEntrada, 1, 1)
        Me.tblOrganizadorDatosEntrada.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblOrganizadorDatosEntrada.Location = New System.Drawing.Point(0, 0)
        Me.tblOrganizadorDatosEntrada.Name = "tblOrganizadorDatosEntrada"
        Me.tblOrganizadorDatosEntrada.RowCount = 8
        Me.tblOrganizadorDatosEntrada.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatosEntrada.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatosEntrada.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatosEntrada.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatosEntrada.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatosEntrada.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatosEntrada.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatosEntrada.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorDatosEntrada.Size = New System.Drawing.Size(639, 294)
        Me.tblOrganizadorDatosEntrada.TabIndex = 0
        '
        'txtLoteSalida
        '
        Me.txtLoteSalida.controlarBotonBorrar = True
        Me.txtLoteSalida.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtLoteSalida.Formato = ""
        Me.txtLoteSalida.Location = New System.Drawing.Point(414, 32)
        Me.txtLoteSalida.mostrarSiempreBotonBorrar = False
        Me.txtLoteSalida.Name = "txtLoteSalida"
        Me.txtLoteSalida.seleccionarTodo = True
        Me.txtLoteSalida.Size = New System.Drawing.Size(222, 23)
        Me.txtLoteSalida.TabIndex = 2
        '
        'KryptonLabel1
        '
        Me.KryptonLabel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.KryptonLabel1.Location = New System.Drawing.Point(316, 32)
        Me.KryptonLabel1.Name = "KryptonLabel1"
        Me.KryptonLabel1.Size = New System.Drawing.Size(72, 23)
        Me.KryptonLabel1.TabIndex = 1
        Me.KryptonLabel1.Values.Text = "Lote Salida"
        '
        'lblProveedor
        '
        Me.lblProveedor.Location = New System.Drawing.Point(3, 61)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(67, 20)
        Me.lblProveedor.TabIndex = 0
        Me.lblProveedor.Values.Text = "Proveedor"
        '
        'lblEspecie
        '
        Me.lblEspecie.Location = New System.Drawing.Point(3, 119)
        Me.lblEspecie.Name = "lblEspecie"
        Me.lblEspecie.Size = New System.Drawing.Size(51, 20)
        Me.lblEspecie.TabIndex = 0
        Me.lblEspecie.Values.Text = "Especie"
        '
        'lblZona
        '
        Me.lblZona.Location = New System.Drawing.Point(3, 148)
        Me.lblZona.Name = "lblZona"
        Me.lblZona.Size = New System.Drawing.Size(38, 20)
        Me.lblZona.TabIndex = 0
        Me.lblZona.Values.Text = "Zona"
        '
        'lblSubzona
        '
        Me.lblSubzona.Location = New System.Drawing.Point(316, 148)
        Me.lblSubzona.Name = "lblSubzona"
        Me.lblSubzona.Size = New System.Drawing.Size(57, 20)
        Me.lblSubzona.TabIndex = 0
        Me.lblSubzona.Values.Text = "Subzona"
        '
        'lblEmbarcacion
        '
        Me.lblEmbarcacion.Location = New System.Drawing.Point(3, 90)
        Me.lblEmbarcacion.Name = "lblEmbarcacion"
        Me.lblEmbarcacion.Size = New System.Drawing.Size(80, 20)
        Me.lblEmbarcacion.TabIndex = 0
        Me.lblEmbarcacion.Values.Text = "Embarcación"
        '
        'lblPresentacion
        '
        Me.lblPresentacion.Location = New System.Drawing.Point(3, 177)
        Me.lblPresentacion.Name = "lblPresentacion"
        Me.lblPresentacion.Size = New System.Drawing.Size(80, 20)
        Me.lblPresentacion.TabIndex = 0
        Me.lblPresentacion.Values.Text = "Presentacion"
        '
        'lblProduccion
        '
        Me.lblProduccion.Location = New System.Drawing.Point(316, 177)
        Me.lblProduccion.Name = "lblProduccion"
        Me.lblProduccion.Size = New System.Drawing.Size(72, 20)
        Me.lblProduccion.TabIndex = 0
        Me.lblProduccion.Values.Text = "Producción"
        '
        'txtProveedor
        '
        Me.tblOrganizadorDatosEntrada.SetColumnSpan(Me.txtProveedor, 4)
        Me.txtProveedor.controlarBotonBorrar = True
        Me.txtProveedor.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtProveedor.Formato = ""
        Me.txtProveedor.Location = New System.Drawing.Point(89, 61)
        Me.txtProveedor.mostrarSiempreBotonBorrar = False
        Me.txtProveedor.Name = "txtProveedor"
        Me.txtProveedor.seleccionarTodo = True
        Me.txtProveedor.Size = New System.Drawing.Size(547, 23)
        Me.txtProveedor.TabIndex = 1
        '
        'txtEmbarcacion
        '
        Me.tblOrganizadorDatosEntrada.SetColumnSpan(Me.txtEmbarcacion, 4)
        Me.txtEmbarcacion.controlarBotonBorrar = True
        Me.txtEmbarcacion.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtEmbarcacion.Formato = ""
        Me.txtEmbarcacion.Location = New System.Drawing.Point(89, 90)
        Me.txtEmbarcacion.mostrarSiempreBotonBorrar = False
        Me.txtEmbarcacion.Name = "txtEmbarcacion"
        Me.txtEmbarcacion.seleccionarTodo = True
        Me.txtEmbarcacion.Size = New System.Drawing.Size(547, 23)
        Me.txtEmbarcacion.TabIndex = 1
        '
        'txtEspecie
        '
        Me.tblOrganizadorDatosEntrada.SetColumnSpan(Me.txtEspecie, 4)
        Me.txtEspecie.controlarBotonBorrar = True
        Me.txtEspecie.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtEspecie.Formato = ""
        Me.txtEspecie.Location = New System.Drawing.Point(89, 119)
        Me.txtEspecie.mostrarSiempreBotonBorrar = False
        Me.txtEspecie.Name = "txtEspecie"
        Me.txtEspecie.seleccionarTodo = True
        Me.txtEspecie.Size = New System.Drawing.Size(547, 23)
        Me.txtEspecie.TabIndex = 1
        '
        'txtZona
        '
        Me.txtZona.controlarBotonBorrar = True
        Me.txtZona.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtZona.Formato = ""
        Me.txtZona.Location = New System.Drawing.Point(89, 148)
        Me.txtZona.mostrarSiempreBotonBorrar = False
        Me.txtZona.Name = "txtZona"
        Me.txtZona.seleccionarTodo = True
        Me.txtZona.Size = New System.Drawing.Size(221, 23)
        Me.txtZona.TabIndex = 1
        '
        'txtPresentacion
        '
        Me.txtPresentacion.controlarBotonBorrar = True
        Me.txtPresentacion.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtPresentacion.Formato = ""
        Me.txtPresentacion.Location = New System.Drawing.Point(89, 177)
        Me.txtPresentacion.mostrarSiempreBotonBorrar = False
        Me.txtPresentacion.Name = "txtPresentacion"
        Me.txtPresentacion.seleccionarTodo = True
        Me.txtPresentacion.Size = New System.Drawing.Size(221, 23)
        Me.txtPresentacion.TabIndex = 1
        '
        'txtSubzona
        '
        Me.txtSubzona.controlarBotonBorrar = True
        Me.txtSubzona.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtSubzona.Formato = ""
        Me.txtSubzona.Location = New System.Drawing.Point(414, 148)
        Me.txtSubzona.mostrarSiempreBotonBorrar = False
        Me.txtSubzona.Name = "txtSubzona"
        Me.txtSubzona.seleccionarTodo = True
        Me.txtSubzona.Size = New System.Drawing.Size(222, 23)
        Me.txtSubzona.TabIndex = 1
        '
        'txtProduccion
        '
        Me.txtProduccion.controlarBotonBorrar = True
        Me.txtProduccion.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtProduccion.Formato = ""
        Me.txtProduccion.Location = New System.Drawing.Point(414, 177)
        Me.txtProduccion.mostrarSiempreBotonBorrar = False
        Me.txtProduccion.Name = "txtProduccion"
        Me.txtProduccion.seleccionarTodo = True
        Me.txtProduccion.Size = New System.Drawing.Size(222, 23)
        Me.txtProduccion.TabIndex = 1
        '
        'picQR
        '
        Me.picQR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.picQR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tblOrganizadorDatosEntrada.SetColumnSpan(Me.picQR, 5)
        Me.picQR.Dock = System.Windows.Forms.DockStyle.Fill
        Me.picQR.Location = New System.Drawing.Point(3, 206)
        Me.picQR.Name = "picQR"
        Me.picQR.Size = New System.Drawing.Size(633, 85)
        Me.picQR.TabIndex = 3
        Me.picQR.TabStop = False
        '
        't
        '
        Me.t.Location = New System.Drawing.Point(3, 3)
        Me.t.Name = "t"
        Me.t.Size = New System.Drawing.Size(71, 20)
        Me.t.TabIndex = 0
        Me.t.Values.Text = "Nº Albarán"
        '
        'txtCodigo
        '
        Me.txtCodigo.controlarBotonBorrar = True
        Me.txtCodigo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtCodigo.Formato = ""
        Me.txtCodigo.Location = New System.Drawing.Point(89, 3)
        Me.txtCodigo.mostrarSiempreBotonBorrar = False
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.seleccionarTodo = True
        Me.txtCodigo.Size = New System.Drawing.Size(221, 23)
        Me.txtCodigo.TabIndex = 1
        '
        'lblLote
        '
        Me.lblLote.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblLote.Location = New System.Drawing.Point(3, 32)
        Me.lblLote.Name = "lblLote"
        Me.lblLote.Size = New System.Drawing.Size(80, 23)
        Me.lblLote.TabIndex = 0
        Me.lblLote.Values.Text = "Lote Entrada"
        '
        'txtLoteEntrada
        '
        Me.txtLoteEntrada.controlarBotonBorrar = True
        Me.txtLoteEntrada.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtLoteEntrada.Formato = ""
        Me.txtLoteEntrada.Location = New System.Drawing.Point(89, 32)
        Me.txtLoteEntrada.mostrarSiempreBotonBorrar = False
        Me.txtLoteEntrada.Name = "txtLoteEntrada"
        Me.txtLoteEntrada.seleccionarTodo = True
        Me.txtLoteEntrada.Size = New System.Drawing.Size(221, 23)
        Me.txtLoteEntrada.TabIndex = 1
        '
        'txtPeso
        '
        Me.txtPeso.controlarBotonBorrar = True
        Me.txtPeso.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtPeso.Formato = ""
        Me.txtPeso.Location = New System.Drawing.Point(412, 3)
        Me.txtPeso.mostrarSiempreBotonBorrar = False
        Me.txtPeso.Name = "txtPeso"
        Me.txtPeso.seleccionarTodo = True
        Me.txtPeso.Size = New System.Drawing.Size(232, 23)
        Me.txtPeso.TabIndex = 1
        '
        'txtLineaEntrada
        '
        Me.tblOrganizadorDatos.SetColumnSpan(Me.txtLineaEntrada, 4)
        Me.txtLineaEntrada.DescripcionReadOnly = False
        Me.txtLineaEntrada.DescripcionVisible = True
        Me.txtLineaEntrada.DisplayMember = "Descripcion"
        Me.txtLineaEntrada.DisplayText = ""
        Me.txtLineaEntrada.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtLineaEntrada.FormularioBusqueda = "Escritorio.frmSeleccionarDesgloseAlbaranEntrada"
        Me.txtLineaEntrada.Item = Nothing
        Me.txtLineaEntrada.Location = New System.Drawing.Point(113, 59)
        Me.txtLineaEntrada.Name = "txtLineaEntrada"
        Me.txtLineaEntrada.Size = New System.Drawing.Size(531, 20)
        Me.txtLineaEntrada.TabIndex = 3
        Me.txtLineaEntrada.UseOnlyNumbers = False
        Me.txtLineaEntrada.UseUpperCase = True
        Me.txtLineaEntrada.Validar = True
        Me.txtLineaEntrada.ValueMember = "numeroLote"
        Me.txtLineaEntrada.ValueText = ""
        '
        'dtpFecha
        '
        Me.dtpFecha.CalendarTodayText = "Hoy:"
        Me.dtpFecha.Checked = False
        Me.dtpFecha.controlarBotonBorrar = True
        Me.dtpFecha.CustomNullText = "(Sin fecha)"
        Me.dtpFecha.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecha.Location = New System.Drawing.Point(113, 3)
        Me.dtpFecha.mostrarSiempreBotonBorrar = False
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(231, 23)
        Me.dtpFecha.TabIndex = 5
        Me.dtpFecha.TipoLimpieza = Escritorio.Quadralia.Controles.aDateTimePicker.TiposLimpieza.ValueYValueNullable
        '
        'cboExpedidor
        '
        Me.tblOrganizadorDatos.SetColumnSpan(Me.cboExpedidor, 4)
        Me.cboExpedidor.controlarBotonBorrar = True
        Me.cboExpedidor.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cboExpedidor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExpedidor.DropDownWidth = 131
        Me.cboExpedidor.Location = New System.Drawing.Point(113, 32)
        Me.cboExpedidor.mostrarSiempreBotonBorrar = False
        Me.cboExpedidor.Name = "cboExpedidor"
        Me.cboExpedidor.Size = New System.Drawing.Size(531, 21)
        Me.cboExpedidor.TabIndex = 12
        '
        'chkSincronizado
        '
        Me.tblOrganizadorDatos.SetColumnSpan(Me.chkSincronizado, 3)
        Me.chkSincronizado.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl
        Me.chkSincronizado.Location = New System.Drawing.Point(350, 112)
        Me.chkSincronizado.Name = "chkSincronizado"
        Me.chkSincronizado.Size = New System.Drawing.Size(94, 20)
        Me.chkSincronizado.TabIndex = 1
        Me.chkSincronizado.Text = "Sincronizado"
        Me.chkSincronizado.Values.Text = "Sincronizado"
        '
        'chkReactivarRegistro
        '
        Me.tblOrganizadorDatos.SetColumnSpan(Me.chkReactivarRegistro, 3)
        Me.chkReactivarRegistro.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl
        Me.chkReactivarRegistro.Location = New System.Drawing.Point(3, 112)
        Me.chkReactivarRegistro.Name = "chkReactivarRegistro"
        Me.chkReactivarRegistro.Size = New System.Drawing.Size(155, 20)
        Me.chkReactivarRegistro.TabIndex = 1
        Me.chkReactivarRegistro.Text = "Volver a activar Etiqueta"
        Me.chkReactivarRegistro.Values.Text = "Volver a activar Etiqueta"
        '
        'lblPeso
        '
        Me.lblPeso.Location = New System.Drawing.Point(350, 3)
        Me.lblPeso.Name = "lblPeso"
        Me.lblPeso.Size = New System.Drawing.Size(36, 20)
        Me.lblPeso.TabIndex = 0
        Me.lblPeso.Values.Text = "Peso"
        '
        'lblFecha
        '
        Me.lblFecha.Location = New System.Drawing.Point(3, 3)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(42, 20)
        Me.lblFecha.TabIndex = 0
        Me.lblFecha.Values.Text = "Fecha"
        '
        'lblDisenho
        '
        Me.lblDisenho.Location = New System.Drawing.Point(3, 85)
        Me.lblDisenho.Name = "lblDisenho"
        Me.lblDisenho.Size = New System.Drawing.Size(55, 20)
        Me.lblDisenho.TabIndex = 0
        Me.lblDisenho.Values.Text = "Etiqueta"
        '
        'lblTitulo
        '
        Me.lblTitulo.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.Custom1
        Me.lblTitulo.Location = New System.Drawing.Point(5, 9)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(93, 34)
        Me.lblTitulo.TabIndex = 0
        Me.lblTitulo.Values.Image = Global.Escritorio.My.Resources.Resources.Etiqueta_32
        Me.lblTitulo.Values.Text = "Etiquetas"
        '
        'grpCabecera
        '
        Me.grpCabecera.Dock = System.Windows.Forms.DockStyle.Top
        Me.grpCabecera.GroupBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.PanelCustom1
        Me.grpCabecera.Location = New System.Drawing.Point(0, 0)
        Me.grpCabecera.Name = "grpCabecera"
        '
        'grpCabecera.Panel
        '
        Me.grpCabecera.Panel.Controls.Add(Me.lblTitulo)
        Me.grpCabecera.Size = New System.Drawing.Size(881, 57)
        Me.grpCabecera.StateCommon.Border.Color1 = System.Drawing.Color.DarkOrange
        Me.grpCabecera.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom
        Me.grpCabecera.StateCommon.Border.Width = 5
        Me.grpCabecera.TabIndex = 0
        '
        'epErrores
        '
        Me.epErrores.Alineacion = System.Windows.Forms.ErrorIconAlignment.TopLeft
        Me.epErrores.ContainerControl = Me
        '
        'frmGestionEtiquetas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(881, 571)
        Me.Controls.Add(Me.splSeparador)
        Me.Controls.Add(Me.grpCabecera)
        Me.Name = "frmGestionEtiquetas"
        Me.Text = "Gestión de Etiquetas"
        CType(Me.grpResultados.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpResultados.Panel.ResumeLayout(False)
        CType(Me.grpResultados, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpResultados.ResumeLayout(False)
        CType(Me.dgvResultadosBuscador, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.splSeparador.Panel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.splSeparador.Panel1.ResumeLayout(False)
        Me.splSeparador.Panel1.PerformLayout()
        CType(Me.splSeparador.Panel2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.splSeparador.Panel2.ResumeLayout(False)
        CType(Me.splSeparador, System.ComponentModel.ISupportInitialize).EndInit()
        Me.splSeparador.ResumeLayout(False)
        CType(Me.hdrBusqueda.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrBusqueda.Panel.ResumeLayout(False)
        CType(Me.hdrBusqueda, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrBusqueda.ResumeLayout(False)
        Me.tblOrganizadorBusqueda.ResumeLayout(False)
        Me.tblOrganizadorBusqueda.PerformLayout()
        CType(Me.cboBSubZona, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboBZonaFAO, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tabGeneral, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabGeneral.ResumeLayout(False)
        CType(Me.tbpDatos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpDatos.ResumeLayout(False)
        Me.tbpDatos.PerformLayout()
        Me.tblOrganizadorDatos.ResumeLayout(False)
        Me.tblOrganizadorDatos.PerformLayout()
        CType(Me.cboDisenho, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.hdrLineaEntrada.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrLineaEntrada.Panel.ResumeLayout(False)
        CType(Me.hdrLineaEntrada, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrLineaEntrada.ResumeLayout(False)
        Me.tblOrganizadorDatosEntrada.ResumeLayout(False)
        Me.tblOrganizadorDatosEntrada.PerformLayout()
        CType(Me.picQR, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboExpedidor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grpCabecera.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpCabecera.Panel.ResumeLayout(False)
        Me.grpCabecera.Panel.PerformLayout()
        CType(Me.grpCabecera, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpCabecera.ResumeLayout(False)
        CType(Me.epErrores, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents splSeparador As ComponentFactory.Krypton.Toolkit.KryptonSplitContainer
    Friend WithEvents hdrBusqueda As ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup
    Friend WithEvents btnOcultarBusqueda As ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup
    Friend WithEvents lblTitulo As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents grpCabecera As ComponentFactory.Krypton.Toolkit.KryptonGroup
    Friend WithEvents epErrores As Quadralia.Controles.Validacion.GestorErrores
    Friend WithEvents lblBcodigo As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtBcodigo As Quadralia.Controles.aTextBox
    Friend WithEvents lblBFechaInicio As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents dtpBFechaInicio As Quadralia.Controles.aDateTimePicker
    Friend WithEvents lblobservaciones As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblFormaPago_id As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblCliente_id As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblBProveedor As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents btnBBuscar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents grpResultados As ComponentFactory.Krypton.Toolkit.KryptonGroupBox
    Friend WithEvents dgvResultadosBuscador As ComponentFactory.Krypton.Toolkit.KryptonDataGridView
    Friend WithEvents tblOrganizadorBusqueda As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblBFechaFin As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents dtpBFechaFin As Quadralia.Controles.aDateTimePicker
    Friend WithEvents tabGeneral As ComponentFactory.Krypton.Navigator.KryptonNavigator
    Friend WithEvents tbpDatos As ComponentFactory.Krypton.Navigator.KryptonPage
    Friend WithEvents tblOrganizadorDatos As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtBProveedor As Escritorio.cTextboxBuscador
    Friend WithEvents chkBRegistrosEliminados As ComponentFactory.Krypton.Toolkit.KryptonCheckBox
    Friend WithEvents lblBEspecie As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblBZonaFAO As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtBEspecie As Escritorio.cTextboxBuscador
    Friend WithEvents cboBZonaFAO As Escritorio.Quadralia.Controles.aComboBox
    Friend WithEvents btnLimpiarBusqueda As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents colResultadoCodigo As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents colResultadoFecha As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents Proveedor As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents txtBLoteSalida As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents lblBLoteSalida As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents cboBSubZona As Escritorio.Quadralia.Controles.aComboBox
    Friend WithEvents lblBSubZona As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblFecha As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblPeso As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblExpedidor As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents chkSincronizado As ComponentFactory.Krypton.Toolkit.KryptonCheckBox
    Friend WithEvents lblLineaEntrada As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents hdrLineaEntrada As ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup
    Friend WithEvents tblOrganizadorDatosEntrada As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents t As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblProveedor As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblEspecie As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblLote As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblZona As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblSubzona As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblEmbarcacion As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblPresentacion As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblProduccion As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtLoteEntrada As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents txtProveedor As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents txtEmbarcacion As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents txtEspecie As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents txtZona As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents txtPresentacion As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents txtSubzona As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents txtProduccion As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents txtCodigo As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents txtPeso As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents txtLineaEntrada As Escritorio.cBuscadorLineaEntrada
    Friend WithEvents dtpFecha As Escritorio.Quadralia.Controles.aDateTimePicker
    Friend WithEvents cboExpedidor As Escritorio.Quadralia.Controles.aComboBox
    Friend WithEvents chkReactivarRegistro As ComponentFactory.Krypton.Toolkit.KryptonCheckBox
    Private WithEvents picQR As System.Windows.Forms.PictureBox
    Friend WithEvents lblDisenho As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents cboDisenho As Escritorio.Quadralia.Controles.aComboBox
    Friend WithEvents KryptonLabel1 As KryptonLabel
    Friend WithEvents txtLoteSalida As Quadralia.Controles.aTextBox
End Class
