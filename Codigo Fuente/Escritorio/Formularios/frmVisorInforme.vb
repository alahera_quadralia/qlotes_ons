Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.IO
Imports System.Linq
Imports System.Text
Imports System.Data.Common
Imports System.Text.RegularExpressions

Public Class frmVisorInforme
    Inherits FormularioHijo

#Region " DECLARACIONES "
    Private _TipoInforme As Informe = Informe.Entrada
    Private _dsDatos As DataSet
    Private _Contexto As Entidades = Nothing

    Private _MostrarLogos As Boolean = False
    Private _CambioControlado As Boolean = False
#End Region
#Region " CONTROL DE BOTONES "
    Private Sub ControlarBotones()
        ControlarRibbon()
    End Sub

    ''' <summary>
    ''' Controla el cambio de botones del formulario principal
    ''' </summary>
    Private Sub ControlarRibbon()
        If Me.ParentForm Is Nothing Then Exit Sub
        If Not TypeOf Me.ParentForm Is frmPrincipal Then Exit Sub

        With DirectCast(Me.ParentForm, frmPrincipal)
            _CambioControlado = True
            .btnInformesAccionesVerOcultarLogos.Visible = True
            .btnInformesAccionesVerOcultarLogos.Checked = _MostrarLogos
            _CambioControlado = False
        End With
    End Sub
#End Region
#Region " ENUMERADOS "
    Public Enum Informe
        Entrada
        Salida
        ListadoSalida
        EtiquetasTrazabilidad
        Empaquetado
        EmpaquetadoMakro
    End Enum
#End Region
#Region " PROPIEDADES "
    Public Property Reporte As ReportDocument

    Public Property Datos As IList = Nothing

    Private ReadOnly Property Contexto As Entidades
        Get
            If _Contexto Is Nothing Then _Contexto = cDAL.Instancia.getContext
            Return _Contexto
        End Get
    End Property

    Public Property TipoInforme As Informe
        Get
            Return _TipoInforme
        End Get
        Set(ByVal value As Informe)
            _TipoInforme = value

            rptVisor.EnableDrillDown = False

            Me.Text &= "Informe"

            ' Tipo de informe
            Select Case value
                Case Informe.Entrada
                    _Reporte = New rptEntrada
                    Me.Text &= " - Albaran Entrada"
                Case Informe.Salida
                    If Quadralia.Aplicacion.GestorInformes.Informes.ContainsKey("LotesSalida") AndAlso Quadralia.Aplicacion.GestorInformes.Informes("LotesSalida").Count > 0 Then
                        _Reporte = Quadralia.Aplicacion.GestorInformes.Informes("LotesSalida").Values(0).Documento
                    Else
                        _Reporte = Nothing
                    End If

                    Me.Text &= " - Env�os"
                Case Informe.ListadoSalida
                    _Reporte = New rptListadoSalida
                    Me.Text &= " - Listado Env�os"
                Case Informe.EtiquetasTrazabilidad
                    If Quadralia.Aplicacion.GestorInformes.Informes.ContainsKey("EtiquetasTrazabilidad") AndAlso Quadralia.Aplicacion.GestorInformes.Informes("EtiquetasTrazabilidad").Count > 0 Then
                        _Reporte = Quadralia.Aplicacion.GestorInformes.Informes("EtiquetasTrazabilidad").Values(0).Documento
                    Else
                        _Reporte = Nothing
                    End If

                    Me.Text &= " - Etiqueta Trazabilidad"
                Case Informe.Empaquetado
                    _Reporte = New rptEmpaquetado
                    Me.Text &= " - Empaquetado"
                Case Informe.EmpaquetadoMakro
                    _Reporte = Nothing
                    Me.Text &= " - Empaquetado"

                Case Else
                    _Reporte = Nothing
            End Select

            lblTitulo.Text = Me.Text
        End Set
    End Property

    Public Overrides ReadOnly Property Tab As ComponentFactory.Krypton.Ribbon.KryptonRibbonTab
        Get
            Return frmPrincipal.tabInforme
        End Get
    End Property

    Public Overrides Property Estado As Quadralia.Enumerados.EstadoFormulario
        Get
            Return MyBase.Estado
        End Get
        Set(ByVal value As Quadralia.Enumerados.EstadoFormulario)
            MyBase.Estado = value
            ControlarBotones()
        End Set
    End Property
#End Region
#Region " CONSTRUCTORES "
    Public Sub New()
        InitializeComponent()
    End Sub
#End Region
#Region " RUTINA DE INICIO / FIN "
    ''' <summary>
    ''' Rutina de inicio del formulario
    ''' </summary>
    Private Sub Inicio(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ControlarBotones()
    End Sub
#End Region
#Region " REFRESCAR DATOS "


    Public Function EAN13(ByVal chaine As String) As String
        Dim i As Integer
        Dim first As Integer
        Dim checksum As Integer = 0
        Dim CodeBarre As String = ""
        Dim tableA As Boolean
        If Regex.IsMatch(chaine, "^\d{12}$") Then
            For i = 1 To 12 - 1 Step 2
                checksum += Convert.ToInt32(chaine.Substring(i, 1))
            Next

            checksum = checksum * 3
            For i = 0 To 12 - 1 Step 2
                checksum += Convert.ToInt32(chaine.Substring(i, 1))
            Next

            chaine = chaine & ((10 - checksum Mod 10) Mod 10).ToString()
            CodeBarre = chaine.Substring(0, 1) + Convert.ToChar((65 + Convert.ToInt32(chaine.Substring(1, 1))))
            first = Convert.ToInt32(chaine.Substring(0, 1))
            For i = 2 To 6
                tableA = False
                Select Case i
                    Case 2
                        If first >= 0 AndAlso first <= 3 Then tableA = True
                    Case 3
                        If first = 0 OrElse first = 4 OrElse first = 7 OrElse first = 8 Then tableA = True
                    Case 4
                        If first = 0 OrElse first = 1 OrElse first = 4 OrElse first = 5 OrElse first = 9 Then tableA = True
                    Case 5
                        If first = 0 OrElse first = 2 OrElse first = 5 OrElse first = 6 OrElse first = 7 Then tableA = True
                    Case 6
                        If first = 0 OrElse first = 3 OrElse first = 6 OrElse first = 8 OrElse first = 9 Then tableA = True
                End Select

                If tableA Then CodeBarre += Convert.ToChar((65 + Convert.ToInt32(chaine.Substring(i, 1)))) Else CodeBarre += Convert.ToChar((75 + Convert.ToInt32(chaine.Substring(i, 1))))
            Next

            CodeBarre += "*"

            For i = 7 To 12
                CodeBarre += Convert.ToChar((97 + Convert.ToInt32(chaine.Substring(i, 1))))
            Next

            CodeBarre += "+"
        End If

        Return CodeBarre
    End Function


    ''' <summary>
    ''' Se genera un dataset a trav�s de una consulta y se asigna al informe cargado en el visor    
    ''' </summary>
    ''' <param name="pCodBarrasVertical"></param>
    Public Sub RefrescarDatos(Optional ByVal pCodBarrasVertical As Boolean = False, Optional ByVal pNombreInforme As String = "")
        _dsDatos = New DataSet
        Try
            Select Case TipoInforme
                Case Informe.EmpaquetadoMakro
                    If Datos Is Nothing OrElse Datos.Count = 0 Then Exit Select

                    _dsDatos = New dsEmpaquetadoMakro
                    ' Traigo los datos
                    For Each Empaquetado As Empaquetado In Datos
                        Dim Fila As dsEmpaquetadoMakro.dtEmpaquetadoMakroRow = DirectCast(_dsDatos, dsEmpaquetadoMakro).dtEmpaquetadoMakro.NewdtEmpaquetadoMakroRow

                        With Fila
                            .Id = Empaquetado.id
                            .Texto1 = Empaquetado.Etiquetas(0).Numero_Lote
                            .Texto2 = Empaquetado.Etiquetas(0).fecha.ToShortDateString()
                            .Texto3 = Empaquetado.Etiquetas(0).fecha.AddMonths(18).ToShortDateString()
                            .Texto4 = Empaquetado.Etiquetas.Count()
                            .Texto5 = "Pulpo entero eviscerado " + Environment.NewLine +
                                      "ultracongelado" + Environment.NewLine +
                                      "(Octopus vulgaris)"
                            .Texto6 = "Pulponor S. Coop. Galega" + Environment.NewLine + Environment.NewLine +
                                      "Insuela, 17" + Environment.NewLine +
                                      "Esteiro - Muros" + Environment.NewLine +
                                      "15240 A Coru�a, Espa�a" + Environment.NewLine + Environment.NewLine +
                                      "N.R.S: 40047431/C"
                            Select Case Empaquetado.Etiquetas(0).cantidad
                                Case Is < 1.5
                                    .Texto8 = "2100104"
                                    .Texto7 = "P"
                                Case Is < 2
                                    .Texto8 = "2100118"
                                    .Texto7 = "M"
                                Case Is < 3
                                    .Texto8 = "2102184"
                                    .Texto7 = "G"
                                Case Else
                                    .Texto8 = "2102185"
                                    .Texto7 = "E"
                            End Select

                            Dim miPesoDecimal As Decimal = Empaquetado.Peso - Math.Truncate(Empaquetado.Peso)
                            Dim miPeso As String = Math.Truncate(Empaquetado.Peso).ToString().PadLeft(2, Convert.ToChar("0"))
                            miPeso += miPesoDecimal.ToString().Substring(2).PadRight(3, Convert.ToString("0"))
                            .Texto9 = EAN13(.Texto8 + miPeso)

                            DirectCast(_dsDatos, dsEmpaquetadoMakro).dtEmpaquetadoMakro.AdddtEmpaquetadoMakroRow(Fila)
                        End With
                    Next
                Case Informe.Empaquetado
                    If Datos Is Nothing OrElse Datos.Count = 0 Then Exit Select

                    _dsDatos = New dsEmpaquetado
                    ' Traigo los datos
                    For Each Empaquetado As Empaquetado In Datos
                        Dim Fila As dsEmpaquetado.dtDatosRow = DirectCast(_dsDatos, dsEmpaquetado).dtDatos.NewdtDatosRow

                        With Fila
                            .Id = Empaquetado.id
                            .CodigoEtiqueta = Empaquetado.Codigo
                            .Fecha = Empaquetado.fecha
                            .Paquetes = Empaquetado.Etiquetas.Count
                            .URL = Empaquetado.ObtenerURI(cConfiguracionPrograma.Instancia)
                            .CodigoConsulta = Empaquetado.ObtenerCodigo(cConfiguracionPrograma.Instancia)
                            .Peso = Empaquetado.Peso

                            .Lotes = String.Join(vbCrLf, Empaquetado.Etiquetas.Select(Function(x) x.LoteSalida).Take(10))
                            If Empaquetado.Etiquetas.Count > 10 Then
                                .Lotes2 = String.Join(vbCrLf, Empaquetado.Etiquetas.Select(Function(x) x.LoteSalida).Skip(10).Take(10))
                            End If
                            Dim Aux As New QRCoder.QRCodeGenerator()
                            .QR = Quadralia.Imagenes.Imagen2Byte(Aux.CreateQrCode(.CodigoEtiqueta, QRCoder.QRCodeGenerator.ECCLevel.M).GetGraphic(50))

                            DirectCast(_dsDatos, dsEmpaquetado).dtDatos.AdddtDatosRow(Fila)
                        End With

                    Next
                Case Informe.Entrada
                    If Datos Is Nothing OrElse Datos.Count = 0 Then Exit Select

                    _dsDatos = New dsAlbaranes

                    ' Traigo los datos
                    For Each Alb As LoteEntrada In Datos
                        Dim Fila As dsAlbaranes.dtDatosRow = DirectCast(_dsDatos, dsAlbaranes).dtDatos.NewdtDatosRow

                        With Fila
                            .Numero = Alb.codigo
                            .ProCli = Alb.razonSocialProveedor
                            .Fecha = Alb.fecha
                            .Observaciones = Alb.observaciones
                            .id = Alb.id

                            For Each l As LoteEntradaLinea In Alb.Lineas
                                Dim Linea As dsAlbaranes.dtLineasRow = DirectCast(_dsDatos, dsAlbaranes).dtLineas.NewdtLineasRow

                                With Linea
                                    .id = l.id
                                    .idAlb = Alb.id
                                    .Uds = l.cantidad
                                    .Especie = l.nombreEspecie
                                    .FAO = l.nombreFAO
                                    .SubZona = l.nombreSubZona
                                    .Presentacion = l.nombrePresentacion
                                    .Produccion = l.nombreProduccion
                                    .Lote = l.numeroLote
                                    If l.fechaDesembarco.HasValue Then .Desembarco = l.fechaDesembarco.Value
                                    .NombreProveedor = l.nombreProveedor
                                End With

                                DirectCast(_dsDatos, dsAlbaranes).dtLineas.AdddtLineasRow(Linea)
                            Next

                            DirectCast(_dsDatos, dsAlbaranes).dtDatos.AdddtDatosRow(Fila)
                        End With

                    Next
                Case Informe.Salida
                    If Datos Is Nothing OrElse Datos.Count = 0 Then Exit Select

                    _dsDatos = New dsLoteSalida

                    ' Traigo los datos
                    For Each Salida As LoteSalida In Datos
                        If Salida.fechaBaja.HasValue Then Continue For

                        Dim Fila As dsLoteSalida.dtDatosRow = DirectCast(_dsDatos, dsLoteSalida).dtDatos.NewdtDatosRow
                        With Fila
                            .id = Salida.id
                            .Codigo = Salida.codigo
                            .Cliente = Salida.nombreCliente
                            .Fecha = Salida.fecha
                            .Expedidor = Salida.nombreExpedidorYDireccion
                            If Salida.logotipoExpedidor IsNot Nothing Then .Logotipo = Salida.logotipoExpedidor
                            .Observaciones = Salida.observaciones
                            .DireccionCli = Salida.DireccionCliente

                            DirectCast(_dsDatos, dsLoteSalida).dtDatos.AdddtDatosRow(Fila)

                            ' Meto los empaquetados
                            For Each Empaquetado As Empaquetado In Salida.Empaquetados
                                If Empaquetado.fechaBaja.HasValue Then Continue For
                                Dim LineaEmpaquetado As dsLoteSalida.dtEmpaquetadoRow = DirectCast(_dsDatos, dsLoteSalida).dtEmpaquetado.NewdtEmpaquetadoRow

                                With LineaEmpaquetado
                                    .id = Empaquetado.id
                                    .idLoteSalida = Salida.id
                                    .Codigo = Empaquetado.Codigo
                                    .Fecha = Empaquetado.fecha
                                End With

                                DirectCast(_dsDatos, dsLoteSalida).dtEmpaquetado.AdddtEmpaquetadoRow(LineaEmpaquetado)

                                ' Meto las etiquetas
                                For Each Etiqueta As Etiqueta In Empaquetado.Etiquetas
                                    If Etiqueta.fechaBaja.HasValue Then Continue For
                                    Dim LineaEtiqueta As dsLoteSalida.dtEtiquetaRow = DirectCast(_dsDatos, dsLoteSalida).dtEtiqueta.NewdtEtiquetaRow

                                    With LineaEtiqueta
                                        .Id = Etiqueta.id
                                        .idEmpaquetado = Empaquetado.id
                                        .Peso = Etiqueta.cantidad
                                        .Lote = Etiqueta.CodigoCompletoLineaAlbaranEntrada
                                        .FechaEnvasado = Etiqueta.fecha
                                        .NombreProveedor = Etiqueta.NombreProveedor
                                        .NombreBarco = Etiqueta.NombreProveedor
                                        .denominacionCientifica = Etiqueta.NombreCientificoEspecie
                                        .MetodoPresentacion = Etiqueta.NombrePresentacion
                                        .MetodoProduccion = Etiqueta.NombreProduccion
                                        .Zona = Etiqueta.NombreZona
                                        .Subzona = Etiqueta.NombreSubzona
                                        .NombreExpedidor = Etiqueta.NombreExpedidor
                                        .CodigoAlbaranEntrada = Etiqueta.CodigoAlbaranEntrada
                                        .denominacionComercial = Etiqueta.NombreComercialEspecie
                                        .Numero_Lote = Etiqueta.LoteSalida
                                        .CodigoEspecie = Etiqueta.CodigoEspecie
                                        .CodigoEtiqueta = Etiqueta.ObtenerCodigo(cConfiguracionPrograma.Instancia)
                                    End With

                                    DirectCast(_dsDatos, dsLoteSalida).dtEtiqueta.AdddtEtiquetaRow(LineaEtiqueta)
                                Next
                            Next


                        End With

                    Next
                Case Informe.ListadoSalida
                    If Datos Is Nothing OrElse Datos.Count = 0 Then Exit Select

                    _dsDatos = New dsListadoSalida


                    ' Traigo los datos
                    For Each Alb As LoteEntrada In Datos
                        Dim Fila As dsListadoSalida.dtEntradaRow = DirectCast(_dsDatos, dsListadoSalida).dtEntrada.NewdtEntradaRow

                        With Fila
                            .id = Alb.id
                            .Numero = Alb.codigo
                            .ProCli = Alb.nombreProveedor
                            .Fecha = Alb.fecha
                            .Observaciones = Alb.observaciones

                            For Each sal As LoteSalida In Alb.LotesSalida
                                Dim Linea As dsListadoSalida.dtSalidaRow = DirectCast(_dsDatos, dsListadoSalida).dtSalida.NewdtSalidaRow

                                With Linea
                                    .id = sal.id
                                    .idSalida = Alb.id
                                    .Numero = sal.codigo
                                    .ProCli = sal.nombreCliente
                                    .Fecha = sal.fecha
                                    .Expedidor = sal.nombreExpedidor
                                    .Observaciones = sal.observaciones

                                    For Each emp As Empaquetado In sal.Empaquetados
                                        Dim empaquetado As dsListadoSalida.dtEmpaquetadoRow = DirectCast(_dsDatos, dsListadoSalida).dtEmpaquetado.NewdtEmpaquetadoRow
                                        With empaquetado
                                            .id = emp.id
                                            .idEmpaquetado = sal.id
                                            .Codigo = emp.Codigo
                                            .Fecha = emp.fecha
                                            .NumEtiquetas = emp.NumeroEtiquetas

                                            For Each etq As Etiqueta In emp.Etiquetas
                                                Dim etiqueta As dsListadoSalida.dtEtiquetaRow = DirectCast(_dsDatos, dsListadoSalida).dtEtiqueta.NewdtEtiquetaRow
                                                With etiqueta
                                                    .id = etq.id
                                                    .idEtiqueta = emp.id
                                                    .Codigo = etq.Codigo
                                                    .LoteSalida = etq.LoteSalida
                                                End With
                                                DirectCast(_dsDatos, dsListadoSalida).dtEtiqueta.AdddtEtiquetaRow(etiqueta)
                                            Next
                                        End With
                                        DirectCast(_dsDatos, dsListadoSalida).dtEmpaquetado.AdddtEmpaquetadoRow(empaquetado)
                                    Next
                                End With

                                DirectCast(_dsDatos, dsListadoSalida).dtSalida.AdddtSalidaRow(Linea)
                            Next

                            DirectCast(_dsDatos, dsListadoSalida).dtEntrada.AdddtEntradaRow(Fila)
                        End With

                    Next

                Case Informe.EtiquetasTrazabilidad
                    If Datos Is Nothing OrElse Datos.Count = 0 Then Exit Select

                    _dsDatos = New dsEtiquetaTrazabilidad

                    Dim qrString = String.Empty
                    Dim splitname = _Reporte.FileName.Split("\")
                    ' Traigo los datos
                    For Each Etq As Etiqueta In Datos
                        Dim Fila As dsEtiquetaTrazabilidad.dtDatosRow = DirectCast(_dsDatos, dsEtiquetaTrazabilidad).dtDatos.NewdtDatosRow()


                        With Fila
                            .Circulo = String.Format("ES{0}{1}{0}C.E.", Environment.NewLine, Etq.RegistroOCifExpedidor)
                            .Id = Etq.id
                            .Peso = Etq.cantidad
                            .Lote = Etq.LoteSalida
                            .FechaEnvasado = Etq.fecha
                            .NombreProveedor = Etq.NombreProveedor
                            .NombreBarco = Etq.NombreProveedor
                            .denominacionCientifica = Etq.NombreCientificoEspecie
                            .MetodoPresentacion = Etq.NombrePresentacion
                            .MetodoProduccion = Etq.NombreProduccion
                            .Zona = Etq.NombreZona
                            .Subzona = Etq.NombreSubzona
                            .NombreExpedidor = Etq.NombreDireccionExpedidor
                            .CodigoAlbaranEntrada = Etq.CodigoAlbaranEntrada
                            .denominacionComercial = Etq.NombreComercialEspecie
                            .CodigoEspecie = Etq.CodigoEspecie
                            .URL = Etq.ObtenerURI(cConfiguracionPrograma.Instancia)
                            .UrlEmpresa = cConfiguracionPrograma.Instancia.TrazamareFTPServidor
                            .CodigoEtiqueta = Etq.ObtenerCodigo(cConfiguracionPrograma.Instancia)
                            If Etq.logotipoExpedidor IsNot Nothing Then .Logotipo = Etq.logotipoExpedidor

                            If (pNombreInforme.ToUpper() = "MAKRO") Then
                                Dim codigoEan13 As String = String.Empty
                                Select Case .Peso
                                    Case Is < 1.5
                                        codigoEan13 = "2100104"
                                        .LetraIdentificadora = "P"
                                    Case Is < 2
                                        codigoEan13 = "2100118"
                                        .LetraIdentificadora = "M"
                                    Case Is < 3
                                        codigoEan13 = "2102184"
                                        .LetraIdentificadora = "G"
                                    Case Else
                                        codigoEan13 = "2102185"
                                        .LetraIdentificadora = "E"
                                End Select

                                codigoEan13 += (.Peso * 1000).ToString().Replace(".", "").PadLeft(5, "0")
                                .Ean13 = EAN13(codigoEan13)
                            End If

                            Select Case .Peso
                                Case Is < 1.5
                                    If (pCodBarrasVertical) Then
                                        .CodBarras = Convert.FromBase64String("iVBORw0KGgoAAAANSUhEUgAAAJkAAADmCAMAAADx9tecAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAMAUExURQAAABERESIiIjMzM0RERFVVVWZmZnd3d4iIiJmZmaqqqru7u8zMzN3d3e7u7v///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIqlLPkAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAHdElNRQfgARQIHTdxScDxAAAAPHRFWHRTb3VyY2UAaHR0cDovL3d3dy50ZXJyeWJ1cnRvbi5jby51ay9iYXJjb2Rld3JpdGVyL2dlbmVyYXRvci9/WnBxAAAAGHRFWHRTb2Z0d2FyZQBwYWludC5uZXQgNC4wLjlsM35OAAAE90lEQVR4Xu2cwZarIAyGg6KoqLz/294kxJlO60J6F/yLfOdMpbr5DiCEQIcKFvQjBGZGzFW0KwiwZiJ2qaGZ1T8t1wsGWl9XpSGZmdN1kQ8QYM2uZrQClJkVagnH7KfKrIhkZgUGyuylyuoXoDqzq4JUZx+4WTtu1o6btcNjGhbmBW1mVzy8nz3GvLyffQOY2ZHzaUWrw86oyjqF+m1MB5LZPlhZSTiteQQaUpa6KnmbiWYYs5nmq4Mx7LmjmAV6EeM+RwnFjIIVKpnNIChlIO1jFwst9qgzpSQaX9S2QIc96kwpJw8aU8rClkYZNuxRZ7iazmhlISxQc8CeIlcWhTiv8p7WJ71Rszdub0IAZuaxxmNYxGONZjzWaMdjjVY81miHq8ljjUbU7I3bmxCAmXms8RgW8VijGY812vFYoxWPNdrhavJYoxE1e+P2JgRgZh5rPIZFPNZoxmONdjzWaMVjjXa4mjzWaETN3ri9CQGKWUp/RloGxYxoyFY0cMwC/a0264KdEY9zomE1K8EedUY8dOSPv02K05r8cfI0PuhgJnfqpTs2ph0yE0yrRtt6oz9mxjMBB7RccxOeGbfpIhWHaMacG8caVu7NXzNBGrU/yGafoLTmJ4hmdUFg1dkZ9Ulz4rlpkyzCtAOZ1VXcmvVCYYdpTTaKKQUaJRbaOeyAMZuIl0tll1SLMCDlNfQSqYZBC1BeI+plMjPJa2ihPzbarpHfSrnimEXtZz9E2lDMNiJLOTLHTOFEMSuSL0u1KAPaBjQH5BRt6cRDG78H9qgz1eiivp4wrfkBsBka5gVtZlc8vJ89xry8n30DmJnvCT+GRXxPuJmbPWEIbveEIeBe5XvCjdzuCVu5Nzd7wvakO597wvYAgPc94XoXEDd7zm7hRu1xvRGTdZLkgaZpSTJC+qA7rMSzOJut/GVgOaA87UqBo7Iz6MR+IOVpR002rpZ65K9brc3ecK/SGT2RpYPgYo1kadqyIWVDa2taci9xrFFL3dkoiNrALyXDAS1MrCGjxpjywq9oXnUEgTErydZ0QgBaOzHnOmskNE6LxxrfAWd2crytBe1w3eH+XweyU0+46OSphe6Ih4pJuD3ySxoOmNasZomCTJxH5Jkdyyxc8ybQnrCa7TTWb1B7wiKSr/AMKa+hZuePmdQZBGImab2Z6ma1hLj2qDNixgyjDBfc36KsAyCQkVZTB1x10sl07QRBbcJy5FXS2hnrXNALukT3WKMBz2s8g5U+8xoQiNJHXgMCz2u082tm8ZnkNSC4zWtA4HmN7/jIa0Cgau95DQjU7A2c1nwH0axudFp1dkZ9/Kx7I37WvR0/696OjbZ+1r0BP+veivj4Wff/B9DMNi2sOjujKhf21R51RlUuwMys8AuM2cvCqQLzBpwT0aJH9jhSk0+gd3MJNgdc/Uw/IdhHijJz4pnJP2QIG6QZRxySc4Q0kxOivKrTor2jnVGVikRDWqhPeqMqxnGdPdBPRIDN0DAvaDO74uH97DHm5f3sG9ysHTdrx83aATTLWTOiOGY5xZmVeClA+n8KYczqHoqsg4McqsJZCbPRnGYKgxwOPWe4PZSVLMUNlNu2qXywn74i7aHU86rRfID2UOx3wsF++gpUZ7Nu0vEyWPNBJ9Be3c6DWBxpjJSOso1A57bLKiNs2PeafATaE+YWXJOc09jlzMZ8lPIPWTwG/Zct9dYAAAAASUVORK5CYII=")
                                    Else
                                        .CodBarras = Convert.FromBase64String("iVBORw0KGgoAAAANSUhEUgAAAOYAAACZCAMAAAAMwLadAAAJJGlDQ1BpY2MAAHjalZVnUJNZF8fv8zzphUASQodQQ5EqJYCUEFoo0quoQOidUEVsiLgCK4qINEUQUUDBVSmyVkSxsCgoYkE3yCKgrBtXERWUF/Sd0Xnf2Q/7n7n3/OY/Z+4995wPFwCCOFgSvLQnJqULvJ3smIFBwUzwg8L4aSkcT0838I96Pwyg5XhvBfj3IkREpvGX4sLSyuWnCNIBgLKXWDMrPWWZDy8xPTz+K59dZsFSgUt8Y5mjv/Ho15xvLPqa4+vNXXoVCgAcKfoHDv+B/3vvslQ4gvTYqMhspk9yVHpWmCCSmbbcCR6Xy/QUJEfFJkT+UPC/Sv4HpUdmpy9HbnLKBkFsdEw68/8ONTIwNATfZ/HW62uPIUb//85nWd+95HoA2LMAIHu+e+GVAHTuAED68XdPbamvlHwAOu7wMwSZ3zzU8oYGBEABdCADFIEq0AS6wAiYAUtgCxyAC/AAviAIrAN8EAMSgQBkgVywDRSAIrAH7AdVoBY0gCbQCk6DTnAeXAHXwW1wFwyDJ0AIJsArIALvwTwEQViIDNEgGUgJUod0ICOIDVlDDpAb5A0FQaFQNJQEZUC50HaoCCqFqqA6qAn6BToHXYFuQoPQI2gMmob+hj7BCEyC6bACrAHrw2yYA7vCvvBaOBpOhXPgfHg3XAHXwyfgDvgKfBsehoXwK3gWAQgRYSDKiC7CRriIBxKMRCECZDNSiJQj9Ugr0o30IfcQITKDfERhUDQUE6WLskQ5o/xQfFQqajOqGFWFOo7qQPWi7qHGUCLUFzQZLY/WQVugeehAdDQ6C12ALkc3otvR19DD6An0ewwGw8CwMGYYZ0wQJg6zEVOMOYhpw1zGDGLGMbNYLFYGq4O1wnpgw7Dp2AJsJfYE9hJ2CDuB/YAj4pRwRjhHXDAuCZeHK8c14y7ihnCTuHm8OF4db4H3wEfgN+BL8A34bvwd/AR+niBBYBGsCL6EOMI2QgWhlXCNMEp4SyQSVYjmRC9iLHErsYJ4iniDOEb8SKKStElcUggpg7SbdIx0mfSI9JZMJmuQbcnB5HTybnIT+Sr5GfmDGE1MT4wnFiG2RaxarENsSOw1BU9Rp3Ao6yg5lHLKGcodyow4XlxDnCseJr5ZvFr8nPiI+KwETcJQwkMiUaJYolnipsQUFUvVoDpQI6j51CPUq9RxGkJTpXFpfNp2WgPtGm2CjqGz6Dx6HL2IfpI+QBdJUiWNJf0lsyWrJS9IChkIQ4PBYyQwShinGQ8Yn6QUpDhSkVK7pFqlhqTmpOWkbaUjpQul26SHpT/JMGUcZOJl9sp0yjyVRclqy3rJZskekr0mOyNHl7OU48sVyp2WeywPy2vLe8tvlD8i3y8/q6Co4KSQolCpcFVhRpGhaKsYp1imeFFxWommZK0Uq1SmdEnpJVOSyWEmMCuYvUyRsryys3KGcp3ygPK8CkvFTyVPpU3lqSpBla0apVqm2qMqUlNSc1fLVWtRe6yOV2erx6gfUO9Tn9NgaQRo7NTo1JhiSbN4rBxWC2tUk6xpo5mqWa95XwujxdaK1zqodVcb1jbRjtGu1r6jA+uY6sTqHNQZXIFeYb4iaUX9ihFdki5HN1O3RXdMj6Hnppen16n3Wl9NP1h/r36f/hcDE4MEgwaDJ4ZUQxfDPMNuw7+NtI34RtVG91eSVzqu3LKya+UbYx3jSONDxg9NaCbuJjtNekw+m5qZCkxbTafN1MxCzWrMRth0tie7mH3DHG1uZ77F/Lz5RwtTi3SL0xZ/Wepaxls2W06tYq2KXNWwatxKxSrMqs5KaM20DrU+bC20UbYJs6m3eW6rahth22g7ydHixHFOcF7bGdgJ7Nrt5rgW3E3cy/aIvZN9of2AA9XBz6HK4ZmjimO0Y4ujyMnEaaPTZWe0s6vzXucRngKPz2viiVzMXDa59LqSXH1cq1yfu2m7Cdy63WF3F/d97qOr1Vcnre70AB48j30eTz1Znqmev3phvDy9qr1eeBt653r3+dB81vs0+7z3tfMt8X3ip+mX4dfjT/EP8W/ynwuwDygNEAbqB24KvB0kGxQb1BWMDfYPbgyeXeOwZv+aiRCTkIKQB2tZa7PX3lwnuy5h3YX1lPVh68+EokMDQptDF8I8wurDZsN54TXhIj6Xf4D/KsI2oixiOtIqsjRyMsoqqjRqKtoqel/0dIxNTHnMTCw3tir2TZxzXG3cXLxH/LH4xYSAhLZEXGJo4rkkalJ8Um+yYnJ28mCKTkpBijDVInV/qkjgKmhMg9LWpnWl05c+xf4MzYwdGWOZ1pnVmR+y/LPOZEtkJ2X3b9DesGvDZI5jztGNqI38jT25yrnbcsc2cTbVbYY2h2/u2aK6JX/LxFanrce3EbbFb/stzyCvNO/d9oDt3fkK+Vvzx3c47WgpECsQFIzstNxZ+xPqp9ifBnat3FW560thROGtIoOi8qKFYn7xrZ8Nf674eXF31O6BEtOSQ3swe5L2PNhrs/d4qURpTun4Pvd9HWXMssKyd/vX779Zblxee4BwIOOAsMKtoqtSrXJP5UJVTNVwtV11W418za6auYMRB4cO2R5qrVWoLar9dDj28MM6p7qOeo368iOYI5lHXjT4N/QdZR9tapRtLGr8fCzpmPC49/HeJrOmpmb55pIWuCWjZfpEyIm7J+1PdrXqtta1MdqKToFTGade/hL6y4PTrqd7zrDPtJ5VP1vTTmsv7IA6NnSIOmM6hV1BXYPnXM71dFt2t/+q9+ux88rnqy9IXii5SLiYf3HxUs6l2cspl2euRF8Z71nf8+Rq4NX7vV69A9dcr9247nj9ah+n79INqxvnb1rcPHeLfavztuntjn6T/vbfTH5rHzAd6Lhjdqfrrvnd7sFVgxeHbIau3LO/d/0+7/7t4dXDgw/8HjwcCRkRPox4OPUo4dGbx5mP559sHUWPFj4Vf1r+TP5Z/e9av7cJTYUXxuzH+p/7PH8yzh9/9UfaHwsT+S/IL8onlSabpoymzk87Tt99ueblxKuUV/MzBX9K/FnzWvP12b9s/+oXBYom3gjeLP5d/Fbm7bF3xu96Zj1nn71PfD8/V/hB5sPxj+yPfZ8CPk3OZy1gFyo+a33u/uL6ZXQxcXHxPy6ikLxyKdSVAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAwUExURf///wAAAMzMzGZmZhEREVVVVbu7u+7u7qqqqkRERDMzM5mZmXd3dyIiIt3d3YiIiAgs9wcAAAABYktHRACIBR1IAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AEUCB03cUnA8QAAA/1JREFUeNrtl+1y4yAMRQETMAbb7/+2K/NhENgt7ezO7I/LtI0VIUVHEiIVYlxS1t8kt1KrT6s8FR3fxT30vvt9ZXf19hOp9fvtAiYwgQlMYAITmMAEJjCBCUxgAhOYwAQmMIEJTGACE5jABCYwgQlMYAITmMAEJjCBCUxgAhOYwAQmMIEJTGACE5jABCYwgQlMYAITmMAEJjCBCUxgAhOYwAQmMIEJTGACE5jABCYwgQlMYAITmMAEJjCBCUxgAhOYwAQmMIEJTGACE5jABCYwgQlMYAITmMAEJjCBCUxgAhOYwAQmMIEJTGAC87eY/2L9R77/o1D+om+1aCk/5nW71eV5dVLquvMSN5+eQ+ngMOqEDbvUy9H5br3JukJnMS99ienJ9Ua/5xvmpzxfKK7uXChKStDyhMl0dieJfhTzzbx1mK3FvNSdeUZJtVqsEKeU5hnzzIdcmBQSyUcKU5OF0TLWzMmmHQbddgi7yL313Xm7o9ltZzEvsbjLbLqLuef8h0dMpcv+kHe4lHCdwIL8XC9bGy3XqURCefeNb+6tdo7pLealfoK3oEE69tph2k2e+bkULMRe9HKL0hFCsWlS1+pyItLr7Zt7y2tNzMxiXurPZgt65pCW+nHtdkq6yc8+HE1VF1aFg3rCGpUlriv1MlI3vrk31rLcYl4aR1DlpAYLsQBSVWWdhlRjw01V6sirGj4Eb/PHbDR1pA5WDLot1+14usKzt5JTP1rMSw+TthZUbVK7Pc6MAdNqfTBM/6Hh6NOmc798aJ/alMamo4G92UF3D7frHLFQqrccqysbG4t56fFCKaDKxaf9CdPJVTDMa+9msrkOxtMba6xDHOk0W5dB14RieCjVmyhN0GOaH0gv92bkpK6hU2JPHUPiK42HFlMZs5Tg082VZvlh0kmjqtpeJ4foR29RyjP/15gvK2KWQ+zjkWaL5ortMBN8Cj7Z2eZQ1w9vdbqGYscgwg233P3LLealN0iZW5z5vpfpv4KJEryoh1/szC6+zXUu18s8NFTxlh5KpNxiXnqFbJp5+wazeoqZ+ZTcXyjWFNuYLKa7r5c1X11dXCXPZ74CRW8xLz1Slqe1JFOJx1UiKvGoKJ/5nj3yaVRFGnT+vpkDi6D1Fgtzz1xuMS+9lTJuiHeAWO4z8obpcvCfmHa6b1WS3P237OE6m6bbwb+9cm8xzbeaW8xLr5C0Q8v9NKd7H1UFk16dN3QjpjFK/2GE1W9Jonm9nVFnBx0VV5/Ga57wzhu1XZNmbjEvdZStPN6bL5hi1Wlnbu7QSip+H5DueNBlaeFuB2+fRskt5qWWs39DhRDGS/Nh2ZV21hN8nK2d+UJ3Seobb3xxi3npZf0BLH1PUQw4UPwAAAAldEVYdGRhdGU6Y3JlYXRlADIwMTYtMDEtMjBUMDg6Mjk6NTUrMDA6MDDv9HV1AAAAJXRFWHRkYXRlOm1vZGlmeQAyMDE2LTAxLTIwVDA4OjI5OjU1KzAwOjAwnqnNyQAAAC10RVh0aWNjOmNvcHlyaWdodABDb3B5cmlnaHQgQXJ0aWZleCBTb2Z0d2FyZSAyMDExCLrFtAAAADF0RVh0aWNjOmRlc2NyaXB0aW9uAEFydGlmZXggU29mdHdhcmUgc1JHQiBJQ0MgUHJvZmlsZRMMAYYAAAAydEVYdGljYzptYW51ZmFjdHVyZXIAQXJ0aWZleCBTb2Z0d2FyZSBzUkdCIElDQyBQcm9maWxlXH49nwAAACt0RVh0aWNjOm1vZGVsAEFydGlmZXggU29mdHdhcmUgc1JHQiBJQ0MgUHJvZmlsZTEogqEAAAA/dEVYdFNvZnR3YXJlAEJhcmNvZGUgV3JpdGVyIGluIFB1cmUgUG9zdFNjcmlwdCAtIFZlcnNpb24gMjAxNS0xMS0yNKIlrvAAAAA8dEVYdFNvdXJjZQBodHRwOi8vd3d3LnRlcnJ5YnVydG9uLmNvLnVrL2JhcmNvZGV3cml0ZXIvZ2VuZXJhdG9yL39acHEAAAAASUVORK5CYII=")
                                    End If

                                    .LetraIdentificadora = "P"
                                Case Is < 2
                                    If (pCodBarrasVertical) Then
                                        .CodBarras = Convert.FromBase64String("iVBORw0KGgoAAAANSUhEUgAAAJkAAADmCAMAAADx9tecAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAMAUExURQAAABERESIiIjMzM0RERFVVVWZmZnd3d4iIiJmZmaqqqru7u8zMzN3d3e7u7v///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIqlLPkAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAHdElNRQfgARQIHjGzBzYHAAAAPHRFWHRTb3VyY2UAaHR0cDovL3d3dy50ZXJyeWJ1cnRvbi5jby51ay9iYXJjb2Rld3JpdGVyL2dlbmVyYXRvci9/WnBxAAAAGHRFWHRTb2Z0d2FyZQBwYWludC5uZXQgNC4wLjlsM35OAAAFKElEQVR4Xu2dy7LjKgxFhY2N8Yv//9uWhJyTxBmE9IA90Kq6MQmTVYBBCE5fKljQQwjMjJiraE8QYM1E7FJDM6v/abk+MND2uhoNycycrod8gABrdnWjFaDMrFBLOGaPJrMikpkVGCizpyarX4DazJ4KUpvdcLN23KwdN2uH5zQszAvazJ54+Dj7GvPycfYLKGYpnVa6QDEjGjYrGjhmgV6bzcZdZ8TjnGjIZiVYVWfEo5Q1UPzrUpze5I8z8XDL1qdQZqUckVtwyof8oj/0x8xK2Wfp3mHCM+M+XaThEM2Yc02gZoJ0an+Qze6g9OYdYDMITOYFq+qMybxgVZ0xmResqjOlbO8AzbRvwJjpQh6fAJo1lrfhhmPGHWqFSu3U3qhKhDXLSR8XVtUZk3nBqjpjMi98/BECMLNj266NujVnZ1QlT6F+G5Pu6iBgkX2wssKvKUpvHoGGtElblW3llWqGMZtpfsoEseeOYhboJUWVKaGYUbBCZWMzCEoZSMfYxUIceiBQSqLxSW0NdFhVZ0o5edKYksbZaxpl2rCqznAznZIAuggL1Bqwp8iNRSHOmnasNb1Rszc+/ggBmJnHGl/DIh5rNOOxRjsea7TisUY73EweazSiZm98/BECFDO/r9GO39doRTz8vkYrNqf5fY0GHmbcp35f4ztezQTp1P4gm91B6c07iGZ1Q2DN2Rn1SXPitWmVLMK0A5nVXVze9EFhh+lNNoopBRolFto57IAxm4i3S2WXVIswIOU19BGphkELUF4j6mMyM8lraKE/NtvmyG+lPHHMoo6zB5FWFLOVyFKOzDFTOFHMiuTL7MKGTGgr0BqwpWhbJ57a+D2wqs5Uo4v6esL05g1gMzTMC9rMnnj4OPsa8/Jx9gtgZn4m/DUs4mfCzXw4E4bg45kwBDyq/Ey4kY9nwlbuzYczYavpzv1M2CoAeD8Trr8C4mbfs1u4UUdcb8QkT5I80DQtSUZIK7rDSryKs1nmLwPLAeVpMwWOys6gC/uBlKcdNdmYLfXIX9famr3hUaUreiJLB8HFGsnStGVFyobW3rTkXuJYo5a6s1IQtYFfSoYDWphYQ2aNMW0Lv6Jb1hkExqwk29MJAWjvxJx51khonBaPNX4DzuzkeFsLOuC6w+O/TmSn3nDRxVML3REPFZNwe+SXNBwwvVnNEgVZOI/IKzuWWbjWTaAzYTXbaazfoM6ERWS7wjOkvIaanQ8zaTMIxEzSejPVw2oJca2qM2LGDKNMFzzeouwDIJCZVlMH3HQyyHTvBEHtwnJsWdLaG9a9oCd0i+6xRgOe1/gOVrrnNSAQpVteAwLPa7TzZ2bxmeQ1IPiY14DA8xq/cctrQKBq73kNCNTsDZzefAfRrB50WnN2Rn38rnsjfte9Hb/r3o7Ntn7XvQG/696K+Phd9/8H0MwOLaw5O6MqF/bVqjqjKhdgZlb4A8bsaeNUgXkDzolo0St7HKnJJ9C7uQRbA65xpp8Q7CNFWTnxzOQfZAgrpBlHHJJzhDSTG6K8q9OivaOdUZWKRENaqDW9URXjuO4e6CciwGZomBe0mT3x8HH2Nebl4+wX3KwdN2vHzdoBNNs2zYjimG0pzqzEWwHSf6cQxqyeocg+OMilKpydMBvNaaYwyOXQc4Y7Q8lkKW6g3LYt5YP96SvSGUq9r3r9X4GAzlDs74SD/ekrUJvNekjH22DNB51AZ3U7T2JxpDFSOso6At3bLllm2LDvNfkIdCbMPZiT3NPY5c7GfJTyD/J4Kl6KWpCsAAAAAElFTkSuQmCC")
                                    Else
                                        .CodBarras = Convert.FromBase64String("iVBORw0KGgoAAAANSUhEUgAAAOYAAACZCAMAAAAMwLadAAAJJGlDQ1BpY2MAAHjalZVnUJNZF8fv8zzphUASQodQQ5EqJYCUEFoo0quoQOidUEVsiLgCK4qINEUQUUDBVSmyVkSxsCgoYkE3yCKgrBtXERWUF/Sd0Xnf2Q/7n7n3/OY/Z+4995wPFwCCOFgSvLQnJqULvJ3smIFBwUzwg8L4aSkcT0838I96Pwyg5XhvBfj3IkREpvGX4sLSyuWnCNIBgLKXWDMrPWWZDy8xPTz+K59dZsFSgUt8Y5mjv/Ho15xvLPqa4+vNXXoVCgAcKfoHDv+B/3vvslQ4gvTYqMhspk9yVHpWmCCSmbbcCR6Xy/QUJEfFJkT+UPC/Sv4HpUdmpy9HbnLKBkFsdEw68/8ONTIwNATfZ/HW62uPIUb//85nWd+95HoA2LMAIHu+e+GVAHTuAED68XdPbamvlHwAOu7wMwSZ3zzU8oYGBEABdCADFIEq0AS6wAiYAUtgCxyAC/AAviAIrAN8EAMSgQBkgVywDRSAIrAH7AdVoBY0gCbQCk6DTnAeXAHXwW1wFwyDJ0AIJsArIALvwTwEQViIDNEgGUgJUod0ICOIDVlDDpAb5A0FQaFQNJQEZUC50HaoCCqFqqA6qAn6BToHXYFuQoPQI2gMmob+hj7BCEyC6bACrAHrw2yYA7vCvvBaOBpOhXPgfHg3XAHXwyfgDvgKfBsehoXwK3gWAQgRYSDKiC7CRriIBxKMRCECZDNSiJQj9Ugr0o30IfcQITKDfERhUDQUE6WLskQ5o/xQfFQqajOqGFWFOo7qQPWi7qHGUCLUFzQZLY/WQVugeehAdDQ6C12ALkc3otvR19DD6An0ewwGw8CwMGYYZ0wQJg6zEVOMOYhpw1zGDGLGMbNYLFYGq4O1wnpgw7Dp2AJsJfYE9hJ2CDuB/YAj4pRwRjhHXDAuCZeHK8c14y7ihnCTuHm8OF4db4H3wEfgN+BL8A34bvwd/AR+niBBYBGsCL6EOMI2QgWhlXCNMEp4SyQSVYjmRC9iLHErsYJ4iniDOEb8SKKStElcUggpg7SbdIx0mfSI9JZMJmuQbcnB5HTybnIT+Sr5GfmDGE1MT4wnFiG2RaxarENsSOw1BU9Rp3Ao6yg5lHLKGcodyow4XlxDnCseJr5ZvFr8nPiI+KwETcJQwkMiUaJYolnipsQUFUvVoDpQI6j51CPUq9RxGkJTpXFpfNp2WgPtGm2CjqGz6Dx6HL2IfpI+QBdJUiWNJf0lsyWrJS9IChkIQ4PBYyQwShinGQ8Yn6QUpDhSkVK7pFqlhqTmpOWkbaUjpQul26SHpT/JMGUcZOJl9sp0yjyVRclqy3rJZskekr0mOyNHl7OU48sVyp2WeywPy2vLe8tvlD8i3y8/q6Co4KSQolCpcFVhRpGhaKsYp1imeFFxWommZK0Uq1SmdEnpJVOSyWEmMCuYvUyRsryys3KGcp3ygPK8CkvFTyVPpU3lqSpBla0apVqm2qMqUlNSc1fLVWtRe6yOV2erx6gfUO9Tn9NgaQRo7NTo1JhiSbN4rBxWC2tUk6xpo5mqWa95XwujxdaK1zqodVcb1jbRjtGu1r6jA+uY6sTqHNQZXIFeYb4iaUX9ihFdki5HN1O3RXdMj6Hnppen16n3Wl9NP1h/r36f/hcDE4MEgwaDJ4ZUQxfDPMNuw7+NtI34RtVG91eSVzqu3LKya+UbYx3jSONDxg9NaCbuJjtNekw+m5qZCkxbTafN1MxCzWrMRth0tie7mH3DHG1uZ77F/Lz5RwtTi3SL0xZ/Wepaxls2W06tYq2KXNWwatxKxSrMqs5KaM20DrU+bC20UbYJs6m3eW6rahth22g7ydHixHFOcF7bGdgJ7Nrt5rgW3E3cy/aIvZN9of2AA9XBz6HK4ZmjimO0Y4ujyMnEaaPTZWe0s6vzXucRngKPz2viiVzMXDa59LqSXH1cq1yfu2m7Cdy63WF3F/d97qOr1Vcnre70AB48j30eTz1Znqmev3phvDy9qr1eeBt653r3+dB81vs0+7z3tfMt8X3ip+mX4dfjT/EP8W/ynwuwDygNEAbqB24KvB0kGxQb1BWMDfYPbgyeXeOwZv+aiRCTkIKQB2tZa7PX3lwnuy5h3YX1lPVh68+EokMDQptDF8I8wurDZsN54TXhIj6Xf4D/KsI2oixiOtIqsjRyMsoqqjRqKtoqel/0dIxNTHnMTCw3tir2TZxzXG3cXLxH/LH4xYSAhLZEXGJo4rkkalJ8Um+yYnJ28mCKTkpBijDVInV/qkjgKmhMg9LWpnWl05c+xf4MzYwdGWOZ1pnVmR+y/LPOZEtkJ2X3b9DesGvDZI5jztGNqI38jT25yrnbcsc2cTbVbYY2h2/u2aK6JX/LxFanrce3EbbFb/stzyCvNO/d9oDt3fkK+Vvzx3c47WgpECsQFIzstNxZ+xPqp9ifBnat3FW560thROGtIoOi8qKFYn7xrZ8Nf674eXF31O6BEtOSQ3swe5L2PNhrs/d4qURpTun4Pvd9HWXMssKyd/vX779Zblxee4BwIOOAsMKtoqtSrXJP5UJVTNVwtV11W418za6auYMRB4cO2R5qrVWoLar9dDj28MM6p7qOeo368iOYI5lHXjT4N/QdZR9tapRtLGr8fCzpmPC49/HeJrOmpmb55pIWuCWjZfpEyIm7J+1PdrXqtta1MdqKToFTGade/hL6y4PTrqd7zrDPtJ5VP1vTTmsv7IA6NnSIOmM6hV1BXYPnXM71dFt2t/+q9+ux88rnqy9IXii5SLiYf3HxUs6l2cspl2euRF8Z71nf8+Rq4NX7vV69A9dcr9247nj9ah+n79INqxvnb1rcPHeLfavztuntjn6T/vbfTH5rHzAd6Lhjdqfrrvnd7sFVgxeHbIau3LO/d/0+7/7t4dXDgw/8HjwcCRkRPox4OPUo4dGbx5mP559sHUWPFj4Vf1r+TP5Z/e9av7cJTYUXxuzH+p/7PH8yzh9/9UfaHwsT+S/IL8onlSabpoymzk87Tt99ueblxKuUV/MzBX9K/FnzWvP12b9s/+oXBYom3gjeLP5d/Fbm7bF3xu96Zj1nn71PfD8/V/hB5sPxj+yPfZ8CPk3OZy1gFyo+a33u/uL6ZXQxcXHxPy6ikLxyKdSVAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAwUExURf///wAAAMzMzGZmZhEREVVVVbu7u+7u7qqqqkRERDMzM5mZmXd3dyIiIt3d3YiIiAgs9wcAAAABYktHRACIBR1IAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AEUCB4xswc2BwAABDtJREFUeNrtluly3SAMhdkueMH2+79tZRYjAU7STjvTH4dJcq2Io6tPgLBS49C6/WabW9yfR32qPjlLRuhj9/Pq7BZttMacuF/9dAATmMAEJjCBCUxgAhOYwAQmMIEJTGACE5jABCYwgQlMYAITmMAEJjCBCUxgAhOYwAQmMIEJTGACE5jABCYwgQlMYAITmMAEJjCBCUxgAhOYwAQmMIEJTGACE5jABCYwgQlMYAITmMAEJjCBCUxgAhOYwAQmMIEJTGACE5jABCYwgQlMYAITmMAEJjCBCUxgAhOYwAQmMIEJTGACE5jABCYwgfmnmP9i/Eex/6NU/mJsY53WH/86Pbj6vKxauzbzNrc9P8e6g+PoUyEe2tmzi82j6TZipxisO2Xt1aj7EnOnKRv9Xm+Yn/p8o6xtpqUsqUB2hil84SCLfoyILaJ1mFzRWzn895iSl9bKBqUurZ9VktOvcuiVzymRfeY0HSm802nNVs22w+DbThWsPnjsLtqTzRE6RW8pmSzTibxrr3oW8ygFilNM4+r8WGasueAuf1XUn/tj49lKn8kktCY7iy2jtZ3je0Vnqf3eAj1m0vUdnINGvYrPDjNs+irPdcFi2ou73pJ1xlg1rHTcVwqRP5/YMloZS2YWis4qx0NiZl1/NjnoVVKy7ev4dCq6L897PNmqWrEKJ+2J4Ovhkb66Xl47FltGY1uvV3SWqLjUjS2ocdIGi2kBtGnO1g1pjb2Umrwj72/aY9xDSWGjrqNdDGrwbSWnc3w1eKLVmu6jorNmmEU36bRtQc2m3XqknjFgBudOgbl/6GSUmNdxx3D5wNxtc6WGvYXBp1tbPGUqLVrhWOtEpuisCWbVTS+UCmrSkdbHDHPVixKY99zNF7mL/u4HS6pnavfUW+3gY2l6mUqLJpMXis6aYFbr5d5MnLRr6JSEy6WU5MjtgWMa721NPt9juc+fPheaVjX0vkmaQ7RkPTfG72E23XwkzHrA93bA22ZIJ7s7mwSfk8+6wA51S4z7XEszjEnEJ0n77F+pmOgFpmX7fg6p2YZ3fa1vPvkKpmryqjUGdQhd+rf0rWW9/GRD1Wj5oVFwxUTPMcO0ehKSbebtG8z2Lakyn1rDGyX4qk3FEr7nelnK1dXlXOt8letR9YqJnmMy3ZSyPi21KGY+t2ZU8zHJvso9e5bTaKo1+PbnZo4iAx4tpf7sPamY6Dnm+rpn+euBzXeAsq8H2T9vQTn5Tyof3bcmW+vzt86RvpC72ynfXmW0VObHLRUTPcMMMuwcktROH5e/1qENDpj0ue6ebsTcRumlKy77li3q19uVfGHw0eK6y+9OLGYfjbYkK7NUjHqGubwsT/fqPrk3XzDV4vLMsrkjt0x6H9DrOfEVy8qwQzR+xKRi0DPM+HY0h35nYoyL+sEIC81sJ/i8uM5/4bst8000OaRiqv+d8QvnNkqL0ZIBeAAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxNi0wMS0yMFQwODozMDo0OSswMDowMNll65UAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTYtMDEtMjBUMDg6MzA6NDkrMDA6MDCoOFMpAAAALXRFWHRpY2M6Y29weXJpZ2h0AENvcHlyaWdodCBBcnRpZmV4IFNvZnR3YXJlIDIwMTEIusW0AAAAMXRFWHRpY2M6ZGVzY3JpcHRpb24AQXJ0aWZleCBTb2Z0d2FyZSBzUkdCIElDQyBQcm9maWxlEwwBhgAAADJ0RVh0aWNjOm1hbnVmYWN0dXJlcgBBcnRpZmV4IFNvZnR3YXJlIHNSR0IgSUNDIFByb2ZpbGVcfj2fAAAAK3RFWHRpY2M6bW9kZWwAQXJ0aWZleCBTb2Z0d2FyZSBzUkdCIElDQyBQcm9maWxlMSiCoQAAAD90RVh0U29mdHdhcmUAQmFyY29kZSBXcml0ZXIgaW4gUHVyZSBQb3N0U2NyaXB0IC0gVmVyc2lvbiAyMDE1LTExLTI0oiWu8AAAADx0RVh0U291cmNlAGh0dHA6Ly93d3cudGVycnlidXJ0b24uY28udWsvYmFyY29kZXdyaXRlci9nZW5lcmF0b3Ivf1pwcQAAAABJRU5ErkJggg==")
                                    End If

                                    .LetraIdentificadora = "M"
                                Case Is < 3
                                    If (pCodBarrasVertical) Then
                                        .CodBarras = Convert.FromBase64String("iVBORw0KGgoAAAANSUhEUgAAAJkAAADmCAMAAADx9tecAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAMAUExURQAAABERESIiIjMzM0RERFVVVWZmZnd3d4iIiJmZmaqqqru7u8zMzN3d3e7u7v///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIqlLPkAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAHdElNRQfgARQIHwmCHr/YAAAAPHRFWHRTb3VyY2UAaHR0cDovL3d3dy50ZXJyeWJ1cnRvbi5jby51ay9iYXJjb2Rld3JpdGVyL2dlbmVyYXRvci9/WnBxAAAAGHRFWHRTb2Z0d2FyZQBwYWludC5uZXQgNC4wLjlsM35OAAAFcElEQVR4Xu2dy5bqIBBFi4QE8+T///bWK2qrA/EOOIPaa7VBmewFhEdBOlSxoLsQmBkxV9KvIMCaidilhmZmf5q2CwZaXlehIZm503WRDxBgza5q9ASUmScshWN2LzJPIpl5goEyeyoy+wJUZn5VkMrsjTBrJ8zaCbN2uE/Dwr2gzfyKR7Szr3GvaGe/AGhWil4AzbxGgc0g+OABY5Y8dQfG7JyIbptCJJ8wZrXeEvlNidXOmH2kfKiSfvWszqhKPQul9WGmnyisieYT0qyemQbA2lTK9dVyeqMqzsE9hgBWm08AmZ1eWIL0tJ7sDncZRMn6WmlkOGajtbjx1G9iBgGPTZS4LrenEcqzOiNFpo1sT3YFMqMkQlJ2WS5IZqOK1TpooYmZfe9OImv6daWBU0BmM82emiQFVJs70VB4CsQdW6K8ApnVRVYCYlZ3nm0gmdXjlvW2lOFAZkKe1RkzemZHGtFfADPjyZn3Hki1uUy+HB6LLaEQYBG7Iy94XEepzSNxf7ZJWdVtnYl7WxQzHgOuBsaw545idh83jYUKitk1C3I2NoNAJj/axi5udPOszsgCeHxSWxMdntUZHiu505iKxs/WwquVgtOfndnTQrpBjQF7ybK0S3le5D61nN6o2Qsff4QAzCzmGl/DIjHXaCbmGu3EXKOVmGu0w8UUc41G1OyFjz9CgGJWyp+elkExIxoeezsKjlmiv8XmTbAz4nFONCxuJXhWZ8RDe/78qFKc2uQP2a8btDOTX+zSHe/TDhkJpkVn2/pDf9yMRwKe0HLJTXhmXKc3KThEM+Zcea7h6d78NROkUvuDbPYOSm2+g2hmCwIvzs6oT5kLj02rRBGmHcjMVnHLphdKOHt1bJRLSTTKXGjnaQeM2US8XJI9a9tOH5DiGnrJdpCE6xYnrmE71ZObSVxDE/3x3nbJfFfKFccsazu7k2lFMVuJPOTIHDOlE8VMT176uTjp0FagMWAr2ZdO3LXxfeBZnTGjC7s9YWrzDWAzNNwL2syveEQ7+xr3inb2C2BmsSf8NSwSe8LNfNgThuDjnjAE3KpiT7iRj3vCnu7Nhz1hz+nO+56wZwDwuidsvwISZt+z+3TDWlxvxGSZJHigYVqSiJBmdIeVeBRns4W/DCwHFKddKPGs7JTnhHlAR4rT2nN1i4ce+etqpdkbblU6ohfycBDcXKN4mLauSNFQq00P7hWea1iqO6s+WVoHvikZntDCzDWk1xjLduNbdFu0B4Exq+XpX2zIk8w4ZvVcZp0JjdMt5hq/AWd28nxbE9rgusPt3zqyU0+46OCpie6Ih4rJdHvkmzQdMLVpZoWSDJxH5pEdy8yf4IfaE1az/f68PNKesIhs1/QMKa6hZufdTMoMAjGTsN5MtlktU1zP6oyYMcMo3QW3tyzrAAikp9XQARedNDJdO0FgVViPbZGw9oZ1LugJXaLHXKOBiGt8Byu9xzUgEKW3uAYEEddo52Hm8zOJa0DwMa4BQcQ1fuMtrgGBqr3GNSBQsxdwavMVRDPb6PTi7Iz6xFn3RuKseztx1r0d723jrHsDcda9FfGJs+7/D6DZ9c4FCFTlwr96VmdU5QLMzBMPYMyeFk4GzB3w4Z0LntOft3cu6CcEr+9c0E8M4p0LPxHvXPgaVXHinQu/48WJg3tBm/kVj2hnX+Ne0c5+IczaCbN2wqwdQLNt04gojtlW8sxKvBQg/T+FMGa2hyLr4CSHqnBWwmw0l5nSIIdDzxluD2UhD3EDxbZ9KB/80VekPRQ7r5rdB2gPxZ8TTv7oK1CZ2fudeBms8aATaK9u504sjzRmKkddR6Bz2/Z+p7TvFnwE2hPmGlyKnNPY5czGfNT6DxuTB53MgB66AAAAAElFTkSuQmCC")
                                    Else
                                        .CodBarras = Convert.FromBase64String("iVBORw0KGgoAAAANSUhEUgAAAOYAAACZCAMAAAAMwLadAAAJJGlDQ1BpY2MAAHjalZVnUJNZF8fv8zzphUASQodQQ5EqJYCUEFoo0quoQOidUEVsiLgCK4qINEUQUUDBVSmyVkSxsCgoYkE3yCKgrBtXERWUF/Sd0Xnf2Q/7n7n3/OY/Z+4995wPFwCCOFgSvLQnJqULvJ3smIFBwUzwg8L4aSkcT0838I96Pwyg5XhvBfj3IkREpvGX4sLSyuWnCNIBgLKXWDMrPWWZDy8xPTz+K59dZsFSgUt8Y5mjv/Ho15xvLPqa4+vNXXoVCgAcKfoHDv+B/3vvslQ4gvTYqMhspk9yVHpWmCCSmbbcCR6Xy/QUJEfFJkT+UPC/Sv4HpUdmpy9HbnLKBkFsdEw68/8ONTIwNATfZ/HW62uPIUb//85nWd+95HoA2LMAIHu+e+GVAHTuAED68XdPbamvlHwAOu7wMwSZ3zzU8oYGBEABdCADFIEq0AS6wAiYAUtgCxyAC/AAviAIrAN8EAMSgQBkgVywDRSAIrAH7AdVoBY0gCbQCk6DTnAeXAHXwW1wFwyDJ0AIJsArIALvwTwEQViIDNEgGUgJUod0ICOIDVlDDpAb5A0FQaFQNJQEZUC50HaoCCqFqqA6qAn6BToHXYFuQoPQI2gMmob+hj7BCEyC6bACrAHrw2yYA7vCvvBaOBpOhXPgfHg3XAHXwyfgDvgKfBsehoXwK3gWAQgRYSDKiC7CRriIBxKMRCECZDNSiJQj9Ugr0o30IfcQITKDfERhUDQUE6WLskQ5o/xQfFQqajOqGFWFOo7qQPWi7qHGUCLUFzQZLY/WQVugeehAdDQ6C12ALkc3otvR19DD6An0ewwGw8CwMGYYZ0wQJg6zEVOMOYhpw1zGDGLGMbNYLFYGq4O1wnpgw7Dp2AJsJfYE9hJ2CDuB/YAj4pRwRjhHXDAuCZeHK8c14y7ihnCTuHm8OF4db4H3wEfgN+BL8A34bvwd/AR+niBBYBGsCL6EOMI2QgWhlXCNMEp4SyQSVYjmRC9iLHErsYJ4iniDOEb8SKKStElcUggpg7SbdIx0mfSI9JZMJmuQbcnB5HTybnIT+Sr5GfmDGE1MT4wnFiG2RaxarENsSOw1BU9Rp3Ao6yg5lHLKGcodyow4XlxDnCseJr5ZvFr8nPiI+KwETcJQwkMiUaJYolnipsQUFUvVoDpQI6j51CPUq9RxGkJTpXFpfNp2WgPtGm2CjqGz6Dx6HL2IfpI+QBdJUiWNJf0lsyWrJS9IChkIQ4PBYyQwShinGQ8Yn6QUpDhSkVK7pFqlhqTmpOWkbaUjpQul26SHpT/JMGUcZOJl9sp0yjyVRclqy3rJZskekr0mOyNHl7OU48sVyp2WeywPy2vLe8tvlD8i3y8/q6Co4KSQolCpcFVhRpGhaKsYp1imeFFxWommZK0Uq1SmdEnpJVOSyWEmMCuYvUyRsryys3KGcp3ygPK8CkvFTyVPpU3lqSpBla0apVqm2qMqUlNSc1fLVWtRe6yOV2erx6gfUO9Tn9NgaQRo7NTo1JhiSbN4rBxWC2tUk6xpo5mqWa95XwujxdaK1zqodVcb1jbRjtGu1r6jA+uY6sTqHNQZXIFeYb4iaUX9ihFdki5HN1O3RXdMj6Hnppen16n3Wl9NP1h/r36f/hcDE4MEgwaDJ4ZUQxfDPMNuw7+NtI34RtVG91eSVzqu3LKya+UbYx3jSONDxg9NaCbuJjtNekw+m5qZCkxbTafN1MxCzWrMRth0tie7mH3DHG1uZ77F/Lz5RwtTi3SL0xZ/Wepaxls2W06tYq2KXNWwatxKxSrMqs5KaM20DrU+bC20UbYJs6m3eW6rahth22g7ydHixHFOcF7bGdgJ7Nrt5rgW3E3cy/aIvZN9of2AA9XBz6HK4ZmjimO0Y4ujyMnEaaPTZWe0s6vzXucRngKPz2viiVzMXDa59LqSXH1cq1yfu2m7Cdy63WF3F/d97qOr1Vcnre70AB48j30eTz1Znqmev3phvDy9qr1eeBt653r3+dB81vs0+7z3tfMt8X3ip+mX4dfjT/EP8W/ynwuwDygNEAbqB24KvB0kGxQb1BWMDfYPbgyeXeOwZv+aiRCTkIKQB2tZa7PX3lwnuy5h3YX1lPVh68+EokMDQptDF8I8wurDZsN54TXhIj6Xf4D/KsI2oixiOtIqsjRyMsoqqjRqKtoqel/0dIxNTHnMTCw3tir2TZxzXG3cXLxH/LH4xYSAhLZEXGJo4rkkalJ8Um+yYnJ28mCKTkpBijDVInV/qkjgKmhMg9LWpnWl05c+xf4MzYwdGWOZ1pnVmR+y/LPOZEtkJ2X3b9DesGvDZI5jztGNqI38jT25yrnbcsc2cTbVbYY2h2/u2aK6JX/LxFanrce3EbbFb/stzyCvNO/d9oDt3fkK+Vvzx3c47WgpECsQFIzstNxZ+xPqp9ifBnat3FW560thROGtIoOi8qKFYn7xrZ8Nf674eXF31O6BEtOSQ3swe5L2PNhrs/d4qURpTun4Pvd9HWXMssKyd/vX779Zblxee4BwIOOAsMKtoqtSrXJP5UJVTNVwtV11W418za6auYMRB4cO2R5qrVWoLar9dDj28MM6p7qOeo368iOYI5lHXjT4N/QdZR9tapRtLGr8fCzpmPC49/HeJrOmpmb55pIWuCWjZfpEyIm7J+1PdrXqtta1MdqKToFTGade/hL6y4PTrqd7zrDPtJ5VP1vTTmsv7IA6NnSIOmM6hV1BXYPnXM71dFt2t/+q9+ux88rnqy9IXii5SLiYf3HxUs6l2cspl2euRF8Z71nf8+Rq4NX7vV69A9dcr9247nj9ah+n79INqxvnb1rcPHeLfavztuntjn6T/vbfTH5rHzAd6Lhjdqfrrvnd7sFVgxeHbIau3LO/d/0+7/7t4dXDgw/8HjwcCRkRPox4OPUo4dGbx5mP559sHUWPFj4Vf1r+TP5Z/e9av7cJTYUXxuzH+p/7PH8yzh9/9UfaHwsT+S/IL8onlSabpoymzk87Tt99ueblxKuUV/MzBX9K/FnzWvP12b9s/+oXBYom3gjeLP5d/Fbm7bF3xu96Zj1nn71PfD8/V/hB5sPxj+yPfZ8CPk3OZy1gFyo+a33u/uL6ZXQxcXHxPy6ikLxyKdSVAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAwUExURf///wAAAMzMzGZmZhEREVVVVbu7u+7u7qqqqkRERDMzM5mZmXd3dyIiIt3d3YiIiAgs9wcAAAABYktHRACIBR1IAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AEUCB8Jgh6/2AAABFZJREFUeNrtl+uS3CgMhQ2mwTbYfv+3XZmLkQSdSbZ2q/LjUJlpq4U05xMgnGUZhzH9p9jc4v4y2lPzyVkyg86t57XZPdtotd9z//K7A5jABCYwgQlMYAITmMAEJjCBCUxgAhOYwAQmMIEJTGACE5jABCYwgQlMYAITmMAEJjCBCUxgAhOYwAQmMIEJTGACE5jABCYwgQlMYAITmMAEJjCBCUxgAhOYwAQmMIEJTGACE5jABCYwgQlMYAITmMAEJjCBCUxgAhOYwAQmMIEJTGACE5jABCYwgQlMYAITmMAEJjCBCUxgAhOYwAQmMIEJzH+L+X+Mvyj3XyTlP8xtV2fMx3+dHlx73nZjXJ/5mEcsz6nt4DT6lpBO49ZL5ebZTB9JRQxW1nvcjKAq/CVmpNQH/fQ4Nf3Tnh+Uvc9cSSX9wXWGKXzhJIv+WZFbZFOYPEJbWe8DGrRCdeYFJVVipYDbmHeV5PS7HvDFF0lkX0WmowjvTF6z3bDtMPiOawmrOXlule1VcwYVoayr6KXcSSsUultfehfzrPV/w8R069r8VGfspeCugCXzeT4Orlb6bCGhNYkst8zW18XrCGXdVS+VKSiFuoNz0GR28ammh8Pc9bktWMp7MZojW1dKLYaVjvtqIcrnm1tmq2MrzCJCWW9Z2vbrCvXZ5KB3lbT2P8enU9F9fY7pYqu6ilW4qMbB22pJXxPmjWO5ZTaxZWXEYG0SsyscW1DnDGWT08G23dm7Ia2xl6G27MhnNWJKMVQJB3Ud41JYBt9R9Vzjq8GbrdU0jhHKaqNAC4WTTtsX1B7G7WfuGQNmcO4SmPFDzTGWSff55HCxbFNqm/tR+5/0vc3tOWNCSs9WOXa5UDlCWb08SSucXigN1O756Zxh5h3CMZ+5h6/hLvlIX2x5HXK7p/63Dj4m00spPdvSNoHG9Noq43b1rHGFX+7NzPmUhdo1xW3DhNIeOKb1fm3iyz1W+vzlS6Fj7n/SN5Ops2Wr9tCfMTfaK+V4jArHkTHbAY91r7NBfSVMkqQqvsQFdqi7MO5zXWYYNNRspSht/8oIHR/obWBvx3WqUEMatuGdrvUTLV/BliZ+6Y1hOUVc/lr6Wm+ciwlvL+xVkBEqnrbfe8C+KBwg2WY+fsDsKnNlPq32D0rwLTYXS/je62Wrx0kxtzrf9XpcdIS06C1hDXOFc8r2tLVi2mU6mqKmx2b7rvfsVU+jbdbgi+/NnIQCni0v2ttzZYS02GvMpGbzpczB5R14fc/ItyR7Ff/JZaf71hZrf3+3OdIXSne75NurzJbL/LplhLTOSaucYsqXPXoVPm9/70MbHJLQ5x493YiljdL/MNIWj2LRgTnu7AuDjxbX3T46ua1UNtqSrMwyQlhsm3qtUFNye7w3v9Vqc2Vm3dyJWza/D5j9mviqtcq0Q7YPc8oIZtnfxVyGr2xKadwJkxE2mtlP8HXzOP8L32PZH7LJISOm8X8y/gFzK016cCvIvQAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxNi0wMS0yMFQwODozMTowOSswMDowMLLtjlEAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTYtMDEtMjBUMDg6MzE6MDkrMDA6MDDDsDbtAAAALXRFWHRpY2M6Y29weXJpZ2h0AENvcHlyaWdodCBBcnRpZmV4IFNvZnR3YXJlIDIwMTEIusW0AAAAMXRFWHRpY2M6ZGVzY3JpcHRpb24AQXJ0aWZleCBTb2Z0d2FyZSBzUkdCIElDQyBQcm9maWxlEwwBhgAAADJ0RVh0aWNjOm1hbnVmYWN0dXJlcgBBcnRpZmV4IFNvZnR3YXJlIHNSR0IgSUNDIFByb2ZpbGVcfj2fAAAAK3RFWHRpY2M6bW9kZWwAQXJ0aWZleCBTb2Z0d2FyZSBzUkdCIElDQyBQcm9maWxlMSiCoQAAAD90RVh0U29mdHdhcmUAQmFyY29kZSBXcml0ZXIgaW4gUHVyZSBQb3N0U2NyaXB0IC0gVmVyc2lvbiAyMDE1LTExLTI0oiWu8AAAADx0RVh0U291cmNlAGh0dHA6Ly93d3cudGVycnlidXJ0b24uY28udWsvYmFyY29kZXdyaXRlci9nZW5lcmF0b3Ivf1pwcQAAAABJRU5ErkJggg==")
                                    End If

                                    .LetraIdentificadora = "G"
                                Case Else
                                    If (pCodBarrasVertical) Then
                                        .CodBarras = Convert.FromBase64String("iVBORw0KGgoAAAANSUhEUgAAAJkAAADmCAMAAADx9tecAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAMAUExURQAAABERESIiIjMzM0RERFVVVWZmZnd3d4iIiJmZmaqqqru7u8zMzN3d3e7u7v///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIqlLPkAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAHdElNRQfgARQIHxThGNMBAAAAPHRFWHRTb3VyY2UAaHR0cDovL3d3dy50ZXJyeWJ1cnRvbi5jby51ay9iYXJjb2Rld3JpdGVyL2dlbmVyYXRvci9/WnBxAAAAGHRFWHRTb2Z0d2FyZQBwYWludC5uZXQgNC4wLjlsM35OAAAFL0lEQVR4Xu2d2bLjKgxFhSfi+f//9kpiOydT3QrpB/aDVlXHOLysAgyyIKfl5ELuQmRmolxFXEmgNTOxS43NrPzzcrlw4O11NRqTGZyui32QQGt2dSMKVGYolBKP2b3JUGQyQ0GhMntosnJD1Ga4Okxt9kaY1RNm9YRZPTqncQEvajNc+Yhx9jXwinH2CzHO/h/IPIGqxkDmCVQ1BjJPoKox57m+QvMEQPEPGrNtUpvhAaJZ4/Yy3HjMtENRKJRObY2rDLRmc/bLBaoaA5knUNUYyDzx8UsKGM12/0RzNsZ98pTX81w6vR83IjOdaJV59YukjaY31WjIOUkv+Ti3UToas1Fu+rmJTH7bycZilorIIDrUlJtkFjMZ/DLCbCUyKyLzoE+lXXnMBh9ndwZZWMwWkbyWKVantknSwWJ2Zp3FEGzYhLYQrQFrHmYXU6dBnwNUNaYYXZTHk6Y33yAz29f1QBHN2RhXmcdU7vpsD2kpt0ZFNot+7uhTytKbe5IOE9q66FvxRGM2yXQNMEU9iWKNBzGqdVMSCgWLNSiwUPFaNJ2blOi7ObZs9g9qS5IdVY05z0MnjTF7Tm/JvU0bqGqMNtMxoGwkDdZQbIx34ZYHbSxJwzTbc1pqWuNmL3z8kgIys4g1vkZFItaoJmKNeiLWqCVijXq0mSLWqMTNXvj4JQUsZjk/zbQKi5lIV5JTd3jMku1RPIAh2BjzOEbpkHR0UNUY8/CZ39KggKc39ePQZbzzycy+KZfmYE7bbSUYZ4+2/Yv2wExXAjtSIt3IZ6Z9erOGYzRTjkVjDZRb82xmWKe2h9nsHZbefIfRrLwQoDkb4z5xXqOSOK9RT5zXqAezbZzXqCDOa9RiPnFe498hNmMDXtRmuPIR4+xr4BXj7BfIzGJP+GtUJPaEq/mwJ0zBxz1hCnRUxZ5wJR/3hFFuzYc9YdQ0531PGBUEvO4Jl28JCbPv2RBulBHXGjOZR0seeJpWLCPkFc1RJV3F1WzWm07liPK0sySNyo7kC/vOlKftPdk4I/Wot0tpzdboqPIVPQvSQXSxRkaa9lyYsqGlN5HcyxprlFJzFkmm1ulDqWhASxNr2KzR5/Wmj+g6+wxCY3ZmvNMZiejdSTnmySOhfrxFrPEbdGaHxtte8AHXHB3/ZSI7/ISLL55eaI55uJiF270+pGmn6c1iliXZwrkPurJzmaVr3STaE3azTfpyR7UnbCLrFZ4x5TXc7LibWZtRYGaW1pukbFZbiIuqxpiZ0vU2Xeh4G+w9gAKbaT11oE1ng8zfnSgoXXju62xp7ZXrXNAD/ooesUYFkdf4DlV6z2tQYEpveQ0KIq9Rz58Z4jPLa1DwMa9BQeQ1fuMtr0GBq73mNShwsxd4evMVRrOy0YnmbIz7xFn3SuKsez1x1r0ezLZx1r2COOtei/nEWfd/h9AMmxZozsa4ygVuUdUYV7kgM0PhDxqzhxenAs0TcIwiNz+yp5GafRI9m7eENeAaZ/5JwdbLYCsnn5n9QYa0UJppxGE5R0ozOyGqb3VexDPaGFcpWDTkhVLTGlcB+3X2wD8ZITZjA17UZrjyEePsa+AV4+wXwqyeMKsnzOohNFtXz4jymK15mFRJXwXE/04hjVnZQ7H34GSHqnjehNVoypOkzg6HHhPdHsosSHET5baxlHf46SvTHko5r3r9D4REeyj4nXDCT1+J2mzyTTp9DfZ80EG0V7fpJDb00g+S93Ppic5tn7PNsGnbSvKRaE9Ye3DOdk5jszMb036e/wFdhCjembIDfwAAAABJRU5ErkJggg==")
                                    Else
                                        .CodBarras = Convert.FromBase64String("iVBORw0KGgoAAAANSUhEUgAAAOYAAACZCAMAAAAMwLadAAAJJGlDQ1BpY2MAAHjalZVnUJNZF8fv8zzphUASQodQQ5EqJYCUEFoo0quoQOidUEVsiLgCK4qINEUQUUDBVSmyVkSxsCgoYkE3yCKgrBtXERWUF/Sd0Xnf2Q/7n7n3/OY/Z+4995wPFwCCOFgSvLQnJqULvJ3smIFBwUzwg8L4aSkcT0838I96Pwyg5XhvBfj3IkREpvGX4sLSyuWnCNIBgLKXWDMrPWWZDy8xPTz+K59dZsFSgUt8Y5mjv/Ho15xvLPqa4+vNXXoVCgAcKfoHDv+B/3vvslQ4gvTYqMhspk9yVHpWmCCSmbbcCR6Xy/QUJEfFJkT+UPC/Sv4HpUdmpy9HbnLKBkFsdEw68/8ONTIwNATfZ/HW62uPIUb//85nWd+95HoA2LMAIHu+e+GVAHTuAED68XdPbamvlHwAOu7wMwSZ3zzU8oYGBEABdCADFIEq0AS6wAiYAUtgCxyAC/AAviAIrAN8EAMSgQBkgVywDRSAIrAH7AdVoBY0gCbQCk6DTnAeXAHXwW1wFwyDJ0AIJsArIALvwTwEQViIDNEgGUgJUod0ICOIDVlDDpAb5A0FQaFQNJQEZUC50HaoCCqFqqA6qAn6BToHXYFuQoPQI2gMmob+hj7BCEyC6bACrAHrw2yYA7vCvvBaOBpOhXPgfHg3XAHXwyfgDvgKfBsehoXwK3gWAQgRYSDKiC7CRriIBxKMRCECZDNSiJQj9Ugr0o30IfcQITKDfERhUDQUE6WLskQ5o/xQfFQqajOqGFWFOo7qQPWi7qHGUCLUFzQZLY/WQVugeehAdDQ6C12ALkc3otvR19DD6An0ewwGw8CwMGYYZ0wQJg6zEVOMOYhpw1zGDGLGMbNYLFYGq4O1wnpgw7Dp2AJsJfYE9hJ2CDuB/YAj4pRwRjhHXDAuCZeHK8c14y7ihnCTuHm8OF4db4H3wEfgN+BL8A34bvwd/AR+niBBYBGsCL6EOMI2QgWhlXCNMEp4SyQSVYjmRC9iLHErsYJ4iniDOEb8SKKStElcUggpg7SbdIx0mfSI9JZMJmuQbcnB5HTybnIT+Sr5GfmDGE1MT4wnFiG2RaxarENsSOw1BU9Rp3Ao6yg5lHLKGcodyow4XlxDnCseJr5ZvFr8nPiI+KwETcJQwkMiUaJYolnipsQUFUvVoDpQI6j51CPUq9RxGkJTpXFpfNp2WgPtGm2CjqGz6Dx6HL2IfpI+QBdJUiWNJf0lsyWrJS9IChkIQ4PBYyQwShinGQ8Yn6QUpDhSkVK7pFqlhqTmpOWkbaUjpQul26SHpT/JMGUcZOJl9sp0yjyVRclqy3rJZskekr0mOyNHl7OU48sVyp2WeywPy2vLe8tvlD8i3y8/q6Co4KSQolCpcFVhRpGhaKsYp1imeFFxWommZK0Uq1SmdEnpJVOSyWEmMCuYvUyRsryys3KGcp3ygPK8CkvFTyVPpU3lqSpBla0apVqm2qMqUlNSc1fLVWtRe6yOV2erx6gfUO9Tn9NgaQRo7NTo1JhiSbN4rBxWC2tUk6xpo5mqWa95XwujxdaK1zqodVcb1jbRjtGu1r6jA+uY6sTqHNQZXIFeYb4iaUX9ihFdki5HN1O3RXdMj6Hnppen16n3Wl9NP1h/r36f/hcDE4MEgwaDJ4ZUQxfDPMNuw7+NtI34RtVG91eSVzqu3LKya+UbYx3jSONDxg9NaCbuJjtNekw+m5qZCkxbTafN1MxCzWrMRth0tie7mH3DHG1uZ77F/Lz5RwtTi3SL0xZ/Wepaxls2W06tYq2KXNWwatxKxSrMqs5KaM20DrU+bC20UbYJs6m3eW6rahth22g7ydHixHFOcF7bGdgJ7Nrt5rgW3E3cy/aIvZN9of2AA9XBz6HK4ZmjimO0Y4ujyMnEaaPTZWe0s6vzXucRngKPz2viiVzMXDa59LqSXH1cq1yfu2m7Cdy63WF3F/d97qOr1Vcnre70AB48j30eTz1Znqmev3phvDy9qr1eeBt653r3+dB81vs0+7z3tfMt8X3ip+mX4dfjT/EP8W/ynwuwDygNEAbqB24KvB0kGxQb1BWMDfYPbgyeXeOwZv+aiRCTkIKQB2tZa7PX3lwnuy5h3YX1lPVh68+EokMDQptDF8I8wurDZsN54TXhIj6Xf4D/KsI2oixiOtIqsjRyMsoqqjRqKtoqel/0dIxNTHnMTCw3tir2TZxzXG3cXLxH/LH4xYSAhLZEXGJo4rkkalJ8Um+yYnJ28mCKTkpBijDVInV/qkjgKmhMg9LWpnWl05c+xf4MzYwdGWOZ1pnVmR+y/LPOZEtkJ2X3b9DesGvDZI5jztGNqI38jT25yrnbcsc2cTbVbYY2h2/u2aK6JX/LxFanrce3EbbFb/stzyCvNO/d9oDt3fkK+Vvzx3c47WgpECsQFIzstNxZ+xPqp9ifBnat3FW560thROGtIoOi8qKFYn7xrZ8Nf674eXF31O6BEtOSQ3swe5L2PNhrs/d4qURpTun4Pvd9HWXMssKyd/vX779Zblxee4BwIOOAsMKtoqtSrXJP5UJVTNVwtV11W418za6auYMRB4cO2R5qrVWoLar9dDj28MM6p7qOeo368iOYI5lHXjT4N/QdZR9tapRtLGr8fCzpmPC49/HeJrOmpmb55pIWuCWjZfpEyIm7J+1PdrXqtta1MdqKToFTGade/hL6y4PTrqd7zrDPtJ5VP1vTTmsv7IA6NnSIOmM6hV1BXYPnXM71dFt2t/+q9+ux88rnqy9IXii5SLiYf3HxUs6l2cspl2euRF8Z71nf8+Rq4NX7vV69A9dcr9247nj9ah+n79INqxvnb1rcPHeLfavztuntjn6T/vbfTH5rHzAd6Lhjdqfrrvnd7sFVgxeHbIau3LO/d/0+7/7t4dXDgw/8HjwcCRkRPox4OPUo4dGbx5mP559sHUWPFj4Vf1r+TP5Z/e9av7cJTYUXxuzH+p/7PH8yzh9/9UfaHwsT+S/IL8onlSabpoymzk87Tt99ueblxKuUV/MzBX9K/FnzWvP12b9s/+oXBYom3gjeLP5d/Fbm7bF3xu96Zj1nn71PfD8/V/hB5sPxj+yPfZ8CPk3OZy1gFyo+a33u/uL6ZXQxcXHxPy6ikLxyKdSVAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAwUExURf///wAAAMzMzGZmZhEREVVVVbu7u+7u7qqqqkRERDMzM5mZmXd3dyIiIt3d3YiIiAgs9wcAAAABYktHRACIBR1IAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AEUCB8U4RjTAQAABCpJREFUeNrtl+uO3CAMhSFhIASSvP/b1uESsCGzVdVK/XFQtxOvOR5/GExWqXFo3X6y3Vu9P4/6VH18Fo8gY8t5dXaLJi2uawrp/3EAE5jABCYwgQlMYAITmMAEJjCBCUxgAhOYwAQmMIEJTGACE5jABCYwgQlMYAITmMAEJjCBCUxgAhOYwAQmMIEJTGACE5jABCYwgQlMYAITmMAEJjCBCUxgAhOYwAQmMIEJTGACE5jABCYwgQlMYAITmMAEJjCBCUxgAhOYwAQmMIEJTGACE5jABCYwgQlMYAITmMAEJjCBCUxgAhOYwAQmMIH5p5j/YvxHsf+jVP5i7GU1Wn/s63Rn6vPmtTZt5m3uIT/HuoPj6FMuHtqsp4jdR9NtRKEYLJEFAWg7y5uPQKF3+rneMD/1+UbxbeZK30YLtM4wmc8dZNG/hcVm0QRmr5CWyCJ92QyT81KtVqfUpfWzPnz6VY65sjk02Wf+OkMKa3Sqmdfd8g6+/VRu1UcfW0R7sjmcUAhr1D2ps7xrd3qKeZQliVPMxdT5sczwecFNDh715/7Y+2y5b8kZUU1CF5tHazvHSoWwhC7chZ1Xk4FG7dmnmO52fZXnWrCY9mLQe7LOGKumW7reVxYifz6xebQytpw7UwhL6PJhmZ/NHvQqKa3t6/rptHi2PId4dlVdWRVO2hPOLsXivlovq00Xm0djW5YrhDXR+dcW1Dhpg8VUAL00Z+uGVGPLpUvekXfsEGNwJYWduo420anBt5csztkVXqLVNQ2jQlgT3TtmV9Bl18YfqWcMmM6Yk2GGD52FkCddxx3DhLxNqf15ati7G3xPc7vPGEulRSscvk7sFMKa6Py3C6WCLukQ62OG6fWmGOY9d7dFbqK9O8CW6pDaPfXWdfB1aVqeSovG0uUKYX3RvdybiZOqT7vdXSalxEc+5j3mYu1ak8/3WO7zp80LTVV10jemOUZLVun5P2NynbjM5pj1gId0wNmgvuIEZobPyWed6w51S6z3mZamG5OID9z67EOumOub7jvm88fFKWI/w8pXMFWTV60xqIPp0q+5z5d1t5MNVaPlh0rBFXO9a9YXzPEvp/0HzPYtaWU+de1vFGerNi0W8z3Xy1auLpFzXeerXI9KKpg16r5htgul7nGx+Tgti7sk+yr37FlO41KtwReemzmyDPpoKdmnd3IFtwbdO2b/erDmO0Ctba+/YPqS/CctO923S7b883+dw30ud7eTv73yaGmZHzdXcEvqXjH5y95p9HHZy+vXyldM+vTB0o2Y2yi9ZsUt7Nmifr1fyecGHxXXXDYYVkwZjbZkt8xcwSype8MUr+6Te/MFU20mzyzxY28t6X1A+3PiK9bKww7RPp2TK5gldO/VlL9YYoyb+o3hNprZwp9Xr7NffLe1/BCND65g1lfdy/gFzsZLoF9lXrIAAAAldEVYdGRhdGU6Y3JlYXRlADIwMTYtMDEtMjBUMDg6MzE6MjArMDA6MDBlUMz/AAAAJXRFWHRkYXRlOm1vZGlmeQAyMDE2LTAxLTIwVDA4OjMxOjIwKzAwOjAwFA10QwAAAC10RVh0aWNjOmNvcHlyaWdodABDb3B5cmlnaHQgQXJ0aWZleCBTb2Z0d2FyZSAyMDExCLrFtAAAADF0RVh0aWNjOmRlc2NyaXB0aW9uAEFydGlmZXggU29mdHdhcmUgc1JHQiBJQ0MgUHJvZmlsZRMMAYYAAAAydEVYdGljYzptYW51ZmFjdHVyZXIAQXJ0aWZleCBTb2Z0d2FyZSBzUkdCIElDQyBQcm9maWxlXH49nwAAACt0RVh0aWNjOm1vZGVsAEFydGlmZXggU29mdHdhcmUgc1JHQiBJQ0MgUHJvZmlsZTEogqEAAAA/dEVYdFNvZnR3YXJlAEJhcmNvZGUgV3JpdGVyIGluIFB1cmUgUG9zdFNjcmlwdCAtIFZlcnNpb24gMjAxNS0xMS0yNKIlrvAAAAA8dEVYdFNvdXJjZQBodHRwOi8vd3d3LnRlcnJ5YnVydG9uLmNvLnVrL2JhcmNvZGV3cml0ZXIvZ2VuZXJhdG9yL39acHEAAAAASUVORK5CYII=")
                                    End If

                                    .LetraIdentificadora = "E"
                            End Select

                            ' Primer Precio
                            ' .CodBarras = Convert.FromBase64String("iVBORw0KGgoAAAANSUhEUgAAAOYAAACZCAMAAAAMwLadAAAJJGlDQ1BpY2MAAHjalZVnUJNZF8fv8zzphUASQodQQ5EqJYCUEFoo0quoQOidUEVsiLgCK4qINEUQUUDBVSmyVkSxsCgoYkE3yCKgrBtXERWUF/Sd0Xnf2Q/7n7n3/OY/Z+4995wPFwCCOFgSvLQnJqULvJ3smIFBwUzwg8L4aSkcT0838I96Pwyg5XhvBfj3IkREpvGX4sLSyuWnCNIBgLKXWDMrPWWZDy8xPTz+K59dZsFSgUt8Y5mjv/Ho15xvLPqa4+vNXXoVCgAcKfoHDv+B/3vvslQ4gvTYqMhspk9yVHpWmCCSmbbcCR6Xy/QUJEfFJkT+UPC/Sv4HpUdmpy9HbnLKBkFsdEw68/8ONTIwNATfZ/HW62uPIUb//85nWd+95HoA2LMAIHu+e+GVAHTuAED68XdPbamvlHwAOu7wMwSZ3zzU8oYGBEABdCADFIEq0AS6wAiYAUtgCxyAC/AAviAIrAN8EAMSgQBkgVywDRSAIrAH7AdVoBY0gCbQCk6DTnAeXAHXwW1wFwyDJ0AIJsArIALvwTwEQViIDNEgGUgJUod0ICOIDVlDDpAb5A0FQaFQNJQEZUC50HaoCCqFqqA6qAn6BToHXYFuQoPQI2gMmob+hj7BCEyC6bACrAHrw2yYA7vCvvBaOBpOhXPgfHg3XAHXwyfgDvgKfBsehoXwK3gWAQgRYSDKiC7CRriIBxKMRCECZDNSiJQj9Ugr0o30IfcQITKDfERhUDQUE6WLskQ5o/xQfFQqajOqGFWFOo7qQPWi7qHGUCLUFzQZLY/WQVugeehAdDQ6C12ALkc3otvR19DD6An0ewwGw8CwMGYYZ0wQJg6zEVOMOYhpw1zGDGLGMbNYLFYGq4O1wnpgw7Dp2AJsJfYE9hJ2CDuB/YAj4pRwRjhHXDAuCZeHK8c14y7ihnCTuHm8OF4db4H3wEfgN+BL8A34bvwd/AR+niBBYBGsCL6EOMI2QgWhlXCNMEp4SyQSVYjmRC9iLHErsYJ4iniDOEb8SKKStElcUggpg7SbdIx0mfSI9JZMJmuQbcnB5HTybnIT+Sr5GfmDGE1MT4wnFiG2RaxarENsSOw1BU9Rp3Ao6yg5lHLKGcodyow4XlxDnCseJr5ZvFr8nPiI+KwETcJQwkMiUaJYolnipsQUFUvVoDpQI6j51CPUq9RxGkJTpXFpfNp2WgPtGm2CjqGz6Dx6HL2IfpI+QBdJUiWNJf0lsyWrJS9IChkIQ4PBYyQwShinGQ8Yn6QUpDhSkVK7pFqlhqTmpOWkbaUjpQul26SHpT/JMGUcZOJl9sp0yjyVRclqy3rJZskekr0mOyNHl7OU48sVyp2WeywPy2vLe8tvlD8i3y8/q6Co4KSQolCpcFVhRpGhaKsYp1imeFFxWommZK0Uq1SmdEnpJVOSyWEmMCuYvUyRsryys3KGcp3ygPK8CkvFTyVPpU3lqSpBla0apVqm2qMqUlNSc1fLVWtRe6yOV2erx6gfUO9Tn9NgaQRo7NTo1JhiSbN4rBxWC2tUk6xpo5mqWa95XwujxdaK1zqodVcb1jbRjtGu1r6jA+uY6sTqHNQZXIFeYb4iaUX9ihFdki5HN1O3RXdMj6Hnppen16n3Wl9NP1h/r36f/hcDE4MEgwaDJ4ZUQxfDPMNuw7+NtI34RtVG91eSVzqu3LKya+UbYx3jSONDxg9NaCbuJjtNekw+m5qZCkxbTafN1MxCzWrMRth0tie7mH3DHG1uZ77F/Lz5RwtTi3SL0xZ/Wepaxls2W06tYq2KXNWwatxKxSrMqs5KaM20DrU+bC20UbYJs6m3eW6rahth22g7ydHixHFOcF7bGdgJ7Nrt5rgW3E3cy/aIvZN9of2AA9XBz6HK4ZmjimO0Y4ujyMnEaaPTZWe0s6vzXucRngKPz2viiVzMXDa59LqSXH1cq1yfu2m7Cdy63WF3F/d97qOr1Vcnre70AB48j30eTz1Znqmev3phvDy9qr1eeBt653r3+dB81vs0+7z3tfMt8X3ip+mX4dfjT/EP8W/ynwuwDygNEAbqB24KvB0kGxQb1BWMDfYPbgyeXeOwZv+aiRCTkIKQB2tZa7PX3lwnuy5h3YX1lPVh68+EokMDQptDF8I8wurDZsN54TXhIj6Xf4D/KsI2oixiOtIqsjRyMsoqqjRqKtoqel/0dIxNTHnMTCw3tir2TZxzXG3cXLxH/LH4xYSAhLZEXGJo4rkkalJ8Um+yYnJ28mCKTkpBijDVInV/qkjgKmhMg9LWpnWl05c+xf4MzYwdGWOZ1pnVmR+y/LPOZEtkJ2X3b9DesGvDZI5jztGNqI38jT25yrnbcsc2cTbVbYY2h2/u2aK6JX/LxFanrce3EbbFb/stzyCvNO/d9oDt3fkK+Vvzx3c47WgpECsQFIzstNxZ+xPqp9ifBnat3FW560thROGtIoOi8qKFYn7xrZ8Nf674eXF31O6BEtOSQ3swe5L2PNhrs/d4qURpTun4Pvd9HWXMssKyd/vX779Zblxee4BwIOOAsMKtoqtSrXJP5UJVTNVwtV11W418za6auYMRB4cO2R5qrVWoLar9dDj28MM6p7qOeo368iOYI5lHXjT4N/QdZR9tapRtLGr8fCzpmPC49/HeJrOmpmb55pIWuCWjZfpEyIm7J+1PdrXqtta1MdqKToFTGade/hL6y4PTrqd7zrDPtJ5VP1vTTmsv7IA6NnSIOmM6hV1BXYPnXM71dFt2t/+q9+ux88rnqy9IXii5SLiYf3HxUs6l2cspl2euRF8Z71nf8+Rq4NX7vV69A9dcr9247nj9ah+n79INqxvnb1rcPHeLfavztuntjn6T/vbfTH5rHzAd6Lhjdqfrrvnd7sFVgxeHbIau3LO/d/0+7/7t4dXDgw/8HjwcCRkRPox4OPUo4dGbx5mP559sHUWPFj4Vf1r+TP5Z/e9av7cJTYUXxuzH+p/7PH8yzh9/9UfaHwsT+S/IL8onlSabpoymzk87Tt99ueblxKuUV/MzBX9K/FnzWvP12b9s/+oXBYom3gjeLP5d/Fbm7bF3xu96Zj1nn71PfD8/V/hB5sPxj+yPfZ8CPk3OZy1gFyo+a33u/uL6ZXQxcXHxPy6ikLxyKdSVAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAwUExURf///wAAAMzMzGZmZhEREVVVVbu7u+7u7qqqqkRERDMzM5mZmXd3dyIiIt3d3YiIiAgs9wcAAAABYktHRACIBR1IAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AEUCCAWV8iYEQAABDVJREFUeNrtl+2SnSAMhkU54Afq/d9tIxBJgseddtqZ/niZ7h6zITnvQyDYYeiHc+2n2NKS/jL4iX16ls5gc9t5PLtls1b/bTK65f1xABOYwAQmMIEJTGACE5jABCYwgQlMYAITmMAEJjCBCUxgAhOYwAQmMIEJTGACE5jABCYwgQlMYAITmMAEJjCBCUxgAhOYwAQmMIEJTGACE5jABCYwgQlMYAITmMAEJjCBCUxgAhOYwAQmMIEJTGACE5jABCYwgQlMYAITmMAEJjCBCUxgAhOYwAQmMIEJTGACE5jABCYwgQlMYAITmMAEJjCB+aeY/2L8R7n/Iyl/Mfc4eec+4ev06Pl5XpzzbeZlrlt5TryDU+8bYtqdnw6TW2ZzbSQT0Vlak1D/irlR6pV+zm+YH36+UJY2cyKV9BXTE6byxZ0s+jeq3CqbwZQR1tKapHpz5hUlrcsUh+F07q6Snn7W4z6Ekozso8j0FBG8yzVbnNgOnW89hji5XeY22W41ezQR1lKalHqlm7vUXcy9rn96xBw9z091xlIW3Bew5D7XxyrVat9YSKgmm8its7UqBRthLK1JqbcdXIImt6hPgxlXd9ZnLljKe3Fza7aOlDhGLJ301YUon3duna2OuTCrCGNpTUq9PZsS9KySpvZ1cjoteuCVS4eo6qSqcNCqxsCHR/u4XsF5kVtnU1tWRxhLa1Lq+xbUOGmDpVwANzZn64a0SkGHjmVHXtXYUtpilbBS13E+xaHzrbVuR/9qcGdj/VsfYSytSal/6LStoOPq/LLnntFhRu8Phbl9qDluZdK5Xzn8VrYptc2FWt4aO9/d3K4zpqS0bJVj4YkiwlhGk1T/eKEw6Ljkp/0Jc3HzoDCvuWu9pKh2YaM/zLkOud1Tb506n5AZtJSWbeBNYDGDtYwmqf7LvZk5adfQKYmnz5L0KO1BYo4hTCy+3GOlzx+hnDSqarQ+16nvs2XrvjHeMLWmN/UKkw/4xge8Deor0WAW+CK+xEVxqJsw6fNNZuxFpBtuuvevjlCW0fSiXkA63vAy9z2CfQUbWPzQGsOwq7j8Z+1b6kqHhw3F2coDr4KOUJbR9KJeQorNvP6A2VTm3B9e+wslBo7NX6d89/Uy1+ZvmFnpydeijVBWh/lVPVPy08yLOQ6PgxWxnjHbZ71nj3oaR7Y633bfbUkpkNly0e6eqyOe4pumF/Xy9WAqd8AgXxmfMZcq/pOXnW6ssVjL/ZvnaF8s/eHQb686WxZ6u3XEUzxrelGvX/YO7/YznIt73twiJX0uW6AbsbRR+h9Gmre1WNTx1jP7Yuej4vozbF4V02ajLSmE6oiHeNb0Xb15dX+4N79gDrMvM+v2SNIa8/uAW44HX7UmnbbL9hFOHdHHP96bhtP+YUwpfb92xIgzzWxn4DhlXHjxXdb4QzY9dMRj/O+o/wUKJUxHX3qIlAAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxNi0wMS0yMFQwODozMjoyMiswMDowMBn4ZtUAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTYtMDEtMjBUMDg6MzI6MjIrMDA6MDBopd5pAAAALXRFWHRpY2M6Y29weXJpZ2h0AENvcHlyaWdodCBBcnRpZmV4IFNvZnR3YXJlIDIwMTEIusW0AAAAMXRFWHRpY2M6ZGVzY3JpcHRpb24AQXJ0aWZleCBTb2Z0d2FyZSBzUkdCIElDQyBQcm9maWxlEwwBhgAAADJ0RVh0aWNjOm1hbnVmYWN0dXJlcgBBcnRpZmV4IFNvZnR3YXJlIHNSR0IgSUNDIFByb2ZpbGVcfj2fAAAAK3RFWHRpY2M6bW9kZWwAQXJ0aWZleCBTb2Z0d2FyZSBzUkdCIElDQyBQcm9maWxlMSiCoQAAAD90RVh0U29mdHdhcmUAQmFyY29kZSBXcml0ZXIgaW4gUHVyZSBQb3N0U2NyaXB0IC0gVmVyc2lvbiAyMDE1LTExLTI0oiWu8AAAADx0RVh0U291cmNlAGh0dHA6Ly93d3cudGVycnlidXJ0b24uY28udWsvYmFyY29kZXdyaXRlci9nZW5lcmF0b3Ivf1pwcQAAAABJRU5ErkJggg==")
                            Dim Aux As New QRCoder.QRCodeGenerator()
                            .QR = Quadralia.Imagenes.Imagen2Byte(Aux.CreateQrCode(.URL, QRCoder.QRCodeGenerator.ECCLevel.M).GetGraphic(50))
                        End With

                        DirectCast(_dsDatos, dsEtiquetaTrazabilidad).dtDatos.AdddtDatosRow(Fila)
                    Next

                Case Else
                    Exit Sub
            End Select

            AsignarOrigenDatos(_dsDatos)
            AsignarParametros()
            rptVisor.ReportSource = _Reporte
            ''rptVisor.Refresh()
            ControlarBotones()
        Catch ex As Exception
            Quadralia.Aplicacion.InformarError(ex)
        End Try

    End Sub

    ''' <summary>
    ''' Se asigna un dataset al informe cargado en el visor
    ''' </summary>
    Private Sub AsignarOrigenDatos(ByVal dsDatos As DataSet)
        AsignarOrigenDatos(_Reporte, dsDatos)
    End Sub

    ''' <summary>
    ''' Se asigna un origen de datos a un informe
    ''' </summary>
    Private Sub AsignarOrigenDatos(ByVal informe As ReportDocument, ByVal dsDatos As DataSet)

        ' Now assign the dataset to all tables in the main report
        For Each tabla As CrystalDecisions.CrystalReports.Engine.Table In informe.Database.Tables
            tabla.SetDataSource(dsDatos)
        Next

        ' Now loop through all the sections and its objects to do the same for the subreports
        For Each seccion In informe.ReportDefinition.Sections
            ' In each section we need to loop through all the reporting objects
            For Each Objeto In seccion.ReportObjects
                If Objeto.Kind = ReportObjectKind.SubreportObject Then
                    Dim subInforme As SubreportObject = DirectCast(Objeto, SubreportObject)
                    Dim subDocumento As ReportDocument = subInforme.OpenSubreport(subInforme.SubreportName)

                    AsignarOrigenDatos(subDocumento, dsDatos)
                End If
            Next
        Next
    End Sub

    ''' <summary>
    ''' Asigna los par�metros al informe
    ''' </summary>
    Private Sub AsignarParametros()
        EstablecerParametro("MostrarLogos", _MostrarLogos)
    End Sub

    ''' <summary>
    ''' Estable el valor de un parametro en el informe solicitado
    ''' </summary>
    Private Sub EstablecerParametro(ByVal nombreParametro As String, ByVal valor As Object)
        Dim parametro As CrystalDecisions.Shared.ParameterField

        Try
            parametro = _Reporte.ParameterFields.Item(nombreParametro)
            parametro.CurrentValues.Clear()

            Dim ValorParametro As New CrystalDecisions.Shared.ParameterDiscreteValue()

            parametro.CurrentValues.IsNoValue = valor Is Nothing
            ValorParametro.Value = valor
            parametro.CurrentValues.Add(ValorParametro)
        Catch ex As Exception
        End Try
    End Sub
#End Region
#Region " METODOS PUBLICOS "
    ''' <summary>
    ''' Imprime el informe
    ''' </summary>    
    Public Sub Exportar()
        rptVisor.ExportReport()
    End Sub

    ''' <summary>
    ''' Imprime el informe    
    ''' </summary>    
    Public Sub Imprimir()
        rptVisor.PrintReport()
    End Sub

    ''' <summary>
    ''' Imprime el informe    
    ''' </summary>    
    Public Sub Imprimir(NombreImpresora As String, Optional Orientacion As CrystalDecisions.Shared.PaperOrientation = PaperOrientation.DefaultPaperOrientation, Optional FormatoPapel As Integer = -1)
#If DEBUG Then
        Me.ShowDialog()
        'Exit Sub
#End If
        _Reporte.PrintOptions.PrinterName = NombreImpresora
        _Reporte.PrintOptions.PaperOrientation = Orientacion
        If FormatoPapel > 0 Then _Reporte.PrintOptions.PaperSize = FormatoPapel
        _Reporte.PrintToPrinter(1, False, 0, 0)
    End Sub

    Public Function ExportarInformePDF(Optional ByVal NombreArchivo As String = "") As String
        ' Validaciones
        If _Reporte Is Nothing Then Return Nothing

        If String.IsNullOrEmpty(NombreArchivo) Then
            NombreArchivo = String.Format("{0}_{1:yyyyMMddHHmmss}.pdf", lblTitulo.Text.Trim, DateTime.Now)
            NombreArchivo = My.Computer.FileSystem.CombinePath(IO.Path.GetTempPath(), NombreArchivo)
        End If

        ' Verifico que no exista el archivo
        If My.Computer.FileSystem.FileExists(NombreArchivo) Then My.Computer.FileSystem.DeleteFile(NombreArchivo)

        ' Exporto
        _Reporte.ExportToDisk(ExportFormatType.PortableDocFormat, NombreArchivo)

        Return NombreArchivo
    End Function

    'Public Sub ImprimirEnPDF(Optional ByVal NombreImpresora As String = "")
    '    ' Verifico que tenga el pdf configurado
    '    If Not My.Computer.FileSystem.FileExists(cConfiguracionAplicacion.Instancia.RutaAcrobat) Then
    '        Quadralia.Aplicacion.MostrarMessageBox("No se puede imprimir las etiquetas solicitadas porque no se ha configurado al ruta al software de Adobre Reader", "Etiqueta no configurada", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '        Exit Sub
    '    End If

    '    Dim FicheroTemporal As String = ExportarInformePDF()
    '    ImprimirPDF(FicheroTemporal, NombreImpresora)
    '    If My.Computer.FileSystem.FileExists(FicheroTemporal) Then My.Computer.FileSystem.DeleteFile(FicheroTemporal, FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.DeletePermanently)
    'End Sub

    'Private Sub ImprimirPDF(ByVal Ruta As String, Optional ByVal NombreImpresora As String = "")
    '    ' Miro si tengo la impresora establecida, sino, pido alguna
    '    If String.IsNullOrEmpty(NombreImpresora) Then
    '        Dim pd As New PrintDialog
    '        If Not pd.ShowDialog() = Windows.Forms.DialogResult.OK Then Exit Sub

    '        NombreImpresora = pd.PrinterSettings.PrinterName
    '    End If

    '    ' Configuro los par�metros
    '    Dim RutaAplicacion = cConfiguracionAplicacion.Instancia.RutaAcrobat
    '    ' Dim Parametros As String = String.Format("/n /t ""{0}"" ""{1}""", Ruta, NombreImpresora)
    '    Dim Parametros As String = String.Format("/n /p ""{0}""", Ruta, NombreImpresora)
    '    Dim Process As New Process()
    '    Process.StartInfo = New ProcessStartInfo(RutaAplicacion, Parametros)
    '    Process.Start()
    '    Process.WaitForExit(20000)
    'End Sub

    ''' <summary>
    ''' Muestra / Oculta las cabeceras del informe
    ''' </summary>
    ''' <param name="Mostrar"></param>
    Public Sub MostrarCabeceras(ByVal Mostrar As Boolean)
        If _CambioControlado Then Exit Sub

        _MostrarLogos = Mostrar
        RefrescarDatos()
    End Sub
#End Region
#Region " RESTO DE FUNCIONES "
    Private Sub rptVisor_Error(ByVal source As System.Object, ByVal e As CrystalDecisions.Windows.Forms.ExceptionEventArgs) Handles rptVisor.Error
#If DEBUG Then
#Else
        e.Handled = True
#End If
    End Sub
#End Region
End Class
