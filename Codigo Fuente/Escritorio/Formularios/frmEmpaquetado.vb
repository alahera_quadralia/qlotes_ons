Public Class frmEmpaquetado
#Region " DECLARACIONES "
    Private _EstadoFormulario As Quadralia.Enumerados.EstadoFormulario = Quadralia.Enumerados.EstadoFormulario.Espera  ' Indica el estado en el que se encuentra el formulario
    Private _Registro As Empaquetado = Nothing
    Private _Contexto As Entidades = Nothing
    Private _dgvDesgloseEliminando As Boolean = False

    Private _dgvLoad As Boolean = False ' Bandera para evitar que se cargue la primera fila cuando se asigna un origen de datos al DGV
#End Region
#Region " PROPIEDADES "
    Public Overrides Property Estado() As Quadralia.Enumerados.EstadoFormulario
        Get
            Return _EstadoFormulario
        End Get
        Set(ByVal value As Quadralia.Enumerados.EstadoFormulario)
            _EstadoFormulario = value

            'Para evitar refrescos raros, cuando ponemos el formulario en estado de espera, volvemos a la pesta�a de datos
            hdrBusqueda.Panel.Enabled = (value = Quadralia.Enumerados.EstadoFormulario.Espera)
            htmlObservaciones.Enabled = (value <> Quadralia.Enumerados.EstadoFormulario.Espera)
            tbpLotesEntrada.Visible = (value <> Quadralia.Enumerados.EstadoFormulario.Anhadiendo)
            dgvLotesEntrada.Enabled = True

            ' Desactivo los controles de edici�n
            dtpFecha.Enabled = (value <> Quadralia.Enumerados.EstadoFormulario.Espera)

            'For Each Fila As DataGridViewRow In dgvEtiquetas.Rows
            '    Fila.DefaultCellStyle = New DataGridViewCellStyle
            'Next

            ' Tengo que establecer el foco en un control para que no quede "muerto" el ribbon
            If value = Quadralia.Enumerados.EstadoFormulario.Espera Then
                Me.ActiveControl = txtBcodigo.TextBox
            Else
                Me.ActiveControl = dtpFecha
            End If

            ConfigurarEntorno()
            ControlarBotones()
        End Set
    End Property

    ''' <summary>
    ''' Registro que se muestra actualmente en el formulario
    ''' </summary>
    Public Property Registro As Empaquetado
        Get
            Return _Registro
        End Get
        Set(ByVal value As Empaquetado)
            On Error Resume Next
            _Registro = value

            CargarRegistro(value)
        End Set
    End Property

    ''' <summary>
    ''' Contexto que utilizar� el formulario
    ''' </summary>
    Public Property Contexto As Entidades
        Get
            If _Contexto Is Nothing Then
                _Contexto = cDAL.Instancia.getContext()
                _Contexto.ContextOptions.LazyLoadingEnabled = True
            End If
            Return _Contexto
        End Get
        Set(ByVal value As Entidades)
            _Contexto = value
        End Set
    End Property

    ''' <summary>
    ''' Obtiene / establece el id del elemento cargado en el formulario
    ''' </summary>
    Public Property IdRegistro As Long
        Get
            If Me.Registro Is Nothing Then Return -1 Else Return Me.Registro.id
        End Get
        Set(ByVal value As Long)
            Me.Registro = (From ent As Empaquetado In Me.Contexto.Empaquetados Where ent.id = value).FirstOrDefault
        End Set
    End Property
#End Region
#Region " IMPLEMENTACIONES NECESARIAS DEL FORMULARIO HIJO "
    ''' <summary>
    ''' Tab del ribbon asociado a este formulario
    ''' </summary>
    Public Overrides ReadOnly Property Tab() As ComponentFactory.Krypton.Ribbon.KryptonRibbonTab
        Get
            Return frmPrincipal.tabEmpaquetado
        End Get
    End Property
#End Region
#Region " LIMPIEZA "
    ''' <summary>
    ''' Limpia los controles relacionados con la b�squeda de registros
    ''' </summary>
    Private Sub LimpiarBusqueda() Handles btnLimpiarBusqueda.Click
        txtBcodigo.Text = String.Empty
        dtpBFechaInicio.Value = DateTime.Now
        dtpBFechaInicio.ValueNullable = Nothing
        dtpBFechaFin.Value = DateTime.Now
        dtpBFechaFin.ValueNullable = Nothing
        txtBEtiqueta.Clear()
        txtBLoteEntrada.Clear()

        chkBRegistrosEliminados.Checked = False

        dgvResultadosBuscador.DataSource = Nothing
        dgvResultadosBuscador.Rows.Clear()
    End Sub

    ''' <summary>
    ''' Limpia los controles relacionados con la edicion de registros
    ''' </summary>
    Private Sub LimpiarDatos()
        txtCodigo.Clear()
        txtPeso.Clear()
        txtNumEtiquetas.Clear()
        dtpFecha.ValueNullable = DateTime.Now
        chkReactivarRegistro.Checked = False
        htmlObservaciones.Clear()
        dgvEtiquetas.DataSource = Nothing

        Me.hdrCabeceraEtiquetas.ValuesPrimary.Heading = "Etiquetas"
        Me.hdrCabeceraEtiquetas.StateCommon.HeaderPrimary.Back.Color1 = System.Drawing.Color.Transparent
        Me.hdrCabeceraEtiquetas.StateCommon.HeaderPrimary.Back.Color2 = System.Drawing.Color.Transparent

        LimpiarLotesEntrada()
        epErrores.Clear()
    End Sub

    Private Sub LimpiarLotesEntrada()
        dgvLotesEntrada.DataSource = Nothing
        dgvLotesEntrada.Rows.Clear()
    End Sub

    ''' <summary>
    ''' Borra todos los controles del formulario
    ''' </summary>
    Private Sub LimpiarTodo()
        LimpiarBusqueda()
        LimpiarDatos()
    End Sub
#End Region
#Region " CONTROL DE BOTONES "
    Private Sub ControlarBotones()
        ControlarRibbon()
        ControlarBotonesEtiquetas()
    End Sub

    ''' <summary>
    ''' Controla el cambio de botones del formulario principal
    ''' </summary>
    Private Sub ControlarRibbon()
        If Me.ParentForm Is Nothing Then Exit Sub
        If Not TypeOf Me.ParentForm Is frmPrincipal Then Exit Sub

        With DirectCast(Me.ParentForm, frmPrincipal)
            .krgtEmpaquetadoAccionesEdicion.Visible = (Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera)
            .krgtEmpaquetadoAccionesEspera.Visible = Not .krgtEmpaquetadoAccionesEdicion.Visible
            .mnuRapidoGuardar.Enabled = .krgtEmpaquetadoAccionesEdicion.Visible

            .btnEmpaquetadoAccionesModificar.Enabled = (Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera) AndAlso Registro IsNot Nothing
            .btnEmpaquetadoAccionesEliminar.Enabled = .btnEmpaquetadoAccionesModificar.Enabled

            ' Lector de c�digos
            .krgtEmpaquetadoMasAcciones1.Visible = (Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera)
            .btnEmpaquetadoAccionesLectorCodigos.Enabled = Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera AndAlso Me.Registro IsNot Nothing

            '' Impresi�n
            .krgtEmpaquetadoMasAcciones2.Visible = True
            .btnEmpaquetadoAccionesImprimir.Enabled = (Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera AndAlso Me.Registro IsNot Nothing)
            .btnEmpaquetadoAccionesImprimirEtiquetas.Enabled = (Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera AndAlso dgvEtiquetas.SelectedRows.Count > 0)
            .btnEmpaquetadoAccionesImprimirMakro.Visible = False '(Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera AndAlso Me.Registro IsNot Nothing)
            '.btnEmpaquetadoAccionesImprimirMakro.Enabled = (Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera AndAlso Me.Registro IsNot Nothing)
        End With
    End Sub

    ''' <summary>
    ''' Controla los botones relativos a las etiquetas
    ''' </summary>
    Private Sub ControlarBotonesEtiquetas()
        btnEtiquetasLector.Enabled = IIf(Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera, ButtonEnabled.True, ButtonEnabled.False)
        btnEtiquetasNuevo.Enabled = IIf(Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera, ButtonEnabled.True, ButtonEnabled.False)
        btnEtiquetasEliminar.Enabled = IIf(Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera AndAlso dgvEtiquetas.SelectedRows.Count > 0, ButtonEnabled.True, ButtonEnabled.False)
    End Sub
#End Region
#Region " RUTINA DE INICIO / FIN Y CARGA DE DATOS MAESTROS "
    ''' <summary>
    ''' Rutina de inicio del formulario
    ''' </summary>
    Private Sub Inicio(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Tag = Me.Text
        tabGeneral.SelectedIndex = 0

        Quadralia.Formularios.AutoTabular(Me)

        ' Cargamos los datos maestros
        CargarDatosMaestros()
        ConfigurarEntorno()

        ' Limpieza
        LimpiarTodo()
        tabGeneral.SelectedIndex = 0
        Me.Registro = Nothing

        ' Oculto el buscador
        If Not cConfiguracionAplicacion.Instancia.BuscadoresAbiertos Then OcultarBusquedaDobleClick(hdrBusqueda, Nothing)

        ' No quiero nuevas columnas
        dgvResultadosBuscador.AutoGenerateColumns = False
        dgvEtiquetas.AutoGenerateColumns = False
        dgvLotesEntrada.AutoGenerateColumns = False

        Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera
    End Sub

    ''' <summary>
    ''' Carga los datos maestros
    ''' </summary>
    Public Sub CargarDatosMaestros()
    End Sub
#End Region
#Region " BUSQUEDA, CARGA DE Y EDICION DE REGISTROS "
    ''' <summary>
    ''' Realiza la b�squeda de registros coincidentes con los par�metros especificados
    ''' </summary>
    Private Sub Buscar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBBuscar.Click

        'If (IsDBNull(dtpBFechaInicio.ValueNullable) AndAlso IsDBNull(dtpBFechaFin.ValueNullable) AndAlso
        '    String.IsNullOrEmpty(txtBcodigo.Text) AndAlso txtBEtiqueta.Item Is Nothing AndAlso
        '    txtBLoteEntrada.Item Is Nothing AndAlso Not String.IsNullOrEmpty(txtBNLote.Text)) Then
        '    Quadralia.Aplicacion.MostrarMessageBox("AVISO", "Es necesario especificar alg�n par�metro m�s aparte del N� de Lote")
        '    Exit Sub
        'End If

        Dim FechaInicio As Nullable(Of DateTime) = Nothing
        Dim FechaFin As Nullable(Of DateTime) = Nothing
        Dim NumeroCodigo As Nullable(Of Integer) = Nothing
        Dim IdEtiqueta As Nullable(Of Long) = Nothing
        Dim IdLoteEntrada As Nullable(Of Long) = Nothing
        Dim LoteSalida As String = String.Empty

        Dim Aux As Integer = 0


        If Not IsDBNull(dtpBFechaInicio.ValueNullable) Then FechaInicio = dtpBFechaInicio.Value.Date + New TimeSpan(0, 0, 0)
        If Not IsDBNull(dtpBFechaFin.ValueNullable) Then FechaFin = dtpBFechaFin.Value.Date + New TimeSpan(23, 59, 59)
        If Not String.IsNullOrEmpty(txtBcodigo.Text) AndAlso Integer.TryParse(txtBcodigo.Text, Aux) Then NumeroCodigo = Aux
        If txtBEtiqueta.Item IsNot Nothing Then IdEtiqueta = DirectCast(txtBEtiqueta.Item, Etiqueta).id
        If txtBLoteEntrada.Item IsNot Nothing Then IdLoteEntrada = DirectCast(txtBLoteEntrada.Item, LoteEntrada).id
        If Not String.IsNullOrEmpty(txtBLoteSAlida.Text) Then LoteSalida = txtBLoteSAlida.Text

        Dim Registros As New List(Of Empaquetado)
        'Dim Registros = (From emp As Empaquetado In Contexto.Empaquetados
        '                 From etq As Etiqueta In emp.Etiquetas.DefaultIfEmpty
        '                 Where ((chkBRegistrosEliminados.Checked OrElse Not emp.fechaBaja.HasValue) _
        '                        AndAlso (Not NumeroCodigo.HasValue OrElse emp.id = NumeroCodigo.Value) _
        '                        AndAlso (Not FechaInicio.HasValue OrElse emp.fecha >= FechaInicio.Value) _
        '                        AndAlso (Not FechaFin.HasValue OrElse emp.fecha <= FechaFin.Value) _
        '                        AndAlso (Not IdEtiqueta.HasValue OrElse etq.id = IdEtiqueta.Value) _
        '                        AndAlso (Not IdLoteEntrada.HasValue OrElse (etq.LoteEntradaLinea IsNot Nothing AndAlso etq.LoteEntradaLinea.LoteEntrada IsNot Nothing AndAlso etq.LoteEntradaLinea.LoteEntrada.id = IdLoteEntrada.Value)))
        '                 Order By emp.fecha Descending
        '                 Select emp).Distinct().ToList

        If Not String.IsNullOrEmpty(LoteSalida) Then
            Dim res As List(Of Etiqueta) = (From emp As Empaquetado In Contexto.Empaquetados
                                            From etq As Etiqueta In emp.Etiquetas.DefaultIfEmpty
                                            Where ((chkBRegistrosEliminados.Checked OrElse Not emp.fechaBaja.HasValue) _
                                                    AndAlso (Not NumeroCodigo.HasValue OrElse emp.id = NumeroCodigo.Value) _
                                                    AndAlso (Not FechaInicio.HasValue OrElse emp.fecha >= FechaInicio.Value) _
                                                    AndAlso (Not FechaFin.HasValue OrElse emp.fecha <= FechaFin.Value) _
                                                    AndAlso (Not IdEtiqueta.HasValue OrElse etq.id = IdEtiqueta.Value) _
                                                    AndAlso (Not IdLoteEntrada.HasValue OrElse (etq.LoteEntradaLinea IsNot Nothing AndAlso etq.LoteEntradaLinea.LoteEntrada IsNot Nothing AndAlso etq.LoteEntradaLinea.LoteEntrada.id = IdLoteEntrada.Value)))
                                            Order By emp.fecha Descending
                                            Select etq).Distinct().ToList
            Dim lista As New List(Of Empaquetado)
            For Each etq As Etiqueta In res
                If etq.LoteSalida = LoteSalida OrElse etq.LoteSalida.Contains(LoteSalida) Then
                    lista.Add(etq.Empaquetado)
                End If

            Next
            registros = lista.Distinct.ToList
        Else
            registros = (From emp As Empaquetado In Contexto.Empaquetados
                         From etq As Etiqueta In emp.Etiquetas.DefaultIfEmpty
                         Where ((chkBRegistrosEliminados.Checked OrElse Not emp.fechaBaja.HasValue) _
                                    AndAlso (Not NumeroCodigo.HasValue OrElse emp.id = NumeroCodigo.Value) _
                                    AndAlso (Not FechaInicio.HasValue OrElse emp.fecha >= FechaInicio.Value) _
                                    AndAlso (Not FechaFin.HasValue OrElse emp.fecha <= FechaFin.Value) _
                                    AndAlso (Not IdEtiqueta.HasValue OrElse etq.id = IdEtiqueta.Value) _
                                    AndAlso (Not IdLoteEntrada.HasValue OrElse (etq.LoteEntradaLinea IsNot Nothing AndAlso etq.LoteEntradaLinea.LoteEntrada IsNot Nothing AndAlso etq.LoteEntradaLinea.LoteEntrada.id = IdLoteEntrada.Value)))
                         Order By emp.fecha Descending
                         Select emp).Distinct().ToList
        End If

        If Registros IsNot Nothing Then
            _dgvLoad = (Registros.Count > 1)
            dgvResultadosBuscador.DataSource = Registros
            _dgvLoad = False
        End If

        If (Registros.Count > 1) Then dgvResultadosBuscador.ClearSelection()
    End Sub

    ''' <summary>
    ''' Se prepara el formulario para introducir un nuevo registro    
    ''' </summary>    
    Public Overrides Sub Nuevo()
        LimpiarDatos()
        Me.Estado = Quadralia.Enumerados.EstadoFormulario.Anhadiendo

        Dim Aux = New Empaquetado
        Aux.fecha = DateTime.Now()

        Aux.UsuarioReference.EntityKey = Quadralia.Aplicacion.UsuarioConectado.EntityKey

        ' Lo a�ado al contexto
        Contexto.Empaquetados.AddObject(Aux)
        Me.Registro = Aux
    End Sub

    ''' <summary>
    ''' Se carga el registro seleccionado el formulario
    ''' </summary>
    Private Sub Cargar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvResultadosBuscador.SelectionChanged
        If dgvResultadosBuscador.SelectedRows.Count = 1 Then
            Me.Registro = dgvResultadosBuscador.SelectedRows(0).DataBoundItem
        Else
            Me.Registro = Nothing
            LimpiarDatos()
        End If
    End Sub

    ''' <summary>
    ''' Carga el registro actual del formulario
    ''' </summary>
    Private Sub CargarRegistro()
        CargarRegistro(Me.Registro)
    End Sub

    ''' <summary>
    ''' Se carga un registro en el formulario
    ''' </summary>
    Private Sub CargarRegistro(ByVal Instancia As Empaquetado)
        ' Si el DGV est� vinculando un origen de datos, no cargo nada
        If _dgvLoad Then Exit Sub

        ' Limpio el formulario y cargo el registro
        LimpiarDatos()
        ControlarBotones()

        If Instancia Is Nothing Then Exit Sub

        With Instancia
            If Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Anhadiendo Then txtCodigo.Text = .Codigo
            dtpFecha.ValueNullable = .fecha
            If Not String.IsNullOrEmpty(.observaciones) Then htmlObservaciones.Text = .observaciones
            chkReactivarRegistro.Visible = .fechaBaja.GetHashCode

            Me.Text = String.Format("{0} [{1}]", Me.Tag, .Codigo)
        End With

        RefrescarEtiquetas()
        RefrescarLotesEntrada()
        _Registro = Instancia
    End Sub

    ''' <summary>
    ''' Preparamos el formulario para modificar un registro cargado
    ''' </summary>
    Public Overrides Sub Modificar()
        If Me.Registro IsNot Nothing Then
            Me.Estado = Quadralia.Enumerados.EstadoFormulario.Editando
        End If
    End Sub

    ''' <summary>
    ''' Eliminaci�n l�gica del registro
    ''' </summary>
    Public Overrides Sub Eliminar()
        If Registro IsNot Nothing Then
            If Quadralia.Aplicacion.MostrarMessageBox(My.Resources.ConfirmarEliminacion, My.Resources.TituloConfirmarEliminacion, MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
                ' Lo elimino del contexto
                Me.Registro.fechaBaja = DateTime.Now
                Me.Contexto.SaveChanges()

                If dgvResultadosBuscador.SelectedRows.Count > 0 Then Buscar(Nothing, Nothing) Else Cargar(Nothing, Nothing)
            End If
        End If
    End Sub

    ''' <summary>
    ''' Cancela la edici�n del registro
    ''' </summary>
    Public Overrides Sub Cancelar()
        If Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera Then Exit Sub

        ' Cambio el estado del formulario
        Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera

        ' Limpio los datos y desecho los cambios
        Quadralia.Linq.CancelarCambios(Me.Contexto)

        ' Limpio los datos
        LimpiarDatos()

        ' Cargo el registro seleccionado
        Cargar(Nothing, Nothing)
    End Sub

    ''' <summary>
    ''' Se guardan los datos en la base de datos
    ''' </summary>
    ''' <param name="MostrarMensaje">Indica si se muestra un mensaje con el resultado de la funci�n</param>
    Public Overrides Function Guardar(Optional ByVal MostrarMensaje As Boolean = True) As Boolean
        If Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera Then Return True

        ''comprobamos que s�lo se usa un lote en el paquete
        'If (Registro IsNot Nothing And Registro.Etiquetas.Count() > 0) Then
        '    Dim numLotesDistintos = (From e In Registro.Etiquetas Group e By lote = e.Numero_Lote Into Group).Count()

        '    If (numLotesDistintos > 1) Then
        '        Quadralia.Aplicacion.MostrarMessageBox("No se permite asignar m�s de un lote a un paquete", "AVISO", MessageBoxButtons.OK, MessageBoxIcon.Information)
        '        Return False
        '    End If
        'End If
        ' Valido los controles
        Me.ValidateChildren(ValidationConstraints.Visible + ValidationConstraints.Enabled)

        If epErrores.HasErrors Then
                If MostrarMensaje Then Quadralia.Aplicacion.MostrarMessageBox(My.Resources.ErroresEncontrados, My.Resources.TituloErroresEncontrados, MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return False
        End If

        ' Guardo los datos en el registro
        With Registro
            If IsDBNull(dtpFecha.ValueNullable) Then .fecha = Nothing Else .fecha = dtpFecha.Value
            .observaciones = htmlObservaciones.Text

            If chkReactivarRegistro.Visible AndAlso chkReactivarRegistro.Checked Then .fechaBaja = Nothing
        End With

        Try
            ' Guardo el registro en base de datos
            Contexto.SaveChanges()

            ' Si estoy a�adiendo, obtengo un n�mero de Albar�n
            If MostrarMensaje Then Quadralia.Aplicacion.MostrarMensajeNtfIco(My.Resources.TituloDatosGuardatos, My.Resources.DatosGuardados, ToolTipIcon.Info)

            ' Restablezco el formulario
            Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera
            LimpiarDatos()
            CargarRegistro()

            Return True
        Catch ex As Exception
            If MostrarMensaje Then Quadralia.Aplicacion.InformarError(ex)
            Return False
        End Try
    End Function
#End Region
#Region " RESTO DE FUNCIONES "
    ''' <summary>
    ''' Oculta / muestra el panel de b�squeda
    ''' </summary>
    Private Sub OcultarBusqueda(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOcultarBusqueda.Click
        Quadralia.KryptonForms.EsconderPanel(splSeparador, hdrBusqueda)
    End Sub

    ''' <summary>
    ''' Ordena los resultados del DGV de Resultados   
    ''' </summary>
    Private Sub OrdenarResultados(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvResultadosBuscador.ColumnHeaderMouseClick
        Quadralia.Formularios.Funcionalidad.OrdenarDGV(Of Empaquetado)(dgvResultadosBuscador, e)
    End Sub

    ''' <summary>
    ''' Cuando el usuario hace doble click en el panel, desencadenamos la misma funci�n que si hiciera click en el bot�n
    ''' </summary>
    Private Sub OcultarBusquedaDobleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hdrBusqueda.DoubleClick
        ' Hay que hacerlo as�, no sirve con llamar a la funci�n directamente. si no, el bot�n del header se descontrola y desaparece el interior del panel
        btnOcultarBusqueda.PerformClick()
    End Sub

    ''' <summary>
    ''' Lanzamos la b�squeda de registros si el usuario presiona ENTER en los campos de par�metros
    ''' </summary>
    Private Sub ManejadorBusquedasTexto(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtBcodigo.KeyDown, dtpBFechaFin.KeyDown, dtpBFechaInicio.KeyDown, txtBLoteEntrada.KeyDown, txtBEtiqueta.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.Handled = True
            Buscar(Nothing, Nothing)
        End If
    End Sub

    ''' <summary>
    ''' Evita que salga el molesto error del DGV
    ''' </summary>
    Private Sub EvitarErrores(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvResultadosBuscador.DataError, dgvEtiquetas.DataError
        e.Cancel = True
    End Sub

    ''' <summary>
    ''' Pone el formulario en modo edici�n cuando el usuario hace doble click sobre un resultado del buscador
    ''' </summary>
    Private Sub ActivarEdicionRegistro(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvResultadosBuscador.CellDoubleClick, dgvResultadosBuscador.CellContentDoubleClick
        If e.RowIndex > -1 Then
            dgvResultadosBuscador.Rows(e.RowIndex).Selected = True
            Modificar()
        End If
    End Sub

    ''' <summary>
    ''' Nos desplaza a traves de los controles al pulsar enter
    ''' </summary>
    Private Sub EnterComoTab(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.Enter AndAlso (Me.ActiveControl Is Nothing OrElse Me.ActiveControl.Parent Is Nothing OrElse Not TypeOf (Me.ActiveControl.Parent) Is KryptonTextBox OrElse Not DirectCast(Me.ActiveControl.Parent, KryptonTextBox).Multiline) Then
            e.Handled = True
            SendKeys.Send("{TAB}")
        End If
    End Sub

    ''' <summary>
    ''' Control de las teclas de acci�n del usuario
    ''' </summary>
    Private Sub ControlTeclasBuscador(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvResultadosBuscador.KeyDown
        If Me.Registro Is Nothing Then Exit Sub

        Select Case e.KeyCode
            Case Keys.Delete
                e.Handled = True
                Eliminar()
            Case Keys.F2, Keys.Enter
                e.Handled = True
                Modificar()
        End Select
    End Sub
#End Region
#Region " ETIQUETAS "
    ''' <summary>
    ''' Pido confirmaci�n antes de eliminar el registro
    ''' </summary>
    Private Sub EliminarRegistro() Handles btnEtiquetasEliminar.Click
        ' Validaciones
        If Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera Then Exit Sub
        If Me.Registro Is Nothing Then Exit Sub
        If dgvEtiquetas.SelectedRows.Count <= 0 Then Exit Sub
        If Quadralia.Aplicacion.MostrarMessageBox("�Desea realmente eliminar las etiquetas seleccionadas?", "Confirmar Eliminaci�n", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) <> DialogResult.Yes Then Exit Sub

        ' Elimino las l�neas
        For Each Linea As DataGridViewRow In dgvEtiquetas.SelectedRows
            If Linea.DataBoundItem Is Nothing Then Continue For
            If Me.Registro.Etiquetas.Contains(Linea.DataBoundItem) Then Me.Registro.Etiquetas.Remove(Linea.DataBoundItem)
        Next

        RefrescarEtiquetas()
    End Sub

    ''' <summary>
    ''' Busca un lote de entrada para asignar al lote de salida
    ''' </summary>
    Private Sub AnhadirEtiqueta() Handles btnEtiquetasNuevo.Click
        If Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera Then Exit Sub
        If Me.Registro Is Nothing Then Exit Sub

        Dim formulario As New frmSeleccionarEtiquetaEmpaquetado(Me.Contexto, Me.Registro.Etiquetas.ToList, True)
        If formulario.ShowDialog() <> DialogResult.OK Then Exit Sub

        For Each Etiqueta As Etiqueta In formulario.Items
            Me.Registro.Etiquetas.Add(Etiqueta)
        Next

        RefrescarEtiquetas()
    End Sub

    ''' <summary>
    ''' Se produce cuando el usuario selecciona una fila
    ''' </summary>
    Private Sub EtiquetaSeleccionada(sender As Object, e As EventArgs) Handles dgvEtiquetas.SelectionChanged
        ControlarBotones()
    End Sub

    ''' <summary>
    ''' Ordena los registros por la columna deseada
    ''' </summary>
    Private Sub OrdenarEtiquetas(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvEtiquetas.ColumnHeaderMouseClick
        Quadralia.Formularios.Funcionalidad.OrdenarDGV(Of Etiqueta)(dgvEtiquetas, e)
    End Sub

    ''' <summary>
    ''' Refresca el DGV de etiquetas
    ''' </summary>
    Private Sub RefrescarEtiquetas()
        dgvEtiquetas.DataSource = Nothing
        dgvEtiquetas.Rows.Clear()
        ControlarBotonesEtiquetas()

        ' Validaciones
        If Registro Is Nothing OrElse Registro.Etiquetas Is Nothing OrElse Registro.Etiquetas.Count = 0 Then Exit Sub
        Quadralia.Aplicacion.RefrescarDGVOrdenado(Of Etiqueta)(dgvEtiquetas, Me.Registro.Etiquetas.ToList)

        Me.txtPeso.Text = Registro.Peso
        Me.txtNumEtiquetas.Text = Registro.NumeroEtiquetas
        If (Registro.Etiquetas.Select(Function(p) p.Tama�o).Distinct().Count() > 1) Then
            Me.hdrCabeceraEtiquetas.StateCommon.HeaderPrimary.Back.Color1 = System.Drawing.Color.Red
            Me.hdrCabeceraEtiquetas.StateCommon.HeaderPrimary.Back.Color2 = System.Drawing.Color.Red
            hdrCabeceraEtiquetas.ValuesPrimary.Heading = "Etiquetas varios tama�os"
        Else
            Me.hdrCabeceraEtiquetas.StateCommon.HeaderPrimary.Back.Color1 = System.Drawing.Color.Transparent
            Me.hdrCabeceraEtiquetas.StateCommon.HeaderPrimary.Back.Color2 = System.Drawing.Color.Transparent
        End If
    End Sub

    ''' <summary>
    ''' Muestra el formulario con la etiqueta cargada al usuario
    ''' </summary>
    Private Sub VerEtiqueta(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvEtiquetas.DoubleClick
        If Not cConfiguracionAplicacion.Instancia.PermitirNavegabilidad Then Exit Sub

        If dgvEtiquetas.SelectedRows.Count > 0 AndAlso Quadralia.Aplicacion.PuedeAccederA(Quadralia.Aplicacion.Permisos.ETIQUETAS) Then
            Dim FormularioEtiquetas As frmGestionEtiquetas = Quadralia.Aplicacion.AbrirFormulario(frmPrincipal, "Escritorio.frmGestionEtiquetas", Quadralia.Aplicacion.Permisos.ETIQUETAS, My.Resources.Etiqueta_16)
            FormularioEtiquetas.IdRegistro = DirectCast(dgvEtiquetas.SelectedRows(0).DataBoundItem, Etiqueta).id
        End If
    End Sub

    ''' <summary>
    ''' Abre el lector de c�digo de barras
    ''' </summary>
    Public Sub AbrirLector() Handles btnEtiquetasLector.Click
        ' Validaciones
        If Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera Then Exit Sub
        If Me.Registro Is Nothing Then Exit Sub

        Dim frmLector As New frmLectorBarras(Me.Contexto, Me.Registro.Etiquetas.ToList)
        If frmLector.ShowDialog() <> Windows.Forms.DialogResult.OK Then Exit Sub

        ' A�ado las etiquetas le�das
        For Each Etiqueta As Etiqueta In frmLector.Items
            Me.Registro.Etiquetas.Add(Etiqueta)
        Next

        RefrescarEtiquetas()
    End Sub
#End Region
#Region " VALIDACIONES "
    ''' <summary>
    ''' Verifica que se haya introducido texto en un textbox marcado como obligatorio
    ''' </summary>
    Private Sub ValidarTextBoxObligatorio(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs)
        If Not TypeOf (sender) Is KryptonTextBox Then Exit Sub

        If Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera AndAlso String.IsNullOrEmpty(DirectCast(sender, KryptonTextBox).Text) Then
            epErrores.SetError(sender, My.Resources.TextBoxObligatorio)
        Else
            epErrores.SetError(sender, "")
        End If
    End Sub

    ''' <summary>
    ''' Verifica que se haya seleccionado una fecha del DTP marcado como obligatorio
    ''' </summary>
    Private Sub ValidarDTPObligatorio(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles dtpFecha.Validating
        If Not TypeOf (sender) Is KryptonDateTimePicker Then Exit Sub

        If Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera AndAlso IsDBNull(DirectCast(sender, KryptonDateTimePicker).ValueNullable) Then
            epErrores.SetError(sender, My.Resources.DTPObligatorio)
        Else
            epErrores.SetError(sender, "")
        End If
    End Sub

    ''' <summary>
    ''' Obliga al usuario a seleccionar un cliente antes de continuar
    ''' </summary>
    Private Sub ValidarTxtBuscador(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs)
        If Not TypeOf (sender) Is cTextboxBuscador Then Exit Sub

        If Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera AndAlso DirectCast(sender, cTextboxBuscador).Item Is Nothing Then
            epErrores.SetError(sender, My.Resources.TextBoxObligatorio)
        Else
            epErrores.SetError(sender, "")
        End If
    End Sub

    ''' <summary>
    ''' Verifica que se haya hecho una seleccion en un combobox marcado como obligatorio
    ''' </summary>
    Private Sub ComboObligatorio(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs)
        If Not TypeOf (sender) Is KryptonComboBox Then Exit Sub

        If Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera AndAlso DirectCast(sender, KryptonComboBox).SelectedIndex = -1 Then
            epErrores.SetError(sender, My.Resources.ComboObligatorio)
        Else
            epErrores.SetError(sender, "")
        End If
    End Sub

    Private Sub SoloDecimales(ByVal sender As Object, ByVal e As KeyPressEventArgs)
        Quadralia.Validadores.QuadraliaValidadores.SoloNumerosYDecimales(sender, e)
    End Sub
#End Region
#Region " SEGURIDAD "
    ''' <summary>
    ''' Configura el formulario en base a los permisos del usuario
    ''' </summary>
    Private Sub ConfigurarEntorno()
        tbpLotesEntrada.Visible = Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera AndAlso Quadralia.Aplicacion.UsuarioConectado IsNot Nothing AndAlso Quadralia.Aplicacion.PuedeAccederA(Quadralia.Aplicacion.Permisos.LOTES_ENTRADA)
    End Sub
#End Region
#Region " NAVEGABILIDAD "
#Region " ALBARANES ENTRADA "
    Private Sub RefrescarLotesEntrada()
        dgvLotesEntrada.DataSource = Nothing
        dgvLotesEntrada.Rows.Clear()

        ' Validaciones
        If Registro Is Nothing OrElse Registro.AlbaranesEntrada Is Nothing OrElse Registro.AlbaranesEntrada.Count = 0 Then Exit Sub
        Quadralia.Aplicacion.RefrescarDGVOrdenado(Of LoteEntrada)(dgvLotesEntrada, Me.Registro.AlbaranesEntrada)
    End Sub

    Private Sub VerLoteEntrada(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvLotesEntrada.DoubleClick
        If Not cConfiguracionAplicacion.Instancia.PermitirNavegabilidad Then Exit Sub

        If dgvLotesEntrada.SelectedRows.Count > 0 AndAlso Quadralia.Aplicacion.PuedeAccederA(Quadralia.Aplicacion.Permisos.LOTES_ENTRADA) Then
            Dim FormularioAlbaran As frmLoteEntrada = Quadralia.Aplicacion.AbrirFormulario(frmPrincipal, "Escritorio.frmLoteEntrada", Quadralia.Aplicacion.Permisos.LOTES_ENTRADA, My.Resources.Entrada_16)
            FormularioAlbaran.IdRegistro = DirectCast(dgvLotesEntrada.SelectedRows(0).DataBoundItem, LoteEntrada).id
        End If
    End Sub

    ''' <summary>
    ''' Ordena los registros por la columna deseada
    ''' </summary>
    Private Sub OrdenarAlbaranes(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvLotesEntrada.ColumnHeaderMouseClick
        Quadralia.Formularios.Funcionalidad.OrdenarDGV(Of LoteEntrada)(dgvLotesEntrada, e)
    End Sub
#End Region
#End Region
#Region " IMPRIMIR "
    ''' <summary>
    ''' Imprime el registro actual
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Imprimir(ByVal Optional pImprimirMakro As Boolean = False)
        ' Validaciones
        If Me.Registro Is Nothing Then Exit Sub
        If Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera Then Exit Sub

        ' Imprimo la lista
        Dim Lista As New List(Of Empaquetado)
        Lista.Add(Me.Registro)

        If (Me.Registro.Etiquetas.Count() = 0) Then
            Quadralia.Aplicacion.MostrarMessageBox("El paquete no contiene etiquetas. Operaci�n cancelada.", "AVISO")
            Exit Sub
        End If
        ImprimirEmpaquetado(Lista, pImprimirMakro)
    End Sub

    ''' <summary>
    ''' Imprime los registros especificados como par�metro de entrada
    ''' </summary>
    Public Shared Sub ImprimirEmpaquetado(ByVal Empaquetados As List(Of Empaquetado), ByVal Optional pImprimirMakro As Boolean = False)
        If Not Quadralia.Aplicacion.PuedeAccederA(Quadralia.Aplicacion.Permisos.EMPAQUETADO) Then Exit Sub
        If Empaquetados Is Nothing OrElse Empaquetados.Count = 0 Then Exit Sub

        Dim Formulario As frmVisorInforme = Nothing
        Dim Peso As Decimal = Empaquetados(0).Peso
#If DEBUG Then
        'If (pImprimirMakro) Then
        '    If (Peso = 0 OrElse Peso >= 100) Then
        '        Quadralia.Aplicacion.MostrarMessageBox("El peso del empaquetado Makro debe estar entre 0 y 99,99 Kg")
        '        Return
        '    End If

        '    Formulario = Quadralia.Aplicacion.AbrirInforme(Nothing, frmVisorInforme.Informe.EmpaquetadoMakro, Quadralia.Aplicacion.Permisos.EMPAQUETADO)
        'Else
        '    Formulario = Quadralia.Aplicacion.AbrirInforme(Nothing, frmVisorInforme.Informe.Empaquetado, Quadralia.Aplicacion.Permisos.EMPAQUETADO)
        'End If

        Formulario = Quadralia.Aplicacion.AbrirInforme(Nothing, frmVisorInforme.Informe.Empaquetado, Quadralia.Aplicacion.Permisos.EMPAQUETADO)
#Else
        If (pImprimirMakro) Then
            If (Peso = 0 OrElse Peso >= 100) Then
                Quadralia.Aplicacion.MostrarMessageBox("El peso del empaquetado Makro debe estar entre 0 y 99,99 Kg")
                Return
            End If
            Formulario = Quadralia.Aplicacion.AbrirInforme(Nothing, frmVisorInforme.Informe.EmpaquetadoMakro, Quadralia.Aplicacion.Permisos.EMPAQUETADO)
        Else
            Formulario = Quadralia.Aplicacion.AbrirInforme(Nothing, frmVisorInforme.Informe.Empaquetado, Quadralia.Aplicacion.Permisos.EMPAQUETADO, Nothing, False)
        end if
#End If
        If Formulario Is Nothing Then Exit Sub

        Formulario.Datos = Empaquetados
        Formulario.RefrescarDatos()

#If Not DEBUG Then
        Formulario.Imprimir(cConfiguracionAplicacion.Instancia.Impresora, cConfiguracionAplicacion.Instancia.Orientacion, cConfiguracionAplicacion.Instancia.Formato)
        Formulario.Close()
#End If
    End Sub


#End Region
#Region " IMPRIMIR ETIQUETAS "
    ''' <summary>
    ''' Imprime el registro actual
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ImprimirEtiquetas(ByVal Informe As cInforme)
        ' Validaciones
        If Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera Then Exit Sub
        If Me.Registro Is Nothing Then Exit Sub
        If Me.Registro.Etiquetas.Count = 0 Then Exit Sub
        If Me.dgvEtiquetas.SelectedRows.Count = 0 Then Exit Sub

        Dim Etiquetas As List(Of Etiqueta) = (From fil As DataGridViewRow In dgvEtiquetas.SelectedRows Where fil.DataBoundItem IsNot Nothing Select DirectCast(fil.DataBoundItem, Etiqueta)).ToList

        ' Imprimo la lista
        frmGestionEtiquetas.ImprimirEtiquetas(Etiquetas, Informe)
    End Sub


#End Region
End Class
