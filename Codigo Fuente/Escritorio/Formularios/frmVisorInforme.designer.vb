<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVisorInforme
    Inherits FormularioHijo

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmVisorInforme))
        Me.grpCabecera = New ComponentFactory.Krypton.Toolkit.KryptonGroup()
        Me.lblTitulo = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.rptVisor = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        CType(Me.grpCabecera, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpCabecera.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpCabecera.Panel.SuspendLayout()
        Me.grpCabecera.SuspendLayout()
        Me.SuspendLayout()
        '
        'grpCabecera
        '
        resources.ApplyResources(Me.grpCabecera, "grpCabecera")
        Me.grpCabecera.GroupBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.PanelCustom1
        Me.grpCabecera.Name = "grpCabecera"
        '
        'grpCabecera.Panel
        '
        Me.grpCabecera.Panel.Controls.Add(Me.lblTitulo)
        Me.grpCabecera.StateCommon.Border.Color1 = System.Drawing.Color.DarkOrange
        Me.grpCabecera.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom
        Me.grpCabecera.StateCommon.Border.Width = 5
        '
        'lblTitulo
        '
        Me.lblTitulo.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.Custom1
        resources.ApplyResources(Me.lblTitulo, "lblTitulo")
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Values.Image = Global.Escritorio.My.Resources.Resources.Informe_32
        Me.lblTitulo.Values.Text = resources.GetString("lblTitulo.Values.Text")
        '
        'rptVisor
        '
        Me.rptVisor.ActiveViewIndex = -1
        Me.rptVisor.AutoValidate = System.Windows.Forms.AutoValidate.Disable
        Me.rptVisor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.rptVisor.Cursor = System.Windows.Forms.Cursors.Default
        resources.ApplyResources(Me.rptVisor, "rptVisor")
        Me.rptVisor.EnableToolTips = False
        Me.rptVisor.Name = "rptVisor"
        '
        'frmVisorInformeSimple
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.rptVisor)
        Me.Controls.Add(Me.grpCabecera)
        Me.Name = "frmVisorInformeSimple"
        CType(Me.grpCabecera.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpCabecera.Panel.ResumeLayout(False)
        Me.grpCabecera.Panel.PerformLayout()
        CType(Me.grpCabecera, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpCabecera.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
    Private WithEvents rptVisor As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Private WithEvents grpCabecera As ComponentFactory.Krypton.Toolkit.KryptonGroup
    Private WithEvents lblTitulo As ComponentFactory.Krypton.Toolkit.KryptonLabel
End Class
