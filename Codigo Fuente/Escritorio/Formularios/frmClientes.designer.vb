<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmClientes
    Inherits Escritorio.FormularioHijo

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Dise�ador de Windows Forms.  
    'No lo modifique con el editor de c�digo.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmClientes))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.btnBBuscar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.lblBUsuario = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblBEmail = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtBCif = New Escritorio.Quadralia.Controles.aTextBox()
        Me.grpResultados = New ComponentFactory.Krypton.Toolkit.KryptonGroupBox()
        Me.dgvResultadosBuscador = New ComponentFactory.Krypton.Toolkit.KryptonDataGridView()
        Me.colCodigo = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colRazonSocial = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.splSeparador = New ComponentFactory.Krypton.Toolkit.KryptonSplitContainer()
        Me.hdrBusqueda = New ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup()
        Me.btnOcultarBusqueda = New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup()
        Me.tblOrganizadorBusqueda = New System.Windows.Forms.TableLayoutPanel()
        Me.btnLimpiarBusqueda = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.lblBCodigoDe = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtBNombre = New Escritorio.Quadralia.Controles.aTextBox()
        Me.lblBCodigoA = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.chkBEliminados = New ComponentFactory.Krypton.Toolkit.KryptonCheckBox()
        Me.txtBCodigoDe = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtBCodigoA = New Escritorio.Quadralia.Controles.aTextBox()
        Me.tabGeneral = New ComponentFactory.Krypton.Navigator.KryptonNavigator()
        Me.tbpDatosContacto = New ComponentFactory.Krypton.Navigator.KryptonPage()
        Me.tblOrganizadorDatos = New System.Windows.Forms.TableLayoutPanel()
        Me.lblRazonSocial = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblNombreComercial = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblFax = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblTelefono1 = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblTelefono2 = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtRazonSocial = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtNombreComercial = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtTelefono1 = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtTelefono2 = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtFax = New Escritorio.Quadralia.Controles.aTextBox()
        Me.chkReactivarRegistro = New ComponentFactory.Krypton.Toolkit.KryptonCheckBox()
        Me.txtEmail = New Escritorio.Quadralia.Controles.aTextBox()
        Me.lblEmail = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblWeb = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtWeb = New Escritorio.Quadralia.Controles.aTextBox()
        Me.lblAlerta = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtAlerta = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtCIF = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtCodigo = New Escritorio.Quadralia.Controles.aTextBox()
        Me.lblCIF = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblCodigo = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.tbpDirecciones = New ComponentFactory.Krypton.Navigator.KryptonPage()
        Me.hdrDirecciones = New ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup()
        Me.btnNuevaDireccion = New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup()
        Me.btnEliminarDireccion = New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup()
        Me.vsrDirecciones = New Escritorio.ListaVisores()
        Me.tbpContactos = New ComponentFactory.Krypton.Navigator.KryptonPage()
        Me.hdrContactos = New ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup()
        Me.btnNuevoContacto = New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup()
        Me.btnEliminarContacto = New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup()
        Me.vsrContactos = New Escritorio.ListaVisores()
        Me.tbpObservaciones = New ComponentFactory.Krypton.Navigator.KryptonPage()
        Me.htmlObservaciones = New Escritorio.EditorHTML()
        Me.tbpLotesSalida = New ComponentFactory.Krypton.Navigator.KryptonPage()
        Me.splSeparadorAlbaranesSalida = New ComponentFactory.Krypton.Toolkit.KryptonSplitContainer()
        Me.hdrAlbaranes = New ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup()
        Me.btnOcultarFiltroAlbaranes = New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup()
        Me.tblFiltrosAlbaranes = New System.Windows.Forms.TableLayoutPanel()
        Me.lblFiltroSalidaCodigoDesde = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblFiltroSalidaCodigoHasta = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblFiltroSalidaFechaDesde = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblFiltroSalidaFechaHasta = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtFiltroSalidaCodigoDesde = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtFiltroSalidaCodigoHasta = New Escritorio.Quadralia.Controles.aTextBox()
        Me.dtpFiltroSalidaFechaDesde = New Escritorio.Quadralia.Controles.aDateTimePicker()
        Me.dtpFiltroSalidaFechaHasta = New Escritorio.Quadralia.Controles.aDateTimePicker()
        Me.dgvAlbaranesSalida = New ComponentFactory.Krypton.Toolkit.KryptonDataGridView()
        Me.colLoteSalidaCodigo = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colLoteSalidaFecha = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colLoteSalidaExpedidor = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.lblTitulo = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.grpCabecera = New ComponentFactory.Krypton.Toolkit.KryptonGroup()
        Me.epErrores = New Escritorio.Quadralia.Controles.Validacion.GestorErrores()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        CType(Me.grpResultados, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpResultados.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpResultados.Panel.SuspendLayout()
        Me.grpResultados.SuspendLayout()
        CType(Me.dgvResultadosBuscador, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.splSeparador, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.splSeparador.Panel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.splSeparador.Panel1.SuspendLayout()
        CType(Me.splSeparador.Panel2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.splSeparador.Panel2.SuspendLayout()
        Me.splSeparador.SuspendLayout()
        CType(Me.hdrBusqueda, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.hdrBusqueda.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.hdrBusqueda.Panel.SuspendLayout()
        Me.hdrBusqueda.SuspendLayout()
        Me.tblOrganizadorBusqueda.SuspendLayout()
        CType(Me.tabGeneral, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabGeneral.SuspendLayout()
        CType(Me.tbpDatosContacto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpDatosContacto.SuspendLayout()
        Me.tblOrganizadorDatos.SuspendLayout()
        CType(Me.tbpDirecciones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpDirecciones.SuspendLayout()
        CType(Me.hdrDirecciones, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.hdrDirecciones.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.hdrDirecciones.Panel.SuspendLayout()
        Me.hdrDirecciones.SuspendLayout()
        CType(Me.tbpContactos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpContactos.SuspendLayout()
        CType(Me.hdrContactos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.hdrContactos.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.hdrContactos.Panel.SuspendLayout()
        Me.hdrContactos.SuspendLayout()
        CType(Me.tbpObservaciones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpObservaciones.SuspendLayout()
        CType(Me.tbpLotesSalida, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpLotesSalida.SuspendLayout()
        CType(Me.splSeparadorAlbaranesSalida, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.splSeparadorAlbaranesSalida.Panel1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.splSeparadorAlbaranesSalida.Panel1.SuspendLayout()
        CType(Me.splSeparadorAlbaranesSalida.Panel2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.splSeparadorAlbaranesSalida.Panel2.SuspendLayout()
        Me.splSeparadorAlbaranesSalida.SuspendLayout()
        CType(Me.hdrAlbaranes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.hdrAlbaranes.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.hdrAlbaranes.Panel.SuspendLayout()
        Me.hdrAlbaranes.SuspendLayout()
        Me.tblFiltrosAlbaranes.SuspendLayout()
        CType(Me.dgvAlbaranesSalida, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpCabecera, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpCabecera.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpCabecera.Panel.SuspendLayout()
        Me.grpCabecera.SuspendLayout()
        CType(Me.epErrores, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnBBuscar
        '
        Me.tblOrganizadorBusqueda.SetColumnSpan(Me.btnBBuscar, 4)
        Me.btnBBuscar.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnBBuscar.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnBBuscar.Location = New System.Drawing.Point(3, 135)
        Me.btnBBuscar.Name = "btnBBuscar"
        Me.btnBBuscar.Size = New System.Drawing.Size(219, 25)
        Me.btnBBuscar.TabIndex = 9
        Me.ToolTip1.SetToolTip(Me.btnBBuscar, "Realice la b�squeda de proveedores con los filtros establecidos")
        Me.btnBBuscar.Values.Image = CType(resources.GetObject("btnBBuscar.Values.Image"), System.Drawing.Image)
        Me.btnBBuscar.Values.Text = "Buscar"
        '
        'lblBUsuario
        '
        Me.lblBUsuario.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblBUsuario.Location = New System.Drawing.Point(14, 83)
        Me.lblBUsuario.Name = "lblBUsuario"
        Me.lblBUsuario.Size = New System.Drawing.Size(56, 20)
        Me.lblBUsuario.TabIndex = 6
        Me.lblBUsuario.Values.Text = "Nombre"
        '
        'lblBEmail
        '
        Me.lblBEmail.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblBEmail.Location = New System.Drawing.Point(43, 57)
        Me.lblBEmail.Name = "lblBEmail"
        Me.lblBEmail.Size = New System.Drawing.Size(27, 20)
        Me.lblBEmail.TabIndex = 4
        Me.lblBEmail.Values.Text = "CIF"
        '
        'txtBCif
        '
        Me.txtBCif.AlwaysActive = False
        Me.tblOrganizadorBusqueda.SetColumnSpan(Me.txtBCif, 3)
        Me.txtBCif.controlarBotonBorrar = True
        Me.txtBCif.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtBCif.Formato = ""
        Me.txtBCif.Location = New System.Drawing.Point(76, 57)
        Me.txtBCif.MaxLength = 255
        Me.txtBCif.mostrarSiempreBotonBorrar = False
        Me.txtBCif.Name = "txtBCif"
        Me.txtBCif.seleccionarTodo = True
        Me.txtBCif.Size = New System.Drawing.Size(146, 20)
        Me.txtBCif.TabIndex = 5
        Me.ToolTip1.SetToolTip(Me.txtBCif, "Introduzca el CIF/VAT del proveedor para realizar el filtrado" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10))
        '
        'grpResultados
        '
        Me.tblOrganizadorBusqueda.SetColumnSpan(Me.grpResultados, 4)
        Me.grpResultados.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grpResultados.Location = New System.Drawing.Point(3, 166)
        Me.grpResultados.Name = "grpResultados"
        '
        'grpResultados.Panel
        '
        Me.grpResultados.Panel.Controls.Add(Me.dgvResultadosBuscador)
        Me.grpResultados.Size = New System.Drawing.Size(219, 151)
        Me.grpResultados.TabIndex = 10
        Me.grpResultados.Text = "Resultados"
        Me.grpResultados.Values.Heading = "Resultados"
        '
        'dgvResultadosBuscador
        '
        Me.dgvResultadosBuscador.AllowUserToAddRows = False
        Me.dgvResultadosBuscador.AllowUserToDeleteRows = False
        Me.dgvResultadosBuscador.AllowUserToResizeRows = False
        Me.dgvResultadosBuscador.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colCodigo, Me.colRazonSocial})
        Me.dgvResultadosBuscador.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvResultadosBuscador.Location = New System.Drawing.Point(0, 0)
        Me.dgvResultadosBuscador.MultiSelect = False
        Me.dgvResultadosBuscador.Name = "dgvResultadosBuscador"
        Me.dgvResultadosBuscador.ReadOnly = True
        Me.dgvResultadosBuscador.RowHeadersVisible = False
        Me.dgvResultadosBuscador.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvResultadosBuscador.Size = New System.Drawing.Size(215, 127)
        Me.dgvResultadosBuscador.TabIndex = 0
        Me.ToolTip1.SetToolTip(Me.dgvResultadosBuscador, "Resultado de la b�squeda de proveedores")
        '
        'colCodigo
        '
        Me.colCodigo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colCodigo.DataPropertyName = "codigo"
        Me.colCodigo.HeaderText = "C�d."
        Me.colCodigo.Name = "colCodigo"
        Me.colCodigo.ReadOnly = True
        Me.colCodigo.Width = 61
        '
        'colRazonSocial
        '
        Me.colRazonSocial.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colRazonSocial.DataPropertyName = "razonSocial"
        Me.colRazonSocial.HeaderText = "Raz�n Social"
        Me.colRazonSocial.Name = "colRazonSocial"
        Me.colRazonSocial.ReadOnly = True
        Me.colRazonSocial.Width = 153
        '
        'splSeparador
        '
        Me.splSeparador.Cursor = System.Windows.Forms.Cursors.Default
        Me.splSeparador.Dock = System.Windows.Forms.DockStyle.Fill
        Me.splSeparador.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.splSeparador.Location = New System.Drawing.Point(0, 57)
        Me.splSeparador.Name = "splSeparador"
        '
        'splSeparador.Panel1
        '
        Me.splSeparador.Panel1.Controls.Add(Me.hdrBusqueda)
        '
        'splSeparador.Panel2
        '
        Me.splSeparador.Panel2.Controls.Add(Me.tabGeneral)
        Me.splSeparador.Size = New System.Drawing.Size(810, 357)
        Me.splSeparador.SplitterDistance = 227
        Me.splSeparador.TabIndex = 1
        '
        'hdrBusqueda
        '
        Me.hdrBusqueda.AutoSize = True
        Me.hdrBusqueda.ButtonSpecs.AddRange(New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup() {Me.btnOcultarBusqueda})
        Me.hdrBusqueda.Dock = System.Windows.Forms.DockStyle.Fill
        Me.hdrBusqueda.HeaderVisibleSecondary = False
        Me.hdrBusqueda.Location = New System.Drawing.Point(0, 0)
        Me.hdrBusqueda.Name = "hdrBusqueda"
        '
        'hdrBusqueda.Panel
        '
        Me.hdrBusqueda.Panel.Controls.Add(Me.tblOrganizadorBusqueda)
        Me.hdrBusqueda.Size = New System.Drawing.Size(227, 357)
        Me.hdrBusqueda.TabIndex = 0
        Me.ToolTip1.SetToolTip(Me.hdrBusqueda, "Configure los distintos filtros de b�squeda para localizar el elemento deseado")
        Me.hdrBusqueda.ValuesPrimary.Heading = "Buscar"
        Me.hdrBusqueda.ValuesPrimary.Image = CType(resources.GetObject("hdrBusqueda.ValuesPrimary.Image"), System.Drawing.Image)
        '
        'btnOcultarBusqueda
        '
        Me.btnOcultarBusqueda.Type = ComponentFactory.Krypton.Toolkit.PaletteButtonSpecStyle.ArrowLeft
        Me.btnOcultarBusqueda.UniqueName = "BE70F7722CF94C60618F65819BBF010D"
        '
        'tblOrganizadorBusqueda
        '
        Me.tblOrganizadorBusqueda.BackColor = System.Drawing.Color.Transparent
        Me.tblOrganizadorBusqueda.ColumnCount = 4
        Me.tblOrganizadorBusqueda.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblOrganizadorBusqueda.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblOrganizadorBusqueda.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblOrganizadorBusqueda.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblOrganizadorBusqueda.Controls.Add(Me.btnLimpiarBusqueda, 0, 0)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.txtBCif, 1, 2)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.lblBEmail, 0, 2)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.btnBBuscar, 0, 5)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.grpResultados, 0, 6)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.lblBCodigoDe, 0, 1)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.lblBUsuario, 0, 3)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.txtBNombre, 1, 3)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.lblBCodigoA, 2, 1)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.chkBEliminados, 0, 4)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.txtBCodigoDe, 1, 1)
        Me.tblOrganizadorBusqueda.Controls.Add(Me.txtBCodigoA, 3, 1)
        Me.tblOrganizadorBusqueda.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblOrganizadorBusqueda.Location = New System.Drawing.Point(0, 0)
        Me.tblOrganizadorBusqueda.Name = "tblOrganizadorBusqueda"
        Me.tblOrganizadorBusqueda.RowCount = 7
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorBusqueda.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorBusqueda.Size = New System.Drawing.Size(225, 320)
        Me.tblOrganizadorBusqueda.TabIndex = 0
        '
        'btnLimpiarBusqueda
        '
        Me.btnLimpiarBusqueda.AutoSize = True
        Me.btnLimpiarBusqueda.ButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.ButtonSpec
        Me.tblOrganizadorBusqueda.SetColumnSpan(Me.btnLimpiarBusqueda, 4)
        Me.btnLimpiarBusqueda.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnLimpiarBusqueda.Location = New System.Drawing.Point(97, 3)
        Me.btnLimpiarBusqueda.Name = "btnLimpiarBusqueda"
        Me.btnLimpiarBusqueda.Size = New System.Drawing.Size(125, 22)
        Me.btnLimpiarBusqueda.TabIndex = 11
        Me.btnLimpiarBusqueda.Values.Image = Global.Escritorio.My.Resources.Resources.Limpiar_16
        Me.btnLimpiarBusqueda.Values.Text = "Limpiar B�squeda"
        '
        'lblBCodigoDe
        '
        Me.lblBCodigoDe.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblBCodigoDe.Location = New System.Drawing.Point(3, 31)
        Me.lblBCodigoDe.Name = "lblBCodigoDe"
        Me.lblBCodigoDe.Size = New System.Drawing.Size(67, 20)
        Me.lblBCodigoDe.TabIndex = 0
        Me.lblBCodigoDe.Values.Text = "Codigo de"
        '
        'txtBNombre
        '
        Me.txtBNombre.AlwaysActive = False
        Me.tblOrganizadorBusqueda.SetColumnSpan(Me.txtBNombre, 3)
        Me.txtBNombre.controlarBotonBorrar = True
        Me.txtBNombre.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtBNombre.Formato = ""
        Me.txtBNombre.Location = New System.Drawing.Point(76, 83)
        Me.txtBNombre.MaxLength = 255
        Me.txtBNombre.mostrarSiempreBotonBorrar = False
        Me.txtBNombre.Name = "txtBNombre"
        Me.txtBNombre.seleccionarTodo = True
        Me.txtBNombre.Size = New System.Drawing.Size(146, 20)
        Me.txtBNombre.TabIndex = 7
        Me.ToolTip1.SetToolTip(Me.txtBNombre, "Introduzca el nombre comercial o raz�n social del proveedor para filtrar los resu" & _
                "ltados")
        '
        'lblBCodigoA
        '
        Me.lblBCodigoA.Location = New System.Drawing.Point(140, 31)
        Me.lblBCodigoA.Name = "lblBCodigoA"
        Me.lblBCodigoA.Size = New System.Drawing.Size(17, 20)
        Me.lblBCodigoA.TabIndex = 2
        Me.lblBCodigoA.Values.Text = "a"
        '
        'chkBEliminados
        '
        Me.tblOrganizadorBusqueda.SetColumnSpan(Me.chkBEliminados, 4)
        Me.chkBEliminados.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl
        Me.chkBEliminados.Location = New System.Drawing.Point(3, 109)
        Me.chkBEliminados.Name = "chkBEliminados"
        Me.chkBEliminados.Size = New System.Drawing.Size(183, 20)
        Me.chkBEliminados.TabIndex = 8
        Me.chkBEliminados.Text = "Incluir los clientes eliminados"
        Me.ToolTip1.SetToolTip(Me.chkBEliminados, "Incluya los proveedores eliminados en los resultados de la b�squeda")
        Me.chkBEliminados.Values.Text = "Incluir los clientes eliminados"
        '
        'txtBCodigoDe
        '
        Me.txtBCodigoDe.AlwaysActive = False
        Me.txtBCodigoDe.controlarBotonBorrar = True
        Me.txtBCodigoDe.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtBCodigoDe.Formato = ""
        Me.txtBCodigoDe.Location = New System.Drawing.Point(76, 31)
        Me.txtBCodigoDe.MaxLength = 255
        Me.txtBCodigoDe.mostrarSiempreBotonBorrar = False
        Me.txtBCodigoDe.Name = "txtBCodigoDe"
        Me.txtBCodigoDe.seleccionarTodo = True
        Me.txtBCodigoDe.Size = New System.Drawing.Size(58, 20)
        Me.txtBCodigoDe.TabIndex = 1
        '
        'txtBCodigoA
        '
        Me.txtBCodigoA.AlwaysActive = False
        Me.txtBCodigoA.controlarBotonBorrar = True
        Me.txtBCodigoA.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtBCodigoA.Formato = ""
        Me.txtBCodigoA.Location = New System.Drawing.Point(163, 31)
        Me.txtBCodigoA.MaxLength = 255
        Me.txtBCodigoA.mostrarSiempreBotonBorrar = False
        Me.txtBCodigoA.Name = "txtBCodigoA"
        Me.txtBCodigoA.seleccionarTodo = True
        Me.txtBCodigoA.Size = New System.Drawing.Size(59, 20)
        Me.txtBCodigoA.TabIndex = 3
        '
        'tabGeneral
        '
        Me.tabGeneral.AllowPageReorder = False
        Me.tabGeneral.Button.CloseButtonAction = ComponentFactory.Krypton.Navigator.CloseButtonAction.None
        Me.tabGeneral.Button.CloseButtonDisplay = ComponentFactory.Krypton.Navigator.ButtonDisplay.Hide
        Me.tabGeneral.Button.CloseButtonShortcut = System.Windows.Forms.Keys.None
        Me.tabGeneral.Button.ContextButtonAction = ComponentFactory.Krypton.Navigator.ContextButtonAction.None
        Me.tabGeneral.Button.ContextButtonDisplay = ComponentFactory.Krypton.Navigator.ButtonDisplay.Hide
        Me.tabGeneral.Button.ContextButtonShortcut = System.Windows.Forms.Keys.None
        Me.tabGeneral.Button.NextButtonAction = ComponentFactory.Krypton.Navigator.DirectionButtonAction.None
        Me.tabGeneral.Button.NextButtonShortcut = System.Windows.Forms.Keys.None
        Me.tabGeneral.Button.PreviousButtonAction = ComponentFactory.Krypton.Navigator.DirectionButtonAction.None
        Me.tabGeneral.Button.PreviousButtonShortcut = System.Windows.Forms.Keys.None
        Me.tabGeneral.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabGeneral.Location = New System.Drawing.Point(0, 0)
        Me.tabGeneral.Name = "tabGeneral"
        Me.tabGeneral.Pages.AddRange(New ComponentFactory.Krypton.Navigator.KryptonPage() {Me.tbpDatosContacto, Me.tbpDirecciones, Me.tbpContactos, Me.tbpObservaciones, Me.tbpLotesSalida})
        Me.tabGeneral.SelectedIndex = 0
        Me.tabGeneral.Size = New System.Drawing.Size(578, 357)
        Me.tabGeneral.TabIndex = 0
        Me.tabGeneral.Text = "KryptonNavigator1"
        '
        'tbpDatosContacto
        '
        Me.tbpDatosContacto.AutoHiddenSlideSize = New System.Drawing.Size(200, 200)
        Me.tbpDatosContacto.Controls.Add(Me.tblOrganizadorDatos)
        Me.tbpDatosContacto.Flags = 65534
        Me.tbpDatosContacto.LastVisibleSet = True
        Me.tbpDatosContacto.MinimumSize = New System.Drawing.Size(50, 50)
        Me.tbpDatosContacto.Name = "tbpDatosContacto"
        Me.tbpDatosContacto.Size = New System.Drawing.Size(576, 330)
        Me.tbpDatosContacto.Text = "Datos"
        Me.tbpDatosContacto.ToolTipTitle = "Page ToolTip"
        Me.tbpDatosContacto.UniqueName = "D3D995E6D5024C0241B10034A07A28A8"
        '
        'tblOrganizadorDatos
        '
        Me.tblOrganizadorDatos.BackColor = System.Drawing.Color.Transparent
        Me.tblOrganizadorDatos.ColumnCount = 6
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOrganizadorDatos.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblOrganizadorDatos.Controls.Add(Me.lblRazonSocial, 0, 1)
        Me.tblOrganizadorDatos.Controls.Add(Me.lblNombreComercial, 0, 2)
        Me.tblOrganizadorDatos.Controls.Add(Me.lblFax, 0, 5)
        Me.tblOrganizadorDatos.Controls.Add(Me.lblTelefono1, 0, 4)
        Me.tblOrganizadorDatos.Controls.Add(Me.lblTelefono2, 3, 4)
        Me.tblOrganizadorDatos.Controls.Add(Me.txtRazonSocial, 2, 1)
        Me.tblOrganizadorDatos.Controls.Add(Me.txtNombreComercial, 2, 2)
        Me.tblOrganizadorDatos.Controls.Add(Me.txtTelefono1, 2, 4)
        Me.tblOrganizadorDatos.Controls.Add(Me.txtTelefono2, 5, 4)
        Me.tblOrganizadorDatos.Controls.Add(Me.txtFax, 2, 5)
        Me.tblOrganizadorDatos.Controls.Add(Me.chkReactivarRegistro, 0, 11)
        Me.tblOrganizadorDatos.Controls.Add(Me.txtEmail, 5, 5)
        Me.tblOrganizadorDatos.Controls.Add(Me.lblEmail, 3, 5)
        Me.tblOrganizadorDatos.Controls.Add(Me.lblWeb, 0, 9)
        Me.tblOrganizadorDatos.Controls.Add(Me.txtWeb, 2, 9)
        Me.tblOrganizadorDatos.Controls.Add(Me.lblAlerta, 0, 10)
        Me.tblOrganizadorDatos.Controls.Add(Me.txtAlerta, 2, 10)
        Me.tblOrganizadorDatos.Controls.Add(Me.txtCIF, 5, 0)
        Me.tblOrganizadorDatos.Controls.Add(Me.txtCodigo, 2, 0)
        Me.tblOrganizadorDatos.Controls.Add(Me.lblCIF, 3, 0)
        Me.tblOrganizadorDatos.Controls.Add(Me.lblCodigo, 0, 0)
        Me.tblOrganizadorDatos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblOrganizadorDatos.Location = New System.Drawing.Point(0, 0)
        Me.tblOrganizadorDatos.Name = "tblOrganizadorDatos"
        Me.tblOrganizadorDatos.RowCount = 13
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizadorDatos.Size = New System.Drawing.Size(576, 330)
        Me.tblOrganizadorDatos.TabIndex = 0
        '
        'lblRazonSocial
        '
        Me.lblRazonSocial.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblRazonSocial.Location = New System.Drawing.Point(38, 29)
        Me.lblRazonSocial.Name = "lblRazonSocial"
        Me.lblRazonSocial.Size = New System.Drawing.Size(78, 20)
        Me.lblRazonSocial.TabIndex = 4
        Me.lblRazonSocial.Values.Text = "Raz�n social"
        '
        'lblNombreComercial
        '
        Me.lblNombreComercial.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblNombreComercial.Location = New System.Drawing.Point(5, 55)
        Me.lblNombreComercial.Name = "lblNombreComercial"
        Me.lblNombreComercial.Size = New System.Drawing.Size(111, 20)
        Me.lblNombreComercial.TabIndex = 6
        Me.lblNombreComercial.Values.Text = "Nombre comercial"
        '
        'lblFax
        '
        Me.lblFax.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblFax.Location = New System.Drawing.Point(88, 107)
        Me.lblFax.Name = "lblFax"
        Me.lblFax.Size = New System.Drawing.Size(28, 20)
        Me.lblFax.TabIndex = 12
        Me.lblFax.Values.Text = "Fax"
        '
        'lblTelefono1
        '
        Me.lblTelefono1.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblTelefono1.Location = New System.Drawing.Point(48, 81)
        Me.lblTelefono1.Name = "lblTelefono1"
        Me.lblTelefono1.Size = New System.Drawing.Size(68, 20)
        Me.lblTelefono1.TabIndex = 8
        Me.lblTelefono1.Values.Text = "Tel�fono 1"
        '
        'lblTelefono2
        '
        Me.lblTelefono2.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblTelefono2.Location = New System.Drawing.Point(335, 81)
        Me.lblTelefono2.Name = "lblTelefono2"
        Me.lblTelefono2.Size = New System.Drawing.Size(68, 20)
        Me.lblTelefono2.TabIndex = 10
        Me.lblTelefono2.Values.Text = "Tel�fono 2"
        '
        'txtRazonSocial
        '
        Me.txtRazonSocial.AlwaysActive = False
        Me.tblOrganizadorDatos.SetColumnSpan(Me.txtRazonSocial, 4)
        Me.txtRazonSocial.controlarBotonBorrar = True
        Me.txtRazonSocial.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtRazonSocial.Formato = ""
        Me.txtRazonSocial.Location = New System.Drawing.Point(142, 29)
        Me.txtRazonSocial.MaxLength = 255
        Me.txtRazonSocial.mostrarSiempreBotonBorrar = False
        Me.txtRazonSocial.Name = "txtRazonSocial"
        Me.txtRazonSocial.seleccionarTodo = True
        Me.txtRazonSocial.Size = New System.Drawing.Size(431, 20)
        Me.txtRazonSocial.TabIndex = 5
        '
        'txtNombreComercial
        '
        Me.txtNombreComercial.AlwaysActive = False
        Me.tblOrganizadorDatos.SetColumnSpan(Me.txtNombreComercial, 4)
        Me.txtNombreComercial.controlarBotonBorrar = True
        Me.txtNombreComercial.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtNombreComercial.Formato = ""
        Me.txtNombreComercial.Location = New System.Drawing.Point(142, 55)
        Me.txtNombreComercial.MaxLength = 255
        Me.txtNombreComercial.mostrarSiempreBotonBorrar = False
        Me.txtNombreComercial.Name = "txtNombreComercial"
        Me.txtNombreComercial.seleccionarTodo = True
        Me.txtNombreComercial.Size = New System.Drawing.Size(431, 20)
        Me.txtNombreComercial.TabIndex = 7
        '
        'txtTelefono1
        '
        Me.txtTelefono1.AlwaysActive = False
        Me.txtTelefono1.controlarBotonBorrar = True
        Me.txtTelefono1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtTelefono1.Formato = ""
        Me.txtTelefono1.Location = New System.Drawing.Point(142, 81)
        Me.txtTelefono1.MaxLength = 32
        Me.txtTelefono1.mostrarSiempreBotonBorrar = False
        Me.txtTelefono1.Name = "txtTelefono1"
        Me.txtTelefono1.seleccionarTodo = True
        Me.txtTelefono1.Size = New System.Drawing.Size(144, 20)
        Me.txtTelefono1.TabIndex = 9
        '
        'txtTelefono2
        '
        Me.txtTelefono2.AlwaysActive = False
        Me.txtTelefono2.controlarBotonBorrar = True
        Me.txtTelefono2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtTelefono2.Formato = ""
        Me.txtTelefono2.Location = New System.Drawing.Point(429, 81)
        Me.txtTelefono2.MaxLength = 32
        Me.txtTelefono2.mostrarSiempreBotonBorrar = False
        Me.txtTelefono2.Name = "txtTelefono2"
        Me.txtTelefono2.seleccionarTodo = True
        Me.txtTelefono2.Size = New System.Drawing.Size(144, 20)
        Me.txtTelefono2.TabIndex = 11
        '
        'txtFax
        '
        Me.txtFax.AlwaysActive = False
        Me.txtFax.controlarBotonBorrar = True
        Me.txtFax.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtFax.Formato = ""
        Me.txtFax.Location = New System.Drawing.Point(142, 107)
        Me.txtFax.MaxLength = 32
        Me.txtFax.mostrarSiempreBotonBorrar = False
        Me.txtFax.Name = "txtFax"
        Me.txtFax.seleccionarTodo = True
        Me.txtFax.Size = New System.Drawing.Size(144, 20)
        Me.txtFax.TabIndex = 13
        '
        'chkReactivarRegistro
        '
        Me.tblOrganizadorDatos.SetColumnSpan(Me.chkReactivarRegistro, 4)
        Me.chkReactivarRegistro.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl
        Me.chkReactivarRegistro.Location = New System.Drawing.Point(3, 185)
        Me.chkReactivarRegistro.Name = "chkReactivarRegistro"
        Me.chkReactivarRegistro.Size = New System.Drawing.Size(125, 20)
        Me.chkReactivarRegistro.TabIndex = 20
        Me.chkReactivarRegistro.Text = "Reactivar el cliente"
        Me.ToolTip1.SetToolTip(Me.chkReactivarRegistro, "Vuelve a activar el cliente en la base de datos")
        Me.chkReactivarRegistro.Values.Text = "Reactivar el cliente"
        Me.chkReactivarRegistro.Visible = False
        '
        'txtEmail
        '
        Me.txtEmail.AlwaysActive = False
        Me.txtEmail.controlarBotonBorrar = True
        Me.txtEmail.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtEmail.Formato = ""
        Me.txtEmail.Location = New System.Drawing.Point(429, 107)
        Me.txtEmail.MaxLength = 255
        Me.txtEmail.mostrarSiempreBotonBorrar = False
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.seleccionarTodo = True
        Me.txtEmail.Size = New System.Drawing.Size(144, 20)
        Me.txtEmail.TabIndex = 15
        '
        'lblEmail
        '
        Me.lblEmail.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblEmail.Location = New System.Drawing.Point(292, 107)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(111, 20)
        Me.lblEmail.TabIndex = 14
        Me.lblEmail.Values.Text = "Correo electr�nico"
        '
        'lblWeb
        '
        Me.lblWeb.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblWeb.Location = New System.Drawing.Point(40, 133)
        Me.lblWeb.Name = "lblWeb"
        Me.lblWeb.Size = New System.Drawing.Size(76, 20)
        Me.lblWeb.TabIndex = 16
        Me.lblWeb.Values.Text = "P�gina Web"
        '
        'txtWeb
        '
        Me.txtWeb.AlwaysActive = False
        Me.tblOrganizadorDatos.SetColumnSpan(Me.txtWeb, 4)
        Me.txtWeb.controlarBotonBorrar = True
        Me.txtWeb.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtWeb.Formato = ""
        Me.txtWeb.Location = New System.Drawing.Point(142, 133)
        Me.txtWeb.MaxLength = 255
        Me.txtWeb.mostrarSiempreBotonBorrar = False
        Me.txtWeb.Name = "txtWeb"
        Me.txtWeb.seleccionarTodo = True
        Me.txtWeb.Size = New System.Drawing.Size(431, 20)
        Me.txtWeb.TabIndex = 17
        '
        'lblAlerta
        '
        Me.lblAlerta.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblAlerta.Location = New System.Drawing.Point(3, 159)
        Me.lblAlerta.Name = "lblAlerta"
        Me.lblAlerta.Size = New System.Drawing.Size(113, 20)
        Me.lblAlerta.TabIndex = 21
        Me.lblAlerta.Values.Text = "Notificaci�n  alerta"
        '
        'txtAlerta
        '
        Me.txtAlerta.AlwaysActive = False
        Me.tblOrganizadorDatos.SetColumnSpan(Me.txtAlerta, 4)
        Me.txtAlerta.controlarBotonBorrar = True
        Me.txtAlerta.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtAlerta.Formato = ""
        Me.txtAlerta.Location = New System.Drawing.Point(142, 159)
        Me.txtAlerta.MaxLength = 255
        Me.txtAlerta.mostrarSiempreBotonBorrar = False
        Me.txtAlerta.Name = "txtAlerta"
        Me.txtAlerta.seleccionarTodo = True
        Me.txtAlerta.Size = New System.Drawing.Size(431, 20)
        Me.txtAlerta.TabIndex = 17
        '
        'txtCIF
        '
        Me.txtCIF.AlwaysActive = False
        Me.txtCIF.controlarBotonBorrar = True
        Me.txtCIF.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtCIF.Formato = ""
        Me.txtCIF.Location = New System.Drawing.Point(429, 3)
        Me.txtCIF.MaxLength = 32
        Me.txtCIF.mostrarSiempreBotonBorrar = False
        Me.txtCIF.Name = "txtCIF"
        Me.txtCIF.seleccionarTodo = True
        Me.txtCIF.Size = New System.Drawing.Size(144, 20)
        Me.txtCIF.TabIndex = 1
        '
        'txtCodigo
        '
        Me.txtCodigo.AlwaysActive = False
        Me.txtCodigo.controlarBotonBorrar = True
        Me.txtCodigo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtCodigo.Enabled = False
        Me.txtCodigo.Formato = ""
        Me.txtCodigo.Location = New System.Drawing.Point(142, 3)
        Me.txtCodigo.MaxLength = 16
        Me.txtCodigo.mostrarSiempreBotonBorrar = False
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.seleccionarTodo = True
        Me.txtCodigo.Size = New System.Drawing.Size(144, 20)
        Me.txtCodigo.TabIndex = 3
        Me.txtCodigo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblCIF
        '
        Me.lblCIF.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblCIF.Location = New System.Drawing.Point(376, 3)
        Me.lblCIF.Name = "lblCIF"
        Me.lblCIF.Size = New System.Drawing.Size(27, 20)
        Me.lblCIF.TabIndex = 0
        Me.lblCIF.Values.Text = "CIF"
        '
        'lblCodigo
        '
        Me.lblCodigo.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblCodigo.Location = New System.Drawing.Point(66, 3)
        Me.lblCodigo.Name = "lblCodigo"
        Me.lblCodigo.Size = New System.Drawing.Size(50, 20)
        Me.lblCodigo.TabIndex = 2
        Me.lblCodigo.Values.Text = "C�digo"
        '
        'tbpDirecciones
        '
        Me.tbpDirecciones.AutoHiddenSlideSize = New System.Drawing.Size(200, 200)
        Me.tbpDirecciones.Controls.Add(Me.hdrDirecciones)
        Me.tbpDirecciones.Flags = 65534
        Me.tbpDirecciones.LastVisibleSet = True
        Me.tbpDirecciones.MinimumSize = New System.Drawing.Size(50, 50)
        Me.tbpDirecciones.Name = "tbpDirecciones"
        Me.tbpDirecciones.Size = New System.Drawing.Size(545, 405)
        Me.tbpDirecciones.Text = "Direcciones"
        Me.tbpDirecciones.ToolTipTitle = "Page ToolTip"
        Me.tbpDirecciones.UniqueName = "F7BCF7D7E9CB42F55ABB0A4A5AA8CB19"
        '
        'hdrDirecciones
        '
        Me.hdrDirecciones.AutoSize = True
        Me.hdrDirecciones.ButtonSpecs.AddRange(New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup() {Me.btnNuevaDireccion, Me.btnEliminarDireccion})
        Me.hdrDirecciones.Dock = System.Windows.Forms.DockStyle.Fill
        Me.hdrDirecciones.HeaderStylePrimary = ComponentFactory.Krypton.Toolkit.HeaderStyle.Secondary
        Me.hdrDirecciones.HeaderVisibleSecondary = False
        Me.hdrDirecciones.Location = New System.Drawing.Point(0, 0)
        Me.hdrDirecciones.Name = "hdrDirecciones"
        '
        'hdrDirecciones.Panel
        '
        Me.hdrDirecciones.Panel.Controls.Add(Me.vsrDirecciones)
        Me.hdrDirecciones.Size = New System.Drawing.Size(545, 405)
        Me.hdrDirecciones.TabIndex = 0
        Me.hdrDirecciones.ValuesPrimary.Heading = "Direcciones"
        Me.hdrDirecciones.ValuesPrimary.Image = Nothing
        '
        'btnNuevaDireccion
        '
        Me.btnNuevaDireccion.Image = Global.Escritorio.My.Resources.Resources.Nuevo_16
        Me.btnNuevaDireccion.ToolTipBody = "A�ade una nueva direcci�n al proveedor"
        Me.btnNuevaDireccion.UniqueName = "07DE158AA65E4281219838804DAA5460"
        '
        'btnEliminarDireccion
        '
        Me.btnEliminarDireccion.Image = Global.Escritorio.My.Resources.Resources.Eliminar_16
        Me.btnEliminarDireccion.ToolTipBody = "Elimina la direcci�n seleccionada"
        Me.btnEliminarDireccion.UniqueName = "C9D8865EF34A47AF1D80B791BF0FC5E5"
        '
        'vsrDirecciones
        '
        Me.vsrDirecciones.BackColor = System.Drawing.Color.Transparent
        Me.vsrDirecciones.Contexto = Nothing
        Me.vsrDirecciones.Current = Nothing
        Me.vsrDirecciones.Dock = System.Windows.Forms.DockStyle.Fill
        Me.vsrDirecciones.Items = Nothing
        Me.vsrDirecciones.Location = New System.Drawing.Point(0, 0)
        Me.vsrDirecciones.Name = "vsrDirecciones"
        Me.vsrDirecciones.Size = New System.Drawing.Size(543, 378)
        Me.vsrDirecciones.TabIndex = 0
        Me.vsrDirecciones.Tipo = Escritorio.ListaVisores.Tipos.Direcciones
        '
        'tbpContactos
        '
        Me.tbpContactos.AutoHiddenSlideSize = New System.Drawing.Size(200, 200)
        Me.tbpContactos.Controls.Add(Me.hdrContactos)
        Me.tbpContactos.Flags = 65534
        Me.tbpContactos.LastVisibleSet = True
        Me.tbpContactos.MinimumSize = New System.Drawing.Size(50, 50)
        Me.tbpContactos.Name = "tbpContactos"
        Me.tbpContactos.Size = New System.Drawing.Size(545, 405)
        Me.tbpContactos.Text = "Contactos"
        Me.tbpContactos.ToolTipTitle = "Page ToolTip"
        Me.tbpContactos.UniqueName = "034529F64C574DC716A90B7637404FB3"
        '
        'hdrContactos
        '
        Me.hdrContactos.AutoSize = True
        Me.hdrContactos.ButtonSpecs.AddRange(New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup() {Me.btnNuevoContacto, Me.btnEliminarContacto})
        Me.hdrContactos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.hdrContactos.HeaderStylePrimary = ComponentFactory.Krypton.Toolkit.HeaderStyle.Secondary
        Me.hdrContactos.HeaderVisibleSecondary = False
        Me.hdrContactos.Location = New System.Drawing.Point(0, 0)
        Me.hdrContactos.Name = "hdrContactos"
        '
        'hdrContactos.Panel
        '
        Me.hdrContactos.Panel.Controls.Add(Me.vsrContactos)
        Me.hdrContactos.Size = New System.Drawing.Size(545, 405)
        Me.hdrContactos.TabIndex = 0
        Me.hdrContactos.ValuesPrimary.Heading = "Contactos"
        Me.hdrContactos.ValuesPrimary.Image = Nothing
        '
        'btnNuevoContacto
        '
        Me.btnNuevoContacto.Image = Global.Escritorio.My.Resources.Resources.Nuevo_16
        Me.btnNuevoContacto.ToolTipBody = "A�ade un nuevo contacto al proveedor"
        Me.btnNuevoContacto.UniqueName = "07DE158AA65E4281219838804DAA5460"
        '
        'btnEliminarContacto
        '
        Me.btnEliminarContacto.Image = Global.Escritorio.My.Resources.Resources.Eliminar_16
        Me.btnEliminarContacto.ToolTipBody = "Elimina el contacto seleccionado"
        Me.btnEliminarContacto.UniqueName = "C9D8865EF34A47AF1D80B791BF0FC5E5"
        '
        'vsrContactos
        '
        Me.vsrContactos.BackColor = System.Drawing.Color.Transparent
        Me.vsrContactos.Contexto = Nothing
        Me.vsrContactos.Current = Nothing
        Me.vsrContactos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.vsrContactos.Items = Nothing
        Me.vsrContactos.Location = New System.Drawing.Point(0, 0)
        Me.vsrContactos.Name = "vsrContactos"
        Me.vsrContactos.Size = New System.Drawing.Size(543, 378)
        Me.vsrContactos.TabIndex = 0
        Me.vsrContactos.Tipo = Escritorio.ListaVisores.Tipos.Contactos
        '
        'tbpObservaciones
        '
        Me.tbpObservaciones.AutoHiddenSlideSize = New System.Drawing.Size(200, 200)
        Me.tbpObservaciones.Controls.Add(Me.htmlObservaciones)
        Me.tbpObservaciones.Flags = 65534
        Me.tbpObservaciones.LastVisibleSet = True
        Me.tbpObservaciones.MinimumSize = New System.Drawing.Size(50, 50)
        Me.tbpObservaciones.Name = "tbpObservaciones"
        Me.tbpObservaciones.Size = New System.Drawing.Size(576, 330)
        Me.tbpObservaciones.Text = "Observaciones"
        Me.tbpObservaciones.ToolTipTitle = "Page ToolTip"
        Me.tbpObservaciones.UniqueName = "D7D0D0D7C60441F701AF557B8C60517E"
        '
        'htmlObservaciones
        '
        Me.htmlObservaciones.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.htmlObservaciones.ConvertirImagenesABase64 = True
        Me.htmlObservaciones.Dock = System.Windows.Forms.DockStyle.Fill
        Me.htmlObservaciones.Location = New System.Drawing.Point(0, 0)
        Me.htmlObservaciones.MostrarBarrasEdicion = True
        Me.htmlObservaciones.Name = "htmlObservaciones"
        Me.htmlObservaciones.ReadOnly = False
        Me.htmlObservaciones.Size = New System.Drawing.Size(576, 330)
        Me.htmlObservaciones.TabIndex = 0
        '
        'tbpLotesSalida
        '
        Me.tbpLotesSalida.AutoHiddenSlideSize = New System.Drawing.Size(200, 200)
        Me.tbpLotesSalida.Controls.Add(Me.splSeparadorAlbaranesSalida)
        Me.tbpLotesSalida.Flags = 65534
        Me.tbpLotesSalida.LastVisibleSet = True
        Me.tbpLotesSalida.MinimumSize = New System.Drawing.Size(50, 50)
        Me.tbpLotesSalida.Name = "tbpLotesSalida"
        Me.tbpLotesSalida.Size = New System.Drawing.Size(576, 330)
        Me.tbpLotesSalida.Text = "Env�os"
        Me.tbpLotesSalida.ToolTipTitle = "Page ToolTip"
        Me.tbpLotesSalida.UniqueName = "7476B0A107A644C032B472CF54B28A5C"
        '
        'splSeparadorAlbaranesSalida
        '
        Me.splSeparadorAlbaranesSalida.Cursor = System.Windows.Forms.Cursors.Default
        Me.splSeparadorAlbaranesSalida.Dock = System.Windows.Forms.DockStyle.Fill
        Me.splSeparadorAlbaranesSalida.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.splSeparadorAlbaranesSalida.Location = New System.Drawing.Point(0, 0)
        Me.splSeparadorAlbaranesSalida.Name = "splSeparadorAlbaranesSalida"
        Me.splSeparadorAlbaranesSalida.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'splSeparadorAlbaranesSalida.Panel1
        '
        Me.splSeparadorAlbaranesSalida.Panel1.Controls.Add(Me.hdrAlbaranes)
        '
        'splSeparadorAlbaranesSalida.Panel2
        '
        Me.splSeparadorAlbaranesSalida.Panel2.Controls.Add(Me.dgvAlbaranesSalida)
        Me.splSeparadorAlbaranesSalida.Size = New System.Drawing.Size(576, 330)
        Me.splSeparadorAlbaranesSalida.SplitterDistance = 78
        Me.splSeparadorAlbaranesSalida.TabIndex = 0
        '
        'hdrAlbaranes
        '
        Me.hdrAlbaranes.ButtonSpecs.AddRange(New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup() {Me.btnOcultarFiltroAlbaranes})
        Me.hdrAlbaranes.Dock = System.Windows.Forms.DockStyle.Fill
        Me.hdrAlbaranes.HeaderStylePrimary = ComponentFactory.Krypton.Toolkit.HeaderStyle.Secondary
        Me.hdrAlbaranes.HeaderVisibleSecondary = False
        Me.hdrAlbaranes.Location = New System.Drawing.Point(0, 0)
        Me.hdrAlbaranes.Name = "hdrAlbaranes"
        '
        'hdrAlbaranes.Panel
        '
        Me.hdrAlbaranes.Panel.Controls.Add(Me.tblFiltrosAlbaranes)
        Me.hdrAlbaranes.Size = New System.Drawing.Size(576, 78)
        Me.hdrAlbaranes.TabIndex = 11
        Me.hdrAlbaranes.ValuesPrimary.Heading = "Filtros para b�squeda de env�os"
        Me.hdrAlbaranes.ValuesPrimary.Image = Global.Escritorio.My.Resources.Resources.Buscar_16
        '
        'btnOcultarFiltroAlbaranes
        '
        Me.btnOcultarFiltroAlbaranes.Type = ComponentFactory.Krypton.Toolkit.PaletteButtonSpecStyle.ArrowUp
        Me.btnOcultarFiltroAlbaranes.UniqueName = "9F7DAF936E594AB76895345303F35C3F"
        '
        'tblFiltrosAlbaranes
        '
        Me.tblFiltrosAlbaranes.BackColor = System.Drawing.Color.Transparent
        Me.tblFiltrosAlbaranes.ColumnCount = 5
        Me.tblFiltrosAlbaranes.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblFiltrosAlbaranes.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.tblFiltrosAlbaranes.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblFiltrosAlbaranes.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.tblFiltrosAlbaranes.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblFiltrosAlbaranes.Controls.Add(Me.lblFiltroSalidaCodigoDesde, 0, 0)
        Me.tblFiltrosAlbaranes.Controls.Add(Me.lblFiltroSalidaCodigoHasta, 2, 0)
        Me.tblFiltrosAlbaranes.Controls.Add(Me.lblFiltroSalidaFechaDesde, 0, 1)
        Me.tblFiltrosAlbaranes.Controls.Add(Me.lblFiltroSalidaFechaHasta, 2, 1)
        Me.tblFiltrosAlbaranes.Controls.Add(Me.txtFiltroSalidaCodigoDesde, 1, 0)
        Me.tblFiltrosAlbaranes.Controls.Add(Me.txtFiltroSalidaCodigoHasta, 3, 0)
        Me.tblFiltrosAlbaranes.Controls.Add(Me.dtpFiltroSalidaFechaDesde, 1, 1)
        Me.tblFiltrosAlbaranes.Controls.Add(Me.dtpFiltroSalidaFechaHasta, 3, 1)
        Me.tblFiltrosAlbaranes.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblFiltrosAlbaranes.Location = New System.Drawing.Point(0, 0)
        Me.tblFiltrosAlbaranes.Name = "tblFiltrosAlbaranes"
        Me.tblFiltrosAlbaranes.RowCount = 3
        Me.tblFiltrosAlbaranes.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblFiltrosAlbaranes.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblFiltrosAlbaranes.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblFiltrosAlbaranes.Size = New System.Drawing.Size(574, 55)
        Me.tblFiltrosAlbaranes.TabIndex = 9
        '
        'lblFiltroSalidaCodigoDesde
        '
        Me.lblFiltroSalidaCodigoDesde.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblFiltroSalidaCodigoDesde.Location = New System.Drawing.Point(3, 3)
        Me.lblFiltroSalidaCodigoDesde.Name = "lblFiltroSalidaCodigoDesde"
        Me.lblFiltroSalidaCodigoDesde.Size = New System.Drawing.Size(67, 20)
        Me.lblFiltroSalidaCodigoDesde.TabIndex = 1
        Me.lblFiltroSalidaCodigoDesde.Values.Text = "C�digo de"
        '
        'lblFiltroSalidaCodigoHasta
        '
        Me.lblFiltroSalidaCodigoHasta.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblFiltroSalidaCodigoHasta.Location = New System.Drawing.Point(315, 3)
        Me.lblFiltroSalidaCodigoHasta.Name = "lblFiltroSalidaCodigoHasta"
        Me.lblFiltroSalidaCodigoHasta.Size = New System.Drawing.Size(17, 20)
        Me.lblFiltroSalidaCodigoHasta.TabIndex = 20
        Me.lblFiltroSalidaCodigoHasta.Values.Text = "a"
        '
        'lblFiltroSalidaFechaDesde
        '
        Me.lblFiltroSalidaFechaDesde.Dock = System.Windows.Forms.DockStyle.Right
        Me.lblFiltroSalidaFechaDesde.Location = New System.Drawing.Point(11, 29)
        Me.lblFiltroSalidaFechaDesde.Name = "lblFiltroSalidaFechaDesde"
        Me.lblFiltroSalidaFechaDesde.Size = New System.Drawing.Size(59, 21)
        Me.lblFiltroSalidaFechaDesde.TabIndex = 3
        Me.lblFiltroSalidaFechaDesde.Values.Text = "Fecha de"
        '
        'lblFiltroSalidaFechaHasta
        '
        Me.lblFiltroSalidaFechaHasta.Location = New System.Drawing.Point(315, 29)
        Me.lblFiltroSalidaFechaHasta.Name = "lblFiltroSalidaFechaHasta"
        Me.lblFiltroSalidaFechaHasta.Size = New System.Drawing.Size(17, 20)
        Me.lblFiltroSalidaFechaHasta.TabIndex = 3
        Me.lblFiltroSalidaFechaHasta.Values.Text = "a"
        '
        'txtFiltroSalidaCodigoDesde
        '
        Me.txtFiltroSalidaCodigoDesde.controlarBotonBorrar = True
        Me.txtFiltroSalidaCodigoDesde.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtFiltroSalidaCodigoDesde.Formato = ""
        Me.txtFiltroSalidaCodigoDesde.Location = New System.Drawing.Point(76, 3)
        Me.txtFiltroSalidaCodigoDesde.mostrarSiempreBotonBorrar = False
        Me.txtFiltroSalidaCodigoDesde.Name = "txtFiltroSalidaCodigoDesde"
        Me.txtFiltroSalidaCodigoDesde.seleccionarTodo = True
        Me.txtFiltroSalidaCodigoDesde.Size = New System.Drawing.Size(233, 20)
        Me.txtFiltroSalidaCodigoDesde.TabIndex = 21
        '
        'txtFiltroSalidaCodigoHasta
        '
        Me.txtFiltroSalidaCodigoHasta.controlarBotonBorrar = True
        Me.txtFiltroSalidaCodigoHasta.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtFiltroSalidaCodigoHasta.Formato = ""
        Me.txtFiltroSalidaCodigoHasta.Location = New System.Drawing.Point(338, 3)
        Me.txtFiltroSalidaCodigoHasta.mostrarSiempreBotonBorrar = False
        Me.txtFiltroSalidaCodigoHasta.Name = "txtFiltroSalidaCodigoHasta"
        Me.txtFiltroSalidaCodigoHasta.seleccionarTodo = True
        Me.txtFiltroSalidaCodigoHasta.Size = New System.Drawing.Size(233, 20)
        Me.txtFiltroSalidaCodigoHasta.TabIndex = 21
        '
        'dtpFiltroSalidaFechaDesde
        '
        Me.dtpFiltroSalidaFechaDesde.CalendarTodayText = "Hoy:"
        Me.dtpFiltroSalidaFechaDesde.controlarBotonBorrar = True
        Me.dtpFiltroSalidaFechaDesde.CustomNullText = "(Sin fecha)"
        Me.dtpFiltroSalidaFechaDesde.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dtpFiltroSalidaFechaDesde.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFiltroSalidaFechaDesde.Location = New System.Drawing.Point(76, 29)
        Me.dtpFiltroSalidaFechaDesde.mostrarSiempreBotonBorrar = False
        Me.dtpFiltroSalidaFechaDesde.Name = "dtpFiltroSalidaFechaDesde"
        Me.dtpFiltroSalidaFechaDesde.Size = New System.Drawing.Size(233, 21)
        Me.dtpFiltroSalidaFechaDesde.TabIndex = 22
        Me.dtpFiltroSalidaFechaDesde.TipoLimpieza = Escritorio.Quadralia.Controles.aDateTimePicker.TiposLimpieza.ValueYValueNullable
        '
        'dtpFiltroSalidaFechaHasta
        '
        Me.dtpFiltroSalidaFechaHasta.CalendarTodayText = "Hoy:"
        Me.dtpFiltroSalidaFechaHasta.controlarBotonBorrar = True
        Me.dtpFiltroSalidaFechaHasta.CustomNullText = "(Sin fecha)"
        Me.dtpFiltroSalidaFechaHasta.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dtpFiltroSalidaFechaHasta.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFiltroSalidaFechaHasta.Location = New System.Drawing.Point(338, 29)
        Me.dtpFiltroSalidaFechaHasta.mostrarSiempreBotonBorrar = False
        Me.dtpFiltroSalidaFechaHasta.Name = "dtpFiltroSalidaFechaHasta"
        Me.dtpFiltroSalidaFechaHasta.Size = New System.Drawing.Size(233, 21)
        Me.dtpFiltroSalidaFechaHasta.TabIndex = 22
        Me.dtpFiltroSalidaFechaHasta.TipoLimpieza = Escritorio.Quadralia.Controles.aDateTimePicker.TiposLimpieza.ValueYValueNullable
        '
        'dgvAlbaranesSalida
        '
        Me.dgvAlbaranesSalida.AllowUserToAddRows = False
        Me.dgvAlbaranesSalida.AllowUserToDeleteRows = False
        Me.dgvAlbaranesSalida.AllowUserToOrderColumns = True
        Me.dgvAlbaranesSalida.AllowUserToResizeRows = False
        Me.dgvAlbaranesSalida.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colLoteSalidaCodigo, Me.colLoteSalidaFecha, Me.colLoteSalidaExpedidor})
        Me.dgvAlbaranesSalida.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvAlbaranesSalida.Location = New System.Drawing.Point(0, 0)
        Me.dgvAlbaranesSalida.MultiSelect = False
        Me.dgvAlbaranesSalida.Name = "dgvAlbaranesSalida"
        Me.dgvAlbaranesSalida.RowHeadersVisible = False
        Me.dgvAlbaranesSalida.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAlbaranesSalida.Size = New System.Drawing.Size(576, 247)
        Me.dgvAlbaranesSalida.TabIndex = 1
        '
        'colLoteSalidaCodigo
        '
        Me.colLoteSalidaCodigo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colLoteSalidaCodigo.DataPropertyName = "codigo"
        Me.colLoteSalidaCodigo.HeaderText = "C�digo"
        Me.colLoteSalidaCodigo.Name = "colLoteSalidaCodigo"
        Me.colLoteSalidaCodigo.ReadOnly = True
        Me.colLoteSalidaCodigo.Width = 75
        '
        'colLoteSalidaFecha
        '
        Me.colLoteSalidaFecha.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colLoteSalidaFecha.DataPropertyName = "fecha"
        DataGridViewCellStyle1.Format = "d"
        Me.colLoteSalidaFecha.DefaultCellStyle = DataGridViewCellStyle1
        Me.colLoteSalidaFecha.HeaderText = "Fecha"
        Me.colLoteSalidaFecha.Name = "colLoteSalidaFecha"
        Me.colLoteSalidaFecha.ReadOnly = True
        Me.colLoteSalidaFecha.Width = 67
        '
        'colLoteSalidaExpedidor
        '
        Me.colLoteSalidaExpedidor.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colLoteSalidaExpedidor.DataPropertyName = "nombreExpedidor"
        Me.colLoteSalidaExpedidor.HeaderText = "Expedidor"
        Me.colLoteSalidaExpedidor.Name = "colLoteSalidaExpedidor"
        Me.colLoteSalidaExpedidor.ReadOnly = True
        Me.colLoteSalidaExpedidor.Width = 433
        '
        'lblTitulo
        '
        Me.lblTitulo.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.Custom1
        Me.lblTitulo.Location = New System.Drawing.Point(5, 9)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(241, 34)
        Me.lblTitulo.TabIndex = 0
        Me.lblTitulo.Values.Image = Global.Escritorio.My.Resources.Resources.Cliente_32
        Me.lblTitulo.Values.Text = "Gesti�n de Clientes"
        '
        'grpCabecera
        '
        Me.grpCabecera.Dock = System.Windows.Forms.DockStyle.Top
        Me.grpCabecera.GroupBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.PanelCustom1
        Me.grpCabecera.Location = New System.Drawing.Point(0, 0)
        Me.grpCabecera.Name = "grpCabecera"
        '
        'grpCabecera.Panel
        '
        Me.grpCabecera.Panel.Controls.Add(Me.lblTitulo)
        Me.grpCabecera.Size = New System.Drawing.Size(810, 57)
        Me.grpCabecera.StateCommon.Border.Color1 = System.Drawing.Color.DarkOrange
        Me.grpCabecera.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom
        Me.grpCabecera.StateCommon.Border.Width = 5
        Me.grpCabecera.TabIndex = 0
        '
        'epErrores
        '
        Me.epErrores.Alineacion = System.Windows.Forms.ErrorIconAlignment.MiddleLeft
        Me.epErrores.ContainerControl = Me
        '
        'frmClientes
        '
        Me.ClientSize = New System.Drawing.Size(810, 414)
        Me.Controls.Add(Me.splSeparador)
        Me.Controls.Add(Me.grpCabecera)
        Me.Name = "frmClientes"
        Me.Text = "Gesti�n de Clientes"
        CType(Me.grpResultados.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpResultados.Panel.ResumeLayout(False)
        CType(Me.grpResultados, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpResultados.ResumeLayout(False)
        CType(Me.dgvResultadosBuscador, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.splSeparador.Panel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.splSeparador.Panel1.ResumeLayout(False)
        Me.splSeparador.Panel1.PerformLayout()
        CType(Me.splSeparador.Panel2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.splSeparador.Panel2.ResumeLayout(False)
        CType(Me.splSeparador, System.ComponentModel.ISupportInitialize).EndInit()
        Me.splSeparador.ResumeLayout(False)
        CType(Me.hdrBusqueda.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrBusqueda.Panel.ResumeLayout(False)
        CType(Me.hdrBusqueda, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrBusqueda.ResumeLayout(False)
        Me.tblOrganizadorBusqueda.ResumeLayout(False)
        Me.tblOrganizadorBusqueda.PerformLayout()
        CType(Me.tabGeneral, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabGeneral.ResumeLayout(False)
        CType(Me.tbpDatosContacto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpDatosContacto.ResumeLayout(False)
        Me.tblOrganizadorDatos.ResumeLayout(False)
        Me.tblOrganizadorDatos.PerformLayout()
        CType(Me.tbpDirecciones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpDirecciones.ResumeLayout(False)
        Me.tbpDirecciones.PerformLayout()
        CType(Me.hdrDirecciones.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrDirecciones.Panel.ResumeLayout(False)
        CType(Me.hdrDirecciones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrDirecciones.ResumeLayout(False)
        CType(Me.tbpContactos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpContactos.ResumeLayout(False)
        Me.tbpContactos.PerformLayout()
        CType(Me.hdrContactos.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrContactos.Panel.ResumeLayout(False)
        CType(Me.hdrContactos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrContactos.ResumeLayout(False)
        CType(Me.tbpObservaciones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpObservaciones.ResumeLayout(False)
        CType(Me.tbpLotesSalida, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpLotesSalida.ResumeLayout(False)
        CType(Me.splSeparadorAlbaranesSalida.Panel1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.splSeparadorAlbaranesSalida.Panel1.ResumeLayout(False)
        CType(Me.splSeparadorAlbaranesSalida.Panel2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.splSeparadorAlbaranesSalida.Panel2.ResumeLayout(False)
        CType(Me.splSeparadorAlbaranesSalida, System.ComponentModel.ISupportInitialize).EndInit()
        Me.splSeparadorAlbaranesSalida.ResumeLayout(False)
        CType(Me.hdrAlbaranes.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrAlbaranes.Panel.ResumeLayout(False)
        CType(Me.hdrAlbaranes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrAlbaranes.ResumeLayout(False)
        Me.tblFiltrosAlbaranes.ResumeLayout(False)
        Me.tblFiltrosAlbaranes.PerformLayout()
        CType(Me.dgvAlbaranesSalida, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grpCabecera.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpCabecera.Panel.ResumeLayout(False)
        Me.grpCabecera.Panel.PerformLayout()
        CType(Me.grpCabecera, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpCabecera.ResumeLayout(False)
        CType(Me.epErrores, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents splSeparador As ComponentFactory.Krypton.Toolkit.KryptonSplitContainer
    Friend WithEvents hdrBusqueda As ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup
    Friend WithEvents btnOcultarBusqueda As ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup
    Friend WithEvents lblTitulo As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents grpCabecera As ComponentFactory.Krypton.Toolkit.KryptonGroup
    Friend WithEvents epErrores As Quadralia.Controles.Validacion.GestorErrores
    Friend WithEvents lblBUsuario As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblBEmail As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtBCif As Quadralia.Controles.aTextBox
    Friend WithEvents btnBBuscar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents grpResultados As ComponentFactory.Krypton.Toolkit.KryptonGroupBox
    Friend WithEvents dgvResultadosBuscador As ComponentFactory.Krypton.Toolkit.KryptonDataGridView
    Friend WithEvents tblOrganizadorBusqueda As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents tabGeneral As ComponentFactory.Krypton.Navigator.KryptonNavigator
    Friend WithEvents tbpDatosContacto As ComponentFactory.Krypton.Navigator.KryptonPage
    Friend WithEvents tbpDirecciones As ComponentFactory.Krypton.Navigator.KryptonPage
    Friend WithEvents tbpContactos As ComponentFactory.Krypton.Navigator.KryptonPage
    Friend WithEvents lblBCodigoDe As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtBNombre As Quadralia.Controles.aTextBox
    Friend WithEvents tblOrganizadorDatos As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtCodigo As Quadralia.Controles.aTextBox
    Friend WithEvents lblCodigo As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtCIF As Quadralia.Controles.aTextBox
    Friend WithEvents lblCIF As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblRazonSocial As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblTelefono1 As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblTelefono2 As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblEmail As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtRazonSocial As Quadralia.Controles.aTextBox
    Friend WithEvents txtEmail As Quadralia.Controles.aTextBox
    Friend WithEvents txtTelefono1 As Quadralia.Controles.aTextBox
    Friend WithEvents txtTelefono2 As Quadralia.Controles.aTextBox
    Friend WithEvents hdrDirecciones As ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup
    Friend WithEvents btnNuevaDireccion As ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup
    Friend WithEvents btnEliminarDireccion As ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup
    Friend WithEvents hdrContactos As ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup
    Friend WithEvents btnNuevoContacto As ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup
    Friend WithEvents btnEliminarContacto As ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup
    Friend WithEvents vsrContactos As Escritorio.ListaVisores
    Friend WithEvents vsrDirecciones As Escritorio.ListaVisores
    Friend WithEvents tbpObservaciones As ComponentFactory.Krypton.Navigator.KryptonPage
    Friend WithEvents htmlObservaciones As Escritorio.EditorHTML
    Friend WithEvents lblBCodigoA As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents chkBEliminados As ComponentFactory.Krypton.Toolkit.KryptonCheckBox
    Friend WithEvents chkReactivarRegistro As ComponentFactory.Krypton.Toolkit.KryptonCheckBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents txtBCodigoDe As Quadralia.Controles.aTextBox
    Friend WithEvents txtBCodigoA As Quadralia.Controles.aTextBox
    Friend WithEvents lblWeb As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtWeb As Quadralia.Controles.aTextBox
    Friend WithEvents colCodigo As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents colRazonSocial As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents btnLimpiarBusqueda As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents tbpLotesSalida As ComponentFactory.Krypton.Navigator.KryptonPage
    Friend WithEvents splSeparadorAlbaranesSalida As ComponentFactory.Krypton.Toolkit.KryptonSplitContainer
    Friend WithEvents hdrAlbaranes As ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup
    Friend WithEvents btnOcultarFiltroAlbaranes As ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup
    Friend WithEvents tblFiltrosAlbaranes As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblFiltroSalidaCodigoDesde As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblFiltroSalidaCodigoHasta As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblFiltroSalidaFechaDesde As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblFiltroSalidaFechaHasta As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtFiltroSalidaCodigoDesde As Quadralia.Controles.aTextBox
    Friend WithEvents txtFiltroSalidaCodigoHasta As Quadralia.Controles.aTextBox
    Friend WithEvents dtpFiltroSalidaFechaDesde As Escritorio.Quadralia.Controles.aDateTimePicker
    Friend WithEvents dtpFiltroSalidaFechaHasta As Escritorio.Quadralia.Controles.aDateTimePicker
    Friend WithEvents dgvAlbaranesSalida As ComponentFactory.Krypton.Toolkit.KryptonDataGridView
    Friend WithEvents lblNombreComercial As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblFax As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtNombreComercial As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents txtFax As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents lblAlerta As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtAlerta As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents colLoteSalidaCodigo As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents colLoteSalidaFecha As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents colLoteSalidaExpedidor As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
End Class
