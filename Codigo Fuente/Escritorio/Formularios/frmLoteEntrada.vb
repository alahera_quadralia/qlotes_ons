Public Class frmLoteEntrada
#Region " DECLARACIONES "
    Private _EstadoFormulario As Quadralia.Enumerados.EstadoFormulario = Quadralia.Enumerados.EstadoFormulario.Espera  ' Indica el estado en el que se encuentra el formulario
    Private _Registro As LoteEntrada = Nothing
    Private _Contexto As Entidades = Nothing
    Private _dgvDesgloseEliminando As Boolean = False

    Private _dgvLoad As Boolean = False ' Bandera para evitar que se cargue la primera fila cuando se asigna un origen de datos al DGV
#End Region
#Region " PROPIEDADES "
    Public Overrides Property Estado() As Quadralia.Enumerados.EstadoFormulario
        Get
            Return _EstadoFormulario
        End Get
        Set(ByVal value As Quadralia.Enumerados.EstadoFormulario)
            _EstadoFormulario = value

            'Para evitar refrescos raros, cuando ponemos el formulario en estado de espera, volvemos a la pesta�a de datos
            hdrBusqueda.Panel.Enabled = (value = Quadralia.Enumerados.EstadoFormulario.Espera)
            htmlObservaciones.Enabled = (value <> Quadralia.Enumerados.EstadoFormulario.Espera)
            tbpFacturas.Visible = (value <> Quadralia.Enumerados.EstadoFormulario.Anhadiendo)
            dgvLotesSalida.Enabled = True

            ' Desactivo todo menos el dgv de lineas
            For Each ctrl As Control In tblOrganizadorDatos.Controls
                ctrl.Enabled = ctrl IsNot dgvLineas AndAlso (value <> Quadralia.Enumerados.EstadoFormulario.Espera)
            Next

            ' Configuro DGV
            dgvLineas.Enabled = True
            dgvLineas.AllowUserToAddRows = (value <> Quadralia.Enumerados.EstadoFormulario.Espera)
            dgvLineas.ReadOnly = Not dgvLineas.AllowUserToAddRows
            dgvLineas.AllowUserToDeleteRows = dgvLineas.AllowUserToAddRows

            ' Reconfiguro los colores
            For Each Fila As DataGridViewRow In dgvLineas.Rows
                Fila.DefaultCellStyle = New DataGridViewCellStyle
            Next

            ' Tengo que establecer el foco en un control para que no quede "muerto" el ribbon
            If value = Quadralia.Enumerados.EstadoFormulario.Espera Then
                Me.ActiveControl = txtBcodigo.TextBox
            Else
                Me.ActiveControl = txtProveedor
            End If

            ControlarBotones()
        End Set
    End Property

    ''' <summary>
    ''' Registro que se muestra actualmente en el formulario
    ''' </summary>
    Public Property Registro As LoteEntrada
        Get
            Return _Registro
        End Get
        Set(ByVal value As LoteEntrada)
            On Error Resume Next
            _Registro = value

            If value IsNot Nothing Then If Not value.Lineas.IsLoaded Then value.Lineas.Load()

            CargarRegistro(value)
        End Set
    End Property

    ''' <summary>
    ''' Contexto que utilizar� el formulario
    ''' </summary>
    Public Property Contexto As Entidades
        Get
            If _Contexto Is Nothing Then
                _Contexto = cDAL.Instancia.getContext()
                _Contexto.ContextOptions.LazyLoadingEnabled = True
            End If
            Return _Contexto
        End Get
        Set(ByVal value As Entidades)
            _Contexto = value
        End Set
    End Property

    ''' <summary>
    ''' Obtiene / establece el id del elemento cargado en el formulario
    ''' </summary>
    Public Property IdRegistro As Long
        Get
            If Me.Registro Is Nothing Then Return -1 Else Return Me.Registro.id
        End Get
        Set(ByVal value As Long)
            Me.Registro = (From ent As LoteEntrada In Me.Contexto.LotesEntrada Where ent.id = value).FirstOrDefault
        End Set
    End Property
#End Region
#Region " IMPLEMENTACIONES NECESARIAS DEL FORMULARIO HIJO "
    ''' <summary>
    ''' Tab del ribbon asociado a este formulario
    ''' </summary>
    Public Overrides ReadOnly Property Tab() As ComponentFactory.Krypton.Ribbon.KryptonRibbonTab
        Get
            Return frmPrincipal.tabLotesEntrada
        End Get
    End Property
#End Region
#Region " LIMPIEZA "
    ''' <summary>
    ''' Limpia los controles relacionados con la b�squeda de registros
    ''' </summary>
    Private Sub LimpiarBusqueda() Handles btnLimpiarBusqueda.Click
        txtBcodigo.Text = String.Empty

        dtpBFechaInicio.Value = DateTime.Now
        dtpBFechaInicio.ValueNullable = Nothing

        dtpBFechaFin.Value = DateTime.Now
        dtpBFechaFin.ValueNullable = Nothing

        txtBProveedor.Clear()
        txtBEspecie.Clear()
        txtBNumeroLote.Clear()
        cboBZonaFAO.Clear()
        cboBSubZona.Clear()

        chkBRegistrosEliminados.Checked = False

        dgvResultadosBuscador.DataSource = Nothing
        dgvResultadosBuscador.Rows.Clear()
    End Sub

    ''' <summary>
    ''' Limpia los controles relacionados con la edicion de registros
    ''' </summary>
    Private Sub LimpiarDatos()
        txtCodigo.Clear()
        txtProveedor.Clear()
        dtpFecha.ValueNullable = DateTime.Now
        chkReactivarRegistro.Checked = False

        dgvLineas.DataSource = Nothing
        txtTotal.Clear()
        htmlObservaciones.Clear()

        LimpiarLotesSalida()

        epErrores.Clear()
    End Sub

    Private Sub LimpiarLotesSalida()
        dgvLotesSalida.DataSource = Nothing
        dgvLotesSalida.Rows.Clear()
    End Sub

    ''' <summary>
    ''' Borra todos los controles del formulario
    ''' </summary>
    Private Sub LimpiarTodo()
        LimpiarBusqueda()
        LimpiarDatos()
    End Sub
#End Region
#Region " CONTROL DE BOTONES "
    Private Sub ControlarBotones()
        ControlarRibbon()
    End Sub

    ''' <summary>
    ''' Controla el cambio de botones del formulario principal
    ''' </summary>
    Private Sub ControlarRibbon()
        If Me.ParentForm Is Nothing Then Exit Sub
        If Not TypeOf Me.ParentForm Is frmPrincipal Then Exit Sub

        With DirectCast(Me.ParentForm, frmPrincipal)
            .krgtLotesEntradaAccionesEdicion.Visible = (Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera)
            .krgtLotesEntradaAccionesEspera.Visible = Not .krgtLotesEntradaAccionesEdicion.Visible
            .mnuRapidoGuardar.Enabled = .krgtLotesEntradaAccionesEdicion.Visible

            .btnLotesEntradaAccionesModificar.Enabled = (Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera) AndAlso Registro IsNot Nothing
            .btnLotesEntradaAccionesEliminar.Enabled = .btnLotesEntradaAccionesModificar.Enabled

            .krgtLotesEntradaMasAcciones.Visible = Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera
            .btnLotesEntradaAccionesImprimir.Enabled = Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera AndAlso Me.Registro IsNot Nothing
            Try
                .btnLotesEntradaAccionesImprimirListadoSalida.Enabled = Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera AndAlso Me.Registro IsNot Nothing AndAlso Me.Registro.LotesSalida IsNot Nothing AndAlso Me.Registro.LotesSalida.Count > 0
            Catch ex As Exception
                Debugger.Break()
            End Try
        End With
    End Sub
#End Region
#Region " RUTINA DE INICIO / FIN Y CARGA DE DATOS MAESTROS "
    ''' <summary>
    ''' Rutina de inicio del formulario
    ''' </summary>
    Private Sub Inicio(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Tag = Me.Text
        tabGeneral.SelectedIndex = 0

        ' Cargamos los datos maestros
        CargarDatosMaestros()
        ConfigurarEntorno()

        ' Limpieza
        LimpiarTodo()
        tabGeneral.SelectedIndex = 0
        Me.Registro = Nothing

        ' Oculto el buscador
        If Not cConfiguracionAplicacion.Instancia.BuscadoresAbiertos Then OcultarBusquedaDobleClick(hdrBusqueda, Nothing)

        ' No quiero nuevas columnas
        dgvResultadosBuscador.AutoGenerateColumns = False
        dgvLineas.AutoGenerateColumns = False
        dgvLotesSalida.AutoGenerateColumns = False
        colFechaDesembarco.CalendarTodayDate = DateTime.Now

        Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera
    End Sub

    ''' <summary>
    ''' Carga los datos maestros
    ''' </summary>
    Public Sub CargarDatosMaestros()
        ' Cargar Zonas FAO
        With colZonaFao
            .DataSource = Nothing
            .Items.Clear()

            .DisplayMember = "descripcion"
            .ValueMember = "Self"
            .DataSource = (From fao As ZonaFao In Contexto.ZonasFao Select fao)
        End With

        ' Cargar Subzonas
        With colEntradaSubzona
            .DataSource = Nothing
            .Items.Clear()

            .DisplayMember = "descripcion"
            .ValueMember = "Self"
            .DataSource = (From sbz As SubZona In Contexto.SubZonas Select sbz)
        End With

        With cboBZonaFAO
            .DataSource = Nothing
            .Items.Clear()

            .DisplayMember = "descripcion"
            .ValueMember = "id"
            .DataSource = (From fao As ZonaFao In Contexto.ZonasFao Select fao.id, fao.descripcion).ToList

            .Clear()
        End With

        ' Cargar Subzonas
        With cboBSubZona
            .DataSource = Nothing
            .Items.Clear()

            .DisplayMember = "descripcion"
            .ValueMember = "id"
            .DataSource = (From sbz As SubZona In Contexto.SubZonas Select sbz.id, sbz.descripcion).ToList
            .Clear()
        End With

        ' Cargar Presentacion
        With colPresentacion
            .DataSource = Nothing
            .Items.Clear()

            .DisplayMember = "descripcion"
            .ValueMember = "Self"
            .DataSource = (From it As Presentacion In Contexto.FormasPresentacion Select it)
        End With


        ' Cargar Produccion
        With colProduccion
            .DataSource = Nothing
            .Items.Clear()

            .DisplayMember = "descripcion"
            .ValueMember = "Self"
            .DataSource = (From it As MetodoProduccion In Contexto.MetodosProduccion Select it)
        End With

        ' Cargar Produccion
        With colEntradaBarco
            .DataSource = Nothing
            .Items.Clear()

            .DisplayMember = "descripcion"
            .ValueMember = "Self"
            .DataSource = (From it As Barco In Contexto.Barcos Select it)
        End With
    End Sub
#End Region
#Region " BUSQUEDA, CARGA DE Y EDICION DE REGISTROS "
    ''' <summary>
    ''' Realiza la b�squeda de registros coincidentes con los par�metros especificados
    ''' </summary>
    Private Sub Buscar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBBuscar.Click
        Dim FechaInicio As Nullable(Of DateTime) = Nothing
        Dim FechaFin As Nullable(Of DateTime) = Nothing
        Dim IdProveedor As Nullable(Of Long) = Nothing
        Dim IdEspecie As Nullable(Of Integer) = Nothing
        Dim IdFao As Nullable(Of Integer) = Nothing
        Dim IdSubZona As Nullable(Of Integer) = Nothing

        If Not IsDBNull(dtpBFechaInicio.ValueNullable) Then FechaInicio = dtpBFechaInicio.Value.Date + New TimeSpan(0, 0, 0)
        If Not IsDBNull(dtpBFechaFin.ValueNullable) Then FechaFin = dtpBFechaFin.Value.Date + New TimeSpan(23, 59, 59)
        If txtBProveedor.Item IsNot Nothing Then IdProveedor = DirectCast(txtBProveedor.Item, Proveedor).id
        If txtBEspecie.Item IsNot Nothing Then IdEspecie = DirectCast(txtBEspecie.Item, Especie).id
        If cboBZonaFAO.SelectedIndex > -1 Then IdFao = cboBZonaFAO.SelectedValue
        If cboBSubZona.SelectedIndex > -1 Then IdSubZona = cboBSubZona.SelectedValue

        Dim Registros = (From ent As LoteEntrada In Contexto.LotesEntrada _
                         From lin As LoteEntradalinea In ent.Lineas.DefaultIfEmpty _
                         Where ((chkBRegistrosEliminados.Checked OrElse Not ent.fechaBaja.HasValue) _
                                AndAlso (String.IsNullOrEmpty(txtBcodigo.Text) OrElse ent.codigo.ToUpper.Contains(txtBcodigo.Text.ToUpper)) _
                                AndAlso (Not FechaInicio.HasValue OrElse ent.fecha >= FechaInicio.Value) _
                                AndAlso (Not FechaFin.HasValue OrElse ent.fecha <= FechaFin.Value) _
                                AndAlso (Not IdProveedor.HasValue OrElse (ent.Proveedor IsNot Nothing AndAlso ent.Proveedor.id = IdProveedor)) _
                                AndAlso (Not IdEspecie.HasValue OrElse (lin IsNot Nothing AndAlso lin.Especie IsNot Nothing AndAlso lin.Especie.id = IdEspecie.Value)) _
                                AndAlso (Not IdFao.HasValue OrElse (lin IsNot Nothing AndAlso lin.ZonaFao IsNot Nothing AndAlso lin.ZonaFao.id = IdFao.Value)) _
                                AndAlso (Not IdSubZona.HasValue OrElse (lin IsNot Nothing AndAlso lin.SubZona IsNot Nothing AndAlso lin.SubZona.id = IdSubZona.Value)) _
                                AndAlso (String.IsNullOrEmpty(txtBNumeroLote.Text) OrElse (lin IsNot Nothing AndAlso lin.numeroLote.ToUpper.Contains(txtBNumeroLote.Text.ToUpper)))) _
                                Select ent).Distinct()

        If Registros IsNot Nothing Then
            _dgvLoad = (Registros.Count > 1)
            dgvResultadosBuscador.DataSource = Registros.ToList
            _dgvLoad = False
        End If

        If (Registros.Count > 1) Then dgvResultadosBuscador.ClearSelection()
    End Sub

    ''' <summary>
    ''' Se prepara el formulario para introducir un nuevo registro    
    ''' </summary>    
    Public Overrides Sub Nuevo()
        LimpiarDatos()
        Me.Estado = Quadralia.Enumerados.EstadoFormulario.Anhadiendo

        Dim Aux = New LoteEntrada
        Aux.fecha = DateTime.Now()

        Aux.UsuarioReference.EntityKey = Quadralia.Aplicacion.UsuarioConectado.EntityKey

        ' Lo a�ado al contexto
        Contexto.LotesEntrada.AddObject(Aux)
        Me.Registro = Aux
    End Sub

    ''' <summary>
    ''' Se carga el registro seleccionado el formulario
    ''' </summary>
    Private Sub Cargar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvResultadosBuscador.SelectionChanged
        If dgvResultadosBuscador.SelectedRows.Count = 1 Then
            Me.Registro = dgvResultadosBuscador.SelectedRows(0).DataBoundItem
        Else
            Me.Registro = Nothing
            LimpiarDatos()
        End If
    End Sub

    ''' <summary>
    ''' Carga el registro actual del formulario
    ''' </summary>
    Private Sub CargarRegistro()
        CargarRegistro(Me.Registro)
    End Sub

    ''' <summary>
    ''' Se carga un registro en el formulario
    ''' </summary>
    Private Sub CargarRegistro(ByVal Instancia As LoteEntrada)
        ' Si el DGV est� vinculando un origen de datos, no cargo nada
        If _dgvLoad Then Exit Sub

        ' Limpio el formulario y cargo el registro
        LimpiarDatos()
        ControlarBotones()

        If Instancia Is Nothing Then Exit Sub

        With Instancia
            txtCodigo.Text = .codigo
            dtpFecha.ValueNullable = .fecha
            If .Proveedor IsNot Nothing Then txtProveedor.Item = .Proveedor
            If Not String.IsNullOrEmpty(.observaciones) Then htmlObservaciones.Text = .observaciones
            If .Lineas IsNot Nothing Then dgvLineas.DataSource = .Lineas
            chkReactivarRegistro.Visible = .fechaBaja.GetHashCode

            Me.Text = String.Format("{0} [{1}]", Me.Tag, .codigo)
            Me.txtTotal.Text = Math.Round(.TotalPeso(), 2).ToString()
        End With

        RefrescarAlbaranes()
        _Registro = Instancia
    End Sub

    ''' <summary>
    ''' Preparamos el formulario para modificar un registro cargado
    ''' </summary>
    Public Overrides Sub Modificar()
        If Me.Registro IsNot Nothing 
            Me.Estado = Quadralia.Enumerados.EstadoFormulario.Editando
        End If
    End Sub

    ''' <summary>
    ''' Eliminaci�n l�gica del registro
    ''' </summary>
    Public Overrides Sub Eliminar()
        If Registro IsNot Nothing Then
            If Quadralia.Aplicacion.MostrarMessageBox(My.Resources.ConfirmarEliminacion, My.Resources.TituloConfirmarEliminacion, MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
                ' Lo elimino del contexto
                Me.Registro.fechaBaja = DateTime.Now
                Me.Contexto.SaveChanges()

                If dgvResultadosBuscador.SelectedRows.Count > 0 Then Buscar(Nothing, Nothing) Else Cargar(Nothing, Nothing)
            End If
        End If
    End Sub

    ''' <summary>
    ''' Cancela la edici�n del registro
    ''' </summary>
    Public Overrides Sub Cancelar()
        If Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera Then Exit Sub

        ' Cambio el estado del formulario
        Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera

        ' Limpio los datos y desecho los cambios
        Quadralia.Linq.CancelarCambios(Me.Contexto)

        ' Limpio los datos
        LimpiarDatos()

        ' Cargo el registro seleccionado
        Cargar(Nothing, Nothing)
    End Sub

    ''' <summary>
    ''' Se guardan los datos en la base de datos
    ''' </summary>
    ''' <param name="MostrarMensaje">Indica si se muestra un mensaje con el resultado de la funci�n</param>
    Public Overrides Function Guardar(Optional ByVal MostrarMensaje As Boolean = True) As Boolean
        If Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera Then Return True

        ' Valido los controles
        Me.ValidateChildren(ValidationConstraints.Visible + ValidationConstraints.Enabled)

        If epErrores.HasErrors OrElse Not DGVValido() Then
            If MostrarMensaje Then Quadralia.Aplicacion.MostrarMessageBox(My.Resources.ErroresEncontrados, My.Resources.TituloErroresEncontrados, MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        End If

        ' Guardo los datos en el registro
        With Registro
            .codigo = txtCodigo.Text
            If IsDBNull(dtpFecha.ValueNullable) Then .fecha = Nothing Else .fecha = dtpFecha.Value
            If txtProveedor.Item Is Nothing Then .Proveedor = Nothing Else .Proveedor = txtProveedor.Item
            .observaciones = htmlObservaciones.Text

            If chkReactivarRegistro.Visible AndAlso chkReactivarRegistro.Checked Then .fechaBaja = Nothing
        End With

        Try
            ' Elimino posibles errores
            For Each entrada As Objects.ObjectStateEntry In Contexto.ObjectStateManager.GetObjectStateEntries(EntityState.Added)
                If entrada.Entity IsNot Nothing AndAlso TypeOf (entrada.Entity) Is LoteEntradalinea AndAlso DirectCast(entrada.Entity, LoteEntradalinea).LoteEntrada Is Nothing Then Contexto.Detach(entrada.Entity)
            Next

            ' Guardo el registro en base de datos
            Contexto.SaveChanges()

            ' Si estoy a�adiendo, obtengo un n�mero de Albar�n
            If Me.Estado = Quadralia.Enumerados.EstadoFormulario.Anhadiendo AndAlso String.IsNullOrEmpty(txtCodigo.Text) Then
                Dim Aux As New Data.Objects.ObjectParameter("codigoEnt", Registro.codigo)
                Dim Resultado = Contexto.CalcularCodigoEntrada(Registro.id, Aux)
                Registro.codigo = Aux.Value
                Contexto.AcceptAllChanges()
            End If

            ' Si estoy a�adiendo, obtengo un n�mero de Albar�n
            If MostrarMensaje Then Quadralia.Aplicacion.MostrarMensajeNtfIco(My.Resources.TituloDatosGuardatos, My.Resources.DatosGuardados, ToolTipIcon.Info)

            ' Restablezco el formulario
            Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera
            LimpiarDatos()
            CargarRegistro()

            Return True
        Catch ex As Exception
            If MostrarMensaje Then Quadralia.Aplicacion.InformarError(ex)
            Return False
        End Try
    End Function
#End Region
#Region " RESTO DE FUNCIONES "
    ''' <summary>
    ''' Muestra el aviso preconfigurado cuando se selecciona el proveedor
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub MostrarAvisoProveedor() Handles txtProveedor.ItemChanged
        If Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera Then Exit Sub
        If txtProveedor.Item Is Nothing Then Exit Sub

        With DirectCast(txtProveedor.Item, Proveedor)
            If .alerta Is Nothing Then Exit Sub
            If String.IsNullOrEmpty(.alerta) Then Exit Sub

            ' Muestro el mensaje de alerta
            cNotificationManager.Instance.MostrarAviso(.razonSocial, .alerta, cNotificationManager.TipoVentana.Aviso_Proveedor)
        End With
    End Sub

    ''' <summary>
    ''' Oculta / muestra el panel de b�squeda
    ''' </summary>
    Private Sub OcultarBusqueda(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOcultarBusqueda.Click
        Quadralia.KryptonForms.EsconderPanel(splSeparador, hdrBusqueda)
    End Sub

    ''' <summary>
    ''' Ordena los resultados del DGV de Resultados   
    ''' </summary>
    Private Sub OrdenarResultados(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvResultadosBuscador.ColumnHeaderMouseClick
        Quadralia.Formularios.Funcionalidad.OrdenarDGV(Of LoteEntrada)(dgvResultadosBuscador, e)
    End Sub

    ''' <summary>
    ''' Cuando el usuario hace doble click en el panel, desencadenamos la misma funci�n que si hiciera click en el bot�n
    ''' </summary>
    Private Sub OcultarBusquedaDobleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hdrBusqueda.DoubleClick
        ' Hay que hacerlo as�, no sirve con llamar a la funci�n directamente. si no, el bot�n del header se descontrola y desaparece el interior del panel
        btnOcultarBusqueda.PerformClick()
    End Sub

    ''' <summary>
    ''' Lanzamos la b�squeda de registros si el usuario presiona ENTER en los campos de par�metros
    ''' </summary>
    Private Sub ManejadorBusquedasTexto(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtBcodigo.KeyDown, dtpBFechaFin.KeyDown, dtpBFechaInicio.KeyDown, txtBProveedor.KeyDown, txtBEspecie.KeyDown, cboBZonaFAO.KeyDown, cboBSubZona.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.Handled = True
            Buscar(Nothing, Nothing)
        End If
    End Sub

    ''' <summary>
    ''' Evita que salga el molesto error del DGV
    ''' </summary>
    Private Sub EvitarErrores(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvLineas.DataError, dgvResultadosBuscador.DataError, dgvLotesSalida.DataError
        e.Cancel = True
    End Sub

    ''' <summary>
    ''' Pone el formulario en modo edici�n cuando el usuario hace doble click sobre un resultado del buscador
    ''' </summary>
    Private Sub ActivarEdicionRegistro(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvResultadosBuscador.CellDoubleClick, dgvResultadosBuscador.CellContentDoubleClick
        If e.RowIndex > -1 Then
            dgvResultadosBuscador.Rows(e.RowIndex).Selected = True
            Modificar()
        End If
    End Sub

    ''' <summary>
    ''' Nos desplaza a traves de los controles al pulsar enter
    ''' </summary>
    Private Sub EnterComoTab(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.Enter AndAlso (Me.ActiveControl Is Nothing OrElse Me.ActiveControl.Parent Is Nothing OrElse Not TypeOf (Me.ActiveControl.Parent) Is KryptonTextBox OrElse Not DirectCast(Me.ActiveControl.Parent, KryptonTextBox).Multiline) Then
            e.Handled = True
            SendKeys.Send("{TAB}")
        End If
    End Sub

    ''' <summary>
    ''' Control de las teclas de acci�n del usuario
    ''' </summary>
    Private Sub ControlTeclas(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvLineas.KeyDown
        If dgvLineas.CurrentCell Is Nothing OrElse dgvLineas.CurrentCell.RowIndex = -1 Then Exit Sub
        If e.KeyCode <> Keys.F6 AndAlso Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera Then Exit Sub

        Select Case e.KeyCode
            Case Keys.Delete
                If dgvLineas.IsCurrentCellInEditMode Then Exit Sub
                If dgvLineas.CurrentCell.OwningRow.IsNewRow Then Exit Sub
                e.Handled = True
                EliminarRegistro(dgvLineas, New DataGridViewRowCancelEventArgs(dgvLineas.CurrentCell.OwningRow))

            Case Keys.F3
                If dgvLineas.CurrentCell.ColumnIndex = colEspecie.Index Then
                    e.Handled = True
                    dgvLineas.BeginEdit(True)
                    BuscarEspecies(Nothing, Nothing)
                End If
            Case Keys.F6
                If Not Quadralia.Aplicacion.PuedeAccederA(Quadralia.Aplicacion.Permisos.LOTES_SALIDA) Then Exit Sub
                If dgvLineas.CurrentCell.OwningRow.IsNewRow Then Exit Sub
                If dgvLineas.CurrentCell.OwningRow.DataBoundItem Is Nothing Then Exit Sub
                If DirectCast(dgvLineas.CurrentCell.OwningRow.DataBoundItem, LoteEntradalinea).LotesSalida.Count <= 0 Then Exit Sub

                If DirectCast(dgvLineas.CurrentCell.OwningRow.DataBoundItem, LoteEntradalinea).LotesSalida.Count = 1 Then
                    Dim Formulario As frmLoteSalida = Quadralia.Aplicacion.AbrirFormulario(Me.ParentForm, "Escritorio.frmLoteSalida", Quadralia.Aplicacion.Permisos.LOTES_SALIDA, My.Resources.Salida_16)
                    Formulario.IdRegistro = DirectCast(dgvLineas.CurrentCell.OwningRow.DataBoundItem, LoteEntradalinea).LotesSalida(0).id
                    Formulario.MarcarLineaEntrada(DirectCast(dgvLineas.CurrentCell.OwningRow.DataBoundItem, LoteEntradalinea).id)
                Else
                    Dim FormularioVisor As New frmVisorLoteSalida()
                    FormularioVisor.FormularioPrincipal = Me.ParentForm
                    FormularioVisor.Registro = DirectCast(dgvLineas.CurrentCell.OwningRow.DataBoundItem, LoteEntradalinea)
                    FormularioVisor.ShowDialog()
                End If
        End Select
    End Sub

    Private Sub BuscarEspecies(ByVal sender As Object, ByVal e As EventArgs)
        Dim formulario As New frmSeleccionarEspecies(Me.Contexto)

        If formulario.ShowDialog() = DialogResult.OK AndAlso formulario.Items.Count > 0 Then
            ' Guardo el �ndice actual para luego restaura la celda
            dgvLineas.EditingControl.Text = formulario.Items(0).denominacionComercial
            If dgvLineas.CurrentRow IsNot Nothing AndAlso dgvLineas.CurrentRow.DataBoundItem IsNot Nothing Then DirectCast(dgvLineas.CurrentRow.DataBoundItem, LoteEntradalinea).Especie = formulario.Items(0)
            dgvLineas.EndEdit()
        End If
    End Sub

    ''' <summary>
    ''' Control de las teclas de acci�n del usuario
    ''' </summary>
    Private Sub ControlTeclasBuscador(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvResultadosBuscador.KeyDown
        If Me.Registro Is Nothing Then Exit Sub

        Select Case e.KeyCode
            Case Keys.Delete
                e.Handled = True
                Eliminar()
            Case Keys.F2, Keys.Enter
                e.Handled = True
                Modificar()
        End Select
    End Sub

    ''' <summary>
    ''' Lanza la b�squeda parcial del registro al pulsar F3    
    ''' </summary>
    Private Sub BuscarEnEdicionTextboxColumn(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode <> Keys.F3 Then Exit Sub
        If dgvLineas.CurrentCell Is Nothing OrElse dgvLineas.EditingControl Is Nothing Then Exit Sub
        If TypeOf (sender) Is KryptonTextBox AndAlso DirectCast(sender, KryptonTextBox).ButtonSpecs.Count = 1 Then DirectCast(sender, KryptonTextBox).ButtonSpecs(0).PerformClick()
    End Sub

    ''' <summary>
    ''' Carga los valores por defecto de las lineas
    ''' </summary>
    Private Sub ValoresPorDefecto(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewRowEventArgs) Handles dgvLineas.DefaultValuesNeeded
        e.Row.Cells(colRefEspecie.Index).Value = (From esp As Especie In Me.Contexto.Especies Where esp.predeterminada Select esp).FirstOrDefault
        e.Row.Cells(colZonaFao.Index).Value = (From fao As ZonaFao In Me.Contexto.ZonasFao Where fao.predeterminada Select fao).FirstOrDefault
        e.Row.Cells(colEntradaSubzona.Index).Value = (From sbz As SubZona In Me.Contexto.SubZonas Where sbz.predeterminada Select sbz).FirstOrDefault
        e.Row.Cells(colPresentacion.Index).Value = (From pre As Presentacion In Me.Contexto.FormasPresentacion Where pre.predeterminada Select pre).FirstOrDefault
        e.Row.Cells(colProduccion.Index).Value = (From pro As MetodoProduccion In Me.Contexto.MetodosProduccion Where pro.predeterminada Select pro).FirstOrDefault
        e.Row.Cells(colEntradaBarco.Index).Value = (From bar As Barco In Me.Contexto.Barcos Where bar.predeterminada Select bar).FirstOrDefault

        If Not IsDBNull(dtpFecha.ValueNullable) Then
            e.Row.Cells(colFechaDesembarco.Index).Value = dtpFecha.Value
        Else
            e.Row.Cells(colFechaDesembarco.Index).Value = DateTime.Now
        End If
    End Sub
#End Region
#Region " DESGLOSE "
    ''' <summary>
    ''' Pido confirmaci�n antes de eliminar el registro
    ''' </summary>
    Private Sub EliminarRegistro(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles dgvLineas.UserDeletingRow
        e.Cancel = True
        If Quadralia.Aplicacion.MostrarMessageBox("�Desea realmente eliminar la l�nea seleccionada?", "Confirmar Eliminaci�n", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.Yes Then
            dgvLineas.CurrentCell = Nothing
            dgvLineas.CommitEdit(DataGridViewDataErrorContexts.CurrentCellChange)
            If e.Row.DataBoundItem IsNot Nothing Then Contexto.DeleteObject(e.Row.DataBoundItem)
            If (_Registro IsNot Nothing) Then
                txtTotal.Text = Math.Round(_Registro.TotalPeso, 2).ToString()
            Else
                txtTotal.Clear()
            End If
        End If
    End Sub

    ''' <summary>
    ''' Marca la linea especificada en el DGV
    ''' </summary>
    ''' <param name="IdLinea"></param>
    Public Sub MarcarLinea(ByVal IdLinea As Long)
        ' Validaciones
        If IdLinea <= 0 Then Exit Sub
        If dgvLineas.RowCount < 1 Then Exit Sub

        ' Recorro las lineas
        For Each Fila As DataGridViewRow In dgvLineas.Rows
            If Fila.DataBoundItem Is Nothing Then Continue For

            ' Si encuentro la l�nea deseada, la marco
            If DirectCast(Fila.DataBoundItem, LoteEntradalinea).id = IdLinea Then
                Fila.DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 140, 0)
                Exit Sub
            End If
        Next
    End Sub
#End Region
#Region " VALIDACIONES "
    ''' <summary>
    ''' Verifica que se haya introducido texto en un textbox marcado como obligatorio
    ''' </summary>
    Private Sub ValidarTextBoxObligatorio(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs)
        If Not TypeOf (sender) Is KryptonTextBox Then Exit Sub

        If Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera AndAlso String.IsNullOrEmpty(DirectCast(sender, KryptonTextBox).Text) Then
            epErrores.SetError(sender, My.Resources.TextBoxObligatorio)
        Else
            epErrores.SetError(sender, "")
        End If
    End Sub

    ''' <summary>
    ''' Verifica que se haya seleccionado una fecha del DTP marcado como obligatorio
    ''' </summary>
    Private Sub ValidarDTPObligatorio(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles dtpFecha.Validating
        If Not TypeOf (sender) Is KryptonDateTimePicker Then Exit Sub

        If Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera AndAlso IsDBNull(DirectCast(sender, KryptonDateTimePicker).ValueNullable) Then
            epErrores.SetError(sender, My.Resources.DTPObligatorio)
        Else
            epErrores.SetError(sender, "")
        End If
    End Sub

    ''' <summary>
    ''' Obliga al usuario a seleccionar un cliente antes de continuar
    ''' </summary>
    Private Sub ValidarTxtBuscador(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtProveedor.Validating
        If Not TypeOf (sender) Is cTextboxBuscador Then Exit Sub

        If Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera AndAlso DirectCast(sender, cTextboxBuscador).Item Is Nothing Then
            epErrores.SetError(sender, My.Resources.TextBoxObligatorio)
        Else
            epErrores.SetError(sender, "")
        End If
    End Sub

    ''' <summary>
    ''' Verifica que se haya hecho una seleccion en un combobox marcado como obligatorio
    ''' </summary>
    Private Sub ComboObligatorio(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs)
        If Not TypeOf (sender) Is KryptonComboBox Then Exit Sub

        If Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera AndAlso DirectCast(sender, KryptonComboBox).SelectedIndex = -1 Then
            epErrores.SetError(sender, My.Resources.ComboObligatorio)
        Else
            epErrores.SetError(sender, "")
        End If
    End Sub

    Private Sub CeldasSoloNumeros(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvLineas.EditingControlShowing
        RemoveHandler e.Control.KeyPress, AddressOf SoloDecimales
        RemoveHandler e.Control.KeyDown, AddressOf BuscarEnEdicionTextboxColumn
        If TypeOf (e.Control) Is KryptonTextBox Then DirectCast(e.Control, KryptonTextBox).ReadOnly = False

        If dgvLineas.CurrentCell Is Nothing Then Exit Sub

        Select Case dgvLineas.CurrentCell.ColumnIndex
            Case colCantidad.Index
                AddHandler e.Control.KeyPress, AddressOf SoloDecimales

                e.Control.Text = e.Control.Text.Replace("%", "").Trim
                e.Control.Text = e.Control.Text.Replace("�", "").Trim

            Case colEspecie.Index
                ' La bloqueo para que no puedan escribir directamente en ella
                DirectCast(e.Control, KryptonTextBox).ReadOnly = True

                Dim botonBuscar As New ButtonSpecAny

                With botonBuscar
                    .Image = My.Resources.Buscar_16
                    .Style = PaletteButtonStyle.InputControl
                    .ToolTipBody = "Buscar una especie"
                    AddHandler .Click, AddressOf BuscarEspecies
                End With

                DirectCast(e.Control, KryptonTextBox).ButtonSpecs.Add(botonBuscar)
                AddHandler DirectCast(e.Control, KryptonTextBox).KeyDown, AddressOf BuscarEnEdicionTextboxColumn

            Case colZonaFao.Index
                With DirectCast(e.Control, ComboBox)
                    If dgvLineas.CurrentCell.Value IsNot Nothing Then
                        .SelectedItem = dgvLineas.CurrentCell.Value
                    Else
                        .SelectedItem = (From fao As ZonaFao In Me.Contexto.ZonasFao Where fao.predeterminada Select fao).FirstOrDefault
                    End If
                End With

            Case colEntradaSubzona.Index
                With DirectCast(e.Control, ComboBox)
                    If dgvLineas.CurrentCell.Value IsNot Nothing Then
                        .SelectedItem = dgvLineas.CurrentCell.Value
                    Else
                        .SelectedItem = (From sbz As SubZona In Me.Contexto.SubZonas Where sbz.predeterminada Select sbz).FirstOrDefault
                    End If
                End With

            Case colPresentacion.Index
                With DirectCast(e.Control, ComboBox)
                    If dgvLineas.CurrentCell.Value IsNot Nothing Then
                        .SelectedItem = dgvLineas.CurrentCell.Value
                    Else
                        .SelectedItem = (From pre As Presentacion In Me.Contexto.FormasPresentacion Where pre.predeterminada Select pre).FirstOrDefault
                    End If
                End With

            Case colProduccion.Index
                With DirectCast(e.Control, ComboBox)
                    If dgvLineas.CurrentCell.Value IsNot Nothing Then
                        .SelectedItem = dgvLineas.CurrentCell.Value
                    Else
                        .SelectedItem = (From pro As MetodoProduccion In Me.Contexto.MetodosProduccion Where pro.predeterminada Select pro).FirstOrDefault
                    End If
                End With

            Case colEntradaBarco.Index
                With DirectCast(e.Control, ComboBox)
                    If dgvLineas.CurrentCell.Value IsNot Nothing Then
                        .SelectedItem = dgvLineas.CurrentCell.Value
                    Else
                        .SelectedItem = (From bar As Barco In Me.Contexto.Barcos Where bar.predeterminada Select bar).FirstOrDefault
                    End If
                End With
        End Select
    End Sub

    Private Sub SoloDecimales(ByVal sender As Object, ByVal e As KeyPressEventArgs)
        Quadralia.Validadores.QuadraliaValidadores.SoloNumerosYDecimales(sender, e)
    End Sub

    ''' <summary>
    ''' Valida que el usuario no pueda meter un c�digo ya existente o uno nulo
    ''' </summary>
    Private Sub ValidarCodigo(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtCodigo.Validating
        If Me.Estado = Quadralia.Enumerados.EstadoFormulario.Editando AndAlso String.IsNullOrEmpty(txtCodigo.Text) Then
            epErrores.SetError(txtCodigo, My.Resources.TextBoxObligatorio)
        ElseIf Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera AndAlso (From ent As LoteEntrada In Contexto.LotesEntrada Where ent.codigo.Trim().ToUpper() = txtCodigo.Text.Trim().ToUpper() AndAlso ent.id <> Registro.id Select ent).Count > 0 Then
            epErrores.SetError(txtCodigo, "El n�mero introducido se encuentra actualmente en uso. Por favor, rev�selo")
        Else
            epErrores.SetError(txtCodigo, "")
        End If
    End Sub

    ''' <summary>
    ''' Valida que se hayan introducido todos los datos correctamente
    ''' </summary>
    Private Sub ValidarFila(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvLineas.RowValidating
        Dim Fila As DataGridViewRow = dgvLineas.Rows(e.RowIndex)
        If Fila.IsNewRow Then Exit Sub
        If Fila.DataBoundItem Is Nothing Then Exit Sub
        Dim Linea As LoteEntradalinea = Fila.DataBoundItem

        ' Valido que haya introducido los datos minimos necesarios
        Fila.Cells(colCantidad.Index).ErrorText = IIf(Linea.cantidad <= 0, "Debe introducir una cantidad mayor que 0", "")
        Fila.Cells(colEspecie.Index).ErrorText = IIf(Linea.Especie Is Nothing, "Debe seleccionar una especie del listado diponible", "")
        Fila.Cells(colNumeroLote.Index).ErrorText = IIf(String.IsNullOrEmpty(Linea.numeroLote), "Debe introducir un n�mero de lote", "")
    End Sub

    ''' <summary>
    ''' Verifica que no haya errores en el DGV
    ''' </summary>
    Private Function DGVValido() As Boolean
        dgvLineas.CurrentCell = Nothing

        For Each Fila As DataGridViewRow In dgvLineas.Rows
            For Each Celda As DataGridViewCell In Fila.Cells
                If Not String.IsNullOrEmpty(Celda.ErrorText) Then Return False
            Next
        Next

        Return True
    End Function
#End Region
#Region " SEGURIDAD "
    ''' <summary>
    ''' Configura el formulario en base a los permisos del usuario
    ''' </summary>
    Private Sub ConfigurarEntorno()
    End Sub
#End Region
#Region " NAVEGABILIDAD "
#Region " ALBARANES SALIDA "
    Private Sub RefrescarAlbaranes()

        dgvLotesSalida.DataSource = Nothing
        dgvLotesSalida.Rows.Clear()

        ' Validaciones
        If Registro Is Nothing OrElse Registro.LotesSalida Is Nothing OrElse Registro.LotesSalida.Count = 0 Then
            Exit Sub
        End If

        dgvLotesSalida.DataSource = Me.Registro.LotesSalida.ToList()
        dgvLotesSalida.ClearSelection()
    End Sub

    Private Sub VerLoteSalida(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvLotesSalida.DoubleClick
        If Not cConfiguracionAplicacion.Instancia.PermitirNavegabilidad Then Exit Sub

        If dgvLotesSalida.SelectedRows.Count > 0 AndAlso Quadralia.Aplicacion.PuedeAccederA(Quadralia.Aplicacion.Permisos.LOTES_SALIDA) Then
            Dim FormularioAlbaran As frmLoteSalida = Quadralia.Aplicacion.AbrirFormulario(frmPrincipal, "Escritorio.frmLoteSalida", Quadralia.Aplicacion.Permisos.LOTES_SALIDA, My.Resources.Salida_32)
            FormularioAlbaran.IdRegistro = DirectCast(dgvLotesSalida.SelectedRows(0).DataBoundItem, LoteSalida).id
        End If
    End Sub

    ''' <summary>
    ''' Ordena los registros por la columna deseada
    ''' </summary>
    Private Sub OrdenarAlbaranes(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvLotesSalida.ColumnHeaderMouseClick
        Quadralia.Formularios.Funcionalidad.OrdenarDGV(Of LoteSalida)(dgvLotesSalida, e)
    End Sub
#End Region
#End Region
#Region " IMPRIMIR "
    ''' <summary>
    ''' Imprime el registro actual
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Imprimir()
        ' Validaciones
        If Me.Registro Is Nothing Then Exit Sub
        If Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera Then Exit Sub

        ' Imprimo la lista
        Dim Lista As New List(Of LoteEntrada)
        Lista.Add(Me.Registro)
        Imprimir(Lista)
    End Sub

    ''' <summary>
    ''' Imprime los registros especificados como par�metro de entrada
    ''' </summary>
    Private Sub Imprimir(ByVal Registros As List(Of LoteEntrada))
        Dim Formulario As frmVisorInforme = Quadralia.Aplicacion.AbrirInforme(Me.ParentForm, frmVisorInforme.Informe.Entrada, Quadralia.Aplicacion.Permisos.LOTES_ENTRADA)
        If Formulario Is Nothing Then Exit Sub

        Formulario.Datos = Registros
        Formulario.RefrescarDatos()
    End Sub

    ''' <summary>
    ''' Imprime el registro actual
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ImprimirSalidas()
        ' Validaciones
        If Me.Registro Is Nothing Then Exit Sub
        If Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera Then Exit Sub
        If Me.Registro.LotesSalida Is Nothing OrElse Me.Registro.LotesSalida.Count = 0 Then Exit Sub

        ' Imprimo la lista
        Dim Lista As New List(Of LoteEntrada)
        Lista.Add(Me.Registro)
        ImprimirSalidas(Lista)
    End Sub

    ''' <summary>
    ''' Imprime los registros especificados como par�metro de entrada
    ''' </summary>
    Private Sub ImprimirSalidas(ByVal Registros As List(Of LoteEntrada))
        Dim Formulario As frmVisorInforme = Quadralia.Aplicacion.AbrirInforme(Me.ParentForm, frmVisorInforme.Informe.ListadoSalida, Quadralia.Aplicacion.Permisos.LOTES_ENTRADA)
        If Formulario Is Nothing Then Exit Sub

        Formulario.Datos = Registros
        Formulario.RefrescarDatos()
    End Sub

    Private Sub dgvLineas_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dgvLineas.CellEndEdit
        If (e.ColumnIndex = colCantidad.Index) Then
            If (_Registro IsNot Nothing) Then
                txtTotal.Text = Math.Round(_Registro.TotalPeso, 2).ToString()
                If (dgvLineas.Rows.Count > (_Registro.Lineas.Count + 1)) Then
                    ''caso de que est� a�adiendo l�nea nueva
                    txtTotal.Text = Math.Round(Convert.ToDecimal(txtTotal.Text) + Convert.ToDecimal(dgvLineas.CurrentCell.Value), 2).ToString()
                End If
            Else
                txtTotal.Clear()
            End If
        End If
    End Sub

#End Region
End Class
