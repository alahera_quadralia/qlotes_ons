Public Class frmLoteSalida
#Region " DECLARACIONES "
    Private _EstadoFormulario As Quadralia.Enumerados.EstadoFormulario = Quadralia.Enumerados.EstadoFormulario.Espera  ' Indica el estado en el que se encuentra el formulario
    Private _Registro As LoteSalida = Nothing
    Private _Contexto As Entidades = Nothing
    Private _dgvDesgloseEliminando As Boolean = False

    Private _dgvLoad As Boolean = False ' Bandera para evitar que se cargue la primera fila cuando se asigna un origen de datos al DGV
#End Region
#Region " PROPIEDADES "
    Public Overrides Property Estado() As Quadralia.Enumerados.EstadoFormulario
        Get
            Return _EstadoFormulario
        End Get
        Set(ByVal value As Quadralia.Enumerados.EstadoFormulario)
            _EstadoFormulario = value

            'Para evitar refrescos raros, cuando ponemos el formulario en estado de espera, volvemos a la pesta�a de datos
            hdrBusqueda.Panel.Enabled = (value = Quadralia.Enumerados.EstadoFormulario.Espera)
            htmlObservaciones.Enabled = (value <> Quadralia.Enumerados.EstadoFormulario.Espera)
            tbpLotesEntrada.Visible = (value <> Quadralia.Enumerados.EstadoFormulario.Espera)
            tbpEtiquetas.Visible = (value <> Quadralia.Enumerados.EstadoFormulario.Espera)

            ' Desactivo todo menos el dgv de lineas
            For Each ctrl As Control In tblOrganizadorDatos.Controls
                ctrl.Enabled = ctrl IsNot hdrCabeceraEmpaquetado AndAlso (value <> Quadralia.Enumerados.EstadoFormulario.Espera)
            Next

            hdrCabeceraEmpaquetado.Enabled = True


            ' Configuro DGV
            For Each Fila As DataGridViewRow In dgvEmpaquetados.Rows
                Fila.DefaultCellStyle = New DataGridViewCellStyle
            Next

            ' Tengo que establecer el foco en un control para que no quede "muerto" el ribbon
            If value = Quadralia.Enumerados.EstadoFormulario.Espera Then
                Me.ActiveControl = txtBcodigo.TextBox
            Else
                Me.ActiveControl = txtCliente
            End If

            ConfigurarEntorno()
            ControlarBotones()
        End Set
    End Property

    ''' <summary>
    ''' Registro que se muestra actualmente en el formulario
    ''' </summary>
    Public Property Registro As LoteSalida
        Get
            Return _Registro
        End Get
        Set(ByVal value As LoteSalida)
            On Error Resume Next
            _Registro = value

            If value IsNot Nothing Then If Not value.Empaquetados.IsLoaded Then value.Empaquetados.Load()

            CargarRegistro(value)
        End Set
    End Property

    ''' <summary>
    ''' Contexto que utilizar� el formulario
    ''' </summary>
    Public Property Contexto As Entidades
        Get
            If _Contexto Is Nothing Then
                _Contexto = cDAL.Instancia.getContext()
                _Contexto.ContextOptions.LazyLoadingEnabled = True
            End If
            Return _Contexto
        End Get
        Set(ByVal value As Entidades)
            _Contexto = value
        End Set
    End Property

    ''' <summary>
    ''' Obtiene / establece el id del elemento cargado en el formulario
    ''' </summary>
    Public Property IdRegistro As Long
        Get
            If Me.Registro Is Nothing Then Return -1 Else Return Me.Registro.id
        End Get
        Set(ByVal value As Long)
            Me.Registro = (From ent As LoteSalida In Me.Contexto.LotesSalida Where ent.id = value).FirstOrDefault
        End Set
    End Property
#End Region
#Region " IMPLEMENTACIONES NECESARIAS DEL FORMULARIO HIJO "
    ''' <summary>
    ''' Tab del ribbon asociado a este formulario
    ''' </summary>
    Public Overrides ReadOnly Property Tab() As ComponentFactory.Krypton.Ribbon.KryptonRibbonTab
        Get
            Return frmPrincipal.tabLotesSalida
        End Get
    End Property
#End Region
#Region " LIMPIEZA "
    ''' <summary>
    ''' Limpia los controles relacionados con la b�squeda de registros
    ''' </summary>
    Private Sub LimpiarBusqueda() Handles btnLimpiarBusqueda.Click
        txtBcodigo.Text = String.Empty
        dtpBFechaInicio.Clear()
        dtpBFechaFin.Clear()
        txtBCliente.Clear()
        txtBEtiqueta.Clear()
        txtBLoteSalida.Clear()

        chkBRegistrosEliminados.Checked = False

        dgvResultadosBuscador.DataSource = Nothing
        dgvResultadosBuscador.Rows.Clear()
    End Sub

    ''' <summary>
    ''' Limpia los controles relacionados con la edicion de registros
    ''' </summary>
    Private Sub LimpiarDatos()
        txtCodigo.Clear()
        txtCliente.Clear()
        dtpFecha.ValueNullable = DateTime.Now
        chkReactivarRegistro.Checked = False
        cboDireccionCliente.Clear()

        dgvEmpaquetados.DataSource = Nothing

        htmlObservaciones.Clear()

        LimpiarLotesSalida()
        LimpiarEtiquetas()

        epErrores.Clear()
    End Sub

    Private Sub LimpiarLotesSalida()
        dgvLotesEntrada.DataSource = Nothing
        dgvLotesEntrada.Rows.Clear()
    End Sub

    Private Sub LimpiarEtiquetas()
        dgvEtiquetas.DataSource = Nothing
        dgvEtiquetas.Rows.Clear()
    End Sub

    ''' <summary>
    ''' Borra todos los controles del formulario
    ''' </summary>
    Private Sub LimpiarTodo()
        LimpiarBusqueda()
        LimpiarDatos()
    End Sub
#End Region
#Region " CONTROL DE BOTONES "
    Private Sub ControlarBotones()
        ControlarRibbon()
        ControlarBotonesEmpaquetados()
    End Sub

    ''' <summary>
    ''' Controla el cambio de botones del formulario principal
    ''' </summary>
    Private Sub ControlarRibbon()
        If Me.ParentForm Is Nothing Then Exit Sub
        If Not TypeOf Me.ParentForm Is frmPrincipal Then Exit Sub

        With DirectCast(Me.ParentForm, frmPrincipal)
            .krgtLotesSalidaAccionesEdicion.Visible = (Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera)
            .krgtLotesSalidaAccionesEspera.Visible = Not .krgtLotesSalidaAccionesEdicion.Visible
            .mnuRapidoGuardar.Enabled = .krgtLotesSalidaAccionesEdicion.Visible

            .btnLotesSalidaAccionesModificar.Enabled = (Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera) AndAlso Registro IsNot Nothing
            .btnLotesSalidaAccionesEliminar.Enabled = .btnLotesSalidaAccionesModificar.Enabled

            .krgtLotesSalidaMasAcciones.Visible = Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera
            .btnLotesSalidaAccionesImprimir.Enabled = Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera AndAlso Me.Registro IsNot Nothing
            .btnLotesSalidaAccionesImprimirEtiquetas.Enabled = Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera AndAlso Me.Registro IsNot Nothing AndAlso dgvEtiquetas.SelectedRows.Count > 0
        End With
    End Sub

    ''' <summary>
    ''' Controla los botones relativos a las etiquetas
    ''' </summary>
    Private Sub ControlarBotonesEmpaquetados()
        btnEtiquetasLector.Enabled = IIf(Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera, ButtonEnabled.True, ButtonEnabled.False)
        btnEtiquetasNuevo.Enabled = IIf(Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera, ButtonEnabled.True, ButtonEnabled.False)
        btnEtiquetasEliminar.Enabled = IIf(Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera AndAlso dgvEmpaquetados.SelectedRows.Count > 0, ButtonEnabled.True, ButtonEnabled.False)
    End Sub
#End Region
#Region " RUTINA DE INICIO / FIN Y CARGA DE DATOS MAESTROS "
    ''' <summary>
    ''' Rutina de inicio del formulario
    ''' </summary>
    Private Sub Inicio(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Tag = Me.Text
        tabGeneral.SelectedIndex = 0

        Quadralia.Formularios.AutoTabular(Me)

        ' Cargamos los datos maestros
        CargarDatosMaestros()
        ConfigurarEntorno()

        ' Limpieza
        LimpiarTodo()
        tabGeneral.SelectedIndex = 0
        Me.Registro = Nothing

        ' Oculto el buscador
        If Not cConfiguracionAplicacion.Instancia.BuscadoresAbiertos Then OcultarBusquedaDobleClick(hdrBusqueda, Nothing)

        ' No quiero nuevas columnas
        dgvResultadosBuscador.AutoGenerateColumns = False
        dgvEmpaquetados.AutoGenerateColumns = False
        dgvLotesEntrada.AutoGenerateColumns = False
        dgvEtiquetas.AutoGenerateColumns = False

        Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera
    End Sub

    ''' <summary>
    ''' Carga los datos maestros
    ''' </summary>
    Public Sub CargarDatosMaestros()
        ' Cargar Expedidores
        With cboExpedidor
            .DataSource = Nothing
            .Items.Clear()

            .DisplayMember = "razonSocial"
            .ValueMember = "id"
            .DataSource = (From exp As Expedidor In Contexto.Expedidores Where Not exp.fechaBaja.HasValue Order By exp.razonSocial Select exp)

            .SelectedItem = Nothing
            .SelectedIndex = -1
        End With
    End Sub

    ''' <summary>
    ''' Carga las direcciones del cliente
    ''' </summary>
    Private Sub CargarDireccionesCliente()
        cboDireccionCliente.DataSource = Nothing
        cboDireccionCliente.Items.Clear()

        If txtCliente.Item IsNot Nothing Then
            Dim ClienteCargado As Cliente = txtCliente.Item

            ' Obtengo las direcciones del cliente y a�ado la actual, en caso de ser necesario
            Dim ListaDirecciones = (From dir As DireccionCliente In ClienteCargado.Direcciones
                                    Order By dir.principal Descending
                                    Select dir).ToList

            cboDireccionCliente.DisplayMember = "DireccionConTipo"
            cboDireccionCliente.ValueMember = "id"
            cboDireccionCliente.DataSource = ListaDirecciones

            ' Si me pasaron una direcci�n la selecciono por defecto, si no, no selecciono ninguna
            If ListaDirecciones.Count > 0 Then cboDireccionCliente.SelectedItem = ListaDirecciones(0)
        End If
    End Sub
#End Region
#Region " BUSQUEDA, CARGA DE Y EDICION DE REGISTROS "
    ''' <summary>
    ''' Realiza la b�squeda de registros coincidentes con los par�metros especificados
    ''' </summary>
    Private Sub Buscar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBBuscar.Click
        Dim FechaInicio As Nullable(Of DateTime) = Nothing
        Dim FechaFin As Nullable(Of DateTime) = Nothing
        Dim IdCliente As Nullable(Of Long) = Nothing
        Dim IdEtiqueta As Nullable(Of Long) = Nothing
        Dim LoteSalida As String = String.Empty
        Dim Aux As Integer = 0

        If Not IsDBNull(dtpBFechaInicio.ValueNullable) Then FechaInicio = dtpBFechaInicio.Value.Date + New TimeSpan(0, 0, 0)
        If Not IsDBNull(dtpBFechaFin.ValueNullable) Then FechaFin = dtpBFechaFin.Value.Date + New TimeSpan(23, 59, 59)
        If txtBCliente.Item IsNot Nothing Then IdCliente = DirectCast(txtBCliente.Item, Cliente).id
        If txtBEtiqueta.Item IsNot Nothing Then IdEtiqueta = DirectCast(txtBEtiqueta.Item, Etiqueta).id
        If Not String.IsNullOrEmpty(txtBLoteSalida.Text) Then LoteSalida = txtBLoteSalida.Text

        Dim registros As New List(Of LoteSalida)
        If String.IsNullOrEmpty(LoteSalida) Then

            registros = (From sal As LoteSalida In Contexto.LotesSalida
                         From emp As Empaquetado In sal.Empaquetados.DefaultIfEmpty
                         From etq As Etiqueta In emp.Etiquetas.DefaultIfEmpty
                         Where ((chkBRegistrosEliminados.Checked OrElse Not sal.fechaBaja.HasValue) _
                                    AndAlso (String.IsNullOrEmpty(txtBcodigo.Text) OrElse sal.codigo.ToUpper.Contains(txtBcodigo.Text.ToUpper)) _
                                    AndAlso (Not FechaInicio.HasValue OrElse sal.fecha >= FechaInicio.Value) _
                                    AndAlso (Not FechaFin.HasValue OrElse sal.fecha <= FechaFin.Value) _
                                    AndAlso (Not IdCliente.HasValue OrElse (sal.Cliente IsNot Nothing AndAlso sal.Cliente.id = IdCliente)) _
                                    AndAlso (Not IdEtiqueta.HasValue OrElse (etq IsNot Nothing AndAlso etq.id = IdEtiqueta.Value))
                                    )
                         Select sal).Distinct().ToList
        Else
            Dim etqs = (From sal As LoteSalida In Contexto.LotesSalida
                        From emp As Empaquetado In sal.Empaquetados.DefaultIfEmpty
                        From etq As Etiqueta In emp.Etiquetas.DefaultIfEmpty
                        Where ((chkBRegistrosEliminados.Checked OrElse Not sal.fechaBaja.HasValue) _
                                    AndAlso (String.IsNullOrEmpty(txtBcodigo.Text) OrElse sal.codigo.ToUpper.Contains(txtBcodigo.Text.ToUpper)) _
                                    AndAlso (Not FechaInicio.HasValue OrElse sal.fecha >= FechaInicio.Value) _
                                    AndAlso (Not FechaFin.HasValue OrElse sal.fecha <= FechaFin.Value) _
                                    AndAlso (Not IdCliente.HasValue OrElse (sal.Cliente IsNot Nothing AndAlso sal.Cliente.id = IdCliente)) _
                                    AndAlso (Not IdEtiqueta.HasValue OrElse (etq IsNot Nothing AndAlso etq.id = IdEtiqueta.Value))
                                    )
                        Select etq).Distinct().ToList
            Dim res = New List(Of LoteSalida)
            For Each etq In etqs
                If etq.LoteSalida = LoteSalida OrElse etq.LoteSalida.Contains(LoteSalida) Then
                    res.Add(etq.Empaquetado.LoteSalida)
                End If
            Next
            registros = res.Distinct.ToList
        End If
        If Registros IsNot Nothing Then
            _dgvLoad = (Registros.Count > 1)
            dgvResultadosBuscador.DataSource = Registros
            _dgvLoad = False
        End If

        If (Registros.Count > 1) Then dgvResultadosBuscador.ClearSelection()
    End Sub

    ''' <summary>
    ''' Se prepara el formulario para introducir un nuevo registro    
    ''' </summary>    
    Public Overrides Sub Nuevo()
        LimpiarDatos()
        Me.Estado = Quadralia.Enumerados.EstadoFormulario.Anhadiendo

        Dim Aux = New LoteSalida
        Aux.fecha = DateTime.Now()

        Aux.UsuarioReference.EntityKey = Quadralia.Aplicacion.UsuarioConectado.EntityKey
        Aux.Expedidor = (From exp As Expedidor In Contexto.Expedidores Where exp.predeterminada AndAlso Not exp.fechaBaja.HasValue Select exp).FirstOrDefault

        ' Lo a�ado al contexto
        Contexto.LotesSalida.AddObject(Aux)
        Me.Registro = Aux
    End Sub

    ''' <summary>
    ''' Se carga el registro seleccionado el formulario
    ''' </summary>
    Private Sub Cargar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvResultadosBuscador.SelectionChanged
        If dgvResultadosBuscador.SelectedRows.Count = 1 Then
            Me.Registro = dgvResultadosBuscador.SelectedRows(0).DataBoundItem
        Else
            Me.Registro = Nothing
            LimpiarDatos()
        End If
    End Sub

    ''' <summary>
    ''' Carga el registro actual del formulario
    ''' </summary>
    Private Sub CargarRegistro()
        CargarRegistro(Me.Registro)
    End Sub

    ''' <summary>
    ''' Se carga un registro en el formulario
    ''' </summary>
    Private Sub CargarRegistro(ByVal Instancia As LoteSalida)
        ' Si el DGV est� vinculando un origen de datos, no cargo nada
        If _dgvLoad Then Exit Sub

        ' Limpio el formulario y cargo el registro
        LimpiarDatos()
        ControlarBotones()

        If Instancia Is Nothing Then Exit Sub

        With Instancia
            txtCodigo.Text = .codigo
            dtpFecha.ValueNullable = .fecha
            If .Cliente IsNot Nothing Then txtCliente.Item = .Cliente
            If .Direccion IsNot Nothing Then cboDireccionCliente.SelectedValue = .Direccion.id Else cboDireccionCliente.SelectedIndex = -1
            If Not String.IsNullOrEmpty(.observaciones) Then htmlObservaciones.Text = .observaciones
            If .Expedidor IsNot Nothing Then cboExpedidor.SelectedItem = .Expedidor
            chkReactivarRegistro.Visible = .fechaBaja.GetHashCode

            Me.Text = String.Format("{0} [{1}]", Me.Tag, .codigo)
        End With

        RefrescarEmpaquetados()
        RefrescarLotesEntrada()
        RefrescarEtiquetas()
        _Registro = Instancia
    End Sub

    ''' <summary>
    ''' Preparamos el formulario para modificar un registro cargado
    ''' </summary>
    Public Overrides Sub Modificar()
        If Me.Registro IsNot Nothing Then
            Me.Estado = Quadralia.Enumerados.EstadoFormulario.Editando
        End If
    End Sub

    ''' <summary>
    ''' Eliminaci�n l�gica del registro
    ''' </summary>
    Public Overrides Sub Eliminar()
        If Registro IsNot Nothing Then
            If Quadralia.Aplicacion.MostrarMessageBox(My.Resources.ConfirmarEliminacion, My.Resources.TituloConfirmarEliminacion, MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
                ' Lo elimino del contexto
                Me.Registro.fechaBaja = DateTime.Now
                Me.Contexto.SaveChanges()

                If dgvResultadosBuscador.SelectedRows.Count > 0 Then Buscar(Nothing, Nothing) Else Cargar(Nothing, Nothing)
            End If
        End If
    End Sub

    ''' <summary>
    ''' Cancela la edici�n del registro
    ''' </summary>
    Public Overrides Sub Cancelar()
        If Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera Then Exit Sub

        ' Cambio el estado del formulario
        Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera

        ' Limpio los datos y desecho los cambios
        Quadralia.Linq.CancelarCambios(Me.Contexto)

        ' Limpio los datos
        LimpiarDatos()

        ' Cargo el registro seleccionado
        Cargar(Nothing, Nothing)
    End Sub

    ''' <summary>
    ''' Se guardan los datos en la base de datos
    ''' </summary>
    ''' <param name="MostrarMensaje">Indica si se muestra un mensaje con el resultado de la funci�n</param>
    Public Overrides Function Guardar(Optional ByVal MostrarMensaje As Boolean = True) As Boolean
        If Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera Then Return True

        ' Valido los controles
        Me.ValidateChildren(ValidationConstraints.Visible + ValidationConstraints.Enabled)

        If epErrores.HasErrors Then
            If MostrarMensaje Then Quadralia.Aplicacion.MostrarMessageBox(My.Resources.ErroresEncontrados, My.Resources.TituloErroresEncontrados, MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        End If

        ' Guardo los datos en el registro
        With Registro
            .codigo = txtCodigo.Text
            If IsDBNull(dtpFecha.ValueNullable) Then .fecha = Nothing Else .fecha = dtpFecha.Value
            If txtCliente.Item Is Nothing Then .Cliente = Nothing Else .Cliente = txtCliente.Item
            If cboDireccionCliente.SelectedIndex = -1 Then .Direccion = Nothing Else .Direccion = cboDireccionCliente.SelectedItem
            If cboExpedidor.SelectedIndex = -1 Then .Expedidor = Nothing Else .Expedidor = cboExpedidor.SelectedItem
            .observaciones = htmlObservaciones.Text

            If chkReactivarRegistro.Visible AndAlso chkReactivarRegistro.Checked Then .fechaBaja = Nothing
        End With

        Try
            ' Guardo el registro en base de datos
            Contexto.SaveChanges()

            ' Si estoy a�adiendo, obtengo un n�mero de Albar�n
            If Me.Estado = Quadralia.Enumerados.EstadoFormulario.Anhadiendo AndAlso String.IsNullOrEmpty(txtCodigo.Text) Then
                Dim Aux As New Data.Objects.ObjectParameter("codigoSal", Registro.codigo)
                Dim Resultado = Contexto.CalcularCodigoSalida(Registro.id, Aux)
                Registro.codigo = Aux.Value
                Contexto.AcceptAllChanges()
            End If

            ' Si estoy a�adiendo, obtengo un n�mero de Albar�n
            If MostrarMensaje Then Quadralia.Aplicacion.MostrarMensajeNtfIco(My.Resources.TituloDatosGuardatos, My.Resources.DatosGuardados, ToolTipIcon.Info)

            ' Restablezco el formulario
            Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera
            LimpiarDatos()
            CargarRegistro()

            Return True
        Catch ex As Exception
            If MostrarMensaje Then Quadralia.Aplicacion.InformarError(ex)
            Return False
        End Try
    End Function
#End Region
#Region " RESTO DE FUNCIONES "
    ''' <summary>
    ''' Muestra el aviso preconfigurado cuando se selecciona el proveedor
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub MostrarAvisoProveedor() Handles txtCliente.ItemChanged
        If txtCliente.Item Is Nothing Then Exit Sub

        ' Cargo las direcciones
        CargarDireccionesCliente()

        If Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera Then Exit Sub
        With DirectCast(txtCliente.Item, Cliente)
            If .alerta Is Nothing Then Exit Sub
            If String.IsNullOrEmpty(.alerta) Then Exit Sub

            ' Muestro el mensaje de alerta
            cNotificationManager.Instance.MostrarAviso(.razonSocial, .alerta, cNotificationManager.TipoVentana.Aviso_Cliente)
        End With
    End Sub

    ''' <summary>
    ''' Oculta / muestra el panel de b�squeda
    ''' </summary>
    Private Sub OcultarBusqueda(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOcultarBusqueda.Click
        Quadralia.KryptonForms.EsconderPanel(splSeparador, hdrBusqueda)
    End Sub

    ''' <summary>
    ''' Ordena los resultados del DGV de Resultados   
    ''' </summary>
    Private Sub OrdenarResultados(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvResultadosBuscador.ColumnHeaderMouseClick
        Quadralia.Formularios.Funcionalidad.OrdenarDGV(Of LoteSalida)(dgvResultadosBuscador, e)
    End Sub

    ''' <summary>
    ''' Cuando el usuario hace doble click en el panel, desencadenamos la misma funci�n que si hiciera click en el bot�n
    ''' </summary>
    Private Sub OcultarBusquedaDobleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hdrBusqueda.DoubleClick
        ' Hay que hacerlo as�, no sirve con llamar a la funci�n directamente. si no, el bot�n del header se descontrola y desaparece el interior del panel
        btnOcultarBusqueda.PerformClick()
    End Sub

    ''' <summary>
    ''' Lanzamos la b�squeda de registros si el usuario presiona ENTER en los campos de par�metros
    ''' </summary>
    Private Sub ManejadorBusquedasTexto(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cboExpedidor.KeyDown, txtBcodigo.KeyDown, txtBEtiqueta.KeyDown, txtBCliente.KeyDown, dtpBFechaInicio.KeyDown, dtpBFechaFin.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.Handled = True
            Buscar(Nothing, Nothing)
        End If
    End Sub

    ''' <summary>
    ''' Evita que salga el molesto error del DGV
    ''' </summary>
    Private Sub EvitarErrores(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvEtiquetas.DataError, dgvLotesEntrada.DataError, dgvResultadosBuscador.DataError, dgvEmpaquetados.DataError
        e.Cancel = True
    End Sub

    ''' <summary>
    ''' Pone el formulario en modo edici�n cuando el usuario hace doble click sobre un resultado del buscador
    ''' </summary>
    Private Sub ActivarEdicionRegistro(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvResultadosBuscador.CellContentDoubleClick, dgvResultadosBuscador.CellDoubleClick
        If e.RowIndex > -1 Then
            dgvResultadosBuscador.Rows(e.RowIndex).Selected = True
            Modificar()
        End If
    End Sub

    ''' <summary>
    ''' Nos desplaza a traves de los controles al pulsar enter
    ''' </summary>
    Private Sub EnterComoTab(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.Enter AndAlso (Me.ActiveControl Is Nothing OrElse Me.ActiveControl.Parent Is Nothing OrElse Not TypeOf (Me.ActiveControl.Parent) Is KryptonTextBox OrElse Not DirectCast(Me.ActiveControl.Parent, KryptonTextBox).Multiline) Then
            e.Handled = True
            SendKeys.Send("{TAB}")
        End If
    End Sub

    ''' <summary>
    ''' Control de las teclas de acci�n del usuario
    ''' </summary>
    Private Sub ControlTeclasBuscador(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvResultadosBuscador.KeyDown
        If Me.Registro Is Nothing Then Exit Sub

        Select Case e.KeyCode
            Case Keys.Delete
                e.Handled = True
                Eliminar()
            Case Keys.F2, Keys.Enter
                e.Handled = True
                Modificar()
        End Select
    End Sub

    ''' <summary>
    ''' Controlamos los botones de imprimir etiquetas
    ''' </summary>
    Private Sub EtiquetaSeleccionada(sender As Object, e As EventArgs) Handles dgvEtiquetas.SelectionChanged
        ControlarBotones()
    End Sub
#End Region
#Region " EMPAQUETADOS "
    ''' <summary>
    ''' Pido confirmaci�n antes de eliminar el registro
    ''' </summary>
    Private Sub EliminarRegistro() Handles btnEtiquetasEliminar.Click
        ' Validaciones
        If Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera Then Exit Sub
        If Me.Registro Is Nothing Then Exit Sub
        If dgvEmpaquetados.SelectedRows.Count <= 0 Then Exit Sub
        If Quadralia.Aplicacion.MostrarMessageBox("�Desea realmente eliminar los empaquetados?", "Confirmar Eliminaci�n", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) <> DialogResult.Yes Then Exit Sub

        ' Elimino las l�neas
        For Each Linea As DataGridViewRow In dgvEmpaquetados.SelectedRows
            If Linea.DataBoundItem Is Nothing Then Continue For
            If Me.Registro.Empaquetados.Contains(Linea.DataBoundItem) Then Me.Registro.Empaquetados.Remove(Linea.DataBoundItem)
        Next

        RefrescarEmpaquetados()
    End Sub

    ''' <summary>
    ''' Busca un empaquetado para asignar al lote de salida
    ''' </summary>
    Private Sub AnhadirEmpaquetado() Handles btnEtiquetasNuevo.Click
        If Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera Then Exit Sub
        If Me.Registro Is Nothing Then Exit Sub

        Dim formulario As New frmSeleccionarEmpaquetadoLote(Me.Contexto, Me.Registro.Empaquetados.ToList, True)
        If formulario.ShowDialog() <> DialogResult.OK Then Exit Sub

        For Each Empaquetado As Empaquetado In formulario.Items
            Me.Registro.Empaquetados.Add(Empaquetado)
        Next

        RefrescarEmpaquetados()
    End Sub

    ''' <summary>
    ''' Se produce cuando el usuario selecciona una fila
    ''' </summary>
    Private Sub EmpaquetadoSeleccionado(sender As Object, e As EventArgs) Handles dgvEmpaquetados.SelectionChanged
        ControlarBotones()
    End Sub

    ''' <summary>
    ''' Ordena los registros por la columna deseada
    ''' </summary>
    Private Sub OrdenarEmpaquetados(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvEmpaquetados.ColumnHeaderMouseClick
        Quadralia.Formularios.Funcionalidad.OrdenarDGV(Of Empaquetado)(dgvEmpaquetados, e)
    End Sub

    ''' <summary>
    ''' Refresca el DGV de Empaquetados
    ''' </summary>
    Private Sub RefrescarEmpaquetados()
        dgvEmpaquetados.DataSource = Nothing
        dgvEmpaquetados.Rows.Clear()
        ControlarBotonesEmpaquetados()

        ' Validaciones
        If Registro Is Nothing OrElse Registro.Empaquetados Is Nothing OrElse Registro.Empaquetados.Count = 0 Then Exit Sub
        Quadralia.Aplicacion.RefrescarDGVOrdenado(Of Empaquetado)(dgvEmpaquetados, Me.Registro.Empaquetados.ToList)
    End Sub

    ''' <summary>
    ''' Muestra el formulario con el Empaquetado cargado al usuario
    ''' </summary>
    Private Sub VerEmpaquetado(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvEmpaquetados.DoubleClick
        If Not cConfiguracionAplicacion.Instancia.PermitirNavegabilidad Then Exit Sub

        If dgvEmpaquetados.SelectedRows.Count > 0 AndAlso Quadralia.Aplicacion.PuedeAccederA(Quadralia.Aplicacion.Permisos.EMPAQUETADO) Then
            Dim FormularioEmpaquetado As frmEmpaquetado = Quadralia.Aplicacion.AbrirFormulario(frmPrincipal, "Escritorio.frmEmpaquetado", Quadralia.Aplicacion.Permisos.EMPAQUETADO, My.Resources.Empaquetado_16)
            FormularioEmpaquetado.IdRegistro = DirectCast(dgvEmpaquetados.SelectedRows(0).DataBoundItem, Empaquetado).id
        End If
    End Sub

    ''' <summary>
    ''' Abre el lector de c�digo de barras
    ''' </summary>
    Public Sub AbrirLector() Handles btnEtiquetasLector.Click
        ' Validaciones
        If Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera Then Exit Sub
        If Me.Registro Is Nothing Then Exit Sub

        Dim frmLector As New frmLectorBarrasEmpaquetados(Me.Contexto, Me.Registro.Empaquetados.ToList)
        If frmLector.ShowDialog() <> Windows.Forms.DialogResult.OK Then Exit Sub

        ' A�ado las etiquetas le�das
        For Each Empaquetado As Empaquetado In frmLector.Items
            Me.Registro.Empaquetados.Add(Empaquetado)
        Next

        RefrescarEmpaquetados()
    End Sub
#End Region
#Region " VALIDACIONES "
    ''' <summary>
    ''' Verifica que se haya introducido texto en un textbox marcado como obligatorio
    ''' </summary>
    Private Sub ValidarTextBoxObligatorio(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs)
        If Not TypeOf (sender) Is KryptonTextBox Then Exit Sub

        If Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera AndAlso String.IsNullOrEmpty(DirectCast(sender, KryptonTextBox).Text) Then
            epErrores.SetError(sender, My.Resources.TextBoxObligatorio)
        Else
            epErrores.SetError(sender, "")
        End If
    End Sub

    ''' <summary>
    ''' Verifica que se haya seleccionado una fecha del DTP marcado como obligatorio
    ''' </summary>
    Private Sub ValidarDTPObligatorio(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles dtpFecha.Validating
        If Not TypeOf (sender) Is KryptonDateTimePicker Then Exit Sub

        If Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera AndAlso IsDBNull(DirectCast(sender, KryptonDateTimePicker).ValueNullable) Then
            epErrores.SetError(sender, My.Resources.DTPObligatorio)
        Else
            epErrores.SetError(sender, "")
        End If
    End Sub

    ''' <summary>
    ''' Obliga al usuario a seleccionar un cliente antes de continuar
    ''' </summary>
    Private Sub ValidarTxtBuscador(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtCliente.Validating
        If Not TypeOf (sender) Is cTextboxBuscador Then Exit Sub

        If Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera AndAlso DirectCast(sender, cTextboxBuscador).Item Is Nothing Then
            epErrores.SetError(sender, My.Resources.TextBoxObligatorio)
        Else
            epErrores.SetError(sender, "")
        End If
    End Sub

    ''' <summary>
    ''' Verifica que se haya hecho una seleccion en un combobox marcado como obligatorio
    ''' </summary>
    Private Sub ComboObligatorio(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cboExpedidor.Validating
        If Not TypeOf (sender) Is KryptonComboBox Then Exit Sub

        If Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera AndAlso DirectCast(sender, KryptonComboBox).SelectedIndex = -1 Then
            epErrores.SetError(sender, My.Resources.ComboObligatorio)
        Else
            epErrores.SetError(sender, "")
        End If
    End Sub

    ''' <summary>
    ''' Valida que el usuario no pueda meter un c�digo ya existente o uno nulo
    ''' </summary>
    Private Sub ValidarCodigo(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtCodigo.Validating
        If Me.Estado = Quadralia.Enumerados.EstadoFormulario.Editando AndAlso String.IsNullOrEmpty(txtCodigo.Text) Then
            epErrores.SetError(txtCodigo, My.Resources.TextBoxObligatorio)
        ElseIf Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera AndAlso (From ent As LoteSalida In Contexto.LotesSalida Where ent.codigo.Trim().ToUpper() = txtCodigo.Text.Trim().ToUpper() AndAlso ent.id <> Registro.id Select ent).Count > 0 Then
            epErrores.SetError(txtCodigo, "El n�mero introducido se encuentra actualmente en uso. Por favor, rev�selo")
        Else
            epErrores.SetError(txtCodigo, "")
        End If
    End Sub
#End Region
#Region " SEGURIDAD "
    ''' <summary>
    ''' Configura el formulario en base a los permisos del usuario
    ''' </summary>
    Private Sub ConfigurarEntorno()
        tbpLotesEntrada.Visible = Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera AndAlso Quadralia.Aplicacion.UsuarioConectado IsNot Nothing AndAlso Quadralia.Aplicacion.PuedeAccederA(Quadralia.Aplicacion.Permisos.LOTES_ENTRADA)
        tbpEtiquetas.Visible = Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera AndAlso Quadralia.Aplicacion.UsuarioConectado IsNot Nothing AndAlso Quadralia.Aplicacion.PuedeAccederA(Quadralia.Aplicacion.Permisos.ETIQUETAS)
    End Sub
#End Region
#Region " NAVEGABILIDAD "
#Region " ALBARANES ENTRADA "
    Private Sub RefrescarLotesEntrada()
        dgvLotesEntrada.DataSource = Nothing
        dgvLotesEntrada.Rows.Clear()

        ' Validaciones
        If Registro Is Nothing OrElse Registro.LotesEntrada Is Nothing OrElse Registro.LotesEntrada.Count = 0 Then
            Exit Sub
        End If

        dgvLotesEntrada.DataSource = Me.Registro.LotesEntrada.ToList
        dgvLotesEntrada.ClearSelection()
    End Sub

    Private Sub VerLoteEntrada(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvLotesEntrada.DoubleClick
        If Not cConfiguracionAplicacion.Instancia.PermitirNavegabilidad Then Exit Sub

        If dgvLotesEntrada.SelectedRows.Count > 0 AndAlso Quadralia.Aplicacion.PuedeAccederA(Quadralia.Aplicacion.Permisos.LOTES_ENTRADA) Then
            Dim FormularioAlbaran As frmLoteEntrada = Quadralia.Aplicacion.AbrirFormulario(frmPrincipal, "Escritorio.frmLoteEntrada", Quadralia.Aplicacion.Permisos.LOTES_ENTRADA, My.Resources.Entrada_16)
            FormularioAlbaran.IdRegistro = DirectCast(dgvLotesEntrada.SelectedRows(0).DataBoundItem, LoteEntrada).id
        End If
    End Sub

    ''' <summary>
    ''' Ordena los registros por la columna deseada
    ''' </summary>
    Private Sub OrdenarAlbaranes(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvLotesEntrada.ColumnHeaderMouseClick
        Quadralia.Formularios.Funcionalidad.OrdenarDGV(Of LoteEntrada)(dgvLotesEntrada, e)
    End Sub
#End Region
#Region " ETIQUETAS "
    Private Sub RefrescarEtiquetas()
        dgvEtiquetas.DataSource = Nothing
        dgvEtiquetas.Rows.Clear()

        ' Validaciones
        If Registro Is Nothing OrElse Registro.Etiquetas Is Nothing OrElse Registro.Etiquetas.Count = 0 Then
            Exit Sub
        End If

        dgvEtiquetas.DataSource = Me.Registro.Etiquetas.Where(Function(p) p.fechaBaja Is Nothing).ToList
        dgvEtiquetas.ClearSelection()
    End Sub

    Private Sub VerEtiqueta(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvEtiquetas.DoubleClick
        If Not cConfiguracionAplicacion.Instancia.PermitirNavegabilidad Then Exit Sub

        If dgvEtiquetas.SelectedRows.Count > 0 AndAlso Quadralia.Aplicacion.PuedeAccederA(Quadralia.Aplicacion.Permisos.ETIQUETAS) Then
            Dim FormularioEtiqueta As frmGestionEtiquetas = Quadralia.Aplicacion.AbrirFormulario(Me.ParentForm, "Escritorio.frmGestionEtiquetas", Quadralia.Aplicacion.Permisos.ETIQUETAS, My.Resources.Etiqueta_16)
            FormularioEtiqueta.IdRegistro = DirectCast(dgvEtiquetas.SelectedRows(0).DataBoundItem, Etiqueta).id
        End If
    End Sub

    ''' <summary>
    ''' Ordena los registros por la columna deseada
    ''' </summary>
    Private Sub OrdenarEtiquetas(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvEtiquetas.ColumnHeaderMouseClick
        Quadralia.Formularios.Funcionalidad.OrdenarDGV(Of Etiqueta)(dgvEtiquetas, e)
    End Sub
#End Region

    ''' <summary>
    ''' Marca la linea especificada en el DGV
    ''' </summary>
    ''' <param name="IdLinea"></param>
    Public Sub MarcarLineaEntrada(ByVal IdLinea As Long)
        ' Validaciones
        If IdLinea <= 0 Then Exit Sub
        If dgvEmpaquetados.RowCount < 1 Then Exit Sub

        ' Recorro las lineas
        For Each Fila As DataGridViewRow In dgvEmpaquetados.Rows
            If Fila.DataBoundItem Is Nothing Then Continue For

            ' Si encuentro la l�nea deseada, la marco
            If DirectCast(Fila.DataBoundItem, Empaquetado).Etiquetas.Any(Function(etq) etq.LoteEntradaLinea IsNot Nothing AndAlso etq.LoteEntradaLinea.id = IdLinea) Then
                Fila.DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 140, 0)
            End If
        Next
    End Sub
#End Region
#Region " IMPRIMIR "
    ''' <summary>
    ''' Imprime el registro actual
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Imprimir(ByVal Informe As cInforme)
        ' Validaciones
        If Me.Registro Is Nothing Then Exit Sub
        If Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera Then Exit Sub

        ' Imprimo la lista
        Dim Lista As New List(Of LoteSalida)
        Lista.Add(Me.Registro)
        ImprimirLoteSalida(Informe, Lista)
    End Sub

    ''' <summary>
    ''' Imprime los registros especificados como par�metro de entrada
    ''' </summary>
    Public Shared Sub ImprimirLoteSalida(ByVal Informe As cInforme, ByVal LotesSalida As List(Of LoteSalida))
        If Not Quadralia.Aplicacion.PuedeAccederA(Quadralia.Aplicacion.Permisos.LOTES_SALIDA) Then Exit Sub
        If LotesSalida Is Nothing OrElse LotesSalida.Count = 0 Then Exit Sub

        Dim Formulario As frmVisorInforme = Quadralia.Aplicacion.AbrirInforme(frmPrincipal, frmVisorInforme.Informe.Salida, Quadralia.Aplicacion.Permisos.LOTES_SALIDA)
        If Formulario Is Nothing Then Exit Sub

        Formulario.Datos = LotesSalida
        If Informe IsNot Nothing Then Formulario.Reporte = Informe.Documento
        Formulario.RefrescarDatos()
    End Sub
#End Region
#Region " IMPRIMIR ETIQUETAS "
    ''' <summary>
    ''' Imprime el registro actual
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ImprimirEtiquetas(ByVal Informe As cInforme)
        ' Validaciones
        If Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera Then Exit Sub
        If Me.Registro Is Nothing Then Exit Sub
        If Me.dgvEtiquetas.SelectedRows.Count = 0 Then Exit Sub

        Dim Etiquetas As List(Of Etiqueta) = (From fil As DataGridViewRow In dgvEtiquetas.SelectedRows Where fil.DataBoundItem IsNot Nothing Select DirectCast(fil.DataBoundItem, Etiqueta)).ToList

        ' Imprimo la lista
        frmGestionEtiquetas.ImprimirEtiquetas(Etiquetas, Informe)
    End Sub
#End Region
End Class
