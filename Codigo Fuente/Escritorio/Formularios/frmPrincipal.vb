﻿Imports ComponentFactory.Krypton.Ribbon
Imports System.Threading

Public Class frmPrincipal
#Region " ACCIONES SOBRE FORMULARIOHIJO "
    ''' <summary>
    ''' Llama a la función de añadir nuevos registros del formulario hijo
    ''' </summary>
    Private Sub Nuevo(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUsuariosAccionesNuevo.Click, btnProveedoresAccionesNuevo.Click, btnLotesSalidaAccionesNuevo.Click, btnLotesEntradaAccionesNuevo.Click, btnClientesAccionesNuevo.Click, btnExpedidoresAccionesNuevo.Click, btnEtiquetasAccionesNuevo.Click, btnEmpaquetadoAccionesNuevo.Click
        If Not TypeOf (ActiveMdiChild) Is FormularioHijo Then Exit Sub
        If DirectCast(sender, ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton).RibbonTab IsNot DirectCast(Me.ActiveMdiChild, FormularioHijo).Tab Then Exit Sub

        DirectCast(Me.ActiveMdiChild, FormularioHijo).Nuevo()
    End Sub

    ''' <summary>
    ''' Llama a la función de modificar un registro del formulario hijo
    ''' </summary>
    Private Sub Modificar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUsuariosAccionesModificar.Click, btnProveedoresAccionesModificar.Click, btnLotesSalidaAccionesModificar.Click, btnLotesEntradaAccionesModificar.Click, btnClientesAccionesModificar.Click, btnExpedidoresAccionesModificar.Click, btnEtiquetasAccionesModificar.Click, btnEmpaquetadoAccionesModificar.Click
        If Not TypeOf (ActiveMdiChild) Is FormularioHijo Then Exit Sub
        If DirectCast(sender, ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton).RibbonTab IsNot DirectCast(Me.ActiveMdiChild, FormularioHijo).Tab Then Exit Sub

        DirectCast(Me.ActiveMdiChild, FormularioHijo).Modificar()
    End Sub

    ''' <summary>
    ''' Llama a la función de eliminar registros del formulario hijo
    ''' </summary>
    Private Sub Eliminar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUsuariosAccionesEliminar.Click, btnProveedoresAccionesEliminar.Click, btnLotesSalidaAccionesEliminar.Click, btnLotesEntradaAccionesEliminar.Click, btnClientesAccionesEliminar.Click, btnExpedidoresAccionesEliminar.Click, btnEtiquetasAccionesEliminar.Click, btnEmpaquetadoAccionesEliminar.Click
        If Not TypeOf (ActiveMdiChild) Is FormularioHijo Then Exit Sub
        If DirectCast(sender, ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton).RibbonTab IsNot DirectCast(Me.ActiveMdiChild, FormularioHijo).Tab Then Exit Sub

        DirectCast(Me.ActiveMdiChild, FormularioHijo).Eliminar()
    End Sub

    ''' <summary>
    ''' Llama a la función de guardado de datos del formulario hijo
    ''' </summary>
    Private Sub Guardar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUsuariosAccionesGuardar.Click, btnConfiguracionAccionesGuardar.Click, btnProveedoresAccionesGuardar.Click, btnLotesSalidaAccionesGuardar.Click, btnLotesEntradaAccionesGuardar.Click, btnClientesAccionesGuardar.Click, btnExpedidoresAccionesGuardar.Click, btnEtiquetasAccionesGuardar.Click, btnEmpaquetadoAccionesGuardar.Click
        If Not TypeOf (ActiveMdiChild) Is FormularioHijo Then Exit Sub
        If DirectCast(sender, ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton).RibbonTab IsNot DirectCast(Me.ActiveMdiChild, FormularioHijo).Tab Then Exit Sub

        DirectCast(Me.ActiveMdiChild, FormularioHijo).Guardar()
    End Sub

    ''' <summary>
    ''' Llama a la función de guardado de datos del formulario hijo
    ''' </summary>
    Private Sub GuardarGenerico(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRapidoGuardar.Click
        If Not TypeOf (ActiveMdiChild) Is FormularioHijo Then Exit Sub
        DirectCast(Me.ActiveMdiChild, FormularioHijo).Guardar()
    End Sub

    ''' <summary>
    ''' Llama a la función de cancelar la edicion de registros del formulario hijo
    ''' </summary>
    Private Sub Cancelar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUsuariosAccionesCancelar.Click, btnConfiguracionAccionesCancelar.Click, btnProveedoresAccionesCancelar.Click, btnLotesSalidaAccionesCancelar.Click, btnLotesEntradaAccionesCancelar.Click, btnClientesAccionesCancelar.Click, btnExpedidoresAccionesCancelar.Click, btnEtiquetasAccionesCancelar.Click, btnEmpaquetadoAccionesCancelar.Click
        If Not TypeOf (ActiveMdiChild) Is FormularioHijo Then Exit Sub
        If DirectCast(sender, ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton).RibbonTab IsNot DirectCast(Me.ActiveMdiChild, FormularioHijo).Tab Then Exit Sub

        DirectCast(Me.ActiveMdiChild, FormularioHijo).Cancelar()
    End Sub

    ''' <summary>
    ''' Cierra el formulario activo
    ''' </summary>
    Private Sub Cerrar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUsuariosSalir.Click, btnProveedoresAccionesCerrar.Click, btnLotesSalidaAccionesCerrar.Click, btnLotesEntradaAccionesCerrar.Click, btnClientesAccionesCerrar.Click, btnExpedidoresAccionesCerrar.Click, btnInformesAccionesCerrar.Click, btnGenerarEtiquetasCerrar.Click, btnEtiquetasAccionesCerrar.Click, btnEmpaquetadoAccionesCerrar.Click
        If Not TypeOf (ActiveMdiChild) Is FormularioHijo Then Exit Sub
        If DirectCast(sender, ComponentFactory.Krypton.Ribbon.KryptonRibbonGroupButton).RibbonTab IsNot DirectCast(Me.ActiveMdiChild, FormularioHijo).Tab Then Exit Sub

        DirectCast(Me.ActiveMdiChild, FormularioHijo).Cerrar()
    End Sub
#End Region
#Region " ABRIR VENTANAS "
#Region " DATOS ADMINISTRATIVOS "
    ''' <summary>
    ''' Abre el formulario para gestionar los expedidores
    ''' </summary>
    Private Sub AbrirExpedidores(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatosAdministrativosExpedidores.Click, mnuOrbExpedidores.Click
        Quadralia.Aplicacion.AbrirFormulario(Me, "Escritorio.frmExpedidores", Quadralia.Aplicacion.Permisos.EXPEDIDORES, My.Resources.Expedidor_16)
    End Sub

    ''' <summary>
    ''' Abre el formulario para gestionar proveedores
    ''' </summary>
    Private Sub AbrirProveedores(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatosAdministrativosProveedores.Click, mnuOrbProveedores.Click
        Quadralia.Aplicacion.AbrirFormulario(Me, "Escritorio.frmProveedores", Quadralia.Aplicacion.Permisos.PROVEEDORES, My.Resources.Proveedor_16)
    End Sub

    ''' <summary>
    ''' Abre el formulario para gestionar clientes
    ''' </summary>
    Private Sub AbrirClientes(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatosAdministrativosClientes.Click, mnuOrbClientes.Click
        Quadralia.Aplicacion.AbrirFormulario(Me, "Escritorio.frmClientes", Quadralia.Aplicacion.Permisos.CLIENTES, My.Resources.Cliente_16)
    End Sub
#End Region
#Region " LOTES "
    ''' <summary>
    ''' Abre el formulario para gestionar lotes de entrada
    ''' </summary>
    Private Sub AbrirLoteEntrada(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatosAdministrativosLotesEntrada.Click, mnuOrbLotesEntrada.Click
        Quadralia.Aplicacion.AbrirFormulario(Me, "Escritorio.frmLoteEntrada", Quadralia.Aplicacion.Permisos.LOTES_ENTRADA, My.Resources.Entrada_16)
    End Sub

    ''' <summary>
    ''' Abre el formulario para la gestión de empaquetados
    ''' </summary>
    Private Sub AbrirEmpaquetado(sender As Object, e As EventArgs) Handles mnuDatosAdministrativosEmpaquetado.Click, mnuOrbEmpaquetado.Click
        Quadralia.Aplicacion.AbrirFormulario(Me, "Escritorio.frmEmpaquetado", Quadralia.Aplicacion.Permisos.EMPAQUETADO, My.Resources.Empaquetado_16)
    End Sub

    ''' <summary>
    ''' Abre el formulario para gestionar lotes de Salida
    ''' </summary>
    Private Sub AbrirLoteSalida(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatosAdministrativosLotesSalida.Click, mnuOrbLotesSalida.Click
        Quadralia.Aplicacion.AbrirFormulario(Me, "Escritorio.frmLoteSalida", Quadralia.Aplicacion.Permisos.LOTES_SALIDA, My.Resources.Salida_16)
    End Sub

    ''' <summary>
    ''' Abre el importador de albaranes de entrada
    ''' </summary>
    Private Sub AbrirImportardorAlbaranEntrada(sender As Object, e As EventArgs) Handles mnuAlbaranesEntradaImportar.Click
        Quadralia.Aplicacion.AbrirFormulario(Me, "Escritorio.frmImportadorLoteEntrada", Quadralia.Aplicacion.Permisos.LOTES_ENTRADA, My.Resources.Importar_16)
    End Sub

    ''' <summary>
    ''' Abre el formulario de generación automática de etiquetas
    ''' </summary>
    Private Sub AbrirGeneracionAutomaticaEtiquetas(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuOrbEtiquetasGeneracionAutomatica.Click, rtnEtiquetasGeneracionAutomatica.Click
        Dim Formulario As frmPesadoAutomatico = Quadralia.Aplicacion.AbrirFormulario(Me, "Escritorio.frmPesadoAutomatico", Quadralia.Aplicacion.Permisos.ETIQUETAS, My.Resources.Etiqueta_16, True, 1)

        ' Miro si tengo que pedir datos
        If Formulario IsNot Nothing AndAlso Formulario.LoteEntrada Is Nothing AndAlso Formulario.Expedidor Is Nothing Then
            Dim Expedidor As Expedidor = (From exp As Expedidor In Formulario.Contexto.Expedidores Where exp.predeterminada AndAlso Not exp.fechaBaja.HasValue Select exp).FirstOrDefault ' ¿Expedidor por defecto?
            If Expedidor Is Nothing AndAlso Formulario.Contexto.Expedidores.Where(Function(exp) Not exp.fechaBaja.HasValue).Count = 1 Then Expedidor = Formulario.Contexto.Expedidores.Where(Function(exp) Not exp.fechaBaja.HasValue).FirstOrDefault ' ¿Expedidor unico?
            Dim IdExpedidor As Integer = 0
            If Expedidor IsNot Nothing Then IdExpedidor = Expedidor.id

            Dim FormularioBusqueda As New frmSeleccionarExpedidorLoteEntrada() With {.Expedidor = Expedidor, .Contexto = Formulario.Contexto, .Disenho = Formulario.Disenho}

            If FormularioBusqueda.ShowDialog() <> Windows.Forms.DialogResult.OK Then Exit Sub
            Formulario.Expedidor = FormularioBusqueda.Expedidor
            Formulario.LoteEntrada = FormularioBusqueda.Lote
            Formulario.Disenho = FormularioBusqueda.Disenho
        End If
    End Sub

    ''' <summary>
    ''' Abre la ventana de gestión de etiquetas
    ''' </summary>
    Private Sub AbrirGestionEtiquetas(sender As Object, e As EventArgs) Handles mnuDatosAdministrativosEtiquetas.Click, mnuOrbEtiquetas.Click
        Quadralia.Aplicacion.AbrirFormulario(Me, "Escritorio.frmGestionEtiquetas", Quadralia.Aplicacion.Permisos.ETIQUETAS, My.Resources.Etiqueta_16)
    End Sub
#End Region
#Region " CONFIGURACIÓN "
    ''' <summary>
    ''' Abre el formulario para la configuración de la base de datos
    ''' </summary>
    Private Sub AbrirConfiguracionBaseDeDatos(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rtnConfiguracionProgramaBaseDatos.Click, mnuOrbBaseDatos.Click
        If Not Quadralia.Aplicacion.PuedeAccederA(Quadralia.Aplicacion.Permisos.CONFIGURACION_PROGRAMA) Then Exit Sub
        Quadralia.Aplicacion.AbrirFormulario(Me, "Escritorio.frmConfiguracionBBDD", Quadralia.Aplicacion.Permisos.CONFIGURACION_PROGRAMA, My.Resources.ConfiguracionBaseDatos_16)
    End Sub

    ''' <summary>
    ''' Abre el formulario de configuración de variables globales
    ''' </summary>
    Private Sub AbrirParametrosGlobales(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rtnConfiguracionProgramaParametrosGlobales.Click, mnuOrbConfiguracionPrograma.Click
        If Not Quadralia.Aplicacion.PuedeAccederA(Quadralia.Aplicacion.Permisos.CONFIGURACION_PROGRAMA) Then Exit Sub
        Quadralia.Aplicacion.AbrirFormulario(Me, "Escritorio.frmConfiguracionGeneral", Quadralia.Aplicacion.Permisos.CONFIGURACION_PROGRAMA, My.Resources.Configurar_16)
    End Sub

    ''' <summary>
    ''' Abre un formulario de gestión de usuarios
    ''' </summary>
    Private Sub AbrirUsuarios(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuOrbUsuarios.Click, mnuConfiguracionUsuarios.Click
        If Not Quadralia.Aplicacion.PuedeAccederA(Quadralia.Aplicacion.Permisos.USUARIOS) Then Exit Sub
        Quadralia.Aplicacion.AbrirFormulario(Me, "Escritorio.frmUsuario", Quadralia.Aplicacion.Permisos.USUARIOS, My.Resources.Usuarios_16)
    End Sub

    ''' <summary>
    ''' Abre el formulario de copia de seguridad
    ''' </summary>
    Private Sub AbrirCopiaSeguridad(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuOrbCopiaSeguridad.Click, rtnConfiguracionProgramaCopiaSeguridad.Click
        Quadralia.Aplicacion.AbrirFormulario(Me, "Escritorio.frmProgresoCopiaSeguridad", Quadralia.Aplicacion.Permisos.CONFIGURACION_PROGRAMA, My.Resources.CopiaSeguridad_16)
    End Sub

    ''' <summary>
    ''' Abre el formulario de copia de seguridad
    ''' </summary>
    Private Sub AbrirDatosMaestros(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rtnConfiguracionProgramaDatosMaestros.Click, mnuOrbDatosMaestros.Click
        Quadralia.Aplicacion.AbrirFormulario(Me, "Escritorio.frmDatosMaestros", Quadralia.Aplicacion.Permisos.DATOS_MAESTROS, My.Resources.DatosMaestros_16)
    End Sub
#End Region
#Region " ACERCA DE  "
    ''' <summary>
    ''' Abre el formulario de Acerca De...
    ''' </summary>
    Private Sub AbrirAcercaDe(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuConfiguracionAcercaDe.Click
#If DEBUG Then
        Dim Depuracion As New frmDebug
        Depuracion.MdiParent = Me
        Depuracion.Show()
#Else
            Dim ventanaAcercaDe As New frmAcercaDe
            ventanaAcercaDe.ShowDialog()
#End If
    End Sub
#End Region
#End Region
#Region " VENTANAS MDI "
    ''' <summary>
    ''' Muestra la ventana asociada al tab seleccionado
    ''' </summary>
    Private Sub MostrarVentanas(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbbPrincipal.SelectedTabChanged
        For Each Hijo In Me.MdiChildren
            If TypeOf (Hijo) Is FormularioHijo AndAlso DirectCast(Hijo, FormularioHijo).Tab Is rbbPrincipal.SelectedTab Then
                Hijo.Activate()
                Exit Sub
            End If
        Next
    End Sub

    Private Sub btnVentanasMosaicoH_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVentanasMosaicoH.Click
        Me.LayoutMdi(MdiLayout.TileHorizontal)
        rbbPrincipal.SelectedTab = tabVentanas
    End Sub

    Private Sub btnVentanasMosaicoV_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVentanasMosaicoV.Click
        Me.LayoutMdi(MdiLayout.TileVertical)
        rbbPrincipal.SelectedTab = tabVentanas
    End Sub

    Private Sub btnVentanasCascada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVentanasCascada.Click
        Me.LayoutMdi(MdiLayout.Cascade)
        rbbPrincipal.SelectedTab = tabVentanas
    End Sub

    Private Sub btnVentanasMinimizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVentanasMinimizar.Click
        For i As Integer = 0 To Me.MdiChildren.Length - 1
            Me.MdiChildren(i).WindowState = FormWindowState.Minimized
        Next
        rbbPrincipal.SelectedTab = tabVentanas
    End Sub

    Private Sub btnVentanasRestaurar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVentanasRestaurar.Click
        For i As Integer = 0 To Me.MdiChildren.Length - 1
            Me.MdiChildren(i).WindowState = FormWindowState.Normal
        Next
        rbbPrincipal.SelectedTab = tabVentanas
    End Sub

    Private Sub btnVentanasCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVentanasCerrar.Click
        Dim CuantasHay As Integer = 0

        For Each frm As Form In Me.MdiChildren
            CuantasHay = CuantasHay + 1
        Next

        Dim laRespuesta As MsgBoxResult
        Dim plural As String

        If CuantasHay > 1 Then
            plural = "s"
        Else
            plural = ""
        End If

        If CuantasHay > 0 Then
            laRespuesta = MsgBox("Hay " & CuantasHay & " ventana" & plural & " abierta" & plural & ". ¿Está seguro de querer cerrarla" & plural & "? Si no tiene guardado el trabajo, éste será descartado.", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Atención")
            If laRespuesta = MsgBoxResult.No Then Exit Sub
        End If

        For Each frm As Form In Me.MdiChildren
            frm.Close()
        Next
        rbbPrincipal.SelectedTab = tabVentanas
    End Sub
#End Region
#Region " RUTINA DE INICIO / FIN "
    ''' <summary>
    ''' Cierre de la aplicación
    ''' </summary>
    Private Sub ControlarCierre(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        If Quadralia.Aplicacion.SalidaForzada Then Exit Sub
        If Quadralia.Aplicacion.MostrarMessageBox("¿Está seguro de querer salir de la aplicación?", "Salir", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then e.Cancel = True
    End Sub

    ''' <summary>
    ''' Rutina de inicio
    ''' </summary>
    Private Sub Inicio(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Configuro las cadenas
        With mngManager.GlobalStrings
            .Abort = "Cancelar"
            .Cancel = "Cancelar"
            .Close = "Cerrar"
            .Ignore = "Ignorar"
            .No = "No"
            .OK = "Aceptar"
            .Retry = "Reintentar"
            .Today = "Hoy"
            .Yes = "Si"
        End With

        ' Configuro el actualizador
        Quadralia.Actualizaciones.ConfigurarActualizador(mnuInfoActualizacion, upActualizador, Quadralia.Aplicacion.ObtenerParametroActualizacion())

        ' Configuro los tipos de Etiquetas
        AnhadirModelos(btnGenerarEtiquetasRemprimirUltimaEtiqueta, "EtiquetasTrazabilidad", AddressOf ReimprimirUltimaEtiqueta)
        AnhadirModelos(btnEtiquetasMasAccionesImprimir, "EtiquetasTrazabilidad", AddressOf ImprimirEtiqueta)
        AnhadirModelos(btnEmpaquetadoAccionesImprimirEtiquetas, "EtiquetasTrazabilidad", AddressOf ImprimirEtiquetaEmpaquetado)
        AnhadirModelos(btnLotesSalidaAccionesImprimir, "LotesSalida", AddressOf ImprimirLoteSalida)
        AnhadirModelos(btnLotesSalidaAccionesImprimirEtiquetas, "EtiquetasTrazabilidad", AddressOf ImprimirEtiquetasLotesSalida)

        ntfIcono.Text = My.Application.Info.ProductName()
        ' Oculto los tabs que no me interesan del ribbon
        Dim TabsIniciales As New List(Of KryptonRibbonTab)
        TabsIniciales.AddRange(New KryptonRibbonTab() {tabGeneral, tabVentanas})

        For Each Pestana As KryptonRibbonTab In rbbPrincipal.RibbonTabs
            Pestana.Visible = TabsIniciales.Contains(Pestana)
        Next

        ' Establezo Pestana principal como visible
        rbbPrincipal.SelectedTab = tabGeneral

        'Loop, looking for MdiClient type Forms
        For Each mdic As Control In Me.Controls
            If TypeOf (mdic) Is MdiClient Then
                mdic.BackColor = Me.BackColor
                AddHandler mdic.Paint, AddressOf Mdi_Paint
                AddHandler mdic.SizeChanged, AddressOf Mdi_SizeChanged

                Exit For
            End If
        Next

        AddHandler cConfiguracionPrograma.Instancia.ConfiguracionCambiada, AddressOf ConfiguracionCambiada

        ' Configuro el entorno del usuario en funcion de sus permisos
        ConfigurarEntorno()
    End Sub

    ''' <summary>
    ''' Configura el entorno de la aplicación en función de los permisos del usuario conectado
    ''' </summary>
    Private Sub ConfigurarEntorno()
        mnuRapidoCambiarContrasenha.Visible = Quadralia.Aplicacion.ConexionBBDD AndAlso Quadralia.Aplicacion.UsuarioConectado IsNot Nothing AndAlso Quadralia.Aplicacion.UsuarioConectado.id >= 1

        tabInformes.Visible = Quadralia.Aplicacion.ConexionBBDD AndAlso Quadralia.Aplicacion.PuedeAccederA(Quadralia.Aplicacion.Permisos.INFORMES) 'And False

        ' Primero botón por botón
        mnuDatosAdministrativosExpedidores.Visible = Quadralia.Aplicacion.ConexionBBDD AndAlso Quadralia.Aplicacion.PuedeAccederA(Quadralia.Aplicacion.Permisos.EXPEDIDORES)
        mnuDatosAdministrativosProveedores.Visible = Quadralia.Aplicacion.ConexionBBDD AndAlso Quadralia.Aplicacion.PuedeAccederA(Quadralia.Aplicacion.Permisos.PROVEEDORES)
        mnuDatosAdministrativosClientes.Visible = Quadralia.Aplicacion.ConexionBBDD AndAlso Quadralia.Aplicacion.PuedeAccederA(Quadralia.Aplicacion.Permisos.CLIENTES)
        mnuDatosAdministrativosLotesEntrada.Visible = Quadralia.Aplicacion.ConexionBBDD AndAlso Quadralia.Aplicacion.PuedeAccederA(Quadralia.Aplicacion.Permisos.LOTES_ENTRADA)
        mnuDatosAdministrativosLotesSalida.Visible = Quadralia.Aplicacion.ConexionBBDD AndAlso Quadralia.Aplicacion.PuedeAccederA(Quadralia.Aplicacion.Permisos.LOTES_SALIDA)
        mnuDatosAdministrativosEtiquetas.Visible = Quadralia.Aplicacion.ConexionBBDD AndAlso Quadralia.Aplicacion.PuedeAccederA(Quadralia.Aplicacion.Permisos.ETIQUETAS)
        mnuDatosAdministrativosEmpaquetado.Visible = Quadralia.Aplicacion.ConexionBBDD AndAlso Quadralia.Aplicacion.PuedeAccederA(Quadralia.Aplicacion.Permisos.EMPAQUETADO)
        mnuConfiguracionUsuarios.Visible = Quadralia.Aplicacion.ConexionBBDD AndAlso Quadralia.Aplicacion.PuedeAccederA(Quadralia.Aplicacion.Permisos.USUARIOS)

        rtnConfiguracionProgramaParametrosGlobales.Visible = Quadralia.Aplicacion.ConexionBBDD AndAlso Quadralia.Aplicacion.PuedeAccederA(Quadralia.Aplicacion.Permisos.CONFIGURACION_PROGRAMA)
        rtnConfiguracionProgramaBaseDatos.Visible = (Not Quadralia.Aplicacion.ConexionBBDD) OrElse (Quadralia.Aplicacion.ConexionBBDD AndAlso Quadralia.Aplicacion.PuedeAccederA(Quadralia.Aplicacion.Permisos.CONFIGURACION_PROGRAMA))
        rtnConfiguracionProgramaCopiaSeguridad.Visible = Quadralia.Aplicacion.ConexionBBDD
        rtnConfiguracionProgramaDatosMaestros.Visible = Quadralia.Aplicacion.ConexionBBDD AndAlso Quadralia.Aplicacion.PuedeAccederA(Quadralia.Aplicacion.Permisos.DATOS_MAESTROS)

        ' Elimino los grupos que han quedado vacíos
        For Each Grupo In tabGeneral.Groups
            Dim BotonVisible As Boolean = False

            For Each Elemento In Grupo.Items
                If TypeOf Elemento Is KryptonRibbonGroupTriple AndAlso DirectCast(Elemento, KryptonRibbonGroupTriple).Items.Count > 0 Then
                    For Each ctrl In DirectCast(Elemento, KryptonRibbonGroupTriple).Items
                        If ctrl.Visible Then BotonVisible = True
                    Next
                End If
            Next

            Grupo.Visible = BotonVisible
        Next

        ' Configuro ahora el menu superior
        mnuOrbExpedidores.Visible = mnuDatosAdministrativosExpedidores.Visible
        mnuOrbProveedores.Visible = mnuDatosAdministrativosProveedores.Visible
        mnuOrbClientes.Visible = mnuDatosAdministrativosClientes.Visible
        mnuOrbDatosAdministrativos.Visible = mnuOrbExpedidores.Visible OrElse mnuOrbProveedores.Visible OrElse mnuOrbClientes.Visible

        mnuOrbLotesEntrada.Visible = mnuDatosAdministrativosLotesEntrada.Visible
        mnuOrbLotesSalida.Visible = mnuDatosAdministrativosLotesSalida.Visible
        mnuOrbEtiquetas.Visible = mnuDatosAdministrativosEtiquetas.Visible
        mnuOrbEtiquetasGeneracionAutomatica.Visible = mnuDatosAdministrativosEtiquetas.Visible
        mnuOrbEmpaquetado.Visible = mnuDatosAdministrativosEmpaquetado.Visible
        mnuOrbLotes.Visible = mnuDatosAdministrativosLotesEntrada.Visible OrElse mnuDatosAdministrativosLotesSalida.Visible OrElse mnuDatosAdministrativosEtiquetas.Visible OrElse mnuOrbEmpaquetado.Visible

        mnuOrbBaseDatos.Visible = rtnConfiguracionProgramaBaseDatos.Visible
        mnuOrbConfiguracionPrograma.Visible = rtnConfiguracionProgramaParametrosGlobales.Visible
        mnuOrbCopiaSeguridad.Visible = rtnConfiguracionProgramaCopiaSeguridad.Visible
        mnuOrbDatosMaestros.Visible = rtnConfiguracionProgramaDatosMaestros.Visible

        mnuOrbUsuarios.Visible = mnuConfiguracionUsuarios.Visible
    End Sub

    ''' <summary>
    ''' Añade los modelos de informes a cada categoria
    ''' </summary>
    Private Sub AnhadirModelos(ByRef BotonMenu As KryptonRibbonGroupButton, ByVal Clave As String, ByVal hdlr As EventHandler)
        ' Validaciones
        If BotonMenu Is Nothing Then Exit Sub
        If Not Quadralia.Aplicacion.GestorInformes.Informes.ContainsKey(Clave) Then Exit Sub

        ' Cambio el tipo de botón
        BotonMenu.ButtonType = GroupButtonType.Split
        AddHandler BotonMenu.Click, hdlr

        Dim Menu As New KryptonContextMenu
        Dim ListaCabecera As New KryptonContextMenuItems

        ' Añado los listados correspondientes
        With ListaCabecera
            For Each Informe As cInforme In Quadralia.Aplicacion.GestorInformes.Informes(Clave).Values
                Dim Entrada As New KryptonContextMenuItem With {.Text = Informe.Nombre, .Tag = Informe}
                AddHandler Entrada.Click, hdlr
                .Items.Add(Entrada)
            Next
        End With

        Menu.Items.Add(ListaCabecera)
        BotonMenu.KryptonContextMenu = Menu
    End Sub
#End Region
#Region " RESTO DE FUNCIONES "
    ''' <summary>
    ''' Oculta las opciones para cambiar el posicionamiento del QAT sobre el ribbon
    ''' </summary>
    Private Sub OcultarMenuDesplazamientoRibbon(ByVal sender As System.Object, ByVal e As ComponentFactory.Krypton.Toolkit.ContextMenuArgs)
        Dim TotalItems As Integer = DirectCast(e.KryptonContextMenu.Items(1), ComponentFactory.Krypton.Toolkit.KryptonContextMenuItems).Items.Count
        DirectCast(e.KryptonContextMenu.Items(1), ComponentFactory.Krypton.Toolkit.KryptonContextMenuItems).Items(TotalItems - 3).Visible = False
        DirectCast(e.KryptonContextMenu.Items(1), ComponentFactory.Krypton.Toolkit.KryptonContextMenuItems).Items(TotalItems - 2).Visible = False
    End Sub

    ''' <summary>
    ''' Rutina para el cambio de contraseña del usuario conectado
    ''' </summary>
    Private Sub CambiarContrasenha(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRapidoCambiarContrasenha.Click
        ' Validaciones
        If Quadralia.Aplicacion.UsuarioConectado Is Nothing Then Exit Sub
        If Quadralia.Aplicacion.UsuarioConectado.id <= 0 Then Exit Sub

        Dim Formulario As frmCambioClave = Quadralia.Aplicacion.AbrirFormulario(Nothing, "Escritorio.frmCambioClave", Quadralia.Aplicacion.Permisos.PUBLICO, My.Resources.Contrasenha_16, False)
        Formulario.Tipo = frmCambioClave.TipoFormularo.CambioClave
        Formulario.IdUsuario = Quadralia.Aplicacion.UsuarioConectado.id
        Formulario.StartPosition = FormStartPosition.CenterParent

        Formulario.ShowDialog()
    End Sub

    ''' <summary>
    ''' Termina la ejecución de la aplicación
    ''' </summary>
    Private Sub SalirAplicacion(ByVal sender As System.Object, ByVal e As EventArgs) Handles mnuSalir.Click
        Me.Close()
    End Sub
#End Region
#Region " FONDO MDI "
    Private Sub Mdi_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        CType(sender, MdiClient).Invalidate()
    End Sub

    Private Sub Mdi_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs)
        Dim Imagen As Image = My.Resources.fondo
        Dim TamanhoImagen As Drawing.Size = Imagen.Size
        Dim TamanhoControl As Drawing.Size = DirectCast(sender, Control).Size

        e.Graphics.DrawImage(Imagen, TamanhoControl.Width - TamanhoImagen.Width - 40, TamanhoControl.Height - TamanhoImagen.Height - 40)
    End Sub
#End Region
#Region " TIMERS "
#End Region
#Region " RIBBON - INFORMES "
    ''' <summary>
    ''' Abre el listado de albaranes
    ''' </summary>
    Private Sub AbrirInforme(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ' Quadralia.Aplicacion.AbrirInforme(Me, frmVisorInformeParametros.Informe.ListadoAlbaranes, "Listado de Albaranes", Quadralia.Aplicacion.Permisos.INFORMES, My.Resources.AlbaranesSalida_Grande)
    End Sub
    ''' <summary>
    ''' Imprime el formulario de vista previa
    ''' </summary>
    Private Sub ImprimirVistaPrevia(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInformesAccionesImprimir.Click
        If TypeOf (ActiveMdiChild) Is IVisorInforme Then DirectCast(ActiveMdiChild, IVisorInforme).Imprimir()
        If TypeOf (ActiveMdiChild) Is frmVisorInforme Then DirectCast(ActiveMdiChild, frmVisorInforme).Imprimir()
    End Sub
    ''' <summary>
    ''' Exporta el formulario actual
    ''' </summary>
    Private Sub ExportarVistaPrevia(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInformeAccionesExportar.Click
        If TypeOf (ActiveMdiChild) Is IVisorInforme Then DirectCast(ActiveMdiChild, IVisorInforme).Exportar()
        If TypeOf (ActiveMdiChild) Is frmVisorInforme Then DirectCast(ActiveMdiChild, frmVisorInforme).Exportar()
    End Sub

    Private Sub IrPrimeraPagina(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInformesPaginaPrimera.Click
        If TypeOf (ActiveMdiChild) Is IVisorInforme Then DirectCast(ActiveMdiChild, IVisorInforme).Primera()
    End Sub

    Private Sub IrAnteriorPagina(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInformesPaginaAnterior.Click
        If TypeOf (ActiveMdiChild) Is IVisorInforme Then DirectCast(ActiveMdiChild, IVisorInforme).Anterior()
    End Sub

    Private Sub IrSiguientePagina(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInformesPaginaSiguiente.Click
        If TypeOf (ActiveMdiChild) Is IVisorInforme Then DirectCast(ActiveMdiChild, IVisorInforme).Siguiente()
    End Sub

    Private Sub IrUltimaPagina(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInformesPaginaUltima.Click
        If TypeOf (ActiveMdiChild) Is IVisorInforme Then DirectCast(ActiveMdiChild, IVisorInforme).Ultima()
    End Sub
    ''' <summary>
    ''' Muestra / oculta los logotipos al imprimir una etiqueta
    ''' </summary>
    Private Sub MostrarLogotipos(ByVal sender As System.Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Handles btnInformesAccionesVerOcultarLogos.PropertyChanged
        If TypeOf (ActiveMdiChild) Is frmVisorInforme Then DirectCast(ActiveMdiChild, frmVisorInforme).MostrarCabeceras(btnInformesAccionesVerOcultarLogos.Checked)
    End Sub
#End Region
#Region "RIBBON - LISTADOS"
    Private Sub AbrirInformeStock(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuInformesStock_Stock.Click
        Quadralia.Aplicacion.AbrirInformeParametros(Me, frmVisorInformeParametros.Informe.InformeStock, "Informe de Stock", Quadralia.Aplicacion.Permisos.INFORMES, My.Resources.Empaquetado_32)
    End Sub
    Private Sub AbrirInformeEntradas(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuInformesStock_Entradas.Click
        Quadralia.Aplicacion.AbrirInformeParametros(Me, frmVisorInformeParametros.Informe.InformeEntradas, "Informe de Entradas", Quadralia.Aplicacion.Permisos.INFORMES, My.Resources.Entrada_32)
    End Sub
    Private Sub AbrirInformeVentas(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuInformesVentas_Ventas.Click
        Quadralia.Aplicacion.AbrirInformeParametros(Me, frmVisorInformeParametros.Informe.InformeVentas, "Informe de Ventas", Quadralia.Aplicacion.Permisos.INFORMES, My.Resources.Salida_32)
    End Sub
#End Region
#Region " GENERACION AUTOMATICA DE ETIQUETAS "
    Private Sub CambiarImpresionAutomatica(sender As Object, e As EventArgs) Handles chkGenerarEtiquetasImpresionAutomatica.PropertyChanged, chkGenerarEtiquetasImpresionAutomatica2.CheckedChanged
        If TypeOf (ActiveMdiChild) Is frmPesadoAutomatico Then DirectCast(ActiveMdiChild, frmPesadoAutomatico).ImpresionAutomatica = sender.Checked
    End Sub

    ''' <summary>
    ''' Genera una etiqueta con el peso actual que muestra la báscula
    ''' </summary>
    Private Sub GenerarEtiquetaPesoActual(sender As Object, e As EventArgs) Handles btnGenerarEtiquetasGenerarPesoActual.Click
        If TypeOf (ActiveMdiChild) Is frmPesadoAutomatico Then DirectCast(ActiveMdiChild, frmPesadoAutomatico).GenerarEtiquetaPesoActual(True)
    End Sub

    ''' <summary>
    ''' Genera una etiqueta solicitando al usuario previamente los datos
    ''' </summary>
    Private Sub GenerarEtiquetaManual(sender As Object, e As EventArgs) Handles btnGenerarEtiquetasGenerarPesoManual.Click
        If TypeOf (ActiveMdiChild) Is frmPesadoAutomatico Then DirectCast(ActiveMdiChild, frmPesadoAutomatico).GenerarEtiquetaManual()
    End Sub

    ''' <summary>
    ''' Descarta la última etiqueta generada
    ''' </summary>
    Private Sub DescartarUltimaEtiqueta(sender As Object, e As EventArgs) Handles btnGenerarEtiquetasDescartarUltimaEtiqueta.Click
        If TypeOf (ActiveMdiChild) Is frmPesadoAutomatico Then DirectCast(ActiveMdiChild, frmPesadoAutomatico).DescartarUltimaEtiqueta()
    End Sub

    ''' <summary>
    ''' Vuelve a imprimir la última etiqueta generada
    ''' </summary>
    Private Sub ReimprimirUltimaEtiqueta(sender As Object, e As EventArgs)
        If TypeOf (ActiveMdiChild) Is frmPesadoAutomatico Then DirectCast(ActiveMdiChild, frmPesadoAutomatico).ReimprimirUltimaEtiqueta(sender.Tag)
    End Sub
#End Region
#Region " LOTES DE SALIDA "
    ''' <summary>
    ''' Imprime todas las etiquetas del lote de salidaActual
    ''' </summary>
    Private Sub ImprimirEtiquetasLotesSalida(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If TypeOf (ActiveMdiChild) Is frmLoteSalida Then DirectCast(ActiveMdiChild, frmLoteSalida).ImprimirEtiquetas(sender.tag)
    End Sub

    ''' <summary>
    ''' Imprime todas las etiquetas del lote de salidaActual
    ''' </summary>
    Private Sub ImprimirLoteSalida(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If TypeOf (ActiveMdiChild) Is frmLoteSalida Then DirectCast(ActiveMdiChild, frmLoteSalida).Imprimir(sender.Tag)
    End Sub

    ''' <summary>
    ''' Imprime todas las etiquetas del lote de salidaActual
    ''' </summary>
    Private Sub ImprimirLoteEntrada(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLotesEntradaAccionesImprimir.Click
        If TypeOf (ActiveMdiChild) Is frmLoteEntrada Then DirectCast(ActiveMdiChild, frmLoteEntrada).Imprimir()
    End Sub

    ''' <summary>
    ''' Imprime todas las etiquetas del lote de salidaActual
    ''' </summary>
    Private Sub ImprimirSalidasLoteEntrada(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLotesEntradaAccionesImprimirListadoSalida.Click
        If TypeOf (ActiveMdiChild) Is frmLoteEntrada Then DirectCast(ActiveMdiChild, frmLoteEntrada).ImprimirSalidas()
    End Sub
#End Region
#Region " ETIQUETAS "
    ''' <summary>
    ''' Imprime la etiqueta actual
    ''' </summary>
    Private Sub ImprimirEtiqueta(sender As Object, e As EventArgs)
        If TypeOf (ActiveMdiChild) Is frmGestionEtiquetas Then DirectCast(ActiveMdiChild, frmGestionEtiquetas).Imprimir(sender.Tag)
    End Sub
#End Region
#Region " EMPAQUETADO "
    ''' <summary>
    ''' Abre el formulario con el lector de codigo de barras para el empaquetado
    ''' </summary>
    Private Sub btnEmpaquetadoAccionesLectorCodigos_Click(sender As Object, e As EventArgs) Handles btnEmpaquetadoAccionesLectorCodigos.Click
        If TypeOf (ActiveMdiChild) Is frmEmpaquetado Then DirectCast(ActiveMdiChild, frmEmpaquetado).AbrirLector()
    End Sub

    ''' <summary>
    ''' Imprime el empaquetado actual
    ''' </summary>
    Private Sub ImprimirEmpaquetado(sender As Object, e As EventArgs) Handles btnEmpaquetadoAccionesImprimir.Click
        If TypeOf (ActiveMdiChild) Is frmEmpaquetado Then DirectCast(ActiveMdiChild, frmEmpaquetado).Imprimir()
    End Sub

    ''' <summary>
    ''' Imprime el empaquetado actual Makro
    ''' </summary>
    Private Sub ImprimirEmpaquetadoMakro(sender As Object, e As EventArgs) Handles btnEmpaquetadoAccionesImprimirMakro.Click
        If TypeOf (ActiveMdiChild) Is frmEmpaquetado Then DirectCast(ActiveMdiChild, frmEmpaquetado).Imprimir(True)
    End Sub

    ''' <summary>
    ''' Imprime las etiquetas presentes en el empaquetado actual
    ''' </summary>
    Private Sub ImprimirEtiquetaEmpaquetado(sender As Object, e As EventArgs)
        If TypeOf (ActiveMdiChild) Is frmEmpaquetado Then DirectCast(ActiveMdiChild, frmEmpaquetado).ImprimirEtiquetas(sender.Tag)
    End Sub
#End Region
#Region " SINCRONIZACIÓN ETIQUETAS "
    ''' <summary>
    ''' Sincroniza las etiquetas de trazamare
    ''' </summary>
    Private Sub SincronizarEtiquetas(sender As Object, e As EventArgs) Handles btnSincronizarTrazamare.Click
        Quadralia.Aplicacion.SincronizarEtiquetasOns()
    End Sub

    ''' <summary>
    ''' Controla de que se tenga que levantar el servico de sincronización
    ''' </summary>
    Private Sub ConfiguracionCambiada(ByRef Configuracion As Configuracion)
        If Configuracion Is Nothing Then Exit Sub

        ' Miro si el servicio está instalado
        Dim ctl As System.ServiceProcess.ServiceController = System.ServiceProcess.ServiceController.GetServices().FirstOrDefault(Function(svc) svc.ServiceName = "qLotesSync")
        If ctl Is Nothing Then Exit Sub

        If Configuracion.IntervaloSincronizacion.HasValue AndAlso ctl.Status <> ServiceProcess.ServiceControllerStatus.Running Then
            Try
                ctl.Start()
            Catch ex As Exception
            End Try
        End If
    End Sub

    ''' <summary>
    ''' Muestra el log de sincronización del servicio
    ''' </summary>
    Private Sub VerLogSincronizacion(sender As Object, e As EventArgs) Handles rtnSincronizacionEtiquetasVerLog.Click
        Quadralia.Aplicacion.AbrirFormulario(Me, "Escritorio.frmLogSincronizacion", Quadralia.Aplicacion.Permisos.PUBLICO, My.Resources.Sincronizar_16)
    End Sub
#End Region
#Region " ACTUALIZACIONES "
    ''' <summary>
    ''' Busca si existen actualizaciones disponibles
    ''' </summary>
    Private Sub BuscarActualizaciones(sender As Object, e As EventArgs) Handles mnuConfiguracionComprobarActualizaciones.Click
        If Not upActualizador.IsBusy Then upActualizador.UpdateInteractive(Me, Kjs.AppLife.Update.Controller.ErrorDisplayLevel.ShowExceptionMessage, Kjs.AppLife.Update.Controller.ApplyUpdateOptions.AutoClose, Quadralia.Aplicacion.ObtenerParametroActualizacion())
    End Sub
#End Region
#Region " AYUDA "
    ''' <summary>
    ''' Abre el navegador por defecto con la web de quadralia
    ''' </summary>
    Private Sub IrWebQuadralia(sender As Object, e As EventArgs) Handles mnuAyudaWebQuadralia.Click
        Try
            Process.Start("http://www.quadralia.com")
        Catch ex As Exception
        End Try
    End Sub

    ''' <summary>
    ''' Abre el navegador por defecto con la web de soporte remoto
    ''' </summary>
    Private Sub IrWebSoporte(sender As Object, e As EventArgs) Handles mnuAyudaAyudaSoporteRemoto.Click
        Try
            Process.Start("http://www.quadralia.com/soporte-remoto")
        Catch ex As Exception
        End Try
    End Sub

    Private Sub mnuInformesStock_Entradas_Click(sender As Object, e As EventArgs) Handles mnuInformesStock_Entradas.Click

    End Sub
#End Region
End Class