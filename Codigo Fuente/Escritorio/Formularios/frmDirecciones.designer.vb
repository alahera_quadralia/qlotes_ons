﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDirecciones
    Inherits KryptonForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDirecciones))
        Me.hdrContenido = New ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup()
        Me.tblOrganizador = New System.Windows.Forms.TableLayoutPanel()
        Me.lblDescripcion = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblTipo = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtDescripcion = New Escritorio.Quadralia.Controles.aTextBox()
        Me.cboTipo = New Escritorio.Quadralia.Controles.aComboBox()
        Me.txtProvincia = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtPoblacion = New Escritorio.Quadralia.Controles.aTextBox()
        Me.lblProvincia = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtCP = New Escritorio.Quadralia.Controles.aTextBox()
        Me.lblPoblacion = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblCP = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtDireccion = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtPais = New Escritorio.Quadralia.Controles.aTextBox()
        Me.lblDireccion = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblPais = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.pCuerpo = New ComponentFactory.Krypton.Toolkit.KryptonPanel()
        Me.pBotonera = New ComponentFactory.Krypton.Toolkit.KryptonPanel()
        Me.btnAceptar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.btnCerrar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.epErrores = New Escritorio.Quadralia.Controles.Validacion.GestorErrores()
        CType(Me.hdrContenido, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.hdrContenido.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.hdrContenido.Panel.SuspendLayout()
        Me.hdrContenido.SuspendLayout()
        Me.tblOrganizador.SuspendLayout()
        CType(Me.cboTipo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pCuerpo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pCuerpo.SuspendLayout()
        CType(Me.pBotonera, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pBotonera.SuspendLayout()
        CType(Me.epErrores, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'hdrContenido
        '
        resources.ApplyResources(Me.hdrContenido, "hdrContenido")
        Me.hdrContenido.HeaderVisibleSecondary = False
        Me.hdrContenido.Name = "hdrContenido"
        '
        'hdrContenido.Panel
        '
        Me.hdrContenido.Panel.Controls.Add(Me.tblOrganizador)
        Me.hdrContenido.ValuesPrimary.Heading = resources.GetString("hdrContenido.ValuesPrimary.Heading")
        Me.hdrContenido.ValuesPrimary.Image = CType(resources.GetObject("hdrContenido.ValuesPrimary.Image"), System.Drawing.Image)
        '
        'tblOrganizador
        '
        Me.tblOrganizador.BackColor = System.Drawing.Color.Transparent
        resources.ApplyResources(Me.tblOrganizador, "tblOrganizador")
        Me.tblOrganizador.Controls.Add(Me.lblDescripcion, 0, 0)
        Me.tblOrganizador.Controls.Add(Me.lblTipo, 0, 1)
        Me.tblOrganizador.Controls.Add(Me.txtDescripcion, 2, 0)
        Me.tblOrganizador.Controls.Add(Me.cboTipo, 2, 1)
        Me.tblOrganizador.Controls.Add(Me.txtProvincia, 2, 9)
        Me.tblOrganizador.Controls.Add(Me.txtPoblacion, 2, 6)
        Me.tblOrganizador.Controls.Add(Me.lblProvincia, 0, 9)
        Me.tblOrganizador.Controls.Add(Me.txtCP, 2, 4)
        Me.tblOrganizador.Controls.Add(Me.lblPoblacion, 0, 6)
        Me.tblOrganizador.Controls.Add(Me.lblCP, 0, 4)
        Me.tblOrganizador.Controls.Add(Me.txtDireccion, 2, 2)
        Me.tblOrganizador.Controls.Add(Me.txtPais, 2, 10)
        Me.tblOrganizador.Controls.Add(Me.lblDireccion, 0, 2)
        Me.tblOrganizador.Controls.Add(Me.lblPais, 0, 10)
        Me.tblOrganizador.Name = "tblOrganizador"
        '
        'lblDescripcion
        '
        resources.ApplyResources(Me.lblDescripcion, "lblDescripcion")
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Values.Text = resources.GetString("lblDescripcion.Values.Text")
        '
        'lblTipo
        '
        resources.ApplyResources(Me.lblTipo, "lblTipo")
        Me.lblTipo.Name = "lblTipo"
        Me.lblTipo.Values.Text = resources.GetString("lblTipo.Values.Text")
        '
        'txtDescripcion
        '
        Me.txtDescripcion.AlwaysActive = False
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcion.controlarBotonBorrar = True
        resources.ApplyResources(Me.txtDescripcion, "txtDescripcion")
        Me.txtDescripcion.Formato = ""
        Me.txtDescripcion.mostrarSiempreBotonBorrar = False
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.seleccionarTodo = True
        '
        'cboTipo
        '
        Me.cboTipo.controlarBotonBorrar = True
        resources.ApplyResources(Me.cboTipo, "cboTipo")
        Me.cboTipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipo.DropDownWidth = 260
        Me.cboTipo.mostrarSiempreBotonBorrar = False
        Me.cboTipo.Name = "cboTipo"
        '
        'txtProvincia
        '
        Me.txtProvincia.controlarBotonBorrar = True
        resources.ApplyResources(Me.txtProvincia, "txtProvincia")
        Me.txtProvincia.Formato = ""
        Me.txtProvincia.mostrarSiempreBotonBorrar = False
        Me.txtProvincia.Name = "txtProvincia"
        Me.txtProvincia.seleccionarTodo = True
        '
        'txtPoblacion
        '
        Me.txtPoblacion.AlwaysActive = False
        Me.txtPoblacion.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtPoblacion.controlarBotonBorrar = True
        resources.ApplyResources(Me.txtPoblacion, "txtPoblacion")
        Me.txtPoblacion.Formato = ""
        Me.txtPoblacion.mostrarSiempreBotonBorrar = False
        Me.txtPoblacion.Name = "txtPoblacion"
        Me.txtPoblacion.seleccionarTodo = True
        '
        'lblProvincia
        '
        resources.ApplyResources(Me.lblProvincia, "lblProvincia")
        Me.lblProvincia.Name = "lblProvincia"
        Me.lblProvincia.Values.Text = resources.GetString("lblProvincia.Values.Text")
        '
        'txtCP
        '
        Me.txtCP.AlwaysActive = False
        Me.txtCP.controlarBotonBorrar = True
        resources.ApplyResources(Me.txtCP, "txtCP")
        Me.txtCP.Formato = ""
        Me.txtCP.mostrarSiempreBotonBorrar = False
        Me.txtCP.Name = "txtCP"
        Me.txtCP.seleccionarTodo = True
        '
        'lblPoblacion
        '
        resources.ApplyResources(Me.lblPoblacion, "lblPoblacion")
        Me.lblPoblacion.Name = "lblPoblacion"
        Me.lblPoblacion.Values.Text = resources.GetString("lblPoblacion.Values.Text")
        '
        'lblCP
        '
        resources.ApplyResources(Me.lblCP, "lblCP")
        Me.lblCP.Name = "lblCP"
        Me.lblCP.Values.Text = resources.GetString("lblCP.Values.Text")
        '
        'txtDireccion
        '
        Me.txtDireccion.AlwaysActive = False
        Me.txtDireccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDireccion.controlarBotonBorrar = True
        resources.ApplyResources(Me.txtDireccion, "txtDireccion")
        Me.txtDireccion.Formato = ""
        Me.txtDireccion.mostrarSiempreBotonBorrar = False
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.seleccionarTodo = True
        '
        'txtPais
        '
        Me.txtPais.controlarBotonBorrar = True
        resources.ApplyResources(Me.txtPais, "txtPais")
        Me.txtPais.Formato = ""
        Me.txtPais.mostrarSiempreBotonBorrar = False
        Me.txtPais.Name = "txtPais"
        Me.txtPais.seleccionarTodo = True
        '
        'lblDireccion
        '
        resources.ApplyResources(Me.lblDireccion, "lblDireccion")
        Me.lblDireccion.Name = "lblDireccion"
        Me.lblDireccion.Values.Text = resources.GetString("lblDireccion.Values.Text")
        '
        'lblPais
        '
        resources.ApplyResources(Me.lblPais, "lblPais")
        Me.lblPais.Name = "lblPais"
        Me.lblPais.Values.Text = resources.GetString("lblPais.Values.Text")
        '
        'pCuerpo
        '
        Me.pCuerpo.Controls.Add(Me.hdrContenido)
        Me.pCuerpo.Controls.Add(Me.pBotonera)
        resources.ApplyResources(Me.pCuerpo, "pCuerpo")
        Me.pCuerpo.Name = "pCuerpo"
        '
        'pBotonera
        '
        Me.pBotonera.Controls.Add(Me.btnAceptar)
        Me.pBotonera.Controls.Add(Me.btnCerrar)
        resources.ApplyResources(Me.pBotonera, "pBotonera")
        Me.pBotonera.Name = "pBotonera"
        '
        'btnAceptar
        '
        resources.ApplyResources(Me.btnAceptar, "btnAceptar")
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Values.Text = resources.GetString("btnAceptar.Values.Text")
        '
        'btnCerrar
        '
        resources.ApplyResources(Me.btnCerrar, "btnCerrar")
        Me.btnCerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Values.Text = resources.GetString("btnCerrar.Values.Text")
        '
        'epErrores
        '
        Me.epErrores.Alineacion = System.Windows.Forms.ErrorIconAlignment.TopLeft
        Me.epErrores.ContainerControl = Me
        '
        'frmDirecciones
        '
        Me.AcceptButton = Me.btnAceptar
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCerrar
        Me.ControlBox = False
        Me.Controls.Add(Me.pCuerpo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.Name = "frmDirecciones"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        CType(Me.hdrContenido.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrContenido.Panel.ResumeLayout(False)
        CType(Me.hdrContenido, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrContenido.ResumeLayout(False)
        Me.tblOrganizador.ResumeLayout(False)
        Me.tblOrganizador.PerformLayout()
        CType(Me.cboTipo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pCuerpo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pCuerpo.ResumeLayout(False)
        CType(Me.pBotonera, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pBotonera.ResumeLayout(False)
        CType(Me.epErrores, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pCuerpo As ComponentFactory.Krypton.Toolkit.KryptonPanel
    Friend WithEvents hdrContenido As ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup
    Friend WithEvents pBotonera As ComponentFactory.Krypton.Toolkit.KryptonPanel
    Friend WithEvents btnCerrar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents btnAceptar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents tblOrganizador As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblCP As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblDireccion As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblPoblacion As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblPais As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtDireccion As Quadralia.Controles.aTextBox
    Friend WithEvents txtPoblacion As Quadralia.Controles.aTextBox
    Friend WithEvents txtCP As Quadralia.Controles.aTextBox
    Friend WithEvents epErrores As Escritorio.Quadralia.Controles.Validacion.GestorErrores
    Friend WithEvents lblDescripcion As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblTipo As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtDescripcion As Quadralia.Controles.aTextBox
    Friend WithEvents lblProvincia As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents cboTipo As Quadralia.Controles.aComboBox
    Friend WithEvents txtProvincia As Quadralia.Controles.aTextBox
    Friend WithEvents txtPais As Quadralia.Controles.aTextBox
End Class
