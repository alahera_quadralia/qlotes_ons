﻿Imports System.IO
Imports System.IO.Compression

Public Class frmImportadorLoteEntrada
#Region " CLASES AUXILIARES "
    Private Class CamposImportación
        Public Property NombreCampo As String = String.Empty
        Public Property Descripcion As String = String.Empty
        Public Property ColumnaExcel As Integer = 0

        Public ReadOnly Property Self As CamposImportación
            Get
                Return Me
            End Get
        End Property
    End Class

    Private Class cAsignacion
        Public Property ColumnaFichero As String = String.Empty
        Public Property ColumnaBBDD As CamposImportación = Nothing
    End Class
#End Region
#Region " DECLARACIONES "
    Private _Importador As cImportador = Nothing            ' Importador de archivos Excel y CSV
    Private _PermitirCambio As Boolean = False
    Private _Contexto As Entidades = Nothing
#End Region
#Region " PROPIEDADES "
    Private ReadOnly Property Importador As cImportador
        Get
            If _Importador Is Nothing Then _Importador = New cImportador()
            Return _Importador
        End Get
    End Property

    Private ReadOnly Property Contexto As Entidades
        Get
            If _Contexto Is Nothing Then
                _Contexto = cDAL.Instancia.getContext
                _Contexto.ContextOptions.LazyLoadingEnabled = True
            End If
            Return _Contexto
        End Get
    End Property
#End Region
#Region " CONTROL DEL ASISTENTE "
    ''' <summary>
    ''' Controla los botones del asistente
    ''' </summary>
    Private Sub ControlarBotones() Handles tabAsistente.SelectedPageChanged
        btnAnterior.Visible = tabAsistente.SelectedIndex > 0 AndAlso tabAsistente.SelectedIndex < tabAsistente.Pages.Count - 1
        btnSiguiente.Text = IIf(tabAsistente.SelectedIndex = tabAsistente.Pages.Count - 1, "&Finalizar", "&Siguiente >")

        btnSiguiente.Enabled = Not tabAsistente.SelectedPage Is tbpGenerar

        If tabAsistente.SelectedPage Is tbpGenerar Then bgDatos.RunWorkerAsync()
    End Sub

    ''' <summary>
    ''' Controla los botones del asistente
    ''' </summary>
    Private Sub PreYPostAcciones(ByVal sender As Object, ByVal e As ComponentFactory.Krypton.Navigator.KryptonPageCancelEventArgs) Handles tabAsistente.Selecting
        If e.Index = tabAsistente.SelectedIndex OrElse tabAsistente.SelectedIndex = -1 Then Exit Sub
        If Not _PermitirCambio Then
            e.Cancel = True
            Exit Sub
        End If

        Select Case True
            Case tabAsistente.SelectedPage Is tbpFicheros
                CargarImportador()
            Case e.Item Is tbpResumen
                GenerarResumen()
        End Select

        _PermitirCambio = False
    End Sub

    ''' <summary>
    ''' Avanza a la siguiente página en caso de que no haya errores
    ''' </summary>
    Private Sub AvanzarPagina(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSiguiente.Click
        epErrores.Clear()
        Me.ValidateChildren(ValidationConstraints.Visible)

        If epErrores.Count = 0 AndAlso tabAsistente.SelectedIndex < tabAsistente.Pages.Count - 1 Then
            _PermitirCambio = True
            tabAsistente.SelectedIndex += 1
        ElseIf tabAsistente.SelectedIndex = tabAsistente.Pages.Count - 1 Then
            Me.Close()
        End If
    End Sub

    ''' <summary>
    ''' Retrocede a la pagina anterior
    ''' </summary>
    Private Sub RetrocederPagina(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnterior.Click
        If tabAsistente.SelectedIndex > 0 Then
            _PermitirCambio = True
            tabAsistente.SelectedIndex -= 1
        End If
    End Sub
#End Region
#Region " RUTINA DE INICIO "
    ''' <summary>
    ''' Rutnia de inicio
    ''' </summary>
    Private Sub Inicio(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CheckForIllegalCrossThreadCalls = False

        tabAsistente.SelectedIndex = 0
        CargarDatosMaestros()
        Quadralia.Formularios.AutoTabular(Me)

        ControlarBotones()
    End Sub

    ''' <summary>
    ''' Carga los datos maestros
    ''' </summary>
    Private Sub CargarDatosMaestros()
        Dim Lista As New List(Of CamposImportación)
        Lista.Add(New CamposImportación With {.NombreCampo = "Peso", .Descripcion = "Peso", .ColumnaExcel = 0})
        Lista.Add(New CamposImportación With {.NombreCampo = "Especie", .Descripcion = "Especie", .ColumnaExcel = 1})
        Lista.Add(New CamposImportación With {.NombreCampo = "FAO", .Descripcion = "FAO", .ColumnaExcel = 2})
        Lista.Add(New CamposImportación With {.NombreCampo = "SubZona", .Descripcion = "SubZona", .ColumnaExcel = 3})
        Lista.Add(New CamposImportación With {.NombreCampo = "Presentación", .Descripcion = "Presentación", .ColumnaExcel = 4})
        Lista.Add(New CamposImportación With {.NombreCampo = "Producción", .Descripcion = "Producción", .ColumnaExcel = 5})
        Lista.Add(New CamposImportación With {.NombreCampo = "Embarcación", .Descripcion = "Embarcación", .ColumnaExcel = 6})
        Lista.Add(New CamposImportación With {.NombreCampo = "Lote", .Descripcion = "Lote", .ColumnaExcel = 7})
        Lista.Add(New CamposImportación With {.NombreCampo = "Exportación", .Descripcion = "Fecha Desemb.", .ColumnaExcel = 8})

        With cboCampo
            .DataSource = Nothing
            .Items.Clear()

            .DataSource = Lista
            .DisplayMember = "Descripcion"
            .ValueMember = "Self"
        End With

        With cboProveedor
            .DataSource = Nothing
            .Items.Clear()

            .DisplayMember = "razonSocial"
            .ValueMember = "id"
            .DataSource = (From prv As Proveedor In Me.Contexto.Proveedores Where Not prv.fechaBaja.HasValue Order By prv.razonSocial Select prv).ToList

            .Clear()
        End With
    End Sub
#End Region
#Region " FICHEROS "
    ''' <summary>
    ''' Valida que se haya introducido un fichero
    ''' </summary>
    Private Sub ValidarRutasFicheros(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtArchivoExcel.Validating
        ValidarFicheros(sender, tbpFicheros, False)
    End Sub

    ''' <summary>
    ''' Busca un fichero en el equipo
    ''' </summary>
    Private Sub BuscarFicheroExcel(ByVal sender As System.Object, ByVal e As EventArgs) Handles btnBuscarExcel.Click
        Dim Buscador As New OpenFileDialog

        With Buscador
            .AutoUpgradeEnabled = True
            .CheckFileExists = True
            .CheckPathExists = True

            .Filter = "Archivos de Excel (*.xls, *.xlsx)|*.xls;*.xlsx|Todos los archivos (*.*)|*.*"
            .Multiselect = False
            .RestoreDirectory = True
            .Title = "Seleccione el fichero con los datos en Excel"
            If .ShowDialog <> Windows.Forms.DialogResult.OK Then Exit Sub

            txtArchivoExcel.Text = .FileName
        End With
    End Sub
#End Region
#Region " IMPORTACION "
    ''' <summary>
    '''
    ''' </summary>
    Private Sub CargarImportador()
        ' Si ya lo tenía cargado, no hago nada
        If Importador.RutaFichero = txtArchivoExcel.Text Then Exit Sub

        ' Limpio el formulario y preparo el importador
        cboHojas.DataSource = Nothing
        cboHojas.Items.Clear()

        Importador.PrimeraFilaContieneCabecera = chkCabeceraExcel.Checked

        ' Validaciones
        If String.IsNullOrEmpty(txtArchivoExcel.Text) Then Exit Sub
        If Not My.Computer.FileSystem.FileExists(txtArchivoExcel.Text) Then Exit Sub

        ' Cargo las hojas
        If Importador.Leer(txtArchivoExcel.Text) Then
            For i As Integer = 0 To Importador.DataSetDatos.Tables.Count - 1
                'Cargamos el combo con el nombre de las hojas
                cboHojas.Items.Add(Importador.DataSetDatos.Tables(i).TableName)
            Next

            'Selecciono la última hoja
            cboHojas.SelectedIndex = Importador.DataSetDatos.Tables.Count - 1
        Else
            _Importador = New cImportador()
        End If
    End Sub

    ''' <summary>
    ''' Carga en el visor de contenido la hoja del archivo excel seleccionada en el combobox
    ''' </summary>
    Private Sub MostrarTabla(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboHojas.SelectedValueChanged
        If cboHojas.SelectedIndex > -1 AndAlso Importador IsNot Nothing Then
            ' Cargo el DGV del archivo
            Dim MaxIndice = Math.Min(10, Importador.DataSetDatos.Tables(cboHojas.SelectedItem).Rows.Count)

            Dim Tabla As DataTable = Importador.DataSetDatos.Tables(cboHojas.SelectedItem).Clone()

            ' Pillamos solo las filas que nos interesa
            For i As Integer = 0 To MaxIndice - 1
                Tabla.ImportRow(Importador.DataSetDatos.Tables(cboHojas.SelectedItem).Rows(i))
            Next

            ' dgvArchivo.DataSource = Importador.DataSetDatos.Tables(cboHojas.SelectedItem)
            dgvArchivo.DataSource = Tabla

            'Limpiamos el control de seleccion de columnas
            dgvEnlazar.Rows.Clear()

            'Metemos tantas filas como columnas tiene el dataTable
            For i As Integer = 0 To Importador.DataSetDatos.Tables(cboHojas.SelectedItem).Columns.Count - 1
                Dim columna As DataColumn = Importador.DataSetDatos.Tables(cboHojas.SelectedItem).Columns(i)

                dgvEnlazar.Rows.Add(1)
                dgvEnlazar.Item(0, dgvEnlazar.Rows.Count - 1).Value = True

                dgvEnlazar.Item(1, dgvEnlazar.Rows.Count - 1).Value = "Columna: " & columna.ColumnName
                dgvEnlazar.Item(1, dgvEnlazar.Rows.Count - 1).Tag = columna.ColumnName

                dgvEnlazar.Item(2, dgvEnlazar.Rows.Count - 1).Value = (From it As CamposImportación In DirectCast(cboCampo.DataSource, List(Of CamposImportación)) Where it.ColumnaExcel = i Select it).FirstOrDefault
            Next
        End If
    End Sub

    ''' <summary>
    ''' Pone el combo con indice -1 en caso de que el valor seleccionado sea nulo
    ''' </summary>
    Private Sub RestablecerIndexCombo(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvEnlazar.EditingControlShowing
        If Not TypeOf (e.Control) Is ComboBox Then Exit Sub

        Try
            If dgvEnlazar.CurrentCell.Value Is Nothing Then
                DirectCast(e.Control, ComboBox).SelectedIndex = -1
            End If
        Catch ex As Exception
        End Try
    End Sub

    ''' <summary>
    ''' Valida que el usuario haya seleccionado un valor del combo
    ''' </summary>
    Private Sub ComboBoxObligatorio(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cboHojas.Validating
        ValidarComboObligatorio(cboHojas, tbpAsignaciones)
    End Sub
#End Region
#Region " PARAMETROS "
    ''' <summary>
    ''' Valida el combo de la tienda
    ''' </summary>
    Private Sub ValidarTienda(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cboProveedor.Validating
        ValidarComboObligatorio(cboProveedor, tbpParametros)
    End Sub

    Private Sub ValidarFecha(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles dtpFecha.Validating
        ValidarDTPOligatorio(dtpFecha, tbpParametros)
    End Sub
#End Region
#Region " RESUMEN "
    ''' <summary>
    ''' Genera el resumen de los parámetros establecidos
    ''' </summary>
    Private Sub GenerarResumen()
        Dim sb = New System.Text.StringBuilder()
        sb.Append("{\rtf1\ansi")
        sb.Append("\fs22 \b Ficheros \b0{\pard\par}\fs18")
        sb.Append("\tab\b Excel: \b0" & txtArchivoExcel.Text.Replace("\", "\\") & "{\pard\par}")
        If chkCabeceraExcel.Checked Then sb.Append("\tab\b Primera fila como cabecera \b0 {\pard\par}")

        sb.Append("{\pard\par}")
        sb.Append("\fs22 \b Asignaciones \b0{\pard\par}\fs18")
        sb.Append("\tab\b Hoja: \b0" & cboHojas.Text & "{\pard\par}")
        sb.Append("\tab\b Columnas: \b0 {\pard\par}")
        For Each Fila As DataGridViewRow In dgvEnlazar.Rows
            If DirectCast(Fila.Cells(0), DataGridViewCheckBoxCell).FormattedValue Then
                sb.Append("\tab\tab " & Fila.Cells(1).Tag & " -> " & Fila.Cells(2).FormattedValue)
                sb.Append("{\pard\par}")
            End If
        Next

        sb.Append("{\pard\par}")
        sb.Append("\fs22 \b Parámetros \b0{\pard\par}\fs18")
        sb.Append("\tab\b Fecha: \b0 " & dtpFecha.Value.ToShortDateString & "{\pard\par}")
        sb.Append("\tab\b Proveedor: \b0 " & cboProveedor.Text & "{\pard\par}")

        sb.Append("}")

        txtResumen.Clear()
        txtResumen.Rtf = sb.ToString
    End Sub
#End Region
#Region " GENERACION "
    ''' <summary>
    ''' Genera los archivos deseados
    ''' </summary>
    Private Sub GenerarArchivo(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgDatos.DoWork
        Application.DoEvents()

        Try
            ' Validaciones
            If String.IsNullOrEmpty(txtArchivoExcel.Text) OrElse Not My.Computer.FileSystem.FileExists(txtArchivoExcel.Text) Then Throw New Exception("No se encuentra el archivo de excel")

            ' Obtengo las asignaciones
            Dim Asignaciones As New List(Of cAsignacion)

            For Each Fila As DataGridViewRow In dgvEnlazar.Rows
                If DirectCast(Fila.Cells(0), DataGridViewCheckBoxCell).FormattedValue Then


                    Asignaciones.Add(New cAsignacion With {
                                     .ColumnaFichero = Fila.Cells(1).Tag,
                                     .ColumnaBBDD = Fila.Cells(2).Value
                    })
                End If
            Next

            If Asignaciones.Count = 0 Then Throw New Exception("No se establecieron asignaciones entre columnas")

            ' Preparo los datos
            pgbProgeso.Minimum = 0
            pgbProgeso.Maximum = Importador.DataSetDatos.Tables(cboHojas.SelectedItem).Rows.Count
            pgbProgeso.Step = 1

            ' Notifico progreso
            lblProgreso.Text = "Importando registros"

            ' Comienzo la importación
            Dim LoteEntrada As New LoteEntrada()
            Contexto.LotesEntrada.AddObject(LoteEntrada)

            If cboProveedor.SelectedIndex > -1 Then LoteEntrada.Proveedor = cboProveedor.SelectedItem Else LoteEntrada.Proveedor = Nothing
            If Not IsDBNull(dtpFecha.ValueNullable) Then LoteEntrada.fecha = dtpFecha.Value Else LoteEntrada.fecha = DateTime.Now
            LoteEntrada.observaciones = String.Empty
            LoteEntrada.UsuarioReference.EntityKey = Quadralia.Aplicacion.UsuarioConectado.EntityKey
            LoteEntrada.codigo = String.Empty

            For Each Fila As DataRow In Importador.DataSetDatos.Tables(cboHojas.SelectedItem).Rows
                Dim Aux As LoteEntradalinea = ImportarRegistro(Fila, Asignaciones)
                If Aux IsNot Nothing Then LoteEntrada.Lineas.Add(Aux)
            Next

            Try
                Contexto.SaveChanges()
            Catch ex As Exception
                Debugger.Break()
            End Try

            ' Si estoy añadiendo, obtengo un número de Albarán
            Dim AuxCodigo As New Data.Objects.ObjectParameter("codigoEnt", LoteEntrada.codigo)
            Dim Resultado = Contexto.CalcularCodigoEntrada(LoteEntrada.id, AuxCodigo)
            LoteEntrada.codigo = AuxCodigo.Value
            Contexto.AcceptAllChanges()

            lblProgreso.Text = "Registros importados correctamente"
            e.Result = LoteEntrada
        Catch ex As Exception
            lblProgreso.Text = ex.Message
            btnAnterior.Enabled = True
            btnAnterior.Visible = True

            e.Result = Nothing
        Finally
            btnSiguiente.Enabled = True
        End Try
    End Sub

    ''' <summary>
    ''' Importa el producto deseado a la base de datos
    ''' </summary>
    Private Function ImportarRegistro(Datos As DataRow, Asignaciones As List(Of cAsignacion)) As LoteEntradalinea
        ' Traspaso los datos del artículo
        Dim Registro As New LoteEntradalinea
        Dim Aux As Object = Nothing

        If Asignaciones.Any(Function(asg) asg.ColumnaBBDD IsNot Nothing AndAlso asg.ColumnaBBDD.ColumnaExcel = 0) Then
            Aux = Datos(Asignaciones.Where(Function(asg) asg.ColumnaBBDD IsNot Nothing AndAlso asg.ColumnaBBDD.ColumnaExcel = 0).FirstOrDefault().ColumnaFichero)
            If Aux IsNot Nothing AndAlso Aux IsNot DBNull.Value Then Registro.cantidad = Aux
        End If

        If Asignaciones.Any(Function(asg) asg.ColumnaBBDD IsNot Nothing AndAlso asg.ColumnaBBDD.ColumnaExcel = 1) Then
            Aux = Datos(Asignaciones.Where(Function(asg) asg.ColumnaBBDD IsNot Nothing AndAlso asg.ColumnaBBDD.ColumnaExcel = 1).FirstOrDefault().ColumnaFichero)
            If Aux IsNot Nothing AndAlso Aux IsNot DBNull.Value Then
                Dim aux2 As String = Aux.ToString.ToUpper

                ' Busco la especie, si no está, tomo la predeterminada
                Dim Especie As Especie = (From esp As Especie In Me.Contexto.Especies Where esp.denominacionComercial.ToUpper = aux2 OrElse esp.denominacionCientifica.ToUpper = aux2 Select esp).FirstOrDefault
                If Especie Is Nothing Then Especie = (From esp As Especie In Me.Contexto.Especies Where esp.predeterminada Select esp).FirstOrDefault
                If Especie Is Nothing Then Especie = (From esp As Especie In Me.Contexto.Especies Select esp).FirstOrDefault

                Registro.Especie = Especie
            End If
        End If

        If Asignaciones.Any(Function(asg) asg.ColumnaBBDD IsNot Nothing AndAlso asg.ColumnaBBDD.ColumnaExcel = 2) Then
            Aux = Datos(Asignaciones.Where(Function(asg) asg.ColumnaBBDD IsNot Nothing AndAlso asg.ColumnaBBDD.ColumnaExcel = 2).FirstOrDefault().ColumnaFichero)
            If Aux IsNot Nothing AndAlso Aux IsNot DBNull.Value Then
                ' Busco la ZonaFAO, si no está, tomo la predeterminada
                Dim aux2 As String = Aux.ToString.ToUpper

                Dim ZonaFAO As ZonaFao = (From fao As ZonaFao In Me.Contexto.ZonasFao Where fao.descripcion.ToUpper = aux2 Select fao).FirstOrDefault
                If ZonaFAO Is Nothing Then ZonaFAO = (From fao As ZonaFao In Me.Contexto.ZonasFao Where fao.predeterminada Select fao).FirstOrDefault

                Registro.ZonaFao = ZonaFAO
            End If
        End If

        If Asignaciones.Any(Function(asg) asg.ColumnaBBDD IsNot Nothing AndAlso asg.ColumnaBBDD.ColumnaExcel = 3) Then
            Aux = Datos(Asignaciones.Where(Function(asg) asg.ColumnaBBDD IsNot Nothing AndAlso asg.ColumnaBBDD.ColumnaExcel = 3).FirstOrDefault().ColumnaFichero)
            If Aux IsNot Nothing AndAlso Aux IsNot DBNull.Value Then
                ' Busco la SubZonaFAO, si no está, tomo la predeterminada
                Dim aux2 As String = Aux.ToString.ToUpper

                Dim SubZonaFAO As SubZona = (From fao As SubZona In Me.Contexto.SubZonas Where fao.descripcion.ToUpper = aux2 Select fao).FirstOrDefault
                If SubZonaFAO Is Nothing Then SubZonaFAO = (From fao As SubZona In Me.Contexto.SubZonas Where fao.predeterminada Select fao).FirstOrDefault

                Registro.SubZona = SubZonaFAO
            End If
        End If

        If Asignaciones.Any(Function(asg) asg.ColumnaBBDD IsNot Nothing AndAlso asg.ColumnaBBDD.ColumnaExcel = 4) Then
            Aux = Datos(Asignaciones.Where(Function(asg) asg.ColumnaBBDD IsNot Nothing AndAlso asg.ColumnaBBDD.ColumnaExcel = 4).FirstOrDefault().ColumnaFichero)
            If Aux IsNot Nothing AndAlso Aux IsNot DBNull.Value Then
                ' Busco la Presentacion, si no está, tomo la predeterminada
                Dim aux2 As String = Aux.ToString.ToUpper

                Dim Presentacion As Presentacion = (From pre As Presentacion In Me.Contexto.FormasPresentacion Where pre.descripcion.ToUpper = aux2 Select pre).FirstOrDefault
                If Presentacion Is Nothing Then Presentacion = (From pre As Presentacion In Me.Contexto.FormasPresentacion Where pre.predeterminada Select pre).FirstOrDefault

                Registro.Presentacion = Presentacion
            End If
        End If

        If Asignaciones.Any(Function(asg) asg.ColumnaBBDD IsNot Nothing AndAlso asg.ColumnaBBDD.ColumnaExcel = 5) Then
            Aux = Datos(Asignaciones.Where(Function(asg) asg.ColumnaBBDD IsNot Nothing AndAlso asg.ColumnaBBDD.ColumnaExcel = 5).FirstOrDefault().ColumnaFichero)
            If Aux IsNot Nothing AndAlso Aux IsNot DBNull.Value Then
                ' Busco la Producción, si no está, tomo la predeterminada
                Dim aux2 As String = Aux.ToString.ToUpper

                Dim Produccion As MetodoProduccion = (From pro As MetodoProduccion In Me.Contexto.MetodosProduccion Where pro.descripcion.ToUpper = aux2 Select pro).FirstOrDefault
                If Produccion Is Nothing Then Produccion = (From pro As MetodoProduccion In Me.Contexto.MetodosProduccion Where pro.predeterminada Select pro).FirstOrDefault

                Registro.Produccion = Produccion
            End If
        End If

        If Asignaciones.Any(Function(asg) asg.ColumnaBBDD IsNot Nothing AndAlso asg.ColumnaBBDD.ColumnaExcel = 6) Then
            Aux = Datos(Asignaciones.Where(Function(asg) asg.ColumnaBBDD IsNot Nothing AndAlso asg.ColumnaBBDD.ColumnaExcel = 6).FirstOrDefault().ColumnaFichero)
            If Aux IsNot Nothing AndAlso Aux IsNot DBNull.Value Then
                ' Busco la Producción, si no está, la meto
                Dim aux2 As String = Aux.ToString.ToUpper

                Dim Embarcacion As Barco = (From bar As Barco In Me.Contexto.Barcos Where bar.descripcion.ToUpper = aux2 Select bar).FirstOrDefault
                If Embarcacion Is Nothing Then
                    Embarcacion = New Barco() With {.descripcion = Aux, .sincronizadoWeb = False, .predeterminada = False}
                    Contexto.Barcos.AddObject(Embarcacion)
                End If

                Registro.Barco = Embarcacion
            End If
        End If

        If Asignaciones.Any(Function(asg) asg.ColumnaBBDD IsNot Nothing AndAlso asg.ColumnaBBDD.ColumnaExcel = 7) Then
            Aux = Datos(Asignaciones.Where(Function(asg) asg.ColumnaBBDD IsNot Nothing AndAlso asg.ColumnaBBDD.ColumnaExcel = 7).FirstOrDefault().ColumnaFichero)
            If Aux IsNot Nothing AndAlso Aux IsNot DBNull.Value Then Registro.numeroLote = Aux
        End If

        If Asignaciones.Any(Function(asg) asg.ColumnaBBDD IsNot Nothing AndAlso asg.ColumnaBBDD.ColumnaExcel = 8) Then
            Aux = Datos(Asignaciones.Where(Function(asg) asg.ColumnaBBDD IsNot Nothing AndAlso asg.ColumnaBBDD.ColumnaExcel = 8).FirstOrDefault().ColumnaFichero)
            If Aux IsNot Nothing AndAlso Aux IsNot DBNull.Value Then
                Dim Fecha As New DateTime
                If Not DateTime.TryParse(Aux, Fecha) Then Fecha = DateTime.Now

                Registro.fechaDesembarco = Fecha
            End If
        End If

        Return Registro
    End Function

    ''' <summary>
    ''' Muestra el resultado de la importación de artículos
    ''' </summary>
    Private Sub MostrarResultadoImportacion(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgDatos.RunWorkerCompleted
        If e.Result Is Nothing Then
            Quadralia.Aplicacion.MostrarMessageBox("No se pudo completar el proceso de importación. Consulte los errores para ver los problemas ocurridos", "Fallo al sincronizar", MessageBoxButtons.OK, MessageBoxIcon.Error)
            btnSiguiente.PerformClick()
            Exit Sub
        End If

        Dim Registro As LoteEntrada = e.Result
        If Quadralia.Aplicacion.MostrarMessageBox(String.Format("Se ha generado correctamente el lote de entrada con número: {1}{0}¿Desea visualizarlo ahora?", Environment.NewLine, Registro.codigo), "Proceso completado correctamente", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then Me.Close()

        Dim Formulario As frmLoteEntrada = Quadralia.Aplicacion.AbrirFormulario(Me.ParentForm, "Escritorio.frmLoteEntrada", Quadralia.Aplicacion.Permisos.LOTES_ENTRADA, My.Resources.Entrada_16, True)
        Formulario.IdRegistro = Registro.id

        Me.Close()
    End Sub
#End Region
#Region " VALIDACIONES "
    ''' <summary>
    ''' Valida un combo marcado como obligatorio
    ''' </summary>
    Private Sub ValidarComboObligatorio(ByRef sender As Quadralia.Controles.aComboBox, ByRef Pagina As ComponentFactory.Krypton.Navigator.KryptonPage)
        If tabAsistente.SelectedPage IsNot Pagina Then Exit Sub

        If sender.SelectedIndex = -1 Then
            epErrores.SetError(sender, "Debe seleccionar un valor de la lista")
        Else
            epErrores.SetError(sender, "")
        End If
    End Sub

    ''' <summary>
    ''' Valida un combo marcado como obligatorio
    ''' </summary>
    Private Sub ValidarDTPOligatorio(ByRef sender As Quadralia.Controles.aDateTimePicker, ByRef Pagina As ComponentFactory.Krypton.Navigator.KryptonPage)
        If tabAsistente.SelectedPage IsNot Pagina Then Exit Sub

        If IsDBNull(sender.ValueNullable) Then
            epErrores.SetError(sender, "Debe seleccionar una fecha")
        Else
            epErrores.SetError(sender, "")
        End If
    End Sub

    ''' <summary>
    ''' Valida que se haya introducido una ruta válida
    ''' </summary>
    Private Sub ValidarFicheros(ByRef sender As Quadralia.Controles.aTextBox, ByRef Pagina As ComponentFactory.Krypton.Navigator.KryptonPage, ByVal EsDirectorio As Boolean)
        If tabAsistente.SelectedPage IsNot Pagina Then Exit Sub


        If EsDirectorio AndAlso (String.IsNullOrEmpty(sender.Text) OrElse Not My.Computer.FileSystem.DirectoryExists(sender.Text)) Then
            epErrores.SetError(sender, "Debe especificar un directorio de destino")
        ElseIf Not EsDirectorio AndAlso (String.IsNullOrEmpty(sender.Text) OrElse Not My.Computer.FileSystem.FileExists(sender.Text)) Then
            epErrores.SetError(sender, "Debe especificar la ruta del fichero")
        Else
            epErrores.SetError(sender, "")
        End If
    End Sub
#End Region
End Class