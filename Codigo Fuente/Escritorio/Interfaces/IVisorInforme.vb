﻿Public Interface IVisorInforme
    Sub Imprimir()
    Sub Exportar()

    Sub Primera()
    Sub Anterior()
    Sub Siguiente()
    Sub Ultima()

    Sub MostrarCabeceras(ByVal Mostrar As Boolean)
    Sub MostrarAtencion(ByVal Mostrar As Boolean)
    Sub MostrarTelefonos(ByVal Mostrar As Boolean)
    Sub MostrarNombre(ByVal Mostrar As Boolean)
End Interface
