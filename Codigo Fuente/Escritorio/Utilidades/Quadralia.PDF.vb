﻿Imports System.Collections.Generic
Imports System.Text
Imports System.Windows.Forms
Imports System.Drawing.Printing
Imports System.Drawing
Imports System.Text.RegularExpressions
Imports System.IO
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports iTextSharp.text.html

Namespace Quadralia
    Namespace PDF
        Class UtilidadesPDF
            ''' <summary>
            ''' Obtiene la imagen de la pagina indicada
            ''' </summary>
            Public Shared Function ObtenerImagenPaginaPDF(ByVal epdfDoc As PDFLibNet.PDFWrapper, _
                                                          ByVal ePagina As Long, _
                                                          Optional ByVal eDPI As Short = 150, _
                                                          Optional ByVal eCalidadJPG As Short = 100) As System.Drawing.Image
                If epdfDoc IsNot Nothing Then
                    Try
                        Cursor.Current = Cursors.WaitCursor
                        Dim RutaTemp As String = My.Computer.FileSystem.GetTempFileName
                        epdfDoc.ExportJpg(RutaTemp, ePagina, ePagina, eDPI, eCalidadJPG)
                        While epdfDoc.IsJpgBusy
                            Application.DoEvents()
                        End While

                        Return Quadralia.Imagenes.ObtenerImagenStream(RutaTemp)
                    Catch ex As System.Exception
                        Return Nothing
                    Finally
                        Cursor.Current = Cursors.Default
                    End Try
                Else
                    Return Nothing
                End If
            End Function


            ''' <summary>
            ''' Conversor de HTML a PDF
            ''' </summary>
            Public Shared Function Exportar_HTML_PDF(ByVal eRutaHTML As String, _
                                                     ByVal eRutaPDF As String) As Boolean
                Dim ParaDevolver As Boolean = True
                Try
                    Cursor.Current = Cursors.WaitCursor
                    Dim document As New iTextSharp.text.Document(PageSize.A4, 80, 50, 30, 65)
                    Using fs As New FileStream(eRutaPDF, FileMode.Create)
                        Dim PdfWriter As PdfWriter = PdfWriter.GetInstance(document, fs)
                        Using sr As New StreamReader(eRutaHTML)
                            ' SIGUIENTE:  Crear un parser que sea capaz de cerrar las tablas para evitar problema                            
                            ' No se puede convertir un objeto de tipo 'iTextSharp.text.html.simpleparser.CellWrapper' al tipo 'iTextSharp.text.Paragraph'.
                            '
                            ' Buscando en interneet encontre que este error se produce debido a que están mal cerrados los tag <tr> o <td> en las tablas
                            ' lo que provoca que al parsear no sea capaz de convertir los objetos.
                            ' Ahora mismo genera la excepción y no exporta el documento. Esto sólo ocurre si se copia y pega de internet
                            ' y al hacer esto, queden las tablas mal cerradas...
                            '
                            Dim parsedList As IList = iTextSharp.text.html.simpleparser.HTMLWorker.ParseToList(sr, Nothing)
                            document.Open()
                            For Each objeto As Object In parsedList
                                document.Add(objeto)
                            Next
                            document.Close()
                        End Using
                    End Using
                Catch ex As System.Exception
                    ParaDevolver = False
                Finally
                    Cursor.Current = Cursors.Default
                End Try

                Return ParaDevolver
            End Function

            Public Shared Function ImprimirPDF(ByVal eRutaPDF As String) As Boolean
                Dim ParaDevolver As Boolean = True
                Dim _pdfDoc As PDFLibNet.PDFWrapper = Nothing
                Try
                    Cursor.Current = Cursors.WaitCursor
                    _pdfDoc = New PDFLibNet.PDFWrapper()
                    _pdfDoc.LoadPDF(eRutaPDF)
                    ParaDevolver = Quadralia.PDF.ImprimirPDF.PrintImagesToPrinter(_pdfDoc)
                Catch ex As System.Exception
                    ParaDevolver = False
                Finally
                    Cursor.Current = Cursors.WaitCursor
                    If _pdfDoc IsNot Nothing Then _pdfDoc.Dispose()
                End Try
                Return ParaDevolver
            End Function
        End Class

        ''' <summary>
        ''' Clase encargarda de imprimir PDF
        ''' </summary>        
        Class ImprimirPDF
            Private printDocument As PrintDocument
            Private pdfDoc As PDFLibNet.PDFWrapper
            Private endPage As Integer
            Private currentPage As Integer
            Private finishedPrinting As Boolean = False
            Private Const RENDER_DPI As Integer = 300

            Private Sub New(ByVal printDocument As PrintDocument, ByVal pdfDoc As PDFLibNet.PDFWrapper)
                Me.printDocument = printDocument
                Me.pdfDoc = pdfDoc
                Me.endPage = printDocument.PrinterSettings.ToPage
                Me.currentPage = printDocument.PrinterSettings.FromPage
                AddHandler printDocument.PrintPage, AddressOf PrintDocument_PrintPage
                AddHandler printDocument.EndPrint, AddressOf PrintDocument_EndPrint
            End Sub

            Private Sub PrintDocument_EndPrint(ByVal sender As Object, ByVal e As PrintEventArgs)
                finishedPrinting = True
            End Sub

            Private Sub PrintDocument_PrintPage(ByVal sender As Object, ByVal e As PrintPageEventArgs)
                Dim image As System.Drawing.Image = Quadralia.PDF.UtilidadesPDF.ObtenerImagenPaginaPDF(pdfDoc, currentPage)

                If image IsNot Nothing Then
                    AutoRotate(image, e.Graphics.VisibleClipBounds)

                    Dim xFactor As Single = ((e.Graphics.VisibleClipBounds.Width / 100) * image.HorizontalResolution) / image.Width
                    Dim yFactor As Single = ((e.Graphics.VisibleClipBounds.Height / 100) * image.VerticalResolution) / image.Height

                    Dim scalePercentage As Single = If((yFactor > xFactor), xFactor, yFactor)
                    Dim optimalDPI As Integer = CInt(Math.Truncate(RENDER_DPI * scalePercentage))

                    e.Graphics.ScaleTransform(scalePercentage, scalePercentage)
                    e.Graphics.DrawImage(image, 0, 0)

                    e.HasMorePages = (currentPage < endPage)

                    image.Dispose()
                    currentPage += 1
                End If
            End Sub

            Private Sub AutoRotate(ByVal image As System.Drawing.Image, ByVal bounds As RectangleF)
                If (image.Height > image.Width And bounds.Width > bounds.Height) Or (image.Width > image.Height And bounds.Height > bounds.Width) Then
                    image.RotateFlip(RotateFlipType.Rotate270FlipNone)
                End If
            End Sub


            Friend Shared Function PrintImagesToPrinter(ByVal pdfDoc As PDFLibNet.PDFWrapper) As Boolean
                Dim PreferenciasImpresion As New PrinterSettings

                Try

                    If Not ImpresoraDisponible() Then
                        Dim pd As New PrintDialog()
                        pd.AllowPrintToFile = False
                        pd.AllowSomePages = True
                        pd.PrinterSettings.FromPage = InlineAssignHelper(pd.PrinterSettings.MinimumPage, 1)
                        pd.PrinterSettings.ToPage = InlineAssignHelper(pd.PrinterSettings.MaximumPage, pdfDoc.PageCount)

                        If pd.ShowDialog() <> DialogResult.OK Then Return False
                        PreferenciasImpresion = pd.PrinterSettings
                    Else
                        PreferenciasImpresion.Copies = 1
                        PreferenciasImpresion.FromPage = 1
                        PreferenciasImpresion.ToPage = pdfDoc.PageCount
                        PreferenciasImpresion.PrinterName = cConfiguracionAplicacion.Instancia.Impresora
                    End If

                    Dim printDocument As New PrintDocument()
                    printDocument.PrintController = New StandardPrintController()
                    printDocument.PrinterSettings = PreferenciasImpresion

                    ' printDocument.DefaultPageSettings.PaperSize = New System.Drawing.Printing.PaperSize("A5", 584, 827)
                    Dim printUtil As New ImprimirPDF(printDocument, pdfDoc)
                    Cursor.Current = Cursors.WaitCursor
                    printUtil.printDocument.Print()
                    Dim retVal As Boolean = printUtil.finishedPrinting
                    printUtil = Nothing
                    GC.Collect()
                    Cursor.Current = Cursors.[Default]

                    Return True
                Catch ex As Exception
                    Return False
                End Try
            End Function

            Private Shared Function ImpresoraDisponible() As Boolean
                If String.IsNullOrEmpty(cConfiguracionAplicacion.Instancia.Impresora) Then Return False

                For Each Impresora As String In Printing.PrinterSettings.InstalledPrinters
                    If String.Equals(Impresora, cConfiguracionAplicacion.Instancia.Impresora, StringComparison.OrdinalIgnoreCase) Then Return True
                Next

                Return False
            End Function


            Private Shared Function InlineAssignHelper(Of T)(ByRef target As T, ByVal value As T) As T
                target = value
                Return value
            End Function
        End Class
    End Namespace
End Namespace
