﻿Imports System.Threading

Namespace Quadralia
    Namespace Aplicacion
        Module Aplicacion
#Region " ESTRUCTURAS Y ENUMERADOS "
            Public Enum Permisos As UInteger
                PUBLICO = 0
                USUARIO_AUTENTICADO = 1
                CONFIGURACION_PROGRAMA = 2
                USUARIOS = 4
                DATOS_MAESTROS = 8
                INFORMES = 16
                PROVEEDORES = 32
                LOTES_ENTRADA = 64
                LOTES_SALIDA = 128
                CLIENTES = 256
                EXPEDIDORES = 512
                ETIQUETAS = 1024
                EMPAQUETADO = 2048
            End Enum
#End Region
#Region " DECLARACIONES "
            Private WithEvents Splash As frmSplash = Nothing
            Private Const SegundosMinimosSplash As Int32 = 5
            Private HiloSplash As Thread
            Public ConexionBBDD As Boolean = False
            Private _VentanaMostrada As Boolean = False
            Public UsuarioConectado As Usuario = Nothing
            Public SalidaForzada As Boolean = False

            Public GestorInformes As New cFormatosInforme()
#End Region
#Region " SPLASH FORM "
            ''' <summary>
            ''' Cierra el formulario de Splash
            ''' </summary>
            Private Sub CerrarSplash()
                If Splash Is Nothing Then Exit Sub

                If Splash.SegundosMostrado() < SegundosMinimosSplash Then Thread.Sleep((SegundosMinimosSplash - Splash.SegundosMostrado) * 1000)

                ' Shut down the splash screen
                Try
                    Splash.Invoke(New EventHandler(AddressOf Splash.Dispose))
                Catch ex As Exception

                End Try

                While HiloSplash.IsAlive
                    Application.DoEvents()
                End While

                Application.DoEvents()
            End Sub

            ''' <summary>
            ''' Rutina de inicio del formulario Splash
            ''' </summary>
            Public Sub MostrarSplash()
                ' Singleton
                If Splash Is Nothing Then Splash = New frmSplash()
                Application.Run(Splash)
            End Sub

            ''' <summary>
            ''' Muestra un mensaje en el formulario de splash
            ''' </summary>
            ''' <param name="Mensaje">Mensaje que queremos mostrar</param>
            Private Sub MostrarEnSplash(ByVal Mensaje As String)
                If Splash Is Nothing Then Exit Sub

                Splash.EscribirEstado = Mensaje
            End Sub

            Private Sub VentanaMostrada() Handles Splash.VentanaMostrada
                Threading.Thread.Sleep(500)
                _VentanaMostrada = True
            End Sub
#End Region
#Region " RUTINA DE INICIO Y APERTURA DE FORMULARIOS "
            ''' <summary>
            ''' Para poder realizar las UNIT Test
            ''' </summary>
            ''' <remarks></remarks>
            Public Sub inicializarConexion()
                If ContieneArgumento("/CadenaConexion") Then
                    cDAL.Instancia.CadenaConexion = ObtenerValor("/CadenaConexion")
                Else
                    cDAL.Instancia.CadenaConexion = String.Format("Server={0};Database={1};Uid={2};Pwd={3};Port={4}", cConfiguracionBBDD.Instancia.BBDDServidor, cConfiguracionBBDD.Instancia.BBDDNombre, cConfiguracionBBDD.Instancia.BBDDUsuario, cConfiguracionBBDD.Instancia.BBDDPassword, cConfiguracionBBDD.Instancia.BBDDPuerto)
                End If

                If ContieneArgumento("/ProveedorBBDD") Then
                    cDAL.Instancia.Provider = ObtenerValor("/ProveedorBBDD")
                Else
                    cDAL.Instancia.Provider = "MySql.Data.MySqlClient"
                End If
            End Sub

            ''' <summary>
            ''' Punto de entrada de la aplicación
            ''' </summary>
            Public Sub Inicio()
                Try
                    ' Cargo la configuración del programa
                    cConfiguracionAplicacion.Instancia.Cargar(1)
                    cConfiguracionBBDD.Instancia.Cargar(1)

#If DEBUG Then
                    ' Establezco valores para debug
                    'cConfiguracionBBDD.Instancia.BBDDServidor = "192.168.0.110"
                    'cConfiguracionBBDD.Instancia.BBDDUsuario = "root"
                    'cConfiguracionBBDD.Instancia.BBDDPassword = "quadralia"

                    cConfiguracionBBDD.Instancia.BBDDServidor = "127.0.0.1"
                    cConfiguracionBBDD.Instancia.BBDDUsuario = "root2"
                    cConfiguracionBBDD.Instancia.BBDDPassword = "28porrinho28"
                    cConfiguracionBBDD.Instancia.BBDDNombre = "qLotesOns"
#End If
                Catch ex As Exception
                    InformarError(ex)
                End Try

                ' Tengo que crear la pantalla de login aqui para que luego me la muestre correctamente
                Dim aux = frmPrincipal.upActualizador

                'Muestro el formulario de splash en otro hilo para que no me moleste
                HiloSplash = New Thread(New ThreadStart(AddressOf MostrarSplash))
                HiloSplash.Start()

                While Not _VentanaMostrada
                    Threading.Thread.Sleep(100)
                End While

                Splash.EscribirEstado = "Configurando aplicación"
                Dim RutaInformes As String = My.Computer.FileSystem.GetParentPath(Application.ExecutablePath)
                RutaInformes = My.Computer.FileSystem.CombinePath(RutaInformes, "Informes")
                If My.Computer.FileSystem.DirectoryExists(RutaInformes) Then GestorInformes.Init(RutaInformes)

                Splash.EscribirEstado = "Comprobando conexión con Base de datos"
                inicializarConexion()

                 Using contexto = cDAL.Instancia.getContext
                    Try
                        ConexionBBDD = contexto.DatabaseExists
                        Splash.EscribirEstado = "Conexión establecida correctamente"
                    Catch ex As Exception
                        ConexionBBDD = False
                        Splash.EscribirEstado = "No se puede conectar con el servidor especificado"
                    Finally
                        contexto.Connection.Close()
                    End Try
                End Using

                If ConexionBBDD Then
                    Dim Contexto As DAL.Entidades = DAL.cDAL.Instancia.getContext()
                    Dim Configuracion As Configuracion = (From cnf As Configuracion In Contexto.Configuraciones Select cnf).FirstOrDefault
                    Splash.EscribirEstado = "Comprobando actualizaciones"
                    Quadralia.Actualizaciones.ComprobarActualizacionesDescargadas(aux, ObtenerParametroActualizacion())

                    Splash.EscribirEstado = "Sincronizando Datos de Trazamare"

                    SincronizarZonasYSubZonas(Configuracion)

                    Splash.EscribirEstado = "Iniciando proceso de sincronización de etiquetas."
                    If (Configuracion.IntervaloSincronizacion.HasValue) Then
                        qSinc.Sincronizador.Instancia.intervalo = Configuracion.IntervaloSincronizacion.Value
                        qSinc.Sincronizador.Instancia.IniciarTimer()
                    End If
                End If



                ' Cerramos el formulario
                CerrarSplash()

                ' Miro si sólo tengo que realizar copia de la base de datos
                If ContieneArgumento("/CopiaSeguridad") Then
                    Dim Ruta As String = My.Computer.FileSystem.CombinePath(Application.StartupPath, String.Format("{0:yyyyMMddhhmmss}.qbak", DateTime.Now))
                    If (ContieneArgumento("/RutaCS")) Then Ruta = ObtenerValor("/RutaCS")

                    ' Verifico la ruta
                    If Not My.Computer.FileSystem.DirectoryExists(IO.Path.GetDirectoryName(Ruta)) Then End

                    ' Comienzo con el proceso
                    Using Formulario As New frmProgresoCopiaSeguridad()
                        Formulario.Show()
                        Formulario.RealizarCopiaSeguridad(Ruta, False)
                    End Using

                    End

                End If

                Dim Login As New frmLogin
                Select Case Login.ShowDialog
                    Case Windows.Forms.DialogResult.OK
                        If UsuarioConectado Is Nothing Then End
                    Case Else
                        End
                End Select
                Login.Dispose()
            End Sub



            Public Function ObtenerParametroActualizacion() As Dictionary(Of String, Object)
                Dim Resultado As New Dictionary(Of String, Object)

                Resultado.Add("CadenaConexion", cDAL.Instancia.CadenaConexion)
                Resultado.Add("CadenaConexionSQLite", cConfiguracionAplicacion.CadenaConexionUtilizada)
                Resultado.Add("CodigoPrograma", "QLT")

                Return Resultado
            End Function

            ''' <summary>
            ''' Abre un formulario y controla el cierre y activacion
            ''' </summary>
            ''' <param name="FormularioPrincipal">Instancia del formulario principal</param>
            ''' <param name="TipoFormulario">Tipo de formulario que queremos abrir</param>
            ''' <param name="Imagen">Imagen que se utilizará como icono del formulario</param>
            ''' <remarks></remarks>
            Public Function AbrirFormulario(ByVal FormularioPrincipal As frmPrincipal, ByVal TipoFormulario As String, ByVal PermisoRequerido As Permisos, Optional ByVal Imagen As Image = Nothing, Optional Mostrar As Boolean = True, Optional ByVal MaxVentanas As Integer = -1) As Form

                If Not PuedeAccederA(PermisoRequerido) Then
                    MostrarMessageBox("No dispone de permisos suficientes para acceder a esta ventana", "Permisos insuficientes", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Return Nothing
                End If

                TipoFormulario = TipoFormulario
                Dim Formulario As Form = Quadralia.Formularios.AbrirFormularioUnico(FormularioPrincipal, TipoFormulario, Imagen, Formularios.PosicionesFormulario.EsquinaSuperiorIzquierda, MaxVentanas)
                Quadralia.Formularios.AutoTabular(Formulario)
                If cConfiguracionAplicacion.Instancia.VentanasMaximizadas Then Formulario.WindowState = FormWindowState.Maximized
                If Mostrar Then Formulario.Show()
                Return Formulario
            End Function

            ''' <summary>
            ''' Indica si el usuario puede acceder a determinadas partes del sistema
            ''' </summary>
            ''' <param name="Permiso">Nivel que queremos verificar que posee el usuario</param>
            Public Function PuedeAccederA(ByVal Permiso As Permisos) As Boolean
                Return PuedeAccederA(UsuarioConectado, Permiso)
            End Function

            ''' <summary>
            ''' Indica si el usuario puede acceder a determinadas partes del sistema
            ''' </summary>
            ''' <param name="Permiso">Nivel que queremos verificar que posee el usuario</param>
            Public Function PuedeAccederA(ByVal Usuario As Usuario, ByVal Permiso As Permisos) As Boolean
                If (Permiso = Permisos.PUBLICO) Then Return True
                If Permiso = Permisos.USUARIO_AUTENTICADO AndAlso Usuario IsNot Nothing Then Return True

                ' Si no hay conexión, permito a cualquier usuario acceder al panel de configuracion
                If Not ConexionBBDD AndAlso Permiso = Permisos.CONFIGURACION_PROGRAMA Then Return True

                ' Si es administrador local, permito todo
                If Usuario IsNot Nothing AndAlso Usuario.Id = 0 Then Return True

                ' Si no hay usuario, entonces no puedo verificar permisos. DENIEGO
                If Usuario Is Nothing Then Return False

                Return ((Usuario.Permisos And CInt(Permiso)) = CInt(Permiso))
            End Function
#End Region
#Region " ERRORES "
            ''' <summary>
            ''' Muestra un error y escribe en el log
            ''' </summary>
            ''' <param name="Excepcion">Error que se produjo</param>
            ''' <param name="EscribirEnLog">Indica si escribimos el error en el log o no</param>
            ''' <remarks></remarks>
            Public Sub InformarError(ByRef Excepcion As Exception, Optional ByVal Enviar As Boolean = True, Optional ByVal EscribirEnLog As Boolean = False)
                ' InformarError(Excepcion, My.Resources.ErrorEnAplicacion & Environment.NewLine & Excepcion.Message & Environment.NewLine & Environment.NewLine & My.Resources.ErrorEnAplicacion2, Enviar, EscribirEnLog)
                InformarError(Excepcion, My.Resources.ErrorEnAplicacion & Environment.NewLine & Excepcion.Message, Enviar, EscribirEnLog)
            End Sub

            ''' <summary>
            ''' Muestra un error y escribe en el log
            ''' </summary>
            ''' <param name="Excepcion">Error que se produjo</param>
            ''' <param name="Mensaje">Mensaje de error que se le mostrará al usuario</param>
            ''' <param name="EscribirEnLog">Indica si escribimos el error en el log o no</param>
            Public Sub InformarError(ByRef Excepcion As Exception, ByVal Mensaje As String, Optional ByVal Enviar As Boolean = True, Optional ByVal EscribirEnLog As Boolean = False)
#If DEBUG Then
                Dim CuadroMensaje As New KryptonTaskDialog()

                CuadroMensaje.WindowTitle = My.Resources.ErrorEnAplicacion
                CuadroMensaje.Content = Mensaje

                CuadroMensaje.CheckboxText = "Detener Depurador para inspeccionar error"
                CuadroMensaje.CheckboxState = True
                CuadroMensaje.Icon = MessageBoxIcon.Error
                CuadroMensaje.CommonButtons = TaskDialogButtons.OK
                CuadroMensaje.AllowDialogClose = True

                CuadroMensaje.ShowDialog()
                If CuadroMensaje.CheckboxState Then Debugger.Break()
#Else
                If Enviar Then
                    Dim Formulario As New frmExcepcion(Excepcion, Enviar)
                    If Formulario.ShowDialog() = DialogResult.OK Then
                        If UsuarioConectado IsNot Nothing Then Formulario.InfoError.Personalizado2 = UsuarioConectado.usuario
                        Quadralia.Errores.EnviarError(Excepcion, cConfiguracionAplicacion.Instancia.ServidorErrores, "QLT", Formulario.InfoError)
                    End If
                Else
                    Dim CuadroMensaje As New KryptonTaskDialog()

                    CuadroMensaje.WindowTitle = My.Resources.ErrorEnAplicacion
                    CuadroMensaje.Content = Mensaje
                    CuadroMensaje.Icon = MessageBoxIcon.Error
                    CuadroMensaje.CommonButtons = TaskDialogButtons.OK
                    CuadroMensaje.AllowDialogClose = True

                    CuadroMensaje.ShowDialog()
                End If

                'miro si tengo que escribir en el log
                If EscribirEnLog Then
                    'Genero la informacion del error para escribirla en el log
                    Dim InformacionError As New System.Text.StringBuilder()
                    Dim FinLinea As Integer = Excepcion.StackTrace.IndexOf(Environment.NewLine)

                    With InformacionError
                        .AppendLine(String.Format("Dia: {0}   Hora: {1}", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString()))
                        .AppendLine(String.Format("Error devuelto por el sistema: {0}", Excepcion.Message))
                        .AppendLine(String.Format("Información: {0}", Excepcion.StackTrace.Substring(0, FinLinea)))
                        .AppendLine()
                    End With

                    'Escribo en el fichero elg=EscritorioLog
                    My.Computer.FileSystem.WriteAllText("Log.elg", InformacionError.ToString(), True, System.Text.Encoding.UTF8)
                End If
#End If
            End Sub
#End Region
#Region " AREA DE NOTIFICACION "
            ''' <summary>
            ''' Muestar un mensaje en el icono de notificación
            ''' </summary>
            ''' <param name="Mensaje">Mensaje de la ventana</param>
            ''' <param name="Titulo">Titulo de la ventana</param>
            ''' <param name="Icono">Icono de la ventana</param>
            ''' <remarks></remarks>
            Public Sub MostrarMensajeNtfIco(ByVal Titulo As String, ByVal Mensaje As String, ByVal Icono As ToolTipIcon)
                RemoveHandler frmPrincipal.tmrReloj.Tick, AddressOf OcultarBallon
                AddHandler frmPrincipal.tmrReloj.Tick, AddressOf OcultarBallon

                frmPrincipal.ntfIcono.ShowBalloonTip(2500, Titulo, Mensaje, Icono)
                frmPrincipal.tmrReloj.Start()
            End Sub

            ''' <summary>
            ''' Fuerza que se cierre el ballon del icono de notificación
            ''' </summary>
            Public Sub OcultarBallon(ByVal sender As Object, ByVal e As EventArgs)
                frmPrincipal.tmrReloj.Stop()
                frmPrincipal.ntfIcono.Visible = False
                frmPrincipal.ntfIcono.Visible = True
            End Sub
#End Region
#Region " RESTO DE FUNCIONES "
            ''' <summary>
            ''' Muestra un cuadro de diálogo con los parámetros establecidos
            ''' </summary>
            Public Function MostrarMessageBox(ByVal texto As String, Optional ByVal titulo As String = Nothing, Optional ByVal botones As MessageBoxButtons = Nothing, Optional ByVal icono As MessageBoxIcon = Nothing, Optional ByVal defecto As MessageBoxDefaultButton = Nothing, Optional ByVal opciones As MessageBoxOptions = Nothing) As DialogResult
                ' Añado una nueva linea y un espacio en blanco para subsanar el error de que aparezcan los mensajes cortados
                texto &= Environment.NewLine & " "

                Return KryptonMessageBox.Show(texto, titulo, botones, icono, defecto, opciones)
            End Function

            Public Function ContieneArgumento(ByVal Argumento As String) As Boolean
                For Each Param As String In My.Application.CommandLineArgs
                    If Param.StartsWith(Argumento, StringComparison.OrdinalIgnoreCase) Then Return True
                Next

                Return False
            End Function

            Public Function ObtenerValor(ByVal Argumento As String) As String
                If Not ContieneArgumento(Argumento) Then Return String.Empty

                Dim Resultado As String = String.Empty
                For Each Param As String In My.Application.CommandLineArgs
                    If Param.StartsWith(Argumento, StringComparison.OrdinalIgnoreCase) Then
                        Resultado = Param.Remove(0, Argumento.Length)
                        If Resultado.StartsWith("=", StringComparison.OrdinalIgnoreCase) Then Resultado = Resultado.Remove(0, 1)
                        If Resultado.StartsWith("""", StringComparison.OrdinalIgnoreCase) Then Resultado = Resultado.Remove(0, 1)
                        If Resultado.EndsWith("""", StringComparison.OrdinalIgnoreCase) Then Resultado = Resultado.Remove(Resultado.Length - 1)
                    End If
                Next

                Return Resultado
            End Function

            ''' <summary>
            ''' Refresca las comisiones de los agentes
            ''' </summary>
            ''' <remarks></remarks>
            Public Sub RefrescarDGVOrdenado(Of T)(ByRef sender As DataGridView, ByRef Datos As List(Of T))
                Dim ColumnaOrdenacion As DataGridViewColumn = (From col As DataGridViewColumn In sender.Columns Where col.HeaderCell.SortGlyphDirection <> SortOrder.None Select col).FirstOrDefault
                Dim IndiceColumna As Integer = -1
                Dim Ordenacion As SortOrder = SortOrder.None

                If ColumnaOrdenacion IsNot Nothing Then
                    IndiceColumna = ColumnaOrdenacion.Index
                    Ordenacion = ColumnaOrdenacion.HeaderCell.SortGlyphDirection
                End If

                sender.DataSource = Nothing
                sender.Rows.Clear()

                ' Validaciones
                If Datos Is Nothing Then Exit Sub

                sender.DataSource = Datos
                sender.ClearSelection()
                If IndiceColumna > -1 Then Quadralia.Formularios.Funcionalidad.OrdenarDGV(Of T)(sender, IndiceColumna, Ordenacion)
            End Sub
#End Region
#Region " INFORMES "

            Public Function AbrirInforme(ByVal FormularioPrincipal As frmPrincipal, ByVal TipoInforme As frmVisorInforme.Informe, Optional ByVal Permiso As Permisos = Permisos.INFORMES, Optional ByVal Imagen As Image = Nothing, Optional Mostrar As Boolean = True) As Form
                Try
                    If Permiso = Nothing Then Permiso = Permisos.INFORMES
                    Dim VentanaInforme As frmVisorInforme = AbrirFormulario(FormularioPrincipal, "Escritorio.frmVisorInforme", Permiso, Imagen, Mostrar, -1)
                    VentanaInforme.TipoInforme = TipoInforme

                    Return VentanaInforme
                Catch ex As Exception
                    InformarError(ex)
                    Return Nothing
                End Try
            End Function
            Public Function AbrirInformeParametros(ByVal FormularioPrincipal As frmPrincipal, ByVal TipoInforme As frmVisorInformeParametros.Informe, ByVal NombreInforme As String, Optional ByVal Permiso As Permisos = Permisos.INFORMES, Optional ByVal Imagen As Image = Nothing) As Form
                Try
                    If Permiso = Nothing Then Permiso = Permisos.INFORMES
                    Dim VentanaInforme As frmVisorInformeParametros = AbrirFormulario(FormularioPrincipal, "Escritorio.frmVisorInformeParametros", Permiso, Imagen, -1)
                    VentanaInforme.TipoInforme = TipoInforme

                    If Not String.IsNullOrEmpty(NombreInforme.Trim) Then
                        VentanaInforme.lblTitulo.Text = NombreInforme.Trim
                        VentanaInforme.Text &= String.Format(" [{0}]", NombreInforme.Trim)
                    End If

                    If Imagen IsNot Nothing Then VentanaInforme.lblTitulo.Values.Image = Imagen

                    Return VentanaInforme
                Catch ex As Exception
                    InformarError(ex)
                    Return Nothing
                End Try
            End Function
#End Region
#Region " SINCRONIZACION "
            ''' <summary>
            ''' Sincroniza las etiquetas con el servidor deseado
            ''' </summary>
            Public Function SincronizarEtiquetas(Optional ByVal pSincronizarEtiquetasPaquetes As Boolean = False) As Boolean
                If Not ConexionBBDD Then Return False

                Dim Formulario As frmProgresoSincronizacion = Quadralia.Formularios.AbrirFormularioUnico(frmPrincipal, "Escritorio.frmProgresoSincronizacion", Nothing, Formularios.PosicionesFormulario.EsquinaSuperiorIzquierda, 1)
                Quadralia.Formularios.AutoTabular(Formulario)
                Formulario.Show()

                If qSinc.Sincronizador.Instancia.Ejecutando Then Return True

                Try
                    Dim Contexto As DAL.Entidades = DAL.cDAL.Instancia.getContext()
                    Dim Configuracion As Configuracion = (From cnf As Configuracion In Contexto.Configuraciones Select cnf).FirstOrDefault

                    ' Validaciones
                    If Configuracion Is Nothing Then Return False

                    Dim DatosBBDD = New qSinc.Sincronizador.ConexionBBDD
                    With DatosBBDD
                        .Puerto = Configuracion.TrazamareBBDDPuerto
                        .BaseDatos = Configuracion.TrazamareBBDDBaseDatos
                        .Clave = Configuracion.TrazamareBBDDClave
                        .Servidor = Configuracion.TrazamareBBDDServidor
                        .Timeout = Configuracion.TrazamareBBDDTimeOut
                        .Usuario = Configuracion.TrazamareBBDDUsuario
                    End With

                    Dim DatosFTP = New qSinc.Sincronizador.ConexionFTP
                    With DatosFTP
                        .Puerto = Configuracion.TrazamareFTPPuerto
                        .Clave = Configuracion.TrazamareFTPClave
                        .Servidor = Configuracion.TrazamareFTPServidor
                        .Usuario = Configuracion.TrazamareFTPUsuario
                        .RutaCarpeta = Configuracion.TrazamareFTPServidor
                    End With

                    Dim DatosTrazamare = New qSinc.Sincronizador.ServidorTrazamare
                    With DatosTrazamare
                        .Servidor = Configuracion.TrazamareFTPServidor
                        .Ruta = Configuracion.TrazamareFTPRuta
                        .Clave = Configuracion.TrazamareFTPClave
                    End With

                    qSinc.Sincronizador.Instancia.DatosConexionBBDD = DatosBBDD
                    qSinc.Sincronizador.Instancia.DatosConexionFTP = DatosFTP
                    qSinc.Sincronizador.Instancia.DatosConexionTrazamare = DatosTrazamare

                    Dim Artes As List(Of MetodoProduccion) = (From art As MetodoProduccion In Contexto.MetodosProduccion Where Not art.sincronizadoWeb Select art).ToList
                    Dim Barcos As List(Of Barco) = (From bar As Barco In Contexto.Barcos Where Not bar.sincronizadoWeb Select bar).ToList
                    Dim Especies As List(Of Especie) = (From esp As Especie In Contexto.Especies Where Not esp.sincronizadoWeb Select esp).ToList
                    Dim Empaquetados As List(Of Empaquetado) = (From emp As Empaquetado In Contexto.Empaquetados
                                                                Where Not emp.sincronizadoWeb And emp.fechaBaja Is Nothing Select emp).ToList

                    Dim Etiquetas As List(Of Etiqueta) = Nothing
                    ''si pSincronizarSoloPaquetes se cogen etiquetas de los paquetes no las no sincronizadas                    
                    If (pSincronizarEtiquetasPaquetes) Then
                        Dim idsEmpaquetados As Int64() = (From em In Empaquetados Select em.id).ToArray()
                        If (idsEmpaquetados IsNot Nothing AndAlso idsEmpaquetados.Count() > 0) Then
                            Etiquetas = (From etq As Etiqueta In Contexto.Etiquetas Where idsEmpaquetados.Contains(etq.Empaquetado.id) AndAlso
                                          etq.fechaBaja Is Nothing Select etq).ToList()
                        End If
                    Else
                        Etiquetas = (From etq As Etiqueta In Contexto.Etiquetas Where Not etq.sincronizadoWeb AndAlso etq.fechaBaja Is Nothing Select etq).ToList
                    End If

                    'Create a generic Objects Array
                    Dim param_obj(5) As Object
                    param_obj(0) = Etiquetas
                    param_obj(1) = Barcos
                    param_obj(2) = Especies
                    param_obj(3) = Artes
                    param_obj(4) = Empaquetados
                    param_obj(5) = Contexto

                    Dim worker_thread As New Thread(Sub() aux(param_obj, pSincronizarEtiquetasPaquetes))
                    worker_thread.Start()

                    Return True
                Catch ex As Exception
                    MessageBox.Show(ex.Message + ex.InnerException.ToString())
                    Return False
                End Try
            End Function

            ''' <summary>
            ''' 
            ''' </summary>
            ''' <param name="params"></param>
            ''' <param name="pSincronizarEtiquetasPaquetes"></param>
            Private Sub aux(params As Object, Optional ByVal pSincronizarEtiquetasPaquetes As Boolean = False)

                qSinc.Sincronizador.Instancia.Sincronizar(params(0)) ', params(1), params(2), params(3), params(4), pSincronizarEtiquetasPaquetes)

                Dim Contexto As Entidades = params(5)
                Contexto.SaveChanges()
            End Sub
#End Region

#Region " SINCRONIZACION ONS"
            Private Sub SincronizarZonasYSubZonas(configuracion As Configuracion)
                Try
                    Dim Contexto As DAL.Entidades = DAL.cDAL.Instancia.getContext()


                    ' Validaciones
                    If configuracion Is Nothing Then Exit Sub

                    Dim DatosTrazamare = New qSinc.Sincronizador.ServidorTrazamare
                    With DatosTrazamare
                        .Servidor = configuracion.TrazamareFTPServidor
                        .Ruta = configuracion.TrazamareFTPRuta
                        .Clave = configuracion.TrazamareFTPClave
                    End With

                    qSinc.Sincronizador.Instancia.DatosConexionTrazamare = DatosTrazamare
                    SincronizarZonasSubZonas(Contexto)

                Catch ex As Exception

                End Try


            End Sub

            Public Sub SincronizarZonasSubZonas(ByRef contexto As Entidades)
                Dim zonasTraza As List(Of ZonaFao) = qSinc.Sincronizador.Instancia.ObtenerZonas()
                For Each zon In zonasTraza
                    Dim zAux = contexto.ZonasFao.FirstOrDefault(Function(x) x.idTrazamare = zon.idTrazamare)
                    If zAux Is Nothing Then
                        contexto.ZonasFao.AddObject(zon)
                    End If
                Next

                Dim subZonasTraza As List(Of SubZona) = qSinc.Sincronizador.Instancia.ObtenerSubZonas()
                For Each subz In subZonasTraza
                    Dim zAux = contexto.SubZonas.FirstOrDefault(Function(x) x.idTrazamare = subz.idTrazamare)
                    If zAux Is Nothing Then
                        contexto.SubZonas.AddObject(subz)
                    End If
                Next

                contexto.SaveChanges()
            End Sub

            ''' <summary>
            ''' Sincroniza las etiquetas con el servidor deseado
            ''' </summary>
            Public Function SincronizarEtiquetasOns(Optional ByVal pSincronizarEtiquetasPaquetes As Boolean = False) As Boolean
                If Not ConexionBBDD Then Return False

                Dim Formulario As frmProgresoSincronizacion = Quadralia.Formularios.AbrirFormularioUnico(frmPrincipal, "Escritorio.frmProgresoSincronizacion", Nothing, Formularios.PosicionesFormulario.EsquinaSuperiorIzquierda, 1)
                Quadralia.Formularios.AutoTabular(Formulario)
                Formulario.Show()

                If qSinc.Sincronizador.Instancia.Ejecutando Then Return True

                Try
                    Dim Contexto As DAL.Entidades = DAL.cDAL.Instancia.getContext()
                    Dim Configuracion As Configuracion = (From cnf As Configuracion In Contexto.Configuraciones Select cnf).FirstOrDefault

                    ' Validaciones
                    If Configuracion Is Nothing Then Return False

                    Dim DatosTrazamare = New qSinc.Sincronizador.ServidorTrazamare
                    With DatosTrazamare
                        .Servidor = Configuracion.TrazamareFTPServidor
                        .Ruta = Configuracion.TrazamareFTPRuta
                        .Clave = Configuracion.TrazamareFTPClave
                    End With

                    qSinc.Sincronizador.Instancia.DatosConexionTrazamare = DatosTrazamare

                    Dim Etiquetas As List(Of Etiqueta) = Nothing
                    ''si pSincronizarSoloPaquetes se cogen etiquetas de los paquetes no las no sincronizadas                    

                    Etiquetas = (From etq As Etiqueta In Contexto.Etiquetas Where Not etq.sincronizadoWeb AndAlso etq.fechaBaja Is Nothing Select etq).ToList


                    'Create a generic Objects Array
                    Dim param_obj(2) As Object
                    param_obj(0) = Etiquetas
                    param_obj(1) = Contexto
                    param_obj(2) = Configuracion.CodigoTrazamare
                    Dim worker_thread As New Thread(Sub() auxOns(param_obj))
                    worker_thread.Start()

                    Return True
                Catch ex As Exception
                    MessageBox.Show(ex.Message + ex.InnerException.ToString())
                    Return False
                End Try
            End Function

            ''' <summary>
            ''' 
            ''' </summary>
            ''' <param name="params"></param>
            ''' <param name="pSincronizarEtiquetasPaquetes"></param>
            Private Sub auxOns(params As Object, Optional ByVal pSincronizarEtiquetasPaquetes As Boolean = False)

                qSinc.Sincronizador.Instancia.SincronizarOns(params(2), params(0)) ', params(1), params(2), params(3), params(4), pSincronizarEtiquetasPaquetes)

                Dim Contexto As Entidades = params(1)
                Contexto.SaveChanges()
            End Sub




#End Region
        End Module
    End Namespace
End Namespace
