﻿Public Class frmDebug
    Private _Contexto As Entidades = Nothing
    Private WithEvents Listener As New qBasc.ListenerBascula()

    ''' <summary>
    ''' Contexto que utilizará el formulario
    ''' </summary>
    Public Property Contexto As Entidades
        Get
            If _Contexto Is Nothing Then
                _Contexto = cDAL.Instancia.getContext()
                _Contexto.ContextOptions.LazyLoadingEnabled = True
            End If
            Return _Contexto
        End Get
        Set(ByVal value As Entidades)
            _Contexto = value
        End Set
    End Property

    Private Sub Empezar(sender As Object, e As EventArgs) Handles btnEmpezar.Click
        Listener.ComenzarEscucha()
    End Sub

    Private Sub frmDebug_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Listener.DireccionIP = "192.168.0.210"
        Listener.Puerto = 1000
        Listener.PesoMinimo = 0.05

        CheckForIllegalCrossThreadCalls = False
    End Sub

    Private Sub Listener_LecturaPeso1(sender As Object, Lectura As qBasc.RespuestaBascula) Handles Listener.LecturaPeso
        txtRespuesta.Text = Lectura.Peso
    End Sub

    Private Sub btnDetener_Click(sender As Object, e As EventArgs) Handles btnDetener.Click
        Dim formulario As New frmPesadoAutomatico
        formulario.Show()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim DatosBBDD = New qSinc.Sincronizador.ConexionBBDD
        With DatosBBDD
            .Puerto = 3306
            .BaseDatos = "trazamareae"
            .Clave = "Q.428porrinho28"
            .Servidor = "ovh05.quadralia.com"
            .Timeout = 30
            .Usuario = "root"
        End With

        Dim DatosFTP = New qSinc.Sincronizador.ConexionFTP
        With DatosFTP
            .Puerto = 21
            .Clave = "quadralia"
            .Servidor = "ovh05.quadralia.com"
            .Usuario = "root"
            .RutaCarpeta = "/"
        End With

        qSinc.Sincronizador.Instancia.DatosConexionBBDD = DatosBBDD
        qSinc.Sincronizador.Instancia.DatosConexionFTP = DatosFTP

        Dim Etiquetas As List(Of Etiqueta) = (From etq As Etiqueta In Contexto.Etiquetas Where Not etq.sincronizadoWeb Select etq).ToList
        Dim Artes As List(Of MetodoProduccion) = (From art As MetodoProduccion In Contexto.MetodosProduccion Where Not art.sincronizadoWeb Select art).ToList
        Dim Barcos As List(Of Barco) = (From bar As Barco In Contexto.Barcos Where Not bar.sincronizadoWeb Select bar).ToList
        Dim Especies As List(Of Especie) = (From esp As Especie In Contexto.Especies Where Not esp.sincronizadoWeb Select esp).ToList
        Dim Empaquetados As List(Of Empaquetado) = (From emp As Empaquetado In Contexto.Empaquetados Where Not emp.sincronizadoWeb Select emp).ToList

        qSinc.Sincronizador.Instancia.Sincronizar(Etiquetas, Barcos, Especies, Artes, Empaquetados)
    End Sub
End Class