﻿Public Class frmContacto
    Implements IFormularioItem

    Dim _Contacto As IContacto = Nothing
#Region " PROPIEDADES "
    ''' <summary>
    ''' Contacto con los datos establecidos por el usuario
    ''' </summary>
    Public Property Contacto() As IContacto
        Get
            Return _Contacto
        End Get
        Set(ByVal value As IContacto)
            _Contacto = value
            CargarContacto(_Contacto)
        End Set
    End Property

    ''' <summary>
    ''' Llamada genérica para la interfaz
    ''' </summary>
    Public Property Item As Object Implements IFormularioItem.Item
        Get
            Return _Contacto
        End Get
        Set(ByVal value As Object)
            If value Is Nothing OrElse TypeOf (value) Is IContacto Then
                Contacto = value
            End If
        End Set
    End Property

    Public Property Items As IEnumerable Implements IFormularioItem.Items
        Get
            Dim aux As New ArrayList

            If Item IsNot Nothing Then aux.Add(Item)

            Return aux
        End Get
        Set(ByVal value As IEnumerable)
            If value Is Nothing Then
                Item = Nothing
            Else
                Item = value(0)
            End If
        End Set
    End Property
#End Region
#Region " LIMPIEZA "
    ''' <summary>
    ''' Limpia todos los controles
    ''' </summary>
    Private Sub Limpiar()
        txtNombre.Clear()
        txtApellidos.Clear()
        txtCargo.Clear()
        txtTelefono.Clear()
        nudExtension.Value = 0
        txtMovil.Clear()
        txtFax.Clear()
        txtEmail.Clear()
        txtNotas.Clear()

        epErrores.Clear()
    End Sub
#End Region
#Region " CARGAR / GUARDAR / CANCELAR "
    ''' <summary>
    ''' Carga el contacto en el formulario
    ''' </summary>
    Private Sub CargarContacto(ByVal Contacto As IContacto)
        Limpiar()

        With Contacto
            txtNombre.Text = .Nombre
            txtApellidos.Text = .Apellidos
            txtCargo.Text = .Cargo
            txtTelefono.Text = .Telefono
            nudExtension.Value = .Extension
            txtEmail.Text = .Email
            txtMovil.Text = .Movil
            txtFax.Text = .Fax
            txtNotas.Text = .Observaciones
        End With
    End Sub

    ''' <summary>
    ''' Guarda los datos y sale del formulario
    ''' </summary>
    Private Sub GuardarYSalir(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If Guardar() Then
            Me.DialogResult = Windows.Forms.DialogResult.OK
            Me.Close()
        End If
    End Sub

    Public Function Guardar(Optional ByVal MostrarMensaje As Boolean = True) As Boolean
        ' Valido los datos
        Me.ValidateChildren(ValidationConstraints.Enabled + ValidationConstraints.Visible)

        ' Si hay errores, salgo de la función
        If epErrores.HasErrors Then
            If MostrarMensaje Then Quadralia.Aplicacion.MostrarMessageBox("Corrija los errores antes de continuar", "Errores Encontrados", MessageBoxButtons.OK, MessageBoxIcon.Error)
            epErrores.Controles(0).Focus()
            Return False
        End If

        With _Contacto
            .Nombre = txtNombre.Text
            .Apellidos = txtApellidos.Text
            .Cargo = txtCargo.Text
            .Telefono = txtTelefono.Text
            .Extension = Decimal.ToUInt16(nudExtension.Value)
            .Email = txtEmail.Text
            .Movil = txtMovil.Text
            .Fax = txtFax.Text
            .Observaciones = txtNotas.Text
        End With

        Return True
    End Function

    ''' <summary>
    ''' El usuario indica que no desea crear un nuevo contacto
    ''' </summary>
    Private Sub Cancelar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        _Contacto = Nothing
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub
#End Region
#Region " RUTINA DE INICIO "
    Public Sub New()
        ' Llamada necesaria para el Diseñador de Windows Forms.
        InitializeComponent()

        ' Limpio los controles
        Limpiar()

        For Each UnControl As Control In tblOrganizador.Controls
            Quadralia.Formularios.Funcionalidad.ManejadorTabPorIntro(UnControl)
        Next
    End Sub
#End Region
#Region " VALIDACIONES "
    ''' <summary>
    ''' Obliga al usuario a introducir datos numéricos en el control
    ''' </summary>
    Private Sub SoloNumeros(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtMovil.KeyPress, txtTelefono.KeyPress, txtFax.KeyPress
        ' Quadralia..Validaciones.SoloNumeros(sender, e)
    End Sub

    ''' <summary>
    ''' Valida los correos electronicos
    ''' </summary>
    Private Sub ValidarEmail(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtEmail.Validating
        'If Not String.IsNullOrEmpty(txtEmail.Text) AndAlso Not Utilidades.Validaciones.ValidarEMail(txtEmail.Text) Then
        '    epErrores.SetError(sender, "El email introducido no es válido", "Email Incorrecto")
        'Else
        '    epErrores.SetError(sender, String.Empty)
        'End If
    End Sub
#End Region
End Class