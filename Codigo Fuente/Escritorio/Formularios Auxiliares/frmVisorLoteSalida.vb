﻿Public Class frmVisorLoteSalida
    Public Property Registro As LoteEntradalinea
    Public Property FormularioPrincipal As frmPrincipal

#Region " LIMPIEZA "
    Private Sub LimpiarDatos()
        dgvLotesSalida.DataSource = Nothing
        dgvLotesSalida.Rows.Clear()
    End Sub
#End Region
#Region " CARGAR / GUARDAR / CANCELAR "
    Private Sub cargarRegistros()
        LimpiarDatos()

        If Quadralia.Aplicacion.UsuarioConectado Is Nothing Then Exit Sub
        If Not Quadralia.Aplicacion.PuedeAccederA(Quadralia.Aplicacion.Permisos.LOTES_SALIDA) Then Exit Sub
        If Registro Is Nothing Then Exit Sub
        If Registro.LotesSalida.Count <= 0 Then Exit Sub

        ' Ordeno los registros
        dgvLotesSalida.DataSource = Me.Registro.LotesSalida.ToList()
        If dgvLotesSalida.RowCount = 1 Then
            dgvLotesSalida.Rows(0).Selected = True
            btnAceptar.PerformClick()
        End If
    End Sub

    ''' <summary>
    ''' Añade la lista de elementos seleccionados y los devuelve
    ''' </summary>
    Private Sub Aceptar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If dgvLotesSalida.SelectedRows.Count <> 1 Then Exit Sub
        If dgvLotesSalida.SelectedRows(0).DataBoundItem Is Nothing Then Exit Sub

        ' Abro el formulario
        Dim Formulario As frmLoteSalida = Quadralia.Aplicacion.AbrirFormulario(FormularioPrincipal, "Escritorio.frmLoteSalida", Quadralia.Aplicacion.Permisos.LOTES_SALIDA, My.Resources.Salida_16, True)
        Formulario.IdRegistro = DirectCast(dgvLotesSalida.SelectedRows(0).DataBoundItem, LoteSalida).id
        If Me.Registro IsNot Nothing Then Formulario.MarcarLineaEntrada(Me.Registro.id)

        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    ''' <summary>
    ''' El usuario indica que no desea crear un nuevo contacto
    ''' </summary>
    Private Sub Cancelar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click        
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub
#End Region
#Region " RUTINA DE INICIO "
    Public Sub Inicio() Handles Me.Load
        ' AutoTabulacion
        Quadralia.Formularios.AutoTabular(Me)

        ' No quiero nuevas columnas
        dgvLotesSalida.AutoGenerateColumns = False

        ' Limpio los controles
        LimpiarDatos()

        ' Carga los artículos        
        cargarRegistros()
    End Sub
#End Region
#Region " MANEJO DE CONTROLES "
    ''' <summary>
    ''' Ordena los resultados del DGV de Resultados    
    ''' </summary>
    Private Sub OrdenarResultados(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvLotesSalida.ColumnHeaderMouseClick
        Quadralia.Formularios.Funcionalidad.OrdenarDGV(Of LoteSalida)(dgvLotesSalida, e)
    End Sub

    Private Sub DobleClickItem(ByVal sender As Object, ByVal e As DataGridViewCellEventArgs) Handles dgvLotesSalida.CellDoubleClick
        If e.RowIndex > -1 Then btnAceptar.PerformClick()
    End Sub

    ''' <summary>
    ''' Controlamos el enter para que vaya al botón de aceptar
    ''' </summary>
    Private Sub ControlEnter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvLotesSalida.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.Handled = True
            Aceptar(Nothing, Nothing)
        ElseIf e.KeyCode = Keys.Tab Then
            e.Handled = True
            Me.SelectNextControl(sender, True, True, True, True)
        End If
    End Sub
#End Region
#Region " RESTO DE FUNCIONES "
    ''' <summary>
    ''' Evita que salga el molesto error del DGV
    ''' </summary>
    Private Sub EvitarErrores(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvLotesSalida.DataError
        e.Cancel = True
    End Sub
#End Region
End Class