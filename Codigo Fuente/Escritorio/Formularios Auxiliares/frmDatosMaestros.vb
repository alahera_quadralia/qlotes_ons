﻿Imports Escritorio.Quadralia.BackUpBBDD
Imports System.Data.Objects

Public Class frmDatosMaestros
    Inherits FormularioHijo

#Region " DECLARACIONES "
    Private _Estado As Quadralia.Enumerados.EstadoFormulario = Quadralia.Enumerados.EstadoFormulario.Espera
    Private _Datos As New Dictionary(Of Integer, Object)
    Private _Contexto As Entidades = Nothing
    Private _UltimoIndexSeleccionado As Integer = -1
#End Region
#Region " PROPIEDADES "
    Public Property Contexto As Entidades
        Get
            If _Contexto Is Nothing Then _Contexto = cDAL.Instancia.getContext()
            Return _Contexto
        End Get
        Set(ByVal value As Entidades)
            _Contexto = value
        End Set
    End Property
#End Region
#Region " DECLARACIONES NECESARIAS DE FORMULARIO HIJO "
    Public Overrides ReadOnly Property Tab As ComponentFactory.Krypton.Ribbon.KryptonRibbonTab
        Get
            Return frmPrincipal.tabConfiguracion
        End Get
    End Property
#End Region
#Region " CONTROL DE BOTONES "
    ''' <summary>
    ''' Controla todos los boton
    ''' </summary>
    Private Sub ControlarBotones() Handles dgvDatos.SelectionChanged
        hdrDatos.HeaderVisiblePrimary = cboTabla.SelectedItem IsNot Nothing AndAlso TypeOf (cboTabla.SelectedItem) Is IImagen

        btnAsignarImagen.Enabled = IIf(dgvDatos.CurrentRow IsNot Nothing AndAlso Not dgvDatos.CurrentRow.IsNewRow AndAlso dgvDatos.CurrentRow.DataBoundItem IsNot Nothing AndAlso TypeOf (dgvDatos.CurrentRow.DataBoundItem) Is IImagen, ButtonEnabled.True, ButtonEnabled.False)
        btnEliminarImagen.Enabled = IIf((btnAsignarImagen.Enabled = ButtonEnabled.True) AndAlso DirectCast(dgvDatos.CurrentRow.DataBoundItem, IImagen).TieneImagen, ButtonEnabled.True, ButtonEnabled.False)
        btnVerImagen.Enabled = btnEliminarImagen.Enabled
    End Sub
#End Region
#Region " LIMPIEZA "
    Private Sub LimpiarTodo()
        LimpiarCombo()
        LimpiarDatos()
    End Sub

    Private Sub LimpiarCombo()
        cboTabla.DataSource = Nothing
        cboTabla.Items.Clear()

        cboTabla.DisplayMember = "NombreEntidad"
        cboTabla.ValueMember = "NombreEntidad"

        _UltimoIndexSeleccionado = -1
    End Sub

    ''' <summary>
    ''' Limpia todos los controles con datos del formulario
    ''' </summary>
    Private Sub LimpiarDatos()
        dgvDatos.DataSource = Nothing

        dgvDatos.Rows.Clear()
        dgvDatos.Columns.Clear()
    End Sub
#End Region
#Region " RUTINA DE INICIO / FIN "
    ''' <summary>
    ''' Rutina de inicio del formulario
    ''' </summary>
    Private Sub Inicio(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        dgvDatos.AutoGenerateColumns = False
        CargarDatosMaestros()
    End Sub

    Private Sub CargarDatosMaestros()
        LimpiarCombo()

        With cboTabla.Items
            .Add(New Barco)
            .Add(New Especie)
            .Add(New Extraccion)
            .Add(New Presentacion)
            .Add(New MetodoProduccion)
            .Add(New ZonaFao)
            .Add(New SubZona)
        End With

        cboTabla.SelectedIndex = 0
    End Sub
#End Region
#Region " CARGAR / GUARDAR / CANCELAR "
    ''' <summary>
    ''' Carga la configuración de BBDD en el formulario
    ''' </summary>
    Private Sub Cargar() Handles cboTabla.SelectedIndexChanged
        ' Guardo los datos actuales
        If _UltimoIndexSeleccionado > -1 Then
            _Datos(_UltimoIndexSeleccionado) = dgvDatos.DataSource
        End If

        LimpiarDatos()

        _UltimoIndexSeleccionado = cboTabla.SelectedIndex
        If cboTabla.SelectedIndex = -1 Then Exit Sub

        If TypeOf (cboTabla.SelectedItem) Is IDatoMaestro Then
            ' Cargo las columnas
            With DirectCast(cboTabla.SelectedItem, IDatoMaestro)
                ' Añado la columna de Id
                dgvDatos.Columns.Add(New KryptonDataGridViewTextBoxColumn With {.DataPropertyName = "id", .Visible = False})

                ' Miro si tengo que añadir la columna de imagen
                If TypeOf (cboTabla.SelectedItem) Is IImagen Then
                    dgvDatos.Columns.Add(New DataGridViewCheckBoxColumn With {.DataPropertyName = DirectCast(cboTabla.SelectedItem, IImagen).NombreCampoTieneImagen, .AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells, .HeaderText = "Img."})
                End If

                For Each Columna In .Columnas
                    Dim col As New KryptonDataGridViewTextBoxColumn With {.DataPropertyName = Columna.Propiedad, .AutoSizeMode = Columna.Resize, .HeaderText = Columna.Nombre}
                    col.DefaultCellStyle.NullValue = " "
                    dgvDatos.Columns.Add(col)
                Next

                ' Añado columna de predeterminado
                dgvDatos.Columns.Add(New DataGridViewCheckBoxColumn With {.DataPropertyName = "predeterminada", .AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells, .HeaderText = "Pred."})

                If _Datos.ContainsKey(cboTabla.SelectedIndex) Then
                    dgvDatos.DataSource = _Datos(cboTabla.SelectedIndex)
                Else
                    dgvDatos.DataSource = .ObtenerDatos(Me.Contexto)
                End If
            End With
        End If

        ControlarBotones()
    End Sub

    ''' <summary>
    ''' Guarda los cambios del formulario en BBDD
    ''' </summary>
    Public Overrides Function Guardar(Optional ByVal MostrarMensaje As Boolean = True) As Boolean
        ' Añados los datos actuales
        Try
            dgvDatos.EndEdit()
            Contexto.SaveChanges()
            If MostrarMensaje Then Quadralia.Aplicacion.MostrarMensajeNtfIco(My.Resources.TituloDatosGuardatos, My.Resources.DatosGuardados, ToolTipIcon.Info)

            Me.Close()
            Return True
        Catch ex As Exception
            Quadralia.Aplicacion.InformarError(ex, False, True)
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Cancela los cambios realizados en el formulario
    ''' </summary>
    Public Overrides Sub Cancelar()
        Me.Close()
    End Sub
#End Region
#Region " RESTO DE FUNCIONES "
    Private Sub PropagarCambiosCheck(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvDatos.CurrentCellDirtyStateChanged
        If dgvDatos.CurrentCell.RowIndex > -1 AndAlso dgvDatos.CurrentCell.ColumnIndex = dgvDatos.ColumnCount - 1 AndAlso dgvDatos.IsCurrentCellDirty Then
            dgvDatos.CommitEdit(DataGridViewDataErrorContexts.Commit)
        End If
    End Sub

    Private Sub dgvDatos_CellValueChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDatos.CellValueChanged
        ControlarBotones()

        If e.RowIndex = -1 Then Exit Sub
        If e.ColumnIndex <> dgvDatos.ColumnCount - 1 Then
            Dim Cm As CurrencyManager = dgvDatos.BindingContext(dgvDatos.DataSource, dgvDatos.DataMember)
            Cm.EndCurrentEdit()
            Exit Sub
        End If

        dgvDatos.SuspendLayout()
        Dim Diccionario As New Dictionary(Of Integer, DataGridViewAutoSizeColumnMode)

        ' Ponemos las columnas para que no se autoredimensionen para evitar procesado extra
        For Each Columna As DataGridViewColumn In dgvDatos.Columns
            Diccionario.Add(Columna.Index, Columna.AutoSizeMode)
            Columna.AutoSizeMode = DataGridViewAutoSizeColumnMode.None
        Next

        With DirectCast(dgvDatos.Rows(e.RowIndex).Cells(e.ColumnIndex), DataGridViewCheckBoxCell)
            If .Value Then
                For Each Fila As DataGridViewRow In dgvDatos.Rows
                    If Fila.Index = e.RowIndex Then Continue For

                    Fila.Cells(dgvDatos.ColumnCount - 1).Value = False
                Next
            End If
        End With

        ' Vuelvo a poner los valores por defecto
        For Each Entrada In Diccionario
            dgvDatos.Columns(Entrada.Key).AutoSizeMode = Entrada.Value
        Next
        dgvDatos.ResumeLayout()
    End Sub
#End Region
#Region " IMAGENES "
    ''' <summary>
    ''' Muestra la imagen del registro en caso de que la tenga asignada
    ''' </summary>
    Private Sub VerImagen(sender As Object, e As EventArgs) Handles btnVerImagen.Click
        ' Validaciones
        If dgvDatos.CurrentRow Is Nothing OrElse dgvDatos.CurrentRow.IsNewRow Then Exit Sub
        If dgvDatos.CurrentRow.DataBoundItem Is Nothing OrElse Not TypeOf (dgvDatos.CurrentRow.DataBoundItem) Is IImagen Then Exit Sub

        Dim Registro As IImagen = dgvDatos.CurrentRow.DataBoundItem
        If Not Registro.TieneImagen Then Exit Sub

        ' Muestro el formulario
        Dim Formulario As New frmVerImagen With {.Registro = Registro}
        Formulario.ShowDialog()
    End Sub

    ''' <summary>
    ''' Elimina la imagen asociada al registro
    ''' </summary>
    Private Sub EliminarImagen(sender As Object, e As EventArgs) Handles btnEliminarImagen.Click
        ' Validaciones
        If dgvDatos.CurrentRow Is Nothing OrElse dgvDatos.CurrentRow.IsNewRow Then Exit Sub
        If dgvDatos.CurrentRow.DataBoundItem Is Nothing OrElse Not TypeOf (dgvDatos.CurrentRow.DataBoundItem) Is IImagen Then Exit Sub

        Dim Registro As IImagen = dgvDatos.CurrentRow.DataBoundItem
        If Not Registro.TieneImagen Then Exit Sub

        ' Pido Confirmación
        If Quadralia.Aplicacion.MostrarMessageBox("¿Está seguro de querer eliminar la imagen asociada a este registro?", "Confirmar Eliminación", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then Exit Sub

        ' Muestro el formulario
        Registro.Imagen = Nothing

        ControlarBotones()
    End Sub

    ''' <summary>
    ''' Asigna una nueva imagen al registro seleccionado
    ''' </summary>
    Private Sub AsignarImagen(sender As Object, e As EventArgs) Handles btnAsignarImagen.Click
        ' Validaciones
        If dgvDatos.CurrentRow Is Nothing OrElse dgvDatos.CurrentRow.IsNewRow Then Exit Sub
        If dgvDatos.CurrentRow.DataBoundItem Is Nothing OrElse Not TypeOf (dgvDatos.CurrentRow.DataBoundItem) Is IImagen Then Exit Sub

        Dim Registro As IImagen = dgvDatos.CurrentRow.DataBoundItem
        Dim opfImagenes As New OpenFileDialog

        With opfImagenes
            .CheckFileExists = True
            .CheckPathExists = True
            .Filter = "Archivos de Imagen|*.jpg;*.jpeg;*.gif;*.bmp;*.png;|Todos los archivos|*.*"
        End With

        If opfImagenes.ShowDialog <> Windows.Forms.DialogResult.OK Then Exit Sub
        If Not IO.File.Exists(opfImagenes.FileName) Then Exit Sub

        Try
            Registro.Imagen = Image.FromFile(opfImagenes.FileName)
        Catch ex As Exception
            Registro.Imagen = Nothing
        End Try

        ControlarBotones()
    End Sub
#End Region
End Class