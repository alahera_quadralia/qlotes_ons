﻿Imports Escritorio.Quadralia.BackUpBBDD

Public Class frmConfiguracionBBDD
    Inherits FormularioHijo

#Region " DECLARACIONES "
    Private _Estado As Quadralia.Enumerados.EstadoFormulario = Quadralia.Enumerados.EstadoFormulario.Espera
    Private _Contexto As Entidades = Nothing
    Private ListaElementos As New Dictionary(Of String, Object)
#End Region
#Region " PROPIEDADES "
    Private ReadOnly Property Contexto As Entidades
        Get
            If _Contexto Is Nothing Then _Contexto = cDAL.Instancia.getContext
            Return _Contexto
        End Get
    End Property
#End Region
#Region " DECLARACIONES NECESARIAS DE FORMULARIO HIJO "
    Public Overrides ReadOnly Property Tab As ComponentFactory.Krypton.Ribbon.KryptonRibbonTab
        Get
            Return frmPrincipal.tabConfiguracion
        End Get
    End Property
#End Region
#Region " LIMPIEZA "
    ''' <summary>
    ''' Limpia todos los controles con datos del formulario
    ''' </summary>
    Public Sub LimpiarDatos()
        txtServidorAplicacion.Clear()
        txtPuertoAplicacion.Clear()
        txtNombreBaseDatosAplicacion.Clear()
        txtUsuarioAplicacion.Clear()
        txtClaveAplicacion.Clear()
        txtRespuestaServidorAplicacion.Clear()
    End Sub

    Private Sub LimpiarTodo()
        LimpiarDatos()
    End Sub
#End Region
#Region " RUTINA DE INICIO / FIN "
    ''' <summary>
    ''' Rutina de inicio del formulario
    ''' </summary>
    Private Sub Inicio(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LimpiarTodo()

        Cargar()
        tabGeneral.SelectedIndex = 0
    End Sub
#End Region
#Region " CARGAR / GUARDAR / CANCELAR "
    ''' <summary>
    ''' Carga la configuración de BBDD en el formulario
    ''' </summary>
    Private Sub Cargar()
        With cConfiguracionBBDD.Instancia
            txtServidorAplicacion.Text = .BBDDServidor
            txtPuertoAplicacion.Text = .BBDDPuerto
            txtNombreBaseDatosAplicacion.Text = .BBDDNombre
            txtUsuarioAplicacion.Text = .BBDDUsuario
            txtClaveAplicacion.Text = .BBDDPassword
        End With
    End Sub

    ''' <summary>
    ''' Guarda los cambios del formulario en BBDD
    ''' </summary>
    ''' <param name="MostrarMensaje"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overrides Function Guardar(Optional ByVal MostrarMensaje As Boolean = True) As Boolean
        With cConfiguracionBBDD.Instancia
            .BBDDServidor = txtServidorAplicacion.Text
            If Not String.IsNullOrEmpty(txtPuertoAplicacion.Text) Then Integer.TryParse(txtPuertoAplicacion.Text, .BBDDPuerto)
            .BBDDNombre = txtNombreBaseDatosAplicacion.Text
            .BBDDUsuario = txtUsuarioAplicacion.Text
            .BBDDPassword = txtClaveAplicacion.Text
        End With

        Try
            If Not cConfiguracionBBDD.Instancia.Guardar() Then Return False
            If Quadralia.Aplicacion.ConexionBBDD Then Contexto.SaveChanges()
            If MostrarMensaje Then Quadralia.Aplicacion.MostrarMensajeNtfIco(My.Resources.TituloDatosGuardatos, My.Resources.DatosGuardados, ToolTipIcon.Info)

            Me.Close()
            Return True
        Catch ex As Exception
            Quadralia.Aplicacion.InformarError(ex, False, True)
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Cancela los cambios realizados en el formulario
    ''' </summary>
    Public Overrides Sub Cancelar()
        Me.Close()
    End Sub
#End Region
#Region " VALIDACIONES "
    Private Sub SoloNumeros(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPuertoAplicacion.KeyPress
        Quadralia.Validadores.QuadraliaValidadores.SoloNumeros(sender, e, False)
    End Sub
#End Region
#Region " RESTO DE FUNCIONES "
    ''' <summary>
    ''' Realiza una prueba de conexión al servidor de base de datos de la aplicación
    ''' </summary>
    Private Sub PingServidorAplicacion(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPingServidorAplicacion.Click
        Try
            txtRespuestaServidorAplicacion.Text = "Intentando conectar con el servidor especificado"
            picProgresoPingAplicacion.Visible = True
            tblOrganizadorAplicacion.Refresh()
            btnPingServidorAplicacion.Enabled = False

            Using conexion As System.Data.Common.DbConnection = cDAL.Instancia.GetFactory().CreateConnection
                conexion.ConnectionString = String.Format("Server={0};Database={1};Uid={2};Pwd={3};Port={4}", txtServidorAplicacion.Text, txtNombreBaseDatosAplicacion.Text, txtUsuarioAplicacion.Text, txtClaveAplicacion.Text, txtPuertoAplicacion.Text)
                conexion.Open()
            End Using

            txtRespuestaServidorAplicacion.Text &= Environment.NewLine & "Conexión establecida correctamente"
        Catch ex As Exception
            txtRespuestaServidorAplicacion.Text &= Environment.NewLine & "No se puede conectar con el servidor especificado: " & ex.Message
        Finally
            btnPingServidorAplicacion.Enabled = True
            picProgresoPingAplicacion.Visible = False
        End Try
    End Sub
#End Region
End Class