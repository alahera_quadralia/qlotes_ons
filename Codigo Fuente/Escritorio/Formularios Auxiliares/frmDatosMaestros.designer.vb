﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDatosMaestros
    Inherits FormularioHijo

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDatosMaestros))
        Me.pCuerpo = New ComponentFactory.Krypton.Toolkit.KryptonPanel()
        Me.tbloOrganizador = New System.Windows.Forms.TableLayoutPanel()
        Me.lblTabla = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.cboTabla = New ComponentFactory.Krypton.Toolkit.KryptonComboBox()
        Me.hdrDatos = New ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup()
        Me.btnVerImagen = New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup()
        Me.btnAsignarImagen = New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup()
        Me.btnEliminarImagen = New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup()
        Me.dgvDatos = New ComponentFactory.Krypton.Toolkit.KryptonDataGridView()
        Me.grpCabecera = New ComponentFactory.Krypton.Toolkit.KryptonGroup()
        Me.lblTitulo = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.epErrores = New Escritorio.Quadralia.Controles.Validacion.GestorErrores()
        CType(Me.pCuerpo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pCuerpo.SuspendLayout()
        Me.tbloOrganizador.SuspendLayout()
        CType(Me.cboTabla, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.hdrDatos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.hdrDatos.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.hdrDatos.Panel.SuspendLayout()
        Me.hdrDatos.SuspendLayout()
        CType(Me.dgvDatos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpCabecera, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpCabecera.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpCabecera.Panel.SuspendLayout()
        Me.grpCabecera.SuspendLayout()
        CType(Me.epErrores, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pCuerpo
        '
        Me.pCuerpo.Controls.Add(Me.tbloOrganizador)
        Me.pCuerpo.Controls.Add(Me.grpCabecera)
        resources.ApplyResources(Me.pCuerpo, "pCuerpo")
        Me.pCuerpo.Name = "pCuerpo"
        '
        'tbloOrganizador
        '
        Me.tbloOrganizador.BackColor = System.Drawing.Color.White
        resources.ApplyResources(Me.tbloOrganizador, "tbloOrganizador")
        Me.tbloOrganizador.Controls.Add(Me.lblTabla, 0, 0)
        Me.tbloOrganizador.Controls.Add(Me.cboTabla, 1, 0)
        Me.tbloOrganizador.Controls.Add(Me.hdrDatos, 0, 1)
        Me.tbloOrganizador.Name = "tbloOrganizador"
        '
        'lblTabla
        '
        resources.ApplyResources(Me.lblTabla, "lblTabla")
        Me.lblTabla.Name = "lblTabla"
        Me.lblTabla.Values.Text = resources.GetString("lblTabla.Values.Text")
        '
        'cboTabla
        '
        resources.ApplyResources(Me.cboTabla, "cboTabla")
        Me.cboTabla.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTabla.DropDownWidth = 333
        Me.cboTabla.Name = "cboTabla"
        '
        'hdrDatos
        '
        Me.hdrDatos.AutoCollapseArrow = False
        Me.hdrDatos.ButtonSpecs.AddRange(New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup() {Me.btnVerImagen, Me.btnAsignarImagen, Me.btnEliminarImagen})
        Me.tbloOrganizador.SetColumnSpan(Me.hdrDatos, 2)
        resources.ApplyResources(Me.hdrDatos, "hdrDatos")
        Me.hdrDatos.HeaderStylePrimary = ComponentFactory.Krypton.Toolkit.HeaderStyle.Secondary
        Me.hdrDatos.HeaderVisibleSecondary = False
        Me.hdrDatos.Name = "hdrDatos"
        '
        'hdrDatos.Panel
        '
        Me.hdrDatos.Panel.Controls.Add(Me.dgvDatos)
        Me.hdrDatos.ValuesPrimary.Heading = resources.GetString("hdrDatos.ValuesPrimary.Heading")
        Me.hdrDatos.ValuesPrimary.Image = CType(resources.GetObject("hdrDatos.ValuesPrimary.Image"), System.Drawing.Image)
        '
        'btnVerImagen
        '
        resources.ApplyResources(Me.btnVerImagen, "btnVerImagen")
        Me.btnVerImagen.UniqueName = "C00797627ADF4894529894535E73C285"
        '
        'btnAsignarImagen
        '
        Me.btnAsignarImagen.Image = Global.Escritorio.My.Resources.Resources.AbrirImagen_16
        resources.ApplyResources(Me.btnAsignarImagen, "btnAsignarImagen")
        Me.btnAsignarImagen.UniqueName = "7D576C80DC5E4EFF48BFC29F3955B25C"
        '
        'btnEliminarImagen
        '
        Me.btnEliminarImagen.Image = Global.Escritorio.My.Resources.Resources.EliminarImagen_16
        resources.ApplyResources(Me.btnEliminarImagen, "btnEliminarImagen")
        Me.btnEliminarImagen.UniqueName = "6EE55D3FEE6D4703D285C37517A4B42F"
        '
        'dgvDatos
        '
        Me.dgvDatos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgvDatos, "dgvDatos")
        Me.dgvDatos.Name = "dgvDatos"
        Me.dgvDatos.RowHeadersVisible = False
        '
        'grpCabecera
        '
        resources.ApplyResources(Me.grpCabecera, "grpCabecera")
        Me.grpCabecera.GroupBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.PanelCustom1
        Me.grpCabecera.Name = "grpCabecera"
        '
        'grpCabecera.Panel
        '
        Me.grpCabecera.Panel.Controls.Add(Me.lblTitulo)
        Me.grpCabecera.StateCommon.Border.Color1 = System.Drawing.Color.DarkOrange
        Me.grpCabecera.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom
        Me.grpCabecera.StateCommon.Border.Width = 5
        '
        'lblTitulo
        '
        Me.lblTitulo.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.Custom1
        resources.ApplyResources(Me.lblTitulo, "lblTitulo")
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Values.Image = Global.Escritorio.My.Resources.Resources.DatosMaestros_32
        Me.lblTitulo.Values.Text = resources.GetString("lblTitulo.Values.Text")
        '
        'epErrores
        '
        Me.epErrores.Alineacion = System.Windows.Forms.ErrorIconAlignment.TopLeft
        Me.epErrores.ContainerControl = Me
        '
        'frmDatosMaestros
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.pCuerpo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDatosMaestros"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        CType(Me.pCuerpo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pCuerpo.ResumeLayout(False)
        Me.tbloOrganizador.ResumeLayout(False)
        Me.tbloOrganizador.PerformLayout()
        CType(Me.cboTabla, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.hdrDatos.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrDatos.Panel.ResumeLayout(False)
        CType(Me.hdrDatos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrDatos.ResumeLayout(False)
        CType(Me.dgvDatos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grpCabecera.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpCabecera.Panel.ResumeLayout(False)
        Me.grpCabecera.Panel.PerformLayout()
        CType(Me.grpCabecera, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpCabecera.ResumeLayout(False)
        CType(Me.epErrores, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pCuerpo As ComponentFactory.Krypton.Toolkit.KryptonPanel
    Friend WithEvents epErrores As Quadralia.Controles.Validacion.GestorErrores
    Friend WithEvents grpCabecera As ComponentFactory.Krypton.Toolkit.KryptonGroup
    Friend WithEvents lblTitulo As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents tbloOrganizador As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblTabla As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents cboTabla As ComponentFactory.Krypton.Toolkit.KryptonComboBox
    Friend WithEvents dgvDatos As ComponentFactory.Krypton.Toolkit.KryptonDataGridView
    Friend WithEvents hdrDatos As ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup
    Friend WithEvents btnVerImagen As ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup
    Friend WithEvents btnAsignarImagen As ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup
    Friend WithEvents btnEliminarImagen As ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup
End Class
