﻿Public Class frmSeleccionarAlbaranEntrada
    ''' <summary>
    ''' Listado de familias seleccionadas para devolver al formulario que las solicita
    ''' </summary>    
    Public Property Items As New List(Of LoteEntradalinea)
    Private Property Exclusiones As List(Of LoteEntradalinea)
    Private Property MultipleSeleccion As Boolean
    Public Property Contexto As Entidades

#Region " LIMPIEZA "
    ''' <summary>
    ''' Limpia todos los controles
    ''' </summary>
    Private Sub Limpiar()
        LimpiarBusqueda()
        LimpiarDatos()
    End Sub

    Private Sub LimpiarBusqueda()
        txtBcodigoA.Text = String.Empty
        txtBcodigoDe.Text = String.Empty

        dtpBFechaA.Value = DateTime.Now
        dtpBFechaA.ValueNullable = DateTime.Now

        dtpBFechaDe.Value = DateTime.Now.AddDays(-5)
        dtpBFechaDe.ValueNullable = DateTime.Now.AddDays(-5)
    End Sub

    Private Sub LimpiarDatos()
        dgvRecibos.DataSource = Nothing
        dgvRecibos.Rows.Clear()
    End Sub

    Private Sub LimpiarControl(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Quadralia.KryptonForms.LimpiarControl(sender)
    End Sub
#End Region
#Region " CARGAR / GUARDAR / CANCELAR "
    Private Sub PrepararDataGrid()
        dgvRecibos.AutoGenerateColumns = False

        If Not MultipleSeleccion Then Exit Sub

        Dim colSelect As New DataGridViewCheckBoxColumn
        With colSelect
            .HeaderText = "Sel."
            .DataPropertyName = Nothing
            .Name = Nothing
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            .ReadOnly = False
        End With

        dgvRecibos.Columns.Insert(0, colSelect)
    End Sub

    Private Sub cargarRegistros() Handles btnBBuscar.Click
        Dim FechaInicio As Nullable(Of DateTime) = Nothing
        Dim FechaFin As Nullable(Of DateTime) = Nothing
        Dim IdEspecie As Nullable(Of Integer) = Nothing
        Dim IdProveedor As Nullable(Of Long) = Nothing

        If Not IsDBNull(dtpBFechaDe.ValueNullable) Then FechaInicio = dtpBFechaDe.Value.Date + New TimeSpan(0, 0, 0)
        If Not IsDBNull(dtpBFechaA.ValueNullable) Then FechaFin = dtpBFechaA.Value.Date + New TimeSpan(23, 59, 59)
        If txtBEspecie.Item IsNot Nothing Then IdEspecie = DirectCast(txtBEspecie.Item, Especie).id
        If txtBProveedor.Item IsNot Nothing Then IdProveedor = DirectCast(txtBProveedor.Item, Proveedor).id

        Dim Registros = (From entrada As LoteEntrada In Contexto.LotesEntrada
                         From linea In entrada.Lineas.DefaultIfEmpty
                         Where (linea IsNot Nothing _
                                AndAlso (String.IsNullOrEmpty(txtBcodigoDe.Text) OrElse entrada.codigo >= txtBcodigoDe.Text) _
                                AndAlso (String.IsNullOrEmpty(txtBcodigoA.Text) OrElse entrada.codigo <= txtBcodigoA.Text) _
                                AndAlso (Not FechaInicio.HasValue OrElse (linea IsNot Nothing AndAlso linea.fechaDesembarco.HasValue AndAlso linea.fechaDesembarco.Value >= FechaInicio.Value)) _
                                AndAlso (Not FechaFin.HasValue OrElse (linea IsNot Nothing AndAlso linea.fechaDesembarco.HasValue AndAlso linea.fechaDesembarco.Value <= FechaFin.Value)) _
                                AndAlso (Not IdEspecie.HasValue OrElse (linea IsNot Nothing AndAlso linea.Especie IsNot Nothing AndAlso linea.Especie.id = IdEspecie.Value)) _
                                AndAlso (Not IdProveedor.HasValue OrElse (linea IsNot Nothing AndAlso linea.LoteEntrada IsNot Nothing AndAlso linea.LoteEntrada.Proveedor IsNot Nothing AndAlso linea.LoteEntrada.Proveedor.id = IdProveedor.Value)))
                         Select linea).Distinct

        Dim Total As Integer = Registros.Count

        ' Se asigna el datasource para cargar los clientes
        If Exclusiones IsNot Nothing AndAlso Exclusiones.Count > 0 Then
            Registros = Registros.Except(Exclusiones)
        End If

        ' Ordeno los registros
        dgvRecibos.DataSource = Registros.ToList()
    End Sub

    ''' <summary>
    ''' Añade la lista de elementos seleccionados y los devuelve
    ''' </summary>
    Private Sub Aceptar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If MultipleSeleccion Then
            For Each linea As DataGridViewRow In dgvRecibos.Rows
                If linea.Cells(0).Value = True Then
                    Items.Add(linea.DataBoundItem)
                End If
            Next
        ElseIf dgvRecibos IsNot Nothing AndAlso dgvRecibos.CurrentRow IsNot Nothing AndAlso dgvRecibos.CurrentRow.DataBoundItem IsNot Nothing Then
            Items.Add(dgvRecibos.CurrentRow.DataBoundItem)
        Else
            Items = New List(Of LoteEntradalinea)
        End If

        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    ''' <summary>
    ''' El usuario indica que no desea crear un nuevo contacto
    ''' </summary>
    Private Sub Cancelar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Items.Clear()
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub cboSeleccionar_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSeleccionar.SelectedIndexChanged
        Quadralia.WinForms.Selecciones.cSelecciones.marcarSeleccionados(dgvRecibos, cboSeleccionar.SelectedIndex, 0)
    End Sub
#End Region
#Region " RUTINA DE INICIO "
    Public Sub Inicio() Handles Me.Shown
        ' Situo el foco en el dgv
        dgvRecibos.Focus()
    End Sub

    Public Sub New(ByVal Contexto As Entidades, ByVal Exclusiones As List(Of LoteEntradalinea), Optional ByVal MultipleSeleccion As Boolean = False)
        Me.Contexto = Contexto

        Quadralia.Formularios.AutoTabular(Me)

        ' Llamada necesaria para el Diseñador de Windows Forms.
        InitializeComponent()

        ' Asignación de contextos        
        Me.Exclusiones = Exclusiones
        Me.MultipleSeleccion = MultipleSeleccion

        ' Limpio los controles
        Limpiar()

        ' Crea las columnas necesarias en el DataGrid
        PrepararDataGrid()

        If Not MultipleSeleccion Then
            lblSeleccionar.Visible = False
            cboSeleccionar.Visible = False
        End If

        ' Cargar opciones de seleccion
        Quadralia.WinForms.Selecciones.cSelecciones.anhadirOpcionesSeleccion(cboSeleccionar)

        ' Carga los artículos
        cargarRegistros()
    End Sub

    Public Sub New(ByVal Contexto As Entidades)
        Me.New(Contexto, New List(Of LoteEntradalinea), False)
    End Sub
#End Region
#Region " MANEJO DE CONTROLES "
    ''' <summary>
    ''' Ordena los resultados del DGV de Resultados    
    ''' </summary>
    Private Sub OrdenarResultados(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvRecibos.ColumnHeaderMouseClick
        Quadralia.Formularios.Funcionalidad.OrdenarDGV(Of LoteEntradalinea)(dgvRecibos, e)
    End Sub

    Private Sub DobleClickItem(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvRecibos.DoubleClick
        If dgvRecibos.SelectedRows.Count = 1 AndAlso MultipleSeleccion = False Then
            Aceptar(Nothing, Nothing)
        ElseIf dgvRecibos.SelectedRows.Count = 1 AndAlso MultipleSeleccion Then
            dgvRecibos.SelectedRows(0).Cells(0).Value = Not dgvRecibos.SelectedRows(0).Cells(0).Value
        End If
    End Sub

    ''' <summary>
    ''' Controlamos el enter para que vaya al botón de aceptar
    ''' </summary>
    Private Sub ControlEnter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvRecibos.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.Handled = True
            Aceptar(Nothing, Nothing)
        ElseIf e.KeyCode = Keys.Tab Then
            e.Handled = True
            Me.SelectNextControl(sender, True, True, True, True)
        ElseIf e.KeyCode = Keys.Space AndAlso MultipleSeleccion AndAlso dgvRecibos.CurrentRow.Index > -1 Then
            e.Handled = True
            DobleClickItem(Nothing, Nothing)
        End If
    End Sub
#End Region
#Region " RESTO DE FUNCIONES "
    ''' <summary>
    ''' Evita que salga el molesto error del DGV
    ''' </summary>
    Private Sub EvitarErrores(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvRecibos.DataError
        e.Cancel = True
    End Sub
#End Region
#Region " FUNCIONES EXTRAS "
    ''' <summary>
    ''' Localiza una entidad por su código y la devuelve para ser tratada
    ''' </summary>
    Public Shared Function BuscarPorCodigo(ByVal Contexto As Entidades, ByVal CodigoBusqueda As Long) As List(Of LoteEntradalinea)
        Dim ListaAlbaranes = (From It As LoteEntradalinea In Contexto.LotesEntradaLineas
                              Where It.id = CodigoBusqueda
                              Select It).ToList
        If ListaAlbaranes.Count > 0 Then
            Return ListaAlbaranes
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function ObetenerCodigos(ByVal Contexto As Entidades) As String()
        Return (From It As LoteEntradalinea In Contexto.LotesEntradaLineas Order By It.numeroLote Select It.numeroLote).ToList.ToArray
    End Function
#End Region
#Region " RESTO DE FUNCIONES "
    ''' <summary>
    ''' Lanza la busqueda de registros al pulsar enter
    ''' </summary>
    Private Sub LanzarBusquedaEnEnter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtBcodigoDe.KeyDown, txtBcodigoA.KeyDown, txtBEspecie.KeyDown, dtpBFechaDe.KeyDown, dtpBFechaA.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.Handled = True
            e.SuppressKeyPress = True
            cargarRegistros()
        End If
    End Sub
#End Region
End Class