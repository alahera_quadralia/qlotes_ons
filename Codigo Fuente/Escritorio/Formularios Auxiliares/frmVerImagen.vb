﻿Public Class frmVerImagen
#Region " DECLARACIONES "
    Private _Registro As IImagen
#End Region
#Region " PROPIEDADES "
    ''' <summary>
    ''' Obtiene la linea del lote de entrada mostrada por el servidor
    ''' </summary>
    Public Property Registro As IImagen
        Get
            Return _Registro
        End Get
        Set(value As IImagen)
            _Registro = value
            CargarRegistro(value)
        End Set
    End Property
#End Region
#Region " LIMPIEZA "
    Public Sub Limpiar()
        picQR.Image = Nothing
    End Sub
#End Region
#Region " RUTINA DE INICIO "
    ''' <summary>
    ''' Muestra el formulario de acorde ha si existe conexión con la base de datos o no
    ''' </summary>
    Private Sub Inicio(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Limpiar()
        CargarRegistro()
    End Sub
#End Region
#Region " CARGAR DATOS "
    Private Sub CargarRegistro()
        CargarRegistro(Me.Registro)
    End Sub

    Private Sub CargarRegistro(Instancia As IImagen)
        Limpiar()

        If Instancia Is Nothing Then Exit Sub
        If Not Instancia.TieneImagen Then Exit Sub

        picQR.Image = Instancia.Imagen
    End Sub
#End Region
#Region " ACEPTAR / CANCELAR "
    ''' <summary>
    ''' Lanza la rutina de validación de usuario
    ''' </summary>
    Private Sub Aceptar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Me.Close()
    End Sub
#End Region
End Class