﻿Public Class frmSeleccionarCliente
    ''' <summary>
    ''' Listado de familias seleccionadas para devolver al formulario que las solicita
    ''' </summary>    
    Public Property Items As New List(Of Cliente)
    Private Property Exclusiones As List(Of Cliente)
    Private Property MultipleSeleccion As Boolean
    Private Property Contexto As Entidades

#Region " LIMPIEZA "
    ''' <summary>
    ''' Limpia todos los controles
    ''' </summary>
    Private Sub Limpiar()
        dgvClientes.Rows.Clear()
    End Sub
#End Region
#Region " CARGAR / GUARDAR / CANCELAR "
    Private Sub PrepararDataGrid()
        dgvClientes.Columns.Clear()
        dgvClientes.AutoGenerateColumns = False

        If MultipleSeleccion Then
            Dim colSelect As DataGridViewCheckBoxColumn
            colSelect = New DataGridViewCheckBoxColumn
            CType(colSelect, DataGridViewCheckBoxColumn).HeaderText = "Sel."
            CType(colSelect, DataGridViewCheckBoxColumn).DataPropertyName = Nothing
            CType(colSelect, DataGridViewCheckBoxColumn).Name = Nothing
            CType(colSelect, DataGridViewCheckBoxColumn).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            CType(colSelect, DataGridViewCheckBoxColumn).ReadOnly = False
            dgvClientes.Columns.Add(CType(colSelect, DataGridViewCheckBoxColumn))
        End If

        Dim colCodigo As DataGridViewTextBoxColumn
        colCodigo = New DataGridViewTextBoxColumn
        CType(colCodigo, DataGridViewTextBoxColumn).HeaderText = "Cód."
        CType(colCodigo, DataGridViewTextBoxColumn).Name = "Codigo"
        CType(colCodigo, DataGridViewTextBoxColumn).DataPropertyName = "codigo"
        CType(colCodigo, DataGridViewTextBoxColumn).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
        CType(colCodigo, DataGridViewTextBoxColumn).ReadOnly = True
        dgvClientes.Columns.Add(CType(colCodigo, DataGridViewTextBoxColumn))

        Dim colCIF As DataGridViewTextBoxColumn
        colCIF = New DataGridViewTextBoxColumn
        CType(colCIF, DataGridViewTextBoxColumn).HeaderText = "CIF/VAT"
        CType(colCIF, DataGridViewTextBoxColumn).Name = "cif"
        CType(colCIF, DataGridViewTextBoxColumn).DataPropertyName = "cif"
        CType(colCIF, DataGridViewTextBoxColumn).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
        CType(colCIF, DataGridViewTextBoxColumn).ReadOnly = True
        dgvClientes.Columns.Add(CType(colCIF, DataGridViewTextBoxColumn))

        Dim colNombre As DataGridViewTextBoxColumn
        colNombre = New DataGridViewTextBoxColumn
        CType(colNombre, DataGridViewTextBoxColumn).HeaderText = "Razón Social"
        CType(colNombre, DataGridViewTextBoxColumn).Name = "razonSocial"
        CType(colNombre, DataGridViewTextBoxColumn).DataPropertyName = "razonSocial"
        CType(colNombre, DataGridViewTextBoxColumn).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        CType(colNombre, DataGridViewTextBoxColumn).MinimumWidth = 80
        CType(colNombre, DataGridViewTextBoxColumn).ReadOnly = True
        dgvClientes.Columns.Add(CType(colNombre, DataGridViewTextBoxColumn))

        Dim colNombreComercial As New DataGridViewTextBoxColumn With {
            .HeaderText = "Nombre Comercial",
            .Name = "colNombreComercial",
            .DataPropertyName = "nombreComercial",
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill,
            .MinimumWidth = 80,
            .ReadOnly = True
            }
        dgvClientes.Columns.Add(colNombreComercial)
    End Sub

    Private Sub cargarClientes() Handles btnBBuscar.Click
        ' Se eliminan todos los clientes que ya están añadidos y se aplican los filtros
        Dim ListaArticulos = (From cli As Cliente In Contexto.Clientes _
                              Where ((cli.nombreComercial.ToUpper.Contains(txtBClienteNombre.Text.ToUpper) OrElse String.IsNullOrEmpty(txtBClienteNombre.Text)) _
                              Or (cli.razonSocial.ToUpper.Contains(txtBClienteNombre.Text.ToUpper) OrElse String.IsNullOrEmpty(txtBClienteNombre.Text))) _
                              And (cli.codigo.ToUpper.Contains(txtBClienteCodigo.Text.ToUpper) OrElse String.IsNullOrEmpty(txtBClienteCodigo.Text)) _
                              And (cli.cif.ToUpper.Contains(txtBClienteCIF.Text.ToUpper) OrElse String.IsNullOrEmpty(txtBClienteCIF.Text)) _
                              Select cli).ToList.Except(Exclusiones)

        ' Se asigna el datasource para cargar los clientes
        Me.dgvClientes.DataSource = ListaArticulos.ToList
        dgvClientes.Focus()
    End Sub

    ''' <summary>
    ''' Añade la lista de elementos seleccionados y los devuelve
    ''' </summary>
    Private Sub Aceptar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If MultipleSeleccion Then
            For Each linea As DataGridViewRow In dgvClientes.Rows
                If linea.Cells(0).Value = True Then
                    Items.Add(linea.DataBoundItem)
                End If
            Next
        ElseIf dgvClientes.CurrentRow IsNot Nothing AndAlso dgvClientes.CurrentRow.DataBoundItem IsNot Nothing Then
            Items.Add(dgvClientes.CurrentRow.DataBoundItem)
        Else
            Items = New List(Of Cliente)
        End If

        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    ''' <summary>
    ''' El usuario indica que no desea crear un nuevo contacto
    ''' </summary>
    Private Sub Cancelar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Items.Clear()
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub cboSeleccionar_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSeleccionar.SelectedIndexChanged
        Quadralia.WinForms.Selecciones.cSelecciones.marcarSeleccionados(dgvClientes, cboSeleccionar.SelectedIndex, 0)
    End Sub
#End Region
#Region " RUTINA DE INICIO "
    Public Sub Inicio() Handles Me.Shown
        ' Situo el foco en el buscador
        dgvClientes.Focus()
    End Sub

    Public Sub New(ByVal Contexto As Entidades, _
                   ByVal Exclusiones As List(Of Cliente), _
                   Optional ByVal MultipleSeleccion As Boolean = False)
        ' Llamada necesaria para el Diseñador de Windows Forms.
        InitializeComponent()

        Quadralia.Formularios.AutoTabular(Me)

        ' Asignación de contextos
        Me.Contexto = Contexto
        Me.Exclusiones = Exclusiones
        Me.MultipleSeleccion = MultipleSeleccion

        ' Limpio los controles
        Limpiar()

        ' Crea las columnas necesarias en el DataGrid
        PrepararDataGrid()

        If Not MultipleSeleccion Then
            lblSeleccionar.Visible = False
            cboSeleccionar.Visible = False
        End If

        ' Cargar opciones de seleccion
        Quadralia.WinForms.Selecciones.cSelecciones.anhadirOpcionesSeleccion(cboSeleccionar)

        ' Carga los artículos
        cargarClientes()

        For Each UnControl As Control In tblFiltrosTarifas.Controls
            Quadralia.Formularios.Funcionalidad.ManejadorTabPorIntro(UnControl)
        Next
    End Sub

    Public Sub New(ByVal Contexto As Entidades)
        Me.New(Contexto, New List(Of Cliente), False)
    End Sub
#End Region
#Region " MANEJO DE CONTROLES "
    ''' <summary>
    ''' Ordena los resultados del DGV de Resultados    
    ''' </summary>
    Private Sub OrdenarResultados(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvClientes.ColumnHeaderMouseClick
        Quadralia.Formularios.Funcionalidad.OrdenarDGV(Of Cliente)(dgvClientes, e)
    End Sub

    Private Sub DobleClickItem(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvClientes.DoubleClick
        If dgvClientes.SelectedRows.Count = 1 AndAlso MultipleSeleccion = False Then
            Aceptar(Nothing, Nothing)
        ElseIf dgvClientes.SelectedRows.Count = 1 AndAlso MultipleSeleccion Then
            dgvClientes.SelectedRows(0).Cells(0).Value = Not dgvClientes.SelectedRows(0).Cells(0).Value
        End If
    End Sub

    ''' <summary>
    ''' Controlamos el enter para que vaya al botón de aceptar
    ''' </summary>
    Private Sub ControlEnter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvClientes.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.Handled = True
            Aceptar(Nothing, Nothing)
        ElseIf e.KeyCode = Keys.Tab Then
            e.Handled = True
            Me.SelectNextControl(sender, True, True, True, True)
        ElseIf e.KeyCode = Keys.Space AndAlso MultipleSeleccion AndAlso dgvClientes.CurrentRow.Index > -1 Then
            e.Handled = True
            DobleClickItem(Nothing, Nothing)
        End If
    End Sub
#End Region
#Region " FUNCIONES EXTRAS "
    ''' <summary>
    ''' Localiza una entidad por su código y la devuelve para ser tratada
    ''' </summary>
    Public Shared Function BuscarPorCodigo(ByVal Contexto As Entidades,
                                           ByVal CodigoBusqueda As Long) As List(Of Cliente)
        Dim ListaClientes = (From cli As Cliente In Contexto.Clientes
                             Where cli.id = CodigoBusqueda
                             Select cli).ToList
        If ListaClientes.Count > 0 Then
            Return ListaClientes
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function ObetenerCodigos(ByVal Contexto As Entidades) As String()
        Return (From It As Cliente In Contexto.Clientes Order By It.codigo Select It.codigo).ToList.ToArray
    End Function

    ''' <summary>
    ''' Evita que salga el molesto error del DGV
    ''' </summary>
    Private Sub EvitarErrores(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvClientes.DataError
        e.Cancel = True
    End Sub
#End Region
#Region " RESTO DE FUNCIONES "
    ''' <summary>
    ''' Lanza la busqueda de registros al pulsar enter
    ''' </summary>
    Private Sub LanzarBusquedaEnEnter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtBClienteNombre.KeyDown, txtBClienteCodigo.KeyDown, txtBClienteCIF.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.Handled = True
            e.SuppressKeyPress = True
            cargarClientes()
        End If
    End Sub
#End Region
End Class