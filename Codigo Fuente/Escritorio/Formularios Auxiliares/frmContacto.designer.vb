﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmContacto
    Inherits KryptonForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmContacto))
        Me.hdrContenido = New ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup()
        Me.tblOrganizador = New System.Windows.Forms.TableLayoutPanel()
        Me.lblPuesto = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblNotas = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtCargo = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtNotas = New Escritorio.Quadralia.Controles.aTextBox()
        Me.lblNombre = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblApellidos = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtNombre = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtApellidos = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtTelefono = New Escritorio.Quadralia.Controles.aTextBox()
        Me.lblTelefono = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblExtension = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.nudExtension = New ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown()
        Me.lblEmail = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtEmail = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtMovil = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtFax = New Escritorio.Quadralia.Controles.aTextBox()
        Me.lblMovil = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblFax = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.pCuerpo = New ComponentFactory.Krypton.Toolkit.KryptonPanel()
        Me.pBotonera = New ComponentFactory.Krypton.Toolkit.KryptonPanel()
        Me.btnAceptar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.btnCerrar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.epErrores = New Escritorio.Quadralia.Controles.Validacion.GestorErrores()
        CType(Me.hdrContenido, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.hdrContenido.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.hdrContenido.Panel.SuspendLayout()
        Me.hdrContenido.SuspendLayout()
        Me.tblOrganizador.SuspendLayout()
        CType(Me.pCuerpo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pCuerpo.SuspendLayout()
        CType(Me.pBotonera, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pBotonera.SuspendLayout()
        CType(Me.epErrores, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'hdrContenido
        '
        resources.ApplyResources(Me.hdrContenido, "hdrContenido")
        Me.hdrContenido.HeaderVisibleSecondary = False
        Me.hdrContenido.Name = "hdrContenido"
        '
        'hdrContenido.Panel
        '
        Me.hdrContenido.Panel.Controls.Add(Me.tblOrganizador)
        Me.hdrContenido.ValuesPrimary.Heading = resources.GetString("hdrContenido.ValuesPrimary.Heading")
        Me.hdrContenido.ValuesPrimary.Image = CType(resources.GetObject("hdrContenido.ValuesPrimary.Image"), System.Drawing.Image)
        '
        'tblOrganizador
        '
        Me.tblOrganizador.BackColor = System.Drawing.Color.Transparent
        resources.ApplyResources(Me.tblOrganizador, "tblOrganizador")
        Me.tblOrganizador.Controls.Add(Me.lblPuesto, 0, 2)
        Me.tblOrganizador.Controls.Add(Me.lblNotas, 0, 6)
        Me.tblOrganizador.Controls.Add(Me.txtCargo, 2, 2)
        Me.tblOrganizador.Controls.Add(Me.txtNotas, 0, 7)
        Me.tblOrganizador.Controls.Add(Me.lblNombre, 0, 0)
        Me.tblOrganizador.Controls.Add(Me.lblApellidos, 0, 1)
        Me.tblOrganizador.Controls.Add(Me.txtNombre, 2, 0)
        Me.tblOrganizador.Controls.Add(Me.txtApellidos, 2, 1)
        Me.tblOrganizador.Controls.Add(Me.txtTelefono, 2, 3)
        Me.tblOrganizador.Controls.Add(Me.lblTelefono, 0, 3)
        Me.tblOrganizador.Controls.Add(Me.lblExtension, 3, 3)
        Me.tblOrganizador.Controls.Add(Me.nudExtension, 5, 3)
        Me.tblOrganizador.Controls.Add(Me.lblEmail, 0, 5)
        Me.tblOrganizador.Controls.Add(Me.txtEmail, 2, 5)
        Me.tblOrganizador.Controls.Add(Me.txtMovil, 2, 4)
        Me.tblOrganizador.Controls.Add(Me.txtFax, 5, 4)
        Me.tblOrganizador.Controls.Add(Me.lblMovil, 0, 4)
        Me.tblOrganizador.Controls.Add(Me.lblFax, 3, 4)
        Me.tblOrganizador.Name = "tblOrganizador"
        '
        'lblPuesto
        '
        resources.ApplyResources(Me.lblPuesto, "lblPuesto")
        Me.lblPuesto.Name = "lblPuesto"
        Me.lblPuesto.Values.Text = resources.GetString("lblPuesto.Values.Text")
        '
        'lblNotas
        '
        resources.ApplyResources(Me.lblNotas, "lblNotas")
        Me.lblNotas.Name = "lblNotas"
        Me.lblNotas.Values.Text = resources.GetString("lblNotas.Values.Text")
        '
        'txtCargo
        '
        Me.txtCargo.AlwaysActive = False
        Me.txtCargo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tblOrganizador.SetColumnSpan(Me.txtCargo, 4)
        Me.txtCargo.controlarBotonBorrar = True
        resources.ApplyResources(Me.txtCargo, "txtCargo")
        Me.txtCargo.Formato = ""
        Me.txtCargo.mostrarSiempreBotonBorrar = False
        Me.txtCargo.Name = "txtCargo"
        Me.txtCargo.seleccionarTodo = True
        '
        'txtNotas
        '
        Me.txtNotas.AlwaysActive = False
        Me.tblOrganizador.SetColumnSpan(Me.txtNotas, 6)
        Me.txtNotas.controlarBotonBorrar = True
        resources.ApplyResources(Me.txtNotas, "txtNotas")
        Me.txtNotas.Formato = ""
        Me.txtNotas.mostrarSiempreBotonBorrar = False
        Me.txtNotas.Name = "txtNotas"
        Me.txtNotas.seleccionarTodo = True
        '
        'lblNombre
        '
        resources.ApplyResources(Me.lblNombre, "lblNombre")
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Values.Text = resources.GetString("lblNombre.Values.Text")
        '
        'lblApellidos
        '
        resources.ApplyResources(Me.lblApellidos, "lblApellidos")
        Me.lblApellidos.Name = "lblApellidos"
        Me.lblApellidos.Values.Text = resources.GetString("lblApellidos.Values.Text")
        '
        'txtNombre
        '
        Me.txtNombre.AlwaysActive = False
        Me.txtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tblOrganizador.SetColumnSpan(Me.txtNombre, 4)
        Me.txtNombre.controlarBotonBorrar = True
        resources.ApplyResources(Me.txtNombre, "txtNombre")
        Me.txtNombre.Formato = ""
        Me.txtNombre.mostrarSiempreBotonBorrar = False
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.seleccionarTodo = True
        '
        'txtApellidos
        '
        Me.txtApellidos.AlwaysActive = False
        Me.txtApellidos.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tblOrganizador.SetColumnSpan(Me.txtApellidos, 4)
        Me.txtApellidos.controlarBotonBorrar = True
        resources.ApplyResources(Me.txtApellidos, "txtApellidos")
        Me.txtApellidos.Formato = ""
        Me.txtApellidos.mostrarSiempreBotonBorrar = False
        Me.txtApellidos.Name = "txtApellidos"
        Me.txtApellidos.seleccionarTodo = True
        '
        'txtTelefono
        '
        Me.txtTelefono.AlwaysActive = False
        Me.txtTelefono.controlarBotonBorrar = True
        resources.ApplyResources(Me.txtTelefono, "txtTelefono")
        Me.txtTelefono.Formato = ""
        Me.txtTelefono.mostrarSiempreBotonBorrar = False
        Me.txtTelefono.Name = "txtTelefono"
        Me.txtTelefono.seleccionarTodo = True
        '
        'lblTelefono
        '
        resources.ApplyResources(Me.lblTelefono, "lblTelefono")
        Me.lblTelefono.Name = "lblTelefono"
        Me.lblTelefono.Values.Text = resources.GetString("lblTelefono.Values.Text")
        '
        'lblExtension
        '
        resources.ApplyResources(Me.lblExtension, "lblExtension")
        Me.lblExtension.Name = "lblExtension"
        Me.lblExtension.Values.Text = resources.GetString("lblExtension.Values.Text")
        '
        'nudExtension
        '
        resources.ApplyResources(Me.nudExtension, "nudExtension")
        Me.nudExtension.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.nudExtension.Name = "nudExtension"
        '
        'lblEmail
        '
        resources.ApplyResources(Me.lblEmail, "lblEmail")
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Values.Text = resources.GetString("lblEmail.Values.Text")
        '
        'txtEmail
        '
        Me.txtEmail.AlwaysActive = False
        Me.tblOrganizador.SetColumnSpan(Me.txtEmail, 4)
        Me.txtEmail.controlarBotonBorrar = True
        Me.txtEmail.Formato = ""
        resources.ApplyResources(Me.txtEmail, "txtEmail")
        Me.txtEmail.mostrarSiempreBotonBorrar = False
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.seleccionarTodo = True
        '
        'txtMovil
        '
        Me.txtMovil.AlwaysActive = False
        Me.txtMovil.controlarBotonBorrar = True
        resources.ApplyResources(Me.txtMovil, "txtMovil")
        Me.txtMovil.Formato = ""
        Me.txtMovil.mostrarSiempreBotonBorrar = False
        Me.txtMovil.Name = "txtMovil"
        Me.txtMovil.seleccionarTodo = True
        '
        'txtFax
        '
        Me.txtFax.AlwaysActive = False
        Me.txtFax.controlarBotonBorrar = True
        resources.ApplyResources(Me.txtFax, "txtFax")
        Me.txtFax.Formato = ""
        Me.txtFax.mostrarSiempreBotonBorrar = False
        Me.txtFax.Name = "txtFax"
        Me.txtFax.seleccionarTodo = True
        '
        'lblMovil
        '
        resources.ApplyResources(Me.lblMovil, "lblMovil")
        Me.lblMovil.Name = "lblMovil"
        Me.lblMovil.Values.Text = resources.GetString("lblMovil.Values.Text")
        '
        'lblFax
        '
        resources.ApplyResources(Me.lblFax, "lblFax")
        Me.lblFax.Name = "lblFax"
        Me.lblFax.Values.Text = resources.GetString("lblFax.Values.Text")
        '
        'pCuerpo
        '
        Me.pCuerpo.Controls.Add(Me.hdrContenido)
        Me.pCuerpo.Controls.Add(Me.pBotonera)
        resources.ApplyResources(Me.pCuerpo, "pCuerpo")
        Me.pCuerpo.Name = "pCuerpo"
        '
        'pBotonera
        '
        Me.pBotonera.Controls.Add(Me.btnAceptar)
        Me.pBotonera.Controls.Add(Me.btnCerrar)
        resources.ApplyResources(Me.pBotonera, "pBotonera")
        Me.pBotonera.Name = "pBotonera"
        '
        'btnAceptar
        '
        resources.ApplyResources(Me.btnAceptar, "btnAceptar")
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Values.Text = resources.GetString("btnAceptar.Values.Text")
        '
        'btnCerrar
        '
        resources.ApplyResources(Me.btnCerrar, "btnCerrar")
        Me.btnCerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Values.Text = resources.GetString("btnCerrar.Values.Text")
        '
        'epErrores
        '
        Me.epErrores.Alineacion = System.Windows.Forms.ErrorIconAlignment.TopLeft
        Me.epErrores.ContainerControl = Me
        '
        'frmContacto
        '
        Me.AcceptButton = Me.btnAceptar
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCerrar
        Me.ControlBox = False
        Me.Controls.Add(Me.pCuerpo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.Name = "frmContacto"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        CType(Me.hdrContenido.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrContenido.Panel.ResumeLayout(False)
        CType(Me.hdrContenido, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrContenido.ResumeLayout(False)
        Me.tblOrganizador.ResumeLayout(False)
        Me.tblOrganizador.PerformLayout()
        CType(Me.pCuerpo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pCuerpo.ResumeLayout(False)
        CType(Me.pBotonera, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pBotonera.ResumeLayout(False)
        CType(Me.epErrores, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pCuerpo As ComponentFactory.Krypton.Toolkit.KryptonPanel
    Friend WithEvents hdrContenido As ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup
    Friend WithEvents pBotonera As ComponentFactory.Krypton.Toolkit.KryptonPanel
    Friend WithEvents btnCerrar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents btnAceptar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents tblOrganizador As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblEmail As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblMovil As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblPuesto As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblTelefono As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblFax As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblNotas As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtCargo As Quadralia.Controles.aTextBox
    Friend WithEvents txtMovil As Quadralia.Controles.aTextBox
    Friend WithEvents txtEmail As Quadralia.Controles.aTextBox
    Friend WithEvents txtTelefono As Quadralia.Controles.aTextBox
    Friend WithEvents txtFax As Quadralia.Controles.aTextBox
    Friend WithEvents txtNotas As Quadralia.Controles.aTextBox
    Friend WithEvents epErrores As Escritorio.Quadralia.Controles.Validacion.GestorErrores
    Friend WithEvents lblNombre As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblApellidos As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtNombre As Quadralia.Controles.aTextBox
    Friend WithEvents txtApellidos As Quadralia.Controles.aTextBox
    Friend WithEvents lblExtension As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents nudExtension As ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown
End Class
