﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSeleccionarExpedidorLoteEntrada
    Inherits ComponentFactory.Krypton.Toolkit.KryptonForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnCancelar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.btnAceptar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.lblExpedidor = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblLineaEntrada = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblDisenhoEtiqueta = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.cboExpedidor = New Escritorio.Quadralia.Controles.aComboBox()
        Me.txtLoteEntrada = New Escritorio.cBuscadorLoteEntrada()
        Me.cboDisenhoEtiqueta = New Escritorio.Quadralia.Controles.aComboBox()
        Me.epErrores = New Escritorio.Quadralia.Controles.Validacion.GestorErrores()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.cboExpedidor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboDisenhoEtiqueta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.epErrores, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCancelar
        '
        Me.btnCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancelar.Location = New System.Drawing.Point(380, 97)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(84, 24)
        Me.btnCancelar.TabIndex = 7
        Me.btnCancelar.Values.Text = "&Cancelar"
        '
        'btnAceptar
        '
        Me.btnAceptar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAceptar.Location = New System.Drawing.Point(290, 97)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(84, 24)
        Me.btnAceptar.TabIndex = 6
        Me.btnAceptar.Values.Text = "&Aceptar"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.Controls.Add(Me.btnCancelar, 2, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.btnAceptar, 1, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.lblExpedidor, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.cboExpedidor, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.lblLineaEntrada, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.txtLoteEntrada, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.lblDisenhoEtiqueta, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.cboDisenhoEtiqueta, 1, 2)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 4
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(467, 124)
        Me.TableLayoutPanel1.TabIndex = 8
        '
        'lblExpedidor
        '
        Me.lblExpedidor.Location = New System.Drawing.Point(3, 3)
        Me.lblExpedidor.Name = "lblExpedidor"
        Me.lblExpedidor.Size = New System.Drawing.Size(65, 20)
        Me.lblExpedidor.TabIndex = 8
        Me.lblExpedidor.Values.Text = "Expedidor"
        '
        'lblLineaEntrada
        '
        Me.lblLineaEntrada.Location = New System.Drawing.Point(3, 29)
        Me.lblLineaEntrada.Name = "lblLineaEntrada"
        Me.lblLineaEntrada.Size = New System.Drawing.Size(79, 20)
        Me.lblLineaEntrada.TabIndex = 8
        Me.lblLineaEntrada.Values.Text = "Lote Entrada"
        '
        'lblDisenhoEtiqueta
        '
        Me.lblDisenhoEtiqueta.Location = New System.Drawing.Point(3, 57)
        Me.lblDisenhoEtiqueta.Name = "lblDisenhoEtiqueta"
        Me.lblDisenhoEtiqueta.Size = New System.Drawing.Size(83, 20)
        Me.lblDisenhoEtiqueta.TabIndex = 8
        Me.lblDisenhoEtiqueta.Values.Text = "Tipo Etiqueta"
        '
        'cboExpedidor
        '
        Me.TableLayoutPanel1.SetColumnSpan(Me.cboExpedidor, 2)
        Me.cboExpedidor.controlarBotonBorrar = True
        Me.cboExpedidor.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cboExpedidor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExpedidor.DropDownWidth = 131
        Me.cboExpedidor.Location = New System.Drawing.Point(92, 3)
        Me.cboExpedidor.mostrarSiempreBotonBorrar = False
        Me.cboExpedidor.Name = "cboExpedidor"
        Me.cboExpedidor.Size = New System.Drawing.Size(372, 21)
        Me.cboExpedidor.TabIndex = 13
        '
        'txtLoteEntrada
        '
        Me.TableLayoutPanel1.SetColumnSpan(Me.txtLoteEntrada, 2)
        Me.txtLoteEntrada.DescripcionReadOnly = False
        Me.txtLoteEntrada.DescripcionVisible = True
        Me.txtLoteEntrada.DisplayMember = "Descripcion"
        Me.txtLoteEntrada.DisplayText = ""
        Me.txtLoteEntrada.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtLoteEntrada.FormularioBusqueda = "Escritorio.frmSeleccionarLoteEntrada"
        Me.txtLoteEntrada.Item = Nothing
        Me.txtLoteEntrada.Location = New System.Drawing.Point(92, 29)
        Me.txtLoteEntrada.Name = "txtLoteEntrada"
        Me.txtLoteEntrada.Size = New System.Drawing.Size(372, 22)
        Me.txtLoteEntrada.TabIndex = 14
        Me.txtLoteEntrada.UseOnlyNumbers = False
        Me.txtLoteEntrada.UseUpperCase = True
        Me.txtLoteEntrada.Validar = True
        Me.txtLoteEntrada.ValueMember = "codigo"
        Me.txtLoteEntrada.ValueText = ""
        '
        'cboDisenhoEtiqueta
        '
        Me.TableLayoutPanel1.SetColumnSpan(Me.cboDisenhoEtiqueta, 2)
        Me.cboDisenhoEtiqueta.controlarBotonBorrar = True
        Me.cboDisenhoEtiqueta.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cboDisenhoEtiqueta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDisenhoEtiqueta.DropDownWidth = 131
        Me.cboDisenhoEtiqueta.Location = New System.Drawing.Point(92, 57)
        Me.cboDisenhoEtiqueta.mostrarSiempreBotonBorrar = False
        Me.cboDisenhoEtiqueta.Name = "cboDisenhoEtiqueta"
        Me.cboDisenhoEtiqueta.Size = New System.Drawing.Size(372, 21)
        Me.cboDisenhoEtiqueta.TabIndex = 13
        '
        'epErrores
        '
        Me.epErrores.Alineacion = System.Windows.Forms.ErrorIconAlignment.TopLeft
        Me.epErrores.ContainerControl = Me
        '
        'frmSeleccionarExpedidorLoteEntrada
        '
        Me.AcceptButton = Me.btnAceptar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.CancelButton = Me.btnCancelar
        Me.ClientSize = New System.Drawing.Size(467, 124)
        Me.ControlBox = False
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSeleccionarExpedidorLoteEntrada"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Establecer parámetros"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        CType(Me.cboExpedidor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboDisenhoEtiqueta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.epErrores, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnCancelar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents btnAceptar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents epErrores As Quadralia.Controles.Validacion.GestorErrores
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblExpedidor As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents cboExpedidor As Escritorio.Quadralia.Controles.aComboBox
    Friend WithEvents lblLineaEntrada As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtLoteEntrada As Escritorio.cBuscadorLoteEntrada
    Friend WithEvents lblDisenhoEtiqueta As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents cboDisenhoEtiqueta As Escritorio.Quadralia.Controles.aComboBox
End Class
