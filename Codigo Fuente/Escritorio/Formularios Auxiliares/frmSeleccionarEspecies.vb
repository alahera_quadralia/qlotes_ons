﻿Public Class frmSeleccionarEspecies
    ''' <summary>
    ''' Listado de familias seleccionadas para devolver al formulario que las solicita
    ''' </summary>    
    Public Property Items As New List(Of Especie)
    Private Property Exclusiones As List(Of Especie)
    Private Property MultipleSeleccion As Boolean
    Private Property Contexto As Entidades

#Region " LIMPIEZA "
    ''' <summary>
    ''' Limpia todos los controles del formulario
    ''' </summary>
    Private Sub LimpiarTodo()
        LimpiarBusqueda()
        LimpiarDatos()
    End Sub

    ''' <summary>
    ''' Limpia todos los controles
    ''' </summary>
    Private Sub LimpiarDatos()
        dgvRegistros.Rows.Clear()
    End Sub

    ''' <summary>
    ''' Limpia los controles de los filtros de búsqueda
    ''' </summary>
    Private Sub LimpiarBusqueda()
        txtBNombre.Clear()

        cboSeleccionar.SelectedItem = Nothing
        cboSeleccionar.SelectedIndex = -1
    End Sub
#End Region
#Region " CARGAR / GUARDAR / CANCELAR "
    Private Sub PrepararDataGrid()
        dgvRegistros.AutoGenerateColumns = False

        If MultipleSeleccion Then
            Dim colSelect As New DataGridViewCheckBoxColumn
            With colSelect
                .HeaderText = "Sel."
                .DataPropertyName = Nothing
                .Name = Nothing
                .AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                .ReadOnly = False
            End With
            dgvRegistros.Columns.Insert(0, colSelect)
        End If
    End Sub

    Private Sub cargarRegistros() Handles btnBBuscar.Click
        ' Se eliminan todos los Especies que ya están añadidos y se aplican los filtros
        Dim Registros = (From esp As Especie In Contexto.Especies _
                         Where (String.IsNullOrEmpty(txtBNombre.Text) OrElse esp.denominacionComercial.ToUpper.Contains(txtBNombre.Text.ToUpper()) OrElse esp.denominacionComercial.ToUpper.Contains(txtBNombre.Text.ToUpper()) OrElse esp.alfa3.ToUpper.Contains(txtBNombre.Text.ToUpper())) _
                         Select esp).ToList.Except(Exclusiones)

        ' Se asigna el datasource para cargar los Especies
        Me.dgvRegistros.DataSource = Registros.ToList
        dgvRegistros.Focus()
    End Sub

    ''' <summary>
    ''' Añade la lista de elementos seleccionados y los devuelve
    ''' </summary>
    Private Sub Aceptar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Items = New List(Of Especie)

        If MultipleSeleccion Then
            For Each linea As DataGridViewRow In dgvRegistros.Rows
                If linea.Cells(0).Value = True Then
                    Items.Add(linea.DataBoundItem)
                End If
            Next
        ElseIf dgvRegistros.CurrentRow IsNot Nothing AndAlso dgvRegistros.CurrentRow.DataBoundItem IsNot Nothing Then
            Items.Add(dgvRegistros.CurrentRow.DataBoundItem)
        Else
            Items = New List(Of Especie)
        End If

        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    ''' <summary>
    ''' El usuario indica que no desea crear un nuevo contacto
    ''' </summary>
    Private Sub Cancelar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Items.Clear()
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub cboSeleccionar_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSeleccionar.SelectedIndexChanged
        Quadralia.WinForms.Selecciones.cSelecciones.marcarSeleccionados(dgvRegistros, cboSeleccionar.SelectedIndex, 0)
    End Sub
#End Region
#Region " RUTINA DE INICIO "
    ''' <summary>
    ''' Establecemos el foco por defecto
    ''' </summary>
    Public Sub Inicio(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        Me.ActiveControl = txtBNombre.TextBox
    End Sub

    Public Sub New(ByVal Contexto As Entidades, ByVal Exclusiones As List(Of Especie), Optional ByVal MultipleSeleccion As Boolean = False)
        ' Llamada necesaria para el Diseñador de Windows Forms.
        InitializeComponent()

        Quadralia.Formularios.AutoTabular(Me)

        ' Asignación de contextos
        Me.Contexto = Contexto
        Me.Exclusiones = Exclusiones
        Me.MultipleSeleccion = MultipleSeleccion

        ' Limpio los controles
        LimpiarTodo()

        ' Crea las columnas necesarias en el DataGrid
        PrepararDataGrid()

        If Not MultipleSeleccion Then
            lblSeleccionar.Visible = False
            cboSeleccionar.Visible = False
        End If

        ' Cargar opciones de seleccion
        Quadralia.WinForms.Selecciones.cSelecciones.anhadirOpcionesSeleccion(cboSeleccionar)

        ' Carga los artículos
        cargarRegistros()
    End Sub

    Public Sub New(ByVal Contexto As Entidades)
        Me.New(Contexto, New List(Of Especie), False)
    End Sub
#End Region
#Region " MANEJO DE CONTROLES "
    ''' <summary>
    ''' Ordena los resultados del DGV de Resultados    
    ''' </summary>
    Private Sub OrdenarResultados(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvRegistros.ColumnHeaderMouseClick
        Quadralia.Formularios.Funcionalidad.OrdenarDGV(Of Especie)(dgvRegistros, e)
    End Sub

    Private Sub DobleClickItem(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvRegistros.CellDoubleClick
        If dgvRegistros.SelectedRows.Count = 1 AndAlso MultipleSeleccion = False Then
            Aceptar(Nothing, Nothing)
        ElseIf dgvRegistros.SelectedRows.Count = 1 AndAlso MultipleSeleccion Then
            dgvRegistros.SelectedRows(0).Cells(0).Value = Not dgvRegistros.SelectedRows(0).Cells(0).Value
        End If
    End Sub

    ''' <summary>
    ''' Controlamos el enter para que vaya al botón de aceptar
    ''' </summary>
    Private Sub ControlEnter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvRegistros.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.Handled = True
            Aceptar(Nothing, Nothing)
        ElseIf e.KeyCode = Keys.Tab Then
            e.Handled = True
            Me.SelectNextControl(sender, True, True, True, True)
        ElseIf e.KeyCode = Keys.Space AndAlso MultipleSeleccion AndAlso dgvRegistros.CurrentRow.Index > -1 Then
            e.Handled = True
            DobleClickItem(Nothing, Nothing)
        End If
    End Sub
#End Region
#Region " FUNCIONES EXTRAS "
    ''' <summary>
    ''' Evita que salga el molesto error del DGV
    ''' </summary>
    Private Sub EvitarErrores(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvRegistros.DataError
#If Not Debug Then
        e.Cancel = True
#End If
    End Sub

    ''' <summary>
    ''' Localiza una entidad por su código y la devuelve para ser tratada
    ''' </summary>
    Public Shared Function BuscarPorCodigo(ByVal Contexto As Entidades, ByVal CodigoBusqueda As Long) As List(Of Especie)
        Dim Registros = (From esp As Especie In Contexto.Especies Where esp.alfa3 = CodigoBusqueda Select esp).ToList
        If Registros.Count > 0 Then
            Return Registros
        Else
            Return Nothing
        End If
    End Function
#End Region
#Region " RESTO DE FUNCIONES "
    ''' <summary>
    ''' Lanza la busqueda de registros al pulsar enter
    ''' </summary>
    Private Sub LanzarBusquedaEnEnter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtBNombre.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.Handled = True
            e.SuppressKeyPress = True
            cargarRegistros()
        End If
    End Sub
#End Region
End Class