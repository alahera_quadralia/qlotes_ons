﻿Public Class frmSeleccionarProveedor

    ''' <summary>
    ''' Listado de familias seleccionadas para devolver al formulario que las solicita
    ''' </summary>    
    Public Property Items As New List(Of Proveedor)
    Private Property Exclusiones As List(Of Proveedor)
    Private Property MultipleSeleccion As Boolean
    Private Property Contexto As Entidades

#Region " LIMPIEZA "
    ''' <summary>
    ''' Limpia todos los controles
    ''' </summary>
    Private Sub Limpiar()
        dgvProveedores.Rows.Clear()
    End Sub
#End Region
#Region " CARGAR / GUARDAR / CANCELAR "
    Private Sub PrepararDataGrid()
        dgvProveedores.Columns.Clear()
        dgvProveedores.AutoGenerateColumns = False

        If MultipleSeleccion Then
            Dim colSelect As DataGridViewCheckBoxColumn
            colSelect = New DataGridViewCheckBoxColumn
            CType(colSelect, DataGridViewCheckBoxColumn).HeaderText = "Sel."
            CType(colSelect, DataGridViewCheckBoxColumn).DataPropertyName = Nothing
            CType(colSelect, DataGridViewCheckBoxColumn).Name = Nothing
            CType(colSelect, DataGridViewCheckBoxColumn).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            CType(colSelect, DataGridViewCheckBoxColumn).ReadOnly = False
            dgvProveedores.Columns.Add(CType(colSelect, DataGridViewCheckBoxColumn))
        End If

        Dim colCodigo As DataGridViewTextBoxColumn
        colCodigo = New DataGridViewTextBoxColumn
        CType(colCodigo, DataGridViewTextBoxColumn).HeaderText = "Cód."
        CType(colCodigo, DataGridViewTextBoxColumn).Name = "Codigo"
        CType(colCodigo, DataGridViewTextBoxColumn).DataPropertyName = "codigo"
        CType(colCodigo, DataGridViewTextBoxColumn).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
        CType(colCodigo, DataGridViewTextBoxColumn).ReadOnly = True
        dgvProveedores.Columns.Add(CType(colCodigo, DataGridViewTextBoxColumn))

        Dim colCIF As DataGridViewTextBoxColumn
        colCIF = New DataGridViewTextBoxColumn
        CType(colCIF, DataGridViewTextBoxColumn).HeaderText = "CIF/VAT"
        CType(colCIF, DataGridViewTextBoxColumn).Name = "cif"
        CType(colCIF, DataGridViewTextBoxColumn).DataPropertyName = "cif"
        CType(colCIF, DataGridViewTextBoxColumn).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
        CType(colCIF, DataGridViewTextBoxColumn).ReadOnly = True
        dgvProveedores.Columns.Add(CType(colCIF, DataGridViewTextBoxColumn))

        Dim colNombre As DataGridViewTextBoxColumn
        colNombre = New DataGridViewTextBoxColumn
        CType(colNombre, DataGridViewTextBoxColumn).HeaderText = "Nombre"
        CType(colNombre, DataGridViewTextBoxColumn).Name = "razonSocial"
        CType(colNombre, DataGridViewTextBoxColumn).DataPropertyName = "razonSocial"
        CType(colNombre, DataGridViewTextBoxColumn).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        CType(colNombre, DataGridViewTextBoxColumn).MinimumWidth = 80
        CType(colNombre, DataGridViewTextBoxColumn).ReadOnly = True
        dgvProveedores.Columns.Add(CType(colNombre, DataGridViewTextBoxColumn))
    End Sub

    Private Sub cargarProveedores() Handles btnBBuscar.Click
        ' Se eliminan todos los clientes que ya están añadidos y se aplican los filtros
        Dim ListaArticulos = (From pro As Proveedor In Contexto.Proveedores _
                              Where ((pro.nombreComercial.Contains(txtBProveedorNombre.Text) OrElse String.IsNullOrEmpty(txtBProveedorNombre.Text)) _
                              Or (pro.razonSocial.Contains(txtBProveedorNombre.Text) OrElse String.IsNullOrEmpty(txtBProveedorNombre.Text))) _
                              And (pro.codigo.Contains(txtBProveedorCodigo.Text) OrElse String.IsNullOrEmpty(txtBProveedorCodigo.Text)) _
                              And (pro.cif.Contains(txtBProveedorCIF.Text) OrElse String.IsNullOrEmpty(txtBProveedorCIF.Text)) _
                              Select pro)

        ' Se asigna el datasource para cargar los clientes
        If Exclusiones IsNot Nothing AndAlso Exclusiones.Count > 0 Then
            Me.dgvProveedores.DataSource = ListaArticulos.Except(Exclusiones)
        Else
            Me.dgvProveedores.DataSource = ListaArticulos
        End If

        dgvProveedores.Focus()
    End Sub

    ''' <summary>
    ''' Añade la lista de elementos seleccionados y los devuelve
    ''' </summary>
    Private Sub Aceptar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If MultipleSeleccion Then
            For Each linea As DataGridViewRow In dgvProveedores.Rows
                If linea.Cells(0).Value = True Then
                    Items.Add(linea.DataBoundItem)
                End If
            Next
        ElseIf dgvProveedores.CurrentRow IsNot Nothing AndAlso dgvProveedores.CurrentRow.DataBoundItem IsNot Nothing Then
            Items.Add(dgvProveedores.CurrentRow.DataBoundItem)
        Else
            Items = New List(Of Proveedor)
        End If

        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    ''' <summary>
    ''' El usuario indica que no desea crear un nuevo contacto
    ''' </summary>
    Private Sub Cancelar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Items.Clear()
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub cboSeleccionar_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSeleccionar.SelectedIndexChanged
        Quadralia.WinForms.Selecciones.cSelecciones.marcarSeleccionados(dgvProveedores, cboSeleccionar.SelectedIndex, 0)
    End Sub
#End Region
#Region " RUTINA DE INICIO "
    Public Sub Inicio() Handles Me.Shown
        ' Situo el foco en el dgv
        dgvProveedores.Focus()
    End Sub

    Public Sub New(ByVal Contexto As Entidades, _
                   ByVal Exclusiones As List(Of Proveedor), _
                   Optional ByVal MultipleSeleccion As Boolean = False)
        ' Llamada necesaria para el Diseñador de Windows Forms.
        InitializeComponent()

        Quadralia.Formularios.AutoTabular(Me)

        ' Asignación de contextos
        Me.Contexto = Contexto
        Me.Exclusiones = Exclusiones
        Me.MultipleSeleccion = MultipleSeleccion

        ' Limpio los controles
        Limpiar()

        ' Crea las columnas necesarias en el DataGrid
        PrepararDataGrid()

        If Not MultipleSeleccion Then
            lblSeleccionar.Visible = False
            cboSeleccionar.Visible = False
        End If

        ' Cargar opciones de seleccion
        Quadralia.WinForms.Selecciones.cSelecciones.anhadirOpcionesSeleccion(cboSeleccionar)

        ' Carga los proveedores
        cargarProveedores()

        For Each UnControl As Control In tblFiltrosTarifas.Controls
            Quadralia.Formularios.Funcionalidad.ManejadorTabPorIntro(UnControl)
        Next
    End Sub

    Public Sub New(ByVal Contexto As Entidades)
        Me.New(Contexto, New List(Of Proveedor), False)
    End Sub
#End Region
#Region " MANEJO DE CONTROLES "
    ''' <summary>
    ''' Ordena los resultados del DGV de Resultados    
    ''' </summary>
    Private Sub OrdenarResultados(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvProveedores.ColumnHeaderMouseClick
        Quadralia.Formularios.Funcionalidad.OrdenarDGV(Of Proveedor)(dgvProveedores, e)
    End Sub

    Private Sub DobleClickItem(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvProveedores.DoubleClick
        If dgvProveedores.SelectedRows.Count = 1 AndAlso MultipleSeleccion = False Then
            Aceptar(Nothing, Nothing)
        ElseIf dgvProveedores.SelectedRows.Count = 1 AndAlso MultipleSeleccion Then
            dgvProveedores.SelectedRows(0).Cells(0).Value = Not dgvProveedores.SelectedRows(0).Cells(0).Value
        End If
    End Sub

    ''' <summary>
    ''' Controlamos el enter para que vaya al botón de aceptar
    ''' </summary>
    Private Sub ControlEnter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvProveedores.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.Handled = True
            Aceptar(Nothing, Nothing)
        ElseIf e.KeyCode = Keys.Tab Then
            e.Handled = True
            Me.SelectNextControl(sender, True, True, True, True)
        ElseIf e.KeyCode = Keys.Space AndAlso MultipleSeleccion AndAlso dgvProveedores.CurrentRow.Index > -1 Then
            e.Handled = True
            DobleClickItem(Nothing, Nothing)
        End If
    End Sub
#End Region
#Region " FUNCIONES EXTRAS "
    ''' <summary>
    ''' Localiza una entidad por su código y la devuelve para ser tratada
    ''' </summary>
    Public Shared Function BuscarPorCodigo(ByVal Contexto As Entidades,
                                           ByVal CodigoBusqueda As Long) As List(Of Proveedor)
        Dim ListaProveedores = (From pro As Proveedor In Contexto.Proveedores
                                Where pro.id = CodigoBusqueda
                                Select pro).ToList
        If ListaProveedores.Count > 0 Then
            Return ListaProveedores
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function ObetenerCodigos(ByVal Contexto As Entidades) As String()
        Return (From It As Proveedor In Contexto.Proveedores Order By It.codigo Select It.codigo).ToList.ToArray
    End Function

    ''' <summary>
    ''' Evita que salga el molesto error del DGV
    ''' </summary>
    Private Sub EvitarErrores(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvProveedores.DataError
        e.Cancel = True
    End Sub
#End Region
#Region " RESTO DE FUNCIONES "
    ''' <summary>
    ''' Lanza la busqueda de registros al pulsar enter
    ''' </summary>
    Private Sub LanzarBusquedaEnEnter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtBProveedorNombre.KeyDown, txtBProveedorCodigo.KeyDown, txtBProveedorCIF.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.Handled = True
            e.SuppressKeyPress = True
            cargarProveedores()
        End If
    End Sub
#End Region
End Class