﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSeleccionarProveedor
    Inherits ComponentFactory.Krypton.Toolkit.KryptonForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSeleccionarProveedor))
        Me.hdrContenido = New ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup()
        Me.splTarifas = New System.Windows.Forms.SplitContainer()
        Me.hdrTarifas = New ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup()
        Me.btnBuscarArticulo = New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup()
        Me.tblFiltrosTarifas = New System.Windows.Forms.TableLayoutPanel()
        Me.KryptonLabel2 = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.KryptonLabel1 = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtBProveedorCodigo = New Escritorio.Quadralia.Controles.aTextBox()
        Me.KryptonLabel3 = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtBProveedorCIF = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtBProveedorNombre = New Escritorio.Quadralia.Controles.aTextBox()
        Me.tblResultados = New System.Windows.Forms.TableLayoutPanel()
        Me.cboSeleccionar = New Escritorio.Quadralia.Controles.aComboBox()
        Me.grpResultadoProveedores = New ComponentFactory.Krypton.Toolkit.KryptonGroupBox()
        Me.dgvProveedores = New ComponentFactory.Krypton.Toolkit.KryptonDataGridView()
        Me.lblSeleccionar = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.btnBBuscar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.pCuerpo = New ComponentFactory.Krypton.Toolkit.KryptonPanel()
        Me.pBotonera = New ComponentFactory.Krypton.Toolkit.KryptonPanel()
        Me.btnAceptar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.btnCerrar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        CType(Me.hdrContenido, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.hdrContenido.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.hdrContenido.Panel.SuspendLayout()
        Me.hdrContenido.SuspendLayout()
        CType(Me.splTarifas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.splTarifas.Panel1.SuspendLayout()
        Me.splTarifas.Panel2.SuspendLayout()
        Me.splTarifas.SuspendLayout()
        CType(Me.hdrTarifas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.hdrTarifas.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.hdrTarifas.Panel.SuspendLayout()
        Me.hdrTarifas.SuspendLayout()
        Me.tblFiltrosTarifas.SuspendLayout()
        Me.tblResultados.SuspendLayout()
        CType(Me.cboSeleccionar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpResultadoProveedores, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpResultadoProveedores.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpResultadoProveedores.Panel.SuspendLayout()
        Me.grpResultadoProveedores.SuspendLayout()
        CType(Me.dgvProveedores, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pCuerpo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pCuerpo.SuspendLayout()
        CType(Me.pBotonera, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pBotonera.SuspendLayout()
        Me.SuspendLayout()
        '
        'hdrContenido
        '
        resources.ApplyResources(Me.hdrContenido, "hdrContenido")
        Me.hdrContenido.HeaderVisibleSecondary = False
        Me.hdrContenido.Name = "hdrContenido"
        '
        'hdrContenido.Panel
        '
        Me.hdrContenido.Panel.Controls.Add(Me.splTarifas)
        Me.hdrContenido.ValuesPrimary.Heading = resources.GetString("hdrContenido.ValuesPrimary.Heading")
        Me.hdrContenido.ValuesPrimary.Image = CType(resources.GetObject("hdrContenido.ValuesPrimary.Image"), System.Drawing.Image)
        '
        'splTarifas
        '
        Me.splTarifas.BackColor = System.Drawing.Color.Transparent
        resources.ApplyResources(Me.splTarifas, "splTarifas")
        Me.splTarifas.Name = "splTarifas"
        '
        'splTarifas.Panel1
        '
        Me.splTarifas.Panel1.Controls.Add(Me.hdrTarifas)
        '
        'splTarifas.Panel2
        '
        Me.splTarifas.Panel2.Controls.Add(Me.tblResultados)
        '
        'hdrTarifas
        '
        Me.hdrTarifas.ButtonSpecs.AddRange(New ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup() {Me.btnBuscarArticulo})
        resources.ApplyResources(Me.hdrTarifas, "hdrTarifas")
        Me.hdrTarifas.HeaderStylePrimary = ComponentFactory.Krypton.Toolkit.HeaderStyle.Secondary
        Me.hdrTarifas.HeaderVisibleSecondary = False
        Me.hdrTarifas.Name = "hdrTarifas"
        '
        'hdrTarifas.Panel
        '
        Me.hdrTarifas.Panel.Controls.Add(Me.tblFiltrosTarifas)
        Me.ToolTip1.SetToolTip(Me.hdrTarifas, resources.GetString("hdrTarifas.ToolTip"))
        Me.hdrTarifas.ValuesPrimary.Heading = resources.GetString("hdrTarifas.ValuesPrimary.Heading")
        Me.hdrTarifas.ValuesPrimary.Image = CType(resources.GetObject("hdrTarifas.ValuesPrimary.Image"), System.Drawing.Image)
        '
        'btnBuscarArticulo
        '
        resources.ApplyResources(Me.btnBuscarArticulo, "btnBuscarArticulo")
        Me.btnBuscarArticulo.Image = Global.Escritorio.My.Resources.Resources.Buscar_16
        Me.btnBuscarArticulo.UniqueName = "EBEB696EFDBA4F8E069BDADE94106330"
        '
        'tblFiltrosTarifas
        '
        Me.tblFiltrosTarifas.BackColor = System.Drawing.Color.Transparent
        resources.ApplyResources(Me.tblFiltrosTarifas, "tblFiltrosTarifas")
        Me.tblFiltrosTarifas.Controls.Add(Me.KryptonLabel2, 0, 1)
        Me.tblFiltrosTarifas.Controls.Add(Me.KryptonLabel1, 0, 0)
        Me.tblFiltrosTarifas.Controls.Add(Me.txtBProveedorCodigo, 1, 0)
        Me.tblFiltrosTarifas.Controls.Add(Me.KryptonLabel3, 2, 0)
        Me.tblFiltrosTarifas.Controls.Add(Me.txtBProveedorCIF, 3, 0)
        Me.tblFiltrosTarifas.Controls.Add(Me.txtBProveedorNombre, 1, 1)
        Me.tblFiltrosTarifas.Name = "tblFiltrosTarifas"
        '
        'KryptonLabel2
        '
        resources.ApplyResources(Me.KryptonLabel2, "KryptonLabel2")
        Me.KryptonLabel2.Name = "KryptonLabel2"
        Me.KryptonLabel2.Values.Text = resources.GetString("KryptonLabel2.Values.Text")
        '
        'KryptonLabel1
        '
        resources.ApplyResources(Me.KryptonLabel1, "KryptonLabel1")
        Me.KryptonLabel1.Name = "KryptonLabel1"
        Me.KryptonLabel1.Values.Text = resources.GetString("KryptonLabel1.Values.Text")
        '
        'txtBProveedorCodigo
        '
        Me.txtBProveedorCodigo.AlwaysActive = False
        Me.txtBProveedorCodigo.controlarBotonBorrar = True
        resources.ApplyResources(Me.txtBProveedorCodigo, "txtBProveedorCodigo")
        Me.txtBProveedorCodigo.Formato = ""
        Me.txtBProveedorCodigo.mostrarSiempreBotonBorrar = False
        Me.txtBProveedorCodigo.Name = "txtBProveedorCodigo"
        Me.txtBProveedorCodigo.seleccionarTodo = True
        '
        'KryptonLabel3
        '
        resources.ApplyResources(Me.KryptonLabel3, "KryptonLabel3")
        Me.KryptonLabel3.Name = "KryptonLabel3"
        Me.KryptonLabel3.Values.Text = resources.GetString("KryptonLabel3.Values.Text")
        '
        'txtBProveedorCIF
        '
        Me.txtBProveedorCIF.AlwaysActive = False
        Me.txtBProveedorCIF.controlarBotonBorrar = True
        resources.ApplyResources(Me.txtBProveedorCIF, "txtBProveedorCIF")
        Me.txtBProveedorCIF.Formato = ""
        Me.txtBProveedorCIF.mostrarSiempreBotonBorrar = False
        Me.txtBProveedorCIF.Name = "txtBProveedorCIF"
        Me.txtBProveedorCIF.seleccionarTodo = True
        '
        'txtBProveedorNombre
        '
        Me.txtBProveedorNombre.AlwaysActive = False
        Me.tblFiltrosTarifas.SetColumnSpan(Me.txtBProveedorNombre, 3)
        Me.txtBProveedorNombre.controlarBotonBorrar = True
        resources.ApplyResources(Me.txtBProveedorNombre, "txtBProveedorNombre")
        Me.txtBProveedorNombre.Formato = ""
        Me.txtBProveedorNombre.mostrarSiempreBotonBorrar = False
        Me.txtBProveedorNombre.Name = "txtBProveedorNombre"
        Me.txtBProveedorNombre.seleccionarTodo = True
        '
        'tblResultados
        '
        resources.ApplyResources(Me.tblResultados, "tblResultados")
        Me.tblResultados.Controls.Add(Me.cboSeleccionar, 1, 0)
        Me.tblResultados.Controls.Add(Me.grpResultadoProveedores, 0, 1)
        Me.tblResultados.Controls.Add(Me.lblSeleccionar, 0, 0)
        Me.tblResultados.Controls.Add(Me.btnBBuscar, 2, 0)
        Me.tblResultados.Name = "tblResultados"
        '
        'cboSeleccionar
        '
        resources.ApplyResources(Me.cboSeleccionar, "cboSeleccionar")
        Me.cboSeleccionar.controlarBotonBorrar = False
        Me.cboSeleccionar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSeleccionar.DropDownWidth = 352
        Me.cboSeleccionar.mostrarSiempreBotonBorrar = False
        Me.cboSeleccionar.Name = "cboSeleccionar"
        Me.ToolTip1.SetToolTip(Me.cboSeleccionar, resources.GetString("cboSeleccionar.ToolTip"))
        '
        'grpResultadoProveedores
        '
        Me.tblResultados.SetColumnSpan(Me.grpResultadoProveedores, 3)
        resources.ApplyResources(Me.grpResultadoProveedores, "grpResultadoProveedores")
        Me.grpResultadoProveedores.Name = "grpResultadoProveedores"
        '
        'grpResultadoProveedores.Panel
        '
        Me.grpResultadoProveedores.Panel.Controls.Add(Me.dgvProveedores)
        Me.grpResultadoProveedores.Values.Heading = resources.GetString("grpResultadoProveedores.Values.Heading")
        '
        'dgvProveedores
        '
        Me.dgvProveedores.AllowUserToAddRows = False
        Me.dgvProveedores.AllowUserToDeleteRows = False
        Me.dgvProveedores.AllowUserToResizeRows = False
        resources.ApplyResources(Me.dgvProveedores, "dgvProveedores")
        Me.dgvProveedores.MultiSelect = False
        Me.dgvProveedores.Name = "dgvProveedores"
        Me.dgvProveedores.RowHeadersVisible = False
        Me.dgvProveedores.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        '
        'lblSeleccionar
        '
        resources.ApplyResources(Me.lblSeleccionar, "lblSeleccionar")
        Me.lblSeleccionar.Name = "lblSeleccionar"
        Me.lblSeleccionar.Values.Text = resources.GetString("lblSeleccionar.Values.Text")
        '
        'btnBBuscar
        '
        resources.ApplyResources(Me.btnBBuscar, "btnBBuscar")
        Me.btnBBuscar.Name = "btnBBuscar"
        Me.ToolTip1.SetToolTip(Me.btnBBuscar, resources.GetString("btnBBuscar.ToolTip"))
        Me.btnBBuscar.Values.Image = CType(resources.GetObject("btnBBuscar.Values.Image"), System.Drawing.Image)
        Me.btnBBuscar.Values.Text = resources.GetString("btnBBuscar.Values.Text")
        '
        'pCuerpo
        '
        Me.pCuerpo.Controls.Add(Me.hdrContenido)
        Me.pCuerpo.Controls.Add(Me.pBotonera)
        resources.ApplyResources(Me.pCuerpo, "pCuerpo")
        Me.pCuerpo.Name = "pCuerpo"
        '
        'pBotonera
        '
        Me.pBotonera.Controls.Add(Me.btnAceptar)
        Me.pBotonera.Controls.Add(Me.btnCerrar)
        resources.ApplyResources(Me.pBotonera, "pBotonera")
        Me.pBotonera.Name = "pBotonera"
        '
        'btnAceptar
        '
        resources.ApplyResources(Me.btnAceptar, "btnAceptar")
        Me.btnAceptar.Name = "btnAceptar"
        Me.ToolTip1.SetToolTip(Me.btnAceptar, resources.GetString("btnAceptar.ToolTip"))
        Me.btnAceptar.Values.Text = resources.GetString("btnAceptar.Values.Text")
        '
        'btnCerrar
        '
        resources.ApplyResources(Me.btnCerrar, "btnCerrar")
        Me.btnCerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCerrar.Name = "btnCerrar"
        Me.ToolTip1.SetToolTip(Me.btnCerrar, resources.GetString("btnCerrar.ToolTip"))
        Me.btnCerrar.Values.Text = resources.GetString("btnCerrar.Values.Text")
        '
        'frmSeleccionarProveedor
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCerrar
        Me.ControlBox = False
        Me.Controls.Add(Me.pCuerpo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.Name = "frmSeleccionarProveedor"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        CType(Me.hdrContenido.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrContenido.Panel.ResumeLayout(False)
        CType(Me.hdrContenido, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrContenido.ResumeLayout(False)
        Me.splTarifas.Panel1.ResumeLayout(False)
        Me.splTarifas.Panel2.ResumeLayout(False)
        CType(Me.splTarifas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.splTarifas.ResumeLayout(False)
        CType(Me.hdrTarifas.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrTarifas.Panel.ResumeLayout(False)
        CType(Me.hdrTarifas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrTarifas.ResumeLayout(False)
        Me.tblFiltrosTarifas.ResumeLayout(False)
        Me.tblFiltrosTarifas.PerformLayout()
        Me.tblResultados.ResumeLayout(False)
        Me.tblResultados.PerformLayout()
        CType(Me.cboSeleccionar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grpResultadoProveedores.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpResultadoProveedores.Panel.ResumeLayout(False)
        CType(Me.grpResultadoProveedores, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpResultadoProveedores.ResumeLayout(False)
        CType(Me.dgvProveedores, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pCuerpo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pCuerpo.ResumeLayout(False)
        CType(Me.pBotonera, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pBotonera.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pCuerpo As ComponentFactory.Krypton.Toolkit.KryptonPanel
    Friend WithEvents hdrContenido As ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup
    Friend WithEvents pBotonera As ComponentFactory.Krypton.Toolkit.KryptonPanel
    Friend WithEvents btnCerrar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents btnAceptar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents splTarifas As System.Windows.Forms.SplitContainer
    Friend WithEvents hdrTarifas As ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup
    Friend WithEvents btnBuscarArticulo As ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup
    Friend WithEvents tblFiltrosTarifas As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents KryptonLabel2 As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents KryptonLabel1 As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtBProveedorCodigo As Quadralia.Controles.aTextBox
    Friend WithEvents KryptonLabel3 As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtBProveedorCIF As Quadralia.Controles.aTextBox
    Friend WithEvents grpResultadoProveedores As ComponentFactory.Krypton.Toolkit.KryptonGroupBox
    Friend WithEvents dgvProveedores As ComponentFactory.Krypton.Toolkit.KryptonDataGridView
    Friend WithEvents tblResultados As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents cboSeleccionar As Quadralia.Controles.aComboBox
    Friend WithEvents lblSeleccionar As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtBProveedorNombre As Quadralia.Controles.aTextBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents btnBBuscar As ComponentFactory.Krypton.Toolkit.KryptonButton
End Class
