<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmExcepcion
    Inherits ComponentFactory.Krypton.Toolkit.KryptonForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmExcepcion))
        Me.tabGeneral = New ComponentFactory.Krypton.Navigator.KryptonNavigator()
        Me.tbpGeneral = New ComponentFactory.Krypton.Navigator.KryptonPage()
        Me.chkNotificar = New ComponentFactory.Krypton.Toolkit.KryptonCheckButton()
        Me.lblFecha = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtEmail = New Escritorio.Quadralia.Controles.aTextBox()
        Me.lblEmail = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtComentarios = New Escritorio.Quadralia.Controles.aTextBox()
        Me.lblComentarios = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtFecha = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtVersion = New Escritorio.Quadralia.Controles.aTextBox()
        Me.lblVersion = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtNombreAplicacion = New Escritorio.Quadralia.Controles.aTextBox()
        Me.lblNombreAplicacion = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtMensajeError = New Escritorio.Quadralia.Controles.aTextBox()
        Me.picImagen = New System.Windows.Forms.PictureBox()
        Me.tbpError = New ComponentFactory.Krypton.Navigator.KryptonPage()
        Me.txtPilaLllamadas = New Escritorio.Quadralia.Controles.aTextBox()
        Me.lblPilaLllamadas = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtOrigenExcepcion = New Escritorio.Quadralia.Controles.aTextBox()
        Me.lblOrigenExcepcion = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtTipoExcepcion = New Escritorio.Quadralia.Controles.aTextBox()
        Me.lblMensaje = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblTipoExcepcion = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtMensaje = New Escritorio.Quadralia.Controles.aTextBox()
        Me.tbpCapturaPantalla = New ComponentFactory.Krypton.Navigator.KryptonPage()
        Me.pCaptura = New ComponentFactory.Krypton.Toolkit.KryptonGroupBox()
        Me.picCaptura = New System.Windows.Forms.PictureBox()
        Me.chkAdjuntarCaptura = New ComponentFactory.Krypton.Toolkit.KryptonCheckButton()
        Me.pBotonera = New ComponentFactory.Krypton.Toolkit.KryptonPanel()
        Me.btnGuardarCopia = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.btnAceptar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        CType(Me.tabGeneral, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabGeneral.SuspendLayout()
        CType(Me.tbpGeneral, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpGeneral.SuspendLayout()
        CType(Me.picImagen, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbpError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpError.SuspendLayout()
        CType(Me.tbpCapturaPantalla, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpCapturaPantalla.SuspendLayout()
        CType(Me.pCaptura, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pCaptura.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pCaptura.Panel.SuspendLayout()
        Me.pCaptura.SuspendLayout()
        CType(Me.picCaptura, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pBotonera, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pBotonera.SuspendLayout()
        Me.SuspendLayout()
        '
        'tabGeneral
        '
        Me.tabGeneral.Button.CloseButtonDisplay = ComponentFactory.Krypton.Navigator.ButtonDisplay.Hide
        Me.tabGeneral.Button.ContextButtonDisplay = ComponentFactory.Krypton.Navigator.ButtonDisplay.Hide
        Me.tabGeneral.Button.NextButtonDisplay = ComponentFactory.Krypton.Navigator.ButtonDisplay.Hide
        Me.tabGeneral.Button.PreviousButtonDisplay = ComponentFactory.Krypton.Navigator.ButtonDisplay.Hide
        Me.tabGeneral.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabGeneral.Location = New System.Drawing.Point(0, 0)
        Me.tabGeneral.Name = "tabGeneral"
        Me.tabGeneral.Pages.AddRange(New ComponentFactory.Krypton.Navigator.KryptonPage() {Me.tbpGeneral, Me.tbpError, Me.tbpCapturaPantalla})
        Me.tabGeneral.SelectedIndex = 0
        Me.tabGeneral.Size = New System.Drawing.Size(711, 441)
        Me.tabGeneral.TabIndex = 0
        Me.tabGeneral.Text = "KryptonNavigator1"
        '
        'tbpGeneral
        '
        Me.tbpGeneral.AutoHiddenSlideSize = New System.Drawing.Size(200, 200)
        Me.tbpGeneral.Controls.Add(Me.chkNotificar)
        Me.tbpGeneral.Controls.Add(Me.lblFecha)
        Me.tbpGeneral.Controls.Add(Me.txtEmail)
        Me.tbpGeneral.Controls.Add(Me.lblEmail)
        Me.tbpGeneral.Controls.Add(Me.txtComentarios)
        Me.tbpGeneral.Controls.Add(Me.lblComentarios)
        Me.tbpGeneral.Controls.Add(Me.txtFecha)
        Me.tbpGeneral.Controls.Add(Me.txtVersion)
        Me.tbpGeneral.Controls.Add(Me.lblVersion)
        Me.tbpGeneral.Controls.Add(Me.txtNombreAplicacion)
        Me.tbpGeneral.Controls.Add(Me.lblNombreAplicacion)
        Me.tbpGeneral.Controls.Add(Me.txtMensajeError)
        Me.tbpGeneral.Controls.Add(Me.picImagen)
        Me.tbpGeneral.Flags = 65534
        Me.tbpGeneral.LastVisibleSet = True
        Me.tbpGeneral.MinimumSize = New System.Drawing.Size(50, 50)
        Me.tbpGeneral.Name = "tbpGeneral"
        Me.tbpGeneral.Size = New System.Drawing.Size(709, 414)
        Me.tbpGeneral.Text = "General"
        Me.tbpGeneral.ToolTipTitle = "Page ToolTip"
        Me.tbpGeneral.UniqueName = "BF2231BE71404D414F96A617B3009E57"
        '
        'chkNotificar
        '
        Me.chkNotificar.Checked = True
        Me.chkNotificar.Location = New System.Drawing.Point(11, 80)
        Me.chkNotificar.Name = "chkNotificar"
        Me.chkNotificar.Size = New System.Drawing.Size(93, 25)
        Me.chkNotificar.TabIndex = 23
        Me.chkNotificar.Values.Text = "Notificar"
        '
        'lblFecha
        '
        Me.lblFecha.Font = New System.Drawing.Font("Segoe UI", 13.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.lblFecha.Location = New System.Drawing.Point(8, 168)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(42, 20)
        Me.lblFecha.TabIndex = 22
        Me.lblFecha.Values.Text = "Fecha"
        '
        'txtEmail
        '
        Me.txtEmail.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtEmail.BackColor = System.Drawing.SystemColors.Info
        Me.txtEmail.controlarBotonBorrar = True
        Me.txtEmail.Formato = ""
        Me.txtEmail.Location = New System.Drawing.Point(110, 196)
        Me.txtEmail.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtEmail.mostrarSiempreBotonBorrar = False
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.seleccionarTodo = True
        Me.txtEmail.Size = New System.Drawing.Size(589, 20)
        Me.txtEmail.TabIndex = 21
        '
        'lblEmail
        '
        Me.lblEmail.Location = New System.Drawing.Point(8, 196)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(40, 20)
        Me.lblEmail.TabIndex = 20
        Me.lblEmail.Values.Text = "Email"
        '
        'txtComentarios
        '
        Me.txtComentarios.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtComentarios.BackColor = System.Drawing.SystemColors.Info
        Me.txtComentarios.controlarBotonBorrar = True
        Me.txtComentarios.Formato = ""
        Me.txtComentarios.Location = New System.Drawing.Point(11, 248)
        Me.txtComentarios.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtComentarios.mostrarSiempreBotonBorrar = False
        Me.txtComentarios.Multiline = True
        Me.txtComentarios.Name = "txtComentarios"
        Me.txtComentarios.seleccionarTodo = True
        Me.txtComentarios.Size = New System.Drawing.Size(688, 160)
        Me.txtComentarios.TabIndex = 19
        '
        'lblComentarios
        '
        Me.lblComentarios.Font = New System.Drawing.Font("Segoe UI", 13.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.lblComentarios.Location = New System.Drawing.Point(8, 227)
        Me.lblComentarios.Name = "lblComentarios"
        Me.lblComentarios.Size = New System.Drawing.Size(612, 20)
        Me.lblComentarios.TabIndex = 18
        Me.lblComentarios.Values.Text = "Puede incluir a continuaci�n, una nota aclaratoria acerca de lo que estaba realiz" & _
    "ando cuando fall� la aplicaci�n"
        '
        'txtFecha
        '
        Me.txtFecha.controlarBotonBorrar = False
        Me.txtFecha.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFecha.Enabled = False
        Me.txtFecha.Formato = ""
        Me.txtFecha.Location = New System.Drawing.Point(110, 168)
        Me.txtFecha.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtFecha.mostrarSiempreBotonBorrar = False
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.ReadOnly = True
        Me.txtFecha.seleccionarTodo = True
        Me.txtFecha.Size = New System.Drawing.Size(269, 20)
        Me.txtFecha.TabIndex = 17
        '
        'txtVersion
        '
        Me.txtVersion.controlarBotonBorrar = False
        Me.txtVersion.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtVersion.Enabled = False
        Me.txtVersion.Formato = ""
        Me.txtVersion.Location = New System.Drawing.Point(110, 140)
        Me.txtVersion.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtVersion.mostrarSiempreBotonBorrar = False
        Me.txtVersion.Name = "txtVersion"
        Me.txtVersion.ReadOnly = True
        Me.txtVersion.seleccionarTodo = True
        Me.txtVersion.Size = New System.Drawing.Size(269, 20)
        Me.txtVersion.TabIndex = 16
        '
        'lblVersion
        '
        Me.lblVersion.Font = New System.Drawing.Font("Segoe UI", 13.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.lblVersion.Location = New System.Drawing.Point(8, 140)
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.Size = New System.Drawing.Size(51, 20)
        Me.lblVersion.TabIndex = 15
        Me.lblVersion.Values.Text = "Versi�n"
        '
        'txtNombreAplicacion
        '
        Me.txtNombreAplicacion.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtNombreAplicacion.controlarBotonBorrar = False
        Me.txtNombreAplicacion.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtNombreAplicacion.Enabled = False
        Me.txtNombreAplicacion.Formato = ""
        Me.txtNombreAplicacion.Location = New System.Drawing.Point(110, 112)
        Me.txtNombreAplicacion.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtNombreAplicacion.mostrarSiempreBotonBorrar = False
        Me.txtNombreAplicacion.Name = "txtNombreAplicacion"
        Me.txtNombreAplicacion.ReadOnly = True
        Me.txtNombreAplicacion.seleccionarTodo = True
        Me.txtNombreAplicacion.Size = New System.Drawing.Size(589, 20)
        Me.txtNombreAplicacion.TabIndex = 14
        '
        'lblNombreAplicacion
        '
        Me.lblNombreAplicacion.Font = New System.Drawing.Font("Segoe UI", 13.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.lblNombreAplicacion.Location = New System.Drawing.Point(8, 112)
        Me.lblNombreAplicacion.Name = "lblNombreAplicacion"
        Me.lblNombreAplicacion.Size = New System.Drawing.Size(67, 20)
        Me.lblNombreAplicacion.TabIndex = 13
        Me.lblNombreAplicacion.Values.Text = "Aplicaci�n"
        '
        'txtMensajeError
        '
        Me.txtMensajeError.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtMensajeError.controlarBotonBorrar = False
        Me.txtMensajeError.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtMensajeError.Enabled = False
        Me.txtMensajeError.Formato = ""
        Me.txtMensajeError.Location = New System.Drawing.Point(110, 4)
        Me.txtMensajeError.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtMensajeError.mostrarSiempreBotonBorrar = False
        Me.txtMensajeError.Multiline = True
        Me.txtMensajeError.Name = "txtMensajeError"
        Me.txtMensajeError.ReadOnly = True
        Me.txtMensajeError.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtMensajeError.seleccionarTodo = True
        Me.txtMensajeError.Size = New System.Drawing.Size(589, 101)
        Me.txtMensajeError.TabIndex = 12
        '
        'picImagen
        '
        Me.picImagen.BackColor = System.Drawing.Color.White
        Me.picImagen.Image = Global.Escritorio.My.Resources.Resources.Aviso_64
        Me.picImagen.Location = New System.Drawing.Point(11, 4)
        Me.picImagen.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.picImagen.Name = "picImagen"
        Me.picImagen.Size = New System.Drawing.Size(93, 72)
        Me.picImagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.picImagen.TabIndex = 1
        Me.picImagen.TabStop = False
        '
        'tbpError
        '
        Me.tbpError.AutoHiddenSlideSize = New System.Drawing.Size(200, 200)
        Me.tbpError.Controls.Add(Me.txtPilaLllamadas)
        Me.tbpError.Controls.Add(Me.lblPilaLllamadas)
        Me.tbpError.Controls.Add(Me.txtOrigenExcepcion)
        Me.tbpError.Controls.Add(Me.lblOrigenExcepcion)
        Me.tbpError.Controls.Add(Me.txtTipoExcepcion)
        Me.tbpError.Controls.Add(Me.lblMensaje)
        Me.tbpError.Controls.Add(Me.lblTipoExcepcion)
        Me.tbpError.Controls.Add(Me.txtMensaje)
        Me.tbpError.Flags = 65534
        Me.tbpError.LastVisibleSet = True
        Me.tbpError.MinimumSize = New System.Drawing.Size(50, 50)
        Me.tbpError.Name = "tbpError"
        Me.tbpError.Size = New System.Drawing.Size(709, 414)
        Me.tbpError.Text = "Error"
        Me.tbpError.ToolTipTitle = "Page ToolTip"
        Me.tbpError.UniqueName = "CBDF3C479BF34EFF99B1B9EDBA7EF3A7"
        '
        'txtPilaLllamadas
        '
        Me.txtPilaLllamadas.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPilaLllamadas.controlarBotonBorrar = False
        Me.txtPilaLllamadas.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtPilaLllamadas.Enabled = False
        Me.txtPilaLllamadas.Formato = ""
        Me.txtPilaLllamadas.Location = New System.Drawing.Point(11, 248)
        Me.txtPilaLllamadas.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtPilaLllamadas.mostrarSiempreBotonBorrar = False
        Me.txtPilaLllamadas.Multiline = True
        Me.txtPilaLllamadas.Name = "txtPilaLllamadas"
        Me.txtPilaLllamadas.ReadOnly = True
        Me.txtPilaLllamadas.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtPilaLllamadas.seleccionarTodo = True
        Me.txtPilaLllamadas.Size = New System.Drawing.Size(687, 160)
        Me.txtPilaLllamadas.TabIndex = 22
        '
        'lblPilaLllamadas
        '
        Me.lblPilaLllamadas.Font = New System.Drawing.Font("Segoe UI", 13.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.lblPilaLllamadas.Location = New System.Drawing.Point(11, 228)
        Me.lblPilaLllamadas.Name = "lblPilaLllamadas"
        Me.lblPilaLllamadas.Size = New System.Drawing.Size(98, 20)
        Me.lblPilaLllamadas.TabIndex = 21
        Me.lblPilaLllamadas.Values.Text = "Pila de llamadas"
        '
        'txtOrigenExcepcion
        '
        Me.txtOrigenExcepcion.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtOrigenExcepcion.controlarBotonBorrar = False
        Me.txtOrigenExcepcion.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtOrigenExcepcion.Enabled = False
        Me.txtOrigenExcepcion.Formato = ""
        Me.txtOrigenExcepcion.Location = New System.Drawing.Point(11, 192)
        Me.txtOrigenExcepcion.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtOrigenExcepcion.mostrarSiempreBotonBorrar = False
        Me.txtOrigenExcepcion.Name = "txtOrigenExcepcion"
        Me.txtOrigenExcepcion.ReadOnly = True
        Me.txtOrigenExcepcion.seleccionarTodo = True
        Me.txtOrigenExcepcion.Size = New System.Drawing.Size(687, 20)
        Me.txtOrigenExcepcion.TabIndex = 19
        '
        'lblOrigenExcepcion
        '
        Me.lblOrigenExcepcion.Font = New System.Drawing.Font("Segoe UI", 13.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.lblOrigenExcepcion.Location = New System.Drawing.Point(11, 169)
        Me.lblOrigenExcepcion.Name = "lblOrigenExcepcion"
        Me.lblOrigenExcepcion.Size = New System.Drawing.Size(48, 20)
        Me.lblOrigenExcepcion.TabIndex = 18
        Me.lblOrigenExcepcion.Values.Text = "Origen"
        '
        'txtTipoExcepcion
        '
        Me.txtTipoExcepcion.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTipoExcepcion.controlarBotonBorrar = False
        Me.txtTipoExcepcion.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtTipoExcepcion.Enabled = False
        Me.txtTipoExcepcion.Formato = ""
        Me.txtTipoExcepcion.Location = New System.Drawing.Point(11, 31)
        Me.txtTipoExcepcion.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtTipoExcepcion.mostrarSiempreBotonBorrar = False
        Me.txtTipoExcepcion.Name = "txtTipoExcepcion"
        Me.txtTipoExcepcion.ReadOnly = True
        Me.txtTipoExcepcion.seleccionarTodo = True
        Me.txtTipoExcepcion.Size = New System.Drawing.Size(687, 20)
        Me.txtTipoExcepcion.TabIndex = 17
        '
        'lblMensaje
        '
        Me.lblMensaje.Font = New System.Drawing.Font("Segoe UI", 13.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.lblMensaje.Location = New System.Drawing.Point(11, 67)
        Me.lblMensaje.Name = "lblMensaje"
        Me.lblMensaje.Size = New System.Drawing.Size(56, 20)
        Me.lblMensaje.TabIndex = 16
        Me.lblMensaje.Values.Text = "Mensaje"
        '
        'lblTipoExcepcion
        '
        Me.lblTipoExcepcion.Font = New System.Drawing.Font("Segoe UI", 13.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.lblTipoExcepcion.Location = New System.Drawing.Point(11, 8)
        Me.lblTipoExcepcion.Name = "lblTipoExcepcion"
        Me.lblTipoExcepcion.Size = New System.Drawing.Size(34, 20)
        Me.lblTipoExcepcion.TabIndex = 16
        Me.lblTipoExcepcion.Values.Text = "Tipo"
        '
        'txtMensaje
        '
        Me.txtMensaje.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtMensaje.controlarBotonBorrar = False
        Me.txtMensaje.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtMensaje.Enabled = False
        Me.txtMensaje.Formato = ""
        Me.txtMensaje.Location = New System.Drawing.Point(11, 90)
        Me.txtMensaje.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtMensaje.mostrarSiempreBotonBorrar = False
        Me.txtMensaje.Multiline = True
        Me.txtMensaje.Name = "txtMensaje"
        Me.txtMensaje.ReadOnly = True
        Me.txtMensaje.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtMensaje.seleccionarTodo = True
        Me.txtMensaje.Size = New System.Drawing.Size(687, 63)
        Me.txtMensaje.TabIndex = 15
        '
        'tbpCapturaPantalla
        '
        Me.tbpCapturaPantalla.AutoHiddenSlideSize = New System.Drawing.Size(200, 200)
        Me.tbpCapturaPantalla.Controls.Add(Me.pCaptura)
        Me.tbpCapturaPantalla.Controls.Add(Me.chkAdjuntarCaptura)
        Me.tbpCapturaPantalla.Flags = 65534
        Me.tbpCapturaPantalla.LastVisibleSet = True
        Me.tbpCapturaPantalla.MinimumSize = New System.Drawing.Size(50, 50)
        Me.tbpCapturaPantalla.Name = "tbpCapturaPantalla"
        Me.tbpCapturaPantalla.Size = New System.Drawing.Size(709, 414)
        Me.tbpCapturaPantalla.Text = "Captura Pantalla"
        Me.tbpCapturaPantalla.ToolTipTitle = "Page ToolTip"
        Me.tbpCapturaPantalla.UniqueName = "CCAE20C4B6084C2E4081D00CA978590E"
        '
        'pCaptura
        '
        Me.pCaptura.Location = New System.Drawing.Point(11, 40)
        Me.pCaptura.Name = "pCaptura"
        '
        'pCaptura.Panel
        '
        Me.pCaptura.Panel.Controls.Add(Me.picCaptura)
        Me.pCaptura.Size = New System.Drawing.Size(688, 368)
        Me.pCaptura.TabIndex = 2
        Me.pCaptura.Text = "Captura"
        Me.pCaptura.Values.Heading = "Captura"
        '
        'picCaptura
        '
        Me.picCaptura.BackColor = System.Drawing.Color.Transparent
        Me.picCaptura.Dock = System.Windows.Forms.DockStyle.Fill
        Me.picCaptura.ErrorImage = Nothing
        Me.picCaptura.Location = New System.Drawing.Point(0, 0)
        Me.picCaptura.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.picCaptura.Name = "picCaptura"
        Me.picCaptura.Size = New System.Drawing.Size(684, 344)
        Me.picCaptura.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picCaptura.TabIndex = 2
        Me.picCaptura.TabStop = False
        '
        'chkAdjuntarCaptura
        '
        Me.chkAdjuntarCaptura.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkAdjuntarCaptura.Checked = True
        Me.chkAdjuntarCaptura.Location = New System.Drawing.Point(583, 9)
        Me.chkAdjuntarCaptura.Name = "chkAdjuntarCaptura"
        Me.chkAdjuntarCaptura.Size = New System.Drawing.Size(114, 25)
        Me.chkAdjuntarCaptura.TabIndex = 0
        Me.chkAdjuntarCaptura.Values.Text = "Adjuntar Captura"
        '
        'pBotonera
        '
        Me.pBotonera.Controls.Add(Me.btnGuardarCopia)
        Me.pBotonera.Controls.Add(Me.btnAceptar)
        Me.pBotonera.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pBotonera.Location = New System.Drawing.Point(0, 441)
        Me.pBotonera.Name = "pBotonera"
        Me.pBotonera.Size = New System.Drawing.Size(711, 70)
        Me.pBotonera.TabIndex = 1
        '
        'btnGuardarCopia
        '
        Me.btnGuardarCopia.Location = New System.Drawing.Point(3, 6)
        Me.btnGuardarCopia.Name = "btnGuardarCopia"
        Me.btnGuardarCopia.Size = New System.Drawing.Size(139, 56)
        Me.btnGuardarCopia.TabIndex = 9
        Me.btnGuardarCopia.Values.Image = Global.Escritorio.My.Resources.Resources.Guardar_32
        Me.btnGuardarCopia.Values.Text = "Guardar Copia"
        '
        'btnAceptar
        '
        Me.btnAceptar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAceptar.Location = New System.Drawing.Point(561, 6)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(139, 56)
        Me.btnAceptar.TabIndex = 8
        Me.btnAceptar.Values.Image = CType(resources.GetObject("btnAceptar.Values.Image"), System.Drawing.Image)
        Me.btnAceptar.Values.Text = "Aceptar"
        '
        'frmExcepcion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(711, 511)
        Me.ControlBox = False
        Me.Controls.Add(Me.tabGeneral)
        Me.Controls.Add(Me.pBotonera)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmExcepcion"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Error no controlado"
        Me.TopMost = True
        CType(Me.tabGeneral, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabGeneral.ResumeLayout(False)
        CType(Me.tbpGeneral, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpGeneral.ResumeLayout(False)
        Me.tbpGeneral.PerformLayout()
        CType(Me.picImagen, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbpError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpError.ResumeLayout(False)
        Me.tbpError.PerformLayout()
        CType(Me.tbpCapturaPantalla, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpCapturaPantalla.ResumeLayout(False)
        CType(Me.pCaptura.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pCaptura.Panel.ResumeLayout(False)
        CType(Me.pCaptura, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pCaptura.ResumeLayout(False)
        CType(Me.picCaptura, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pBotonera, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pBotonera.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
    Friend WithEvents tabGeneral As ComponentFactory.Krypton.Navigator.KryptonNavigator
    Friend WithEvents tbpGeneral As ComponentFactory.Krypton.Navigator.KryptonPage
    Friend WithEvents tbpError As ComponentFactory.Krypton.Navigator.KryptonPage
    Friend WithEvents pBotonera As ComponentFactory.Krypton.Toolkit.KryptonPanel
    Friend WithEvents btnGuardarCopia As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents btnAceptar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Private WithEvents lblFecha As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Private WithEvents txtEmail As Escritorio.Quadralia.Controles.aTextBox
    Private WithEvents lblEmail As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Private WithEvents txtComentarios As Escritorio.Quadralia.Controles.aTextBox
    Private WithEvents lblComentarios As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Private WithEvents txtFecha As Escritorio.Quadralia.Controles.aTextBox
    Private WithEvents txtVersion As Escritorio.Quadralia.Controles.aTextBox
    Private WithEvents lblVersion As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Private WithEvents txtNombreAplicacion As Escritorio.Quadralia.Controles.aTextBox
    Private WithEvents lblNombreAplicacion As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Private WithEvents txtMensajeError As Escritorio.Quadralia.Controles.aTextBox
    Private WithEvents picImagen As System.Windows.Forms.PictureBox
    Private WithEvents txtPilaLllamadas As Escritorio.Quadralia.Controles.aTextBox
    Private WithEvents lblPilaLllamadas As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Private WithEvents txtOrigenExcepcion As Escritorio.Quadralia.Controles.aTextBox
    Private WithEvents lblOrigenExcepcion As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Private WithEvents txtTipoExcepcion As Escritorio.Quadralia.Controles.aTextBox
    Private WithEvents lblMensaje As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Private WithEvents lblTipoExcepcion As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Private WithEvents txtMensaje As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents tbpCapturaPantalla As ComponentFactory.Krypton.Navigator.KryptonPage
    Friend WithEvents pCaptura As ComponentFactory.Krypton.Toolkit.KryptonGroupBox
    Private WithEvents picCaptura As System.Windows.Forms.PictureBox
    Friend WithEvents chkAdjuntarCaptura As ComponentFactory.Krypton.Toolkit.KryptonCheckButton
    Friend WithEvents chkNotificar As ComponentFactory.Krypton.Toolkit.KryptonCheckButton
End Class
