﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmNuevoAviso
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.tFade = New System.Windows.Forms.Timer(Me.components)
        Me.tTiempoAviso = New System.Windows.Forms.Timer(Me.components)
        Me.gFondo = New ComponentFactory.Krypton.Toolkit.KryptonGroupBox()
        Me.lblMensaje = New ComponentFactory.Krypton.Toolkit.KryptonWrapLabel()
        Me.picImagen = New System.Windows.Forms.PictureBox()
        Me.hdrTitulo = New ComponentFactory.Krypton.Toolkit.KryptonHeader()
        Me.btnCerrarAviso = New ComponentFactory.Krypton.Toolkit.ButtonSpecAny()
        Me.btnOpciones = New ComponentFactory.Krypton.Toolkit.ButtonSpecAny()
        Me.mnuOpcionesVentana = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuOcultarDurante = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuOcultarTipo = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuOcultarTodos = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuOcultarDurante15m = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuOcultarDurante30m = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuOcultarDurante1h = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuOcultarDurante4h = New System.Windows.Forms.ToolStripMenuItem()
        CType(Me.gFondo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gFondo.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gFondo.Panel.SuspendLayout()
        Me.gFondo.SuspendLayout()
        CType(Me.picImagen, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.mnuOpcionesVentana.SuspendLayout()
        Me.SuspendLayout()
        '
        'tFade
        '
        '
        'tTiempoAviso
        '
        Me.tTiempoAviso.Interval = 1000
        '
        'gFondo
        '
        Me.gFondo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gFondo.Location = New System.Drawing.Point(0, 0)
        Me.gFondo.Margin = New System.Windows.Forms.Padding(0)
        Me.gFondo.Name = "gFondo"
        '
        'gFondo.Panel
        '
        Me.gFondo.Panel.Controls.Add(Me.lblMensaje)
        Me.gFondo.Panel.Controls.Add(Me.picImagen)
        Me.gFondo.Panel.Controls.Add(Me.hdrTitulo)
        Me.gFondo.Size = New System.Drawing.Size(443, 113)
        Me.gFondo.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(CType(CType(65, Byte), Integer), CType(CType(65, Byte), Integer), CType(CType(65, Byte), Integer))
        Me.gFondo.StateCommon.Back.Color2 = System.Drawing.Color.Black
        Me.gFondo.StateCommon.Back.ColorStyle = ComponentFactory.Krypton.Toolkit.PaletteColorStyle.Linear33
        Me.gFondo.StateCommon.Border.Color1 = System.Drawing.Color.White
        Me.gFondo.StateCommon.Border.DrawBorders = CType((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top Or ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) _
                    Or ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) _
                    Or ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right), ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)
        Me.gFondo.StateCommon.Border.Width = 2
        Me.gFondo.TabIndex = 0
        Me.gFondo.Values.Heading = ""
        '
        'lblMensaje
        '
        Me.lblMensaje.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblMensaje.AutoSize = False
        Me.lblMensaje.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblMensaje.ForeColor = System.Drawing.Color.White
        Me.lblMensaje.Location = New System.Drawing.Point(79, 34)
        Me.lblMensaje.Name = "lblMensaje"
        Me.lblMensaje.Size = New System.Drawing.Size(349, 64)
        Me.lblMensaje.StateCommon.TextColor = System.Drawing.Color.White
        Me.lblMensaje.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'picImagen
        '
        Me.picImagen.BackColor = System.Drawing.Color.Transparent
        Me.picImagen.Image = Global.Escritorio.My.Resources.Resources.Aviso_64
        Me.picImagen.Location = New System.Drawing.Point(8, 34)
        Me.picImagen.Name = "picImagen"
        Me.picImagen.Size = New System.Drawing.Size(64, 64)
        Me.picImagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picImagen.TabIndex = 1
        Me.picImagen.TabStop = False
        '
        'hdrTitulo
        '
        Me.hdrTitulo.ButtonSpecs.AddRange(New ComponentFactory.Krypton.Toolkit.ButtonSpecAny() {Me.btnOpciones, Me.btnCerrarAviso})
        Me.hdrTitulo.Dock = System.Windows.Forms.DockStyle.Top
        Me.hdrTitulo.Location = New System.Drawing.Point(0, 0)
        Me.hdrTitulo.Name = "hdrTitulo"
        Me.hdrTitulo.Size = New System.Drawing.Size(437, 28)
        Me.hdrTitulo.StateCommon.Back.Color1 = System.Drawing.Color.Transparent
        Me.hdrTitulo.StateCommon.Back.Color2 = System.Drawing.Color.Transparent
        Me.hdrTitulo.StateCommon.Border.Color1 = System.Drawing.Color.White
        Me.hdrTitulo.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom
        Me.hdrTitulo.StateCommon.Border.Width = 1
        Me.hdrTitulo.StateCommon.Content.Padding = New System.Windows.Forms.Padding(10, -1, -1, -1)
        Me.hdrTitulo.StateCommon.Content.ShortText.Color1 = System.Drawing.Color.White
        Me.hdrTitulo.StateCommon.Content.ShortText.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.hdrTitulo.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center
        Me.hdrTitulo.TabIndex = 0
        Me.hdrTitulo.Values.Description = ""
        Me.hdrTitulo.Values.Heading = "Titulo Ventana"
        Me.hdrTitulo.Values.Image = Nothing
        '
        'btnCerrarAviso
        '
        Me.btnCerrarAviso.Type = ComponentFactory.Krypton.Toolkit.PaletteButtonSpecStyle.Close
        Me.btnCerrarAviso.UniqueName = "072E8E52D17442A1D78FC53527C66800"
        '
        'btnOpciones
        '
        Me.btnOpciones.ContextMenuStrip = Me.mnuOpcionesVentana
        Me.btnOpciones.Text = "Opciones"
        Me.btnOpciones.Type = ComponentFactory.Krypton.Toolkit.PaletteButtonSpecStyle.ArrowDown
        Me.btnOpciones.UniqueName = "8E2BB0D043C24A7B4DB88F74A478154D"
        '
        'mnuOpcionesVentana
        '
        Me.mnuOpcionesVentana.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.mnuOpcionesVentana.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuOcultarDurante, Me.mnuOcultarTipo, Me.mnuOcultarTodos})
        Me.mnuOpcionesVentana.Name = "mnuOpcionesVentana"
        Me.mnuOpcionesVentana.Size = New System.Drawing.Size(264, 70)
        '
        'mnuOcultarDurante
        '
        Me.mnuOcultarDurante.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuOcultarDurante15m, Me.mnuOcultarDurante30m, Me.mnuOcultarDurante1h, Me.mnuOcultarDurante4h})
        Me.mnuOcultarDurante.Name = "mnuOcultarDurante"
        Me.mnuOcultarDurante.Size = New System.Drawing.Size(263, 22)
        Me.mnuOcultarDurante.Text = "Ocultar avisos durante"
        '
        'mnuOcultarTipo
        '
        Me.mnuOcultarTipo.Name = "mnuOcultarTipo"
        Me.mnuOcultarTipo.Size = New System.Drawing.Size(263, 22)
        Me.mnuOcultarTipo.Text = "Ocultar todos los avisos de este tipo"
        '
        'mnuOcultarTodos
        '
        Me.mnuOcultarTodos.Name = "mnuOcultarTodos"
        Me.mnuOcultarTodos.Size = New System.Drawing.Size(263, 22)
        Me.mnuOcultarTodos.Text = "Ocultar todos los avisos"
        '
        'mnuOcultarDurante15m
        '
        Me.mnuOcultarDurante15m.Name = "mnuOcultarDurante15m"
        Me.mnuOcultarDurante15m.Size = New System.Drawing.Size(152, 22)
        Me.mnuOcultarDurante15m.Text = "15 minutos"
        '
        'mnuOcultarDurante30m
        '
        Me.mnuOcultarDurante30m.Name = "mnuOcultarDurante30m"
        Me.mnuOcultarDurante30m.Size = New System.Drawing.Size(152, 22)
        Me.mnuOcultarDurante30m.Text = "30 minutos"
        '
        'mnuOcultarDurante1h
        '
        Me.mnuOcultarDurante1h.Name = "mnuOcultarDurante1h"
        Me.mnuOcultarDurante1h.Size = New System.Drawing.Size(152, 22)
        Me.mnuOcultarDurante1h.Text = "1 hora"
        '
        'mnuOcultarDurante4h
        '
        Me.mnuOcultarDurante4h.Name = "mnuOcultarDurante4h"
        Me.mnuOcultarDurante4h.Size = New System.Drawing.Size(152, 22)
        Me.mnuOcultarDurante4h.Text = "4 horas"
        '
        'frmNuevoAviso
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(443, 113)
        Me.ControlBox = False
        Me.Controls.Add(Me.gFondo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmNuevoAviso"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.TransparencyKey = System.Drawing.Color.WhiteSmoke
        CType(Me.gFondo.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gFondo.Panel.ResumeLayout(False)
        Me.gFondo.Panel.PerformLayout()
        CType(Me.gFondo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gFondo.ResumeLayout(False)
        CType(Me.picImagen, System.ComponentModel.ISupportInitialize).EndInit()
        Me.mnuOpcionesVentana.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tFade As System.Windows.Forms.Timer
    Friend WithEvents tTiempoAviso As System.Windows.Forms.Timer
    Friend WithEvents gFondo As ComponentFactory.Krypton.Toolkit.KryptonGroupBox
    Friend WithEvents picImagen As System.Windows.Forms.PictureBox
    Friend WithEvents hdrTitulo As ComponentFactory.Krypton.Toolkit.KryptonHeader
    Friend WithEvents btnCerrarAviso As ComponentFactory.Krypton.Toolkit.ButtonSpecAny
    Friend WithEvents lblMensaje As ComponentFactory.Krypton.Toolkit.KryptonWrapLabel
    Friend WithEvents btnOpciones As ComponentFactory.Krypton.Toolkit.ButtonSpecAny
    Friend WithEvents mnuOpcionesVentana As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuOcultarDurante As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuOcultarDurante15m As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuOcultarDurante30m As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuOcultarDurante1h As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuOcultarDurante4h As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuOcultarTipo As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuOcultarTodos As System.Windows.Forms.ToolStripMenuItem
End Class
