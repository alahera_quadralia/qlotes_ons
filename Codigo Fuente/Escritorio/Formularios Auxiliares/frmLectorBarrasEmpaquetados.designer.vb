﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLectorBarrasEmpaquetados
    Inherits ComponentFactory.Krypton.Toolkit.KryptonForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLectorBarrasEmpaquetados))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.pCuerpo = New ComponentFactory.Krypton.Toolkit.KryptonPanel()
        Me.hdrContenido = New ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup()
        Me.hdrTarifas = New ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup()
        Me.tblBusqueda = New System.Windows.Forms.TableLayoutPanel()
        Me.dgvEmpaquetados = New ComponentFactory.Krypton.Toolkit.KryptonDataGridView()
        Me.lblBcodigo = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtCodigo = New Escritorio.Quadralia.Controles.aTextBox()
        Me.lblEstado = New ComponentFactory.Krypton.Toolkit.KryptonTextBox()
        Me.hdrBotones = New ComponentFactory.Krypton.Toolkit.KryptonHeader()
        Me.btnEmpaquetadoEliminar = New ComponentFactory.Krypton.Toolkit.ButtonSpecAny()
        Me.pBotonera = New ComponentFactory.Krypton.Toolkit.KryptonPanel()
        Me.btnAceptar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.btnCerrar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.colAlbEntrada = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colZonaFao = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colCantidad = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colNumEtiquetas = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        CType(Me.pCuerpo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pCuerpo.SuspendLayout()
        CType(Me.hdrContenido, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.hdrContenido.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.hdrContenido.Panel.SuspendLayout()
        Me.hdrContenido.SuspendLayout()
        CType(Me.hdrTarifas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.hdrTarifas.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.hdrTarifas.Panel.SuspendLayout()
        Me.hdrTarifas.SuspendLayout()
        Me.tblBusqueda.SuspendLayout()
        CType(Me.dgvEmpaquetados, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pBotonera, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pBotonera.SuspendLayout()
        Me.SuspendLayout()
        '
        'pCuerpo
        '
        Me.pCuerpo.Controls.Add(Me.hdrContenido)
        Me.pCuerpo.Controls.Add(Me.pBotonera)
        resources.ApplyResources(Me.pCuerpo, "pCuerpo")
        Me.pCuerpo.Name = "pCuerpo"
        '
        'hdrContenido
        '
        Me.hdrContenido.HeaderVisibleSecondary = False
        resources.ApplyResources(Me.hdrContenido, "hdrContenido")
        Me.hdrContenido.Name = "hdrContenido"
        '
        'hdrContenido.Panel
        '
        Me.hdrContenido.Panel.Controls.Add(Me.hdrTarifas)
        Me.hdrContenido.ValuesPrimary.Heading = resources.GetString("hdrContenido.ValuesPrimary.Heading")
        Me.hdrContenido.ValuesPrimary.Image = CType(resources.GetObject("hdrContenido.ValuesPrimary.Image"), System.Drawing.Image)
        '
        'hdrTarifas
        '
        resources.ApplyResources(Me.hdrTarifas, "hdrTarifas")
        Me.hdrTarifas.HeaderStylePrimary = ComponentFactory.Krypton.Toolkit.HeaderStyle.Secondary
        Me.hdrTarifas.HeaderVisibleSecondary = False
        Me.hdrTarifas.Name = "hdrTarifas"
        '
        'hdrTarifas.Panel
        '
        Me.hdrTarifas.Panel.Controls.Add(Me.tblBusqueda)
        Me.hdrTarifas.ValuesPrimary.Heading = resources.GetString("hdrTarifas.ValuesPrimary.Heading")
        Me.hdrTarifas.ValuesPrimary.Image = CType(resources.GetObject("hdrTarifas.ValuesPrimary.Image"), System.Drawing.Image)
        '
        'tblBusqueda
        '
        Me.tblBusqueda.BackColor = System.Drawing.Color.Transparent
        resources.ApplyResources(Me.tblBusqueda, "tblBusqueda")
        Me.tblBusqueda.Controls.Add(Me.dgvEmpaquetados, 0, 2)
        Me.tblBusqueda.Controls.Add(Me.lblBcodigo, 0, 0)
        Me.tblBusqueda.Controls.Add(Me.txtCodigo, 1, 0)
        Me.tblBusqueda.Controls.Add(Me.lblEstado, 0, 3)
        Me.tblBusqueda.Controls.Add(Me.hdrBotones, 0, 1)
        Me.tblBusqueda.Name = "tblBusqueda"
        '
        'dgvEmpaquetados
        '
        Me.dgvEmpaquetados.AllowUserToAddRows = False
        Me.dgvEmpaquetados.AllowUserToDeleteRows = False
        Me.dgvEmpaquetados.AllowUserToResizeRows = False
        Me.dgvEmpaquetados.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colAlbEntrada, Me.colZonaFao, Me.colCantidad, Me.colNumEtiquetas})
        Me.tblBusqueda.SetColumnSpan(Me.dgvEmpaquetados, 2)
        resources.ApplyResources(Me.dgvEmpaquetados, "dgvEmpaquetados")
        Me.dgvEmpaquetados.Name = "dgvEmpaquetados"
        Me.dgvEmpaquetados.RowHeadersVisible = False
        Me.dgvEmpaquetados.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        '
        'lblBcodigo
        '
        resources.ApplyResources(Me.lblBcodigo, "lblBcodigo")
        Me.lblBcodigo.Name = "lblBcodigo"
        Me.lblBcodigo.Values.Text = resources.GetString("lblBcodigo.Values.Text")
        '
        'txtCodigo
        '
        Me.txtCodigo.controlarBotonBorrar = True
        resources.ApplyResources(Me.txtCodigo, "txtCodigo")
        Me.txtCodigo.Formato = ""
        Me.txtCodigo.mostrarSiempreBotonBorrar = False
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.seleccionarTodo = True
        Me.txtCodigo.StateCommon.Content.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'lblEstado
        '
        Me.tblBusqueda.SetColumnSpan(Me.lblEstado, 2)
        resources.ApplyResources(Me.lblEstado, "lblEstado")
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.StateCommon.Content.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEstado.StateCommon.Content.Padding = New System.Windows.Forms.Padding(3)
        '
        'hdrBotones
        '
        Me.hdrBotones.ButtonSpecs.AddRange(New ComponentFactory.Krypton.Toolkit.ButtonSpecAny() {Me.btnEmpaquetadoEliminar})
        Me.tblBusqueda.SetColumnSpan(Me.hdrBotones, 2)
        resources.ApplyResources(Me.hdrBotones, "hdrBotones")
        Me.hdrBotones.HeaderStyle = ComponentFactory.Krypton.Toolkit.HeaderStyle.Secondary
        Me.hdrBotones.Name = "hdrBotones"
        Me.hdrBotones.Values.Description = resources.GetString("hdrBotones.Values.Description")
        Me.hdrBotones.Values.Heading = resources.GetString("hdrBotones.Values.Heading")
        Me.hdrBotones.Values.Image = CType(resources.GetObject("hdrBotones.Values.Image"), System.Drawing.Image)
        '
        'btnEmpaquetadoEliminar
        '
        Me.btnEmpaquetadoEliminar.Image = Global.Escritorio.My.Resources.Resources.Eliminar_16
        Me.btnEmpaquetadoEliminar.UniqueName = "D38FE964DF5A441A8F9AF7DF8D55396B"
        '
        'pBotonera
        '
        Me.pBotonera.Controls.Add(Me.btnAceptar)
        Me.pBotonera.Controls.Add(Me.btnCerrar)
        resources.ApplyResources(Me.pBotonera, "pBotonera")
        Me.pBotonera.Name = "pBotonera"
        '
        'btnAceptar
        '
        resources.ApplyResources(Me.btnAceptar, "btnAceptar")
        Me.btnAceptar.Name = "btnAceptar"
        Me.ToolTip1.SetToolTip(Me.btnAceptar, resources.GetString("btnAceptar.ToolTip"))
        Me.btnAceptar.Values.Text = resources.GetString("btnAceptar.Values.Text")
        '
        'btnCerrar
        '
        resources.ApplyResources(Me.btnCerrar, "btnCerrar")
        Me.btnCerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCerrar.Name = "btnCerrar"
        Me.ToolTip1.SetToolTip(Me.btnCerrar, resources.GetString("btnCerrar.ToolTip"))
        Me.btnCerrar.Values.Text = resources.GetString("btnCerrar.Values.Text")
        '
        'colAlbEntrada
        '
        Me.colAlbEntrada.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colAlbEntrada.DataPropertyName = "codigo"
        resources.ApplyResources(Me.colAlbEntrada, "colAlbEntrada")
        Me.colAlbEntrada.Name = "colAlbEntrada"
        Me.colAlbEntrada.ReadOnly = True
        '
        'colZonaFao
        '
        Me.colZonaFao.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colZonaFao.DataPropertyName = "fecha"
        DataGridViewCellStyle1.Format = "d"
        DataGridViewCellStyle1.NullValue = Nothing
        Me.colZonaFao.DefaultCellStyle = DataGridViewCellStyle1
        resources.ApplyResources(Me.colZonaFao, "colZonaFao")
        Me.colZonaFao.Name = "colZonaFao"
        Me.colZonaFao.ReadOnly = True
        '
        'colCantidad
        '
        Me.colCantidad.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colCantidad.DataPropertyName = "Peso"
        resources.ApplyResources(Me.colCantidad, "colCantidad")
        Me.colCantidad.Name = "colCantidad"
        Me.colCantidad.ReadOnly = True
        '
        'colNumEtiquetas
        '
        Me.colNumEtiquetas.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colNumEtiquetas.DataPropertyName = "NumeroEtiquetas"
        resources.ApplyResources(Me.colNumEtiquetas, "colNumEtiquetas")
        Me.colNumEtiquetas.Name = "colNumEtiquetas"
        '
        'frmLectorBarrasEmpaquetados
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCerrar
        Me.Controls.Add(Me.pCuerpo)
        Me.Name = "frmLectorBarrasEmpaquetados"
        Me.ShowIcon = false
        Me.ShowInTaskbar = false
        CType(Me.pCuerpo,System.ComponentModel.ISupportInitialize).EndInit
        Me.pCuerpo.ResumeLayout(false)
        CType(Me.hdrContenido.Panel,System.ComponentModel.ISupportInitialize).EndInit
        Me.hdrContenido.Panel.ResumeLayout(false)
        CType(Me.hdrContenido,System.ComponentModel.ISupportInitialize).EndInit
        Me.hdrContenido.ResumeLayout(false)
        CType(Me.hdrTarifas.Panel,System.ComponentModel.ISupportInitialize).EndInit
        Me.hdrTarifas.Panel.ResumeLayout(false)
        CType(Me.hdrTarifas,System.ComponentModel.ISupportInitialize).EndInit
        Me.hdrTarifas.ResumeLayout(false)
        Me.tblBusqueda.ResumeLayout(false)
        Me.tblBusqueda.PerformLayout
        CType(Me.dgvEmpaquetados,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.pBotonera,System.ComponentModel.ISupportInitialize).EndInit
        Me.pBotonera.ResumeLayout(false)
        Me.ResumeLayout(false)

End Sub
    Friend WithEvents pCuerpo As ComponentFactory.Krypton.Toolkit.KryptonPanel
    Friend WithEvents pBotonera As ComponentFactory.Krypton.Toolkit.KryptonPanel
    Friend WithEvents btnCerrar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents btnAceptar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents hdrContenido As ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup
    Friend WithEvents hdrTarifas As ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup
    Friend WithEvents tblBusqueda As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents dgvEmpaquetados As ComponentFactory.Krypton.Toolkit.KryptonDataGridView
    Friend WithEvents lblBcodigo As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtCodigo As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents lblEstado As ComponentFactory.Krypton.Toolkit.KryptonTextBox
    Friend WithEvents hdrBotones As ComponentFactory.Krypton.Toolkit.KryptonHeader
    Friend WithEvents btnEmpaquetadoEliminar As ComponentFactory.Krypton.Toolkit.ButtonSpecAny
    Friend WithEvents colAlbEntrada As KryptonDataGridViewTextBoxColumn
    Friend WithEvents colZonaFao As KryptonDataGridViewTextBoxColumn
    Friend WithEvents colCantidad As KryptonDataGridViewTextBoxColumn
    Friend WithEvents colNumEtiquetas As KryptonDataGridViewTextBoxColumn
End Class
