﻿Public Class frmDatosEtiquetaManual
#Region " DECLARACIONES "
    Private _Contexto As Entidades = Nothing
    Private _Expedidor As Expedidor = Nothing
    Private _Lote As LoteEntrada = Nothing
    Private _Disenho As String = String.Empty
    Private _Fecha As DateTime = DateTime.Now
    Private _Peso As Double = 0
#End Region
#Region " PROPIEDADES "
    Public Property Contexto As Entidades
        Get
            If _Contexto Is Nothing Then _Contexto = cDAL.Instancia.getContext()
            Return _Contexto
        End Get
        Set(value As Entidades)
            _Contexto = value
        End Set
    End Property

    ''' <summary>
    ''' Obtiene el expedidor mostrado por el formulario
    ''' </summary>
    Public Property Expedidor As Expedidor
        Get
            Return _Expedidor
        End Get
        Set(value As Expedidor)
            _Expedidor = value
            Cargar()
        End Set
    End Property

    ''' <summary>
    ''' Obtiene la linea del lote de entrada mostrada por el servidor
    ''' </summary>
    Public Property LoteEntrada As LoteEntrada
        Get
            Return _Lote
        End Get
        Set(value As LoteEntrada)
            _Lote = value
            Cargar()
        End Set
    End Property

    ''' <summary>
    ''' Obtiene/Establece el identificador del expedidor mostrado en el formulario
    ''' </summary>
    Public Property IdExpedidor As Integer
        Get
            If _Expedidor Is Nothing Then Return 0
            Return _Expedidor.id
        End Get
        Set(ByVal value As Integer)
            Expedidor = (From exp As Expedidor In Me.Contexto.Expedidores Where exp.id = value).FirstOrDefault
        End Set
    End Property

    ''' <summary>
    ''' Obtiene/Establece el identificador de la línea del lote de entrada mostrada en el formulario
    ''' </summary>
    Public Property IdLinea As Integer
        Get
            If _Lote Is Nothing Then Return 0
            Return _Lote.id
        End Get
        Set(ByVal value As Integer)
            LoteEntrada = (From lot As LoteEntrada In Me.Contexto.LotesEntrada Where lot.id = value).FirstOrDefault
        End Set
    End Property

    Public Property Fecha As DateTime
        Get
            Return _Fecha
        End Get
        Set(value As DateTime)
            _Fecha = value
            Cargar()
        End Set
    End Property

    Public Property Peso As Double
        Get
            Return _Peso
        End Get
        Set(value As Double)
            _Peso = value
            Cargar()
        End Set
    End Property

    Public Property Disenho As String
        Get
            Return _Disenho
        End Get
        Set(value As String)
            _Disenho = value
            Cargar()
        End Set
    End Property
#End Region
#Region " VALIDACIONES "
#End Region
#Region " LIMPIEZA "
    Public Sub Limpiar()
        txtPeso.Text = ""
        dtpFecha.Clear()
        cboExpedidor.Clear()
        txtLineaLoteEntrada.Clear()
        cboDisenho.Clear()

        epErrores.Clear()
    End Sub
#End Region
#Region " RUTINA DE INICIO "
    ''' <summary>
    ''' Muestra el formulario de acorde ha si existe conexión con la base de datos o no
    ''' </summary>
    Private Sub Inicio(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Quadralia.Formularios.AutoTabular(Me)

        CargarDatosMaestros()
        Limpiar()

        Cargar()
    End Sub

    ''' <summary>
    ''' Carga los datos maestros en el formulario
    ''' </summary>
    Private Sub CargarDatosMaestros()
        ' Cargar Expedidores
        With cboExpedidor
            .DataSource = Nothing
            .Items.Clear()

            .DisplayMember = "razonSocial"
            .ValueMember = "id"
            .DataSource = (From exp As Expedidor In Contexto.Expedidores Select exp)

            .Clear()
        End With

        ' Cargo los disenhos de etiquetas
        With cboDisenho
            .DataSource = Nothing
            .Items.Clear()

            .DisplayMember = "nombre"
            .ValueMember = "ruta"
            .DataSource = Quadralia.Aplicacion.GestorInformes.Informes("EtiquetasTrazabilidad").Values.ToList

            .Clear()
        End With
    End Sub
#End Region
#Region " CARGAR DATOS "
    Private Sub Cargar()
        Limpiar()

        txtPeso.Text = _Peso
        dtpFecha.ValueNullable = _Fecha
        cboExpedidor.SelectedItem = _Expedidor
        txtLineaLoteEntrada.Item = _Lote
        cboDisenho.SelectedValue = _Disenho
    End Sub
#End Region
#Region " ACEPTAR / CANCELAR "
    ''' <summary>
    ''' Lanza la rutina de validación de usuario
    ''' </summary>
    Private Sub Aceptar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        ' Validaciones
        Me.ValidateChildren()
        If epErrores.Count > 0 Then Exit Sub

        Try
            Dim Aux As Double = 0

            If Double.TryParse(txtPeso.Text, Aux) Then _Peso = Aux Else _Peso = 0
            If IsDBNull(dtpFecha.ValueNullable) Then _Fecha = DateTime.Now Else _Fecha = dtpFecha.Value
            _Expedidor = cboExpedidor.SelectedItem
            _Lote = txtLineaLoteEntrada.Item
            If cboDisenho.SelectedIndex = -1 Then _Disenho = String.Empty Else _Disenho = cboDisenho.SelectedValue

            Me.DialogResult = Windows.Forms.DialogResult.OK
            Me.Close()
        Catch ex As Exception
            Quadralia.Aplicacion.InformarError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' Cancela el inicio de sesión
    ''' </summary>
    Private Sub Cancelar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub
#End Region
#Region " VALIDACIONES "
    ''' <summary>
    ''' Obliga al usuario a introducir solo números en el textbox
    ''' </summary>
    Private Sub SoloDecimales(sender As Object, e As KeyPressEventArgs) Handles txtPeso.KeyPress
        Quadralia.Validadores.QuadraliaValidadores.SoloNumerosYDecimales(sender, e)
    End Sub
#End Region
End Class