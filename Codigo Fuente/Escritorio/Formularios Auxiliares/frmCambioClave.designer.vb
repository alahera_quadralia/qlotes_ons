﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCambioClave
    Inherits ComponentFactory.Krypton.Toolkit.KryptonForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCambioClave))
        Me.lblUsuario = New System.Windows.Forms.Label()
        Me.lblClave = New System.Windows.Forms.Label()
        Me.txtClave2 = New Escritorio.Quadralia.Controles.aTextBox()
        Me.btnCancelar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.btnAceptar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.txtClave1 = New Escritorio.Quadralia.Controles.aTextBox()
        Me.lblClaveActual = New System.Windows.Forms.Label()
        Me.txtClaveActual = New Escritorio.Quadralia.Controles.aTextBox()
        Me.epErrores = New Escritorio.Quadralia.Controles.Validacion.GestorErrores()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        CType(Me.epErrores, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblUsuario
        '
        Me.lblUsuario.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.lblUsuario.AutoSize = True
        Me.lblUsuario.Location = New System.Drawing.Point(6, 32)
        Me.lblUsuario.Name = "lblUsuario"
        Me.lblUsuario.Size = New System.Drawing.Size(96, 13)
        Me.lblUsuario.TabIndex = 2
        Me.lblUsuario.Text = "Nueva Contraseña"
        '
        'lblClave
        '
        Me.lblClave.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.lblClave.AutoSize = True
        Me.lblClave.Location = New System.Drawing.Point(7, 58)
        Me.lblClave.Name = "lblClave"
        Me.lblClave.Size = New System.Drawing.Size(95, 13)
        Me.lblClave.TabIndex = 4
        Me.lblClave.Text = "Repita Contraseña"
        '
        'txtClave2
        '
        Me.TableLayoutPanel1.SetColumnSpan(Me.txtClave2, 2)
        Me.txtClave2.controlarBotonBorrar = True
        Me.txtClave2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtClave2.Formato = ""
        Me.txtClave2.Location = New System.Drawing.Point(108, 55)
        Me.txtClave2.mostrarSiempreBotonBorrar = False
        Me.txtClave2.Name = "txtClave2"
        Me.txtClave2.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtClave2.seleccionarTodo = True
        Me.txtClave2.Size = New System.Drawing.Size(174, 20)
        Me.txtClave2.TabIndex = 5
        Me.txtClave2.UseSystemPasswordChar = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancelar.Location = New System.Drawing.Point(198, 87)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(84, 24)
        Me.btnCancelar.TabIndex = 7
        Me.btnCancelar.Values.Text = "&Cancelar"
        '
        'btnAceptar
        '
        Me.btnAceptar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAceptar.Location = New System.Drawing.Point(108, 87)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(84, 24)
        Me.btnAceptar.TabIndex = 6
        Me.btnAceptar.Values.Text = "&Aceptar"
        '
        'txtClave1
        '
        Me.TableLayoutPanel1.SetColumnSpan(Me.txtClave1, 2)
        Me.txtClave1.controlarBotonBorrar = True
        Me.txtClave1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtClave1.Formato = ""
        Me.txtClave1.Location = New System.Drawing.Point(108, 29)
        Me.txtClave1.mostrarSiempreBotonBorrar = False
        Me.txtClave1.Name = "txtClave1"
        Me.txtClave1.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtClave1.seleccionarTodo = True
        Me.txtClave1.Size = New System.Drawing.Size(174, 20)
        Me.txtClave1.TabIndex = 3
        Me.txtClave1.UseSystemPasswordChar = True
        '
        'lblClaveActual
        '
        Me.lblClaveActual.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.lblClaveActual.AutoSize = True
        Me.lblClaveActual.Location = New System.Drawing.Point(8, 6)
        Me.lblClaveActual.Name = "lblClaveActual"
        Me.lblClaveActual.Size = New System.Drawing.Size(94, 13)
        Me.lblClaveActual.TabIndex = 0
        Me.lblClaveActual.Text = "Contraseña Actual"
        '
        'txtClaveActual
        '
        Me.TableLayoutPanel1.SetColumnSpan(Me.txtClaveActual, 2)
        Me.txtClaveActual.controlarBotonBorrar = True
        Me.txtClaveActual.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtClaveActual.Formato = ""
        Me.txtClaveActual.Location = New System.Drawing.Point(108, 3)
        Me.txtClaveActual.mostrarSiempreBotonBorrar = False
        Me.txtClaveActual.Name = "txtClaveActual"
        Me.txtClaveActual.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtClaveActual.seleccionarTodo = True
        Me.txtClaveActual.Size = New System.Drawing.Size(174, 20)
        Me.txtClaveActual.TabIndex = 1
        Me.txtClaveActual.UseSystemPasswordChar = True
        '
        'epErrores
        '
        Me.epErrores.Alineacion = System.Windows.Forms.ErrorIconAlignment.TopLeft
        Me.epErrores.ContainerControl = Me
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.Controls.Add(Me.btnCancelar, 2, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.lblClaveActual, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.btnAceptar, 1, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.lblUsuario, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.lblClave, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.txtClaveActual, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.txtClave2, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.txtClave1, 1, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 4
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(285, 114)
        Me.TableLayoutPanel1.TabIndex = 8
        '
        'frmCambioClave
        '
        Me.AcceptButton = Me.btnAceptar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.CancelButton = Me.btnCancelar
        Me.ClientSize = New System.Drawing.Size(285, 114)
        Me.ControlBox = False
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCambioClave"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cambiar Contraseña"
        Me.TopMost = True
        CType(Me.epErrores, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblUsuario As System.Windows.Forms.Label
    Friend WithEvents lblClave As System.Windows.Forms.Label
    Friend WithEvents txtClave2 As Quadralia.Controles.aTextBox
    Friend WithEvents btnCancelar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents btnAceptar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents txtClave1 As Quadralia.Controles.aTextBox
    Friend WithEvents lblClaveActual As System.Windows.Forms.Label
    Friend WithEvents txtClaveActual As Quadralia.Controles.aTextBox
    Friend WithEvents epErrores As Quadralia.Controles.Validacion.GestorErrores
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
End Class
