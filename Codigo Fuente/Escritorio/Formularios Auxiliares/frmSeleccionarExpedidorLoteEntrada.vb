﻿Public Class frmSeleccionarExpedidorLoteEntrada
#Region " DECLARACIONES "
    Private _Contexto As Entidades = Nothing
    Private _Expedidor As Expedidor = Nothing
    Private _Lote As LoteEntrada = Nothing
    Private _Disenho As String = String.Empty
#End Region
#Region " PROPIEDADES "
    Public Property Contexto As Entidades
        Get
            If _Contexto Is Nothing Then _Contexto = cDAL.Instancia.getContext()
            Return _Contexto
        End Get
        Set(value As Entidades)
            _Contexto = value
        End Set
    End Property

    ''' <summary>
    ''' Obtiene el expedidor mostrado por el formulario
    ''' </summary>
    Public Property Expedidor As Expedidor
        Get
            Return _Expedidor
        End Get
        Set(value As Expedidor)
            _Expedidor = value
            Cargar()
        End Set
    End Property

    ''' <summary>
    ''' Obtiene la linea del lote de entrada mostrada por el servidor
    ''' </summary>
    Public Property Lote As LoteEntrada
        Get
            Return _Lote
        End Get
        Set(value As LoteEntrada)
            _Lote = value
            Cargar()
        End Set
    End Property

    ''' <summary>
    ''' Obtiene la linea del lote de entrada mostrada por el servidor
    ''' </summary>
    Public Property Disenho As String
        Get
            Return _Disenho
        End Get
        Set(value As String)
            _Disenho = value
            Cargar()
        End Set
    End Property

    ''' <summary>
    ''' Obtiene/Establece el identificador del expedidor mostrado en el formulario
    ''' </summary>
    Public Property IdExpedidor As Integer
        Get
            If _Expedidor Is Nothing Then Return 0
            Return _Expedidor.id
        End Get
        Set(ByVal value As Integer)
            Expedidor = (From exp As Expedidor In Me.Contexto.Expedidores Where exp.id = value).FirstOrDefault
        End Set
    End Property

    ''' <summary>
    ''' Obtiene/Establece el identificador de la línea del lote de entrada mostrada en el formulario
    ''' </summary>
    Public Property IdLote As Integer
        Get
            If _Lote Is Nothing Then Return 0
            Return _Lote.id
        End Get
        Set(ByVal value As Integer)
            _Lote = (From lot As LoteEntrada In Me.Contexto.LotesEntrada Where lot.id = value).FirstOrDefault
        End Set
    End Property
#End Region
#Region " VALIDACIONES "
    ''' <summary>
    ''' Valida que el usuario haya seleccionado un valor de la lista desplegable
    ''' </summary>
    Private Sub ValidarComboObligatorio(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles cboDisenhoEtiqueta.Validating, cboExpedidor.Validating
        If sender Is Nothing OrElse Not TypeOf (sender) Is Quadralia.Controles.aComboBox Then Exit Sub

        If DirectCast(sender, Quadralia.Controles.aComboBox).SelectedIndex = -1 Then
            epErrores.SetError(sender, My.Resources.ComboObligatorio)
        Else
            epErrores.SetError(sender, String.Empty)
        End If
    End Sub
#End Region
#Region " LIMPIEZA "
    Public Sub Limpiar()
        cboExpedidor.Clear()
        txtLoteEntrada.Clear()
        cboDisenhoEtiqueta.Clear()

        epErrores.Clear()
    End Sub
#End Region
#Region " RUTINA DE INICIO "
    ''' <summary>
    ''' Muestra el formulario de acorde ha si existe conexión con la base de datos o no
    ''' </summary>
    Private Sub Inicio(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Quadralia.Formularios.AutoTabular(Me)

        CargarDatosMaestros()
        Limpiar()

        Cargar()
    End Sub

    ''' <summary>
    ''' Carga los datos maestros en el formulario
    ''' </summary>
    Private Sub CargarDatosMaestros()
        ' Cargar Expedidores
        With cboExpedidor
            .DataSource = Nothing
            .Items.Clear()

            .DisplayMember = "razonSocial"
            .ValueMember = "id"
            .DataSource = (From exp As Expedidor In Contexto.Expedidores Select exp)

            .Clear()
        End With

        ' Cargo los disenhos de etiquetas
        With cboDisenhoEtiqueta
            .DataSource = Nothing
            .Items.Clear()

            .DisplayMember = "nombre"
            .ValueMember = "ruta"
            .DataSource = Quadralia.Aplicacion.GestorInformes.Informes("EtiquetasTrazabilidad").Values.ToList

            .Clear()
        End With
    End Sub
#End Region
#Region " CARGAR DATOS "
    Private Sub Cargar()
        Limpiar()

        cboExpedidor.SelectedItem = _Expedidor
        txtLoteEntrada.Item = _Lote
        cboDisenhoEtiqueta.SelectedValue = _Disenho
    End Sub
#End Region
#Region " ACEPTAR / CANCELAR "
    ''' <summary>
    ''' Lanza la rutina de validación de usuario
    ''' </summary>
    Private Sub Aceptar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        ' Validaciones
        Me.ValidateChildren()
        If epErrores.Count > 0 Then Exit Sub

        Try
            _Expedidor = cboExpedidor.SelectedItem
            _Lote = txtLoteEntrada.Item
            _Disenho = cboDisenhoEtiqueta.SelectedValue

            Me.DialogResult = Windows.Forms.DialogResult.OK
            Me.Close()
        Catch ex As Exception
            Quadralia.Aplicacion.InformarError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' Cancela el inicio de sesión
    ''' </summary>
    Private Sub Cancelar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub
#End Region
End Class