﻿Imports Escritorio.Quadralia.Seguridad.Criptografia

Public Class frmProgresoSincronizacion

    Private Sub CerrarFormulario() Handles btnCerrar.Click
        Try
            Me.Close()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub Inicio(sender As Object, e As EventArgs) Handles MyBase.Load
        CheckForIllegalCrossThreadCalls = False

        pgbGeneral.Value = 0
        pgbParticular.Value = 0
        lblEstado.Text = String.Empty

        RemoveHandler qSinc.Sincronizador.Instancia.Progreso, AddressOf NotificarProgreso
        AddHandler qSinc.Sincronizador.Instancia.Progreso, AddressOf NotificarProgreso

        RemoveHandler qSinc.Sincronizador.Instancia.SincronizacionFinalizada, AddressOf CerrarFormulario
        AddHandler qSinc.Sincronizador.Instancia.SincronizacionAcabada, AddressOf NotificarFinalizacion
    End Sub

    Private Sub NotificarProgreso(ProgresoGeneral As Integer, ProgresoParticular As Integer, ElementoProcesado As String)
        pgbGeneral.Value = ProgresoGeneral
        pgbParticular.Value = ProgresoParticular
        lblEstado.Text = ElementoProcesado
    End Sub

    Private Sub NotificarFinalizacion(eti_Sincro As List(Of Etiqueta), eti_Error As List(Of Etiqueta))

        lblResultado.Text = "Se han sincronizado: " & eti_Sincro.Count & " etiquetas."
        If eti_Error.Any Then
            lblResultado.Text &= vbCrLf & "Las siguientes etiquetas no se han podido sicnronizar: " &
                String.Join(vbCrLf, eti_Error.Select(Function(x) x.Codigo))
        End If
    End Sub
End Class
