﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDebug
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CBackGroundWorker1 = New Escritorio.Quadralia.Controles.Threading.cBackGroundWorker()
        Me.txtDireccionIP = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtRespuesta = New Escritorio.Quadralia.Controles.aTextBox()
        Me.btnEmpezar = New System.Windows.Forms.Button()
        Me.btnDetener = New System.Windows.Forms.Button()
        Me.lblIP = New System.Windows.Forms.Label()
        Me.lblRespuesta = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'CBackGroundWorker1
        '
        Me.CBackGroundWorker1.MostrarFormulario = True
        Me.CBackGroundWorker1.TipoBarra = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.CBackGroundWorker1.WorkerReportsProgress = True
        Me.CBackGroundWorker1.WorkerSupportsCancellation = True
        '
        'txtDireccionIP
        '
        Me.txtDireccionIP.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtDireccionIP.controlarBotonBorrar = True
        Me.txtDireccionIP.Formato = ""
        Me.txtDireccionIP.Location = New System.Drawing.Point(80, 13)
        Me.txtDireccionIP.mostrarSiempreBotonBorrar = False
        Me.txtDireccionIP.Name = "txtDireccionIP"
        Me.txtDireccionIP.seleccionarTodo = True
        Me.txtDireccionIP.Size = New System.Drawing.Size(192, 20)
        Me.txtDireccionIP.TabIndex = 0
        '
        'txtRespuesta
        '
        Me.txtRespuesta.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtRespuesta.controlarBotonBorrar = True
        Me.txtRespuesta.Formato = ""
        Me.txtRespuesta.Location = New System.Drawing.Point(12, 59)
        Me.txtRespuesta.mostrarSiempreBotonBorrar = False
        Me.txtRespuesta.Multiline = True
        Me.txtRespuesta.Name = "txtRespuesta"
        Me.txtRespuesta.seleccionarTodo = True
        Me.txtRespuesta.Size = New System.Drawing.Size(260, 162)
        Me.txtRespuesta.TabIndex = 0
        '
        'btnEmpezar
        '
        Me.btnEmpezar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEmpezar.Location = New System.Drawing.Point(116, 227)
        Me.btnEmpezar.Name = "btnEmpezar"
        Me.btnEmpezar.Size = New System.Drawing.Size(75, 23)
        Me.btnEmpezar.TabIndex = 1
        Me.btnEmpezar.Text = "Empezar"
        Me.btnEmpezar.UseVisualStyleBackColor = True
        '
        'btnDetener
        '
        Me.btnDetener.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDetener.Location = New System.Drawing.Point(197, 227)
        Me.btnDetener.Name = "btnDetener"
        Me.btnDetener.Size = New System.Drawing.Size(75, 23)
        Me.btnDetener.TabIndex = 1
        Me.btnDetener.Text = "Detener"
        Me.btnDetener.UseVisualStyleBackColor = True
        '
        'lblIP
        '
        Me.lblIP.AutoSize = True
        Me.lblIP.Location = New System.Drawing.Point(9, 17)
        Me.lblIP.Name = "lblIP"
        Me.lblIP.Size = New System.Drawing.Size(65, 13)
        Me.lblIP.TabIndex = 2
        Me.lblIP.Text = "Dirección IP"
        '
        'lblRespuesta
        '
        Me.lblRespuesta.AutoSize = True
        Me.lblRespuesta.Location = New System.Drawing.Point(9, 43)
        Me.lblRespuesta.Name = "lblRespuesta"
        Me.lblRespuesta.Size = New System.Drawing.Size(58, 13)
        Me.lblRespuesta.TabIndex = 2
        Me.lblRespuesta.Text = "Respuesta"
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button1.Location = New System.Drawing.Point(12, 227)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "Sincronizar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'frmDebug
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 262)
        Me.Controls.Add(Me.lblRespuesta)
        Me.Controls.Add(Me.lblIP)
        Me.Controls.Add(Me.btnDetener)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.btnEmpezar)
        Me.Controls.Add(Me.txtRespuesta)
        Me.Controls.Add(Me.txtDireccionIP)
        Me.Name = "frmDebug"
        Me.Text = "frmDebug"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CBackGroundWorker1 As Escritorio.Quadralia.Controles.Threading.cBackGroundWorker
    Friend WithEvents txtDireccionIP As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents txtRespuesta As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents btnEmpezar As System.Windows.Forms.Button
    Friend WithEvents btnDetener As System.Windows.Forms.Button
    Friend WithEvents lblIP As System.Windows.Forms.Label
    Friend WithEvents lblRespuesta As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
