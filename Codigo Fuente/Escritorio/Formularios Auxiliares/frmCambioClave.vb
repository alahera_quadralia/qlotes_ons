﻿Imports Escritorio.Quadralia.Seguridad.Criptografia

Public Class frmCambioClave
#Region " DECLARACIONES "
    Private _Contexto As Entidades = Nothing
    Private _Usuario As Usuario

    Public Enum TipoFormularo
        CambioClave
        ResetClave
    End Enum
#End Region
#Region " PROPIEDADES "
    Public WriteOnly Property Tipo As TipoFormularo
        Set(value As TipoFormularo)
            lblClaveActual.Visible = (value = TipoFormularo.CambioClave)
            txtClaveActual.Visible = (value = TipoFormularo.CambioClave)
        End Set
    End Property

    Private ReadOnly Property Contexto As Entidades
        Get
            If _Contexto Is Nothing Then
                _Contexto = cDAL.Instancia.getContext()
            End If

            Return _Contexto
        End Get
    End Property

    Private ReadOnly Property Usuario As Usuario
        Get
            If _Usuario Is Nothing Then _Usuario = Quadralia.Aplicacion.UsuarioConectado
            Return _Usuario
        End Get
    End Property

    Public Property IdUsuario As Long
        Get
            If _Usuario IsNot Nothing Then Return _Usuario.Id
            Return 0
        End Get
        Set(ByVal value As Long)
            _Usuario = (From Usu As Usuario In Me.Contexto.Usuarios Where Usu.Id = value).FirstOrDefault
        End Set
    End Property
#End Region
#Region " CONSTRUCTORES "
    Public Sub New(ByRef Usuario As Usuario, ByRef Contexto As Entidades)
        Me.New()
        _Usuario = Usuario
        _Contexto = Contexto
    End Sub

    Public Sub New(ByRef Usuario As Usuario)
        Me.New()
        _Usuario = Usuario
    End Sub


    Public Sub New()
        InitializeComponent()
    End Sub
#End Region
#Region " VALIDACIONES "
    Private Sub ValidarClaveActual(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtClaveActual.Validating
        If Usuario Is Nothing Then
            epErrores.SetError(txtClaveActual, "")
        ElseIf Not String.Equals(EncriptarEnMD5(txtClaveActual.Text), Usuario.clave, StringComparison.OrdinalIgnoreCase) Then
            epErrores.SetError(txtClaveActual, "La contraseña actual no es correcta. Por favor, revísela")
        Else
            epErrores.SetError(txtClaveActual, "")
        End If
    End Sub

    Private Sub ValidarClave1(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtClave1.Validating
        If Usuario Is Nothing Then
            epErrores.SetError(txtClave1, "")
        ElseIf String.IsNullOrEmpty(txtClave1.Text.Trim()) Then
            epErrores.SetError(txtClave1, "La contraseña no puede quedar en blanco. Por favor, escriba una contraseña")
        Else
            epErrores.SetError(txtClave1, "")
        End If
    End Sub

    Private Sub ValidarClave2(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtClave2.Validating
        If Usuario Is Nothing Then
            epErrores.SetError(txtClave2, "")
        ElseIf String.IsNullOrEmpty(txtClave2.Text.Trim()) Then
            epErrores.SetError(txtClave2, "La contraseña no puede quedar en blanco. Por favor, escriba una contraseña")
        ElseIf txtClave2.Text <> txtClave1.Text Then
            epErrores.SetError(txtClave2, "Las contraseñas introducidas no coinciden. Por favor, revíselas")
        Else
            epErrores.SetError(txtClave2, "")
        End If
    End Sub
#End Region
#Region " LIMPIEZA "
    Public Sub Limpiar()
        txtClave1.Clear()
        txtClave2.Clear()
        txtClaveActual.Clear()

        epErrores.Clear()
    End Sub
#End Region
#Region " RUTINA DE INICIO "
    ''' <summary>
    ''' Muestra el formulario de acorde ha si existe conexión con la base de datos o no
    ''' </summary>
    Private Sub Inicio(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Limpiar()
        Me.ActiveControl = txtClaveActual.TextBox
    End Sub
#End Region
#Region " ACEPTAR / CANCELAR "
    ''' <summary>
    ''' Lanza la rutina de validación de usuario
    ''' </summary>
    Private Sub Aceptar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If Usuario Is Nothing OrElse Usuario.id <= 0 Then Exit Sub

#If Not Debug Then
        ' Validaciones
        Me.ValidateChildren()
        If epErrores.Count > 0 Then Exit Sub
#End If

        Try
            Usuario.clave = Quadralia.Seguridad.Criptografia.EncriptarEnMD5(txtClave1.Text)
            Contexto.SaveChanges()
            Quadralia.Aplicacion.MostrarMensajeNtfIco("Cambio contraseña", "La contraseña de acceso se ha cambiado correctamente, la próxima vez que inicie sesión podrá utilizarla.", ToolTipIcon.Info)

            Me.DialogResult = Windows.Forms.DialogResult.OK
            Me.Close()
        Catch ex As Exception
            Quadralia.Aplicacion.InformarError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' Cancela el inicio de sesión
    ''' </summary>
    Private Sub Cancelar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub
#End Region
End Class