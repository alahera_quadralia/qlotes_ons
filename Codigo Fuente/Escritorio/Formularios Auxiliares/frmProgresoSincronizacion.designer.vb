﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProgresoSincronizacion
    Inherits ComponentFactory.Krypton.Toolkit.KryptonForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmProgresoSincronizacion))
        Me.tblOrganizador = New System.Windows.Forms.TableLayoutPanel()
        Me.pgbParticular = New System.Windows.Forms.ProgressBar()
        Me.pgbGeneral = New System.Windows.Forms.ProgressBar()
        Me.btnCerrar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.lblEstado = New ComponentFactory.Krypton.Toolkit.KryptonWrapLabel()
        Me.epErrores = New Escritorio.Quadralia.Controles.Validacion.GestorErrores()
        Me.lblResultado = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.tblOrganizador.SuspendLayout()
        CType(Me.epErrores, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tblOrganizador
        '
        Me.tblOrganizador.ColumnCount = 1
        Me.tblOrganizador.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblOrganizador.Controls.Add(Me.pgbParticular, 0, 2)
        Me.tblOrganizador.Controls.Add(Me.pgbGeneral, 0, 1)
        Me.tblOrganizador.Controls.Add(Me.lblEstado, 0, 0)
        Me.tblOrganizador.Controls.Add(Me.btnCerrar, 0, 4)
        Me.tblOrganizador.Controls.Add(Me.lblResultado, 0, 3)
        Me.tblOrganizador.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblOrganizador.Location = New System.Drawing.Point(0, 0)
        Me.tblOrganizador.Name = "tblOrganizador"
        Me.tblOrganizador.RowCount = 5
        Me.tblOrganizador.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40.0!))
        Me.tblOrganizador.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizador.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizador.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60.0!))
        Me.tblOrganizador.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblOrganizador.Size = New System.Drawing.Size(452, 223)
        Me.tblOrganizador.TabIndex = 0
        '
        'pgbParticular
        '
        Me.pgbParticular.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pgbParticular.Location = New System.Drawing.Point(3, 85)
        Me.pgbParticular.Name = "pgbParticular"
        Me.pgbParticular.Size = New System.Drawing.Size(446, 23)
        Me.pgbParticular.TabIndex = 0
        '
        'pgbGeneral
        '
        Me.pgbGeneral.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pgbGeneral.Location = New System.Drawing.Point(3, 56)
        Me.pgbGeneral.Name = "pgbGeneral"
        Me.pgbGeneral.Size = New System.Drawing.Size(446, 23)
        Me.pgbGeneral.TabIndex = 0
        '
        'btnCerrar
        '
        Me.btnCerrar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCerrar.Location = New System.Drawing.Point(359, 195)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(90, 25)
        Me.btnCerrar.TabIndex = 2
        Me.btnCerrar.Values.Text = "Cerrar"
        '
        'lblEstado
        '
        Me.lblEstado.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblEstado.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.lblEstado.ForeColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(91, Byte), Integer))
        Me.lblEstado.Location = New System.Drawing.Point(3, 0)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(446, 53)
        Me.lblEstado.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'epErrores
        '
        Me.epErrores.Alineacion = System.Windows.Forms.ErrorIconAlignment.TopLeft
        Me.epErrores.ContainerControl = Me
        '
        'lblResultado
        '
        Me.lblResultado.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblResultado.Location = New System.Drawing.Point(3, 114)
        Me.lblResultado.Name = "lblResultado"
        Me.lblResultado.Size = New System.Drawing.Size(446, 74)
        Me.lblResultado.TabIndex = 1
        Me.lblResultado.Values.Text = ""
        '
        'frmProgresoSincronizacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(452, 223)
        Me.ControlBox = False
        Me.Controls.Add(Me.tblOrganizador)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmProgresoSincronizacion"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Progreso Sincronización"
        Me.TopMost = True
        Me.tblOrganizador.ResumeLayout(False)
        Me.tblOrganizador.PerformLayout()
        CType(Me.epErrores, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents epErrores As Quadralia.Controles.Validacion.GestorErrores
    Friend WithEvents tblOrganizador As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents pgbParticular As System.Windows.Forms.ProgressBar
    Friend WithEvents pgbGeneral As System.Windows.Forms.ProgressBar
    Friend WithEvents btnCerrar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents lblEstado As ComponentFactory.Krypton.Toolkit.KryptonWrapLabel
    Friend WithEvents lblResultado As KryptonLabel
End Class
