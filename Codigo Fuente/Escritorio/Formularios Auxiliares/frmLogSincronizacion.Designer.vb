﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLogSincronizacion
    Inherits ComponentFactory.Krypton.Toolkit.KryptonForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLogSincronizacion))
        Me.btnCerrar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.LogEventos = New System.Diagnostics.EventLog()
        Me.colMensaje = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colIdEvento = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colFechaHora = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colTipo = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colImagne = New System.Windows.Forms.DataGridViewImageColumn()
        Me.dgvLog = New ComponentFactory.Krypton.Toolkit.KryptonDataGridView()
        CType(Me.LogEventos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvLog, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCerrar
        '
        Me.btnCerrar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCerrar.Location = New System.Drawing.Point(418, 266)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(75, 24)
        Me.btnCerrar.TabIndex = 3
        Me.btnCerrar.Values.Text = "Cerrar"
        '
        'LogEventos
        '
        Me.LogEventos.EnableRaisingEvents = True
        Me.LogEventos.Log = "Application"
        Me.LogEventos.Source = "SincronizadorTM_Log"
        Me.LogEventos.SynchronizingObject = Me
        '
        'colMensaje
        '
        Me.colMensaje.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colMensaje.HeaderText = "Mensaje"
        Me.colMensaje.Name = "colMensaje"
        Me.colMensaje.ReadOnly = True
        Me.colMensaje.Width = 222
        '
        'colIdEvento
        '
        Me.colIdEvento.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colIdEvento.HeaderText = "Id. del evento"
        Me.colIdEvento.Name = "colIdEvento"
        Me.colIdEvento.ReadOnly = True
        Me.colIdEvento.Width = 107
        '
        'colFechaHora
        '
        Me.colFechaHora.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        DataGridViewCellStyle1.Format = "G"
        DataGridViewCellStyle1.NullValue = Nothing
        Me.colFechaHora.DefaultCellStyle = DataGridViewCellStyle1
        Me.colFechaHora.HeaderText = "Fecha y Hora"
        Me.colFechaHora.Name = "colFechaHora"
        Me.colFechaHora.ReadOnly = True
        Me.colFechaHora.Width = 105
        '
        'colTipo
        '
        Me.colTipo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colTipo.HeaderText = "Nivel"
        Me.colTipo.Name = "colTipo"
        Me.colTipo.ReadOnly = True
        Me.colTipo.Width = 63
        '
        'colImagne
        '
        Me.colImagne.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colImagne.HeaderText = ""
        Me.colImagne.Name = "colImagne"
        Me.colImagne.ReadOnly = True
        Me.colImagne.Width = 7
        '
        'dgvLog
        '
        Me.dgvLog.AllowUserToAddRows = False
        Me.dgvLog.AllowUserToDeleteRows = False
        Me.dgvLog.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvLog.ColumnHeadersHeight = 20
        Me.dgvLog.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colImagne, Me.colTipo, Me.colFechaHora, Me.colIdEvento, Me.colMensaje})
        Me.dgvLog.Location = New System.Drawing.Point(0, 0)
        Me.dgvLog.Name = "dgvLog"
        Me.dgvLog.ReadOnly = True
        Me.dgvLog.RowHeadersVisible = False
        Me.dgvLog.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLog.Size = New System.Drawing.Size(505, 260)
        Me.dgvLog.TabIndex = 5
        '
        'frmLogSincronizacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(505, 302)
        Me.Controls.Add(Me.dgvLog)
        Me.Controls.Add(Me.btnCerrar)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(521, 340)
        Me.Name = "frmLogSincronizacion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Visor Log Sincronización"
        CType(Me.LogEventos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvLog, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnCerrar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents LogEventos As System.Diagnostics.EventLog
    Friend WithEvents dgvLog As ComponentFactory.Krypton.Toolkit.KryptonDataGridView
    Friend WithEvents colImagne As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents colTipo As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents colFechaHora As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents colIdEvento As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents colMensaje As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn

End Class
