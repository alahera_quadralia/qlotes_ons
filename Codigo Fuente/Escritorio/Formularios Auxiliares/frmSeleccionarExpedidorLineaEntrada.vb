﻿Public Class frmSeleccionarExpedidorLineaEntrada
#Region " DECLARACIONES "
    Private _Contexto As Entidades = Nothing
    Private _Expedidor As Expedidor = Nothing
    Private _Linea As LoteEntradalinea = Nothing
#End Region
#Region " PROPIEDADES "
    Public Property Contexto As Entidades
        Get
            If _Contexto Is Nothing Then _Contexto = cDAL.Instancia.getContext()
            Return _Contexto
        End Get
        Set(value As Entidades)
            _Contexto = value
        End Set
    End Property

    ''' <summary>
    ''' Obtiene el expedidor mostrado por el formulario
    ''' </summary>
    Public Property Expedidor As Expedidor
        Get
            Return _Expedidor
        End Get
        Set(value As Expedidor)
            _Expedidor = value
            Cargar()
        End Set
    End Property

    ''' <summary>
    ''' Obtiene la linea del lote de entrada mostrada por el servidor
    ''' </summary>
    Public Property LineaEntrada As LoteEntradalinea
        Get
            Return _Linea
        End Get
        Set(value As LoteEntradalinea)
            _Linea = value
            Cargar()
        End Set
    End Property

    ''' <summary>
    ''' Obtiene/Establece el identificador del expedidor mostrado en el formulario
    ''' </summary>
    Public Property IdExpedidor As Integer
        Get
            If _Expedidor Is Nothing Then Return 0
            Return _Expedidor.id
        End Get
        Set(ByVal value As Integer)
            Expedidor = (From exp As Expedidor In Me.Contexto.Expedidores Where exp.id = value).FirstOrDefault
        End Set
    End Property

    ''' <summary>
    ''' Obtiene/Establece el identificador de la línea del lote de entrada mostrada en el formulario
    ''' </summary>
    Public Property IdLinea As Integer
        Get
            If _Linea Is Nothing Then Return 0
            Return _Linea.id
        End Get
        Set(ByVal value As Integer)
            LineaEntrada = (From lin As LoteEntradalinea In Me.Contexto.LotesEntradaLineas Where lin.id = value).FirstOrDefault
        End Set
    End Property
#End Region
#Region " VALIDACIONES "
#End Region
#Region " LIMPIEZA "
    Public Sub Limpiar()
        cboExpedidor.Clear()
        txtLineaLoteEntrada.Clear()

        epErrores.Clear()
    End Sub
#End Region
#Region " RUTINA DE INICIO "
    ''' <summary>
    ''' Muestra el formulario de acorde ha si existe conexión con la base de datos o no
    ''' </summary>
    Private Sub Inicio(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Quadralia.Formularios.AutoTabular(Me)
        CargarDatosMaestros()
        Limpiar()

        Cargar()
    End Sub

    ''' <summary>
    ''' Carga los datos maestros en el formulario
    ''' </summary>
    Private Sub CargarDatosMaestros()
        ' Cargar Expedidores
        With cboExpedidor
            .DataSource = Nothing
            .Items.Clear()

            .DisplayMember = "razonSocial"
            .ValueMember = "id"
            .DataSource = (From exp As Expedidor In Contexto.Expedidores Select exp)

            .Clear()
        End With
    End Sub
#End Region
#Region " CARGAR DATOS "
    Private Sub Cargar()
        Limpiar()

        cboExpedidor.SelectedItem = _Expedidor
        txtLineaLoteEntrada.Item = _Linea
    End Sub
#End Region
#Region " ACEPTAR / CANCELAR "
    ''' <summary>
    ''' Lanza la rutina de validación de usuario
    ''' </summary>
    Private Sub Aceptar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        ' Validaciones
        Me.ValidateChildren()
        If epErrores.Count > 0 Then Exit Sub

        Try
            _Expedidor = cboExpedidor.SelectedItem
            _Linea = txtLineaLoteEntrada.Item

            Me.DialogResult = Windows.Forms.DialogResult.OK
            Me.Close()
        Catch ex As Exception
            Quadralia.Aplicacion.InformarError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' Cancela el inicio de sesión
    ''' </summary>
    Private Sub Cancelar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub
#End Region
End Class