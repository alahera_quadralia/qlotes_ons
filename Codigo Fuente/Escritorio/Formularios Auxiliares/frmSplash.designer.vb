﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSplash
	Inherits KryptonForm

	'Form reemplaza a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> _
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Requerido por el Diseñador de Windows Forms
	Private components As System.ComponentModel.IContainer

	'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar usando el Diseñador de Windows Forms.  
	'No lo modifique con el editor de código.
	<System.Diagnostics.DebuggerStepThrough()> _
	Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.pSplash = New ComponentFactory.Krypton.Toolkit.KryptonPanel()
        Me.lblEstado = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblVersion = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblTituloAplicacion = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.tmrReloj = New System.Windows.Forms.Timer(Me.components)
        CType(Me.pSplash, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pSplash.SuspendLayout()
        Me.SuspendLayout()
        '
        'pSplash
        '
        Me.pSplash.Controls.Add(Me.lblEstado)
        Me.pSplash.Controls.Add(Me.lblVersion)
        Me.pSplash.Controls.Add(Me.lblTituloAplicacion)
        Me.pSplash.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pSplash.Location = New System.Drawing.Point(0, 0)
        Me.pSplash.Name = "pSplash"
        Me.pSplash.Size = New System.Drawing.Size(449, 131)
        Me.pSplash.StateCommon.Color1 = System.Drawing.Color.FromArgb(CType(CType(67, Byte), Integer), CType(CType(74, Byte), Integer), CType(CType(84, Byte), Integer))
        Me.pSplash.TabIndex = 0
        '
        'lblEstado
        '
        Me.lblEstado.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblEstado.AutoSize = False
        Me.lblEstado.Location = New System.Drawing.Point(12, 96)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(425, 29)
        Me.lblEstado.StateCommon.ShortText.Color1 = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(186, Byte), Integer), CType(CType(67, Byte), Integer))
        Me.lblEstado.StateCommon.ShortText.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEstado.StateCommon.ShortText.Hint = ComponentFactory.Krypton.Toolkit.PaletteTextHint.AntiAlias
        Me.lblEstado.StateCommon.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center
        Me.lblEstado.TabIndex = 1
        Me.lblEstado.Values.Text = "Estado"
        '
        'lblVersion
        '
        Me.lblVersion.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblVersion.Location = New System.Drawing.Point(398, 12)
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.Size = New System.Drawing.Size(39, 20)
        Me.lblVersion.StateCommon.ShortText.Color1 = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(186, Byte), Integer), CType(CType(67, Byte), Integer))
        Me.lblVersion.StateCommon.ShortText.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVersion.StateCommon.ShortText.Hint = ComponentFactory.Krypton.Toolkit.PaletteTextHint.AntiAlias
        Me.lblVersion.StateCommon.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center
        Me.lblVersion.TabIndex = 1
        Me.lblVersion.Values.Text = "{0}"
        '
        'lblTituloAplicacion
        '
        Me.lblTituloAplicacion.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTituloAplicacion.AutoSize = False
        Me.lblTituloAplicacion.Location = New System.Drawing.Point(12, 45)
        Me.lblTituloAplicacion.Name = "lblTituloAplicacion"
        Me.lblTituloAplicacion.Size = New System.Drawing.Size(425, 38)
        Me.lblTituloAplicacion.StateCommon.ShortText.Color1 = System.Drawing.Color.FromArgb(CType(CType(125, Byte), Integer), CType(CType(186, Byte), Integer), CType(CType(67, Byte), Integer))
        Me.lblTituloAplicacion.StateCommon.ShortText.Font = New System.Drawing.Font("Verdana", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTituloAplicacion.StateCommon.ShortText.Hint = ComponentFactory.Krypton.Toolkit.PaletteTextHint.AntiAlias
        Me.lblTituloAplicacion.StateCommon.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center
        Me.lblTituloAplicacion.TabIndex = 0
        Me.lblTituloAplicacion.Values.Text = "Título Aplicación"
        '
        'frmSplash
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(449, 131)
        Me.ControlBox = False
        Me.Controls.Add(Me.pSplash)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmSplash"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(CType(CType(67, Byte), Integer), CType(CType(74, Byte), Integer), CType(CType(84, Byte), Integer))
        Me.StateCommon.Border.DrawBorders = CType((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top Or ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) _
                    Or ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) _
                    Or ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right), ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)
        Me.StateCommon.Border.GraphicsHint = ComponentFactory.Krypton.Toolkit.PaletteGraphicsHint.None
        Me.StateCommon.Border.Width = 3
        CType(Me.pSplash, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pSplash.ResumeLayout(False)
        Me.pSplash.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
	Friend WithEvents pSplash As ComponentFactory.Krypton.Toolkit.KryptonPanel
	Friend WithEvents lblTituloAplicacion As ComponentFactory.Krypton.Toolkit.KryptonLabel
	Friend WithEvents lblEstado As ComponentFactory.Krypton.Toolkit.KryptonLabel
	Friend WithEvents lblVersion As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents tmrReloj As System.Windows.Forms.Timer
End Class
