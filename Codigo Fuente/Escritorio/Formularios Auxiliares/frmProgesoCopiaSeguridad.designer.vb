﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProgresoCopiaSeguridad
    Inherits KryptonForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmProgresoCopiaSeguridad))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.lblRutaRealizar = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtRutaRealizar = New ComponentFactory.Krypton.Toolkit.KryptonTextBox()
        Me.btnBuscarArchivoRealizar = New ComponentFactory.Krypton.Toolkit.ButtonSpecAny()
        Me.btnComenzarRealizar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.pCuerpo = New ComponentFactory.Krypton.Toolkit.KryptonPanel()
        Me.tabGeneral = New ComponentFactory.Krypton.Navigator.KryptonNavigator()
        Me.tbpRealizar = New ComponentFactory.Krypton.Navigator.KryptonPage()
        Me.picProgresoRealizar = New System.Windows.Forms.PictureBox()
        Me.txtDescripcionRealizar = New ComponentFactory.Krypton.Toolkit.KryptonTextBox()
        Me.pgbProgresoGeneralRealizar = New System.Windows.Forms.ProgressBar()
        Me.pgbProgresoParticularRealizar = New System.Windows.Forms.ProgressBar()
        Me.tbpRestaurar = New ComponentFactory.Krypton.Navigator.KryptonPage()
        Me.dgvInformacionArchivo = New ComponentFactory.Krypton.Toolkit.KryptonDataGridView()
        Me.colAplicacion = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colVersion = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colFechaBackup = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.picProgresoRestaurar = New System.Windows.Forms.PictureBox()
        Me.txtDescripcionRestaurar = New ComponentFactory.Krypton.Toolkit.KryptonTextBox()
        Me.pgbProgresoGeneralRestaurar = New System.Windows.Forms.ProgressBar()
        Me.pgbProgresoParticularRestaurar = New System.Windows.Forms.ProgressBar()
        Me.lblRutaRestaurar = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.btnComenzarRestaurar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.txtRutaRestaurar = New ComponentFactory.Krypton.Toolkit.KryptonTextBox()
        Me.btnBuscarArchivoRestaurar = New ComponentFactory.Krypton.Toolkit.ButtonSpecAny()
        Me.grpCabecera = New ComponentFactory.Krypton.Toolkit.KryptonGroup()
        Me.lblTitulo = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.bgRealizarCopia = New System.ComponentModel.BackgroundWorker()
        Me.bgRestaurarCopia = New System.ComponentModel.BackgroundWorker()
        Me.epErrores = New Escritorio.Quadralia.Controles.Validacion.GestorErrores()
        CType(Me.pCuerpo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pCuerpo.SuspendLayout()
        CType(Me.tabGeneral, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabGeneral.SuspendLayout()
        CType(Me.tbpRealizar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpRealizar.SuspendLayout()
        CType(Me.picProgresoRealizar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbpRestaurar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpRestaurar.SuspendLayout()
        CType(Me.dgvInformacionArchivo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picProgresoRestaurar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpCabecera, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpCabecera.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpCabecera.Panel.SuspendLayout()
        Me.grpCabecera.SuspendLayout()
        CType(Me.epErrores, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblRutaRealizar
        '
        resources.ApplyResources(Me.lblRutaRealizar, "lblRutaRealizar")
        Me.lblRutaRealizar.Name = "lblRutaRealizar"
        Me.lblRutaRealizar.Values.Text = resources.GetString("lblRutaRealizar.Values.Text")
        '
        'txtRutaRealizar
        '
        resources.ApplyResources(Me.txtRutaRealizar, "txtRutaRealizar")
        Me.txtRutaRealizar.ButtonSpecs.AddRange(New ComponentFactory.Krypton.Toolkit.ButtonSpecAny() {Me.btnBuscarArchivoRealizar})
        Me.txtRutaRealizar.Name = "txtRutaRealizar"
        Me.txtRutaRealizar.ReadOnly = True
        '
        'btnBuscarArchivoRealizar
        '
        Me.btnBuscarArchivoRealizar.Image = Global.Escritorio.My.Resources.Resources.Buscar_16
        Me.btnBuscarArchivoRealizar.UniqueName = "E62D69ACEB654496279D6CD3E6AFFF61"
        '
        'btnComenzarRealizar
        '
        resources.ApplyResources(Me.btnComenzarRealizar, "btnComenzarRealizar")
        Me.btnComenzarRealizar.Name = "btnComenzarRealizar"
        Me.btnComenzarRealizar.Values.Text = resources.GetString("btnComenzarRealizar.Values.Text")
        '
        'pCuerpo
        '
        Me.pCuerpo.Controls.Add(Me.tabGeneral)
        Me.pCuerpo.Controls.Add(Me.grpCabecera)
        resources.ApplyResources(Me.pCuerpo, "pCuerpo")
        Me.pCuerpo.Name = "pCuerpo"
        '
        'tabGeneral
        '
        Me.tabGeneral.Bar.BarOrientation = ComponentFactory.Krypton.Toolkit.VisualOrientation.Left
        Me.tabGeneral.Bar.ItemOrientation = ComponentFactory.Krypton.Toolkit.ButtonOrientation.FixedTop
        Me.tabGeneral.Bar.TabBorderStyle = ComponentFactory.Krypton.Toolkit.TabBorderStyle.RoundedEqualSmall
        Me.tabGeneral.Bar.TabStyle = ComponentFactory.Krypton.Toolkit.TabStyle.LowProfile
        Me.tabGeneral.Button.ButtonDisplayLogic = ComponentFactory.Krypton.Navigator.ButtonDisplayLogic.None
        Me.tabGeneral.Button.CloseButtonAction = ComponentFactory.Krypton.Navigator.CloseButtonAction.None
        Me.tabGeneral.Button.CloseButtonDisplay = ComponentFactory.Krypton.Navigator.ButtonDisplay.Hide
        resources.ApplyResources(Me.tabGeneral, "tabGeneral")
        Me.tabGeneral.Name = "tabGeneral"
        Me.tabGeneral.Pages.AddRange(New ComponentFactory.Krypton.Navigator.KryptonPage() {Me.tbpRealizar, Me.tbpRestaurar})
        Me.tabGeneral.SelectedIndex = 0
        '
        'tbpRealizar
        '
        Me.tbpRealizar.AutoHiddenSlideSize = New System.Drawing.Size(200, 200)
        Me.tbpRealizar.Controls.Add(Me.picProgresoRealizar)
        Me.tbpRealizar.Controls.Add(Me.txtDescripcionRealizar)
        Me.tbpRealizar.Controls.Add(Me.pgbProgresoGeneralRealizar)
        Me.tbpRealizar.Controls.Add(Me.pgbProgresoParticularRealizar)
        Me.tbpRealizar.Controls.Add(Me.lblRutaRealizar)
        Me.tbpRealizar.Controls.Add(Me.btnComenzarRealizar)
        Me.tbpRealizar.Controls.Add(Me.txtRutaRealizar)
        Me.tbpRealizar.Flags = 65534
        Me.tbpRealizar.LastVisibleSet = True
        resources.ApplyResources(Me.tbpRealizar, "tbpRealizar")
        Me.tbpRealizar.Name = "tbpRealizar"
        Me.tbpRealizar.UniqueName = "F957580818644F3C0F9437DA0BD45BE9"
        '
        'picProgresoRealizar
        '
        resources.ApplyResources(Me.picProgresoRealizar, "picProgresoRealizar")
        Me.picProgresoRealizar.BackColor = System.Drawing.Color.Transparent
        Me.picProgresoRealizar.Image = Global.Escritorio.My.Resources.Resources.Cargando_32
        Me.picProgresoRealizar.Name = "picProgresoRealizar"
        Me.picProgresoRealizar.TabStop = False
        '
        'txtDescripcionRealizar
        '
        resources.ApplyResources(Me.txtDescripcionRealizar, "txtDescripcionRealizar")
        Me.txtDescripcionRealizar.Name = "txtDescripcionRealizar"
        Me.txtDescripcionRealizar.ReadOnly = True
        Me.txtDescripcionRealizar.TabStop = False
        '
        'pgbProgresoGeneralRealizar
        '
        resources.ApplyResources(Me.pgbProgresoGeneralRealizar, "pgbProgresoGeneralRealizar")
        Me.pgbProgresoGeneralRealizar.Name = "pgbProgresoGeneralRealizar"
        '
        'pgbProgresoParticularRealizar
        '
        resources.ApplyResources(Me.pgbProgresoParticularRealizar, "pgbProgresoParticularRealizar")
        Me.pgbProgresoParticularRealizar.Name = "pgbProgresoParticularRealizar"
        '
        'tbpRestaurar
        '
        Me.tbpRestaurar.AutoHiddenSlideSize = New System.Drawing.Size(200, 200)
        Me.tbpRestaurar.Controls.Add(Me.dgvInformacionArchivo)
        Me.tbpRestaurar.Controls.Add(Me.picProgresoRestaurar)
        Me.tbpRestaurar.Controls.Add(Me.txtDescripcionRestaurar)
        Me.tbpRestaurar.Controls.Add(Me.pgbProgresoGeneralRestaurar)
        Me.tbpRestaurar.Controls.Add(Me.pgbProgresoParticularRestaurar)
        Me.tbpRestaurar.Controls.Add(Me.lblRutaRestaurar)
        Me.tbpRestaurar.Controls.Add(Me.btnComenzarRestaurar)
        Me.tbpRestaurar.Controls.Add(Me.txtRutaRestaurar)
        Me.tbpRestaurar.Flags = 65534
        Me.tbpRestaurar.LastVisibleSet = True
        resources.ApplyResources(Me.tbpRestaurar, "tbpRestaurar")
        Me.tbpRestaurar.Name = "tbpRestaurar"
        Me.tbpRestaurar.UniqueName = "532DF874A55F48DB4FAD3DB5E5E22181"
        '
        'dgvInformacionArchivo
        '
        Me.dgvInformacionArchivo.AllowUserToAddRows = False
        Me.dgvInformacionArchivo.AllowUserToDeleteRows = False
        Me.dgvInformacionArchivo.AllowUserToResizeColumns = False
        Me.dgvInformacionArchivo.AllowUserToResizeRows = False
        resources.ApplyResources(Me.dgvInformacionArchivo, "dgvInformacionArchivo")
        Me.dgvInformacionArchivo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvInformacionArchivo.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colAplicacion, Me.colVersion, Me.colFechaBackup})
        Me.dgvInformacionArchivo.MultiSelect = False
        Me.dgvInformacionArchivo.Name = "dgvInformacionArchivo"
        Me.dgvInformacionArchivo.ReadOnly = True
        Me.dgvInformacionArchivo.RowHeadersVisible = False
        Me.dgvInformacionArchivo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        '
        'colAplicacion
        '
        Me.colAplicacion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        resources.ApplyResources(Me.colAplicacion, "colAplicacion")
        Me.colAplicacion.Name = "colAplicacion"
        Me.colAplicacion.ReadOnly = True
        '
        'colVersion
        '
        Me.colVersion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        resources.ApplyResources(Me.colVersion, "colVersion")
        Me.colVersion.Name = "colVersion"
        Me.colVersion.ReadOnly = True
        '
        'colFechaBackup
        '
        Me.colFechaBackup.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        DataGridViewCellStyle1.Format = "F"
        DataGridViewCellStyle1.NullValue = Nothing
        Me.colFechaBackup.DefaultCellStyle = DataGridViewCellStyle1
        resources.ApplyResources(Me.colFechaBackup, "colFechaBackup")
        Me.colFechaBackup.Name = "colFechaBackup"
        Me.colFechaBackup.ReadOnly = True
        '
        'picProgresoRestaurar
        '
        resources.ApplyResources(Me.picProgresoRestaurar, "picProgresoRestaurar")
        Me.picProgresoRestaurar.BackColor = System.Drawing.Color.Transparent
        Me.picProgresoRestaurar.Image = Global.Escritorio.My.Resources.Resources.Cargando_32
        Me.picProgresoRestaurar.Name = "picProgresoRestaurar"
        Me.picProgresoRestaurar.TabStop = False
        '
        'txtDescripcionRestaurar
        '
        resources.ApplyResources(Me.txtDescripcionRestaurar, "txtDescripcionRestaurar")
        Me.txtDescripcionRestaurar.Name = "txtDescripcionRestaurar"
        Me.txtDescripcionRestaurar.ReadOnly = True
        Me.txtDescripcionRestaurar.TabStop = False
        '
        'pgbProgresoGeneralRestaurar
        '
        resources.ApplyResources(Me.pgbProgresoGeneralRestaurar, "pgbProgresoGeneralRestaurar")
        Me.pgbProgresoGeneralRestaurar.Name = "pgbProgresoGeneralRestaurar"
        '
        'pgbProgresoParticularRestaurar
        '
        resources.ApplyResources(Me.pgbProgresoParticularRestaurar, "pgbProgresoParticularRestaurar")
        Me.pgbProgresoParticularRestaurar.Name = "pgbProgresoParticularRestaurar"
        '
        'lblRutaRestaurar
        '
        resources.ApplyResources(Me.lblRutaRestaurar, "lblRutaRestaurar")
        Me.lblRutaRestaurar.Name = "lblRutaRestaurar"
        Me.lblRutaRestaurar.Values.Text = resources.GetString("lblRutaRestaurar.Values.Text")
        '
        'btnComenzarRestaurar
        '
        resources.ApplyResources(Me.btnComenzarRestaurar, "btnComenzarRestaurar")
        Me.btnComenzarRestaurar.Name = "btnComenzarRestaurar"
        Me.btnComenzarRestaurar.Values.Text = resources.GetString("btnComenzarRestaurar.Values.Text")
        '
        'txtRutaRestaurar
        '
        resources.ApplyResources(Me.txtRutaRestaurar, "txtRutaRestaurar")
        Me.txtRutaRestaurar.ButtonSpecs.AddRange(New ComponentFactory.Krypton.Toolkit.ButtonSpecAny() {Me.btnBuscarArchivoRestaurar})
        Me.txtRutaRestaurar.Name = "txtRutaRestaurar"
        Me.txtRutaRestaurar.ReadOnly = True
        '
        'btnBuscarArchivoRestaurar
        '
        Me.btnBuscarArchivoRestaurar.Image = Global.Escritorio.My.Resources.Resources.Buscar_16
        Me.btnBuscarArchivoRestaurar.UniqueName = "E62D69ACEB654496279D6CD3E6AFFF61"
        '
        'grpCabecera
        '
        resources.ApplyResources(Me.grpCabecera, "grpCabecera")
        Me.grpCabecera.GroupBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.PanelCustom1
        Me.grpCabecera.Name = "grpCabecera"
        '
        'grpCabecera.Panel
        '
        Me.grpCabecera.Panel.Controls.Add(Me.lblTitulo)
        Me.grpCabecera.StateCommon.Border.Color1 = System.Drawing.Color.DarkOrange
        Me.grpCabecera.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom
        Me.grpCabecera.StateCommon.Border.Width = 5
        '
        'lblTitulo
        '
        Me.lblTitulo.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.Custom1
        resources.ApplyResources(Me.lblTitulo, "lblTitulo")
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Values.Image = Global.Escritorio.My.Resources.Resources.CopiaSeguridad_32
        Me.lblTitulo.Values.Text = resources.GetString("lblTitulo.Values.Text")
        '
        'bgRealizarCopia
        '
        '
        'bgRestaurarCopia
        '
        '
        'epErrores
        '
        Me.epErrores.Alineacion = System.Windows.Forms.ErrorIconAlignment.TopLeft
        Me.epErrores.ContainerControl = Me
        '
        'frmProgresoCopiaSeguridad
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.pCuerpo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmProgresoCopiaSeguridad"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        CType(Me.pCuerpo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pCuerpo.ResumeLayout(False)
        CType(Me.tabGeneral, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabGeneral.ResumeLayout(False)
        CType(Me.tbpRealizar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpRealizar.ResumeLayout(False)
        Me.tbpRealizar.PerformLayout()
        CType(Me.picProgresoRealizar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbpRestaurar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpRestaurar.ResumeLayout(False)
        Me.tbpRestaurar.PerformLayout()
        CType(Me.dgvInformacionArchivo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picProgresoRestaurar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grpCabecera.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpCabecera.Panel.ResumeLayout(False)
        Me.grpCabecera.Panel.PerformLayout()
        CType(Me.grpCabecera, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpCabecera.ResumeLayout(False)
        CType(Me.epErrores, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pCuerpo As ComponentFactory.Krypton.Toolkit.KryptonPanel
    Friend WithEvents epErrores As Quadralia.Controles.Validacion.GestorErrores
    Friend WithEvents pgbProgresoGeneralRealizar As System.Windows.Forms.ProgressBar
    Friend WithEvents pgbProgresoParticularRealizar As System.Windows.Forms.ProgressBar
    Friend WithEvents btnComenzarRealizar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents lblRutaRealizar As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtRutaRealizar As ComponentFactory.Krypton.Toolkit.KryptonTextBox
    Friend WithEvents btnBuscarArchivoRealizar As ComponentFactory.Krypton.Toolkit.ButtonSpecAny
    Friend WithEvents bgRealizarCopia As System.ComponentModel.BackgroundWorker
    Friend WithEvents tabGeneral As ComponentFactory.Krypton.Navigator.KryptonNavigator
    Friend WithEvents tbpRealizar As ComponentFactory.Krypton.Navigator.KryptonPage
    Friend WithEvents tbpRestaurar As ComponentFactory.Krypton.Navigator.KryptonPage
    Friend WithEvents txtDescripcionRealizar As ComponentFactory.Krypton.Toolkit.KryptonTextBox
    Friend WithEvents picProgresoRealizar As System.Windows.Forms.PictureBox
    Friend WithEvents picProgresoRestaurar As System.Windows.Forms.PictureBox
    Friend WithEvents txtDescripcionRestaurar As ComponentFactory.Krypton.Toolkit.KryptonTextBox
    Friend WithEvents pgbProgresoGeneralRestaurar As System.Windows.Forms.ProgressBar
    Friend WithEvents pgbProgresoParticularRestaurar As System.Windows.Forms.ProgressBar
    Friend WithEvents lblRutaRestaurar As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents btnComenzarRestaurar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents txtRutaRestaurar As ComponentFactory.Krypton.Toolkit.KryptonTextBox
    Friend WithEvents btnBuscarArchivoRestaurar As ComponentFactory.Krypton.Toolkit.ButtonSpecAny
    Friend WithEvents dgvInformacionArchivo As ComponentFactory.Krypton.Toolkit.KryptonDataGridView
    Friend WithEvents bgRestaurarCopia As System.ComponentModel.BackgroundWorker
    Friend WithEvents colAplicacion As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents colVersion As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents colFechaBackup As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents grpCabecera As ComponentFactory.Krypton.Toolkit.KryptonGroup
    Friend WithEvents lblTitulo As ComponentFactory.Krypton.Toolkit.KryptonLabel
End Class
