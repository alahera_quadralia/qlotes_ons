﻿Imports System.Runtime.InteropServices

Public Class frmNuevoAviso
#Region " EVENTOS "
    Public Event ClickVentana(ByRef Ventana As frmNuevoAviso)
#End Region
#Region " CONSTANTES "
    Private Const PASOS As Integer = 60
    Private Const MILISENGUDOS_ANIMACION As Integer = 600
    Private Const OPACIDAD_FINAL As Double = 0.8
#End Region
#Region " PROPIEDADES "
    ''' <summary>
    ''' Titulo de la ventana de aviso
    ''' </summary>
    Public Property Titulo As String
        Get
            Return hdrTitulo.Text
        End Get
        Set(ByVal value As String)
            hdrTitulo.Text = value
            Me.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Cuerpo del mensaje a mostrar
    ''' </summary>
    Public Property Cuerpo As String
        Get
            Return lblMensaje.Text
        End Get
        Set(ByVal value As String)
            lblMensaje.Text = value
        End Set
    End Property

    Public Property Imagen As Image
        Get
            Return picImagen.Image
        End Get
        Set(ByVal value As Image)
            picImagen.Image = value
        End Set
    End Property

    Public Property TiempoAviso As Integer = 0
    Public Property Resultado As cNotificationManager.OcultarNoticacion = Nothing

    Dim tiempoTranscurrido As Integer = 0   ' Tiempo desde que este esta visible
    Dim FadeIn As Boolean = True
    Dim Moviendo As Boolean = False
    Dim posicionX As Integer
    Dim posicionY As Integer

    Protected Overrides ReadOnly Property ShowWithoutActivation As Boolean
        Get
            Return True
        End Get
    End Property

    Public Property Opacidad As Double
        Get
            Return Me.Opacity
        End Get
        Set(ByVal value As Double)
            If value < 0 Then value = 0
            If value > 1 Then value = 1
            Me.Opacity = value
        End Set
    End Property

    ''' <summary>
    ''' Con esto evito que la ventana aparezca al hacer ALT + TAB
    ''' </summary>
    Protected Overrides ReadOnly Property CreateParams() As CreateParams
        Get
            Dim cp As CreateParams = MyBase.CreateParams
            ' turn on WS_EX_TOOLWINDOW style bit
            cp.ExStyle = cp.ExStyle Or (WS_EX_NOACTIVATE Or WS_EX_TOOLWINDOW)
            Return cp
        End Get
    End Property
#End Region
#Region " APIS "
#Region "API Allways On Top"
    <DllImport("user32.dll")> _
    Public Shared Function SetWindowPos(ByVal hWnd As IntPtr, ByVal hWndInsertAfter As IntPtr, ByVal X As Integer, ByVal Y As Integer, ByVal cx As Integer, ByVal cy As Integer, _
                                            ByVal uFlags As UInteger) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

    Shared ReadOnly HWND_TOPMOST As New IntPtr(-1)
    Shared ReadOnly HWND_NOTOPMOST As New IntPtr(-2)
    Shared ReadOnly HWND_TOP As New IntPtr(0)
    Shared ReadOnly HWND_BOTTOM As New IntPtr(1)
    Const SWP_NOSIZE As UInt32 = &H1
    Const SWP_NOMOVE As UInt32 = &H2
    Public Const SWP_NOACTIVATE = &H10
    Public Const SWP_SHOWWINDOW = &H4
    Const TOPMOST_FLAGS As UInt32 = SWP_NOMOVE Or SWP_NOSIZE
    Private Const WS_EX_TOOLWINDOW As Integer = &H80
    Private Const WS_EX_NOACTIVATE As Integer = &H8000000
#End Region
#Region " API MOVER FORM "
    Public Const WM_NCLBUTTONDOWN As Integer = &HA1
    Public Const HT_CAPTION As Integer = &H2

    <DllImportAttribute("user32.dll")> _
    Public Shared Function SendMessage(ByVal hWnd As IntPtr, ByVal Msg As Integer, ByVal wParam As Integer, ByVal lParam As Integer) As Integer
    End Function
    <DllImportAttribute("user32.dll")> _
    Public Shared Function ReleaseCapture() As Boolean
    End Function
#End Region
#End Region
#Region " INICIO / FIN "
    Public Sub New()

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        tFade.Interval = MILISENGUDOS_ANIMACION / PASOS
        FadeIn = True
        Me.Visible = False
        Me.Opacity = 0.01
        Me.TopMost = False
        SetWindowPos(Me.Handle, HWND_TOPMOST, 0, 0, 0, 0, TOPMOST_FLAGS Or SWP_NOACTIVATE)
    End Sub

    Private Sub Inicio(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        tFade.Start()
        If TiempoAviso > 0 Then tTiempoAviso.Start()
    End Sub

    Public Sub Cerrar()
        btnCerrarAviso.PerformClick()
    End Sub

    Private Sub CerrarVentana(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrarAviso.Click
        FadeIn = False
        tFade.Start()
    End Sub
#End Region
#Region " TIMERS "
    Private Sub Fade(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tFade.Tick
        If FadeIn Then
            If Not Me.Visible Then Me.Visible = True
            Me.Opacity += (OPACIDAD_FINAL / PASOS)
            If Me.Opacity >= OPACIDAD_FINAL Then tFade.Stop()
        ElseIf Not VentanaActiva() Then
            Me.Opacity -= (OPACIDAD_FINAL / PASOS)
            If Me.Opacity = 0 Then
                tFade.Stop()
                Me.Close()
            End If
        End If
    End Sub

    Private Sub tTiempoAviso_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tTiempoAviso.Tick
        tiempoTranscurrido += 1
        If tiempoTranscurrido = TiempoAviso Then
            tTiempoAviso.Stop()
            btnCerrarAviso.PerformClick()
        End If
    End Sub
#End Region
#Region " CONTROL MOUSE "
    Private Sub ControlMouse(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles gFondo.MouseClick, picImagen.MouseClick, lblMensaje.MouseClick, hdrTitulo.MouseClick
        If e.Button = Windows.Forms.MouseButtons.Middle Then
            btnCerrarAviso.PerformClick()
        ElseIf sender IsNot hdrTitulo Then
            RaiseEvent ClickVentana(Me)
        End If
    End Sub
#End Region
#Region " ARRASTRAR FORMULARIO "
    ''' <summary>
    ''' Mueve el formulario cuando se arrastras
    ''' </summary>
    Private Sub MoverFormulario(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles hdrTitulo.MouseDown
        If e.Button = MouseButtons.Left Then
            Moviendo = True
            posicionX = Cursor.Position.X - Left
            posicionY = Cursor.Position.Y - Top
        End If
    End Sub

    Private Sub DesplazarFormulario(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles hdrTitulo.MouseMove
        If Moviendo Then
            Left = Cursor.Position.X - posicionX
            Top = Cursor.Position.Y - posicionY
        End If
    End Sub

    Private Sub MoverFormulario_Release(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles hdrTitulo.MouseUp
        If e.Button = MouseButtons.Left Then
            Moviendo = False
        End If
    End Sub
#End Region
#Region " RESALTE EN HOVER "
    Private Sub DarResalte(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.MouseEnter, gFondo.MouseEnter, lblMensaje.MouseEnter, picImagen.MouseEnter, hdrTitulo.MouseEnter
        If VentanaActiva() AndAlso Me.Opacidad >= OPACIDAD_FINAL Then Me.Opacidad = 1
    End Sub

    Private Sub QuitarResalte(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.MouseLeave, gFondo.MouseLeave, lblMensaje.MouseLeave, picImagen.MouseLeave, hdrTitulo.MouseLeave
        If Not VentanaActiva() AndAlso Me.Opacidad = 1 Then Me.Opacidad = OPACIDAD_FINAL
    End Sub
#End Region
#Region " RESTO DE FUNCIONES "
    Private Function VentanaActiva() As Boolean
        If VentanaActiva(Me) Then Return True

        If VentanaActiva(mnuOpcionesVentana) Then Return True

        For Each it As ToolStripItem In mnuOpcionesVentana.Items
            If it.Selected Then Return True
        Next

        For Each it As ToolStripItem In mnuOcultarDurante.DropDownItems
            If it.Selected Then Return True
        Next

        Return False
    End Function

    Private Function VentanaActiva(ByVal Padre As Control) As Boolean
        If Padre Is Nothing Then Padre = Me

        ' Recorro todos los controles mirando si tengo el mouse sobre alguno de ellos
        For Each ctrl As Control In Padre.Controls
            If VentanaActiva(ctrl) Then Return True
        Next

        Return Padre.DisplayRectangle.Contains(Padre.PointToClient(Control.MousePosition))
    End Function
#End Region
#Region " OCULTAR NOTIFICACIONES "
    Private Sub Ocultar15m(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuOcultarDurante15m.Click
        Me.Resultado = New cNotificationManager.OcultarNoticacion
        Me.Resultado.Tipo = cNotificationManager.OcultarNoticacion.TipoOcultacion.Temporal
        Me.Resultado.FechaFinAviso = DateTime.Now.AddMinutes(15)
        Me.Resultado.Icono = picImagen.Image

        btnCerrarAviso.PerformClick()
    End Sub

    Private Sub Ocultar30m(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuOcultarDurante30m.Click
        Me.Resultado = New cNotificationManager.OcultarNoticacion
        Me.Resultado.Tipo = cNotificationManager.OcultarNoticacion.TipoOcultacion.Temporal
        Me.Resultado.FechaFinAviso = DateTime.Now.AddMinutes(30)
        Me.Resultado.Icono = picImagen.Image

        btnCerrarAviso.PerformClick()
    End Sub

    Private Sub Ocultar1h(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuOcultarDurante1h.Click
        Me.Resultado = New cNotificationManager.OcultarNoticacion
        Me.Resultado.Tipo = cNotificationManager.OcultarNoticacion.TipoOcultacion.Temporal
        Me.Resultado.FechaFinAviso = DateTime.Now.AddHours(1)
        Me.Resultado.Icono = picImagen.Image

        btnCerrarAviso.PerformClick()
    End Sub

    Private Sub Ocultar4h(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuOcultarDurante4h.Click
        Me.Resultado = New cNotificationManager.OcultarNoticacion
        Me.Resultado.Tipo = cNotificationManager.OcultarNoticacion.TipoOcultacion.Temporal
        Me.Resultado.FechaFinAviso = DateTime.Now.AddHours(4)
        Me.Resultado.Icono = picImagen.Image

        btnCerrarAviso.PerformClick()
    End Sub

    Private Sub OcultarMismoTipo(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuOcultarTipo.Click
        Me.Resultado = New cNotificationManager.OcultarNoticacion
        Me.Resultado.Tipo = cNotificationManager.OcultarNoticacion.TipoOcultacion.MismoTipo
        Me.Resultado.FechaFinAviso = Nothing
        Me.Resultado.Icono = picImagen.Image

        btnCerrarAviso.PerformClick()
    End Sub

    Private Sub OcultarTodos(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuOcultarTodos.Click
        Me.Resultado = New cNotificationManager.OcultarNoticacion
        Me.Resultado.Tipo = cNotificationManager.OcultarNoticacion.TipoOcultacion.Todos
        Me.Resultado.FechaFinAviso = Nothing
        Me.Resultado.Icono = picImagen.Image

        btnCerrarAviso.PerformClick()
    End Sub
#End Region
End Class