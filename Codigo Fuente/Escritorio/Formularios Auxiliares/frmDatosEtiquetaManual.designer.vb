﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDatosEtiquetaManual
    Inherits ComponentFactory.Krypton.Toolkit.KryptonForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDatosEtiquetaManual))
        Me.btnCancelar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.btnAceptar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.lblExpedidor = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.cboExpedidor = New Escritorio.Quadralia.Controles.aComboBox()
        Me.lblLineaEntrada = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtLineaLoteEntrada = New Escritorio.cBuscadorLoteEntrada()
        Me.lblPeso = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblFecha = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.dtpFecha = New Escritorio.Quadralia.Controles.aDateTimePicker()
        Me.txtPeso = New Escritorio.Quadralia.Controles.aTextBox()
        Me.epErrores = New Escritorio.Quadralia.Controles.Validacion.GestorErrores()
        Me.lblDisenho = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.cboDisenho = New Escritorio.Quadralia.Controles.aComboBox()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.cboExpedidor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.epErrores, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboDisenho, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCancelar
        '
        Me.btnCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancelar.Location = New System.Drawing.Point(380, 147)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(84, 24)
        Me.btnCancelar.TabIndex = 7
        Me.btnCancelar.Values.Text = "&Cancelar"
        '
        'btnAceptar
        '
        Me.btnAceptar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAceptar.Location = New System.Drawing.Point(290, 147)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(84, 24)
        Me.btnAceptar.TabIndex = 6
        Me.btnAceptar.Values.Text = "&Aceptar"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.Controls.Add(Me.cboDisenho, 1, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.btnCancelar, 2, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.btnAceptar, 1, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.lblExpedidor, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.cboExpedidor, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.lblLineaEntrada, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.txtLineaLoteEntrada, 1, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.lblPeso, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.lblFecha, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.dtpFecha, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.txtPeso, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.lblDisenho, 0, 4)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 6
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(467, 174)
        Me.TableLayoutPanel1.TabIndex = 8
        '
        'lblExpedidor
        '
        Me.lblExpedidor.Location = New System.Drawing.Point(3, 56)
        Me.lblExpedidor.Name = "lblExpedidor"
        Me.lblExpedidor.Size = New System.Drawing.Size(65, 20)
        Me.lblExpedidor.TabIndex = 8
        Me.lblExpedidor.Values.Text = "Expedidor"
        '
        'cboExpedidor
        '
        Me.TableLayoutPanel1.SetColumnSpan(Me.cboExpedidor, 2)
        Me.cboExpedidor.controlarBotonBorrar = True
        Me.cboExpedidor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExpedidor.DropDownWidth = 131
        Me.cboExpedidor.Location = New System.Drawing.Point(88, 56)
        Me.cboExpedidor.mostrarSiempreBotonBorrar = False
        Me.cboExpedidor.Name = "cboExpedidor"
        Me.cboExpedidor.Size = New System.Drawing.Size(376, 21)
        Me.cboExpedidor.TabIndex = 13
        '
        'lblLineaEntrada
        '
        Me.lblLineaEntrada.Location = New System.Drawing.Point(3, 82)
        Me.lblLineaEntrada.Name = "lblLineaEntrada"
        Me.lblLineaEntrada.Size = New System.Drawing.Size(79, 20)
        Me.lblLineaEntrada.TabIndex = 8
        Me.lblLineaEntrada.Values.Text = "Lote Entrada"
        '
        'txtLineaLoteEntrada
        '
        Me.TableLayoutPanel1.SetColumnSpan(Me.txtLineaLoteEntrada, 2)
        Me.txtLineaLoteEntrada.DescripcionReadOnly = False
        Me.txtLineaLoteEntrada.DescripcionVisible = True
        Me.txtLineaLoteEntrada.DisplayMember = "Descripcion"
        Me.txtLineaLoteEntrada.DisplayText = ""
        Me.txtLineaLoteEntrada.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtLineaLoteEntrada.FormularioBusqueda = "Escritorio.frmSeleccionarLoteEntrada"
        Me.txtLineaLoteEntrada.Item = Nothing
        Me.txtLineaLoteEntrada.Location = New System.Drawing.Point(88, 82)
        Me.txtLineaLoteEntrada.Name = "txtLineaLoteEntrada"
        Me.txtLineaLoteEntrada.Size = New System.Drawing.Size(376, 22)
        Me.txtLineaLoteEntrada.TabIndex = 14
        Me.txtLineaLoteEntrada.UseOnlyNumbers = False
        Me.txtLineaLoteEntrada.UseUpperCase = True
        Me.txtLineaLoteEntrada.Validar = True
        Me.txtLineaLoteEntrada.ValueMember = "codigo"
        Me.txtLineaLoteEntrada.ValueText = ""
        '
        'lblPeso
        '
        Me.lblPeso.Location = New System.Drawing.Point(3, 3)
        Me.lblPeso.Name = "lblPeso"
        Me.lblPeso.Size = New System.Drawing.Size(36, 20)
        Me.lblPeso.TabIndex = 8
        Me.lblPeso.Values.Text = "Peso"
        '
        'lblFecha
        '
        Me.lblFecha.Location = New System.Drawing.Point(3, 29)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(42, 20)
        Me.lblFecha.TabIndex = 8
        Me.lblFecha.Values.Text = "Fecha"
        '
        'dtpFecha
        '
        Me.dtpFecha.CalendarTodayText = "Hoy:"
        Me.TableLayoutPanel1.SetColumnSpan(Me.dtpFecha, 2)
        Me.dtpFecha.controlarBotonBorrar = True
        Me.dtpFecha.CustomNullText = "(Sin fecha)"
        Me.dtpFecha.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dtpFecha.Location = New System.Drawing.Point(88, 29)
        Me.dtpFecha.mostrarSiempreBotonBorrar = False
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(376, 21)
        Me.dtpFecha.TabIndex = 15
        Me.dtpFecha.TipoLimpieza = Escritorio.Quadralia.Controles.aDateTimePicker.TiposLimpieza.Value
        '
        'txtPeso
        '
        Me.TableLayoutPanel1.SetColumnSpan(Me.txtPeso, 2)
        Me.txtPeso.controlarBotonBorrar = True
        Me.txtPeso.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtPeso.Formato = ""
        Me.txtPeso.Location = New System.Drawing.Point(88, 3)
        Me.txtPeso.mostrarSiempreBotonBorrar = False
        Me.txtPeso.Name = "txtPeso"
        Me.txtPeso.seleccionarTodo = True
        Me.txtPeso.Size = New System.Drawing.Size(376, 20)
        Me.txtPeso.TabIndex = 16
        '
        'epErrores
        '
        Me.epErrores.Alineacion = System.Windows.Forms.ErrorIconAlignment.TopLeft
        Me.epErrores.ContainerControl = Me
        '
        'lblDisenho
        '
        Me.lblDisenho.Location = New System.Drawing.Point(3, 110)
        Me.lblDisenho.Name = "lblDisenho"
        Me.lblDisenho.Size = New System.Drawing.Size(55, 20)
        Me.lblDisenho.TabIndex = 8
        Me.lblDisenho.Values.Text = "Etiqueta"
        '
        'cboDisenho
        '
        Me.TableLayoutPanel1.SetColumnSpan(Me.cboDisenho, 2)
        Me.cboDisenho.controlarBotonBorrar = True
        Me.cboDisenho.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDisenho.DropDownWidth = 131
        Me.cboDisenho.Location = New System.Drawing.Point(88, 110)
        Me.cboDisenho.mostrarSiempreBotonBorrar = False
        Me.cboDisenho.Name = "cboDisenho"
        Me.cboDisenho.Size = New System.Drawing.Size(376, 21)
        Me.cboDisenho.TabIndex = 17
        '
        'frmDatosEtiquetaManual
        '
        Me.AcceptButton = Me.btnAceptar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.CancelButton = Me.btnCancelar
        Me.ClientSize = New System.Drawing.Size(467, 174)
        Me.ControlBox = False
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDatosEtiquetaManual"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Establecer parámetros"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        CType(Me.cboExpedidor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.epErrores, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboDisenho, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnCancelar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents btnAceptar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents epErrores As Quadralia.Controles.Validacion.GestorErrores
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblExpedidor As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblLineaEntrada As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblPeso As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblFecha As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Private WithEvents cboExpedidor As Escritorio.Quadralia.Controles.aComboBox
    Private WithEvents txtLineaLoteEntrada As Escritorio.cBuscadorLoteEntrada
    Private WithEvents dtpFecha As Escritorio.Quadralia.Controles.aDateTimePicker
    Private WithEvents txtPeso As Escritorio.Quadralia.Controles.aTextBox
    Private WithEvents cboDisenho As Escritorio.Quadralia.Controles.aComboBox
    Friend WithEvents lblDisenho As ComponentFactory.Krypton.Toolkit.KryptonLabel
End Class
