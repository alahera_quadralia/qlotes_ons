﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSeleccionarEspecies
    Inherits ComponentFactory.Krypton.Toolkit.KryptonForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSeleccionarEspecies))
        Me.hdrContenido = New ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup()
        Me.tblResultados = New System.Windows.Forms.TableLayoutPanel()
        Me.lblNombre = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.cboSeleccionar = New Escritorio.Quadralia.Controles.aComboBox()
        Me.txtBNombre = New Escritorio.Quadralia.Controles.aTextBox()
        Me.grpResultadoTarifas = New ComponentFactory.Krypton.Toolkit.KryptonGroupBox()
        Me.dgvRegistros = New ComponentFactory.Krypton.Toolkit.KryptonDataGridView()
        Me.colAlfa3 = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colComercial = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colCientifica = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.lblSeleccionar = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.btnBBuscar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.hdrCabecera = New ComponentFactory.Krypton.Toolkit.KryptonHeader()
        Me.pCuerpo = New ComponentFactory.Krypton.Toolkit.KryptonPanel()
        Me.pBotonera = New ComponentFactory.Krypton.Toolkit.KryptonPanel()
        Me.btnAceptar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.btnCerrar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.ttpInfo = New System.Windows.Forms.ToolTip(Me.components)
        CType(Me.hdrContenido, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.hdrContenido.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.hdrContenido.Panel.SuspendLayout()
        Me.hdrContenido.SuspendLayout()
        Me.tblResultados.SuspendLayout()
        CType(Me.cboSeleccionar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpResultadoTarifas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpResultadoTarifas.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpResultadoTarifas.Panel.SuspendLayout()
        Me.grpResultadoTarifas.SuspendLayout()
        CType(Me.dgvRegistros, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pCuerpo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pCuerpo.SuspendLayout()
        CType(Me.pBotonera, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pBotonera.SuspendLayout()
        Me.SuspendLayout()
        '
        'hdrContenido
        '
        resources.ApplyResources(Me.hdrContenido, "hdrContenido")
        Me.hdrContenido.HeaderVisibleSecondary = False
        Me.hdrContenido.Name = "hdrContenido"
        '
        'hdrContenido.Panel
        '
        Me.hdrContenido.Panel.Controls.Add(Me.tblResultados)
        Me.hdrContenido.ValuesPrimary.Heading = resources.GetString("hdrContenido.ValuesPrimary.Heading")
        Me.hdrContenido.ValuesPrimary.Image = CType(resources.GetObject("hdrContenido.ValuesPrimary.Image"), System.Drawing.Image)
        '
        'tblResultados
        '
        Me.tblResultados.BackColor = System.Drawing.Color.White
        resources.ApplyResources(Me.tblResultados, "tblResultados")
        Me.tblResultados.Controls.Add(Me.lblNombre, 0, 1)
        Me.tblResultados.Controls.Add(Me.cboSeleccionar, 1, 3)
        Me.tblResultados.Controls.Add(Me.txtBNombre, 1, 1)
        Me.tblResultados.Controls.Add(Me.grpResultadoTarifas, 0, 4)
        Me.tblResultados.Controls.Add(Me.lblSeleccionar, 0, 3)
        Me.tblResultados.Controls.Add(Me.btnBBuscar, 2, 3)
        Me.tblResultados.Controls.Add(Me.hdrCabecera, 0, 0)
        Me.tblResultados.Name = "tblResultados"
        '
        'lblNombre
        '
        resources.ApplyResources(Me.lblNombre, "lblNombre")
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Values.Text = resources.GetString("lblNombre.Values.Text")
        '
        'cboSeleccionar
        '
        resources.ApplyResources(Me.cboSeleccionar, "cboSeleccionar")
        Me.cboSeleccionar.controlarBotonBorrar = False
        Me.cboSeleccionar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSeleccionar.DropDownWidth = 352
        Me.cboSeleccionar.mostrarSiempreBotonBorrar = False
        Me.cboSeleccionar.Name = "cboSeleccionar"
        Me.ttpInfo.SetToolTip(Me.cboSeleccionar, resources.GetString("cboSeleccionar.ToolTip"))
        '
        'txtBNombre
        '
        Me.txtBNombre.AlwaysActive = False
        Me.tblResultados.SetColumnSpan(Me.txtBNombre, 2)
        Me.txtBNombre.controlarBotonBorrar = True
        resources.ApplyResources(Me.txtBNombre, "txtBNombre")
        Me.txtBNombre.Formato = ""
        Me.txtBNombre.mostrarSiempreBotonBorrar = False
        Me.txtBNombre.Name = "txtBNombre"
        Me.txtBNombre.seleccionarTodo = True
        '
        'grpResultadoTarifas
        '
        Me.tblResultados.SetColumnSpan(Me.grpResultadoTarifas, 3)
        resources.ApplyResources(Me.grpResultadoTarifas, "grpResultadoTarifas")
        Me.grpResultadoTarifas.Name = "grpResultadoTarifas"
        '
        'grpResultadoTarifas.Panel
        '
        Me.grpResultadoTarifas.Panel.Controls.Add(Me.dgvRegistros)
        Me.grpResultadoTarifas.Values.Heading = resources.GetString("grpResultadoTarifas.Values.Heading")
        '
        'dgvRegistros
        '
        Me.dgvRegistros.AllowUserToAddRows = False
        Me.dgvRegistros.AllowUserToDeleteRows = False
        Me.dgvRegistros.AllowUserToResizeRows = False
        Me.dgvRegistros.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colAlfa3, Me.colComercial, Me.colCientifica})
        resources.ApplyResources(Me.dgvRegistros, "dgvRegistros")
        Me.dgvRegistros.MultiSelect = False
        Me.dgvRegistros.Name = "dgvRegistros"
        Me.dgvRegistros.RowHeadersVisible = False
        Me.dgvRegistros.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        '
        'colAlfa3
        '
        Me.colAlfa3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colAlfa3.DataPropertyName = "alfa3"
        resources.ApplyResources(Me.colAlfa3, "colAlfa3")
        Me.colAlfa3.Name = "colAlfa3"
        Me.colAlfa3.ReadOnly = True
        '
        'colComercial
        '
        Me.colComercial.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colComercial.DataPropertyName = "denominacionComercial"
        resources.ApplyResources(Me.colComercial, "colComercial")
        Me.colComercial.Name = "colComercial"
        Me.colComercial.ReadOnly = True
        '
        'colCientifica
        '
        Me.colCientifica.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colCientifica.DataPropertyName = "denominacionCientifica"
        resources.ApplyResources(Me.colCientifica, "colCientifica")
        Me.colCientifica.Name = "colCientifica"
        Me.colCientifica.ReadOnly = True
        '
        'lblSeleccionar
        '
        resources.ApplyResources(Me.lblSeleccionar, "lblSeleccionar")
        Me.lblSeleccionar.Name = "lblSeleccionar"
        Me.lblSeleccionar.Values.Text = resources.GetString("lblSeleccionar.Values.Text")
        '
        'btnBBuscar
        '
        resources.ApplyResources(Me.btnBBuscar, "btnBBuscar")
        Me.btnBBuscar.Name = "btnBBuscar"
        Me.ttpInfo.SetToolTip(Me.btnBBuscar, resources.GetString("btnBBuscar.ToolTip"))
        Me.btnBBuscar.Values.Image = CType(resources.GetObject("btnBBuscar.Values.Image"), System.Drawing.Image)
        Me.btnBBuscar.Values.Text = resources.GetString("btnBBuscar.Values.Text")
        '
        'hdrCabecera
        '
        Me.tblResultados.SetColumnSpan(Me.hdrCabecera, 3)
        resources.ApplyResources(Me.hdrCabecera, "hdrCabecera")
        Me.hdrCabecera.HeaderStyle = ComponentFactory.Krypton.Toolkit.HeaderStyle.Secondary
        Me.hdrCabecera.Name = "hdrCabecera"
        Me.hdrCabecera.Values.Description = resources.GetString("hdrCabecera.Values.Description")
        Me.hdrCabecera.Values.Heading = resources.GetString("hdrCabecera.Values.Heading")
        Me.hdrCabecera.Values.Image = Global.Escritorio.My.Resources.Resources.Buscar_16
        '
        'pCuerpo
        '
        Me.pCuerpo.Controls.Add(Me.hdrContenido)
        Me.pCuerpo.Controls.Add(Me.pBotonera)
        resources.ApplyResources(Me.pCuerpo, "pCuerpo")
        Me.pCuerpo.Name = "pCuerpo"
        '
        'pBotonera
        '
        Me.pBotonera.Controls.Add(Me.btnAceptar)
        Me.pBotonera.Controls.Add(Me.btnCerrar)
        resources.ApplyResources(Me.pBotonera, "pBotonera")
        Me.pBotonera.Name = "pBotonera"
        '
        'btnAceptar
        '
        resources.ApplyResources(Me.btnAceptar, "btnAceptar")
        Me.btnAceptar.Name = "btnAceptar"
        Me.ttpInfo.SetToolTip(Me.btnAceptar, resources.GetString("btnAceptar.ToolTip"))
        Me.btnAceptar.Values.Text = resources.GetString("btnAceptar.Values.Text")
        '
        'btnCerrar
        '
        resources.ApplyResources(Me.btnCerrar, "btnCerrar")
        Me.btnCerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCerrar.Name = "btnCerrar"
        Me.ttpInfo.SetToolTip(Me.btnCerrar, resources.GetString("btnCerrar.ToolTip"))
        Me.btnCerrar.Values.Text = resources.GetString("btnCerrar.Values.Text")
        '
        'frmSeleccionarEspecies
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCerrar
        Me.ControlBox = False
        Me.Controls.Add(Me.pCuerpo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.Name = "frmSeleccionarEspecies"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        CType(Me.hdrContenido.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrContenido.Panel.ResumeLayout(False)
        CType(Me.hdrContenido, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrContenido.ResumeLayout(False)
        Me.tblResultados.ResumeLayout(False)
        Me.tblResultados.PerformLayout()
        CType(Me.cboSeleccionar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grpResultadoTarifas.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpResultadoTarifas.Panel.ResumeLayout(False)
        CType(Me.grpResultadoTarifas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpResultadoTarifas.ResumeLayout(False)
        CType(Me.dgvRegistros, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pCuerpo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pCuerpo.ResumeLayout(False)
        CType(Me.pBotonera, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pBotonera.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pCuerpo As ComponentFactory.Krypton.Toolkit.KryptonPanel
    Friend WithEvents hdrContenido As ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup
    Friend WithEvents pBotonera As ComponentFactory.Krypton.Toolkit.KryptonPanel
    Friend WithEvents btnCerrar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents btnAceptar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents lblNombre As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents grpResultadoTarifas As ComponentFactory.Krypton.Toolkit.KryptonGroupBox
    Friend WithEvents dgvRegistros As ComponentFactory.Krypton.Toolkit.KryptonDataGridView
    Friend WithEvents tblResultados As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents cboSeleccionar As Quadralia.Controles.aComboBox
    Friend WithEvents lblSeleccionar As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtBNombre As Quadralia.Controles.aTextBox
    Friend WithEvents ttpInfo As System.Windows.Forms.ToolTip
    Friend WithEvents btnBBuscar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents hdrCabecera As ComponentFactory.Krypton.Toolkit.KryptonHeader
    Friend WithEvents colAlfa3 As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents colComercial As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents colCientifica As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
End Class
