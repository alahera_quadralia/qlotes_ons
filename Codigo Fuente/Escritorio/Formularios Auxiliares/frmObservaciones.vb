﻿Public Class frmObservaciones
#Region " DECLARACIONES "
    Private _Mensaje As String = String.Empty
#End Region
#Region " PROPIEDADES "
    Public ReadOnly Property Mensaje As String
        Get
            Return _Mensaje
        End Get
    End Property
#End Region
#Region " LIMPIEZA "
    ''' <summary>
    ''' Limpia todos los controles
    ''' </summary>
    Private Sub Limpiar()
        htmlObservaciones.Clear()
    End Sub
#End Region
#Region " CARGAR / GUARDAR / CANCELAR "
    ''' <summary>
    ''' Guarda los datos y sale del formulario
    ''' </summary>
    Private Sub Aceptar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        _Mensaje = htmlObservaciones.Text

        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    ''' <summary>
    ''' El usuario indica que no desea crear un nuevo contacto
    ''' </summary>
    Private Sub Cancelar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        _Mensaje = String.Empty

        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub
#End Region
#Region " RUTINA DE INICIO "    
#End Region
End Class