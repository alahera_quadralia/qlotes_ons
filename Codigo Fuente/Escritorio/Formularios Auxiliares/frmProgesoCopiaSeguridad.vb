﻿Imports Escritorio.Quadralia.BackUpBBDD

Public Class frmProgresoCopiaSeguridad
#Region " DECLARACIONES "
    Private _Estado As Quadralia.Enumerados.EstadoFormulario = Quadralia.Enumerados.EstadoFormulario.Espera
#End Region
#Region " PROPIEDADES "
    Public Property Estado As Quadralia.Enumerados.EstadoFormulario
        Get
            Return _Estado
        End Get
        Set(ByVal value As Quadralia.Enumerados.EstadoFormulario)
            _Estado = value

            btnComenzarRealizar.Enabled = (value = Quadralia.Enumerados.EstadoFormulario.Espera)
            btnBuscarArchivoRealizar.Enabled = IIf(value = Quadralia.Enumerados.EstadoFormulario.Espera, ButtonEnabled.True, ButtonEnabled.False)
            txtRutaRealizar.Enabled = (value = Quadralia.Enumerados.EstadoFormulario.Espera)
            picProgresoRealizar.Visible = (value <> Quadralia.Enumerados.EstadoFormulario.Espera)

            btnComenzarRestaurar.Enabled = (value = Quadralia.Enumerados.EstadoFormulario.Espera)
            btnBuscarArchivoRestaurar.Enabled = IIf(value = Quadralia.Enumerados.EstadoFormulario.Espera, ButtonEnabled.True, ButtonEnabled.False)
            txtRutaRestaurar.Enabled = (value = Quadralia.Enumerados.EstadoFormulario.Espera)
            picProgresoRestaurar.Visible = (value <> Quadralia.Enumerados.EstadoFormulario.Espera)
        End Set
    End Property
#End Region
#Region " LIMPIEZA "
    ''' <summary>
    ''' Limpia todos los controles con datos del formulario
    ''' </summary>
    Public Sub LimpiarDatos()
        txtRutaRealizar.Clear()
        txtRutaRestaurar.Clear()

        txtDescripcionRealizar.Clear()
        txtDescripcionRestaurar.Clear()

        With pgbProgresoGeneralRealizar
            .Minimum = 0
            .Maximum = 100
            .Value = 0
        End With

        With pgbProgresoParticularRealizar
            .Minimum = 0
            .Maximum = 100
            .Value = 0
        End With

        dgvInformacionArchivo.Rows.Clear()
        dgvInformacionArchivo.DataSource = Nothing
    End Sub
#End Region
#Region " RUTINA DE INICIO / FIN "
    ''' <summary>
    ''' Rutina de inicio del formulario
    ''' </summary>
    Private Sub Inicio(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LimpiarDatos()

        tabGeneral.SelectedIndex = 0

        ' Configuro los backgroundworker
        bgRealizarCopia.WorkerReportsProgress = True
        bgRestaurarCopia.WorkerReportsProgress = True
    End Sub

    Private Sub ControlarCierre(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        e.Cancel = Me.Estado <> Quadralia.Enumerados.EstadoFormulario.Espera
    End Sub
#End Region
#Region " REALIZAR BACKUP "
    ''' <summary>
    ''' Comenzar proceso de copia
    ''' </summary>
    Private Sub RealizarBackup(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnComenzarRealizar.Click
        If String.IsNullOrEmpty(txtRutaRealizar.Text) Then Exit Sub

        ' Inicializacion del formulario
        Me.Estado = Quadralia.Enumerados.EstadoFormulario.Editando
        txtDescripcionRealizar.Clear()

        ' Lanzo el proceso
        bgRealizarCopia.RunWorkerAsync(txtRutaRealizar.Text)
    End Sub

    ''' <summary>
    ''' Muestra un cuadro de diálogo para que el usuario seleccione la ruta del fichero
    ''' </summary>
    Private Sub BuscarArchivoRealizar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscarArchivoRealizar.Click
        Dim CuadroDialogo As New SaveFileDialog

        With CuadroDialogo
            .AddExtension = True
            .CheckFileExists = False
            .CheckPathExists = True
            .CreatePrompt = False
            .DefaultExt = ".qbak"
            .RestoreDirectory = True
            .Title = "Guardar Copia de Seguridad "
            .Filter = "Archivos de Copia de Seguridad de Quadralia (qbak)|*.qbak"

            ' Muestro el cuadro de dialogo y guardo el resultado en el textbox
            If .ShowDialog <> Windows.Forms.DialogResult.OK Then Exit Sub
            txtRutaRealizar.Text = .FileName
        End With
    End Sub

#Region " BACKGROUNDWORKER Y PROGRESO "
    ''' <summary>
    ''' Escribimos el nuevo estado en el formulario
    ''' </summary>
    Private Sub CambioEstadoRealizar(ByRef sender As cBackupBBDD, ByVal e As cBackupBBDDCambioEstadoEventArgs)
        Select Case e.EstadoOriginal
            Case EstadosProcesos.CompresionArchivo
                bgRealizarCopia.ReportProgress(1, "Aplicando lógica de compresión sobre el archivo de respaldo")
            Case EstadosProcesos.EncriptacionArchivo
                bgRealizarCopia.ReportProgress(1, "Aplicando lógica de encriptación sobre el archivo de respaldo")
            Case EstadosProcesos.MoviendoArchivo
                bgRealizarCopia.ReportProgress(1, "Moviendo archivo de copia de seguridad")
            Case EstadosProcesos.RealizandoCopiaSeguridad
                bgRealizarCopia.ReportProgress(1, "Realizando copia de seguridad")
            Case EstadosProcesos.RealizandoCopiaSeguridadBBDD
                bgRealizarCopia.ReportProgress(1, "Realizando copia de seguridad del servidor de base de datos")
            Case EstadosProcesos.RestaurandoCopiaSeguridad
                bgRealizarCopia.ReportProgress(1, "Restaurando copia de seguridad")
            Case EstadosProcesos.RestaurandoCopiaSeguridadBBDD
                bgRealizarCopia.ReportProgress(1, "Restaurando copia de seguridad en el servidor de base de datos. Esta operación puede tarder unos minutos")
            Case EstadosProcesos.VerificandoArchivo
                bgRealizarCopia.ReportProgress(1, "Verificando integridad del archivo")
            Case Else
        End Select
    End Sub

    Private Sub MostrarProgresoFormularioRealizar(ByVal sender As System.Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles bgRealizarCopia.ProgressChanged
        If e Is Nothing OrElse e.UserState Is Nothing Then Exit Sub

        Select Case e.ProgressPercentage
            Case 1 ' Cambio de estado
                txtDescripcionRealizar.Text &= e.UserState & Environment.NewLine
            Case 2
                Dim Datos As cBackupBBDDProgresoEventArgs = e.UserState
                If Datos Is Nothing OrElse Datos.Tag Is Nothing Then Exit Sub
                Dim BarraProgreso As ProgressBar = IIf(Datos.Tag = 1, pgbProgresoGeneralRealizar, pgbProgresoParticularRealizar)

                If Datos.ProgresoGeneral < 0 Then
                    BarraProgreso.Value = 0
                Else
                    Integer.TryParse(Math.Ceiling(Datos.ProgresoGeneral), BarraProgreso.Value)
                End If
        End Select
    End Sub

    ''' <summary>
    ''' Mostramos el progreso en las barras de progreso
    ''' </summary>
    Private Sub NotificacionProgresoRealizarGeneral(ByRef sender As cBackupBBDD, ByVal e As cBackupBBDDProgresoEventArgs)
        e.Tag = 1
        bgRealizarCopia.ReportProgress(2, e)
    End Sub

    Private Sub NotificacionProgresoRealizarSecundario(ByRef sender As cBackupBBDD, ByVal e As cBackupBBDDProgresoEventArgs)
        e.Tag = 2
        bgRealizarCopia.ReportProgress(2, e)
    End Sub

    Friend Sub RealizarCopiaSeguridad(ByVal Ruta As String, ByVal MostarMensaje As Boolean)
        Try
            ' Inicializacion del elemento de Copia de Seguridad
            Dim CopSeg As cBackupBBDD = New cBackupBBDDMySql() With { _
                .NombreBBDD = cConfiguracionBBDD.Instancia.BBDDNombre, _
                .Usuario = cConfiguracionBBDD.Instancia.BBDDUsuario, _
                .Clave = cConfiguracionBBDD.Instancia.BBDDPassword, _
                .Puerto = cConfiguracionBBDD.Instancia.BBDDPuerto, _
                .Servidor = cConfiguracionBBDD.Instancia.BBDDServidor
            }

            ' Configuración general del backup
            With CopSeg
                .ClaveEncriptacion = cConfiguracionAplicacion.Instancia.ClaveEncriptacionBackup
                .Comprimir = True
                .Encriptar = True
                .Factoria = cDAL.Instancia.GetFactory
                .LanzarExcepciones = True
                .TiempoEspera = 100
            End With

            ' Añado manejadores
            AddHandler CopSeg.NotificacionProgresoGeneral, AddressOf NotificacionProgresoRealizarGeneral
            AddHandler CopSeg.NotificacionProgresoSecundario, AddressOf NotificacionProgresoRealizarSecundario
            AddHandler CopSeg.CambioEstado, AddressOf CambioEstadoRealizar

            If CopSeg.RealizarCopiaSeguridad(Ruta) Then
                bgRealizarCopia.ReportProgress(1, "Copia de seguridad realizada correctamente")
                If MostarMensaje Then Quadralia.Aplicacion.MostrarMessageBox("El archivo de copia de seguridad se ha generado correctamente", "Proceso Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            bgRealizarCopia.ReportProgress(1, "Error al realizar la copia de seguridad: " & ex.Message)
            If MostarMensaje Then Quadralia.Aplicacion.InformarError(ex, False, False)
        End Try
    End Sub

    Private Sub RealizarTareas(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgRealizarCopia.DoWork
        RealizarCopiaSeguridad(e.Argument, True)
    End Sub

    ''' <summary>
    ''' Finaliza la tarea
    ''' </summary>
    Private Sub FinRestaurar(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgRealizarCopia.RunWorkerCompleted
        Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera
    End Sub
#End Region
#End Region
#Region " RESTAURAR BACKUP "
    ''' <summary>
    ''' Comenzar proceso de copia
    ''' </summary>
    Private Sub RestaurarBackup(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnComenzarRestaurar.Click
        If String.IsNullOrEmpty(txtRutaRestaurar.Text) Then Exit Sub

        ' Inicializacion del formulario
        Me.Estado = Quadralia.Enumerados.EstadoFormulario.Editando
        txtDescripcionRestaurar.Clear()

        ' Lanzo el proceso
        bgRestaurarCopia.RunWorkerAsync(txtRutaRestaurar.Text)
    End Sub

    ''' <summary>
    ''' Muestra un cuadro de diálogo para que el usuario seleccione la ruta del fichero
    ''' </summary>
    Private Sub BuscarArchivoRestaurar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscarArchivoRestaurar.Click
        Dim CuadroDialogo As New OpenFileDialog

        With CuadroDialogo
            .AddExtension = True
            .CheckFileExists = True
            .CheckPathExists = True
            .DefaultExt = ".qbak"
            .RestoreDirectory = True
            .Title = "Seleccionar archivo copia de Seguridad "
            .Filter = "Archivos de Copia de Seguridad de Quadralia (*.qbak)|*.qbak|Todos los archivos|*.*"

            ' Muestro el cuadro de dialogo y guardo el resultado en el textbox
            If .ShowDialog <> Windows.Forms.DialogResult.OK Then Exit Sub
            txtRutaRestaurar.Text = .FileName
        End With
    End Sub

#Region " BACKGROUNDWORKER Y PROGRESO "
    ''' <summary>
    ''' Escribimos el nuevo estado en el formulario
    ''' </summary>
    Private Sub CambioEstadoRestaurar(ByRef sender As cBackupBBDD, ByVal e As cBackupBBDDCambioEstadoEventArgs)
        Select Case e.EstadoOriginal
            Case EstadosProcesos.CompresionArchivo
                bgRestaurarCopia.ReportProgress(1, "Aplicando lógica de compresión sobre el archivo de respaldo")
            Case EstadosProcesos.EncriptacionArchivo
                bgRestaurarCopia.ReportProgress(1, "Aplicando lógica de encriptación sobre el archivo de respaldo")
            Case EstadosProcesos.MoviendoArchivo
                bgRestaurarCopia.ReportProgress(1, "Moviendo archivo de copia de seguridad")
            Case EstadosProcesos.RealizandoCopiaSeguridad
                bgRestaurarCopia.ReportProgress(1, "Realizando copia de seguridad")
            Case EstadosProcesos.RealizandoCopiaSeguridadBBDD
                bgRestaurarCopia.ReportProgress(1, "Realizando copia de seguridad del servidor de base de datos")
            Case EstadosProcesos.RestaurandoCopiaSeguridad
                bgRestaurarCopia.ReportProgress(1, "Restaurando copia de seguridad")
            Case EstadosProcesos.RestaurandoCopiaSeguridadBBDD
                bgRestaurarCopia.ReportProgress(1, "Restaurando copia de seguridad en el servidor de base de datos. Esta operación puede tarder unos minutos")
            Case EstadosProcesos.VerificandoArchivo
                bgRestaurarCopia.ReportProgress(1, "Verificando integridad del archivo")
            Case Else
        End Select
    End Sub

    Private Sub MostrarProgresoFormularioRestaurar(ByVal sender As System.Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles bgRestaurarCopia.ProgressChanged
        If e Is Nothing OrElse e.UserState Is Nothing Then Exit Sub

        Select Case e.ProgressPercentage
            Case 1 ' Cambio de estado
                txtDescripcionRestaurar.Text &= e.UserState & Environment.NewLine
            Case 2
                Dim Datos As cBackupBBDDProgresoEventArgs = e.UserState
                If Datos Is Nothing OrElse Datos.Tag Is Nothing Then Exit Sub
                Dim BarraProgreso As ProgressBar = IIf(Datos.Tag = 1, pgbProgresoGeneralRestaurar, pgbProgresoParticularRestaurar)
                If Datos.ProgresoGeneral < 0 Then
                    BarraProgreso.Value = 0
                Else
                    Integer.TryParse(Math.Ceiling(Datos.ProgresoGeneral), BarraProgreso.Value)
                End If
        End Select
    End Sub

    ''' <summary>
    ''' Mostramos el progreso en las barras de progreso
    ''' </summary>
    Private Sub NotificacionProgresoRestaurarGeneral(ByRef sender As cBackupBBDD, ByVal e As cBackupBBDDProgresoEventArgs)
        e.Tag = 1
        bgRestaurarCopia.ReportProgress(2, e)
    End Sub

    ''' <summary>
    ''' Mostramos el progreso en las barras de progreso
    ''' </summary>
    Private Sub NotificacionProgresoRestaurarSecundario(ByRef sender As cBackupBBDD, ByVal e As cBackupBBDDProgresoEventArgs)
        e.Tag = 2
        bgRestaurarCopia.ReportProgress(2, e)
    End Sub

    Private Sub RestaurarArchivo(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgRestaurarCopia.DoWork
        Try
            ' Inicializacion del elemento de Copia de Seguridad
            Dim CopSeg As cBackupBBDD = New cBackupBBDDMySql() With { _
                .NombreBBDD = cConfiguracionBBDD.Instancia.BBDDNombre, _
                .Usuario = cConfiguracionBBDD.Instancia.BBDDUsuario, _
                .Clave = cConfiguracionBBDD.Instancia.BBDDPassword, _
                .Puerto = cConfiguracionBBDD.Instancia.BBDDPuerto, _
                .Servidor = cConfiguracionBBDD.Instancia.BBDDServidor
            }

            ' Configuración general del backup
            With CopSeg
                .ClaveEncriptacion = cConfiguracionAplicacion.Instancia.ClaveEncriptacionBackup
                .Comprimir = True
                .Encriptar = True
                .Factoria = cDAL.Instancia.GetFactory
                .LanzarExcepciones = True
                .TiempoEspera = 100
            End With

            ' Añado manejadores
            AddHandler CopSeg.NotificacionProgresoGeneral, AddressOf NotificacionProgresoRestaurarGeneral
            AddHandler CopSeg.NotificacionProgresoSecundario, AddressOf NotificacionProgresoRestaurarSecundario
            AddHandler CopSeg.CambioEstado, AddressOf CambioEstadoRestaurar

            If CopSeg.RestaurarCopiaSeguridad(e.Argument) Then
                bgRestaurarCopia.ReportProgress(1, "Copia de seguridad restaurada correctamente")
                Quadralia.Aplicacion.MostrarMessageBox("El archivo de copia de seguridad se ha restaurado correctamente", "Proceso Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            bgRestaurarCopia.ReportProgress(1, "Error al restaurar la copia de seguridad: " & ex.Message)
            Quadralia.Aplicacion.InformarError(ex, False, False)
        End Try
    End Sub

    ''' <summary>
    ''' Finaliza la tarea
    ''' </summary>
    Private Sub FinTareas(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgRestaurarCopia.RunWorkerCompleted
        Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera
    End Sub
#End Region
#End Region
#Region " RESTO DE FUNCIONES "
    Private Sub HabilitarBotonRestaurar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRutaRestaurar.TextChanged
        btnComenzarRestaurar.Enabled = Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera AndAlso Not String.IsNullOrEmpty(txtRutaRestaurar.Text) AndAlso My.Computer.FileSystem.FileExists(txtRutaRestaurar.Text)

        ' Si exise el archivo, trato de leer sus datos
        If btnComenzarRestaurar.Enabled Then
            dgvInformacionArchivo.Rows.Clear()

            Dim InfoBackUp As cBackupBBDD.InfoBackup = cBackupBBDD.obtenerInfoArchivoBackUp(txtRutaRestaurar.Text)
            If String.IsNullOrEmpty(InfoBackUp.NombreAplicacion) Then Exit Sub

            dgvInformacionArchivo.Rows.Add()
            With dgvInformacionArchivo.Rows(0)
                .Cells(colAplicacion.Index).Value = InfoBackUp.NombreAplicacion
                If InfoBackUp.fechaBackUp.HasValue Then .Cells(colFechaBackup.Index).Value = InfoBackUp.fechaBackUp.Value
                .Cells(colVersion.Index).Value = InfoBackUp.Version
            End With

        End If
    End Sub

    Private Sub HabilitarBotonRealizar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRutaRealizar.TextChanged
        btnComenzarRealizar.Enabled = Me.Estado = Quadralia.Enumerados.EstadoFormulario.Espera AndAlso Not String.IsNullOrEmpty(txtRutaRealizar.Text)
    End Sub
#End Region
End Class