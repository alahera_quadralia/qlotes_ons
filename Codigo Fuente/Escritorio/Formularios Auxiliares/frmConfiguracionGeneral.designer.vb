﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConfiguracionGeneral
    Inherits FormularioHijo

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmConfiguracionGeneral))
        Me.pCuerpo = New ComponentFactory.Krypton.Toolkit.KryptonPanel()
        Me.tabGeneral = New ComponentFactory.Krypton.Navigator.KryptonNavigator()
        Me.tbpAplicacion = New ComponentFactory.Krypton.Navigator.KryptonPage()
        Me.tblOrganizador = New System.Windows.Forms.TableLayoutPanel()
        Me.lblBuscadoresDesplegados = New System.Windows.Forms.Label()
        Me.lblFiltrosDesplegados = New System.Windows.Forms.Label()
        Me.lblNavegabilidad = New System.Windows.Forms.Label()
        Me.chkBuscadoresDesplegados = New ComponentFactory.Krypton.Toolkit.KryptonCheckBox()
        Me.chkFiltrosDesplegados = New ComponentFactory.Krypton.Toolkit.KryptonCheckBox()
        Me.chkNavegabilidad = New ComponentFactory.Krypton.Toolkit.KryptonCheckBox()
        Me.chkMostrarVentanasMaximizadas = New ComponentFactory.Krypton.Toolkit.KryptonCheckBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblServidorErrores = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtServidorErrores = New Escritorio.Quadralia.Controles.aTextBox()
        Me.tabImpresionEtiquetas = New ComponentFactory.Krypton.Navigator.KryptonPage()
        Me.tblOrganizadorEtiquetas = New System.Windows.Forms.TableLayoutPanel()
        Me.cboOrientacion = New Escritorio.Quadralia.Controles.aComboBox()
        Me.cboFormato = New Escritorio.Quadralia.Controles.aComboBox()
        Me.lblImpresora = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.cboImpresora = New Escritorio.Quadralia.Controles.aComboBox()
        Me.lblFormato = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblOrientacion = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.tbpBascula = New ComponentFactory.Krypton.Navigator.KryptonPage()
        Me.tblOrganizadorBascula = New System.Windows.Forms.TableLayoutPanel()
        Me.lblDireccionIP = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtDireccionIP = New Escritorio.Quadralia.Controles.aTextBox()
        Me.lblIntervaloMuestreo = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblPuerto = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblExplicacion1 = New ComponentFactory.Krypton.Toolkit.KryptonWrapLabel()
        Me.lblExplicacion2 = New ComponentFactory.Krypton.Toolkit.KryptonWrapLabel()
        Me.lblVariacionMinima = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.nudPuerto = New ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown()
        Me.nudVariacionMinima = New ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown()
        Me.nudIntervaloMuestreo = New ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown()
        Me.lblPesoMinimo = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.nudPesoMinimo = New ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown()
        Me.lblIntervaloEstable = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.nudIntervaloEstable = New ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown()
        Me.tbpTrazamare = New ComponentFactory.Krypton.Navigator.KryptonPage()
        Me.tblOrganizadorSincronizacion = New System.Windows.Forms.TableLayoutPanel()
        Me.lblBBDDServidor = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblBBDDBaseDatos = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblBBDDClave = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblBBDDUsuario = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblBBDDTiemout = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblBBDDPuerto = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblFTPServidor = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblFTPUsuario = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblFTPClave = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.hdrFTP = New ComponentFactory.Krypton.Toolkit.KryptonHeader()
        Me.hdrBBDD = New ComponentFactory.Krypton.Toolkit.KryptonHeader()
        Me.txtBBDDServidor = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtBBDDBaseDatos = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtBBDDUsuario = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtBBDDClave = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtFTPServidor = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtFTPUsuario = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtFTPClave = New Escritorio.Quadralia.Controles.aTextBox()
        Me.nudBBDDTiemout = New ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown()
        Me.nudBBDDPuerto = New ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown()
        Me.nudFTPPuerto = New ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown()
        Me.chkSincronizacionAutomatica = New ComponentFactory.Krypton.Toolkit.KryptonCheckBox()
        Me.nudSincronizacionAutomatica = New ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown()
        Me.lblFTPPuerto = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblFTPCarpeta = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtFTPCarpeta = New Escritorio.Quadralia.Controles.aTextBox()
        Me.grpCabecera = New ComponentFactory.Krypton.Toolkit.KryptonGroup()
        Me.lblTitulo = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.epErrores = New Escritorio.Quadralia.Controles.Validacion.GestorErrores()
        CType(Me.pCuerpo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pCuerpo.SuspendLayout()
        CType(Me.tabGeneral, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabGeneral.SuspendLayout()
        CType(Me.tbpAplicacion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpAplicacion.SuspendLayout()
        Me.tblOrganizador.SuspendLayout()
        CType(Me.tabImpresionEtiquetas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabImpresionEtiquetas.SuspendLayout()
        Me.tblOrganizadorEtiquetas.SuspendLayout()
        CType(Me.cboOrientacion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboFormato, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboImpresora, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbpBascula, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpBascula.SuspendLayout()
        Me.tblOrganizadorBascula.SuspendLayout()
        CType(Me.tbpTrazamare, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpTrazamare.SuspendLayout()
        Me.tblOrganizadorSincronizacion.SuspendLayout()
        CType(Me.grpCabecera, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpCabecera.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpCabecera.Panel.SuspendLayout()
        Me.grpCabecera.SuspendLayout()
        CType(Me.epErrores, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pCuerpo
        '
        Me.pCuerpo.Controls.Add(Me.tabGeneral)
        Me.pCuerpo.Controls.Add(Me.grpCabecera)
        resources.ApplyResources(Me.pCuerpo, "pCuerpo")
        Me.pCuerpo.Name = "pCuerpo"
        '
        'tabGeneral
        '
        Me.tabGeneral.Bar.BarOrientation = ComponentFactory.Krypton.Toolkit.VisualOrientation.Left
        Me.tabGeneral.Bar.ItemOrientation = ComponentFactory.Krypton.Toolkit.ButtonOrientation.FixedTop
        Me.tabGeneral.Bar.TabBorderStyle = ComponentFactory.Krypton.Toolkit.TabBorderStyle.RoundedEqualSmall
        Me.tabGeneral.Bar.TabStyle = ComponentFactory.Krypton.Toolkit.TabStyle.LowProfile
        Me.tabGeneral.Button.ButtonDisplayLogic = ComponentFactory.Krypton.Navigator.ButtonDisplayLogic.None
        Me.tabGeneral.Button.CloseButtonAction = ComponentFactory.Krypton.Navigator.CloseButtonAction.None
        Me.tabGeneral.Button.CloseButtonDisplay = ComponentFactory.Krypton.Navigator.ButtonDisplay.Hide
        resources.ApplyResources(Me.tabGeneral, "tabGeneral")
        Me.tabGeneral.Name = "tabGeneral"
        Me.tabGeneral.Pages.AddRange(New ComponentFactory.Krypton.Navigator.KryptonPage() {Me.tbpAplicacion, Me.tabImpresionEtiquetas, Me.tbpBascula, Me.tbpTrazamare})
        Me.tabGeneral.SelectedIndex = 0
        '
        'tbpAplicacion
        '
        Me.tbpAplicacion.AutoHiddenSlideSize = New System.Drawing.Size(200, 200)
        Me.tbpAplicacion.Controls.Add(Me.tblOrganizador)
        Me.tbpAplicacion.Flags = 65534
        Me.tbpAplicacion.LastVisibleSet = True
        resources.ApplyResources(Me.tbpAplicacion, "tbpAplicacion")
        Me.tbpAplicacion.Name = "tbpAplicacion"
        Me.tbpAplicacion.UniqueName = "F957580818644F3C0F9437DA0BD45BE9"
        '
        'tblOrganizador
        '
        Me.tblOrganizador.BackColor = System.Drawing.Color.Transparent
        resources.ApplyResources(Me.tblOrganizador, "tblOrganizador")
        Me.tblOrganizador.Controls.Add(Me.lblBuscadoresDesplegados, 1, 3)
        Me.tblOrganizador.Controls.Add(Me.lblFiltrosDesplegados, 1, 5)
        Me.tblOrganizador.Controls.Add(Me.lblNavegabilidad, 1, 7)
        Me.tblOrganizador.Controls.Add(Me.chkBuscadoresDesplegados, 0, 2)
        Me.tblOrganizador.Controls.Add(Me.chkFiltrosDesplegados, 0, 4)
        Me.tblOrganizador.Controls.Add(Me.chkNavegabilidad, 0, 6)
        Me.tblOrganizador.Controls.Add(Me.chkMostrarVentanasMaximizadas, 0, 0)
        Me.tblOrganizador.Controls.Add(Me.Label1, 1, 1)
        Me.tblOrganizador.Controls.Add(Me.lblServidorErrores, 0, 8)
        Me.tblOrganizador.Controls.Add(Me.txtServidorErrores, 0, 9)
        Me.tblOrganizador.Name = "tblOrganizador"
        '
        'lblBuscadoresDesplegados
        '
        resources.ApplyResources(Me.lblBuscadoresDesplegados, "lblBuscadoresDesplegados")
        Me.lblBuscadoresDesplegados.Name = "lblBuscadoresDesplegados"
        '
        'lblFiltrosDesplegados
        '
        resources.ApplyResources(Me.lblFiltrosDesplegados, "lblFiltrosDesplegados")
        Me.lblFiltrosDesplegados.Name = "lblFiltrosDesplegados"
        '
        'lblNavegabilidad
        '
        resources.ApplyResources(Me.lblNavegabilidad, "lblNavegabilidad")
        Me.lblNavegabilidad.Name = "lblNavegabilidad"
        '
        'chkBuscadoresDesplegados
        '
        Me.tblOrganizador.SetColumnSpan(Me.chkBuscadoresDesplegados, 2)
        resources.ApplyResources(Me.chkBuscadoresDesplegados, "chkBuscadoresDesplegados")
        Me.chkBuscadoresDesplegados.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl
        Me.chkBuscadoresDesplegados.Name = "chkBuscadoresDesplegados"
        Me.chkBuscadoresDesplegados.StateCommon.ShortText.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkBuscadoresDesplegados.Values.Text = resources.GetString("chkBuscadoresDesplegados.Values.Text")
        '
        'chkFiltrosDesplegados
        '
        Me.tblOrganizador.SetColumnSpan(Me.chkFiltrosDesplegados, 2)
        resources.ApplyResources(Me.chkFiltrosDesplegados, "chkFiltrosDesplegados")
        Me.chkFiltrosDesplegados.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl
        Me.chkFiltrosDesplegados.Name = "chkFiltrosDesplegados"
        Me.chkFiltrosDesplegados.StateCommon.ShortText.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkFiltrosDesplegados.Values.Text = resources.GetString("chkFiltrosDesplegados.Values.Text")
        '
        'chkNavegabilidad
        '
        Me.tblOrganizador.SetColumnSpan(Me.chkNavegabilidad, 2)
        resources.ApplyResources(Me.chkNavegabilidad, "chkNavegabilidad")
        Me.chkNavegabilidad.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl
        Me.chkNavegabilidad.Name = "chkNavegabilidad"
        Me.chkNavegabilidad.StateCommon.ShortText.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkNavegabilidad.Values.Text = resources.GetString("chkNavegabilidad.Values.Text")
        '
        'chkMostrarVentanasMaximizadas
        '
        Me.tblOrganizador.SetColumnSpan(Me.chkMostrarVentanasMaximizadas, 2)
        Me.chkMostrarVentanasMaximizadas.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl
        resources.ApplyResources(Me.chkMostrarVentanasMaximizadas, "chkMostrarVentanasMaximizadas")
        Me.chkMostrarVentanasMaximizadas.Name = "chkMostrarVentanasMaximizadas"
        Me.chkMostrarVentanasMaximizadas.StateCommon.ShortText.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkMostrarVentanasMaximizadas.Values.Text = resources.GetString("chkMostrarVentanasMaximizadas.Values.Text")
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'lblServidorErrores
        '
        Me.tblOrganizador.SetColumnSpan(Me.lblServidorErrores, 2)
        resources.ApplyResources(Me.lblServidorErrores, "lblServidorErrores")
        Me.lblServidorErrores.Name = "lblServidorErrores"
        Me.lblServidorErrores.StateCommon.ShortText.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblServidorErrores.Values.Text = resources.GetString("lblServidorErrores.Values.Text")
        '
        'txtServidorErrores
        '
        Me.tblOrganizador.SetColumnSpan(Me.txtServidorErrores, 2)
        Me.txtServidorErrores.controlarBotonBorrar = True
        resources.ApplyResources(Me.txtServidorErrores, "txtServidorErrores")
        Me.txtServidorErrores.Formato = ""
        Me.txtServidorErrores.mostrarSiempreBotonBorrar = False
        Me.txtServidorErrores.Name = "txtServidorErrores"
        Me.txtServidorErrores.seleccionarTodo = True
        '
        'tabImpresionEtiquetas
        '
        Me.tabImpresionEtiquetas.AutoHiddenSlideSize = New System.Drawing.Size(200, 200)
        Me.tabImpresionEtiquetas.Controls.Add(Me.tblOrganizadorEtiquetas)
        Me.tabImpresionEtiquetas.Flags = 65534
        Me.tabImpresionEtiquetas.LastVisibleSet = True
        resources.ApplyResources(Me.tabImpresionEtiquetas, "tabImpresionEtiquetas")
        Me.tabImpresionEtiquetas.Name = "tabImpresionEtiquetas"
        Me.tabImpresionEtiquetas.UniqueName = "E4811CEC56014ACF1EA72C326CE22112"
        '
        'tblOrganizadorEtiquetas
        '
        Me.tblOrganizadorEtiquetas.BackColor = System.Drawing.Color.Transparent
        resources.ApplyResources(Me.tblOrganizadorEtiquetas, "tblOrganizadorEtiquetas")
        Me.tblOrganizadorEtiquetas.Controls.Add(Me.cboOrientacion, 2, 4)
        Me.tblOrganizadorEtiquetas.Controls.Add(Me.cboFormato, 0, 3)
        Me.tblOrganizadorEtiquetas.Controls.Add(Me.lblImpresora, 0, 0)
        Me.tblOrganizadorEtiquetas.Controls.Add(Me.cboImpresora, 0, 1)
        Me.tblOrganizadorEtiquetas.Controls.Add(Me.lblFormato, 0, 2)
        Me.tblOrganizadorEtiquetas.Controls.Add(Me.lblOrientacion, 1, 4)
        Me.tblOrganizadorEtiquetas.Name = "tblOrganizadorEtiquetas"
        '
        'cboOrientacion
        '
        Me.cboOrientacion.controlarBotonBorrar = True
        resources.ApplyResources(Me.cboOrientacion, "cboOrientacion")
        Me.cboOrientacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOrientacion.DropDownWidth = 494
        Me.cboOrientacion.mostrarSiempreBotonBorrar = False
        Me.cboOrientacion.Name = "cboOrientacion"
        '
        'cboFormato
        '
        Me.tblOrganizadorEtiquetas.SetColumnSpan(Me.cboFormato, 3)
        Me.cboFormato.controlarBotonBorrar = True
        resources.ApplyResources(Me.cboFormato, "cboFormato")
        Me.cboFormato.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFormato.DropDownWidth = 494
        Me.cboFormato.mostrarSiempreBotonBorrar = False
        Me.cboFormato.Name = "cboFormato"
        '
        'lblImpresora
        '
        Me.tblOrganizadorEtiquetas.SetColumnSpan(Me.lblImpresora, 3)
        resources.ApplyResources(Me.lblImpresora, "lblImpresora")
        Me.lblImpresora.Name = "lblImpresora"
        Me.lblImpresora.StateCommon.ShortText.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblImpresora.Values.Text = resources.GetString("lblImpresora.Values.Text")
        '
        'cboImpresora
        '
        Me.tblOrganizadorEtiquetas.SetColumnSpan(Me.cboImpresora, 3)
        Me.cboImpresora.controlarBotonBorrar = True
        resources.ApplyResources(Me.cboImpresora, "cboImpresora")
        Me.cboImpresora.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboImpresora.DropDownWidth = 494
        Me.cboImpresora.mostrarSiempreBotonBorrar = False
        Me.cboImpresora.Name = "cboImpresora"
        '
        'lblFormato
        '
        Me.tblOrganizadorEtiquetas.SetColumnSpan(Me.lblFormato, 3)
        resources.ApplyResources(Me.lblFormato, "lblFormato")
        Me.lblFormato.Name = "lblFormato"
        Me.lblFormato.StateCommon.ShortText.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFormato.Values.Text = resources.GetString("lblFormato.Values.Text")
        '
        'lblOrientacion
        '
        resources.ApplyResources(Me.lblOrientacion, "lblOrientacion")
        Me.lblOrientacion.Name = "lblOrientacion"
        Me.lblOrientacion.Values.Text = resources.GetString("lblOrientacion.Values.Text")
        '
        'tbpBascula
        '
        Me.tbpBascula.AutoHiddenSlideSize = New System.Drawing.Size(200, 200)
        Me.tbpBascula.Controls.Add(Me.tblOrganizadorBascula)
        Me.tbpBascula.Flags = 65534
        Me.tbpBascula.LastVisibleSet = True
        resources.ApplyResources(Me.tbpBascula, "tbpBascula")
        Me.tbpBascula.Name = "tbpBascula"
        Me.tbpBascula.UniqueName = "E27611E2D894439D158008F5996A4290"
        '
        'tblOrganizadorBascula
        '
        Me.tblOrganizadorBascula.BackColor = System.Drawing.Color.Transparent
        resources.ApplyResources(Me.tblOrganizadorBascula, "tblOrganizadorBascula")
        Me.tblOrganizadorBascula.Controls.Add(Me.lblDireccionIP, 0, 1)
        Me.tblOrganizadorBascula.Controls.Add(Me.txtDireccionIP, 2, 1)
        Me.tblOrganizadorBascula.Controls.Add(Me.lblIntervaloMuestreo, 0, 4)
        Me.tblOrganizadorBascula.Controls.Add(Me.lblPuerto, 3, 1)
        Me.tblOrganizadorBascula.Controls.Add(Me.lblExplicacion1, 0, 0)
        Me.tblOrganizadorBascula.Controls.Add(Me.lblExplicacion2, 0, 3)
        Me.tblOrganizadorBascula.Controls.Add(Me.lblVariacionMinima, 3, 5)
        Me.tblOrganizadorBascula.Controls.Add(Me.nudPuerto, 5, 1)
        Me.tblOrganizadorBascula.Controls.Add(Me.nudVariacionMinima, 5, 5)
        Me.tblOrganizadorBascula.Controls.Add(Me.nudIntervaloMuestreo, 2, 4)
        Me.tblOrganizadorBascula.Controls.Add(Me.lblPesoMinimo, 3, 4)
        Me.tblOrganizadorBascula.Controls.Add(Me.nudPesoMinimo, 5, 4)
        Me.tblOrganizadorBascula.Controls.Add(Me.lblIntervaloEstable, 0, 5)
        Me.tblOrganizadorBascula.Controls.Add(Me.nudIntervaloEstable, 2, 5)
        Me.tblOrganizadorBascula.Name = "tblOrganizadorBascula"
        '
        'lblDireccionIP
        '
        resources.ApplyResources(Me.lblDireccionIP, "lblDireccionIP")
        Me.lblDireccionIP.Name = "lblDireccionIP"
        Me.lblDireccionIP.StateCommon.ShortText.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDireccionIP.Values.Text = resources.GetString("lblDireccionIP.Values.Text")
        '
        'txtDireccionIP
        '
        Me.txtDireccionIP.controlarBotonBorrar = True
        Me.txtDireccionIP.Formato = ""
        resources.ApplyResources(Me.txtDireccionIP, "txtDireccionIP")
        Me.txtDireccionIP.mostrarSiempreBotonBorrar = False
        Me.txtDireccionIP.Name = "txtDireccionIP"
        Me.txtDireccionIP.seleccionarTodo = True
        '
        'lblIntervaloMuestreo
        '
        resources.ApplyResources(Me.lblIntervaloMuestreo, "lblIntervaloMuestreo")
        Me.lblIntervaloMuestreo.Name = "lblIntervaloMuestreo"
        Me.lblIntervaloMuestreo.StateCommon.ShortText.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIntervaloMuestreo.Values.Text = resources.GetString("lblIntervaloMuestreo.Values.Text")
        '
        'lblPuerto
        '
        resources.ApplyResources(Me.lblPuerto, "lblPuerto")
        Me.lblPuerto.Name = "lblPuerto"
        Me.lblPuerto.StateCommon.ShortText.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPuerto.Values.Text = resources.GetString("lblPuerto.Values.Text")
        '
        'lblExplicacion1
        '
        Me.tblOrganizadorBascula.SetColumnSpan(Me.lblExplicacion1, 7)
        resources.ApplyResources(Me.lblExplicacion1, "lblExplicacion1")
        Me.lblExplicacion1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(91, Byte), Integer))
        Me.lblExplicacion1.Name = "lblExplicacion1"
        '
        'lblExplicacion2
        '
        Me.tblOrganizadorBascula.SetColumnSpan(Me.lblExplicacion2, 7)
        resources.ApplyResources(Me.lblExplicacion2, "lblExplicacion2")
        Me.lblExplicacion2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(91, Byte), Integer))
        Me.lblExplicacion2.Name = "lblExplicacion2"
        '
        'lblVariacionMinima
        '
        resources.ApplyResources(Me.lblVariacionMinima, "lblVariacionMinima")
        Me.lblVariacionMinima.Name = "lblVariacionMinima"
        Me.lblVariacionMinima.StateCommon.ShortText.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVariacionMinima.Values.Text = resources.GetString("lblVariacionMinima.Values.Text")
        '
        'nudPuerto
        '
        resources.ApplyResources(Me.nudPuerto, "nudPuerto")
        Me.nudPuerto.Maximum = New Decimal(New Integer() {65535, 0, 0, 0})
        Me.nudPuerto.Name = "nudPuerto"
        '
        'nudVariacionMinima
        '
        resources.ApplyResources(Me.nudVariacionMinima, "nudVariacionMinima")
        Me.nudVariacionMinima.Maximum = New Decimal(New Integer() {99999999, 0, 0, 0})
        Me.nudVariacionMinima.Name = "nudVariacionMinima"
        '
        'nudIntervaloMuestreo
        '
        resources.ApplyResources(Me.nudIntervaloMuestreo, "nudIntervaloMuestreo")
        Me.nudIntervaloMuestreo.Maximum = New Decimal(New Integer() {9999999, 0, 0, 0})
        Me.nudIntervaloMuestreo.Name = "nudIntervaloMuestreo"
        '
        'lblPesoMinimo
        '
        resources.ApplyResources(Me.lblPesoMinimo, "lblPesoMinimo")
        Me.lblPesoMinimo.Name = "lblPesoMinimo"
        Me.lblPesoMinimo.StateCommon.ShortText.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPesoMinimo.Values.Text = resources.GetString("lblPesoMinimo.Values.Text")
        '
        'nudPesoMinimo
        '
        resources.ApplyResources(Me.nudPesoMinimo, "nudPesoMinimo")
        Me.nudPesoMinimo.Maximum = New Decimal(New Integer() {99999999, 0, 0, 0})
        Me.nudPesoMinimo.Name = "nudPesoMinimo"
        '
        'lblIntervaloEstable
        '
        resources.ApplyResources(Me.lblIntervaloEstable, "lblIntervaloEstable")
        Me.lblIntervaloEstable.Name = "lblIntervaloEstable"
        Me.lblIntervaloEstable.StateCommon.ShortText.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIntervaloEstable.Values.Text = resources.GetString("lblIntervaloEstable.Values.Text")
        '
        'nudIntervaloEstable
        '
        resources.ApplyResources(Me.nudIntervaloEstable, "nudIntervaloEstable")
        Me.nudIntervaloEstable.Maximum = New Decimal(New Integer() {9999999, 0, 0, 0})
        Me.nudIntervaloEstable.Name = "nudIntervaloEstable"
        '
        'tbpTrazamare
        '
        Me.tbpTrazamare.AutoHiddenSlideSize = New System.Drawing.Size(200, 200)
        Me.tbpTrazamare.Controls.Add(Me.tblOrganizadorSincronizacion)
        Me.tbpTrazamare.Flags = 65534
        Me.tbpTrazamare.LastVisibleSet = True
        resources.ApplyResources(Me.tbpTrazamare, "tbpTrazamare")
        Me.tbpTrazamare.Name = "tbpTrazamare"
        Me.tbpTrazamare.UniqueName = "F61B182859264DBD1281C597FEF16AC3"
        '
        'tblOrganizadorSincronizacion
        '
        Me.tblOrganizadorSincronizacion.BackColor = System.Drawing.Color.Transparent
        resources.ApplyResources(Me.tblOrganizadorSincronizacion, "tblOrganizadorSincronizacion")
        Me.tblOrganizadorSincronizacion.Controls.Add(Me.lblBBDDServidor, 0, 3)
        Me.tblOrganizadorSincronizacion.Controls.Add(Me.lblBBDDBaseDatos, 0, 4)
        Me.tblOrganizadorSincronizacion.Controls.Add(Me.lblBBDDClave, 3, 5)
        Me.tblOrganizadorSincronizacion.Controls.Add(Me.lblBBDDUsuario, 0, 5)
        Me.tblOrganizadorSincronizacion.Controls.Add(Me.lblBBDDTiemout, 0, 6)
        Me.tblOrganizadorSincronizacion.Controls.Add(Me.lblBBDDPuerto, 3, 6)
        Me.tblOrganizadorSincronizacion.Controls.Add(Me.lblFTPServidor, 0, 9)
        Me.tblOrganizadorSincronizacion.Controls.Add(Me.lblFTPUsuario, 0, 11)
        Me.tblOrganizadorSincronizacion.Controls.Add(Me.lblFTPClave, 3, 11)
        Me.tblOrganizadorSincronizacion.Controls.Add(Me.hdrFTP, 0, 8)
        Me.tblOrganizadorSincronizacion.Controls.Add(Me.hdrBBDD, 0, 2)
        Me.tblOrganizadorSincronizacion.Controls.Add(Me.txtBBDDServidor, 2, 3)
        Me.tblOrganizadorSincronizacion.Controls.Add(Me.txtBBDDBaseDatos, 2, 4)
        Me.tblOrganizadorSincronizacion.Controls.Add(Me.txtBBDDUsuario, 2, 5)
        Me.tblOrganizadorSincronizacion.Controls.Add(Me.txtBBDDClave, 5, 5)
        Me.tblOrganizadorSincronizacion.Controls.Add(Me.txtFTPServidor, 2, 9)
        Me.tblOrganizadorSincronizacion.Controls.Add(Me.txtFTPUsuario, 2, 11)
        Me.tblOrganizadorSincronizacion.Controls.Add(Me.txtFTPClave, 5, 11)
        Me.tblOrganizadorSincronizacion.Controls.Add(Me.nudBBDDTiemout, 2, 6)
        Me.tblOrganizadorSincronizacion.Controls.Add(Me.nudBBDDPuerto, 5, 6)
        Me.tblOrganizadorSincronizacion.Controls.Add(Me.nudFTPPuerto, 5, 10)
        Me.tblOrganizadorSincronizacion.Controls.Add(Me.chkSincronizacionAutomatica, 0, 0)
        Me.tblOrganizadorSincronizacion.Controls.Add(Me.nudSincronizacionAutomatica, 5, 0)
        Me.tblOrganizadorSincronizacion.Controls.Add(Me.lblFTPPuerto, 3, 10)
        Me.tblOrganizadorSincronizacion.Controls.Add(Me.lblFTPCarpeta, 0, 10)
        Me.tblOrganizadorSincronizacion.Controls.Add(Me.txtFTPCarpeta, 2, 10)
        Me.tblOrganizadorSincronizacion.Name = "tblOrganizadorSincronizacion"
        '
        'lblBBDDServidor
        '
        resources.ApplyResources(Me.lblBBDDServidor, "lblBBDDServidor")
        Me.lblBBDDServidor.Name = "lblBBDDServidor"
        Me.lblBBDDServidor.Values.Text = resources.GetString("lblBBDDServidor.Values.Text")
        '
        'lblBBDDBaseDatos
        '
        resources.ApplyResources(Me.lblBBDDBaseDatos, "lblBBDDBaseDatos")
        Me.lblBBDDBaseDatos.Name = "lblBBDDBaseDatos"
        Me.lblBBDDBaseDatos.Values.Text = resources.GetString("lblBBDDBaseDatos.Values.Text")
        '
        'lblBBDDClave
        '
        resources.ApplyResources(Me.lblBBDDClave, "lblBBDDClave")
        Me.lblBBDDClave.Name = "lblBBDDClave"
        Me.lblBBDDClave.Values.Text = resources.GetString("lblBBDDClave.Values.Text")
        '
        'lblBBDDUsuario
        '
        resources.ApplyResources(Me.lblBBDDUsuario, "lblBBDDUsuario")
        Me.lblBBDDUsuario.Name = "lblBBDDUsuario"
        Me.lblBBDDUsuario.Values.Text = resources.GetString("lblBBDDUsuario.Values.Text")
        '
        'lblBBDDTiemout
        '
        resources.ApplyResources(Me.lblBBDDTiemout, "lblBBDDTiemout")
        Me.lblBBDDTiemout.Name = "lblBBDDTiemout"
        Me.lblBBDDTiemout.Values.Text = resources.GetString("lblBBDDTiemout.Values.Text")
        '
        'lblBBDDPuerto
        '
        resources.ApplyResources(Me.lblBBDDPuerto, "lblBBDDPuerto")
        Me.lblBBDDPuerto.Name = "lblBBDDPuerto"
        Me.lblBBDDPuerto.Values.Text = resources.GetString("lblBBDDPuerto.Values.Text")
        '
        'lblFTPServidor
        '
        resources.ApplyResources(Me.lblFTPServidor, "lblFTPServidor")
        Me.lblFTPServidor.Name = "lblFTPServidor"
        Me.lblFTPServidor.Values.Text = resources.GetString("lblFTPServidor.Values.Text")
        '
        'lblFTPUsuario
        '
        resources.ApplyResources(Me.lblFTPUsuario, "lblFTPUsuario")
        Me.lblFTPUsuario.Name = "lblFTPUsuario"
        Me.lblFTPUsuario.Values.Text = resources.GetString("lblFTPUsuario.Values.Text")
        '
        'lblFTPClave
        '
        resources.ApplyResources(Me.lblFTPClave, "lblFTPClave")
        Me.lblFTPClave.Name = "lblFTPClave"
        Me.lblFTPClave.Values.Text = resources.GetString("lblFTPClave.Values.Text")
        '
        'hdrFTP
        '
        Me.tblOrganizadorSincronizacion.SetColumnSpan(Me.hdrFTP, 6)
        resources.ApplyResources(Me.hdrFTP, "hdrFTP")
        Me.hdrFTP.HeaderStyle = ComponentFactory.Krypton.Toolkit.HeaderStyle.Secondary
        Me.hdrFTP.Name = "hdrFTP"
        Me.hdrFTP.Values.Description = resources.GetString("hdrFTP.Values.Description")
        Me.hdrFTP.Values.Heading = resources.GetString("hdrFTP.Values.Heading")
        Me.hdrFTP.Values.Image = CType(resources.GetObject("hdrFTP.Values.Image"), System.Drawing.Image)
        '
        'hdrBBDD
        '
        Me.tblOrganizadorSincronizacion.SetColumnSpan(Me.hdrBBDD, 6)
        resources.ApplyResources(Me.hdrBBDD, "hdrBBDD")
        Me.hdrBBDD.HeaderStyle = ComponentFactory.Krypton.Toolkit.HeaderStyle.Secondary
        Me.hdrBBDD.Name = "hdrBBDD"
        Me.hdrBBDD.Values.Description = resources.GetString("hdrBBDD.Values.Description")
        Me.hdrBBDD.Values.Heading = resources.GetString("hdrBBDD.Values.Heading")
        Me.hdrBBDD.Values.Image = CType(resources.GetObject("hdrBBDD.Values.Image"), System.Drawing.Image)
        '
        'txtBBDDServidor
        '
        Me.tblOrganizadorSincronizacion.SetColumnSpan(Me.txtBBDDServidor, 4)
        Me.txtBBDDServidor.controlarBotonBorrar = True
        resources.ApplyResources(Me.txtBBDDServidor, "txtBBDDServidor")
        Me.txtBBDDServidor.Formato = ""
        Me.txtBBDDServidor.mostrarSiempreBotonBorrar = False
        Me.txtBBDDServidor.Name = "txtBBDDServidor"
        Me.txtBBDDServidor.seleccionarTodo = True
        '
        'txtBBDDBaseDatos
        '
        Me.tblOrganizadorSincronizacion.SetColumnSpan(Me.txtBBDDBaseDatos, 4)
        Me.txtBBDDBaseDatos.controlarBotonBorrar = True
        resources.ApplyResources(Me.txtBBDDBaseDatos, "txtBBDDBaseDatos")
        Me.txtBBDDBaseDatos.Formato = ""
        Me.txtBBDDBaseDatos.mostrarSiempreBotonBorrar = False
        Me.txtBBDDBaseDatos.Name = "txtBBDDBaseDatos"
        Me.txtBBDDBaseDatos.seleccionarTodo = True
        '
        'txtBBDDUsuario
        '
        Me.txtBBDDUsuario.controlarBotonBorrar = True
        resources.ApplyResources(Me.txtBBDDUsuario, "txtBBDDUsuario")
        Me.txtBBDDUsuario.Formato = ""
        Me.txtBBDDUsuario.mostrarSiempreBotonBorrar = False
        Me.txtBBDDUsuario.Name = "txtBBDDUsuario"
        Me.txtBBDDUsuario.seleccionarTodo = True
        '
        'txtBBDDClave
        '
        Me.txtBBDDClave.controlarBotonBorrar = True
        resources.ApplyResources(Me.txtBBDDClave, "txtBBDDClave")
        Me.txtBBDDClave.Formato = ""
        Me.txtBBDDClave.mostrarSiempreBotonBorrar = False
        Me.txtBBDDClave.Name = "txtBBDDClave"
        Me.txtBBDDClave.seleccionarTodo = True
        Me.txtBBDDClave.UseSystemPasswordChar = True
        '
        'txtFTPServidor
        '
        Me.tblOrganizadorSincronizacion.SetColumnSpan(Me.txtFTPServidor, 4)
        Me.txtFTPServidor.controlarBotonBorrar = True
        resources.ApplyResources(Me.txtFTPServidor, "txtFTPServidor")
        Me.txtFTPServidor.Formato = ""
        Me.txtFTPServidor.mostrarSiempreBotonBorrar = False
        Me.txtFTPServidor.Name = "txtFTPServidor"
        Me.txtFTPServidor.seleccionarTodo = True
        '
        'txtFTPUsuario
        '
        Me.txtFTPUsuario.controlarBotonBorrar = True
        resources.ApplyResources(Me.txtFTPUsuario, "txtFTPUsuario")
        Me.txtFTPUsuario.Formato = ""
        Me.txtFTPUsuario.mostrarSiempreBotonBorrar = False
        Me.txtFTPUsuario.Name = "txtFTPUsuario"
        Me.txtFTPUsuario.seleccionarTodo = True
        '
        'txtFTPClave
        '
        Me.txtFTPClave.controlarBotonBorrar = True
        resources.ApplyResources(Me.txtFTPClave, "txtFTPClave")
        Me.txtFTPClave.Formato = ""
        Me.txtFTPClave.mostrarSiempreBotonBorrar = False
        Me.txtFTPClave.Name = "txtFTPClave"
        Me.txtFTPClave.seleccionarTodo = True
        Me.txtFTPClave.UseSystemPasswordChar = True
        '
        'nudBBDDTiemout
        '
        resources.ApplyResources(Me.nudBBDDTiemout, "nudBBDDTiemout")
        Me.nudBBDDTiemout.Maximum = New Decimal(New Integer() {500, 0, 0, 0})
        Me.nudBBDDTiemout.Name = "nudBBDDTiemout"
        '
        'nudBBDDPuerto
        '
        resources.ApplyResources(Me.nudBBDDPuerto, "nudBBDDPuerto")
        Me.nudBBDDPuerto.Maximum = New Decimal(New Integer() {65535, 0, 0, 0})
        Me.nudBBDDPuerto.Name = "nudBBDDPuerto"
        '
        'nudFTPPuerto
        '
        resources.ApplyResources(Me.nudFTPPuerto, "nudFTPPuerto")
        Me.nudFTPPuerto.Maximum = New Decimal(New Integer() {65535, 0, 0, 0})
        Me.nudFTPPuerto.Name = "nudFTPPuerto"
        '
        'chkSincronizacionAutomatica
        '
        Me.tblOrganizadorSincronizacion.SetColumnSpan(Me.chkSincronizacionAutomatica, 5)
        resources.ApplyResources(Me.chkSincronizacionAutomatica, "chkSincronizacionAutomatica")
        Me.chkSincronizacionAutomatica.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.NormalControl
        Me.chkSincronizacionAutomatica.Name = "chkSincronizacionAutomatica"
        Me.chkSincronizacionAutomatica.UseWaitCursor = True
        Me.chkSincronizacionAutomatica.Values.Text = resources.GetString("chkSincronizacionAutomatica.Values.Text")
        '
        'nudSincronizacionAutomatica
        '
        resources.ApplyResources(Me.nudSincronizacionAutomatica, "nudSincronizacionAutomatica")
        Me.nudSincronizacionAutomatica.Maximum = New Decimal(New Integer() {500, 0, 0, 0})
        Me.nudSincronizacionAutomatica.Name = "nudSincronizacionAutomatica"
        '
        'lblFTPPuerto
        '
        resources.ApplyResources(Me.lblFTPPuerto, "lblFTPPuerto")
        Me.lblFTPPuerto.Name = "lblFTPPuerto"
        Me.lblFTPPuerto.Values.Text = resources.GetString("lblFTPPuerto.Values.Text")
        '
        'lblFTPCarpeta
        '
        resources.ApplyResources(Me.lblFTPCarpeta, "lblFTPCarpeta")
        Me.lblFTPCarpeta.Name = "lblFTPCarpeta"
        Me.lblFTPCarpeta.Values.Text = resources.GetString("lblFTPCarpeta.Values.Text")
        '
        'txtFTPCarpeta
        '
        Me.txtFTPCarpeta.controlarBotonBorrar = True
        resources.ApplyResources(Me.txtFTPCarpeta, "txtFTPCarpeta")
        Me.txtFTPCarpeta.Formato = ""
        Me.txtFTPCarpeta.mostrarSiempreBotonBorrar = False
        Me.txtFTPCarpeta.Name = "txtFTPCarpeta"
        Me.txtFTPCarpeta.seleccionarTodo = True
        '
        'grpCabecera
        '
        resources.ApplyResources(Me.grpCabecera, "grpCabecera")
        Me.grpCabecera.GroupBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.PanelCustom1
        Me.grpCabecera.Name = "grpCabecera"
        '
        'grpCabecera.Panel
        '
        Me.grpCabecera.Panel.Controls.Add(Me.lblTitulo)
        Me.grpCabecera.StateCommon.Border.Color1 = System.Drawing.Color.DarkOrange
        Me.grpCabecera.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom
        Me.grpCabecera.StateCommon.Border.Width = 5
        '
        'lblTitulo
        '
        Me.lblTitulo.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.Custom1
        resources.ApplyResources(Me.lblTitulo, "lblTitulo")
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Values.Image = Global.Escritorio.My.Resources.Resources.Configurar_32
        Me.lblTitulo.Values.Text = resources.GetString("lblTitulo.Values.Text")
        '
        'epErrores
        '
        Me.epErrores.Alineacion = System.Windows.Forms.ErrorIconAlignment.TopLeft
        Me.epErrores.ContainerControl = Me
        '
        'frmConfiguracionGeneral
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.pCuerpo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmConfiguracionGeneral"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        CType(Me.pCuerpo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pCuerpo.ResumeLayout(False)
        CType(Me.tabGeneral, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabGeneral.ResumeLayout(False)
        CType(Me.tbpAplicacion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpAplicacion.ResumeLayout(False)
        Me.tblOrganizador.ResumeLayout(False)
        Me.tblOrganizador.PerformLayout()
        CType(Me.tabImpresionEtiquetas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabImpresionEtiquetas.ResumeLayout(False)
        Me.tblOrganizadorEtiquetas.ResumeLayout(False)
        Me.tblOrganizadorEtiquetas.PerformLayout()
        CType(Me.cboOrientacion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboFormato, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboImpresora, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbpBascula, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpBascula.ResumeLayout(False)
        Me.tblOrganizadorBascula.ResumeLayout(False)
        Me.tblOrganizadorBascula.PerformLayout()
        CType(Me.tbpTrazamare, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpTrazamare.ResumeLayout(False)
        Me.tblOrganizadorSincronizacion.ResumeLayout(False)
        Me.tblOrganizadorSincronizacion.PerformLayout()
        CType(Me.grpCabecera.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpCabecera.Panel.ResumeLayout(False)
        Me.grpCabecera.Panel.PerformLayout()
        CType(Me.grpCabecera, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpCabecera.ResumeLayout(False)
        CType(Me.epErrores, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pCuerpo As ComponentFactory.Krypton.Toolkit.KryptonPanel
    Friend WithEvents epErrores As Quadralia.Controles.Validacion.GestorErrores
    Friend WithEvents tabGeneral As ComponentFactory.Krypton.Navigator.KryptonNavigator
    Friend WithEvents tbpAplicacion As ComponentFactory.Krypton.Navigator.KryptonPage
    Friend WithEvents grpCabecera As ComponentFactory.Krypton.Toolkit.KryptonGroup
    Friend WithEvents lblTitulo As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents tblOrganizador As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblBuscadoresDesplegados As System.Windows.Forms.Label
    Friend WithEvents lblFiltrosDesplegados As System.Windows.Forms.Label
    Friend WithEvents lblNavegabilidad As System.Windows.Forms.Label
    Friend WithEvents chkBuscadoresDesplegados As ComponentFactory.Krypton.Toolkit.KryptonCheckBox
    Friend WithEvents chkFiltrosDesplegados As ComponentFactory.Krypton.Toolkit.KryptonCheckBox
    Friend WithEvents chkNavegabilidad As ComponentFactory.Krypton.Toolkit.KryptonCheckBox
    Friend WithEvents chkMostrarVentanasMaximizadas As ComponentFactory.Krypton.Toolkit.KryptonCheckBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblServidorErrores As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtServidorErrores As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents tabImpresionEtiquetas As ComponentFactory.Krypton.Navigator.KryptonPage
    Friend WithEvents tblOrganizadorEtiquetas As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblImpresora As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents cboImpresora As Escritorio.Quadralia.Controles.aComboBox
    Friend WithEvents lblFormato As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents cboFormato As Escritorio.Quadralia.Controles.aComboBox
    Friend WithEvents lblOrientacion As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents cboOrientacion As Escritorio.Quadralia.Controles.aComboBox
    Friend WithEvents tbpBascula As ComponentFactory.Krypton.Navigator.KryptonPage
    Friend WithEvents tblOrganizadorBascula As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblDireccionIP As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtDireccionIP As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents lblIntervaloMuestreo As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblPuerto As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblExplicacion1 As ComponentFactory.Krypton.Toolkit.KryptonWrapLabel
    Friend WithEvents lblExplicacion2 As ComponentFactory.Krypton.Toolkit.KryptonWrapLabel
    Friend WithEvents lblPesoMinimo As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblVariacionMinima As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents nudPuerto As ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown
    Friend WithEvents nudVariacionMinima As ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown
    Friend WithEvents nudPesoMinimo As ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown
    Friend WithEvents nudIntervaloMuestreo As ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown
    Friend WithEvents lblIntervaloEstable As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents nudIntervaloEstable As ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown
    Friend WithEvents tbpTrazamare As ComponentFactory.Krypton.Navigator.KryptonPage
    Friend WithEvents tblOrganizadorSincronizacion As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblBBDDServidor As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblBBDDBaseDatos As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblBBDDClave As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblBBDDUsuario As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblBBDDTiemout As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblBBDDPuerto As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblFTPServidor As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblFTPPuerto As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblFTPUsuario As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblFTPClave As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents hdrFTP As ComponentFactory.Krypton.Toolkit.KryptonHeader
    Friend WithEvents hdrBBDD As ComponentFactory.Krypton.Toolkit.KryptonHeader
    Friend WithEvents txtBBDDServidor As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents txtBBDDBaseDatos As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents txtBBDDUsuario As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents txtBBDDClave As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents txtFTPServidor As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents txtFTPUsuario As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents txtFTPClave As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents nudBBDDTiemout As ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown
    Friend WithEvents nudBBDDPuerto As ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown
    Friend WithEvents nudFTPPuerto As ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown
    Friend WithEvents chkSincronizacionAutomatica As ComponentFactory.Krypton.Toolkit.KryptonCheckBox
    Friend WithEvents nudSincronizacionAutomatica As ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown
    Friend WithEvents lblFTPCarpeta As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtFTPCarpeta As Escritorio.Quadralia.Controles.aTextBox
End Class
