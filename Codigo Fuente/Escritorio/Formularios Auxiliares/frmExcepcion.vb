Imports System.Web
Public Class frmExcepcion
    Private _Info As Quadralia.Errores.InfoError = Nothing
    Private _Error As Exception = Nothing

    Public ReadOnly Property InfoError As Quadralia.Errores.InfoError
        Get
            If _Info Is Nothing Then
                _Info = New Quadralia.Errores.InfoError() With {.CapturarPantalla = True, .ConcedeContacto = True, .Licencia = ""}                
            End If
            Return _Info
        End Get
    End Property

#Region " LIMPIEZA "
    Private Sub LimpiarTodo()
        chkNotificar.Checked = True
        txtMensajeError.Clear()
        txtNombreAplicacion.Clear()
        txtVersion.Clear()
        txtFecha.Clear()
        txtEmail.Clear()
        txtComentarios.Clear()
        txtTipoExcepcion.Clear()
        txtMensajeError.Clear()
        txtOrigenExcepcion.Clear()
        txtPilaLllamadas.Clear()
        chkAdjuntarCaptura.Checked = True
        picCaptura.Image = Nothing
    End Sub
#End Region
    Public Sub New(Ex As Exception, ByVal Enviar As Boolean)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        _Error = Ex
        CargarExcepcion(_Error)
        chkNotificar.Checked = Enviar
    End Sub

    Private Sub CargarExcepcion(ex As Exception)
        LimpiarTodo()

        ' Guardo la imagen antes de que aparezca mi formulario
        InfoError.ImagenCaptura = Quadralia.Imagenes.CapturarImagenPantalla

        txtMensaje.Text = ex.Message
        txtNombreAplicacion.Text = My.Application.Info.ProductName
        txtVersion.Text = My.Application.Info.Version.ToString
        txtFecha.Text = DateTime.Now.ToLongTimeString

        txtTipoExcepcion.Text = ex.GetType().ToString()
        txtMensajeError.Text = ex.Message
        txtOrigenExcepcion.Text = ex.Source
        txtPilaLllamadas.Text = String.Format("{0}{2}{1}", ex.InnerException, ex.StackTrace, Environment.NewLine)

        chkAdjuntarCaptura.Checked = True
        picCaptura.Image = InfoError.ImagenCaptura
    End Sub

    Private Sub GuardarCopia(sender As Object, e As EventArgs) Handles btnGuardarCopia.Click
        Dim CuadroDialogo As New SaveFileDialog()

        With CuadroDialogo
            .AutoUpgradeEnabled = True
            .Filter = "Archivos HTML(*.html)|*.html"
            .Title = "Seleccione la ruta donde desea guardar el archivo"
            .RestoreDirectory = True

            If .ShowDialog <> Windows.Forms.DialogResult.OK Then Exit Sub
        End With

        ' Guardo el documento
        Try
            My.Computer.FileSystem.WriteAllText(CuadroDialogo.FileName, GenerarReporte().Trim, False)
            Quadralia.Aplicacion.MostrarMessageBox("El informe se ha generado correctamente", "Informe Generado", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            Quadralia.Aplicacion.MostrarMessageBox("Error al tratar de generar el informe" & ex.Message, "Informe No Generado", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Public Function GenerarReporte() As String
        Dim Resultado As String = String.Format("<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">" & _
            "<html xmlns=""http://www.w3.org/1999/xhtml"">" & _
            "<html xmlns=""http://www.w3.org/1999/xhtml"">" & _
            "<head>" & _
            "<meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"" />" & _
            "<title>{0} {1} Crash Report</title>" & _
            "<style type=""text/css"">" & _
            ".message {{" & _
            "padding-top:5px;" & _
            "padding-bottom:5px;" & _
            "padding-right:20px;" & _
            "padding-left:20px;" & _
            "font-family:Sans-serif;" & _
            "}}" & _
            ".content" & _
            "{{" & _
            "border-style:dashed;" & _
            "border-width:1px;" & _
            "}}" & _
            ".title" & _
            "{{" & _
            "padding-top:1px;" & _
            "padding-bottom:1px;" & _
            "padding-right:10px;" & _
            "padding-left:10px;" & _
            "font-family:Arial;" & _
            "}}" & _
            "</style>" & _
            "</head>" & _
            "<body>" & _
            "<div class=""title"" style=""background-color: #FFCC99"">" & _
            "<h2>{0} {1} Crash Report</h2>" & _
            "</div>" & _
            "<br/>" & _
            "<div class=""content"">" & _
            "<div class=""title"" style=""background-color: #66CCFF;"">" & _
            "<h3>Windows Version</h3>" & _
            "</div>" & _
            "<div class=""message"">" & _
            "<p>{2}</p>" & _
            "</div>" & _
            "</div>" & _
            "<br/>" & _
            "<div class=""content"">" & _
            "<div class=""title"" style=""background-color: #66CCFF;"">" & _
            "<h3>Exception</h3>" & _
            "</div>" & _
            "<div class=""message"">" & _
            "{3}" & _
            "</div>", HttpUtility.HtmlEncode(txtNombreAplicacion.Text), HttpUtility.HtmlEncode(txtVersion.Text), HttpUtility.HtmlEncode(My.Computer.Info.OSFullName), CrearInforme(_Error))

        If Not (String.IsNullOrEmpty(txtComentarios.Text.Trim)) Then
            Resultado &= String.Format("<br/>" & _
                                       "<div class=""content"">" & _
                                       "<div class=""title"" style=""background-color: #66FF99;"">" & _
                                       "<h3>User Comment</h3>" & _
                                       "</div>" & _
                                       "<div class=""message"">" & _
                                       "<p>{0}</p>" & _
                                       "</div>" & _
                                       "</div>", HttpUtility.HtmlEncode(txtComentarios.Text.Trim()))
        End If

        Return Resultado

    End Function

    Function CrearInforme(Ex As Exception) As String
        Dim Resultado As String = String.Format("<br/>" & _
                                                "<div class=""content"">" & _
                                                "<div class=""title"" style=""background-color: #66CCFF;"">" & _
                                                "<h3>Exception Type</h3>" & _
                                                "</div>" & _
                                                "<div class=""message"">" & _
                                                "<p>{0}</p>" & _
                                                "</div>" & _
                                                "</div><br/>" & _
                                                "<div class=""content"">" & _
                                                "<div class=""title"" style=""background-color: #66CCFF;"">" & _
                                                "<h3>Error Message</h3>" & _
                                                "</div>" & _
                                                "<div class=""message"">" & _
                                                "<p>{1}</p>" & _
                                                "</div>" & _
                                                "</div><br/>" & _
                                                "<div class=""content"">" & _
                                                "<div class=""title"" style=""background-color: #66CCFF;"">" & _
                                                "<h3>Source</h3>" & _
                                                "</div>" & _
                                                "<div class=""message"">" & _
                                                "<p>{2}</p>" & _
                                                "</div>" & _
                                                "</div><br/>" & _
                                                "<div class=""content"">" & _
                                                "<div class=""title"" style=""background-color: #66CCFF;"">" & _
                                                "<h3>Stack Trace</h3>" & _
                                                "</div>" & _
                                                "<div class=""message"">" & _
                                                "<p>{3}</p>" & _
                                                "</div>" & _
                                                "</div>", HttpUtility.HtmlEncode(Ex.GetType().ToString()), HttpUtility.HtmlEncode(Ex.Message), HttpUtility.HtmlEncode(Ex.Source), HttpUtility.HtmlEncode(Ex.StackTrace).Replace(Environment.NewLine, "<br/>"))

        If _Error.InnerException IsNot Nothing Then
            Resultado &= String.Format("<br/>" & _
                                       "<div class=""content"">" & _
                                       "<div class=""title"" style=""background-color: #66CCFF;"">" & _
                                       "<h3>Inner Exception</h3>" & _
                                       "</div>" & _
                                       "<div class=""message"">" & _
                                       "{0}" & _
                                       "</div>" & _
                                       "</div>", CrearInforme(Ex.InnerException))
        End If

        Resultado &= "<br />"
        Return Resultado
    End Function

    Private Sub Inicio(sender As Object, e As EventArgs) Handles MyBase.Load
        Quadralia.Formularios.AutoTabular(Me, True)
        tabGeneral.SelectedIndex = 0
    End Sub

    Private Sub Aceptar(sender As Object, e As EventArgs) Handles btnAceptar.Click
        With Me.InfoError
            .CapturarPantalla = chkAdjuntarCaptura.Checked
            .ComentarioCliente = txtComentarios.Text.Trim
            If Not .CapturarPantalla Then .ImagenCaptura = Nothing
            .Personalizado1 = txtEmail.Text
        End With

        If chkNotificar.Checked Then
            Me.DialogResult = Windows.Forms.DialogResult.OK
        Else
            Me.DialogResult = Windows.Forms.DialogResult.Cancel
        End If

        Me.Close()
    End Sub
End Class
