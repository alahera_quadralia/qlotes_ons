﻿Public Class frmLectorBarrasEmpaquetados
    Public Property Contexto As Entidades

#Region " ENUMERADOS "
    Private Enum EstadosLectura
        EsperaLectura = 0
        LecturaCorrecta = 1
        LecturaIncorrecta = 2
        EmpaquetadoYaEnviado = 3
    End Enum
#End Region
#Region " DECLARACIONES "
    Private _Items As New List(Of Empaquetado)
    Private _Exclusiones As New List(Of Empaquetado)
    Private _Estado As EstadosLectura = EstadosLectura.EsperaLectura
#End Region
#Region " PROPIEDADES "
    ''' <summary>
    ''' Listado de Empaquetados seleccionadas por el usuario
    ''' </summary>    
    Public ReadOnly Property Items As List(Of Empaquetado)
        Get
            Return _Items
        End Get
    End Property

    Private Property Estado As EstadosLectura
        Get
            Return _Estado
        End Get
        Set(value As EstadosLectura)
            _Estado = value

            Select Case value
                Case EstadosLectura.EmpaquetadoYaEnviado
                    lblEstado.StateDisabled.Border.Color1 = Color.FromArgb(102, 81, 44)
                    lblEstado.StateDisabled.Back.Color1 = Color.FromArgb(252, 248, 227)
                    lblEstado.StateDisabled.Content.Color1 = Color.FromArgb(138, 109, 59)
                    lblEstado.Text = "Empaquetado presente en otro empaquetado"

                Case EstadosLectura.LecturaCorrecta
                    lblEstado.StateDisabled.Border.Color1 = Color.FromArgb(43, 84, 44)
                    lblEstado.StateDisabled.Back.Color1 = Color.FromArgb(223, 240, 216)
                    lblEstado.StateDisabled.Content.Color1 = Color.FromArgb(60, 118, 61)
                    lblEstado.Text = "Empaquetado localizada correctamente"

                Case EstadosLectura.LecturaIncorrecta
                    lblEstado.StateDisabled.Border.Color1 = Color.FromArgb(132, 53, 52)
                    lblEstado.StateDisabled.Back.Color1 = Color.FromArgb(242, 222, 222)
                    lblEstado.StateDisabled.Content.Color1 = Color.FromArgb(169, 68, 66)
                    lblEstado.Text = "Empaquetado no localizada"

                Case Else
                    lblEstado.StateDisabled.Border.Color1 = Color.FromArgb(36, 82, 149)
                    lblEstado.StateDisabled.Back.Color1 = Color.FromArgb(217, 237, 247)
                    lblEstado.StateDisabled.Content.Color1 = Color.FromArgb(49, 112, 143)
                    lblEstado.Text = "Esperando lectura"

            End Select
        End Set
    End Property
#End Region
#Region " LIMPIEZA "
    ''' <summary>
    ''' Limpia todos los controles
    ''' </summary>
    Private Sub Limpiar()
        txtCodigo.Clear()
        dgvEmpaquetados.DataSource = Nothing
        dgvEmpaquetados.Rows.Clear()
    End Sub
#End Region
#Region " CONTROL DE BOTONES "
    ''' <summary>
    ''' Controla los botones del formulario
    ''' </summary>
    Private Sub ControlarBotones()
        btnEmpaquetadoEliminar.Enabled = IIf(dgvEmpaquetados.SelectedRows.Count > 0, ButtonEnabled.True, ButtonEnabled.False)
    End Sub
#End Region
#Region " CARGAR / GUARDAR / CANCELAR "
    ''' <summary>
    ''' Muestra las Empaquetados cargadas en el DGV
    ''' </summary>
    Private Sub RefrescarEmpaquetados()
        dgvEmpaquetados.DataSource = Nothing
        dgvEmpaquetados.Rows.Clear()

        dgvEmpaquetados.DataSource = _Items
        ControlarBotones()
    End Sub

    ''' <summary>
    ''' Busca la Empaquetado escrita actualmente
    ''' </summary>
    Private Sub BuscarEmpaquetado()
        Dim CodigoEmpaquetado As String = txtCodigo.Text
        Dim Aux As Integer = 0

        ' Me quedo con el código hasta encontrar el último igual
        If Not Integer.TryParse(CodigoEmpaquetado, Aux) AndAlso CodigoEmpaquetado.IndexOf("=") >= 0 Then
            CodigoEmpaquetado = CodigoEmpaquetado.Substring(CodigoEmpaquetado.LastIndexOf("=") + 1)
        End If

        txtCodigo.Clear()

        ' Si no puedo parsear el código. Lectura incorrecta
        If Not Integer.TryParse(CodigoEmpaquetado, Aux) Then
            Me.Estado = EstadosLectura.LecturaIncorrecta
            Exit Sub
        End If

        ' Busco la Empaquetado
        Dim Empaquetado As Empaquetado = Nothing

        Try
            Empaquetado = (From etq As Empaquetado In Contexto.Empaquetados Where etq.id = Aux Select etq).FirstOrDefault

            If Empaquetado Is Nothing Then
                Me.Estado = EstadosLectura.LecturaIncorrecta
            ElseIf Empaquetado.LoteSalida IsNot Nothing OrElse (_Exclusiones IsNot Nothing AndAlso _Exclusiones.Contains(Empaquetado)) Then
                Me.Estado = EstadosLectura.EmpaquetadoYaEnviado
            Else
                Me.Estado = EstadosLectura.LecturaCorrecta

                ' La añado al dgv de Empaquetados leidas
                If Not _Items.Contains(Empaquetado) Then _Items.Add(Empaquetado)
                RefrescarEmpaquetados()
            End If
        Catch ex As Exception
            Me.Estado = EstadosLectura.LecturaIncorrecta
        End Try
    End Sub

    ''' <summary>
    ''' Añade la lista de elementos seleccionados y los devuelve
    ''' </summary>
    Private Sub Aceptar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    ''' <summary>
    ''' El usuario indica que no desea crear un nuevo contacto
    ''' </summary>
    Private Sub Cancelar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Items.Clear()
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    ''' <summary>
    ''' Elimina las Empaquetados seleccionadas de los resultados
    ''' </summary>
    Private Sub EliminarEmpaquetados(sender As Object, e As EventArgs) Handles btnEmpaquetadoEliminar.Click
        ' Validaciones
        If dgvEmpaquetados.SelectedRows.Count <= 0 Then Exit Sub

        For Each Fila As DataGridViewRow In dgvEmpaquetados.SelectedRows
            If Fila.DataBoundItem Is Nothing Then Exit Sub

            If _Items.Contains(Fila.DataBoundItem) Then _Items.Remove(Fila.DataBoundItem)
        Next

        RefrescarEmpaquetados()
    End Sub
#End Region
#Region " RUTINA DE INICIO "
    Public Sub New(ByVal Contexto As Entidades)
        Me.New(Contexto, New List(Of Empaquetado))
    End Sub

    Public Sub New(ByVal Contexto As Entidades, Exclusiones As List(Of Empaquetado))
        Me.Contexto = Contexto
        Me._Exclusiones = Exclusiones

        ' Llamada necesaria para el Diseñador de Windows Forms.
        InitializeComponent()

        Quadralia.Formularios.AutoTabular(Me)

        ' No quiero nuevas columnas
        dgvEmpaquetados.AutoGenerateColumns = False

        ' Limpio los controles
        Limpiar()
        ControlarBotones()
        Me.Estado = EstadosLectura.EsperaLectura
    End Sub
#End Region
#Region " MANEJO DE CONTROLES "
    ''' <summary>
    ''' Ordena los resultados del DGV de Resultados    
    ''' </summary>
    Private Sub OrdenarResultados(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvEmpaquetados.ColumnHeaderMouseClick
        Quadralia.Formularios.Funcionalidad.OrdenarDGV(Of Empaquetado)(dgvEmpaquetados, e)
    End Sub

    ''' <summary>
    ''' Controlamos el enter para que vaya al botón de aceptar
    ''' </summary>
    Private Sub ControlEnter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvEmpaquetados.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.Handled = True
            Aceptar(Nothing, Nothing)
        ElseIf e.KeyCode = Keys.Tab Then
            e.Handled = True
            Me.SelectNextControl(sender, True, True, True, True)
        End If
    End Sub
#End Region
#Region " RESTO DE FUNCIONES "
    ''' <summary>
    ''' Evita que salga el molesto error del DGV
    ''' </summary>
    Private Sub EvitarErrores(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvEmpaquetados.DataError
        e.Cancel = True
    End Sub

    ''' <summary>
    ''' Lanza la busqueda de registros al pulsar enter
    ''' </summary>
    Private Sub LanzarBusquedaEnEnter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCodigo.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.Handled = True
            e.SuppressKeyPress = True

            BuscarEmpaquetado()
        End If
    End Sub

    ''' <summary>
    ''' Evento que ocurre cuando el usuario selecciona un elemento del DGV
    ''' </summary>
    Private Sub SeleccionModificada(sender As Object, e As EventArgs) Handles dgvEmpaquetados.SelectionChanged
        ControlarBotones()
    End Sub
#End Region
End Class