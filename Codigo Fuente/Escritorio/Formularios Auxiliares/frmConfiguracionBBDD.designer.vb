﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConfiguracionBBDD
    Inherits Escritorio.FormularioHijo

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmConfiguracionBBDD))
        Me.pCuerpo = New ComponentFactory.Krypton.Toolkit.KryptonPanel()
        Me.tabGeneral = New ComponentFactory.Krypton.Navigator.KryptonNavigator()
        Me.tbpAplicacion = New ComponentFactory.Krypton.Navigator.KryptonPage()
        Me.tblOrganizadorAplicacion = New System.Windows.Forms.TableLayoutPanel()
        Me.picProgresoPingAplicacion = New System.Windows.Forms.PictureBox()
        Me.btnPingServidorAplicacion = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.lblServidorAplicacion = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblUsuarioAplicacion = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblClaveAplicacion = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblPuertoAplicacion = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtServidorAplicacion = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtPuertoAplicacion = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtNombreBaseDatosAplicacion = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtClaveAplicacion = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtUsuarioAplicacion = New Escritorio.Quadralia.Controles.aTextBox()
        Me.lblNombreBaseDatosAplicacion = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.txtRespuestaServidorAplicacion = New ComponentFactory.Krypton.Toolkit.KryptonTextBox()
        Me.lblRespuestaServidorAplicacion = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.grpCabecera = New ComponentFactory.Krypton.Toolkit.KryptonGroup()
        Me.lblTitulo = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.epErrores = New Escritorio.Quadralia.Controles.Validacion.GestorErrores()
        CType(Me.pCuerpo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pCuerpo.SuspendLayout()
        CType(Me.tabGeneral, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabGeneral.SuspendLayout()
        CType(Me.tbpAplicacion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpAplicacion.SuspendLayout()
        Me.tblOrganizadorAplicacion.SuspendLayout()
        CType(Me.picProgresoPingAplicacion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpCabecera, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grpCabecera.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpCabecera.Panel.SuspendLayout()
        Me.grpCabecera.SuspendLayout()
        CType(Me.epErrores, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pCuerpo
        '
        Me.pCuerpo.Controls.Add(Me.tabGeneral)
        Me.pCuerpo.Controls.Add(Me.grpCabecera)
        resources.ApplyResources(Me.pCuerpo, "pCuerpo")
        Me.pCuerpo.Name = "pCuerpo"
        '
        'tabGeneral
        '
        Me.tabGeneral.Bar.BarOrientation = ComponentFactory.Krypton.Toolkit.VisualOrientation.Left
        Me.tabGeneral.Bar.ItemOrientation = ComponentFactory.Krypton.Toolkit.ButtonOrientation.FixedTop
        Me.tabGeneral.Bar.TabBorderStyle = ComponentFactory.Krypton.Toolkit.TabBorderStyle.RoundedEqualSmall
        Me.tabGeneral.Bar.TabStyle = ComponentFactory.Krypton.Toolkit.TabStyle.LowProfile
        Me.tabGeneral.Button.ButtonDisplayLogic = ComponentFactory.Krypton.Navigator.ButtonDisplayLogic.None
        Me.tabGeneral.Button.CloseButtonAction = ComponentFactory.Krypton.Navigator.CloseButtonAction.None
        Me.tabGeneral.Button.CloseButtonDisplay = ComponentFactory.Krypton.Navigator.ButtonDisplay.Hide
        resources.ApplyResources(Me.tabGeneral, "tabGeneral")
        Me.tabGeneral.Name = "tabGeneral"
        Me.tabGeneral.Pages.AddRange(New ComponentFactory.Krypton.Navigator.KryptonPage() {Me.tbpAplicacion})
        Me.tabGeneral.SelectedIndex = 0
        '
        'tbpAplicacion
        '
        Me.tbpAplicacion.AutoHiddenSlideSize = New System.Drawing.Size(200, 200)
        Me.tbpAplicacion.Controls.Add(Me.tblOrganizadorAplicacion)
        Me.tbpAplicacion.Controls.Add(Me.txtRespuestaServidorAplicacion)
        Me.tbpAplicacion.Controls.Add(Me.lblRespuestaServidorAplicacion)
        Me.tbpAplicacion.Flags = 65534
        Me.tbpAplicacion.LastVisibleSet = True
        resources.ApplyResources(Me.tbpAplicacion, "tbpAplicacion")
        Me.tbpAplicacion.Name = "tbpAplicacion"
        Me.tbpAplicacion.UniqueName = "F957580818644F3C0F9437DA0BD45BE9"
        '
        'tblOrganizadorAplicacion
        '
        resources.ApplyResources(Me.tblOrganizadorAplicacion, "tblOrganizadorAplicacion")
        Me.tblOrganizadorAplicacion.BackColor = System.Drawing.Color.Transparent
        Me.tblOrganizadorAplicacion.Controls.Add(Me.picProgresoPingAplicacion, 5, 0)
        Me.tblOrganizadorAplicacion.Controls.Add(Me.btnPingServidorAplicacion, 0, 4)
        Me.tblOrganizadorAplicacion.Controls.Add(Me.lblServidorAplicacion, 0, 1)
        Me.tblOrganizadorAplicacion.Controls.Add(Me.lblUsuarioAplicacion, 0, 3)
        Me.tblOrganizadorAplicacion.Controls.Add(Me.lblClaveAplicacion, 3, 3)
        Me.tblOrganizadorAplicacion.Controls.Add(Me.lblPuertoAplicacion, 0, 2)
        Me.tblOrganizadorAplicacion.Controls.Add(Me.txtServidorAplicacion, 2, 1)
        Me.tblOrganizadorAplicacion.Controls.Add(Me.txtPuertoAplicacion, 2, 2)
        Me.tblOrganizadorAplicacion.Controls.Add(Me.txtNombreBaseDatosAplicacion, 5, 2)
        Me.tblOrganizadorAplicacion.Controls.Add(Me.txtClaveAplicacion, 5, 3)
        Me.tblOrganizadorAplicacion.Controls.Add(Me.txtUsuarioAplicacion, 2, 3)
        Me.tblOrganizadorAplicacion.Controls.Add(Me.lblNombreBaseDatosAplicacion, 3, 2)
        Me.tblOrganizadorAplicacion.Name = "tblOrganizadorAplicacion"
        '
        'picProgresoPingAplicacion
        '
        resources.ApplyResources(Me.picProgresoPingAplicacion, "picProgresoPingAplicacion")
        Me.picProgresoPingAplicacion.BackColor = System.Drawing.Color.Transparent
        Me.picProgresoPingAplicacion.Image = Global.Escritorio.My.Resources.Resources.Cargando_32
        Me.picProgresoPingAplicacion.Name = "picProgresoPingAplicacion"
        Me.picProgresoPingAplicacion.TabStop = False
        '
        'btnPingServidorAplicacion
        '
        resources.ApplyResources(Me.btnPingServidorAplicacion, "btnPingServidorAplicacion")
        Me.tblOrganizadorAplicacion.SetColumnSpan(Me.btnPingServidorAplicacion, 6)
        Me.btnPingServidorAplicacion.Name = "btnPingServidorAplicacion"
        Me.btnPingServidorAplicacion.Values.Text = resources.GetString("btnPingServidorAplicacion.Values.Text")
        '
        'lblServidorAplicacion
        '
        resources.ApplyResources(Me.lblServidorAplicacion, "lblServidorAplicacion")
        Me.lblServidorAplicacion.Name = "lblServidorAplicacion"
        Me.lblServidorAplicacion.Values.Text = resources.GetString("lblServidorAplicacion.Values.Text")
        '
        'lblUsuarioAplicacion
        '
        resources.ApplyResources(Me.lblUsuarioAplicacion, "lblUsuarioAplicacion")
        Me.lblUsuarioAplicacion.Name = "lblUsuarioAplicacion"
        Me.lblUsuarioAplicacion.Values.Text = resources.GetString("lblUsuarioAplicacion.Values.Text")
        '
        'lblClaveAplicacion
        '
        resources.ApplyResources(Me.lblClaveAplicacion, "lblClaveAplicacion")
        Me.lblClaveAplicacion.Name = "lblClaveAplicacion"
        Me.lblClaveAplicacion.Values.Text = resources.GetString("lblClaveAplicacion.Values.Text")
        '
        'lblPuertoAplicacion
        '
        resources.ApplyResources(Me.lblPuertoAplicacion, "lblPuertoAplicacion")
        Me.lblPuertoAplicacion.Name = "lblPuertoAplicacion"
        Me.lblPuertoAplicacion.Values.Text = resources.GetString("lblPuertoAplicacion.Values.Text")
        '
        'txtServidorAplicacion
        '
        Me.tblOrganizadorAplicacion.SetColumnSpan(Me.txtServidorAplicacion, 4)
        Me.txtServidorAplicacion.controlarBotonBorrar = True
        resources.ApplyResources(Me.txtServidorAplicacion, "txtServidorAplicacion")
        Me.txtServidorAplicacion.Formato = ""
        Me.txtServidorAplicacion.mostrarSiempreBotonBorrar = False
        Me.txtServidorAplicacion.Name = "txtServidorAplicacion"
        Me.txtServidorAplicacion.seleccionarTodo = True
        '
        'txtPuertoAplicacion
        '
        Me.txtPuertoAplicacion.controlarBotonBorrar = True
        resources.ApplyResources(Me.txtPuertoAplicacion, "txtPuertoAplicacion")
        Me.txtPuertoAplicacion.Formato = ""
        Me.txtPuertoAplicacion.mostrarSiempreBotonBorrar = False
        Me.txtPuertoAplicacion.Name = "txtPuertoAplicacion"
        Me.txtPuertoAplicacion.seleccionarTodo = True
        '
        'txtNombreBaseDatosAplicacion
        '
        Me.txtNombreBaseDatosAplicacion.controlarBotonBorrar = True
        resources.ApplyResources(Me.txtNombreBaseDatosAplicacion, "txtNombreBaseDatosAplicacion")
        Me.txtNombreBaseDatosAplicacion.Formato = ""
        Me.txtNombreBaseDatosAplicacion.mostrarSiempreBotonBorrar = False
        Me.txtNombreBaseDatosAplicacion.Name = "txtNombreBaseDatosAplicacion"
        Me.txtNombreBaseDatosAplicacion.seleccionarTodo = True
        '
        'txtClaveAplicacion
        '
        Me.txtClaveAplicacion.controlarBotonBorrar = True
        resources.ApplyResources(Me.txtClaveAplicacion, "txtClaveAplicacion")
        Me.txtClaveAplicacion.Formato = ""
        Me.txtClaveAplicacion.mostrarSiempreBotonBorrar = False
        Me.txtClaveAplicacion.Name = "txtClaveAplicacion"
        Me.txtClaveAplicacion.seleccionarTodo = True
        Me.txtClaveAplicacion.UseSystemPasswordChar = True
        '
        'txtUsuarioAplicacion
        '
        Me.txtUsuarioAplicacion.controlarBotonBorrar = True
        resources.ApplyResources(Me.txtUsuarioAplicacion, "txtUsuarioAplicacion")
        Me.txtUsuarioAplicacion.Formato = ""
        Me.txtUsuarioAplicacion.mostrarSiempreBotonBorrar = False
        Me.txtUsuarioAplicacion.Name = "txtUsuarioAplicacion"
        Me.txtUsuarioAplicacion.seleccionarTodo = True
        '
        'lblNombreBaseDatosAplicacion
        '
        resources.ApplyResources(Me.lblNombreBaseDatosAplicacion, "lblNombreBaseDatosAplicacion")
        Me.lblNombreBaseDatosAplicacion.Name = "lblNombreBaseDatosAplicacion"
        Me.lblNombreBaseDatosAplicacion.Values.Text = resources.GetString("lblNombreBaseDatosAplicacion.Values.Text")
        '
        'txtRespuestaServidorAplicacion
        '
        resources.ApplyResources(Me.txtRespuestaServidorAplicacion, "txtRespuestaServidorAplicacion")
        Me.txtRespuestaServidorAplicacion.Name = "txtRespuestaServidorAplicacion"
        Me.txtRespuestaServidorAplicacion.ReadOnly = True
        Me.txtRespuestaServidorAplicacion.TabStop = False
        '
        'lblRespuestaServidorAplicacion
        '
        resources.ApplyResources(Me.lblRespuestaServidorAplicacion, "lblRespuestaServidorAplicacion")
        Me.lblRespuestaServidorAplicacion.Name = "lblRespuestaServidorAplicacion"
        Me.lblRespuestaServidorAplicacion.Values.Text = resources.GetString("lblRespuestaServidorAplicacion.Values.Text")
        '
        'grpCabecera
        '
        resources.ApplyResources(Me.grpCabecera, "grpCabecera")
        Me.grpCabecera.GroupBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.PanelCustom1
        Me.grpCabecera.Name = "grpCabecera"
        '
        'grpCabecera.Panel
        '
        Me.grpCabecera.Panel.Controls.Add(Me.lblTitulo)
        Me.grpCabecera.StateCommon.Border.Color1 = System.Drawing.Color.DarkOrange
        Me.grpCabecera.StateCommon.Border.DrawBorders = ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom
        Me.grpCabecera.StateCommon.Border.Width = 5
        '
        'lblTitulo
        '
        Me.lblTitulo.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.Custom1
        resources.ApplyResources(Me.lblTitulo, "lblTitulo")
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Values.Image = Global.Escritorio.My.Resources.Resources.ConfiguracionBaseDatos_32
        Me.lblTitulo.Values.Text = resources.GetString("lblTitulo.Values.Text")
        '
        'epErrores
        '
        Me.epErrores.Alineacion = System.Windows.Forms.ErrorIconAlignment.TopLeft
        Me.epErrores.ContainerControl = Me
        '
        'frmConfiguracionBBDD
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.pCuerpo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmConfiguracionBBDD"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        CType(Me.pCuerpo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pCuerpo.ResumeLayout(False)
        CType(Me.tabGeneral, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabGeneral.ResumeLayout(False)
        CType(Me.tbpAplicacion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpAplicacion.ResumeLayout(False)
        Me.tbpAplicacion.PerformLayout()
        Me.tblOrganizadorAplicacion.ResumeLayout(False)
        Me.tblOrganizadorAplicacion.PerformLayout()
        CType(Me.picProgresoPingAplicacion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grpCabecera.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpCabecera.Panel.ResumeLayout(False)
        Me.grpCabecera.Panel.PerformLayout()
        CType(Me.grpCabecera, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpCabecera.ResumeLayout(False)
        CType(Me.epErrores, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pCuerpo As ComponentFactory.Krypton.Toolkit.KryptonPanel
    Friend WithEvents epErrores As Quadralia.Controles.Validacion.GestorErrores
    Friend WithEvents tabGeneral As ComponentFactory.Krypton.Navigator.KryptonNavigator
    Friend WithEvents tbpAplicacion As ComponentFactory.Krypton.Navigator.KryptonPage
    Friend WithEvents picProgresoPingAplicacion As System.Windows.Forms.PictureBox
    Friend WithEvents grpCabecera As ComponentFactory.Krypton.Toolkit.KryptonGroup
    Friend WithEvents lblTitulo As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtRespuestaServidorAplicacion As ComponentFactory.Krypton.Toolkit.KryptonTextBox
    Friend WithEvents lblRespuestaServidorAplicacion As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents btnPingServidorAplicacion As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents tblOrganizadorAplicacion As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblNombreBaseDatosAplicacion As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblServidorAplicacion As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblUsuarioAplicacion As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblClaveAplicacion As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblPuertoAplicacion As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtServidorAplicacion As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents txtPuertoAplicacion As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents txtNombreBaseDatosAplicacion As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents txtClaveAplicacion As Escritorio.Quadralia.Controles.aTextBox
    Friend WithEvents txtUsuarioAplicacion As Escritorio.Quadralia.Controles.aTextBox
End Class
