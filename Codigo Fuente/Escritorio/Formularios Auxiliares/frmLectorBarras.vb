﻿Imports System.Media

Public Class frmLectorBarras
    Public Property Contexto As Entidades

#Region " ENUMERADOS "
    Private Enum EstadosLectura
        EsperaLectura = 0
        LecturaCorrecta = 1
        LecturaIncorrecta = 2
        EtiquetaYaEmpaquetada = 3
        EtiquetaOtroLote = 4
        EtiquetaDescartada = 5
    End Enum
#End Region
#Region " DECLARACIONES "
    Private _Items As New List(Of Etiqueta)
    Private _Exclusiones As New List(Of Etiqueta)
    Private _Estado As EstadosLectura = EstadosLectura.EsperaLectura
#End Region
#Region " PROPIEDADES "
    ''' <summary>
    ''' Listado de Etiquetas seleccionadas por el usuario
    ''' </summary>    
    Public ReadOnly Property Items As List(Of Etiqueta)
        Get
            Return _Items
        End Get
    End Property

    Private Property Estado As EstadosLectura
        Get
            Return _Estado
        End Get
        Set(value As EstadosLectura)
            _Estado = value

            Select Case value
                Case EstadosLectura.EtiquetaYaEmpaquetada
                    lblEstado.StateDisabled.Border.Color1 = Color.FromArgb(102, 81, 44)
                    lblEstado.StateDisabled.Back.Color1 = Color.FromArgb(252, 248, 227)
                    lblEstado.StateDisabled.Content.Color1 = Color.FromArgb(138, 109, 59)
                    lblEstado.Text = "Etiqueta presente en otro empaquetado"
                    LanzarPitidos(1500, 300)
                Case EstadosLectura.LecturaCorrecta
                    lblEstado.StateDisabled.Border.Color1 = Color.FromArgb(43, 84, 44)
                    lblEstado.StateDisabled.Back.Color1 = Color.FromArgb(223, 240, 216)
                    lblEstado.StateDisabled.Content.Color1 = Color.FromArgb(60, 118, 61)
                    lblEstado.Text = "Etiqueta localizada correctamente"
                Case EstadosLectura.LecturaIncorrecta
                    lblEstado.StateDisabled.Border.Color1 = Color.FromArgb(132, 53, 52)
                    lblEstado.StateDisabled.Back.Color1 = Color.FromArgb(242, 222, 222)
                    lblEstado.StateDisabled.Content.Color1 = Color.FromArgb(169, 68, 66)
                    lblEstado.Text = "Etiqueta no localizada"
                    LanzarPitidos(1500, 300)
                Case EstadosLectura.EtiquetaOtroLote
                    lblEstado.StateDisabled.Border.Color1 = Color.FromArgb(32, 32, 32)
                    lblEstado.StateDisabled.Back.Color1 = Color.FromArgb(133, 0, 0)
                    lblEstado.StateDisabled.Content.Color1 = Color.FromArgb(255, 255, 255)
                    lblEstado.Text = "Etiqueta pertenece a otro lote"
                    LanzarPitidos(1500, 300)
                Case EstadosLectura.EtiquetaDescartada
                    lblEstado.StateDisabled.Border.Color1 = Color.FromArgb(32, 32, 32)
                    lblEstado.StateDisabled.Back.Color1 = Color.FromArgb(133, 0, 0)
                    lblEstado.StateDisabled.Content.Color1 = Color.FromArgb(255, 255, 255)
                    lblEstado.Text = "Etiqueta descartada"
                    LanzarPitidos(1500, 300)
                Case Else
                    lblEstado.StateDisabled.Border.Color1 = Color.FromArgb(36, 82, 149)
                    lblEstado.StateDisabled.Back.Color1 = Color.FromArgb(217, 237, 247)
                    lblEstado.StateDisabled.Content.Color1 = Color.FromArgb(49, 112, 143)
                    lblEstado.Text = "Esperando lectura"

            End Select
        End Set
    End Property
#End Region
#Region " LIMPIEZA "
    ''' <summary>
    ''' Limpia todos los controles
    ''' </summary>
    Private Sub Limpiar()
        txtCodigo.Clear()
        dgvEtiquetas.DataSource = Nothing
        dgvEtiquetas.Rows.Clear()
    End Sub
#End Region
#Region " CONTROL DE BOTONES "
    ''' <summary>
    ''' Controla los botones del formulario
    ''' </summary>
    Private Sub ControlarBotones()
        btnEtiquetaEliminar.Enabled = IIf(dgvEtiquetas.SelectedRows.Count > 0, ButtonEnabled.True, ButtonEnabled.False)
    End Sub
#End Region
#Region " CARGAR / GUARDAR / CANCELAR "
    ''' <summary>
    ''' Muestra las etiquetas cargadas en el DGV
    ''' </summary>
    Private Sub RefrescarEtiquetas()
        dgvEtiquetas.DataSource = Nothing
        dgvEtiquetas.Rows.Clear()

        dgvEtiquetas.DataSource = _Items
        hdrContenido.ValuesPrimary.Heading = "Selección de etiquetas " + _Items.Count().ToString()
        ControlarBotones()
    End Sub

    ''' <summary>
    ''' Busca la etiqueta escrita actualmente
    ''' </summary>
    Private Sub BuscarEtiqueta()
        Dim CodigoEtiqueta As String = txtCodigo.Text.ToUpper()
        Dim Aux As Integer = 0

        'If (CodigoEtiqueta.Length < 12) Then
        '    CodigoEtiqueta = txtCodigo.Text.PadLeft(12, Convert.ToChar("0"))
        'End If
        'If CodigoEtiqueta.Contains("/") Then
        '    Dim split = CodigoEtiqueta.Split(cConfiguracionPrograma.Instancia.CodigoTrazamare)
        '    CodigoEtiqueta = split(split.Length - 1)
        'End If
        ''Me quedo con el código hasta encontrar el último igual
        If Not Integer.TryParse(CodigoEtiqueta, Aux) AndAlso CodigoEtiqueta.IndexOf(cConfiguracionPrograma.Instancia.CodigoTrazamare) >= 0 Then
            CodigoEtiqueta = CodigoEtiqueta.Substring(CodigoEtiqueta.LastIndexOf(cConfiguracionPrograma.Instancia.CodigoTrazamare) + 2)
        Else
            Dim split = CodigoEtiqueta.ToUpper.Split(cConfiguracionPrograma.Instancia.CodigoTrazamare)
            CodigoEtiqueta = split(split.Length - 1)
        End If

        txtCodigo.Clear()

        ' Si no puedo parsear el código. Lectura incorrecta
        If Not Integer.TryParse(CodigoEtiqueta, Aux) Then
            Me.Estado = EstadosLectura.LecturaIncorrecta
            Exit Sub
        End If

        ' Busco la Etiqueta
        Dim Etiqueta As Etiqueta = Nothing

        Try
            Etiqueta = (From etq As Etiqueta In Contexto.Etiquetas Where etq.id = Aux Select etq).FirstOrDefault

            If Etiqueta Is Nothing Then
                Me.Estado = EstadosLectura.LecturaIncorrecta
            ElseIf Etiqueta.Empaquetado IsNot Nothing OrElse (_Exclusiones IsNot Nothing AndAlso _Exclusiones.Contains(Etiqueta)) Then
                Me.Estado = EstadosLectura.EtiquetaYaEmpaquetada
            ElseIf Etiqueta.fechaBaja.HasValue Then
                Me.Estado = EstadosLectura.EtiquetaDescartada
            Else
                Me.Estado = EstadosLectura.LecturaCorrecta

                ''comprobamos que sólo se usa un lote en el paquete
                If (Items.Count() > 0) Then
                    If (Items(0).Numero_Lote <> Etiqueta.Numero_Lote) Then
                        Me.Estado = EstadosLectura.EtiquetaOtroLote
                        'Exit Sub
                    End If
                End If

                ' La añado al dgv de etiquetas leidas
                If Not _Items.Contains(Etiqueta) Then _Items.Add(Etiqueta)
                RefrescarEtiquetas()
            End If
        Catch ex As Exception
            Me.Estado = EstadosLectura.LecturaIncorrecta
        End Try
    End Sub

    ''' <summary>
    ''' Añade la lista de elementos seleccionados y los devuelve
    ''' </summary>
    Private Sub Aceptar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click

        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    ''' <summary>
    ''' El usuario indica que no desea crear un nuevo contacto
    ''' </summary>
    Private Sub Cancelar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Items.Clear()
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub
#End Region
#Region " RUTINA DE INICIO "
    Public Sub New(ByVal Contexto As Entidades)
        Me.New(Contexto, New List(Of Etiqueta))
    End Sub

    Public Sub New(ByVal Contexto As Entidades, Exclusiones As List(Of Etiqueta))
        Me.Contexto = Contexto
        Me._Exclusiones = Exclusiones

        ' Llamada necesaria para el Diseñador de Windows Forms.
        InitializeComponent()

        Quadralia.Formularios.AutoTabular(Me)

        ' No quiero nuevas columnas
        dgvEtiquetas.AutoGenerateColumns = False

        ' Limpio los controles
        Limpiar()
        ControlarBotones()
        Me.Estado = EstadosLectura.EsperaLectura
    End Sub
#End Region
#Region " MANEJO DE CONTROLES "
    ''' <summary>
    ''' Ordena los resultados del DGV de Resultados    
    ''' </summary>
    Private Sub OrdenarResultados(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvEtiquetas.ColumnHeaderMouseClick
        Quadralia.Formularios.Funcionalidad.OrdenarDGV(Of Etiqueta)(dgvEtiquetas, e)
    End Sub

    ''' <summary>
    ''' Controlamos el enter para que vaya al botón de aceptar
    ''' </summary>
    Private Sub ControlEnter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvEtiquetas.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.Handled = True
            Aceptar(Nothing, Nothing)
        ElseIf e.KeyCode = Keys.Tab Then
            e.Handled = True
            Me.SelectNextControl(sender, True, True, True, True)
        End If
    End Sub
#End Region
#Region " RESTO DE FUNCIONES "

    ''' <summary>
    ''' Lanza tres pitidos
    ''' </summary>
    ''' <param name="pFrecuencia"></param>
    ''' <param name="pSleep">Tiempo entre pitidos</param>
    Private Sub LanzarPitidos(ByVal pFrecuencia As Int32, ByVal pSleep As Int32)
        System.Console.Beep(pFrecuencia, "300")
        Threading.Thread.Sleep(pSleep)
        System.Console.Beep(pFrecuencia, "300")
        Threading.Thread.Sleep(pSleep)
        System.Console.Beep(pFrecuencia, "1000")
    End Sub

    ''' <summary>
    ''' Evita que salga el molesto error del DGV
    ''' </summary>
    Private Sub EvitarErrores(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvEtiquetas.DataError
        e.Cancel = True
    End Sub

    ''' <summary>
    ''' Lanza la busqueda de registros al pulsar enter
    ''' </summary>
    Private Sub LanzarBusquedaEnEnter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCodigo.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.Handled = True
            e.SuppressKeyPress = True

            BuscarEtiqueta()
        End If
    End Sub

    ''' <summary>
    ''' Evento que ocurre cuando el usuario selecciona un elemento del DGV
    ''' </summary>
    Private Sub SeleccionModificada(sender As Object, e As EventArgs) Handles dgvEtiquetas.SelectionChanged
        ControlarBotones()
    End Sub
#End Region

    ''' <summary>
    ''' Elimina las etiquetas seleccionadas de los resultados
    ''' </summary>
    Private Sub EliminarEtiquetas(sender As Object, e As EventArgs) Handles btnEtiquetaEliminar.Click
        ' Validaciones
        If dgvEtiquetas.SelectedRows.Count <= 0 Then Exit Sub

        For Each Fila As DataGridViewRow In dgvEtiquetas.SelectedRows
            If Fila.DataBoundItem Is Nothing Then Exit Sub

            If _Items.Contains(Fila.DataBoundItem) Then _Items.Remove(Fila.DataBoundItem)
        Next

        RefrescarEtiquetas()
    End Sub
End Class