﻿Public Class frmLogSincronizacion
#Region " CONSTANTES "
    Private Const NOMBRE_SERVICIO As String = "qLotesSync"
#End Region
#Region " INICIO / FIN "
    ''' <summary>
    ''' Rutina del formulario
    ''' </summary>
    Private Sub Inicio(sender As Object, e As EventArgs) Handles MyBase.Load
        RefrescarLog(Nothing, Nothing)
    End Sub

    ''' <summary>
    ''' Cierra el formulario
    ''' </summary>
    Private Sub CerrarFormulario(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub
#End Region
#Region " SERVICIO "
    ''' <summary>
    ''' Refresca el log del servicio
    ''' </summary>
    Private Sub RefrescarLog(sender As Object, e As EntryWrittenEventArgs) Handles LogEventos.EntryWritten
        Dim Entradas As New List(Of EventLogEntry)
        If e Is Nothing OrElse e.Entry Is Nothing Then
            dgvLog.Rows.Clear()

            For Each Entrada As EventLogEntry In LogEventos.Entries
                If Entrada.Source <> NOMBRE_SERVICIO AndAlso Entrada.Source <> LogEventos.Source Then Continue For

                Entradas.Add(Entrada)
            Next
        Else
            If e.Entry.Source = NOMBRE_SERVICIO OrElse e.Entry.Source = LogEventos.Source Then Entradas.Add(e.Entry)
        End If

        Dim Imagen As Image = Nothing
        For Each Entrada As EventLogEntry In Entradas
            Select Case Entrada.EntryType
                Case EventLogEntryType.Error
                    Imagen = My.Resources.error_16
                Case EventLogEntryType.Information
                    Imagen = My.Resources.info_16
                Case EventLogEntryType.Warning
                    Imagen = My.Resources.warning_16
                Case Else
                    Imagen = My.Resources.Llave_16
            End Select


            dgvLog.Rows.Insert(0, Imagen, Entrada.EntryType, Entrada.TimeWritten, Entrada.InstanceId, Entrada.Message)
        Next
    End Sub
#End Region
End Class
