﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVisorLoteSalida
    Inherits ComponentFactory.Krypton.Toolkit.KryptonForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmVisorLoteSalida))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.btnAceptar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.btnCerrar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.tblOrganizador = New System.Windows.Forms.TableLayoutPanel()
        Me.dgvLotesSalida = New ComponentFactory.Krypton.Toolkit.KryptonDataGridView()
        Me.colFacturaCodigo = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colFacturaFecha = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colCliente = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.pFondo = New ComponentFactory.Krypton.Toolkit.KryptonPanel()
        Me.tblOrganizador.SuspendLayout()
        CType(Me.dgvLotesSalida, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pFondo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pFondo.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnAceptar
        '
        resources.ApplyResources(Me.btnAceptar, "btnAceptar")
        Me.btnAceptar.Name = "btnAceptar"
        Me.ToolTip1.SetToolTip(Me.btnAceptar, resources.GetString("btnAceptar.ToolTip"))
        Me.btnAceptar.Values.Text = resources.GetString("btnAceptar.Values.Text")
        '
        'btnCerrar
        '
        resources.ApplyResources(Me.btnCerrar, "btnCerrar")
        Me.btnCerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCerrar.Name = "btnCerrar"
        Me.ToolTip1.SetToolTip(Me.btnCerrar, resources.GetString("btnCerrar.ToolTip"))
        Me.btnCerrar.Values.Text = resources.GetString("btnCerrar.Values.Text")
        '
        'tblOrganizador
        '
        Me.tblOrganizador.BackColor = System.Drawing.Color.Transparent
        resources.ApplyResources(Me.tblOrganizador, "tblOrganizador")
        Me.tblOrganizador.Controls.Add(Me.dgvLotesSalida, 0, 0)
        Me.tblOrganizador.Controls.Add(Me.btnCerrar, 1, 1)
        Me.tblOrganizador.Controls.Add(Me.btnAceptar, 0, 1)
        Me.tblOrganizador.Name = "tblOrganizador"
        '
        'dgvLotesSalida
        '
        Me.dgvLotesSalida.AllowUserToAddRows = False
        Me.dgvLotesSalida.AllowUserToDeleteRows = False
        Me.dgvLotesSalida.AllowUserToResizeRows = False
        Me.dgvLotesSalida.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colFacturaCodigo, Me.colFacturaFecha, Me.colCliente})
        Me.tblOrganizador.SetColumnSpan(Me.dgvLotesSalida, 2)
        resources.ApplyResources(Me.dgvLotesSalida, "dgvLotesSalida")
        Me.dgvLotesSalida.MultiSelect = False
        Me.dgvLotesSalida.Name = "dgvLotesSalida"
        Me.dgvLotesSalida.ReadOnly = True
        Me.dgvLotesSalida.RowHeadersVisible = False
        Me.dgvLotesSalida.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvLotesSalida.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        '
        'colFacturaCodigo
        '
        Me.colFacturaCodigo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colFacturaCodigo.DataPropertyName = "codigo"
        resources.ApplyResources(Me.colFacturaCodigo, "colFacturaCodigo")
        Me.colFacturaCodigo.Name = "colFacturaCodigo"
        Me.colFacturaCodigo.ReadOnly = True
        '
        'colFacturaFecha
        '
        Me.colFacturaFecha.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colFacturaFecha.DataPropertyName = "fecha"
        DataGridViewCellStyle1.Format = "d"
        DataGridViewCellStyle1.NullValue = Nothing
        Me.colFacturaFecha.DefaultCellStyle = DataGridViewCellStyle1
        resources.ApplyResources(Me.colFacturaFecha, "colFacturaFecha")
        Me.colFacturaFecha.Name = "colFacturaFecha"
        Me.colFacturaFecha.ReadOnly = True
        '
        'colCliente
        '
        Me.colCliente.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colCliente.DataPropertyName = "nombreCliente"
        resources.ApplyResources(Me.colCliente, "colCliente")
        Me.colCliente.Name = "colCliente"
        Me.colCliente.ReadOnly = True
        '
        'pFondo
        '
        Me.pFondo.Controls.Add(Me.tblOrganizador)
        resources.ApplyResources(Me.pFondo, "pFondo")
        Me.pFondo.Name = "pFondo"
        '
        'frmVisorLoteSalida
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCerrar
        Me.Controls.Add(Me.pFondo)
        Me.Name = "frmVisorLoteSalida"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.tblOrganizador.ResumeLayout(False)
        CType(Me.dgvLotesSalida, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pFondo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pFondo.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnCerrar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents btnAceptar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents tblOrganizador As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents pFondo As ComponentFactory.Krypton.Toolkit.KryptonPanel
    Friend WithEvents dgvLotesSalida As ComponentFactory.Krypton.Toolkit.KryptonDataGridView
    Friend WithEvents colFacturaCodigo As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents colFacturaFecha As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents colCliente As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
End Class
