﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmSeleccionarAlbaranEntrada
    Inherits ComponentFactory.Krypton.Toolkit.KryptonForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSeleccionarAlbaranEntrada))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.hdrContenido = New ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup()
        Me.hdrTarifas = New ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup()
        Me.tblBusqueda = New System.Windows.Forms.TableLayoutPanel()
        Me.dgvRecibos = New ComponentFactory.Krypton.Toolkit.KryptonDataGridView()
        Me.lblBcodigo = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblSeleccionar = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.KryptonLabel4 = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.KryptonLabel8 = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.KryptonLabel2 = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblBEspecie = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.btnBBuscar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.lblBProveedor = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.pCuerpo = New ComponentFactory.Krypton.Toolkit.KryptonPanel()
        Me.pBotonera = New ComponentFactory.Krypton.Toolkit.KryptonPanel()
        Me.btnAceptar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.btnCerrar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtBProveedor = New Escritorio.cTextboxBuscador()
        Me.cboSeleccionar = New Escritorio.Quadralia.Controles.aComboBox()
        Me.txtBcodigoDe = New Escritorio.Quadralia.Controles.aTextBox()
        Me.txtBcodigoA = New Escritorio.Quadralia.Controles.aTextBox()
        Me.dtpBFechaDe = New Escritorio.Quadralia.Controles.aDateTimePicker()
        Me.dtpBFechaA = New Escritorio.Quadralia.Controles.aDateTimePicker()
        Me.txtBEspecie = New Escritorio.cTextboxBuscador()
        Me.colAlbEntrada = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colEspecie = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colNombreProveedor = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colPresentacion = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colZonaFao = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colFechaDesembarco = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        CType(Me.hdrContenido, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.hdrContenido.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.hdrContenido.Panel.SuspendLayout()
        Me.hdrContenido.SuspendLayout()
        CType(Me.hdrTarifas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.hdrTarifas.Panel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.hdrTarifas.Panel.SuspendLayout()
        Me.hdrTarifas.SuspendLayout()
        Me.tblBusqueda.SuspendLayout()
        CType(Me.dgvRecibos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pCuerpo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pCuerpo.SuspendLayout()
        CType(Me.pBotonera, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pBotonera.SuspendLayout()
        CType(Me.cboSeleccionar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'hdrContenido
        '
        resources.ApplyResources(Me.hdrContenido, "hdrContenido")
        Me.hdrContenido.HeaderVisibleSecondary = False
        Me.hdrContenido.Name = "hdrContenido"
        '
        'hdrContenido.Panel
        '
        Me.hdrContenido.Panel.Controls.Add(Me.hdrTarifas)
        Me.hdrContenido.ValuesPrimary.Heading = resources.GetString("hdrContenido.ValuesPrimary.Heading")
        Me.hdrContenido.ValuesPrimary.Image = CType(resources.GetObject("hdrContenido.ValuesPrimary.Image"), System.Drawing.Image)
        '
        'hdrTarifas
        '
        resources.ApplyResources(Me.hdrTarifas, "hdrTarifas")
        Me.hdrTarifas.HeaderStylePrimary = ComponentFactory.Krypton.Toolkit.HeaderStyle.Secondary
        Me.hdrTarifas.HeaderVisibleSecondary = False
        Me.hdrTarifas.Name = "hdrTarifas"
        '
        'hdrTarifas.Panel
        '
        Me.hdrTarifas.Panel.Controls.Add(Me.tblBusqueda)
        Me.ToolTip1.SetToolTip(Me.hdrTarifas, resources.GetString("hdrTarifas.ToolTip"))
        Me.hdrTarifas.ValuesPrimary.Heading = resources.GetString("hdrTarifas.ValuesPrimary.Heading")
        Me.hdrTarifas.ValuesPrimary.Image = Global.Escritorio.My.Resources.Resources.Buscar_16
        '
        'tblBusqueda
        '
        Me.tblBusqueda.BackColor = System.Drawing.Color.Transparent
        resources.ApplyResources(Me.tblBusqueda, "tblBusqueda")
        Me.tblBusqueda.Controls.Add(Me.txtBProveedor, 1, 2)
        Me.tblBusqueda.Controls.Add(Me.dgvRecibos, 0, 9)
        Me.tblBusqueda.Controls.Add(Me.cboSeleccionar, 1, 8)
        Me.tblBusqueda.Controls.Add(Me.lblBcodigo, 0, 0)
        Me.tblBusqueda.Controls.Add(Me.txtBcodigoDe, 1, 0)
        Me.tblBusqueda.Controls.Add(Me.lblSeleccionar, 0, 8)
        Me.tblBusqueda.Controls.Add(Me.KryptonLabel4, 2, 0)
        Me.tblBusqueda.Controls.Add(Me.txtBcodigoA, 3, 0)
        Me.tblBusqueda.Controls.Add(Me.KryptonLabel8, 0, 3)
        Me.tblBusqueda.Controls.Add(Me.dtpBFechaDe, 1, 3)
        Me.tblBusqueda.Controls.Add(Me.KryptonLabel2, 2, 3)
        Me.tblBusqueda.Controls.Add(Me.dtpBFechaA, 3, 3)
        Me.tblBusqueda.Controls.Add(Me.lblBEspecie, 0, 1)
        Me.tblBusqueda.Controls.Add(Me.txtBEspecie, 1, 1)
        Me.tblBusqueda.Controls.Add(Me.btnBBuscar, 4, 8)
        Me.tblBusqueda.Controls.Add(Me.lblBProveedor, 0, 2)
        Me.tblBusqueda.Name = "tblBusqueda"
        '
        'dgvRecibos
        '
        Me.dgvRecibos.AllowUserToAddRows = False
        Me.dgvRecibos.AllowUserToDeleteRows = False
        Me.dgvRecibos.AllowUserToResizeRows = False
        Me.dgvRecibos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colAlbEntrada, Me.colEspecie, Me.colNombreProveedor, Me.colPresentacion, Me.colZonaFao, Me.colFechaDesembarco})
        Me.tblBusqueda.SetColumnSpan(Me.dgvRecibos, 5)
        resources.ApplyResources(Me.dgvRecibos, "dgvRecibos")
        Me.dgvRecibos.MultiSelect = False
        Me.dgvRecibos.Name = "dgvRecibos"
        Me.dgvRecibos.RowHeadersVisible = False
        Me.dgvRecibos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        '
        'lblBcodigo
        '
        resources.ApplyResources(Me.lblBcodigo, "lblBcodigo")
        Me.lblBcodigo.Name = "lblBcodigo"
        Me.lblBcodigo.Values.Text = resources.GetString("lblBcodigo.Values.Text")
        '
        'lblSeleccionar
        '
        resources.ApplyResources(Me.lblSeleccionar, "lblSeleccionar")
        Me.lblSeleccionar.Name = "lblSeleccionar"
        Me.lblSeleccionar.Values.Text = resources.GetString("lblSeleccionar.Values.Text")
        '
        'KryptonLabel4
        '
        resources.ApplyResources(Me.KryptonLabel4, "KryptonLabel4")
        Me.KryptonLabel4.Name = "KryptonLabel4"
        Me.KryptonLabel4.Values.Text = resources.GetString("KryptonLabel4.Values.Text")
        '
        'KryptonLabel8
        '
        resources.ApplyResources(Me.KryptonLabel8, "KryptonLabel8")
        Me.KryptonLabel8.Name = "KryptonLabel8"
        Me.KryptonLabel8.Values.Text = resources.GetString("KryptonLabel8.Values.Text")
        '
        'KryptonLabel2
        '
        resources.ApplyResources(Me.KryptonLabel2, "KryptonLabel2")
        Me.KryptonLabel2.Name = "KryptonLabel2"
        Me.KryptonLabel2.Values.Text = resources.GetString("KryptonLabel2.Values.Text")
        '
        'lblBEspecie
        '
        resources.ApplyResources(Me.lblBEspecie, "lblBEspecie")
        Me.lblBEspecie.Name = "lblBEspecie"
        Me.lblBEspecie.Values.Text = resources.GetString("lblBEspecie.Values.Text")
        '
        'btnBBuscar
        '
        resources.ApplyResources(Me.btnBBuscar, "btnBBuscar")
        Me.btnBBuscar.Name = "btnBBuscar"
        Me.ToolTip1.SetToolTip(Me.btnBBuscar, resources.GetString("btnBBuscar.ToolTip"))
        Me.btnBBuscar.Values.Image = CType(resources.GetObject("btnBBuscar.Values.Image"), System.Drawing.Image)
        Me.btnBBuscar.Values.Text = resources.GetString("btnBBuscar.Values.Text")
        '
        'lblBProveedor
        '
        resources.ApplyResources(Me.lblBProveedor, "lblBProveedor")
        Me.lblBProveedor.Name = "lblBProveedor"
        Me.lblBProveedor.Values.Text = resources.GetString("lblBProveedor.Values.Text")
        '
        'pCuerpo
        '
        Me.pCuerpo.Controls.Add(Me.hdrContenido)
        Me.pCuerpo.Controls.Add(Me.pBotonera)
        resources.ApplyResources(Me.pCuerpo, "pCuerpo")
        Me.pCuerpo.Name = "pCuerpo"
        '
        'pBotonera
        '
        Me.pBotonera.Controls.Add(Me.btnAceptar)
        Me.pBotonera.Controls.Add(Me.btnCerrar)
        resources.ApplyResources(Me.pBotonera, "pBotonera")
        Me.pBotonera.Name = "pBotonera"
        '
        'btnAceptar
        '
        resources.ApplyResources(Me.btnAceptar, "btnAceptar")
        Me.btnAceptar.Name = "btnAceptar"
        Me.ToolTip1.SetToolTip(Me.btnAceptar, resources.GetString("btnAceptar.ToolTip"))
        Me.btnAceptar.Values.Text = resources.GetString("btnAceptar.Values.Text")
        '
        'btnCerrar
        '
        resources.ApplyResources(Me.btnCerrar, "btnCerrar")
        Me.btnCerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCerrar.Name = "btnCerrar"
        Me.ToolTip1.SetToolTip(Me.btnCerrar, resources.GetString("btnCerrar.ToolTip"))
        Me.btnCerrar.Values.Text = resources.GetString("btnCerrar.Values.Text")
        '
        'txtBProveedor
        '
        Me.tblBusqueda.SetColumnSpan(Me.txtBProveedor, 4)
        Me.txtBProveedor.DescripcionReadOnly = False
        Me.txtBProveedor.DescripcionVisible = True
        Me.txtBProveedor.DisplayMember = "nombreComercial"
        Me.txtBProveedor.DisplayText = ""
        resources.ApplyResources(Me.txtBProveedor, "txtBProveedor")
        Me.txtBProveedor.FormularioBusqueda = "Escritorio.frmSeleccionarProveedor"
        Me.txtBProveedor.Item = Nothing
        Me.txtBProveedor.Name = "txtBProveedor"
        Me.txtBProveedor.UseOnlyNumbers = False
        Me.txtBProveedor.UseUpperCase = True
        Me.txtBProveedor.Validar = True
        Me.txtBProveedor.ValueMember = "codigo"
        Me.txtBProveedor.ValueText = ""
        '
        'cboSeleccionar
        '
        resources.ApplyResources(Me.cboSeleccionar, "cboSeleccionar")
        Me.tblBusqueda.SetColumnSpan(Me.cboSeleccionar, 3)
        Me.cboSeleccionar.controlarBotonBorrar = False
        Me.cboSeleccionar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSeleccionar.DropDownWidth = 352
        Me.cboSeleccionar.mostrarSiempreBotonBorrar = False
        Me.cboSeleccionar.Name = "cboSeleccionar"
        Me.ToolTip1.SetToolTip(Me.cboSeleccionar, resources.GetString("cboSeleccionar.ToolTip"))
        '
        'txtBcodigoDe
        '
        Me.txtBcodigoDe.AlwaysActive = False
        Me.txtBcodigoDe.controlarBotonBorrar = True
        resources.ApplyResources(Me.txtBcodigoDe, "txtBcodigoDe")
        Me.txtBcodigoDe.Formato = ""
        Me.txtBcodigoDe.mostrarSiempreBotonBorrar = False
        Me.txtBcodigoDe.Name = "txtBcodigoDe"
        Me.txtBcodigoDe.seleccionarTodo = True
        '
        'txtBcodigoA
        '
        Me.txtBcodigoA.AlwaysActive = False
        Me.tblBusqueda.SetColumnSpan(Me.txtBcodigoA, 2)
        Me.txtBcodigoA.controlarBotonBorrar = True
        resources.ApplyResources(Me.txtBcodigoA, "txtBcodigoA")
        Me.txtBcodigoA.Formato = ""
        Me.txtBcodigoA.mostrarSiempreBotonBorrar = False
        Me.txtBcodigoA.Name = "txtBcodigoA"
        Me.txtBcodigoA.seleccionarTodo = True
        '
        'dtpBFechaDe
        '
        resources.ApplyResources(Me.dtpBFechaDe, "dtpBFechaDe")
        Me.dtpBFechaDe.controlarBotonBorrar = True
        Me.dtpBFechaDe.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpBFechaDe.mostrarSiempreBotonBorrar = False
        Me.dtpBFechaDe.Name = "dtpBFechaDe"
        Me.dtpBFechaDe.TipoLimpieza = Escritorio.Quadralia.Controles.aDateTimePicker.TiposLimpieza.ValueYValueNullable
        Me.dtpBFechaDe.ValueNullable = New Date(CType(0, Long))
        '
        'dtpBFechaA
        '
        resources.ApplyResources(Me.dtpBFechaA, "dtpBFechaA")
        Me.tblBusqueda.SetColumnSpan(Me.dtpBFechaA, 2)
        Me.dtpBFechaA.controlarBotonBorrar = True
        Me.dtpBFechaA.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpBFechaA.mostrarSiempreBotonBorrar = False
        Me.dtpBFechaA.Name = "dtpBFechaA"
        Me.dtpBFechaA.TipoLimpieza = Escritorio.Quadralia.Controles.aDateTimePicker.TiposLimpieza.ValueYValueNullable
        Me.dtpBFechaA.ValueNullable = New Date(CType(0, Long))
        '
        'txtBEspecie
        '
        Me.tblBusqueda.SetColumnSpan(Me.txtBEspecie, 4)
        Me.txtBEspecie.DescripcionReadOnly = False
        Me.txtBEspecie.DescripcionVisible = True
        Me.txtBEspecie.DisplayMember = "denomincacionComercial"
        Me.txtBEspecie.DisplayText = ""
        resources.ApplyResources(Me.txtBEspecie, "txtBEspecie")
        Me.txtBEspecie.FormularioBusqueda = "Escritorio.frmSeleccionarEspecies"
        Me.txtBEspecie.Item = Nothing
        Me.txtBEspecie.Name = "txtBEspecie"
        Me.txtBEspecie.UseOnlyNumbers = False
        Me.txtBEspecie.UseUpperCase = True
        Me.txtBEspecie.Validar = True
        Me.txtBEspecie.ValueMember = "alfa3"
        Me.txtBEspecie.ValueText = ""
        '
        'colAlbEntrada
        '
        Me.colAlbEntrada.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colAlbEntrada.DataPropertyName = "codigoAlbaranEntrada"
        resources.ApplyResources(Me.colAlbEntrada, "colAlbEntrada")
        Me.colAlbEntrada.Name = "colAlbEntrada"
        Me.colAlbEntrada.ReadOnly = True
        '
        'colEspecie
        '
        Me.colEspecie.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colEspecie.DataPropertyName = "nombreEspecie"
        resources.ApplyResources(Me.colEspecie, "colEspecie")
        Me.colEspecie.Name = "colEspecie"
        Me.colEspecie.ReadOnly = True
        '
        'colNombreProveedor
        '
        Me.colNombreProveedor.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colNombreProveedor.DataPropertyName = "nombreProveedor"
        resources.ApplyResources(Me.colNombreProveedor, "colNombreProveedor")
        Me.colNombreProveedor.Name = "colNombreProveedor"
        Me.colNombreProveedor.ReadOnly = True
        '
        'colPresentacion
        '
        Me.colPresentacion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colPresentacion.DataPropertyName = "nombrePresentacion"
        resources.ApplyResources(Me.colPresentacion, "colPresentacion")
        Me.colPresentacion.Name = "colPresentacion"
        Me.colPresentacion.ReadOnly = True
        '
        'colZonaFao
        '
        Me.colZonaFao.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colZonaFao.DataPropertyName = "nombreFAO"
        resources.ApplyResources(Me.colZonaFao, "colZonaFao")
        Me.colZonaFao.Name = "colZonaFao"
        Me.colZonaFao.ReadOnly = True
        '
        'colFechaDesembarco
        '
        Me.colFechaDesembarco.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colFechaDesembarco.DataPropertyName = "fechaDesembarco"
        DataGridViewCellStyle1.Format = "d"
        Me.colFechaDesembarco.DefaultCellStyle = DataGridViewCellStyle1
        resources.ApplyResources(Me.colFechaDesembarco, "colFechaDesembarco")
        Me.colFechaDesembarco.Name = "colFechaDesembarco"
        Me.colFechaDesembarco.ReadOnly = True
        '
        'frmSeleccionarAlbaranEntrada
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCerrar
        Me.Controls.Add(Me.pCuerpo)
        Me.Name = "frmSeleccionarAlbaranEntrada"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        CType(Me.hdrContenido.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrContenido.Panel.ResumeLayout(False)
        CType(Me.hdrContenido, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrContenido.ResumeLayout(False)
        CType(Me.hdrTarifas.Panel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrTarifas.Panel.ResumeLayout(False)
        CType(Me.hdrTarifas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.hdrTarifas.ResumeLayout(False)
        Me.tblBusqueda.ResumeLayout(False)
        Me.tblBusqueda.PerformLayout()
        CType(Me.dgvRecibos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pCuerpo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pCuerpo.ResumeLayout(False)
        CType(Me.pBotonera, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pBotonera.ResumeLayout(False)
        CType(Me.cboSeleccionar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pCuerpo As ComponentFactory.Krypton.Toolkit.KryptonPanel
    Friend WithEvents hdrContenido As ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup
    Friend WithEvents pBotonera As ComponentFactory.Krypton.Toolkit.KryptonPanel
    Friend WithEvents btnCerrar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents btnAceptar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents hdrTarifas As ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup
    Friend WithEvents dgvRecibos As ComponentFactory.Krypton.Toolkit.KryptonDataGridView
    Friend WithEvents cboSeleccionar As Quadralia.Controles.aComboBox
    Friend WithEvents lblSeleccionar As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents tblBusqueda As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblBcodigo As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtBcodigoDe As Quadralia.Controles.aTextBox
    Friend WithEvents KryptonLabel4 As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtBcodigoA As Quadralia.Controles.aTextBox
    Friend WithEvents KryptonLabel8 As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents dtpBFechaDe As Quadralia.Controles.aDateTimePicker
    Friend WithEvents KryptonLabel2 As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents dtpBFechaA As Quadralia.Controles.aDateTimePicker
    Friend WithEvents lblBEspecie As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtBEspecie As Escritorio.cTextboxBuscador
    Friend WithEvents btnBBuscar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents lblBProveedor As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtBProveedor As Escritorio.cTextboxBuscador
    Friend WithEvents colAlbEntrada As KryptonDataGridViewTextBoxColumn
    Friend WithEvents colEspecie As KryptonDataGridViewTextBoxColumn
    Friend WithEvents colNombreProveedor As KryptonDataGridViewTextBoxColumn
    Friend WithEvents colPresentacion As KryptonDataGridViewTextBoxColumn
    Friend WithEvents colZonaFao As KryptonDataGridViewTextBoxColumn
    Friend WithEvents colFechaDesembarco As KryptonDataGridViewTextBoxColumn
End Class
