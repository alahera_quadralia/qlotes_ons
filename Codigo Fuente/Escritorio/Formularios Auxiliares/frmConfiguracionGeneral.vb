﻿Imports Escritorio.Quadralia.BackUpBBDD
Imports System.Data.Objects
Imports System.Drawing.Printing

Public Class frmConfiguracionGeneral
    Inherits FormularioHijo

#Region " DECLARACIONES "
    Private _Estado As Quadralia.Enumerados.EstadoFormulario = Quadralia.Enumerados.EstadoFormulario.Espera
#End Region
#Region " DECLARACIONES NECESARIAS DE FORMULARIO HIJO "
    Public Overrides ReadOnly Property Tab As ComponentFactory.Krypton.Ribbon.KryptonRibbonTab
        Get
            Return frmPrincipal.tabConfiguracion
        End Get
    End Property
#End Region
#Region " LIMPIEZA "
    ''' <summary>
    ''' Limpia todos los controles con datos del formulario
    ''' </summary>
    Private Sub LimpiarDatosConfiguracion()
        chkBuscadoresDesplegados.Checked = False
        chkMostrarVentanasMaximizadas.Checked = False
        chkFiltrosDesplegados.Checked = False
        chkNavegabilidad.Checked = False
        txtServidorErrores.Clear()

        cboImpresora.SelectedIndex = -1
        cboFormato.Clear()
        cboOrientacion.Clear()

        ' Bascula
        txtDireccionIP.Clear()
        nudPuerto.Value = nudPuerto.Minimum
        nudIntervaloMuestreo.Value = nudIntervaloMuestreo.Minimum
        nudIntervaloEstable.Value = nudIntervaloEstable.Minimum
        nudPesoMinimo.Value = nudPesoMinimo.Minimum
        nudVariacionMinima.Value = nudVariacionMinima.Minimum

        ' Sincronizacion
        chkSincronizacionAutomatica.Checked = False
        nudSincronizacionAutomatica.Value = nudSincronizacionAutomatica.Minimum

        ' MySQL
        txtBBDDBaseDatos.Clear()
        txtBBDDServidor.Clear()
        txtBBDDClave.Clear()
        txtBBDDUsuario.Clear()
        nudBBDDPuerto.Value = nudBBDDPuerto.Minimum
        nudBBDDTiemout.Value = nudBBDDTiemout.Minimum

        ' FTP
        txtFTPClave.Clear()
        txtFTPServidor.Clear()
        txtFTPUsuario.Clear()
        txtFTPCarpeta.Clear()
        nudFTPPuerto.Value = nudFTPPuerto.Minimum
    End Sub
#End Region
#Region " RUTINA DE INICIO / FIN "
    ''' <summary>
    ''' Rutina de inicio del formulario
    ''' </summary>
    Private Sub Inicio(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CargarDatosMaestros()
        Cargar()
        tabGeneral.SelectedIndex = 0
    End Sub

    Private Sub CargarDatosMaestros()

        ' Carga las impresoras
        With cboImpresora
            .DataSource = Nothing
            .Items.Clear()

            For Each Impresora In Printing.PrinterSettings.InstalledPrinters
                .Items.Add(Impresora)
            Next

            .Clear()
        End With

        ' Cargamos los formatos de orientacion
        Dim Orientaciones As New Dictionary(Of Integer, String) From {{CrystalDecisions.Shared.PaperOrientation.DefaultPaperOrientation, "Por Defecto"}, {CrystalDecisions.Shared.PaperOrientation.Landscape, "Horizontal"}, {CrystalDecisions.Shared.PaperOrientation.Portrait, "Vertical"}}
        With cboOrientacion
            .DataSource = Nothing
            .Items.Clear()

            .DisplayMember = "Value"
            .ValueMember = "Key"

            .DataSource = New BindingSource(Orientaciones, Nothing)

            .Clear()
        End With
    End Sub

    ''' <summary>
    ''' Carga los formatos de la impresora seleccionada
    ''' </summary>
    Private Sub CargarFormatosImpresora(sender As Object, e As EventArgs) Handles cboImpresora.SelectedIndexChanged
        cboFormato.DataSource = Nothing
        cboFormato.Items.Clear()


        If cboImpresora.SelectedIndex = -1 Then Exit Sub

        ' Cargo los tipos de formatos de la impresora seleccionada
        Try
            Dim PrinterObj As New System.Drawing.Printing.PrinterSettings()

            Dim Lista As New List(Of PaperSize)

            PrinterObj.PrinterName = cboImpresora.SelectedItem

            For Each it As PaperSize In PrinterObj.PaperSizes
                Lista.Add(it)
            Next

            With cboFormato
                .DisplayMember = "PaperName"
                .ValueMember = "RawKind"
                .DataSource = Lista

                .SelectedItem = Nothing
                .SelectedIndex = -1
            End With

        Catch ex As Exception
            cboFormato.DataSource = Nothing
            cboFormato.Items.Clear()
        End Try
    End Sub
#End Region
#Region " CARGAR / GUARDAR / CANCELAR "
    ''' <summary>
    ''' Carga la configuración de BBDD en el formulario
    ''' </summary>
    Private Sub Cargar()
        With cConfiguracionAplicacion.Instancia
            chkBuscadoresDesplegados.Checked = .BuscadoresAbiertos
            chkMostrarVentanasMaximizadas.Checked = .VentanasMaximizadas
            chkFiltrosDesplegados.Checked = .FiltrosAbiertos
            chkNavegabilidad.Checked = .PermitirNavegabilidad
            txtServidorErrores.Text = .ServidorErrores
            If Not String.IsNullOrEmpty(.Impresora) Then cboImpresora.SelectedItem = .Impresora
            cboFormato.SelectedValue = .Formato
            cboOrientacion.SelectedValue = .Orientacion
        End With

        With cConfiguracionPrograma.Instancia
            txtDireccionIP.Text = .direccionImpresora
            nudPuerto.Value = .puertoImpresora
            nudIntervaloMuestreo.Value = .IntervaloMuestreo
            nudIntervaloEstable.Value = .IntervaloEstable
            nudPesoMinimo.Value = .PesoMinimo
            nudVariacionMinima.Value = .variacionMinima

            ' Sincronizacion
            chkSincronizacionAutomatica.Checked = .IntervaloSincronizacion.HasValue
            If .IntervaloSincronizacion.HasValue Then nudSincronizacionAutomatica.Value = .IntervaloSincronizacion.Value

            ' MySQL
            txtBBDDBaseDatos.Text = .TrazamareBBDDBaseDatos
            txtBBDDServidor.Text = .TrazamareBBDDServidor
            txtBBDDClave.Text = .TrazamareBBDDClave
            txtBBDDUsuario.Text = .TrazamareBBDDUsuario
            nudBBDDPuerto.Value = .TrazamareBBDDPuerto
            nudBBDDTiemout.Value = .TrazamareBBDDTimeOut

            ' FTP
            txtFTPClave.Text = .TrazamareFTPClave
            txtFTPServidor.Text = .TrazamareFTPServidor
            txtFTPUsuario.Text = .TrazamareFTPUsuario
            nudFTPPuerto.Value = .TrazamareFTPPuerto
            txtFTPCarpeta.Text = .TrazamareFTPRuta
        End With
    End Sub

    ''' <summary>
    ''' Guarda los cambios del formulario en BBDD
    ''' </summary>
    ''' <param name="MostrarMensaje"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overrides Function Guardar(Optional ByVal MostrarMensaje As Boolean = True) As Boolean
        With cConfiguracionAplicacion.Instancia
            .BuscadoresAbiertos = chkBuscadoresDesplegados.Checked
            .VentanasMaximizadas = chkMostrarVentanasMaximizadas.Checked
            .FiltrosAbiertos = chkFiltrosDesplegados.Checked
            .PermitirNavegabilidad = chkNavegabilidad.Checked
            .ServidorErrores = txtServidorErrores.Text
            If cboImpresora.SelectedIndex > -1 Then .Impresora = cboImpresora.SelectedItem Else .Impresora = String.Empty
            If cboFormato.SelectedIndex > -1 Then .Formato = cboFormato.SelectedValue Else .Formato = -1
            If cboOrientacion.SelectedIndex > -1 Then .Orientacion = cboOrientacion.SelectedValue Else .Orientacion = -1
        End With

        With cConfiguracionPrograma.Instancia
            .direccionImpresora = txtDireccionIP.Text
            .puertoImpresora = nudPuerto.Value
            .IntervaloMuestreo = nudIntervaloMuestreo.Value
            .IntervaloEstable = nudIntervaloEstable.Value
            .PesoMinimo = nudPesoMinimo.Value
            .variacionMinima = nudVariacionMinima.Value

            ' Sincronizacion
            If chkSincronizacionAutomatica.Checked Then .IntervaloSincronizacion = nudSincronizacionAutomatica.Value Else .IntervaloSincronizacion = Nothing

            ' MySQL
            .TrazamareBBDDBaseDatos = txtBBDDBaseDatos.Text
            .TrazamareBBDDServidor = txtBBDDServidor.Text
            .TrazamareBBDDClave = txtBBDDClave.Text
            .TrazamareBBDDUsuario = txtBBDDUsuario.Text
            .TrazamareBBDDPuerto = nudBBDDPuerto.Value
            .TrazamareBBDDTimeOut = nudBBDDTiemout.Value

            ' FTP
            .TrazamareFTPClave = txtFTPClave.Text
            .TrazamareFTPServidor = txtFTPServidor.Text
            .TrazamareFTPUsuario = txtFTPUsuario.Text
            .TrazamareFTPPuerto = nudFTPPuerto.Value
            .TrazamareFTPRuta = txtFTPCarpeta.Text
        End With

        Try
            If Not cConfiguracionAplicacion.Instancia.Guardar() Then Return False
            If Not cConfiguracionPrograma.Guardar Then Return False
            If MostrarMensaje Then Quadralia.Aplicacion.MostrarMensajeNtfIco(My.Resources.TituloDatosGuardatos, My.Resources.DatosGuardados, ToolTipIcon.Info)

            Me.Close()
            Return True
        Catch ex As Exception
            Quadralia.Aplicacion.InformarError(ex, False, True)
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Cancela los cambios realizados en el formulario
    ''' </summary>
    Public Overrides Sub Cancelar()
        Me.Close()
    End Sub
#End Region
#Region " VALIDACIONES "
    ''' <summary>
    ''' Valida que la dirección IP introducida sea correcta
    ''' </summary>
    Private Sub ValidarDireccionIP(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtDireccionIP.Validating
        If Not TypeOf (sender) Is Quadralia.Controles.aTextBox Then Exit Sub

        Dim Aux As System.Net.IPAddress = Nothing
        If Not String.IsNullOrEmpty(DirectCast(sender, KryptonTextBox).Text) AndAlso Not System.Net.IPAddress.TryParse(DirectCast(sender, KryptonTextBox).Text, Aux) Then
            epErrores.SetError(sender, "La dirección IP Introducida no es válida")
        Else
            epErrores.SetError(sender, "")
        End If
    End Sub
#End Region
End Class