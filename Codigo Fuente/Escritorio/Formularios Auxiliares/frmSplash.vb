﻿Imports System.Resources

Public Class frmSplash
	Inherits KryptonForm

	Public Event VentanaMostrada()
#Region " CONFIGURACION "
	Private Const timerInterval_ms As Integer = 20			' Intervalo entre llamadas del timer
	Private FadeIn_Increment As Double = 0.02				' Incremento en el Fade
	Private FadeOut_Decrement As Double = -0.08				' Decremento en el fade
#End Region
#Region " DECLARACIONES "
	Private splashTimer As System.Threading.Timer			' Timer Timer para crear el efecto de Fade
	Private _HoraInicio As DateTime = Now					' Variable utilizada para calcular el tiempo que lleva mostrado el formulario
	Delegate Sub CambiarOpacidad(ByVal Valor As Double)		' Delegado para cambiar la opacity del formulario
#End Region
#Region " PROPIEDADES "
	''' <summary>
	''' Tiempo en segundos durante los cuales se ha mostrado el formulario
	''' </summary>
	Public ReadOnly Property SegundosMostrado() As Integer
		Get
			Return Math.Abs(DateDiff(DateInterval.Second, _HoraInicio, Now))
		End Get
	End Property

	''' <summary>
	''' Escribe un mensaje en la label de estado
	''' </summary>
	Public WriteOnly Property EscribirEstado() As String
		Set(ByVal value As String)
			lblEstado.Text = value
		End Set
	End Property
#End Region
#Region " RUTINA DE INICIO Y FIN"
	''' <summary>
	''' Rutina de inicio. Se inicializan los valores y se escribe la información de la aplicación en el formulario
	''' </summary>
	Private Sub Inicio(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

		' Vamos crear un efecto de fade, con lo cual tengo que empezar en 0
		Me.Opacity = 0

		' Escribo la información de la aplicación en la label
		lblEstado.Text = String.Empty
        lblTituloAplicacion.Text = My.Application.Info.ProductName

        lblVersion.Text = String.Format("Versión {0}", My.Application.Info.Version.ToString)

		' Creo un timer que se cambie la opacidad del formulario para crear el efecto del fade-in
		Dim splashDelegate As New System.Threading.TimerCallback(AddressOf Me.FadeIn)
		Me.splashTimer = New System.Threading.Timer(splashDelegate, Nothing, timerInterval_ms, timerInterval_ms)
	End Sub

	''' <summary>
	''' Empieza con el efecto de desvanecimiento del formulario
	''' </summary>
	''' <remarks></remarks>
	Public Sub CerrarFormulario()

		' Si todavía no acabó de pintar el formulario, le digo que se detenga
		If splashTimer IsNot Nothing Then splashTimer.Dispose()

		' Creo un timer que se cambie la opacidad del formulario para crear el efecto del fade-out
		Dim splashDelegate As New System.Threading.TimerCallback(AddressOf Me.FadeOut)
		Me.splashTimer = New System.Threading.Timer(splashDelegate, Nothing, timerInterval_ms, timerInterval_ms)
	End Sub
#End Region
#Region " EFECTO FADE "
	''' <summary>
	''' Efecto de aparición del formulario
	''' </summary>
	Public Sub FadeIn()
		SyncLock Me
			If Me.Opacity < 1 Then
				' Cambiamos la opacidad del formulario
				CambiarFade(FadeIn_Increment)
			Else
				' Detenemos el timer para evitar llamadas innecesarias
				splashTimer.Dispose()
				RaiseEvent VentanaMostrada()
			End If
		End SyncLock
	End Sub

	''' <summary>
	''' Efecto de desaparición del formulario
	''' </summary>
	Public Sub FadeOut()
		SyncLock Me
			If Me.Opacity > 0 Then
				' Cambiamos la opacidad del formulario
				CambiarFade(FadeOut_Decrement)
			Else
				' Detenemos el timer para evitar llamadas innecesarias
                splashTimer.Dispose()
			End If
		End SyncLock
	End Sub

	''' <summary>
	''' Llamada segura al formulario. Trabajo con hilos
	''' </summary>
	''' <param name="Valor">Incremento que se va a aplicar a la opacidad del formulario</param>
	Public Sub CambiarFade(ByVal Valor As Double)
		If Me.InvokeRequired Then
			Dim Delegado As New CambiarOpacidad(AddressOf CambiarFade)
			Me.Invoke(Delegado, Valor)
		Else
			Me.Opacity += Valor
		End If
	End Sub
#End Region
End Class