﻿Imports System.ComponentModel

''' <summary>
''' Objeto de escucha de la báscula de pesado
''' </summary>
Public Class ListenerBascula

#Region " CONSTANTES "
    Private Const INTERVALO_MINIMO As Integer = 50
#End Region
#Region " DECLARACIONES "
    Private WithEvents tmrEscucha As Timers.Timer = Nothing ' Timer para la consulta de la báscula
    Private _Comunicacion As New ComunicacionRed()

    ' Banderas
    Private _EjecutandoPeticion As Boolean = False
    Private _BasculaDisponible As Boolean = False
    Private _UltimaPesada As New RespuestaBascula
    Private _UltimaPesadaEstable As New RespuestaBascula
#End Region
#Region " EVENTOS "
    Public Event CambioConexion(sender As Object, e As EventArgs)

    Public Event LecturaPeso(sender As Object, Lectura As RespuestaBascula)
    Public Event LecturaPesoEstable(sender As Object, Lectura As RespuestaBascula)
    Public Event LecturaPesoZero(sender As Object, Lectura As RespuestaBascula)
#End Region
#Region " PROPIEDADES "
    ''' <summary>
    ''' Milisegundos tras los cuales se hará una nueva petición de lectura de la báscula
    ''' </summary>
    Public Property IntervaloMuestreo As Integer = 500

    ''' <summary>
    ''' Peso mínimo necesario para que se produzca un evento de lectura
    ''' </summary>
    Public Property PesoMinimo As Double = 0

    ''' <summary>
    ''' Milisegundos que debe permanecer una pesada como estable antes de lanzar el evento
    ''' </summary>
    Public Property IntervaloEstable As Double = 0

    Public Property VariacionPesoMinima As Double = 0

    Public Property DireccionIP As String = String.Empty

    Public Property Puerto As Integer = 23

    ''' <summary>
    ''' Indica si el listener está en ejecución o no
    ''' </summary>
    Public ReadOnly Property EstaEjecutando As Boolean
        Get
            If tmrEscucha Is Nothing Then Return False
            Return tmrEscucha.Enabled
        End Get
    End Property

    Public ReadOnly Property Conectado As Boolean
        Get
            Return _BasculaDisponible
        End Get
    End Property
#End Region
#Region " CONTROL DEL MOTOR "
    ''' <summary>
    ''' Comienza la escucha de la báscula
    ''' </summary>
    Public Sub ComenzarEscucha()
        ' Validaciones
        If tmrEscucha IsNot Nothing Then Exit Sub ' Ya está corriendo

        ' Como mínimo se hacen lecturas cada 50 milisengundos
        tmrEscucha = New Timers.Timer(Math.Max(INTERVALO_MINIMO, IntervaloMuestreo))
        tmrEscucha.AutoReset = True

        ' Inicializo variables
        _EjecutandoPeticion = False
        _BasculaDisponible = False

        tmrEscucha.Start()
    End Sub

    Public Sub LanzarEvento()
        RaiseEvent LecturaPeso(Me, New RespuestaBascula With {.Linea = " A 200.12"})
    End Sub

    ''' <summary>
    ''' Detiene la escucha de la báscula
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub DetenerEscucha()
        ' Destruyo el objeto para denotar que no está corriendo el motor
        tmrEscucha = Nothing
    End Sub

    ''' <summary>
    ''' Se realiza una nueva consulta a la báscula para obtner su valor
    ''' </summary>
    Private Sub CicloTimer(sender As Object, e As Timers.ElapsedEventArgs) Handles tmrEscucha.Elapsed
        If _EjecutandoPeticion Then Exit Sub

        _EjecutandoPeticion = True
        Try
            Dim Disponible As Boolean = _Comunicacion.HostAccesible(DireccionIP)

            ' Gestiono los eventos
            If Disponible <> _BasculaDisponible Then
                _BasculaDisponible = Disponible
                RaiseEvent CambioConexion(Me, New EventArgs())
            End If

            ' Validaciones
            If Not Disponible Then Exit Sub

            ' Envio el comando
            Dim Respuesta As String = _Comunicacion.EnviarComando(DireccionIP, Puerto, "$", 500)
            If String.IsNullOrEmpty(Respuesta) Then Exit Sub
            Dim RespuestaBascula As New RespuestaBascula With {.Linea = Respuesta}

            ' ¿Cambio el estado y/o peso? NO: se trata de ultima pesada
            If RespuestaBascula.Peso = _UltimaPesada.Peso AndAlso RespuestaBascula.Estado = _UltimaPesada.Estado Then
                RespuestaBascula = _UltimaPesada
            Else
                RaiseEvent LecturaPeso(Nothing, RespuestaBascula)
            End If

            ' Miro el estado para saber el evento que tengo que lanzar
            Select Case RespuestaBascula.Estado
                Case RespuestaBascula.EstadosPesada.PesoEstable, RespuestaBascula.EstadosPesada.PesoEstableConTara

                    If RespuestaBascula.Notificado Then Exit Select ' ¿Ya se notificó?
                    If (DateTime.Now - RespuestaBascula.TimeStamp).TotalMilliseconds < Me.IntervaloEstable Then Exit Select ' ¿Tiempo mínimo necesario para conderarse como pesada estable?
                    If RespuestaBascula.Peso < PesoMinimo Then Exit Select ' ¿Llegó al peso mínimo?
                    If _UltimaPesadaEstable IsNot Nothing AndAlso (Math.Abs(RespuestaBascula.Peso - _UltimaPesadaEstable.Peso) < Math.Abs(VariacionPesoMinima)) Then Exit Select '¿Varió el peso lo necesario?

                    _UltimaPesadaEstable = RespuestaBascula
                    RespuestaBascula.Notificado = True
                    RaiseEvent LecturaPesoEstable(Me, RespuestaBascula)
                Case RespuestaBascula.EstadosPesada.Zero
                    RaiseEvent LecturaPesoZero(Me, RespuestaBascula)
                Case Else
            End Select

            _UltimaPesada = RespuestaBascula
        Catch ex As Exception
        Finally
            _EjecutandoPeticion = False
        End Try
    End Sub

    Public Shared Sub LogMessageToFile(ByVal msg As String)
        Dim sw As System.IO.StreamWriter = System.IO.File.AppendText("C:\qLotesLog.txt")
        Try
            Dim logLine As String = System.String.Format("{0:G}: {1}.", System.DateTime.Now, msg)
            sw.WriteLine(logLine)
        Finally
            sw.Close()
        End Try
    End Sub
#End Region
End Class

Public Class RespuestaBascula
#Region " ENUMERADOS "
    Public Enum EstadosPesada
        PesoEstable ' Letra: A
        PesoEstableConTara ' Letra: B
        Midiendo ' Letra: !
        MidiendoConTara ' Letra: "
        Zero  ' Letra I
        Indefinido
    End Enum
#End Region
#Region " PROPIEDADES "
    Friend Property Linea As String
    Friend Property TimeStamp As DateTime = DateTime.Now
    Friend Property Notificado As Boolean = False

    Friend ReadOnly Property Identificador As Long
        Get
            Return TimeStamp.Ticks
        End Get
    End Property

    Public ReadOnly Property Peso As Double
        Get
            If String.IsNullOrWhiteSpace(Linea) Then Return 0

            Try
                ' Desecho el caracter de control para quedarme con el peso
                Dim CadenaPeso As String = Linea.Substring(2)
                CadenaPeso = CadenaPeso.Replace(".", ",")
                Return Double.Parse(CadenaPeso)
            Catch ex As Exception
                Return 0
            End Try
        End Get
    End Property

    Public ReadOnly Property Estado As EstadosPesada
        Get
            If String.IsNullOrWhiteSpace(Linea) Then Return EstadosPesada.Indefinido

            Try
                ' Me quedo sólo con el caracter de control
                Dim Control As Char = Linea(1).ToString.ToUpper

                Select Case Control
                    Case "A"
                        Return EstadosPesada.PesoEstable
                    Case "B"
                        Return EstadosPesada.PesoEstableConTara
                    Case "I"
                        Return EstadosPesada.Zero
                    Case "!"
                        Return EstadosPesada.Midiendo
                    Case """"
                        Return EstadosPesada.MidiendoConTara
                    Case Else
                        Return EstadosPesada.Indefinido
                End Select
            Catch ex As Exception
                Return EstadosPesada.Indefinido
            End Try
        End Get
    End Property

    Public ReadOnly Property LineaLeida As String
        Get
            Return Linea
        End Get
    End Property
#End Region
End Class

