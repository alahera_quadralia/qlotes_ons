﻿Imports System.Net
Imports System.Net.NetworkInformation
Imports System.Net.Sockets
Imports System.Text

Friend Class ComunicacionRed
#Region " DECLARACIONES "
    Private _stream As NetworkStream
#End Region
    ''' <summary>
    ''' Intenta establecer comunicación con el host deseado
    ''' </summary>
    Public Function HostAccesible(ByVal DireccionIP As String) As Boolean
        If Not My.Computer.Network.IsAvailable Then Return False

        ' Intento realizar un ping al destino solicitado
        For i As Integer = 1 To 4
            If My.Computer.Network.Ping(DireccionIP) Then Return True
        Next

        ' No se pudo establecer el ping
        Return False
    End Function

    ''' <summary>
    ''' Envía un comando a la dirección especificada y espera su respuesta
    ''' </summary>
    ''' <param name="DireccionIP">Dirección (o nombre de la máquina) a la que deseamos enviar el comando</param>
    ''' <param name="Comando">Comando a enviar</param>
    ''' <param name="Timeout">Tiempo de espera máximo para obtener la respuesta de la máquina remota</param>
    Public Function EnviarComando(ByVal DireccionIP As String, puerto As Integer, Comando As String, Timeout As Integer) As String
        ' Validaciones
        If Not HostAccesible(DireccionIP) Then Return String.Empty

        Dim Cliente As TcpClient = Nothing
        Dim Stream As NetworkStream = Nothing

        Try
            Cliente = New TcpClient(DireccionIP, puerto)
            Stream = Cliente.GetStream()
            Stream.ReadTimeout = Timeout

            ' Envío el comando
            Dim buffer As Byte() = Encoding.ASCII.GetBytes(Comando)
            Stream.Write(buffer, 0, buffer.Length)
            Stream.Flush()

            Dim inStream(Cliente.ReceiveBufferSize) As Byte

            Stream.Read(inStream, 0, CInt(Cliente.ReceiveBufferSize))

            ' Devuelvo el mensaje devuelto por el cliente
            Return System.Text.Encoding.ASCII.GetString(inStream)
        Catch ex As Exception
            ''ListenerBascula.LogMessageToFile(ex.Message)
            Return String.Empty
        Finally
            If Stream IsNot Nothing Then Stream.Close()
            If Cliente IsNot Nothing Then Cliente.Close()
        End Try

    End Function
End Class
