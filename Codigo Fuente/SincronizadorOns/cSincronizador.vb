﻿Imports MySql.Data.MySqlClient
Imports System.Net.FtpClient
Imports System.IO

Public Class Sincronizador
#Region " ESTRUCTURAS "
    'Public Structure ConexionBBDD
    '    Public Servidor As String
    '    Public Usuario As String
    '    Public Clave As String
    '    Public BaseDatos As String
    '    Public Puerto As Integer
    '    Public Timeout As Integer
    'End Structure

    'Public Structure ConexionFTP
    '    Public Servidor As String
    '    Public Usuario As String
    '    Public Clave As String
    '    Public RutaCarpeta As String
    '    Public Puerto As Integer
    'End Structure

    Public Structure ConexionTRazamare
        Public Servidor As String
        Public SecurityToken As String
    End Structure
#End Region
#Region " DECLARACIONES "
    Private Shared _Instancia As Sincronizador = Nothing
    Private _Ejecutando As Boolean = False
#End Region
#Region " PROPIEDADES "
    ''' <summary>
    ''' Especifica las creedenciales de conexión con la base de datos remota
    ''' </summary>
    'Public Property DatosConexionBBDD As ConexionBBDD

    ''' <summary>
    ''' Especifica las creedenciales de conexión con el servidor FTP Remoto
    ''' </summary>
    'Public Property DatosConexionFTP As ConexionFTP

    ''' <summary>
    ''' Indica si se está ejecutando el proceso de sincronización
    ''' </summary>
    Public ReadOnly Property Ejecutando As Boolean
        Get
            Return _Ejecutando
        End Get
    End Property

    Private ReadOnly Property CadenaConexion As String
        Get
            Dim puerto As Integer = DatosConexionBBDD.Puerto
            If puerto <= 0 Then puerto = 3306

            Return String.Format("Server={0};Database={1};Uid={2};Pwd={3};Port={4}", DatosConexionBBDD.Servidor, DatosConexionBBDD.BaseDatos, DatosConexionBBDD.Usuario, DatosConexionBBDD.Clave, DatosConexionBBDD.Puerto)
        End Get
    End Property
#End Region
#Region " EVENTOS "
    Public Event Progreso(ProgresoGeneral As Integer, ProgresoParticular As Integer, ElementoProcesado As String)
    Public Event SincronizacionIniciada()
    Public Event SincronizacionFinalizada()
#End Region
#Region " SINGLETON "
    ''' <summary>
    ''' Acceso único al sistema de sincronización
    ''' </summary>
    Public Shared ReadOnly Property Instancia As Sincronizador
        Get
            If _Instancia Is Nothing Then _Instancia = New Sincronizador
            Return _Instancia
        End Get
    End Property
#End Region
#Region " METODOS PUBLICOS "
    ''' <summary>
    ''' Sincroniza todos los elementos con la web de trazamare
    ''' </summary>
    Public Sub Sincronizar(ByVal Etiquetas As List(Of DAL.Etiqueta), ByVal Embarcaciones As List(Of DAL.Barco), ByVal Especies As List(Of DAL.Especie),
                           ByVal Artes As List(Of DAL.MetodoProduccion), ByVal Empaquetados As List(Of DAL.Empaquetado),
                           Optional ByVal pSincronizarEtiquetasPaquetes As Boolean = False)
        _Ejecutando = True
        RaiseEvent SincronizacionIniciada()
        Try
            SincronizarArtes(Artes, 20)
            SincronizarEmbarcaciones(Embarcaciones, 40)
            SincronizarEspecies(Especies, 60)
            SincronizarEtiquetas(Etiquetas, 100, pSincronizarEtiquetasPaquetes)
            SincronizarEmpaquetados(Empaquetados, 80)
        Catch ex As Exception
            EscribirEnLog(ex.Message + "-" + ex.InnerException.ToString())
        End Try

        ''ActualizarEmpaquetadosASinSincronizar()

        _Ejecutando = False

        RaiseEvent SincronizacionFinalizada()
    End Sub

    Public Sub SincronizarEtiquetas(ByVal Etiquetas As List(Of DAL.Etiqueta), Optional ByVal pSincronizarEtiquetasPaquetes As Boolean = False)
        _Ejecutando = True
        RaiseEvent SincronizacionIniciada()
        Try
            SincronizarEtiquetas(Etiquetas, 100, pSincronizarEtiquetasPaquetes)
        Catch ex As Exception
            Throw ex
        End Try

        _Ejecutando = False

        RaiseEvent SincronizacionFinalizada()
    End Sub

    Public Sub SincronizarEmbarcaciones(ByVal Embarcaciones As List(Of DAL.Barco))
        _Ejecutando = True
        RaiseEvent SincronizacionIniciada()
        SincronizarEmbarcaciones(Embarcaciones, 100)
        _Ejecutando = False

        RaiseEvent SincronizacionFinalizada()
    End Sub

    Public Sub SincronizarEspecies(ByVal Especies As List(Of DAL.Especie))
        _Ejecutando = True
        RaiseEvent SincronizacionIniciada()
        SincronizarEspecies(Especies, 100)
        _Ejecutando = False

        RaiseEvent SincronizacionFinalizada()
    End Sub

    Public Sub SincronizarArtes(ByVal Artes As List(Of DAL.MetodoProduccion))
        _Ejecutando = True
        RaiseEvent SincronizacionIniciada()
        SincronizarArtes(Artes, 100)
        _Ejecutando = False

        RaiseEvent SincronizacionFinalizada()
    End Sub

    Public Sub SincronizarEmpaquetados(ByVal Empaquetados As List(Of DAL.Empaquetado))
        _Ejecutando = True
        RaiseEvent SincronizacionIniciada()
        SincronizarEmpaquetados(Empaquetados, 100)
        _Ejecutando = False

        RaiseEvent SincronizacionFinalizada()
    End Sub
#End Region
#Region " METODOS PRIVADOS "
    ''' <summary>
    ''' Lanza el evento previas validaciones de rangos
    ''' </summary>
    Private Sub LanzarEvento(ProgresoGeneral As Double, ProgresoParticular As Double, ElementoProcesado As String)
        Dim General As Integer = Math.Truncate(ProgresoGeneral)
        Dim Particular As Integer = Math.Truncate(ProgresoParticular)

        If General < 0 Then General = 0
        If General > 100 Then General = 100

        If Particular < 0 Then Particular = 0
        If Particular > 100 Then Particular = 100

        RaiseEvent Progreso(General, Particular, ElementoProcesado)
    End Sub

    ''' <summary>
    ''' Sincroniza las etiquetas con el servidor de trazamare
    ''' </summary>
    Private Sub SincronizarEtiquetas(ByVal Etiquetas As List(Of DAL.Etiqueta), IndiceGeneral As Integer, Optional ByVal pSincronizarEtiquetasPaquetes As Boolean = False)
        ' Validaciones       
        If Etiquetas Is Nothing OrElse Etiquetas.Count = 0 Then Exit Sub

        Dim Timeout As Integer = DatosConexionBBDD.Timeout
        If Timeout <= 0 Then Timeout = 30
        Dim Sincronizador As New cConnectionManager(Me.CadenaConexion, Timeout)
        Dim Conexion As New MySqlConnection(Me.CadenaConexion)

        Try
            ' Abro la conexión
            If Not Sincronizador.AbrirConexion(Conexion) Then Exit Sub
            ' Ejecuto las consultas
            For i As Integer = 0 To Etiquetas.Count - 1
                Dim etq As DAL.Etiqueta = Etiquetas(i)
                Dim Comando As New MySqlCommand("REPLACE INTO `etiqueta` (`Id`, `IdBarco`, `IDEspecie`, `IdEmpaquetado`, `Validada`, `dataValidacion`, `Peso`, `Lote`, `Formato`) VALUES (@Id, @IdBarco, @IDEspecie, @IdEmpaquetado, @Validada, @dataValidacion, @Peso, @Lote, @Formato);")
                Comando.Parameters.AddWithValue("Id", etq.id)
                Comando.Parameters.AddWithValue("IdBarco", etq.IdBarco)
                Comando.Parameters.AddWithValue("IDEspecie", etq.IdEspecie)
                Comando.Parameters.AddWithValue("IdEmpaquetado", etq.IdentificadorEmpaquetado)
                Comando.Parameters.AddWithValue("Validada", Not etq.fechaBaja.HasValue)
                Comando.Parameters.AddWithValue("dataValidacion", etq.fecha)
                Comando.Parameters.AddWithValue("Peso", etq.cantidad)
                Comando.Parameters.AddWithValue("Lote", "*%" + etq.LoteEntradaLinea.Numero_Lote + "%*")
                Comando.Parameters.AddWithValue("Formato", etq.DisenhoPred)

                Comando.Connection = Conexion
                Comando.CommandTimeout = Timeout
                Comando.Prepare()
                Comando.ExecuteNonQuery()

                etq.Sincronizando = True
                ''si estamos sincronizando etiquetas de paquetes no tocamos sincronizadoWeb
                If (Not pSincronizarEtiquetasPaquetes) Then
                    etq.sincronizadoWeb = True
                End If
                LanzarEvento(IndiceGeneral, ((i + 1) * 100) / Etiquetas.Count, String.Format("[{1} de {2}] Sincronizando Etiqueta {0}", etq.Codigo, i + 1, Etiquetas.Count))
            Next
        Catch ex As Exception
            EscribirEnLog(ex.Message + "-" + ex.InnerException.ToString())
            Throw ex
        Finally
            Sincronizador.CerrarConexion(Conexion)
        End Try
    End Sub

    ''' <summary>
    ''' Sincroniza las etiquetas con el servidor de trazamare
    ''' </summary>
    Private Sub SincronizarEmbarcaciones(ByVal Barcos As List(Of DAL.Barco), IndiceGeneral As Integer)
        ' Validaciones
        If Barcos Is Nothing OrElse Barcos.Count = 0 Then Exit Sub

        Dim Timeout As Integer = DatosConexionBBDD.Timeout
        If Timeout <= 0 Then Timeout = 30

        Dim Sincronizador As New cConnectionManager(Me.CadenaConexion, Timeout)
        Dim Conexion As New MySqlConnection(Me.CadenaConexion)

        Try
            ' Abro la conexión
            If Not Sincronizador.AbrirConexion(Conexion) Then Exit Sub

            ' Ejecuto las consultas
            For i As Integer = 0 To Barcos.Count - 1
                Dim bar As DAL.Barco = Barcos(i)
                Dim Comando As New MySqlCommand("REPLACE INTO `barco` (`Id`, `Nombre`, `idAlternancia`) VALUES (@Id, @Nombre, @idAlternancia);", Conexion)

                Comando.Parameters.AddWithValue("Id", bar.id)
                Comando.Parameters.AddWithValue("Nombre", bar.descripcion)
                Comando.Parameters.AddWithValue("idAlternancia", 1)

                Comando.CommandTimeout = Timeout
                Comando.Prepare()

                Comando.ExecuteNonQuery()

                ' Miro si tengo que sincronizar imagen
                If Not (SincronizarImagen(bar, "Barcos")) Then Continue For
                bar.Sincronizando = True
                bar.sincronizadoWeb = True
                LanzarEvento(IndiceGeneral, (i + 1 * 100) / Barcos.Count, String.Format("[{1} de {2}] Sincronizando Embaración {0}", bar.descripcion, i + 1, Barcos.Count))
            Next

        Catch ex As Exception
            EscribirEnLog(ex.Message + "-" + ex.InnerException.ToString())
            Throw ex
        Finally
            Sincronizador.CerrarConexion(Conexion)
        End Try
    End Sub

    ''' <summary>
    ''' Sincroniza las especies con el servidor de trazamare
    ''' </summary>
    Private Sub SincronizarEspecies(ByVal Especies As List(Of DAL.Especie), IndiceGeneral As Integer)
        ' Validaciones
        If Especies Is Nothing OrElse Especies.Count = 0 Then Exit Sub

        Dim Timeout As Integer = DatosConexionBBDD.Timeout
        If Timeout <= 0 Then Timeout = 30

        Dim Sincronizador As New cConnectionManager(Me.CadenaConexion, Timeout)
        Dim Conexion As New MySqlConnection(Me.CadenaConexion)

        Try
            ' Abro la conexión
            If Not Sincronizador.AbrirConexion(Conexion) Then Exit Sub

            ' Ejecuto las consultas
            For i As Integer = 0 To Especies.Count - 1
                Dim esp As DAL.Especie = Especies(i)
                Dim Comando As New MySqlCommand("REPLACE INTO `especies` (`Id`, `Nome`) VALUES (@Id, @Nome);")

                Comando.Parameters.AddWithValue("Id", esp.id)
                Comando.Parameters.AddWithValue("Nome", esp.denominacionComercial)

                Comando.Connection = Conexion
                Comando.CommandTimeout = Timeout
                Comando.Prepare()
                Comando.ExecuteNonQuery()

                If Not (SincronizarImagen(esp, "Especies")) Then Continue For

                esp.Sincronizando = True
                esp.sincronizadoWeb = True
                LanzarEvento(IndiceGeneral, ((i + 1) * 100) / Especies.Count, String.Format("[{1} de {2}] Sincronizando Especie {0}", esp.denominacionComercial, i + 1, Especies.Count))
            Next
        Catch ex As Exception
            ' Throw ex
        Finally
            Sincronizador.CerrarConexion(Conexion)
        End Try
    End Sub

    ''' <summary>
    ''' Sincroniza las artes con el servidor de trazamare
    ''' </summary>
    Private Sub SincronizarArtes(ByVal Artes As List(Of DAL.MetodoProduccion), IndiceGeneral As Integer)
        ' Validaciones
        If Artes Is Nothing OrElse Artes.Count = 0 Then Exit Sub

        Dim Timeout As Integer = DatosConexionBBDD.Timeout
        If Timeout <= 0 Then Timeout = 30

        Dim Sincronizador As New cConnectionManager(Me.CadenaConexion, Timeout)
        Dim Conexion As New MySqlConnection(Me.CadenaConexion)

        Try
            ' Abro la conexión
            If Not Sincronizador.AbrirConexion(Conexion) Then Exit Sub

            ' Ejecuto las consultas
            For i As Integer = 0 To Artes.Count - 1
                Dim art As DAL.MetodoProduccion = Artes(i)
                Dim Comando As New MySqlCommand("REPLACE INTO `tAlternancia` (`Id`, `Nombre`) VALUES (@Id, @Nombre);")

                Comando.Parameters.AddWithValue("Id", art.id)
                Comando.Parameters.AddWithValue("Nombre", art.descripcion)

                Comando.Connection = Conexion
                Comando.CommandTimeout = Timeout
                Comando.Prepare()
                Comando.ExecuteNonQuery()

                If Not (SincronizarImagen(art, "Alternancias")) Then Continue For

                art.Sincronizando = True
                art.sincronizadoWeb = True
                LanzarEvento(IndiceGeneral, ((i + 1) * 100) / Artes.Count, String.Format("[{1} de {2}] Sincronizando Arte {0}", art.descripcion, i + 1, Artes.Count))
            Next
        Catch ex As Exception
            Throw ex
        Finally
            Sincronizador.CerrarConexion(Conexion)
        End Try
    End Sub

    ''' <summary>
    ''' Sincroniza las especies con el servidor de trazamare
    ''' </summary>
    Private Sub SincronizarEmpaquetados(ByVal Empaquetados As List(Of DAL.Empaquetado), IndiceGeneral As Integer)
        ' Validaciones
        If Empaquetados Is Nothing OrElse Empaquetados.Count = 0 Then Exit Sub

        Dim Timeout As Integer = DatosConexionBBDD.Timeout
        If Timeout <= 0 Then Timeout = 30

        Dim Sincronizador As New cConnectionManager(Me.CadenaConexion, Timeout)
        Dim Conexion As New MySqlConnection(Me.CadenaConexion)

        Try
            ' Abro la conexión
            If Not Sincronizador.AbrirConexion(Conexion) Then Exit Sub

            ' Ejecuto las consultas
            For i As Integer = 0 To Empaquetados.Count - 1
                Dim emp As DAL.Empaquetado = Empaquetados(i)
                Dim Comando As New MySqlCommand("REPLACE INTO `Empaquetados` (`Id`, `Fecha`) VALUES (@Id, @Fecha);")

                Comando.Parameters.AddWithValue("Id", emp.id)
                Comando.Parameters.AddWithValue("Fecha", emp.fecha)

                Comando.Connection = Conexion
                Comando.CommandTimeout = Timeout
                Comando.Prepare()
                Comando.ExecuteNonQuery()

                emp.Sincronizando = True
                emp.sincronizadoWeb = True
                LanzarEvento(IndiceGeneral, ((i + 1) * 100) / Empaquetados.Count, String.Format("[{1} de {2}] Sincronizando Empaquetado {0}", emp.Codigo, i + 1, Empaquetados.Count))
            Next
        Catch ex As Exception
            ' Throw ex
        Finally
            Sincronizador.CerrarConexion(Conexion)
        End Try
    End Sub

    ''' <summary>
    ''' De momento no se usa Pone el campo sincronizado = 0 para los que se acaban de insertar (Sincronizado = 2)
    ''' </summary>
    Private Sub ActualizarEmpaquetadosASinSincronizar()
        Dim Timeout As Integer = DatosConexionBBDD.Timeout
        If Timeout <= 0 Then Timeout = 30

        Dim Sincronizador As New cConnectionManager(Me.CadenaConexion, Timeout)
        Dim Conexion As New MySqlConnection(Me.CadenaConexion)

        Try
            ' Abro la conexión
            If Not Sincronizador.AbrirConexion(Conexion) Then Exit Sub

            Dim Comando As New MySqlCommand("UPDATE `Empaquetados` SET Sincronizado = 0 WHERE Sincronizado = 2;")

            Comando.Connection = Conexion
            Comando.CommandTimeout = Timeout
            Comando.Prepare()
            Comando.ExecuteNonQuery()
        Catch ex As Exception
            ' Throw ex
        Finally
            Sincronizador.CerrarConexion(Conexion)
        End Try
    End Sub

    Private Function SincronizarImagen(Registo As DAL.IImagen, SubCarpeta As String) As Boolean
        ' Validaciones
        If Not Registo.TieneImagen Then Return True
        If String.IsNullOrEmpty(DatosConexionFTP.Servidor) Then Return True

        Dim RutaCarpeta As String = DatosConexionFTP.RutaCarpeta

        Using Cliente As New FtpClient
            Cliente.Host = Me.DatosConexionFTP.Servidor
            Cliente.Credentials = New Net.NetworkCredential(DatosConexionFTP.Usuario, DatosConexionFTP.Clave)
            Cliente.Port = DatosConexionFTP.Puerto

            Try
                Cliente.Connect()

                ' Miro si existe el directorio
                If Not Cliente.DirectoryExists(RutaCarpeta) Then Cliente.CreateDirectory(RutaCarpeta)
                Cliente.SetWorkingDirectory(RutaCarpeta)

                ' Subcarpeta
                If Not Cliente.DirectoryExists(SubCarpeta) Then Cliente.CreateDirectory(SubCarpeta)
                Cliente.SetWorkingDirectory(SubCarpeta)

                ' Si existe el archivo, lo elimino
                If Cliente.FileExists(Registo.Identificador) Then Cliente.DeleteFile(Registo.Identificador)

                Using ms As New System.IO.MemoryStream
                    Registo.Imagen.Save(ms, System.Drawing.Imaging.ImageFormat.Png)

                    Using ftpStream As System.IO.Stream = Cliente.OpenWrite(Registo.Identificador, FtpDataType.Binary)
                        ftpStream.Write(ms.ToArray(), 0, ms.Length)
                    End Using
                End Using

                Return True
            Catch ex As Exception
                EscribirEnLog(ex.Message + "-" + ex.InnerException.ToString())
                Return False
            Finally
                Cliente.Disconnect()
            End Try
        End Using
    End Function

    Public Sub EscribirEnLog(pMensaje As String)
        Dim strFile As String = "c:\ErroresLog.txt"
        ''Dim fileExists As Boolean = File.Exists(strFile)
        File.AppendAllText(strFile, pMensaje + Environment.NewLine)
    End Sub

#End Region
End Class
