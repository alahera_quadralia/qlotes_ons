﻿Imports System.ServiceProcess

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SincronizadorLotes
    Inherits System.ServiceProcess.ServiceBase

    'UserService overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    ' The main entry point for the process
    <MTAThread()> _
    <System.Diagnostics.DebuggerNonUserCode()> _
    Shared Sub Main()
#If DEBUG Then
        Dim aux As New SincronizadorLotes
            aux.OnStart(Nothing)
#Else
        Dim ServicesToRun() As System.ServiceProcess.ServiceBase

        ' More than one NT Service may run within the same process. To add
        ' another service to this process, change the following line to
        ' create a second service object. For example,
        '
        '   ServicesToRun = New System.ServiceProcess.ServiceBase () {New Service1, New MySecondUserService}
        '
        ServicesToRun = New System.ServiceProcess.ServiceBase() {New SincronizadorLotes}

        System.ServiceProcess.ServiceBase.Run(ServicesToRun)
#End If
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    ' NOTE: The following procedure is required by the Component Designer
    ' It can be modified using the Component Designer.  
    ' Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LogEventos = New System.Diagnostics.EventLog()
        CType(Me.LogEventos, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'LogEventos
        '
        Me.LogEventos.EnableRaisingEvents = True
        Me.LogEventos.Log = "Application"
        Me.LogEventos.Source = "SincronizadorCG_Log"
        '
        'SincronizadorLotes
        '
        Me.ServiceName = "qLotesSyncCG"
        CType(Me.LogEventos, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents LogEventos As System.Diagnostics.EventLog

End Class
