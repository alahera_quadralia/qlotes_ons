﻿Imports System.Reflection
Imports System.Data.Common
Imports System.Configuration
Imports System.Data
Imports System.Collections.Generic

Public MustInherit Class cConnectionManager
#Region " OBTENCION DE OBJETOS "
    Public CadenaDeConexion As String = String.Empty
    Public Property TimeOut As Integer = 100

    Public MustOverride Function ObtenerProveedor() As DbProviderFactory
    Public MustOverride Function AnhadirParametro(ByRef Comando As DbCommand, ByVal NombreParametro As String, ByVal valor As Object) As DbParameter
    Public MustOverride Function ObtenerId(ByRef Comando As DbCommand, ByRef Conexion As DbConnection) As Object

    ''' <summary>
    ''' Obtiene una conexion
    ''' </summary>
    Public Function ObtenerConexion() As DbConnection
        Dim Conexion As DbConnection = ObtenerProveedor.CreateConnection
        Conexion.ConnectionString = CadenaDeConexion

        Return Conexion
    End Function

    ''' <summary>
    ''' Obtiene un adaptador de datos
    ''' </summary>
    Public Function ObtenerDataAdapter() As DbDataAdapter
        Dim dbFactory As DbProviderFactory = ObtenerProveedor()
        Return dbFactory.CreateDataAdapter()
    End Function

    ''' <summary>
    ''' Obtiene un comando vacio
    ''' </summary>
    Public Function ObtenerComando() As DbCommand
        Dim dbFactory As DbProviderFactory = ObtenerProveedor()
        Dim Comando As DbCommand = dbFactory.CreateCommand()
        Comando.CommandTimeout = TimeOut
        Return Comando
    End Function

    ''' <summary>
    ''' Obtiene un comando Especifico
    ''' </summary>
    ''' <param name="ConsultaSQL">Consulta que pretendemos ejecutar con el comando</param>
    Public Function ObtenerComando(ByVal ConsultaSQL As String) As DbCommand
        Dim Comando As DbCommand = ObtenerComando()
        Comando.CommandText = ConsultaSQL
        Return Comando
    End Function

    ''' <summary>
    ''' Obtiene un comando Especifico
    ''' </summary>
    ''' <param name="ConsultaSQL">Consulta que pretendemos ejecutar con el comando</param>
    ''' <param name="Conexion">Conexión necesaria para ejecutar el comando</param>
    Public Function ObtenerComando(ByVal ConsultaSQL As String, ByRef Conexion As DbConnection) As DbCommand
        Dim Comando As DbCommand = ObtenerComando()
        Comando.CommandText = ConsultaSQL
        Comando.Connection = Conexion
        Comando.CommandTimeout = TimeOut
        Return Comando
    End Function

    ''' <summary>
    ''' Obtiene un constructor de datos
    ''' </summary>
    Public Function ObtenerCommandBuilder() As DbCommandBuilder
        Dim dbFactory As DbProviderFactory = ObtenerProveedor()
        Return dbFactory.CreateCommandBuilder()
    End Function

    ''' <summary>
    ''' Obtiene un parámetro para los comandos
    ''' </summary>
    Public Function ObtenerParametro() As DbParameter
        Dim dbFactory As DbProviderFactory = ObtenerProveedor()
        Return dbFactory.CreateParameter()
    End Function
#End Region
#Region " METODOS PRIVADOS "
    ''' <summary>
    ''' Función que abre una conexión
    ''' </summary>
    ''' <returns>True si se estableció la conexión</returns>
    Private Function AbrirConexion(ByVal conn As DbConnection) As Boolean
        If Not conn Is Nothing Then
            If conn.State <> ConnectionState.Connecting AndAlso conn.State <> ConnectionState.Open Then
                conn.Open()
            End If

            Return True
        Else
            Return False
        End If
    End Function

    ''' <summary>
    ''' Función que cierra una conexión existente
    ''' </summary>
    Private Sub CerrarConexion(ByVal conn As DbConnection)
        If Not conn Is Nothing Then
            If conn.State <> ConnectionState.Closed Then conn.Close()
        End If
    End Sub
#End Region
#Region " METODOS PUBLICOS "
    ''' <summary>
    ''' Intenta realizar una conexión con la base de datos especificada
    ''' </summary>
    Public Function SePuedeConectar(ByVal CadenaConexion As String, Optional ByRef ErrorDevuelto As Exception = Nothing) As Boolean
        Dim Conexion As DbConnection = Nothing

        Try
            'Creo la conexión en intento conectarme
            Conexion = ObtenerConexion()
            Conexion.ConnectionString = CadenaConexion
            AbrirConexion(Conexion)

            'Si todo ha ido bien, devuelvo true
            Return True
        Catch ex As Exception
            ErrorDevuelto = ex
            Return False
        Finally
            'Pase lo que pase, cierro la conexión
            CerrarConexion(Conexion)
        End Try

    End Function

    ''' <summary>
    ''' Cierra Un reader previamente abierto
    ''' </summary>
    ''' <param name="Reader">Reader que queremos cerrar</param>
    Public Sub CerrarDataReader(ByRef Reader As IDataReader)
        'Compruebo que el reader esté abierto y no esté cerrado
        If Reader IsNot Nothing AndAlso Not Reader.IsClosed Then
            Reader.Close()
            Reader = Nothing
        End If
    End Sub

    ''' <summary>
    ''' Ejecuta las consultas que se le pasan por parametro
    ''' </summary>
    ''' <param name="Consulta">Consultas a ejecutar</param>
    Public Function EjecutarConsulta(ByVal Consulta() As String) As Long
        Dim Conexion As DbConnection = ObtenerConexion()
        If AbrirConexion(Conexion) Then
            Dim Transaccion As DbTransaction = Conexion.BeginTransaction()

            Dim Resultado As Long = -1

            Try
                For Each Cons As String In Consulta
                    Dim Comando As DbCommand = ObtenerComando(Cons, Conexion)
                    Comando.Transaction = Transaccion
                    Comando.CommandTimeout = TimeOut
                    Resultado = Comando.ExecuteNonQuery()
                Next

                Transaccion.Commit()

            Catch ex As Exception
                Transaccion.Rollback()
                'Throw ex
            Finally
                CerrarConexion(Conexion)
                EjecutarConsulta = Resultado
            End Try
        Else
            Return -1
        End If
    End Function

    ''' <summary>
    ''' Ejecuta la consulta que se le pasa por parametro
    ''' </summary>
    ''' <param name="Consulta">Consulta a ejecutar</param>
    ''' <returns>-1 si se produce un error, o el número de filas afectadas en otro caso</returns>
    Public Function EjecutarConsulta(ByVal Consulta As String) As Long
        Dim ListaConsultas() As String = {Consulta}
        Return EjecutarConsulta(ListaConsultas)
    End Function

    ''' <summary>
    ''' Ejecuta el comando que se le pasa por parametro
    ''' </summary>
    ''' <param name="Comando">Comando a ejecutar</param>
    Public Function EjecutarComando(ByRef Comando As IDbCommand) As Object
        Dim Conexion As DbConnection = ObtenerConexion()

        If AbrirConexion(Conexion) Then
            Try
                Comando.Connection = Conexion
                Comando.CommandTimeout = TimeOut
                Comando.Prepare()
                Return Comando.ExecuteScalar
            Catch ex As Exception
                Throw ex
            Finally
                CerrarConexion(Comando.Connection)
            End Try
        Else
            Return Nothing
        End If
    End Function

    ''' <summary>
    ''' Ejecuta el comando que se le pasa por parametro
    ''' </summary>
    ''' <param name="Comando">Comando a ejecutar</param>
    Public Function EjecutarComandoNonQuery(ByRef Comando As IDbCommand, Optional ByVal MantenerConexionAbierta As Boolean = False) As Boolean
        Dim Conexion As DbConnection = ObtenerConexion()
        If AbrirConexion(Conexion) Then
            Try
                Comando.Connection = Conexion
                Comando.CommandTimeout = TimeOut
                Comando.ExecuteNonQuery()
                Return True
            Catch ex As Exception
                Throw ex
                Return False
            Finally
                If Not MantenerConexionAbierta Then CerrarConexion(Comando.Connection)
            End Try
        End If

        Return -1
    End Function

    ''' <summary>
    ''' Ejecuta la consulta que se le pasa por parametro
    ''' </summary>
    ''' <param name="Consulta">Consulta a ejecutar</param>
    ''' <returns>El dataReader asociado a la consulta</returns>
    Public Function ObtenerReader(ByVal Consulta As String, Optional ByVal Estructura As Boolean = False) As DbDataReader
        Dim Conexion As DbConnection = ObtenerConexion()
        If AbrirConexion(Conexion) Then
            Try
                Dim Comando As DbCommand = ObtenerComando(Consulta, Conexion)
                If Estructura Then
                    Return Comando.ExecuteReader(CommandBehavior.CloseConnection + CommandBehavior.KeyInfo)
                Else
                    Return Comando.ExecuteReader(CommandBehavior.CloseConnection)
                End If
            Catch ex As Exception
                CerrarConexion(Conexion)
                Throw ex
            End Try
        Else
            Return Nothing
        End If
    End Function

    ''' <summary>
    ''' Ejecuta la consulta que se le pasa por parametro
    ''' </summary>
    ''' <param name="Comando">Comando a ejecutar</param>
    ''' <returns>El dataReader asociado a la consulta</returns>
    Public Function ObtenerReader(ByVal Comando As DbCommand, Optional ByVal Estructura As Boolean = False) As DbDataReader
        Dim Conexion As DbConnection = ObtenerConexion()
        If AbrirConexion(Conexion) Then
            Try
                Comando.Connection = Conexion
                Comando.CommandTimeout = TimeOut
                If Estructura Then
                    Return Comando.ExecuteReader(CommandBehavior.CloseConnection + CommandBehavior.KeyInfo)
                Else
                    Return Comando.ExecuteReader(CommandBehavior.CloseConnection)
                End If
            Catch ex As Exception
                CerrarConexion(Conexion)
                Throw ex
            End Try
        Else
            Return Nothing
        End If
    End Function

    ''' <summary>
    ''' Ejecuta la consulta que se le pasa por parametro
    ''' </summary>
    ''' <param name="Consulta">Consulta a ejecutar</param>
    ''' <returns>El valor de la primera columna de la primera fila del conjunto de datos obtenido por la consulta</returns>
    Public Function ObtenerPrimerValor(ByVal Consulta As String) As Object
        Return ObtenerPrimerValor(ObtenerComando(Consulta))
    End Function

    ''' <summary>
    ''' Ejecuta la consulta que se le pasa por parametro
    ''' </summary>
    ''' <param name="Consulta">Consulta a ejecutar</param>
    ''' <returns>El valor de la primera columna de la primera fila del conjunto de datos obtenido por la consulta</returns>
    Public Function ObtenerPrimerValor(ByVal Consulta As String, ByRef Conexion As DbConnection) As Object
        Return ObtenerPrimerValor(ObtenerComando(Consulta, Conexion))
    End Function

    ''' <summary>
    ''' Ejecuta la consulta que se le pasa por parametro
    ''' </summary>
    ''' <param name="Comando">Comando que se va a ejecutar</param>
    ''' <returns>El valor de la primera columna de la primera fila del conjunto de datos obtenido por la consulta</returns>
    Public Function ObtenerPrimerValor(ByVal Comando As DbCommand) As Object
        If Comando.Connection Is Nothing Then Comando.Connection = ObtenerConexion()
        If AbrirConexion(Comando.Connection) Then
            Try
                Return Comando.ExecuteScalar
            Catch ex As Exception
                Throw ex
            Finally
                CerrarConexion(Comando.Connection)
            End Try
        Else
            Return Nothing
        End If
    End Function

    ''' <summary>
    ''' Comprueba si el campo especificado es nulo, en cuyo caso devuelve el valor por defecto
    ''' </summary>
    ''' <param name="DataReader">DataReader en el cual comprobar el campo</param>
    ''' <param name="NombreCampo">Nombre del campo del cual obtener el valor</param>
    ''' <param name="ValorPorDefecto">Valor a devolver en caso de que el registro sea nulo</param>
    Public Function ObtenerValorCorrecto(ByRef DataReader As DbDataReader, ByVal NombreCampo As String, ByVal ValorPorDefecto As Object) As Object
        If DataReader.IsDBNull(DataReader.GetOrdinal(NombreCampo)) Then
            Return ValorPorDefecto
        Else
            Try
                Return DataReader.GetValue(DataReader.GetOrdinal(NombreCampo))
            Catch
                Return ValorPorDefecto
            End Try
        End If
    End Function

    ''' <summary>
    ''' Ejecuta la consulta que se le pasa por parametro
    ''' </summary>
    ''' <returns>Datatable con los datos de la ejecución del comando</returns>
    Public Function ObtenerDataTable(ByVal Consulta As String) As DataTable
        Return ObtenerDataTable(Consulta, ObtenerConexion())
    End Function

    ''' <summary>
    ''' Ejecuta la consulta que se le pasa por parametro
    ''' </summary>
    ''' <returns>Datatable con los datos de la ejecución del comando</returns>
    Public Function ObtenerDataTable(ByVal Consulta As String, ByRef Conexion As DbConnection) As DataTable
        Return ObtenerDataTable(ObtenerComando(Consulta, Conexion))
    End Function

    ''' <summary>
    ''' Ejecuta la consulta que se le pasa por parametro
    ''' </summary>
    ''' <param name="Comando">Comando del cual obtendremos los datos</param>
    ''' <returns>Datatable con los datos de la ejecución del comando</returns>
    Public Function ObtenerDataTable(ByVal Comando As DbCommand) As DataTable
        Dim Conexion As DbConnection = ObtenerConexion()
        Dim Adaptador As DbDataAdapter = ObtenerDataAdapter()
        Dim Resultado As New DataTable

        If AbrirConexion(Conexion) Then
            Try
                Comando.Connection = Conexion
                Comando.CommandTimeout = TimeOut
                Adaptador.SelectCommand = Comando

                ' Obtengo los datos
                Adaptador.Fill(Resultado)

                ' Devuelvo el datatable
                Return Resultado
            Catch ex As Exception
                Throw ex
            Finally
                Adaptador.Dispose()
                CerrarConexion(Conexion)
            End Try
        Else
            Return Nothing
        End If
    End Function
#End Region
End Class