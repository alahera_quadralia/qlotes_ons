﻿Imports System.Data.Common
Imports MySql.Data.MySqlClient

Public Class ConectorMySQL
    Inherits cConnectionManager

    Private Const FormatoParametro As String = "@{0}"

    Public Overrides Function ObtenerProveedor() As DbProviderFactory
        Return MySql.Data.MySqlClient.MySqlClientFactory.Instance
    End Function

    ''' <summary>
    ''' Función para añadir el un parametro a un comando
    ''' </summary>
    ''' <param name="Comando">Comando con el que ejecutar la consulta</param>
    Public Overrides Function AnhadirParametro(ByRef Comando As DbCommand, ByVal NombreParametro As String, ByVal valor As Object) As DbParameter
        If Comando Is Nothing OrElse Not TypeOf (Comando) Is MySqlCommand Then Return Nothing
        Return DirectCast(Comando, MySqlCommand).Parameters.AddWithValue(String.Format(FormatoParametro, NombreParametro), valor)
    End Function

    ''' <summary>
    ''' Función para obtener el último id Insertado
    ''' </summary>
    ''' <param name="Comando">Comando con el que ejecutar la consulta</param>
    Public Overloads Overrides Function ObtenerId(ByRef Comando As DbCommand, ByRef Conexion As DbConnection) As Object
        If Comando Is Nothing OrElse Not TypeOf (Comando) Is MySqlCommand Then Return 0
        Return DirectCast(Comando, MySqlCommand).LastInsertedId
    End Function
End Class