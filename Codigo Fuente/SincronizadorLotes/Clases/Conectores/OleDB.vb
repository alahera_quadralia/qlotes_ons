﻿Imports System.Data.Common
Imports System.Data.OleDb

Public Class ConectorOLEDB
    Inherits cConnectionManager

    Private Const FormatoParametro As String = "@{0}"

    Public Overrides Function ObtenerProveedor() As DbProviderFactory
        Return OleDbFactory.Instance
    End Function

    ''' <summary>
    ''' Función para añadir el un parametro a un comando
    ''' </summary>
    ''' <param name="Comando">Comando con el que ejecutar la consulta</param>
    Public Overrides Function AnhadirParametro(ByRef Comando As DbCommand, ByVal NombreParametro As String, ByVal valor As Object) As DbParameter
        If Comando Is Nothing OrElse Not TypeOf (Comando) Is OleDbCommand Then Return Nothing
        ' Return DirectCast(Comando, OleDbCommand).Parameters.AddWithValue(String.Format(FormatoParametro, NombreParametro), valor)
        Dim Parametro As New OleDbParameter(NombreParametro, valor)
        Comando.Parameters.Add(Parametro)
        Return Parametro
    End Function

    ''' <summary>
    ''' Función para obtener el último id Insertado
    ''' </summary>
    ''' <param name="Comando">Comando con el que ejecutar la consulta</param>
    Public Overloads Overrides Function ObtenerId(ByRef Comando As DbCommand, ByRef Conexion As DbConnection) As Object
        If Not TypeOf (Comando) Is OleDbCommand Then Return 0

        Dim Id As Object = ObtenerPrimerValor("SELECT @@IDENTITY", Conexion)

        If IsDBNull(Id) Then
            Return 0
        Else
            Return Id
        End If
    End Function
End Class