﻿Imports System.Data.OleDb

Public Class Proveedor
    Public Property Id As Integer
    Public Property CIF As String
    Public Property Codigo As String
    Public Property Nombre As String
    Public Property Direccion As String
    Public Property Localidad As String
    Public Property Provincia As String
    Public Property CP As Double
    Public Property Telefono As String
End Class

Public Class Articulo
    Public Property Id As Integer
    Public Property Nombre As String
    Public Property Codigo As String
End Class

Public Class Pedido
    Public Property Lineas As New List(Of LineaPedido)
    Public Property Fecha As DateTime
    Public Property Id As Integer
    Public Property IdClassic As Integer
    Public Property IdAlmacen As Integer
    Public Property NumeroClassic As Double
    Public Property Ejercicio As Integer
    Public Property IdTiendaClassic As Integer

    Public ReadOnly Property Codigo As String
        Get
            Return Id.ToString.PadLeft(8, "0")
        End Get
    End Property

    Public Property Proveedor As Proveedor = Nothing
End Class

Public Class LineaPedido
    Public Property Id As Integer
    Public Property IdClassic As Integer
    Public Property Peso As Double
    Public Property NombreEspecie As String
    Public Property Lote As String
    Public ReadOnly Property NombreArticulo As String
        Get
            If Articulo Is Nothing OrElse String.IsNullOrEmpty(Articulo.Nombre) Then Return NombreEspecie
            Return Articulo.Nombre
        End Get
    End Property

    Public Property Articulo As Articulo = Nothing
End Class

Public Class cBBDD_Proxy
    Private Shared _Instancia As cBBDD_Proxy = Nothing
    Public Shared Property Conector As ConectorOLEDB
    Private ReadOnly Property FechaNula As DateTime
        Get
            Return New DateTime(1899, 12, 30)
        End Get
    End Property

    Public Shared ReadOnly Property Instancia As cBBDD_Proxy
        Get
            If _Instancia Is Nothing Then
                _Instancia = New cBBDD_Proxy()
            End If

            Return _Instancia
        End Get
    End Property

    ''' <summary>
    ''' Importar un pedido a la base de datos
    ''' </summary>
    Public Function ImportarPedido(Pedidos As List(Of Pedido), Instancia As SincronizadorLotes) As Boolean
        Dim Resultado As Boolean = True

        For Each Pedido In Pedidos
            Resultado = ImportarPedido(Pedido, Instancia) AndAlso Resultado
        Next

        Return Resultado
    End Function

    ''' <summary>
    ''' Importar un pedido a la base de datos
    ''' </summary>
    Public Function ImportarPedido(Pedido As Pedido, Instancia As SincronizadorLotes) As Boolean
        Try
            ' Meto el pedido
            If Not GuardarPedidoGeneral(Pedido) Then Throw New Exception("PedSavx01: No se pudo guardar el pedido")

            ' Meto las líneas
            If Pedido.Lineas IsNot Nothing Then
                For Each Linea As LineaPedido In Pedido.Lineas
                    If Not GuardarLinea(Linea, Pedido.IdClassic) Then Continue For
                Next
            End If

            Instancia.NotificarInfo(String.Format("Se ha importado correctamente el lote {0}", Pedido.Codigo), SincronizadorLotes.CodigosEventos.Importacion)
            Return True
        Catch ex As Exception
            Instancia.NotificarError(String.Format("Error al importar el lote {0}", Pedido.Codigo), ex, SincronizadorLotes.CodigosEventos.ErrorImportacion)
            Return False
        End Try
    End Function

#Region " INSERCCION DE DATOS "
#Region " DATOS PEDIDO "
    ''' <summary>
    ''' Guarda los datos generales del pedido y devuelve el id generado
    ''' </summary>
    Private Function GuardarPedidoGeneral(ByRef Pedido As Pedido) As Boolean
        Try
            ' Obtengo el cliente
            Pedido.Proveedor = ObtenerProveedor(cConfiguracionBBDD.Instancia.IdProveedor)

            ' Obtengo los números de factura
            Pedido.IdClassic = ObtenerSiguienteId("pedidop", "clapedp")
            Pedido.NumeroClassic = ObtenerSiguienteId("pedidop", "numero", "WHERE (claeje = " & Pedido.Ejercicio & " AND claemp = " & Pedido.IdTiendaClassic & ")")

            Dim Consulta As String = "INSERT INTO pedidop(clapedp, claemp, claeje, numero, claalm, clapro, cifpro, codpro, nompro, dirpro, locpro, provpro, postalpro, fecha, fechaentre, fechatope, descrip, referencia, bimpo, itedto, itedtopp, iteiva, itere, importe, clafpa, dtocial, dtopp, portes, regimeniva, dpago1, dpago2, clatra, serviped, pago1, pago2, pago3, pago4, coment, telpro) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
            Dim Comando As OleDbCommand = Conector.ObtenerComando(Consulta)

            Conector.AnhadirParametro(Comando, "clapedp", Pedido.IdClassic)
            Conector.AnhadirParametro(Comando, "claemp", Pedido.IdTiendaClassic)
            Conector.AnhadirParametro(Comando, "claeje", Pedido.Ejercicio)
            Conector.AnhadirParametro(Comando, "numero", Pedido.NumeroClassic)
            Conector.AnhadirParametro(Comando, "claalm", Pedido.IdAlmacen)
            Conector.AnhadirParametro(Comando, "clapro", Pedido.Proveedor.Id)
            Conector.AnhadirParametro(Comando, "cifpro", Pedido.Proveedor.CIF)
            Conector.AnhadirParametro(Comando, "codpro", Pedido.Proveedor.Codigo)
            Conector.AnhadirParametro(Comando, "nompro", Pedido.Proveedor.Nombre)
            Conector.AnhadirParametro(Comando, "dirpro", Pedido.Proveedor.Direccion)
            Conector.AnhadirParametro(Comando, "locpro", Pedido.Proveedor.Localidad)
            Conector.AnhadirParametro(Comando, "provpro", Pedido.Proveedor.Provincia)
            Conector.AnhadirParametro(Comando, "postalpro", Pedido.Proveedor.CP)
            Conector.AnhadirParametro(Comando, "fecha", Pedido.Fecha)
            Conector.AnhadirParametro(Comando, "fechaentre", Pedido.Fecha)
            Conector.AnhadirParametro(Comando, "fechatope", FechaNula)
            Conector.AnhadirParametro(Comando, "descrip", String.Format("Lote {0} importado automáticamente", Pedido.Codigo))
            Conector.AnhadirParametro(Comando, "referencia", String.Format("{0}", Pedido.Codigo))
            Conector.AnhadirParametro(Comando, "bimpo", CDbl(0))
            Conector.AnhadirParametro(Comando, "itedto", CDbl(0))
            Conector.AnhadirParametro(Comando, "itedtopp", CDbl(0))
            Conector.AnhadirParametro(Comando, "iteiva", CDbl(0))
            Conector.AnhadirParametro(Comando, "itere", CDbl(0))
            Conector.AnhadirParametro(Comando, "importe", CDbl(0))
            Conector.AnhadirParametro(Comando, "clafpa", CInt(0))
            Conector.AnhadirParametro(Comando, "dtocial", CDbl(0))
            Conector.AnhadirParametro(Comando, "dtopp", CDbl(0))
            Conector.AnhadirParametro(Comando, "portes", CDbl(0))
            Conector.AnhadirParametro(Comando, "regimeniva", CDbl(0))
            Conector.AnhadirParametro(Comando, "dpago1", CDbl(0))
            Conector.AnhadirParametro(Comando, "dpago2", CDbl(0))
            Conector.AnhadirParametro(Comando, "clatra", CInt(0))
            Conector.AnhadirParametro(Comando, "serviped", False)
            Conector.AnhadirParametro(Comando, "pago1", CDbl(0))
            Conector.AnhadirParametro(Comando, "pago2", CDbl(0))
            Conector.AnhadirParametro(Comando, "pago3", CDbl(0))
            Conector.AnhadirParametro(Comando, "pago4", CDbl(0))
            Conector.AnhadirParametro(Comando, "coment", String.Format("Lote {0} importado automáticamente el día {1:dd/MM/yyyy} a las {1:HH/mm/ss}", Pedido.Codigo, DateTime.Now))
            Conector.AnhadirParametro(Comando, "telpro", Pedido.Proveedor.Telefono)

            Return Conector.EjecutarComandoNonQuery(Comando)
        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Guarda los datos de las líneas
    ''' </summary>
    Private Function GuardarLinea(ByRef Linea As LineaPedido, PedidoId As Integer) As Boolean
        Try
            Linea.IdClassic = ObtenerSiguienteId("pedidopl", "clapedpl")

            Dim Consulta As String = "INSERT INTO pedidopl(clapedpl, clapedp, claart, cantidad, servido, codigo, lindesc, coment, dto, iva, precio, tyc, tc1, tc2, tc3, tc4, tc5, tc6, tc7, tc8, tc9, tc10, tc11, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, clacol, mtln, alkz, mtlnum, anular) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
            Dim Comando As OleDbCommand = Conector.ObtenerComando(Consulta)

            Conector.AnhadirParametro(Comando, "clapedpl", Linea.IdClassic)
            Conector.AnhadirParametro(Comando, "clapedp", PedidoId)
            Conector.AnhadirParametro(Comando, "claart", Linea.Articulo.Id)
            Conector.AnhadirParametro(Comando, "cantidad", Linea.Peso)
            Conector.AnhadirParametro(Comando, "servido", 0)
            Conector.AnhadirParametro(Comando, "codigo", Linea.Articulo.Codigo)
            Conector.AnhadirParametro(Comando, "lindesc", Linea.NombreArticulo.Trim())
            Conector.AnhadirParametro(Comando, "coment", Linea.Lote)
            Conector.AnhadirParametro(Comando, "dto", CDbl(0))
            Conector.AnhadirParametro(Comando, "iva", CDbl(0))
            Conector.AnhadirParametro(Comando, "precio", CDbl(0))
            Conector.AnhadirParametro(Comando, "tyc", False)
            Conector.AnhadirParametro(Comando, "tc1", CDbl(0))
            Conector.AnhadirParametro(Comando, "tc2", CDbl(0))
            Conector.AnhadirParametro(Comando, "tc3", CDbl(0))
            Conector.AnhadirParametro(Comando, "tc4", CDbl(0))
            Conector.AnhadirParametro(Comando, "tc5", CDbl(0))
            Conector.AnhadirParametro(Comando, "tc6", CDbl(0))
            Conector.AnhadirParametro(Comando, "tc7", CDbl(0))
            Conector.AnhadirParametro(Comando, "tc8", CDbl(0))
            Conector.AnhadirParametro(Comando, "tc9", CDbl(0))
            Conector.AnhadirParametro(Comando, "tc10", CDbl(0))
            Conector.AnhadirParametro(Comando, "tc11", CDbl(0))
            Conector.AnhadirParametro(Comando, "s1", CDbl(0))
            Conector.AnhadirParametro(Comando, "s2", CDbl(0))
            Conector.AnhadirParametro(Comando, "s3", CDbl(0))
            Conector.AnhadirParametro(Comando, "s4", CDbl(0))
            Conector.AnhadirParametro(Comando, "s5", CDbl(0))
            Conector.AnhadirParametro(Comando, "s6", CDbl(0))
            Conector.AnhadirParametro(Comando, "s7", CDbl(0))
            Conector.AnhadirParametro(Comando, "s8", CDbl(0))
            Conector.AnhadirParametro(Comando, "s9", CDbl(0))
            Conector.AnhadirParametro(Comando, "s10", CDbl(0))
            Conector.AnhadirParametro(Comando, "s11", CDbl(0))
            Conector.AnhadirParametro(Comando, "clacol", CInt(0))
            Conector.AnhadirParametro(Comando, "mtln", False)
            Conector.AnhadirParametro(Comando, "alkz", CDbl(0))
            Conector.AnhadirParametro(Comando, "mtlnum", CDbl(0))
            Conector.AnhadirParametro(Comando, "anular", False)

            Return Conector.EjecutarComandoNonQuery(Comando)
        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function


#End Region
#End Region
#Region " UTILIDADES "
    Private Shared Function ToStr(Campo As Object) As String
        If Campo Is Nothing Then Return String.Empty
        Return Campo.ToString
    End Function

    Private Shared Function ToDbl(Campo As Object) As Double
        If Campo Is Nothing Then Return 0
        If Not IsNumeric(Campo) Then Return 0
        Return CDbl(Campo)
    End Function

    Private Shared Function ToInt(Campo As Object) As Integer
        If Campo Is Nothing Then Return 0
        If Not IsNumeric(Campo) Then Return 0
        Return CInt(Campo)
    End Function

    ''' <summary>
    ''' Bloquea la tabla deseada en el motor de BBDD
    ''' </summary>
    Private Function ObtenerSiguienteId(ByVal Tabla As String, ByVal NombreColumna As String, Optional Condiciones As String = "") As Integer
        Try
            Dim ResultadoConsulta = Conector.ObtenerPrimerValor(String.Format("SELECT MAX({0}) FROM {1} {2}", NombreColumna, Tabla, Condiciones))

            If IsDBNull(ResultadoConsulta) Then
                Return 1
            Else
                Return ResultadoConsulta + 1
            End If
        Catch ex As Exception
            Return 1
        Finally
        End Try
    End Function

    Public Shared Function ObtenerProveedor(IdProveedor As Integer) As Proveedor
        Try
            Dim Resultado As New Proveedor

            Dim dtProveedor As DataTable = Conector.ObtenerDataTable("SELECT cif, codigo, postal, direccion, localidad, nombre, provincia, telefono FROM proveedo WHERE clapro = " & IdProveedor)
            If dtProveedor Is Nothing OrElse dtProveedor.Rows.Count <= 0 Then Return Resultado

            With dtProveedor.Rows(0)
                Resultado.Id = IdProveedor
                If Not .IsNull("cif") Then Resultado.CIF = .Item("cif")
                If Not .IsNull("codigo") Then Resultado.Codigo = .Item("codigo")
                If Not .IsNull("postal") Then Resultado.CP = .Item("postal")
                If Not .IsNull("direccion") Then Resultado.Direccion = .Item("direccion")
                If Not .IsNull("localidad") Then Resultado.Localidad = .Item("localidad")
                If Not .IsNull("nombre") Then Resultado.Nombre = .Item("nombre")
                If Not .IsNull("provincia") Then Resultado.Provincia = .Item("provincia")
                If Not .IsNull("telefono") Then Resultado.Telefono = .Item("telefono")
            End With


            Return Resultado
        Catch ex As Exception
            Return New Proveedor
        End Try
    End Function

    Public Shared Function ObtenerArticulo(CodigoArticulo As String) As Articulo
        Try
            Dim Resultado As New Articulo

            Dim dtArticulo As DataTable = Conector.ObtenerDataTable("SELECT claart, nombre FROM articulo WHERE codigo = """ & CodigoArticulo & """")
            If dtArticulo Is Nothing OrElse dtArticulo.Rows.Count <= 0 Then Return Resultado

            With dtArticulo.Rows(0)
                Resultado.Id = .Item("claart")
                Resultado.Codigo = CodigoArticulo
                If Not .IsNull("nombre") Then Resultado.Nombre = .Item("nombre")
            End With


            Return Resultado
        Catch ex As Exception
            Return New Articulo
        End Try
    End Function

    ''' <summary>
    ''' Obtiene la serie correspondiente al pedido solicitado
    ''' </summary>
    Public Shared Function ObtenerEjercicio(ByRef Fecha As DateTime) As Integer
        Dim Comando As System.Data.Common.DbCommand = Conector.ObtenerComando("SELECT claeje FROM ejercic WHERE (? >= desde) And (? <= hasta)")
        Conector.AnhadirParametro(Comando, "Fecha", Fecha.Date + New TimeSpan(23, 59, 59))
        Conector.AnhadirParametro(Comando, "Fecha", Fecha.Date + New TimeSpan(0, 0, 0))

        Dim Resultado = Conector.ObtenerPrimerValor(Comando)
        Dim UltimoEjercicio = Conector.ObtenerPrimerValor("SELECT MAX(claeje) FROM ejercic")

        If IsDBNull(Resultado) Then
            If IsDBNull(UltimoEjercicio) Then Return 0 Else Return UltimoEjercicio
        Else
            Return CInt(Resultado)
        End If
    End Function

    'Public Shared Function CargarImporteArticulos(ByRef List(Of Sincr Clases.cAsignacionesProductos)) As Integer
    '    Try
    '        Dim Resultado As New Articulo
    '        Dim idArticulos = String.Join(",", pIdArticulo)
    '        pIdArticulo.ForEach(Function(p) idArticulos = p.ToString())
    '        Dim dtArticulo As DataTable = Conector.ObtenerDataTable("SELECT claart, prulcom FROM articulo WHERE codigo in (" & idArticulos & "")

    '        For Each miRow As DataRow In dtArticulo.Rows(0)
    '            cBBDD_Proxy.
    '        Next
    '        If dtArticulo Is Nothing OrElse dtArticulo.Rows.Count <= 0 Then

    '            Return Resultado
    '        End If

    '        With dtArticulo.Rows(0)
    '            Resultado.Id = .Item("claart")
    '            Resultado.Codigo = CodigoArticulo
    '            If Not .IsNull("nombre") Then Resultado.Nombre = .Item("nombre")
    '        End With


    '        Return Resultado
    '    Catch ex As Exception
    '        Return New Articulo
    '    End Try
    'End Function

    ''' <summary>
    ''' Se actualiza número de serie desde comemtario con formato *%texto%*
    ''' </summary>
    ''' <param name="pConectorVFP"></param>
    ''' <returns></returns>
    Public Shared Function ActualizarNumerosSerie(pConectorVFP As ConectorOLEDB) As Boolean
        Try
            Dim strSql As String = "SELECT claalbil, coment FROM albarail WHERE coment LIKE ""*%""" '' LEFT(coment,2) = ""*%"" AND RIGHT(coment,2) = ""%*"""
            Dim dtPedidos As DataTable = pConectorVFP.ObtenerDataTable(strSql)
            Dim comando As OleDbCommand
            If dtPedidos Is Nothing OrElse dtPedidos.Rows.Count <= 0 Then Return True
            Dim resultado As Boolean = True

            For Each miRow As DataRow In dtPedidos.Rows
                If (miRow("coment").ToString().Contains("%*")) Then
                    strSql = "UPDATE albarail SET numserie = """ + miRow("coment").ToString().Trim().Replace("%*", "").Replace("*%", "") + """, coment = """" WHERE claalbil = " + miRow("claalbil").ToString()
                    pConectorVFP.ObtenerComando(strSql)
                    comando = pConectorVFP.ObtenerComando(strSql)
                    resultado = pConectorVFP.EjecutarComandoNonQuery(comando, True)
                    If (resultado = False) Then
                        Return False
                    End If
                End If
            Next

            Return True
        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function

#End Region
End Class
