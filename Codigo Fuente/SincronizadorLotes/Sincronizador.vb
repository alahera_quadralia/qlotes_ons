﻿Imports System.Data.Common

Public Class SincronizadorLotes
#Region " ENUMERADOS "
    Public Enum CodigosEventos
        ConfiguracionCargada = 1
        ConexionBBDD = 2
        SolicitudDeParada = 3
        ParadaCompleta = 4
        Sincronizando = 5
        Importacion = 6

        ' Errores
        ConfiguracionNoCargada = 1001
        SinConexionBBDD = 1002
        SinConfiguracion = 1003
        ErrorSincronizacion = 1005
        ErrorImportacion = 1006
    End Enum
#End Region
#Region " DECLARACIONES "
    ''' <summary>
    ''' Timer para ejecución de sincronizaciones
    ''' </summary>
    Private Timer As Timers.Timer

    ''' <summary>
    ''' Bandera que indica si tenemos conexión a base de datos o no
    ''' </summary>
    Private ConexionBBDD As Boolean = False

    Private _Ejecutando As Boolean = False

    Private _ConectorMysql As ConectorMySQL = Nothing
    Private _ConectorVFP As ConectorOLEDB = Nothing
#End Region
#Region " PROPIEDADES "
    Public ReadOnly Property ConectorMysql As ConectorMySQL
        Get
            If _ConectorMysql Is Nothing Then
                _ConectorMysql = New ConectorMySQL()
                _ConectorMysql.CadenaDeConexion = String.Format("Server={0};Port={1};Database={2};Uid={3};Pwd={4};", cConfiguracionBBDD.Instancia.BBDD_Host, cConfiguracionBBDD.Instancia.BBDD_Puerto, cConfiguracionBBDD.Instancia.BBDD_BaseDatos, cConfiguracionBBDD.Instancia.BBDD_Usuario, cConfiguracionBBDD.Instancia.BBDD_Pass)
            End If

            Return _ConectorMysql
        End Get
    End Property

    Public ReadOnly Property ConectorVFP As ConectorOLEDB
        Get
            If _ConectorVFP Is Nothing Then
                _ConectorVFP = New ConectorOLEDB()
                _ConectorVFP.CadenaDeConexion = String.Format("Provider=vfpoledb;Data Source={0};Collating Sequence=general;", cConfiguracionBBDD.Instancia.RutaCarpeta)
            End If

            Return _ConectorVFP
        End Get
    End Property
#End Region
#Region " INICIO Y PARADA DEL SERVICIIO "
    ''' <summary>
    ''' Rutina de inicio del servicio
    ''' </summary>
    ''' <param name="args"></param>
    ''' <remarks></remarks>
    Protected Overrides Sub OnStart(ByVal args() As String)
        CargarConfiguracion()
        If Not ConexionBBDD Then
            ExitCode = -1
            End
        End If

        RecargarConfiguracion()
    End Sub

    Private Sub RecargarConfiguracion()
#If DEBUG Then
        Sincronizar(Nothing, Nothing)
        Exit Sub
#End If
        If Timer IsNot Nothing Then Timer.Stop()

        ' Cargo la configuración
        Try
            cConfiguracionBBDD.Instancia.Cargar(1)
            If cConfiguracionBBDD.Instancia Is Nothing OrElse cConfiguracionBBDD.Instancia.Id <= 0 Then
                NotificarError("No se ha podido cargar la configuración de la base de datos", New Exception("No se ha podido cargar la configuración de la base de datos"), CodigosEventos.SinConfiguracion)
                ExitCode = -2
                End
            End If
        Catch ex As Exception
            NotificarError("No se ha podido cargar la configuración de la base de datos", ex, CodigosEventos.SinConfiguracion)
        End Try

        ' Añado la configuración del servidor
        If cConfiguracionBBDD.Instancia.SegundosSincronizacion <= 0 Then Exit Sub

        Timer = New Timers.Timer(cConfiguracionBBDD.Instancia.SegundosSincronizacion * 1000)
        RemoveHandler Timer.Elapsed, AddressOf Sincronizar
        AddHandler Timer.Elapsed, AddressOf Sincronizar
        Timer.Start()
    End Sub

    Protected Overrides Sub OnStop()
        NotificarInfo("Petición de parada recibida", CodigosEventos.SolicitudDeParada)
        Timer.Stop()

        NotificarInfo("Servicio Detenido", CodigosEventos.ParadaCompleta)
        ExitCode = 1
    End Sub

    ''' <summary>
    ''' Ejecuta el comando deseado
    ''' </summary>
    Protected Overrides Sub OnCustomCommand(command As Integer)
        Select Case command
            Case 101 ' Recargar Configuracion
                RecargarConfiguracion()
            Case 102 ' Forzar Sincronizacion
                Sincronizar(Nothing, Nothing)
        End Select
    End Sub
#End Region
#Region " CONTROL DEL TIMER "
    Public Sub Sincronizar(sender As Object, e As Timers.ElapsedEventArgs)
        If Not ConexionBBDD Then Exit Sub
        If _Ejecutando Then Exit Sub

        Try
            cBBDD_Proxy.ActualizarNumerosSerie(ConectorVFP)
            Dim Lotes As DataTable = ConectorMysql.ObtenerDataTable("SELECT * FROM `Empaquetados` WHERE `Sincronizado` = 0 ORDER BY Id")

            ' Si no tengo lotes, no sigo
            If Lotes Is Nothing OrElse Lotes.Rows.Count <= 0 Then Exit Sub

            Dim Etiquetas As DataTable = ConectorMysql.ObtenerDataTable("SELECT `etiqueta`.`Id`, `etiqueta`.`IdBarco`, `etiqueta`.`IDEspecie`, `etiqueta`.`IdEmpaquetado`, `etiqueta`.`Validada`, `etiqueta`.`dataValidacion`, `etiqueta`.`Peso`, `etiqueta`.`Formato`, `etiqueta`.`Lote`, `especies`.`Nome` as `NombreEspecie` FROM `etiqueta` left join `especies` on `etiqueta`.`IDEspecie` = `especies`.`Id` WHERE `etiqueta`.`IdEmpaquetado` IN (SELECT Id FROM `Empaquetados` WHERE `Sincronizado` = 0)")
            Dim DsDatos As New DataSet
            Lotes.TableName = "Lotes"
            Etiquetas.TableName = "Etiquetas"
            DsDatos.Tables.Add(Lotes)
            DsDatos.Tables.Add(Etiquetas)
            DsDatos.Relations.Add("Relacion", Lotes.Columns("Id"), Etiquetas.Columns("IdEmpaquetado"))

            Dim Pedidos As New List(Of Pedido)

            cBBDD_Proxy.Conector = ConectorVFP
            For Each Fila As DataRow In Lotes.Rows
                Dim pedido As New Pedido
                If Fila.IsNull("Fecha") Then pedido.Fecha = DateTime.Now Else pedido.Fecha = Fila("Fecha")
                pedido.Ejercicio = cBBDD_Proxy.ObtenerEjercicio(pedido.Fecha)
                pedido.Id = Fila("Id")
                pedido.IdAlmacen = cConfiguracionBBDD.Instancia.IdAlmacen
                pedido.IdTiendaClassic = cConfiguracionBBDD.Instancia.IdTienda
                pedido.Proveedor = cBBDD_Proxy.ObtenerProveedor(cConfiguracionBBDD.Instancia.IdProveedor)

                For Each Etiqueta As DataRow In Fila.GetChildRows("Relacion")
                    Dim Linea As New LineaPedido

                    Linea.Id = Etiqueta("Id")
                    Linea.Peso = Etiqueta("Peso")
                    Linea.Lote = Etiqueta("Lote")

                    If Not Etiqueta.IsNull("NombreEspecie") Then Linea.NombreEspecie = Etiqueta("NombreEspecie")

                    Dim Asignacion As Clases.cAsignacionesProductos = ObtenerAsignacion(Etiqueta("Peso"), Etiqueta("Formato"), Etiqueta("IDEspecie"))
                    If Asignacion IsNot Nothing Then
                        Linea.Articulo = cBBDD_Proxy.ObtenerArticulo(Asignacion.IdArticuloCG.ToString())
                    Else
                        Linea.Articulo = New Articulo
                    End If

                    pedido.Lineas.Add(Linea)
                Next

                Pedidos.Add(pedido)
            Next

            ' Importamos los pedidos
            Dim Importados As New List(Of Integer)
            For Each Pedido In Pedidos
                If cBBDD_Proxy.Instancia.ImportarPedido(Pedido, Me) Then Importados.Add(Pedido.Id)
            Next

            ' Notifico los que han sido importados
            If Importados.Count > 0 Then
                Dim Consulta As String = String.Format("UPDATE `Empaquetados` SET `Sincronizado` = 1 WHERE Id IN ({0})", String.Join(", ", Importados.ToArray))
                ConectorMysql.EjecutarConsulta(Consulta)
            End If
        Catch ex As Exception
            NotificarError("Error durante el proceso de sincronización", ex, CodigosEventos.ErrorSincronizacion)
        Finally
            _Ejecutando = False
        End Try
    End Sub
#End Region
#Region " CONFIGURACIONES "
    ''' <summary>
    ''' Carga la configuracion de acceso a la base de datos y del programa en general
    ''' </summary>
    Private Sub CargarConfiguracion()
        Try
            ' Cargo la configuración del programa
            cConfiguracionBBDD.Instancia.Cargar(1)

            NotificarInfo("Configuración cargada correctamente", CodigosEventos.ConfiguracionCargada)
        Catch ex As Exception
            NotificarError("No se pudo cargar la configuración del archivo de configuración", ex, CodigosEventos.ConfiguracionNoCargada)
        End Try

#If DEBUG Then
        'cConfiguracionBBDD.Instancia.BBDD_Host = "bbdd.trazamare.com"
        'cConfiguracionBBDD.Instancia.BBDD_Puerto = 3306
        'cConfiguracionBBDD.Instancia.BBDD_BaseDatos = "trazamarebd"
        'cConfiguracionBBDD.Instancia.BBDD_Usuario = "root"
        'cConfiguracionBBDD.Instancia.BBDD_Pass = "Q.428porrinho28"
        'cConfiguracionBBDD.Instancia.RutaCarpeta = "C:\ClasGes6\DATOS"
        'cConfiguracionBBDD.Instancia.IdAlmacen = 1
        'cConfiguracionBBDD.Instancia.IdTienda = 1
        'cConfiguracionBBDD.Instancia.IdProveedor = 1

#End If


        Using Conexion As MySql.Data.MySqlClient.MySqlConnection = ConectorMysql.ObtenerConexion()
            Try
                Conexion.Open()
                ConexionBBDD = True
                NotificarInfo("Comunicación con base de datos establecida correctamente", CodigosEventos.ConexionBBDD)
            Catch ex As Exception
                ConexionBBDD = False
                NotificarError("No se pudo establecer una comunicación con la base de datos", ex, CodigosEventos.SinConexionBBDD)
            Finally
                Conexion.Close()
            End Try
        End Using
    End Sub
#End Region
#Region " LOG "
    ''' <summary>
    ''' Escribe un mensaje de información en el log del sistema
    ''' </summary>
    Public Sub NotificarInfo(Mensaje As String, Identificador As CodigosEventos)
        LogEventos.WriteEntry(Mensaje, EventLogEntryType.Information, Identificador)
    End Sub

    ''' <summary>
    ''' Escribe un mensaje de error en el log del sistema
    ''' </summary>
    Public Sub NotificarError(Mensaje As String, ex As Exception, Identificador As CodigosEventos)
        LogEventos.WriteEntry(String.Format("{0}. Error: {1}", Mensaje, ex.Message), EventLogEntryType.Error, Identificador)
    End Sub
#End Region
#Region " PEDIDOS "
    ''' <summary>
    ''' Obtiene el artículo a importar
    ''' </summary>
    Private Function ObtenerAsignacion(Peso As Decimal, Formato As String, IdEspecie As Integer) As Clases.cAsignacionesProductos
        Clases.cAsignacionesProductos.Conector = New ConectorSqlite() With {.CadenaDeConexion = cConfiguracionBBDD.CadenaConexionUtilizada}
        Dim Asignaciones As List(Of Clases.cAsignacionesProductos) = Clases.cAsignacionesProductos.ObtenerTodos()

        ' Obtengo todos los válidos para mi peso
        Dim Lista As List(Of Clases.cAsignacionesProductos) = (From asig As Clases.cAsignacionesProductos In Asignaciones
                                                               Where asig.IdEspecieTM = IdEspecie _
                                                               AndAlso (Not asig.PesoDesde.HasValue OrElse asig.PesoDesde.Value <= Peso) _
                                                               AndAlso (Not asig.PesoHasta.HasValue OrElse asig.PesoHasta.Value > Peso)
                                                               Order By asig.Id Descending
                                                               Select asig).ToList
        'If (Asignaciones IsNot Nothing And Asignaciones.Count > 0) Then
        '    cBBDD_Proxy.CargarImporteArticulos(Asignaciones)
        'End If


        ' ¿Alguno exclusivo para mi formato?
        Dim Asignacion As Clases.cAsignacionesProductos = (From asig As Clases.cAsignacionesProductos In Lista Where asig.Formato.ToUpper = Formato.ToUpper Select asig).FirstOrDefault
        If Asignacion IsNot Nothing Then Return Asignacion


        ' Sin el formato. Devuelvo el primero
        Return Lista.FirstOrDefault
    End Function

    Private Sub LogEventos_EntryWritten(sender As Object, e As EntryWrittenEventArgs) Handles LogEventos.EntryWritten

    End Sub
#End Region
End Class
