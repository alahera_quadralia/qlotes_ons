﻿Imports System.Reflection
Imports System.Data.Common
Imports System.Configuration
Imports System.Data
Imports System.Collections.Generic

Public Module Configuracion
    Public NombreConexion As String = "Conexion"
    Public CadenaDeConexionPropia As String = String.Empty
    Public TimeOut As Integer = 100

	Public Enum EstadoRegistro
		Eliminar
		Modificado
		Nuevo
		Original
	End Enum

    ''' <summary>
    ''' Obtiene la cadena de conexion especifica o devuelve la primera de todas
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
	Public Function CadenaConexion() As String
        Return CadenaDeConexionPropia
	End Function
End Module

Public NotInheritable Class cConnectionManager
#Region " OBTENCION DE OBJETOS "
	''' <summary>
	''' Obtiene una conexion
	''' </summary>
    Public Shared Function ObtenerConexion() As DbConnection
        Dim Conexion As DbConnection = ObtenerProveedor.CreateConnection
        Conexion.ConnectionString = CadenaConexion()        

        Return Conexion
    End Function

	''' <summary>
	''' Obtiene un adaptador de datos
	''' </summary>
    Public Shared Function ObtenerDataAdapter() As DbDataAdapter
        Dim dbFactory As DbProviderFactory = ObtenerProveedor()
        Return dbFactory.CreateDataAdapter()
    End Function

	''' <summary>
	''' Obtiene un comando vacio
	''' </summary>
    Public Shared Function ObtenerComando() As DbCommand
        Dim dbFactory As DbProviderFactory = ObtenerProveedor()
        Dim Comando As DbCommand = dbFactory.CreateCommand()
        Comando.CommandTimeout = TimeOut
        Return Comando
    End Function

	''' <summary>
	''' Obtiene un comando Especifico
	''' </summary>
	''' <param name="ConsultaSQL">Consulta que pretendemos ejecutar con el comando</param>
    Public Shared Function ObtenerComando(ByVal ConsultaSQL As String) As DbCommand
        Dim Comando As DbCommand = ObtenerComando()
        Comando.CommandText = ConsultaSQL
        Return Comando
    End Function

	''' <summary>
	''' Obtiene un comando Especifico
	''' </summary>
	''' <param name="ConsultaSQL">Consulta que pretendemos ejecutar con el comando</param>
	''' <param name="Conexion">Conexión necesaria para ejecutar el comando</param>
    Public Shared Function ObtenerComando(ByVal ConsultaSQL As String, ByRef Conexion As DbConnection) As DbCommand
        Dim Comando As DbCommand = ObtenerComando()
        Comando.CommandText = ConsultaSQL
        Comando.Connection = Conexion
        Comando.CommandTimeout = TimeOut
        Return Comando
    End Function

	''' <summary>
	''' Obtiene un constructor de datos
	''' </summary>
    Public Shared Function ObtenerCommandBuilder() As DbCommandBuilder
        Dim dbFactory As DbProviderFactory = ObtenerProveedor()
        Return dbFactory.CreateCommandBuilder()
    End Function

    ''' <summary>
    ''' Obtiene un parámetro para los comandos
    ''' </summary>
    Public Shared Function ObtenerParametro() As DbParameter
        Dim dbFactory As DbProviderFactory = ObtenerProveedor()
        Return dbFactory.CreateParameter()
    End Function
#End Region
#Region " METODOS PRIVADOS "
    ''' <summary>
    ''' Función que abre una conexión
    ''' </summary>
    ''' <returns>True si se estableció la conexión</returns>
    Private Shared Function AbrirConexion(ByVal conn As DbConnection) As Boolean
        If Not conn Is Nothing Then
            If conn.State <> ConnectionState.Connecting AndAlso conn.State <> ConnectionState.Open Then
                conn.Open()
            End If

            Return True
        Else
            Return False
        End If
    End Function

    ''' <summary>
    ''' Función que cierra una conexión existente
    ''' </summary>
    Private Shared Sub CerrarConexion(ByVal conn As DbConnection)
        If Not conn Is Nothing Then
            If conn.State <> ConnectionState.Closed Then conn.Close()
        End If
    End Sub

    ''' <summary>
    ''' Genera un comando de actualización
    ''' </summary>
    ''' <param name="Tabla">Tabla de la cual obtener la estructura</param>
    ''' <param name="Clase">Objecto del cual obtener los valores</param>
    Private Shared Function CreaComandoActualizacion(ByVal Tabla As String, ByRef Clase As Object) As DbCommand
        Dim Comando As DbCommand = ObtenerComando()
        Dim Constructor As DbCommandBuilder = ObtenerCommandBuilder()
        Dim Texto As String = String.Format("UPDATE {1}{0}{2} SET", Tabla, Constructor.QuotePrefix, Constructor.QuoteSuffix)
        Dim ClausulaWHERE As String = " WHERE 1"
        Dim ParametrosId As New AdvancedList(Of DbParameter)

        'Obtengo la estructura de la tabla
        Dim Reader As DbDataReader = ObtenerReader(String.Format("SELECT * FROM {0} WHERE 0", Tabla), True)
        Dim Esquema As DataTable = Reader.GetSchemaTable()

        Try
            CerrarDataReader(Reader)

            For Each Fila As DataRow In Esquema.Rows()
                'Compruebo si la clase tiene establecida esa propiedad
                If Not Clase.GetType().GetProperty(Fila("ColumnName")) Is Nothing AndAlso Clase.GetType().GetProperty(Fila("ColumnName")).CanRead Then
                    'Si es la clave primaria, va a la parte del where
                    If Fila("IsKey") Then
                        ClausulaWHERE += String.Format(" AND {1}{0}{2} = " & FormatoParametro, Fila("ColumnName"), Constructor.QuotePrefix, Constructor.QuoteSuffix)
                    Else
                        Texto += String.Format(" {1}{0}{2} = " & FormatoParametro & ",", Fila("ColumnName"), Constructor.QuotePrefix, Constructor.QuoteSuffix)
                    End If

                    'Creo el parámetro para que establezca correctamente los valores
                    Dim Parametro As DbParameter = cConnectionManager.AnhadirParametro(Comando, Fila("ColumnName"), CallByName(Clase, Fila("ColumnName"), CallType.Get, Nothing))

                    ' Lo quito para controlar su adicion
                    Comando.Parameters.Remove(Parametro)

                    'Cambio nothings por dbnulls
                    If Fila("DataType").ToString() = "System.DateTime" Then
                        If (Parametro.Value = Nothing) Then Parametro.Value = DBNull.Value
                    Else
                        If IsNothing(Parametro.Value) Then Parametro.Value = DBNull.Value
                    End If

                    If Fila("IsKey") Then
                        ParametrosId.Add(Parametro)
                    Else
                        Comando.Parameters.Add(Parametro)
                    End If
                End If
            Next

            'Quito la última coma y construyo el texto del comando
            Texto = Texto.Remove(Texto.Length - 1)
            Comando.CommandText = Texto + ClausulaWHERE

            'Añado los Id
            For Each Parametro As DbParameter In ParametrosId
                Comando.Parameters.Add(Parametro)
            Next

            Return Comando
        Catch ex As Exception
            Return Nothing
        Finally
            CerrarDataReader(Reader)
        End Try

        Return Comando

    End Function

    ''' <summary>
    ''' Genera un comando de Inserccion
    ''' </summary>
    ''' <param name="Tabla">Tabla de la cual obtener la estructura</param>
    ''' <param name="Clase">Objecto del cual obtener los valores</param>
    Private Shared Function CreaComandoInserccion(ByVal Tabla As String, ByRef Clase As Object, ByRef CampoClave As String) As DbCommand
        Dim Comando As DbCommand = ObtenerComando()
        Dim Constructor As DbCommandBuilder = ObtenerCommandBuilder()
        Dim Campos As New System.Text.StringBuilder()
        Dim Parametros As New System.Text.StringBuilder()

        'Obtengo la estructura de la tabla
        Dim Reader As DbDataReader = ObtenerReader(String.Format("SELECT * FROM {0} WHERE (1 > 2)", Tabla), True)
        Dim Esquema As DataTable = Reader.GetSchemaTable()

        Try
            CerrarDataReader(Reader)

            For Each Fila As DataRow In Esquema.Rows()
                'Si la fila no existe, o no se puede leer, no la incluyo
                If Not Clase.GetType().GetProperty(Fila("ColumnName")) Is Nothing AndAlso Clase.GetType().GetProperty(Fila("ColumnName")).CanRead Then
                    'Miro si tengo el valor requerido en la clase o no
                    If Fila("IsKey") Then
                        CampoClave = Fila("ColumnName")
                    Else

                        'Añado la columna
                        Campos.Append(Constructor.QuotePrefix)
                        Campos.Append(Fila("ColumnName"))
                        Campos.Append(Constructor.QuoteSuffix)
                        Campos.Append(", ")

                        'Añado los values
                        Parametros.Append(String.Format(FormatoParametro, Fila("ColumnName")))
                        Parametros.Append(", ")

                        'Creo los parámetros

                        Dim Aux As Object = Nothing
                        If Not Clase.GetType().GetProperty(Fila("ColumnName")) Is Nothing Then
                            Aux = CallByName(Clase, Fila("ColumnName"), CallType.Get, Nothing)

                            If Fila("DataType").ToString() = "System.DateTime" Then
                                If (Aux = Nothing) Then Aux = DBNull.Value
                            Else
                                If IsNothing(Aux) Then Aux = DBNull.Value
                            End If
                        Else
                            Aux = DBNull.Value
                        End If

                        cConnectionManager.AnhadirParametro(Comando, Fila("ColumnName"), Aux)
                    End If
                End If
            Next

            'Genero la consulta
            Comando.CommandText = String.Format("INSERT INTO {1}{0}{2} (", Tabla, Constructor.QuotePrefix, Constructor.QuoteSuffix)
            Comando.CommandText += Campos.ToString(0, Campos.Length - 2)
            Comando.CommandText += ") VALUES ("
            Comando.CommandText += Parametros.ToString(0, Parametros.Length - 2)
            Comando.CommandText += "); "

            Return Comando
        Catch ex As Exception
            Return Nothing
        Finally
            CerrarDataReader(Reader)
        End Try

        Return Comando

    End Function
#End Region
#Region " METODOS PUBLICOS "

    ''' <summary>
    ''' Intenta realizar una conexión con la base de datos especificada
    ''' </summary>
    Public Shared Function SePuedeConectar(Optional ByRef ErrorDevuelto As Exception = Nothing) As Boolean
        Dim Conexion As DbConnection = Nothing

        Try
            'Creo la conexión en intento conectarme
            Conexion = ObtenerConexion()
            AbrirConexion(Conexion)

            'Si todo ha ido bien, devuelvo true
            Return True
        Catch ex As Exception
            ErrorDevuelto = ex
            Return False
        Finally
            'Pase lo que pase, cierro la conexión
            CerrarConexion(Conexion)
        End Try

    End Function

    ''' <summary>
    ''' Intenta realizar una conexión con la base de datos especificada
    ''' </summary>
    Public Shared Function SePuedeConectar(ByVal CadenaConexion As String, Optional ByRef ErrorDevuelto As Exception = Nothing) As Boolean
        Dim Conexion As DbConnection = Nothing

        Try
            'Creo la conexión en intento conectarme
            Conexion = ObtenerConexion()
            Conexion.ConnectionString = CadenaConexion
            AbrirConexion(Conexion)

            'Si todo ha ido bien, devuelvo true
            Return True
        Catch ex As Exception
            ErrorDevuelto = ex
            Return False
        Finally
            'Pase lo que pase, cierro la conexión
            CerrarConexion(Conexion)
        End Try

    End Function

    ''' <summary>
    ''' Cierra Un reader previamente abierto
    ''' </summary>
    ''' <param name="Reader">Reader que queremos cerrar</param>
    Public Shared Sub CerrarDataReader(ByRef Reader As IDataReader)
        'Compruebo que el reader esté abierto y no esté cerrado
        If Reader IsNot Nothing AndAlso Not Reader.IsClosed Then
            Reader.Close()
            Reader = Nothing
        End If
    End Sub

    ''' <summary>
    ''' Ejecuta las consultas que se le pasan por parametro
    ''' </summary>
    ''' <param name="Consulta">Consultas a ejecutar</param>
    Public Shared Function EjecutarConsulta(ByVal Consulta() As String) As Long
        Dim Conexion As DbConnection = ObtenerConexion()
        If AbrirConexion(Conexion) Then
            Dim Transaccion As DbTransaction = Conexion.BeginTransaction()

            Dim Resultado As Long = -1

            Try
                For Each Cons As String In Consulta
                    Dim Comando As DbCommand = ObtenerComando(Cons, Conexion)
                    Comando.Transaction = Transaccion
                    Comando.CommandTimeout = TimeOut
                    Resultado = Comando.ExecuteNonQuery()
                Next

                Transaccion.Commit()

            Catch ex As Exception
                Transaccion.Rollback()
                'Throw ex
            Finally
                CerrarConexion(Conexion)
                EjecutarConsulta = Resultado
            End Try
        Else
            Return -1
        End If
    End Function

    ''' <summary>
    ''' Ejecuta la consulta que se le pasa por parametro
    ''' </summary>
    ''' <param name="Consulta">Consulta a ejecutar</param>
    ''' <returns>-1 si se produce un error, o el número de filas afectadas en otro caso</returns>
    Public Shared Function EjecutarConsulta(ByVal Consulta As String) As Long
        Dim ListaConsultas() As String = {Consulta}
        Return EjecutarConsulta(ListaConsultas)
    End Function

    ''' <summary>
    ''' Ejecuta el comando que se le pasa por parametro
    ''' </summary>
    ''' <param name="Comando">Comando a ejecutar</param>
    Public Shared Function EjecutarComando(ByRef Comando As IDbCommand) As Object
        Dim Conexion As DbConnection = ObtenerConexion()

        If AbrirConexion(Conexion) Then
            Try
                Comando.Connection = Conexion
                Comando.CommandTimeout = TimeOut
                Comando.Prepare()
                Return Comando.ExecuteScalar
            Catch ex As Exception
                Throw ex
            Finally
                CerrarConexion(Comando.Connection)
            End Try
        Else
            Return Nothing
        End If
    End Function

    ''' <summary>
    ''' Ejecuta el comando que se le pasa por parametro
    ''' </summary>
    ''' <param name="Comando">Comando a ejecutar</param>
    Public Shared Function EjecutarComandoNonQuery(ByRef Comando As IDbCommand, Optional ByVal MantenerConexionAbierta As Boolean = False) As Integer
        Dim Conexion As DbConnection = ObtenerConexion()
        If AbrirConexion(Conexion) Then
            Try
                Comando.Connection = Conexion
                Comando.CommandTimeout = TimeOut
                Comando.Prepare()
                Return Comando.ExecuteNonQuery()
            Catch ex As Exception
                Throw ex
                Return -1
            Finally
                If Not MantenerConexionAbierta Then CerrarConexion(Comando.Connection)
            End Try
        End If

        Return -1
    End Function

    ''' <summary>
    ''' Ejecuta la consulta que se le pasa por parametro
    ''' </summary>
    ''' <param name="Consulta">Consulta a ejecutar</param>
    ''' <returns>El dataReader asociado a la consulta</returns>
    Public Shared Function ObtenerReader(ByVal Consulta As String, Optional ByVal Estructura As Boolean = False) As DbDataReader
        Dim Conexion As DbConnection = ObtenerConexion()
        If AbrirConexion(Conexion) Then
            Try
                Dim Comando As DbCommand = ObtenerComando(Consulta, Conexion)
                If Estructura Then
                    Return Comando.ExecuteReader(CommandBehavior.CloseConnection + CommandBehavior.KeyInfo)
                Else
                    Return Comando.ExecuteReader(CommandBehavior.CloseConnection)
                End If
            Catch ex As Exception
                CerrarConexion(Conexion)
                Throw ex
            End Try
        Else
            Return Nothing
        End If
    End Function

    ''' <summary>
    ''' Ejecuta la consulta que se le pasa por parametro
    ''' </summary>
    ''' <param name="Comando">Comando a ejecutar</param>
    ''' <returns>El dataReader asociado a la consulta</returns>
    Public Shared Function ObtenerReader(ByVal Comando As DbCommand, Optional ByVal Estructura As Boolean = False) As DbDataReader
        Dim Conexion As DbConnection = ObtenerConexion()
        If AbrirConexion(Conexion) Then
            Try
                Comando.Connection = Conexion
                Comando.CommandTimeout = TimeOut
                If Estructura Then
                    Return Comando.ExecuteReader(CommandBehavior.CloseConnection + CommandBehavior.KeyInfo)
                Else
                    Return Comando.ExecuteReader(CommandBehavior.CloseConnection)
                End If
            Catch ex As Exception
                CerrarConexion(Conexion)
                Throw ex
            End Try
        Else
            Return Nothing
        End If
    End Function

    ''' <summary>
    ''' Ejecuta la consulta que se le pasa por parametro
    ''' </summary>
    ''' <param name="Consulta">Consulta a ejecutar</param>
    ''' <returns>El valor de la primera columna de la primera fila del conjunto de datos obtenido por la consulta</returns>
    Public Shared Function ObtenerPrimerValor(ByVal Consulta As String) As Object
        Return ObtenerPrimerValor(ObtenerComando(Consulta))
    End Function

    ''' <summary>
    ''' Ejecuta la consulta que se le pasa por parametro
    ''' </summary>
    ''' <param name="Comando">Comando que se va a ejecutar</param>
    ''' <returns>El valor de la primera columna de la primera fila del conjunto de datos obtenido por la consulta</returns>
    Public Shared Function ObtenerPrimerValor(ByVal Comando As DbCommand) As Object
        If Comando.Connection Is Nothing Then Comando.Connection = ObtenerConexion()
        If AbrirConexion(Comando.Connection) Then
            Try
                Return Comando.ExecuteScalar
            Catch ex As Exception
                Throw ex
            Finally
                CerrarConexion(Comando.Connection)
            End Try
        Else
            Return Nothing
        End If
    End Function

    ''' <summary>
    ''' Comprueba si el campo especificado es nulo, en cuyo caso devuelve el valor por defecto
    ''' </summary>
    ''' <param name="DataReader">DataReader en el cual comprobar el campo</param>
    ''' <param name="NombreCampo">Nombre del campo del cual obtener el valor</param>
    ''' <param name="ValorPorDefecto">Valor a devolver en caso de que el registro sea nulo</param>
    Public Shared Function ObtenerValorCorrecto(ByRef DataReader As DbDataReader, ByVal NombreCampo As String, ByVal ValorPorDefecto As Object) As Object
        If DataReader.IsDBNull(DataReader.GetOrdinal(NombreCampo)) Then
            Return ValorPorDefecto
        Else
            Try
                Return DataReader.GetValue(DataReader.GetOrdinal(NombreCampo))
            Catch
                Return ValorPorDefecto
            End Try
        End If
    End Function

    ''' <summary>
    ''' Ejecuta la consulta que se le pasa por parametro
    ''' </summary>
    ''' <param name="Comando">Comando del cual obtendremos los datos</param>
    ''' <returns>Datatable con los datos de la ejecución del comando</returns>
    Public Shared Function ObtenerDataTable(ByVal Comando As DbCommand) As DataTable
        Dim Conexion As DbConnection = ObtenerConexion()
        Dim Adaptador As DbDataAdapter = ObtenerDataAdapter()
        Dim Resultado As New DataTable

        If AbrirConexion(Conexion) Then
            Try
                Comando.Connection = Conexion
                Comando.CommandTimeout = TimeOut
                Adaptador.SelectCommand = Comando

                ' Obtengo los datos
                Adaptador.Fill(Resultado)

                ' Devuelvo el datatable
                Return Resultado
            Catch ex As Exception
                Throw ex
            Finally
                Adaptador.Dispose()
                CerrarConexion(Conexion)
            End Try
        Else
            Return Nothing
        End If
    End Function
#End Region
#Region " TRABAJO CON CLASES "
    ''' <summary>
    ''' Realiza la consulta y asigna los valores en cla instancia de la clase
    ''' </summary>
    ''' <param name="Consulta">Consulta SQL con la que obtendremos los valores</param>
    ''' <remarks></remarks>
    Public Shared Function LlenarClase(Of T)(ByVal Consulta As String) As T
        Return LlenarClase(Of T)(ObtenerComando(Consulta))
    End Function

    Private Shared Sub LlenarClase(ByRef Reader As DbDataReader, ByRef Clase As Object)
        If Reader.IsClosed Then Exit Sub

        Dim Esquema As DataTable = Reader.GetSchemaTable()
        Dim Valor As Object = Nothing

        'Recorro todo el esquema
        For Each Fila As DataRow In Esquema.Rows

            'Miro si existe una propiedad en el objeto que se llame como la base y que se pueda escribir
            If Not Clase.GetType().GetProperty(Fila("ColumnName")) Is Nothing AndAlso Clase.GetType().GetProperty(Fila("ColumnName")).CanWrite Then
                Valor = Nothing

                'Verifico el tipo de dato para saber como recuperarla
                If Not Reader.IsDBNull(Reader.GetOrdinal(Fila("ColumnName"))) Then Valor = Reader.Item(Reader.GetOrdinal(Fila("ColumnName")))

                'Asigno el valor a la clase
                Try
                    'Convierto al tipo de la clase
                    Valor = Convert.ChangeType(Valor, Clase.GetType().GetProperty(Fila("ColumnName")).PropertyType)
                    Clase.GetType().GetProperty(Fila("ColumnName")).SetValue(Clase, Valor, Nothing)
                Catch
                    CallByName(Clase, Fila("ColumnName"), CallType.Set, Valor)
                End Try

            End If
        Next
    End Sub

    ''' <summary>
    ''' Realiza la consulta y asigna los valores en cla instancia de la clase
    ''' </summary>
    ''' <param name="Comando">Comando con el que obtendremos los valores</param>
    ''' <remarks></remarks>
    Public Shared Function LlenarClase(Of T)(ByVal Comando As DbCommand) As T
        Dim Reader As DbDataReader = ObtenerReader(Comando)
        Dim Clase As T = Activator.CreateInstance(Of T)()

        Try
            If Reader.Read() Then
                LlenarClase(Reader, Clase)
                Return Clase
            Else
                Clase = Nothing
            End If
        Catch
            Return Nothing
        Finally
            CerrarDataReader(Reader)
        End Try
    End Function

    ''' <summary>
    ''' Devuelve una lista con todas las coincidencias encontradas en la tabla
    ''' </summary>
    ''' <typeparam name="T">Tipo de la clase que se va a devoler</typeparam>
    ''' <param name="Comando">Comando del cual obtendremos los datos</param>
    ''' <returns>Lista de clases de tipo T</returns>
    Public Shared Function ObtenerTodos(Of T)(ByVal Comando As DbCommand) As AdvancedList(Of T)
        Dim Resultado As New AdvancedList(Of T)
        Dim Reader As DbDataReader = ObtenerReader(Comando)

        Try
            While Reader.Read
                Dim Clase As T = Activator.CreateInstance(Of T)()

                LlenarClase(Reader, Clase)

                Resultado.Add(Clase)
            End While

            Return Resultado
        Catch ex As Exception
            Return New AdvancedList(Of T)
        Finally
            CerrarDataReader(Reader)
        End Try


    End Function

    ''' <summary>
    ''' Devuelve una lista con todas las coincidencias encontradas en la tabla
    ''' </summary>
    ''' <typeparam name="T">Tipo de la clase que se va a devoler</typeparam>
    ''' <param name="Tabla">Tabla en la que vamos a buscar los registros</param>
    ''' <param name="Clausulas">Cualquier sentencia SQL posterior a "SELECT * FROM TABLA"</param>
    ''' <returns>Lista de clases de tipo T</returns>
    Public Shared Function ObtenerTodos(Of T)(ByVal Tabla As String, Optional ByVal Clausulas As String = "") As AdvancedList(Of T)
        Dim Resultado As AdvancedList(Of T) = Nothing
        Dim Constructor As DbCommandBuilder = ObtenerCommandBuilder()
        Dim Texto As String = String.Format("SELECT * FROM {1}{0}{2}", Tabla, Constructor.QuotePrefix, Constructor.QuoteSuffix)

        If Not String.IsNullOrEmpty(Clausulas) Then Texto &= " " & Clausulas

        Return ObtenerTodos(Of T)(ObtenerComando(Texto))
    End Function

    ''' <summary>
    ''' Recupera los valores de una clase y genera una consulta de actualización con ellos
    ''' </summary>
    ''' <param name="NombreTabla">Nombre de la tabla sobre la que vamos a realizar la actualizacion</param>
    ''' <param name="Clase">Objeto del cual obtendremos los datos</param>
    Public Shared Function ActualizarClase(ByVal NombreTabla As String, ByRef Clase As Object) As Boolean
        Dim Comando As DbCommand = CreaComandoActualizacion(NombreTabla, Clase)
        Dim Conexion As DbConnection = ObtenerConexion()

        If Comando IsNot Nothing Then
            Comando.Connection = Conexion
            Try
                AbrirConexion(Conexion)
                Comando.ExecuteNonQuery()
                Return True
            Catch ex As Exception
                Throw ex
            Finally
                CerrarConexion(Conexion)
            End Try
        Else
            Return False
        End If
    End Function

    ''' <summary>
    ''' Inserta un nuevo objeto en la base de datos
    ''' </summary>
    ''' <param name="NombreTabla">Nombre de la tabla sobre la que vamos a realizar la inserccion</param>
    ''' <param name="Clase">Objeto del cual obtendremos los datos</param>
    Public Shared Function InsertarClase(ByVal NombreTabla As String, ByRef Clase As Object) As Object
        Dim CampoClave As String = ""
        Dim Comando As DbCommand = CreaComandoInserccion(NombreTabla, Clase, CampoClave)
        Dim Conexion As DbConnection = ObtenerConexion()

        If Comando IsNot Nothing Then
            Comando.Connection = Conexion
            Try
                AbrirConexion(Conexion)
                Comando.ExecuteNonQuery()

                Dim Resultado As Object = ObtenerId(Comando)

                If Not Resultado Is Nothing AndAlso Not Clase.GetType().GetProperty(CampoClave) Is Nothing Then
                    Resultado = Convert.ChangeType(Resultado, Clase.GetType().GetProperty(CampoClave).PropertyType)
                    Clase.GetType().GetProperty(CampoClave).SetValue(Clase, Resultado, Nothing)
                End If

                Return Resultado
            Catch ex As Exception
                Throw ex
            Finally
                CerrarConexion(Conexion)
            End Try
        End If

        Return 0
    End Function
#End Region
End Class