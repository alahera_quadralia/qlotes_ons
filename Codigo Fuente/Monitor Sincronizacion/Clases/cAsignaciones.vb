﻿Imports System.Data.Common

Namespace Clases
    Public Class cAsignacionesProductos
        Implements ICloneable

#Region " DECLARACIONES "
        Private _Id As Int64 = 0   ' Nombre Columna: Id    Acepta Nulos: False    Longitud: -1    AutoIncremental: True    Unica: True
        Private _IdEspecieTM As Int64 = 0   ' Nombre Columna: IdEspecieTM    Acepta Nulos: False    Longitud: -1    AutoIncremental: False    Unica: False
        Private _IdArticuloCG As Int64 = 0   ' Nombre Columna: IdArticuloCG    Acepta Nulos: False    Longitud: -1    AutoIncremental: False    Unica: False
        Private _PesoDesde As Nullable(Of Double) ' Nombre Columna: PesoDesde    Acepta Nulos: True    Longitud: -1    AutoIncremental: False    Unica: False
        Private _PesoHasta As Nullable(Of Double) ' Nombre Columna: PesoHasta    Acepta Nulos: True    Longitud: -1    AutoIncremental: False    Unica: False
        Private _Formato As String = String.Empty   ' Nombre Columna: Formato    Acepta Nulos: False    Longitud: 2147483647    AutoIncremental: False    Unica: False

        Private _Modificado As Boolean = False
        Private _InstanciaEliminada As Boolean = False
        Private Shared _NombreTabla As String = "AsignacionesProductos"
        Private Shared _ConsultaSELECT As String = String.Format("SELECT {0}.Id, {0}.IdEspecieTM, {0}.IdArticuloCG, {0}.PesoDesde, {0}.PesoHasta, {0}.Formato FROM {0}", _NombreTabla)
        Private Shared _ConsultaInsert As String = String.Format("INSERT INTO {0}(IdEspecieTM, IdArticuloCG, PesoDesde, PesoHasta, Formato) VALUES (@IdEspecieTM, @IdArticuloCG, @PesoDesde, @PesoHasta, @Formato)", _NombreTabla)
        Private Shared _ConsultaUpdate As String = String.Format("UPDATE {0} SET IdEspecieTM = @IdEspecieTM, IdArticuloCG = @IdArticuloCG, PesoDesde = @PesoDesde, PesoHasta = @PesoHasta, Formato = @Formato WHERE Id = @Id", _NombreTabla)

        Public Event RegistroModificado(ByVal Registro As cAsignacionesProductos)
#End Region
#Region " PROPIEDADES "
        Public Property Id() As Int64
            Get
                Return _Id
            End Get
            Set(Value As Int64)
                If Not _Id.Equals(Value) Then Modificado = True
                _Id = Value
            End Set
        End Property

        Public Property IdEspecieTM() As Int64
            Get
                Return _IdEspecieTM
            End Get
            Set(Value As Int64)
                If Not _IdEspecieTM.Equals(Value) Then Modificado = True
                If value = 0 Then Throw New ArgumentException("Debe especificar un valor para esta propiedad", "IdEspecieTM")
                _IdEspecieTM = Value
            End Set
        End Property

        Public Property IdEspecieTMConfiguracion As UInt32
            Get
                Return _IdEspecieTM
            End Get
            Set(value As UInt32)
                _IdEspecieTM = value
            End Set
        End Property

        Public Property IdArticuloCG() As Int64
            Get
                Return _IdArticuloCG
            End Get
            Set(Value As Int64)
                If Not _IdArticuloCG.Equals(Value) Then Modificado = True
                If value = 0 Then Throw New ArgumentException("Debe especificar un valor para esta propiedad", "IdArticuloCG")
                _IdArticuloCG = Value
            End Set
        End Property

        Public Property IdArticuloConfiguracion As Int32
            Get
                Return _IdArticuloCG
            End Get
            Set(value As Int32)
                _IdArticuloCG = value
            End Set
        End Property

        Public Property PesoDesde() As Nullable(Of Double)
            Get
                Return _PesoDesde
            End Get
            Set(Value As Nullable(Of Double))
                If Not _PesoDesde.Equals(Value) Then Modificado = True
                _PesoDesde = Value
            End Set
        End Property

        Public Property PesoHasta() As Nullable(Of Double)
            Get
                Return _PesoHasta
            End Get
            Set(Value As Nullable(Of Double))
                If Not _PesoHasta.Equals(Value) Then Modificado = True
                _PesoHasta = Value
            End Set
        End Property

        Public Property Formato() As String
            Get
                Return _Formato
            End Get
            Set(Value As String)
                If Not _Formato.Equals(Value) Then Modificado = True
                If String.IsNullOrEmpty(value) Then Throw New ArgumentException("Debe especificar un valor para esta propiedad", "Formato")
                _Formato = Value
            End Set
        End Property

        Public Property Modificado() As Boolean
            Get
                Return _Modificado
            End Get
            Set(Value As Boolean)
                Dim Aux As Boolean = _Modificado
                _Modificado = Value
                If value AndAlso Not Aux Then RaiseEvent RegistroModificado(Me)
            End Set
        End Property

        Public Property InstanciaEliminada() As Boolean
            Get
                Return _InstanciaEliminada
            End Get
            Set(Value As Boolean)
                If _InstanciaEliminada <> Value Then Modificado = True
                _InstanciaEliminada = Value
            End Set
        End Property
#End Region
#Region " METODOS PRIVADOS "
        ''' <summary>
        ''' Añade los parámetros necesarios para las consultas SQL
        ''' </summary>
        ''' <param name="Comando">Comando que va a ejecutar la consulta en la BBDD</param>
        Private Sub AnhadirParametros(ByRef Comando As DbCommand)
            If Comando Is Nothing Then Exit Sub

            With Comando
                cConnectionManager.AnhadirParametro(Comando, "Id", _Id)
                cConnectionManager.AnhadirParametro(Comando, "IdEspecieTM", _IdEspecieTM)
                cConnectionManager.AnhadirParametro(Comando, "IdArticuloCG", _IdArticuloCG)
                cConnectionManager.AnhadirParametro(Comando, "Formato", _Formato)

                If PesoDesde.HasValue Then
                    cConnectionManager.AnhadirParametro(Comando, "PesoDesde", _PesoDesde.Value)
                Else
                    cConnectionManager.AnhadirParametro(Comando, "PesoDesde", Nothing)
                End If

                If PesoHasta.HasValue Then
                    cConnectionManager.AnhadirParametro(Comando, "PesoHasta", _PesoHasta.Value)
                Else
                    cConnectionManager.AnhadirParametro(Comando, "PesoHasta", Nothing)
                End If
            End With
        End Sub

        ''' <summary>
        ''' Carga un registro de la base de datos en una instancia de la clase
        ''' </summary>
        ''' <param name="Registro">Registro de la base de datos que queremos cargar</param>
        ''' <param name="Instancia">Instancia de la clase donde vamos a cargar los datos</param>
        Private Shared Sub Cargar(ByRef Registro As DataRow, ByRef Instancia As cAsignacionesProductos)
            ' Compruebo que tenga datos
            If Registro Is Nothing Then Exit Sub
            ' Cargo los datos
            With Registro
                If Not .IsNull("Id") Then Instancia._Id = .Item("Id")
                If Not .IsNull("IdEspecieTM") Then Instancia._IdEspecieTM = .Item("IdEspecieTM")
                If Not .IsNull("IdArticuloCG") Then Instancia._IdArticuloCG = .Item("IdArticuloCG")
                If Not .IsNull("PesoDesde") Then Instancia._PesoDesde = .Item("PesoDesde")
                If Not .IsNull("PesoHasta") Then Instancia._PesoHasta = .Item("PesoHasta")
                If Not .IsNull("Formato") Then Instancia._Formato = .Item("Formato")
                Instancia._Modificado = False
            End With
        End Sub

        ''' <summary>
        ''' Actualiza un registro existente
        ''' </summary>
        Private Function Actualizar() As Boolean
            Dim Comando As DbCommand = cConnectionManager.ObtenerComando(_ConsultaUpdate)

            ' Añado los parámetros
            AnhadirParametros(Comando)

            ' Ejecuto la consulta
            cConnectionManager.EjecutarComandoNonQuery(Comando)

            ' Restablezco el flag de modificacion
            _Modificado = False

            Return True
        End Function

        ''' <summary>
        ''' Actualiza un registro existente
        ''' </summary>
        Private Function Insertar() As Boolean
            Dim Comando As DbCommand = cConnectionManager.ObtenerComando(_ConsultaInsert)

            ' Añado los parámetros
            AnhadirParametros(Comando)

            ' Ejecuto la consulta
            cConnectionManager.EjecutarComandoNonQuery(Comando, True)

            ' Obtengo el Id
            _Id = cConnectionManager.ObtenerId(Comando)

            Comando.Connection.Close()

            ' Restablezco el flag de modificacion
            _Modificado = False

            Return True
        End Function

        ''' <summary>
        ''' Guarda los datos dependientes del registro
        ''' </summary>
        Private Function GuardarDependencias() As Boolean
            Return True
        End Function
#End Region
#Region " METODOS PUBLICOS "
        ''' <summary>
        ''' Elimina el registro seleccionado de la base de datos
        ''' <param name="Registro">Registro que queremos eliminar de la BBDD</param>
        ''' </summary>
        Public Function Eliminar() As Boolean
            If Id > 0 Then
                Dim Comando As DbCommand = cConnectionManager.ObtenerComando(String.Format("DELETE FROM {0} WHERE Id = @Id", _NombreTabla))

                cConnectionManager.AnhadirParametro(Comando, "Id", Id)
                cConnectionManager.EjecutarComandoNonQuery(Comando)
            End If

            InstanciaEliminada = True
            Return True
        End Function

        ''' <summary>
        ''' Obtiene un registro por su ID
        ''' </summary>
        ''' <param name="Id">Identificador del registro que queremos cargar</param>
        Public Function CargarPorId(ByVal Id As Int64) As Boolean
            Dim Comando As DbCommand = cConnectionManager.ObtenerComando(_ConsultaSELECT & " WHERE Id = @Id")
            Dim Datos As New DataTable

            ' Añado los parámetros
            cConnectionManager.AnhadirParametro(Comando, "Id", Id)

            ' Obtengo los datos
            Datos = cConnectionManager.ObtenerDataTable(Comando)

            ' Si tengo datos, entonces los cargo
            If Datos.Rows.Count = 1 Then
                Cargar(Datos.Rows(0), Me)
                Return True
            Else
                Return False
            End If
        End Function

        ''' <summary>
        ''' Guarda los datos del registro en la base de datos
        ''' </summary>
        Public Function Guardar() As Boolean
            ' Si no está modificado, no tengo que hacer nada
            If Not Modificado Then Return True
            If InstanciaEliminada Then Return Eliminar()
            If Not ValidarRegistro(True) Then Return False

            If _Id > 0 Then
                Actualizar()
                Return GuardarDependencias()
            Else
                Insertar()
                Return GuardarDependencias()
            End If
        End Function

        ''' <summary>
        '''  Valida que el registro tenga cubierto los campos marcados como obligatorios
        ''' </summary>
        Public Function ValidarRegistro(Optional ByVal LanzarExcepcion As Boolean = False) As Boolean
            If IdEspecieTM = 0 Then
                If LanzarExcepcion Then Throw New ArgumentException("Debe especificar un valor para esta propiedad", "IdEspecieTM")
                Return False
            End If
            If IdArticuloCG = 0 Then
                If LanzarExcepcion Then Throw New ArgumentException("Debe especificar un valor para esta propiedad", "IdArticuloCG")
                Return False
            End If
            Return True
        End Function

        ''' <summary>
        ''' Obtiene todos los registros de la base de datos
        ''' </summary>
        Public Shared Function ObtenerTodos() As AdvancedList(Of cAsignacionesProductos)
            Dim Resultado As New AdvancedList(Of cAsignacionesProductos)
            Dim Comando As DbCommand = cConnectionManager.ObtenerComando(_ConsultaSELECT)

            ' Ejecuto el comando y devuelvo los resultado
            Dim Datos As DataTable = cConnectionManager.ObtenerDataTable(Comando)

            ' Si no tengo datos, no tengo nada más que hacer
            If Datos Is Nothing OrElse Datos.Rows.Count = 0 Then Return Resultado

            ' Devuelvo los datos
            For Each Fila As DataRow In Datos.Rows
                Dim NuevoRegistro As New cAsignacionesProductos
                Cargar(Fila, NuevoRegistro)
                Resultado.Add(NuevoRegistro)
            Next

            Return Resultado
        End Function

        ''' <summary>
        ''' Realiza la búsqueda de registro según los criterios establecidos
        ''' </summary>
        Public Shared Function Buscar(ByVal CualquierCampo As String) As AdvancedList(Of cAsignacionesProductos)
            Dim Resultado As New AdvancedList(Of cAsignacionesProductos)
            Dim Consulta As String = _ConsultaSELECT & " WHERE " & _
            "({0}.Formato LIKE @Otros)"

            ' Creo el comando y añado los parámetros
            Dim Comando As DbCommand = cConnectionManager.ObtenerComando(String.Format(Consulta, _NombreTabla))
            cConnectionManager.AnhadirParametro(Comando, "Otros", "%" & CualquierCampo & "%")

            ' Ejecuto el comando y devuelvo los resultado
            Dim Datos As DataTable = cConnectionManager.ObtenerDataTable(Comando)

            ' Si no tengo datos, no tengo nada más que hacer
            If Datos Is Nothing OrElse Datos.Rows.Count = 0 Then Return Resultado

            For Each Fila As DataRow In Datos.Rows
                Dim NuevoRegistro As New cAsignacionesProductos
                Cargar(Fila, NuevoRegistro)
                Resultado.Add(NuevoRegistro)
            Next

            Return Resultado
        End Function
#End Region
#Region " SOBRECARGAS "
        ''' <summary>
        ''' Especificamos los comparadores de igualdad
        ''' </summary>
        Public Overrides Function Equals(ByVal obj As Object) As Boolean
            If obj Is Nothing Then Return False
            If Not TypeOf (obj) Is cAsignacionesProductos Then Return False
            With DirectCast(obj, cAsignacionesProductos)
                If Me._Id <> ._Id Then Return False
                If Me._IdEspecieTM <> ._IdEspecieTM Then Return False
                If Me._IdArticuloCG <> ._IdArticuloCG Then Return False
                If Me._PesoDesde <> ._PesoDesde Then Return False
                If Me._PesoHasta <> ._PesoHasta Then Return False
                If Me._Formato <> ._Formato Then Return False
                If Me._Modificado <> ._Modificado Then Return False
                If Me._InstanciaEliminada <> ._InstanciaEliminada Then Return False
            End With
            Return True
        End Function

        Public Shared Operator =(ByVal Objeto1 As cAsignacionesProductos, ByVal Objeto2 As cAsignacionesProductos) As Boolean
            If ReferenceEquals(Objeto1, Objeto2) Then Return True
            If Objeto1 Is Nothing AndAlso Objeto2 IsNot Nothing Then Return False
            If Objeto2 Is Nothing AndAlso Objeto1 IsNot Nothing Then Return False
            If Objeto1 Is Nothing AndAlso Objeto2 Is Nothing Then Return True
            Return Objeto1.Equals(Objeto2)
        End Operator

        Public Shared Operator <>(ByVal Objeto1 As cAsignacionesProductos, ByVal Objeto2 As cAsignacionesProductos) As Boolean
            Return Not Objeto1 = Objeto2
        End Operator

        ''' <summary>
        ''' Crea una copia en un objeto nuevo
        ''' </summary>
        Public Function Clone() As Object Implements System.ICloneable.Clone
            Return MemberwiseClone()
        End Function
#End Region
    End Class
End Namespace
