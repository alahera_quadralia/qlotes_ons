﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConfiguracion
    Inherits ComponentFactory.Krypton.Toolkit.KryptonForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmConfiguracion))
        Me.btCerrar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.dgvAsignacionesProductos = New ComponentFactory.Krypton.Toolkit.KryptonDataGridView()
        Me.colEspecieTM = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colPesoDesde = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colPesoHasta = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colFormato = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.colArticulo = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.btnAceptar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.tblConfiguracionInicial = New System.Windows.Forms.TableLayoutPanel()
        Me.lblCarpetaClassicGest = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblSegundosComprobacion = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblBBDDBaseDatos = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.nudSegundosComprobacion = New ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown()
        Me.lblBBDDPuerto = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblBBDDUsuario = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblBBDDClave = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.nudBBDDPuerto = New ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown()
        Me.lblBBDDHost = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.btnVerificarConexion = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.tblAsignaciones = New System.Windows.Forms.TableLayoutPanel()
        Me.lblAsignacionProductos = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblAsignacionTienda = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblAsignacionAlmacen = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.lblAsignacionProveedor = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.tabGeneral = New ComponentFactory.Krypton.Navigator.KryptonNavigator()
        Me.tbpGeneral = New ComponentFactory.Krypton.Navigator.KryptonPage()
        Me.tbpAsignaciones = New ComponentFactory.Krypton.Navigator.KryptonPage()
        Me.txtCarpetaClassicGest = New Monitor_Sincronizacion.Quadralia.Controles.aTextBox()
        Me.btnBuscarCarpeta = New ComponentFactory.Krypton.Toolkit.ButtonSpecAny()
        Me.txtBBDDBaseDatos = New Monitor_Sincronizacion.Quadralia.Controles.aTextBox()
        Me.txtBBDDUsuario = New Monitor_Sincronizacion.Quadralia.Controles.aTextBox()
        Me.txtBBDDClave = New Monitor_Sincronizacion.Quadralia.Controles.aTextBox()
        Me.txtBBDDHost = New Monitor_Sincronizacion.Quadralia.Controles.aTextBox()
        Me.txtLogConexion = New Monitor_Sincronizacion.Quadralia.Controles.aTextBox()
        Me.cboAsignacionTienda = New Monitor_Sincronizacion.Quadralia.Controles.aComboBox()
        Me.cboAsignacionProveedor = New Monitor_Sincronizacion.Quadralia.Controles.aComboBox()
        Me.cboAsignacionAlmacen = New Monitor_Sincronizacion.Quadralia.Controles.aComboBox()
        CType(Me.dgvAsignacionesProductos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tblConfiguracionInicial.SuspendLayout()
        Me.tblAsignaciones.SuspendLayout()
        CType(Me.tabGeneral, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabGeneral.SuspendLayout()
        CType(Me.tbpGeneral, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpGeneral.SuspendLayout()
        CType(Me.tbpAsignaciones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpAsignaciones.SuspendLayout()
        CType(Me.cboAsignacionTienda, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboAsignacionProveedor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboAsignacionAlmacen, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btCerrar
        '
        Me.btCerrar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btCerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btCerrar.Location = New System.Drawing.Point(414, 274)
        Me.btCerrar.Name = "btCerrar"
        Me.btCerrar.Size = New System.Drawing.Size(75, 27)
        Me.btCerrar.TabIndex = 2
        Me.btCerrar.Values.Text = "Cerrar"
        '
        'dgvAsignacionesProductos
        '
        Me.dgvAsignacionesProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAsignacionesProductos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colEspecieTM, Me.colPesoDesde, Me.colPesoHasta, Me.colFormato, Me.colArticulo})
        Me.tblAsignaciones.SetColumnSpan(Me.dgvAsignacionesProductos, 4)
        Me.dgvAsignacionesProductos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvAsignacionesProductos.Location = New System.Drawing.Point(3, 103)
        Me.dgvAsignacionesProductos.Name = "dgvAsignacionesProductos"
        Me.dgvAsignacionesProductos.RowHeadersVisible = False
        Me.dgvAsignacionesProductos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAsignacionesProductos.Size = New System.Drawing.Size(497, 135)
        Me.dgvAsignacionesProductos.TabIndex = 5
        '
        'colEspecieTM
        '
        Me.colEspecieTM.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colEspecieTM.DataPropertyName = "IdEspecieTMConfiguracion"
        Me.colEspecieTM.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.[Nothing]
        Me.colEspecieTM.HeaderText = "Especie"
        Me.colEspecieTM.Name = "colEspecieTM"
        Me.colEspecieTM.Width = 56
        '
        'colPesoDesde
        '
        Me.colPesoDesde.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colPesoDesde.DataPropertyName = "PesoDesde"
        Me.colPesoDesde.HeaderText = "Peso Desde"
        Me.colPesoDesde.Name = "colPesoDesde"
        Me.colPesoDesde.Width = 96
        '
        'colPesoHasta
        '
        Me.colPesoHasta.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colPesoHasta.DataPropertyName = "PesoHasta"
        Me.colPesoHasta.HeaderText = "Peso Hasta"
        Me.colPesoHasta.Name = "colPesoHasta"
        Me.colPesoHasta.Width = 94
        '
        'colFormato
        '
        Me.colFormato.DataPropertyName = "Formato"
        Me.colFormato.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.[Nothing]
        Me.colFormato.HeaderText = "Formato"
        Me.colFormato.Name = "colFormato"
        '
        'colArticulo
        '
        Me.colArticulo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colArticulo.DataPropertyName = "IdArticuloConfiguracion"
        Me.colArticulo.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.[Nothing]
        Me.colArticulo.HeaderText = "Artículo"
        Me.colArticulo.Name = "colArticulo"
        '
        'btnAceptar
        '
        Me.btnAceptar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAceptar.Location = New System.Drawing.Point(333, 274)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 27)
        Me.btnAceptar.TabIndex = 1
        Me.btnAceptar.Values.Text = "Aceptar"
        '
        'tblConfiguracionInicial
        '
        Me.tblConfiguracionInicial.BackColor = System.Drawing.Color.Transparent
        Me.tblConfiguracionInicial.ColumnCount = 4
        Me.tblConfiguracionInicial.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblConfiguracionInicial.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblConfiguracionInicial.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblConfiguracionInicial.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblConfiguracionInicial.Controls.Add(Me.lblCarpetaClassicGest, 0, 0)
        Me.tblConfiguracionInicial.Controls.Add(Me.txtCarpetaClassicGest, 1, 0)
        Me.tblConfiguracionInicial.Controls.Add(Me.lblSegundosComprobacion, 2, 0)
        Me.tblConfiguracionInicial.Controls.Add(Me.lblBBDDBaseDatos, 0, 3)
        Me.tblConfiguracionInicial.Controls.Add(Me.txtBBDDBaseDatos, 1, 3)
        Me.tblConfiguracionInicial.Controls.Add(Me.nudSegundosComprobacion, 3, 0)
        Me.tblConfiguracionInicial.Controls.Add(Me.lblBBDDPuerto, 2, 3)
        Me.tblConfiguracionInicial.Controls.Add(Me.lblBBDDUsuario, 0, 4)
        Me.tblConfiguracionInicial.Controls.Add(Me.txtBBDDUsuario, 1, 4)
        Me.tblConfiguracionInicial.Controls.Add(Me.lblBBDDClave, 2, 4)
        Me.tblConfiguracionInicial.Controls.Add(Me.txtBBDDClave, 3, 4)
        Me.tblConfiguracionInicial.Controls.Add(Me.nudBBDDPuerto, 3, 3)
        Me.tblConfiguracionInicial.Controls.Add(Me.lblBBDDHost, 0, 2)
        Me.tblConfiguracionInicial.Controls.Add(Me.txtBBDDHost, 1, 2)
        Me.tblConfiguracionInicial.Controls.Add(Me.txtLogConexion, 0, 6)
        Me.tblConfiguracionInicial.Controls.Add(Me.btnVerificarConexion, 2, 5)
        Me.tblConfiguracionInicial.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblConfiguracionInicial.Location = New System.Drawing.Point(0, 0)
        Me.tblConfiguracionInicial.Name = "tblConfiguracionInicial"
        Me.tblConfiguracionInicial.RowCount = 7
        Me.tblConfiguracionInicial.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblConfiguracionInicial.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblConfiguracionInicial.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblConfiguracionInicial.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblConfiguracionInicial.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblConfiguracionInicial.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblConfiguracionInicial.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblConfiguracionInicial.Size = New System.Drawing.Size(503, 241)
        Me.tblConfiguracionInicial.TabIndex = 0
        '
        'lblCarpetaClassicGest
        '
        Me.lblCarpetaClassicGest.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblCarpetaClassicGest.Location = New System.Drawing.Point(3, 3)
        Me.lblCarpetaClassicGest.Name = "lblCarpetaClassicGest"
        Me.lblCarpetaClassicGest.Size = New System.Drawing.Size(101, 24)
        Me.lblCarpetaClassicGest.TabIndex = 0
        Me.lblCarpetaClassicGest.Values.Text = "Datos Classicges"
        '
        'lblSegundosComprobacion
        '
        Me.lblSegundosComprobacion.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblSegundosComprobacion.Location = New System.Drawing.Point(230, 3)
        Me.lblSegundosComprobacion.Name = "lblSegundosComprobacion"
        Me.lblSegundosComprobacion.Size = New System.Drawing.Size(149, 24)
        Me.lblSegundosComprobacion.TabIndex = 3
        Me.lblSegundosComprobacion.Values.Text = "Segundos Comprobación"
        '
        'lblBBDDBaseDatos
        '
        Me.lblBBDDBaseDatos.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblBBDDBaseDatos.Location = New System.Drawing.Point(3, 79)
        Me.lblBBDDBaseDatos.Name = "lblBBDDBaseDatos"
        Me.lblBBDDBaseDatos.Size = New System.Drawing.Size(71, 22)
        Me.lblBBDDBaseDatos.TabIndex = 7
        Me.lblBBDDBaseDatos.Values.Text = "Base Datos"
        '
        'nudSegundosComprobacion
        '
        Me.nudSegundosComprobacion.Location = New System.Drawing.Point(385, 3)
        Me.nudSegundosComprobacion.Maximum = New Decimal(New Integer() {90000, 0, 0, 0})
        Me.nudSegundosComprobacion.Name = "nudSegundosComprobacion"
        Me.nudSegundosComprobacion.Size = New System.Drawing.Size(80, 22)
        Me.nudSegundosComprobacion.TabIndex = 4
        '
        'lblBBDDPuerto
        '
        Me.lblBBDDPuerto.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblBBDDPuerto.Location = New System.Drawing.Point(230, 79)
        Me.lblBBDDPuerto.Name = "lblBBDDPuerto"
        Me.lblBBDDPuerto.Size = New System.Drawing.Size(47, 22)
        Me.lblBBDDPuerto.TabIndex = 9
        Me.lblBBDDPuerto.Values.Text = "Puerto"
        '
        'lblBBDDUsuario
        '
        Me.lblBBDDUsuario.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblBBDDUsuario.Location = New System.Drawing.Point(3, 107)
        Me.lblBBDDUsuario.Name = "lblBBDDUsuario"
        Me.lblBBDDUsuario.Size = New System.Drawing.Size(52, 20)
        Me.lblBBDDUsuario.TabIndex = 11
        Me.lblBBDDUsuario.Values.Text = "Usuario"
        '
        'lblBBDDClave
        '
        Me.lblBBDDClave.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblBBDDClave.Location = New System.Drawing.Point(230, 107)
        Me.lblBBDDClave.Name = "lblBBDDClave"
        Me.lblBBDDClave.Size = New System.Drawing.Size(40, 20)
        Me.lblBBDDClave.TabIndex = 13
        Me.lblBBDDClave.Values.Text = "Clave"
        '
        'nudBBDDPuerto
        '
        Me.nudBBDDPuerto.Location = New System.Drawing.Point(385, 79)
        Me.nudBBDDPuerto.Maximum = New Decimal(New Integer() {65000, 0, 0, 0})
        Me.nudBBDDPuerto.Name = "nudBBDDPuerto"
        Me.nudBBDDPuerto.Size = New System.Drawing.Size(80, 22)
        Me.nudBBDDPuerto.TabIndex = 10
        '
        'lblBBDDHost
        '
        Me.lblBBDDHost.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblBBDDHost.Location = New System.Drawing.Point(3, 53)
        Me.lblBBDDHost.Name = "lblBBDDHost"
        Me.lblBBDDHost.Size = New System.Drawing.Size(96, 20)
        Me.lblBBDDHost.TabIndex = 5
        Me.lblBBDDHost.Values.Text = "Host Trazamare"
        '
        'btnVerificarConexion
        '
        Me.tblConfiguracionInicial.SetColumnSpan(Me.btnVerificarConexion, 2)
        Me.btnVerificarConexion.Dock = System.Windows.Forms.DockStyle.Fill
        Me.btnVerificarConexion.Location = New System.Drawing.Point(230, 133)
        Me.btnVerificarConexion.Name = "btnVerificarConexion"
        Me.btnVerificarConexion.Size = New System.Drawing.Size(270, 25)
        Me.btnVerificarConexion.TabIndex = 15
        Me.btnVerificarConexion.Values.Text = "Comprobar Conexión"
        '
        'tblAsignaciones
        '
        Me.tblAsignaciones.BackColor = System.Drawing.Color.Transparent
        Me.tblAsignaciones.ColumnCount = 4
        Me.tblAsignaciones.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblAsignaciones.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblAsignaciones.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblAsignaciones.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblAsignaciones.Controls.Add(Me.dgvAsignacionesProductos, 0, 4)
        Me.tblAsignaciones.Controls.Add(Me.lblAsignacionProductos, 0, 3)
        Me.tblAsignaciones.Controls.Add(Me.lblAsignacionTienda, 0, 0)
        Me.tblAsignaciones.Controls.Add(Me.cboAsignacionTienda, 1, 0)
        Me.tblAsignaciones.Controls.Add(Me.lblAsignacionAlmacen, 2, 0)
        Me.tblAsignaciones.Controls.Add(Me.lblAsignacionProveedor, 0, 1)
        Me.tblAsignaciones.Controls.Add(Me.cboAsignacionProveedor, 1, 1)
        Me.tblAsignaciones.Controls.Add(Me.cboAsignacionAlmacen, 3, 0)
        Me.tblAsignaciones.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblAsignaciones.Location = New System.Drawing.Point(0, 0)
        Me.tblAsignaciones.Name = "tblAsignaciones"
        Me.tblAsignaciones.RowCount = 5
        Me.tblAsignaciones.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblAsignaciones.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblAsignaciones.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblAsignaciones.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblAsignaciones.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblAsignaciones.Size = New System.Drawing.Size(503, 241)
        Me.tblAsignaciones.TabIndex = 6
        '
        'lblAsignacionProductos
        '
        Me.lblAsignacionProductos.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblAsignacionProductos.Location = New System.Drawing.Point(3, 77)
        Me.lblAsignacionProductos.Name = "lblAsignacionProductos"
        Me.lblAsignacionProductos.Size = New System.Drawing.Size(66, 20)
        Me.lblAsignacionProductos.TabIndex = 6
        Me.lblAsignacionProductos.Values.Text = "Productos"
        '
        'lblAsignacionTienda
        '
        Me.lblAsignacionTienda.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblAsignacionTienda.Location = New System.Drawing.Point(3, 3)
        Me.lblAsignacionTienda.Name = "lblAsignacionTienda"
        Me.lblAsignacionTienda.Size = New System.Drawing.Size(47, 21)
        Me.lblAsignacionTienda.TabIndex = 6
        Me.lblAsignacionTienda.Values.Text = "Tienda"
        '
        'lblAsignacionAlmacen
        '
        Me.lblAsignacionAlmacen.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblAsignacionAlmacen.Location = New System.Drawing.Point(259, 3)
        Me.lblAsignacionAlmacen.Name = "lblAsignacionAlmacen"
        Me.lblAsignacionAlmacen.Size = New System.Drawing.Size(58, 21)
        Me.lblAsignacionAlmacen.TabIndex = 6
        Me.lblAsignacionAlmacen.Values.Text = "Almacén"
        '
        'lblAsignacionProveedor
        '
        Me.lblAsignacionProveedor.Dock = System.Windows.Forms.DockStyle.Left
        Me.lblAsignacionProveedor.Location = New System.Drawing.Point(3, 30)
        Me.lblAsignacionProveedor.Name = "lblAsignacionProveedor"
        Me.lblAsignacionProveedor.Size = New System.Drawing.Size(67, 21)
        Me.lblAsignacionProveedor.TabIndex = 6
        Me.lblAsignacionProveedor.Values.Text = "Proveedor"
        '
        'tabGeneral
        '
        Me.tabGeneral.AllowPageReorder = False
        Me.tabGeneral.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tabGeneral.Button.ButtonDisplayLogic = ComponentFactory.Krypton.Navigator.ButtonDisplayLogic.None
        Me.tabGeneral.Button.CloseButtonAction = ComponentFactory.Krypton.Navigator.CloseButtonAction.None
        Me.tabGeneral.Button.CloseButtonDisplay = ComponentFactory.Krypton.Navigator.ButtonDisplay.Hide
        Me.tabGeneral.Location = New System.Drawing.Point(0, 0)
        Me.tabGeneral.Name = "tabGeneral"
        Me.tabGeneral.Pages.AddRange(New ComponentFactory.Krypton.Navigator.KryptonPage() {Me.tbpGeneral, Me.tbpAsignaciones})
        Me.tabGeneral.SelectedIndex = 0
        Me.tabGeneral.Size = New System.Drawing.Size(505, 268)
        Me.tabGeneral.TabIndex = 3
        Me.tabGeneral.Text = "KryptonNavigator1"
        '
        'tbpGeneral
        '
        Me.tbpGeneral.AutoHiddenSlideSize = New System.Drawing.Size(200, 200)
        Me.tbpGeneral.Controls.Add(Me.tblConfiguracionInicial)
        Me.tbpGeneral.Flags = 65534
        Me.tbpGeneral.LastVisibleSet = True
        Me.tbpGeneral.MinimumSize = New System.Drawing.Size(50, 50)
        Me.tbpGeneral.Name = "tbpGeneral"
        Me.tbpGeneral.Size = New System.Drawing.Size(503, 241)
        Me.tbpGeneral.Text = "General"
        Me.tbpGeneral.ToolTipTitle = "Page ToolTip"
        Me.tbpGeneral.UniqueName = "FA8FE18EAE984DC1718219386CF33B74"
        '
        'tbpAsignaciones
        '
        Me.tbpAsignaciones.AutoHiddenSlideSize = New System.Drawing.Size(200, 200)
        Me.tbpAsignaciones.Controls.Add(Me.tblAsignaciones)
        Me.tbpAsignaciones.Flags = 65534
        Me.tbpAsignaciones.LastVisibleSet = True
        Me.tbpAsignaciones.MinimumSize = New System.Drawing.Size(50, 50)
        Me.tbpAsignaciones.Name = "tbpAsignaciones"
        Me.tbpAsignaciones.Size = New System.Drawing.Size(503, 241)
        Me.tbpAsignaciones.Text = "Asignaciones"
        Me.tbpAsignaciones.ToolTipTitle = "Page ToolTip"
        Me.tbpAsignaciones.UniqueName = "70BFB8585C624E27F9822E3471DEC0E2"
        '
        'txtCarpetaClassicGest
        '
        Me.txtCarpetaClassicGest.ButtonSpecs.AddRange(New ComponentFactory.Krypton.Toolkit.ButtonSpecAny() {Me.btnBuscarCarpeta})
        Me.txtCarpetaClassicGest.controlarBotonBorrar = True
        Me.txtCarpetaClassicGest.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtCarpetaClassicGest.Formato = ""
        Me.txtCarpetaClassicGest.Location = New System.Drawing.Point(110, 3)
        Me.txtCarpetaClassicGest.mostrarSiempreBotonBorrar = False
        Me.txtCarpetaClassicGest.Name = "txtCarpetaClassicGest"
        Me.txtCarpetaClassicGest.seleccionarTodo = True
        Me.txtCarpetaClassicGest.Size = New System.Drawing.Size(114, 24)
        Me.txtCarpetaClassicGest.TabIndex = 1
        '
        'btnBuscarCarpeta
        '
        Me.btnBuscarCarpeta.Image = Global.Monitor_Sincronizacion.My.Resources.Resources.Buscar_16
        Me.btnBuscarCarpeta.UniqueName = "92E55CC7C0BB4B564BBD7C372793E397"
        '
        'txtBBDDBaseDatos
        '
        Me.txtBBDDBaseDatos.controlarBotonBorrar = True
        Me.txtBBDDBaseDatos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtBBDDBaseDatos.Formato = ""
        Me.txtBBDDBaseDatos.Location = New System.Drawing.Point(110, 79)
        Me.txtBBDDBaseDatos.mostrarSiempreBotonBorrar = False
        Me.txtBBDDBaseDatos.Name = "txtBBDDBaseDatos"
        Me.txtBBDDBaseDatos.seleccionarTodo = True
        Me.txtBBDDBaseDatos.Size = New System.Drawing.Size(114, 20)
        Me.txtBBDDBaseDatos.TabIndex = 8
        '
        'txtBBDDUsuario
        '
        Me.txtBBDDUsuario.controlarBotonBorrar = True
        Me.txtBBDDUsuario.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtBBDDUsuario.Formato = ""
        Me.txtBBDDUsuario.Location = New System.Drawing.Point(110, 107)
        Me.txtBBDDUsuario.mostrarSiempreBotonBorrar = False
        Me.txtBBDDUsuario.Name = "txtBBDDUsuario"
        Me.txtBBDDUsuario.seleccionarTodo = True
        Me.txtBBDDUsuario.Size = New System.Drawing.Size(114, 20)
        Me.txtBBDDUsuario.TabIndex = 12
        '
        'txtBBDDClave
        '
        Me.txtBBDDClave.controlarBotonBorrar = True
        Me.txtBBDDClave.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtBBDDClave.Formato = ""
        Me.txtBBDDClave.Location = New System.Drawing.Point(385, 107)
        Me.txtBBDDClave.mostrarSiempreBotonBorrar = False
        Me.txtBBDDClave.Name = "txtBBDDClave"
        Me.txtBBDDClave.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtBBDDClave.seleccionarTodo = True
        Me.txtBBDDClave.Size = New System.Drawing.Size(115, 20)
        Me.txtBBDDClave.TabIndex = 14
        Me.txtBBDDClave.UseSystemPasswordChar = True
        '
        'txtBBDDHost
        '
        Me.tblConfiguracionInicial.SetColumnSpan(Me.txtBBDDHost, 3)
        Me.txtBBDDHost.controlarBotonBorrar = True
        Me.txtBBDDHost.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtBBDDHost.Formato = ""
        Me.txtBBDDHost.Location = New System.Drawing.Point(110, 53)
        Me.txtBBDDHost.mostrarSiempreBotonBorrar = False
        Me.txtBBDDHost.Name = "txtBBDDHost"
        Me.txtBBDDHost.seleccionarTodo = True
        Me.txtBBDDHost.Size = New System.Drawing.Size(390, 20)
        Me.txtBBDDHost.TabIndex = 6
        '
        'txtLogConexion
        '
        Me.tblConfiguracionInicial.SetColumnSpan(Me.txtLogConexion, 4)
        Me.txtLogConexion.controlarBotonBorrar = True
        Me.txtLogConexion.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtLogConexion.Enabled = False
        Me.txtLogConexion.Formato = ""
        Me.txtLogConexion.Location = New System.Drawing.Point(3, 164)
        Me.txtLogConexion.mostrarSiempreBotonBorrar = False
        Me.txtLogConexion.Multiline = True
        Me.txtLogConexion.Name = "txtLogConexion"
        Me.txtLogConexion.seleccionarTodo = True
        Me.txtLogConexion.Size = New System.Drawing.Size(497, 74)
        Me.txtLogConexion.TabIndex = 16
        '
        'cboAsignacionTienda
        '
        Me.cboAsignacionTienda.controlarBotonBorrar = True
        Me.cboAsignacionTienda.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cboAsignacionTienda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAsignacionTienda.DropDownWidth = 159
        Me.cboAsignacionTienda.FormattingEnabled = True
        Me.cboAsignacionTienda.Location = New System.Drawing.Point(76, 3)
        Me.cboAsignacionTienda.mostrarSiempreBotonBorrar = False
        Me.cboAsignacionTienda.Name = "cboAsignacionTienda"
        Me.cboAsignacionTienda.Size = New System.Drawing.Size(177, 21)
        Me.cboAsignacionTienda.TabIndex = 7
        '
        'cboAsignacionProveedor
        '
        Me.tblAsignaciones.SetColumnSpan(Me.cboAsignacionProveedor, 3)
        Me.cboAsignacionProveedor.controlarBotonBorrar = True
        Me.cboAsignacionProveedor.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cboAsignacionProveedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAsignacionProveedor.DropDownWidth = 388
        Me.cboAsignacionProveedor.FormattingEnabled = True
        Me.cboAsignacionProveedor.Location = New System.Drawing.Point(76, 30)
        Me.cboAsignacionProveedor.mostrarSiempreBotonBorrar = False
        Me.cboAsignacionProveedor.Name = "cboAsignacionProveedor"
        Me.cboAsignacionProveedor.Size = New System.Drawing.Size(424, 21)
        Me.cboAsignacionProveedor.TabIndex = 7
        '
        'cboAsignacionAlmacen
        '
        Me.cboAsignacionAlmacen.controlarBotonBorrar = True
        Me.cboAsignacionAlmacen.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cboAsignacionAlmacen.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAsignacionAlmacen.DropDownWidth = 159
        Me.cboAsignacionAlmacen.FormattingEnabled = True
        Me.cboAsignacionAlmacen.Location = New System.Drawing.Point(323, 3)
        Me.cboAsignacionAlmacen.mostrarSiempreBotonBorrar = False
        Me.cboAsignacionAlmacen.Name = "cboAsignacionAlmacen"
        Me.cboAsignacionAlmacen.Size = New System.Drawing.Size(177, 21)
        Me.cboAsignacionAlmacen.TabIndex = 7
        '
        'frmConfiguracion
        '
        Me.AcceptButton = Me.btnAceptar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btCerrar
        Me.ClientSize = New System.Drawing.Size(505, 313)
        Me.Controls.Add(Me.tabGeneral)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.btCerrar)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmConfiguracion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Monitor Sincronización"
        CType(Me.dgvAsignacionesProductos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tblConfiguracionInicial.ResumeLayout(False)
        Me.tblConfiguracionInicial.PerformLayout()
        Me.tblAsignaciones.ResumeLayout(False)
        Me.tblAsignaciones.PerformLayout()
        CType(Me.tabGeneral, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabGeneral.ResumeLayout(False)
        CType(Me.tbpGeneral, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpGeneral.ResumeLayout(False)
        CType(Me.tbpAsignaciones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpAsignaciones.ResumeLayout(False)
        CType(Me.cboAsignacionTienda, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboAsignacionProveedor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboAsignacionAlmacen, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btCerrar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents dgvAsignacionesProductos As ComponentFactory.Krypton.Toolkit.KryptonDataGridView
    Friend WithEvents btnAceptar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents tblConfiguracionInicial As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblCarpetaClassicGest As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtCarpetaClassicGest As Monitor_Sincronizacion.Quadralia.Controles.aTextBox
    Friend WithEvents lblSegundosComprobacion As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblBBDDBaseDatos As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtBBDDBaseDatos As Monitor_Sincronizacion.Quadralia.Controles.aTextBox
    Friend WithEvents nudSegundosComprobacion As ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown
    Friend WithEvents lblBBDDPuerto As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblBBDDUsuario As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtBBDDUsuario As Monitor_Sincronizacion.Quadralia.Controles.aTextBox
    Friend WithEvents lblBBDDClave As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtBBDDClave As Monitor_Sincronizacion.Quadralia.Controles.aTextBox
    Friend WithEvents nudBBDDPuerto As ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown
    Friend WithEvents lblBBDDHost As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents txtBBDDHost As Monitor_Sincronizacion.Quadralia.Controles.aTextBox
    Friend WithEvents txtLogConexion As Monitor_Sincronizacion.Quadralia.Controles.aTextBox
    Friend WithEvents btnVerificarConexion As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents colEspecieTM As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colPesoDesde As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents colPesoHasta As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents colFormato As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colArticulo As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents tblAsignaciones As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblAsignacionProductos As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblAsignacionTienda As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents cboAsignacionTienda As Monitor_Sincronizacion.Quadralia.Controles.aComboBox
    Friend WithEvents lblAsignacionAlmacen As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents lblAsignacionProveedor As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents cboAsignacionProveedor As Monitor_Sincronizacion.Quadralia.Controles.aComboBox
    Friend WithEvents cboAsignacionAlmacen As Monitor_Sincronizacion.Quadralia.Controles.aComboBox
    Friend WithEvents tabGeneral As ComponentFactory.Krypton.Navigator.KryptonNavigator
    Friend WithEvents tbpGeneral As ComponentFactory.Krypton.Navigator.KryptonPage
    Friend WithEvents tbpAsignaciones As ComponentFactory.Krypton.Navigator.KryptonPage
    Friend WithEvents btnBuscarCarpeta As ComponentFactory.Krypton.Toolkit.ButtonSpecAny

End Class
