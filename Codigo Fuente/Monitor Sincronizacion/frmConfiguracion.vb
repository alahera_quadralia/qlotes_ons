﻿Public Class frmConfiguracion
#Region " LIMPIEZA "
    ''' <summary>
    ''' Limpia todos los controles del formulario
    ''' </summary>
    Private Sub LimpiarTodo()
        txtCarpetaClassicGest.Clear()
        nudSegundosComprobacion.Value = 300

        txtBBDDHost.Clear()
        txtBBDDBaseDatos.Clear()
        nudBBDDPuerto.Value = 3306
        txtBBDDUsuario.Clear()
        txtBBDDClave.Clear()

        txtLogConexion.Clear()

        dgvAsignacionesProductos.DataSource = Nothing
        dgvAsignacionesProductos.Rows.Clear()
    End Sub
#End Region
#Region " RUTINA DE INICIO/CIERRE "
    ''' <summary>
    ''' Rutina de Inicio del formulario
    ''' </summary>
    Private Sub Inicio(sender As Object, e As EventArgs) Handles MyBase.Load
        ' No quiero nuevas columnas
        dgvAsignacionesProductos.AutoGenerateColumns = False

        ' Cargo los datos maestros
        CargarDatosMaestros()

        ' Limpio el formulario
        LimpiarTodo()
        tabGeneral.SelectedIndex = 0

        ' Cargo los datos
        CargarDatos()
    End Sub

    ''' <summary>
    ''' Carga los datos maestros del formulario
    ''' </summary>
    Private Sub CargarDatosMaestros()
        Dim CadenaConexion As String = String.Format("Server={0};Port={1};Database={2};Uid={3};Pwd={4};", cConfiguracionBBDD.Instancia.BBDD_Host, cConfiguracionBBDD.Instancia.BBDD_Puerto, cConfiguracionBBDD.Instancia.BBDD_BaseDatos, cConfiguracionBBDD.Instancia.BBDD_Usuario, cConfiguracionBBDD.Instancia.BBDD_Pass)
        Dim Conexion As New MySql.Data.MySqlClient.MySqlConnection(CadenaConexion)
        Dim Adaptador As MySql.Data.MySqlClient.MySqlDataAdapter = Nothing
        Dim dtEspecie As New DataTable
        Dim dtFormatos As New DataTable

        Try
            Conexion.Open()
            Adaptador = New MySql.Data.MySqlClient.MySqlDataAdapter("SELECT * FROM `especies` ORDER BY `especies`.`Nome`", Conexion)
            Adaptador.Fill(dtEspecie)

            With colEspecieTM
                .DisplayMember = "Nome"
                .ValueMember = "Id"
                .DataSource = dtEspecie
            End With

            Adaptador = New MySql.Data.MySqlClient.MySqlDataAdapter("SELECT DISTINCT Formato FROM etiqueta ORDER BY Formato", Conexion)
            Adaptador.Fill(dtFormatos)

            With colFormato
                .DisplayMember = "Formato"
                .ValueMember = "Formato"
                .DataSource = dtFormatos
            End With
        Catch ex As Exception
        Finally
            Conexion.Close()
        End Try

        With colArticulo
            .DataSource = Nothing
            .Items.Clear()

            .DisplayMember = "Valor"
            .ValueMember = "Id"
            .DataSource = ObtenerDatosMaestros("SELECT claart AS Id, TRIM(nombre) + ' [' + TRIM(Codigo) + ']' AS Valor FROM articulo ORDER BY nombre, Codigo")
        End With

        With cboAsignacionAlmacen
            .DataSource = Nothing
            .Items.Clear()

            .DisplayMember = "Valor"
            .ValueMember = "Id"
            .DataSource = ObtenerDatosMaestros("SELECT claalm AS Id, TRIM(nomalm) AS Valor FROM almacen ORDER BY nomalm")

            .SelectedIndex = -1
        End With

        With cboAsignacionProveedor
            .DataSource = Nothing
            .Items.Clear()

            .DisplayMember = "Valor"
            .ValueMember = "Id"
            .DataSource = ObtenerDatosMaestros("SELECT clapro AS Id, TRIM(nombre) + ' [' + TRIM(Codigo) + ']' AS Valor FROM proveedo ORDER BY nombre, Codigo")

            .SelectedIndex = -1
        End With

        With cboAsignacionTienda
            .DataSource = Nothing
            .Items.Clear()

            .DisplayMember = "Valor"
            .ValueMember = "Id"
            .DataSource = ObtenerDatosMaestros("SELECT claemp AS Id, TRIM(nomemp) AS Valor FROM empresas ORDER BY nomemp")

            .SelectedIndex = -1
        End With
    End Sub

    ''' <summary>
    ''' Ejecuta la consulta deseada en la base de datos de classicgest y devuelve el resultado
    ''' </summary>
    Private Function ObtenerDatosMaestros(Consulta As String) As DataTable
        Dim CadenaConexionCG = String.Format("Provider=vfpoledb;Data Source={0};Collating Sequence=general;", cConfiguracionBBDD.Instancia.RutaCarpeta)
        Dim ConexionCG As New OleDb.OleDbConnection(CadenaConexionCG)
        Try
            Dim Adaptador As New OleDb.OleDbDataAdapter(Consulta, ConexionCG)
            Dim dtResultado As New DataTable

            ConexionCG.Open()
            Adaptador.Fill(dtResultado)

            Return dtResultado
        Catch ex As Exception
            Return New DataTable
        Finally
            ConexionCG.Close()
        End Try
    End Function

    ''' <summary>
    ''' Cierra el formulario
    ''' </summary>
    Private Sub Cerrar(sender As Object, e As EventArgs) Handles btCerrar.Click
        Me.Close()
    End Sub
#End Region
#Region " CARGA/EDICION DE REGISTROS "
    ''' <summary>
    ''' Carga los datos de configuración en el formulario
    ''' </summary>
    Private Sub CargarDatos()
        With cConfiguracionBBDD.Instancia
            txtCarpetaClassicGest.Text = .RutaCarpeta
            nudSegundosComprobacion.Value = .SegundosSincronizacion

            txtBBDDHost.Text = .BBDD_Host
            txtBBDDBaseDatos.Text = .BBDD_BaseDatos
            nudBBDDPuerto.Value = .BBDD_Puerto
            txtBBDDUsuario.Text = .BBDD_Usuario
            txtBBDDClave.Text = .BBDD_Pass

            cboAsignacionAlmacen.SelectedValue = .IdAlmacen
            cboAsignacionProveedor.SelectedValue = .IdProveedor
            cboAsignacionTienda.SelectedValue = .IdTienda
        End With

        ' Cargo las asignaciones
        Dim lista As AdvancedList(Of Clases.cAsignacionesProductos) = Clases.cAsignacionesProductos.ObtenerTodos()
        lista.Filter = "InstanciaEliminada = False"
        dgvAsignacionesProductos.DataSource = lista
    End Sub

    ''' <summary>
    ''' Guarda los datos introducidos y cierra el formulario
    ''' </summary>
    Private Sub GuardarYSalir(sender As Object, e As EventArgs) Handles btnAceptar.Click
        ' Guardo los datos
        With cConfiguracionBBDD.Instancia
            .RutaCarpeta = txtCarpetaClassicGest.Text
            .SegundosSincronizacion = nudSegundosComprobacion.Value

            .BBDD_Host = txtBBDDHost.Text
            .BBDD_BaseDatos = txtBBDDBaseDatos.Text
            .BBDD_Puerto = nudBBDDPuerto.Value
            .BBDD_Usuario = txtBBDDUsuario.Text
            .BBDD_Pass = txtBBDDClave.Text

            If cboAsignacionAlmacen.SelectedIndex = -1 Then .IdAlmacen = 0 Else .IdAlmacen = cboAsignacionAlmacen.SelectedValue
            If cboAsignacionProveedor.SelectedIndex = -1 Then .IdProveedor = 0 Else .IdProveedor = cboAsignacionProveedor.SelectedValue
            If cboAsignacionTienda.SelectedIndex = -1 Then .IdTienda = 0 Else .IdTienda = cboAsignacionTienda.SelectedValue
        End With

        Try
            cConfiguracionBBDD.Instancia.Guardar()

            For Each Articulo As Clases.cAsignacionesProductos In DirectCast(dgvAsignacionesProductos.DataSource, AdvancedList(Of Clases.cAsignacionesProductos)).AllItems
                Articulo.Guardar()
            Next
        Catch ex As Exception
            MessageBox.Show("Error al tratar de guardar la información: " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        Me.Close()
    End Sub

    ''' <summary>
    ''' Controla la eliminación de una línea de asignaciones de artículos
    ''' </summary>
    Private Sub EliminacionArticulo(sender As Object, e As DataGridViewRowCancelEventArgs) Handles dgvAsignacionesProductos.UserDeletingRow
        ' Validaciones
        If e.Row Is Nothing Then Exit Sub
        If e.Row.IsNewRow Then Exit Sub
        If e.Row.DataBoundItem Is Nothing Then Exit Sub

        e.Cancel = True
        DirectCast(e.Row.DataBoundItem, Clases.cAsignacionesProductos).InstanciaEliminada = True
        dgvAsignacionesProductos.CurrentCell = Nothing
        e.Row.Visible = False
    End Sub
#End Region
#Region " PRUEBA DE CONEXION "
    ''' <summary>
    ''' Intenta realizar una conexión a la base de datos de trazamare con los parámetros especificados
    ''' </summary>
    Private Sub VerificarConexion() Handles btnVerificarConexion.Click
        Dim CadenaConexion As String = String.Format("Server={0};Port={1};Database={2};Uid={3};Pwd={4};", txtBBDDHost.Text, nudBBDDPuerto.Value, txtBBDDBaseDatos.Text, txtBBDDUsuario.Text, txtBBDDClave.Text)
        Dim Conexion As New MySql.Data.MySqlClient.MySqlConnection(CadenaConexion)

        Try
            txtLogConexion.Text = "Realizando intento de conexión a la base de datos con los parámetros especificados"

            Conexion.Open()
            txtLogConexion.Text = "Conexión establecida con éxito"
        Catch ex As Exception
            txtLogConexion.Text = "Error al conectarse a la base de datos: " & ex.Message
        Finally
            Conexion.Close()
            Conexion.Dispose()
        End Try
    End Sub
#End Region
#Region " RESTO DE FUNCIONES "
    ''' <summary>
    ''' Muestra un fomrulario para que el usuario pueda localizar la carpeta de classicGest
    ''' </summary>
    Private Sub BuscarCarpeta(sender As Object, e As EventArgs) Handles btnBuscarCarpeta.Click
        Dim CuadroDialogo As New FolderBrowserDialog

        With CuadroDialogo
            .Description = "Seleccione la carpeta de datos de ClassicGest"
            .ShowNewFolderButton = False
            If Not String.IsNullOrWhiteSpace(txtCarpetaClassicGest.Text) AndAlso My.Computer.FileSystem.DirectoryExists(txtCarpetaClassicGest.Text) Then .SelectedPath = txtCarpetaClassicGest.Text

            If .ShowDialog <> Windows.Forms.DialogResult.OK Then Exit Sub
            txtCarpetaClassicGest.Text = .SelectedPath
        End With
    End Sub

    ''' <summary>
    ''' Evita que aparezca el molesto error
    ''' </summary>
    Private Sub EvitarError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dgvAsignacionesProductos.DataError
        e.Cancel = True
    End Sub
#End Region
End Class