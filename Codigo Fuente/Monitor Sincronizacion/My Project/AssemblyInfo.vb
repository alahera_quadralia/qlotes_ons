Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Monitor Sincronizacion")> 
<Assembly: AssemblyDescription("Monitor del servicio de sincronización")> 
<Assembly: AssemblyCompany("Quadralia")> 
<Assembly: AssemblyProduct("Monitor Sincronizacion")> 
<Assembly: AssemblyCopyright("Copyright ©  2016")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("51e10237-f8cc-4fac-80dd-663f64daad94")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.2020.1204")> 
<Assembly: AssemblyFileVersion("1.0.2020.1204")> 
