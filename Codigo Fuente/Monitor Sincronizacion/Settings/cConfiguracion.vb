﻿Public Class cConfiguracionBBDD
    Inherits cConfiguracionBase(Of cConfiguracionBBDD)

#Region " GENERAL "
    Public Id As Integer

    Public Overrides ReadOnly Property NombreTabla As String
        Get
            Return "Configuracion"
        End Get
    End Property
#End Region
#Region " BASE DE DATOS "
    Public Property RutaCarpeta As String
    Public Property SegundosSincronizacion As Integer
    Public Property BBDD_Host As String
    Public Property BBDD_Usuario As String
    Public Property BBDD_Pass As String
    Public Property BBDD_Puerto As Integer
    Public Property BBDD_BaseDatos As String
    Public Property IdProveedor As Integer
    Public Property IdAlmacen As Integer
    Public Property IdTienda As Integer
#End Region
#Region " METODOS A IMPLEMENTAR DE LA CLASE BASE "
    Public Overrides Function ObtenerDiccionario() As List(Of Mapeado)
        Dim Diccionario As New List(Of Mapeado)
        Diccionario.Add(New Mapeado("RutaCarpeta", "RutaCarpeta", "", False))
        Diccionario.Add(New Mapeado("SegundosSincronizacion", "SegundosSincronizacion", 10, False))
        Diccionario.Add(New Mapeado("BBDD_Host", "BBDD_Host", "", False))
        Diccionario.Add(New Mapeado("BBDD_Usuario", "BBDD_Usuario", "", False))
        Diccionario.Add(New Mapeado("BBDD_Pass", "BBDD_Pass", "", False))
        Diccionario.Add(New Mapeado("BBDD_Puerto", "BBDD_Puerto", 3306, False))
        Diccionario.Add(New Mapeado("BBDD_BaseDatos", "BBDD_BaseDatos", "", False))
        Diccionario.Add(New Mapeado("IdProveedor", "IdProveedor", 0, False))
        Diccionario.Add(New Mapeado("IdAlmacen", "IdAlmacen", 0, False))
        Diccionario.Add(New Mapeado("IdTienda", "IdTienda", 0, False))
        Diccionario.Add(New Mapeado("Id", "Id", 0, True))

        Return Diccionario
    End Function
#End Region
End Class