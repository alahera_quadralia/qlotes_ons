﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPrincipal
    Inherits ComponentFactory.Krypton.Toolkit.KryptonForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPrincipal))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.btnIniciar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.lblTitulo = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.btnReiniciar = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.lblEstado = New ComponentFactory.Krypton.Toolkit.KryptonLabel()
        Me.scControlador = New System.ServiceProcess.ServiceController()
        Me.tmrRefrescar = New System.Windows.Forms.Timer(Me.components)
        Me.LogEventos = New System.Diagnostics.EventLog()
        Me.btnConfiguracion = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        Me.ssEstado = New System.Windows.Forms.StatusStrip()
        Me.mnuInfoActualizacion = New Kjs.AppLife.Update.Controller.StatusStripUpdateDisplay()
        Me.upActualizador = New Kjs.AppLife.Update.Controller.UpdateController(Me.components)
        Me.colMensaje = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colIdEvento = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colFechaHora = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colTipo = New ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn()
        Me.colImagne = New System.Windows.Forms.DataGridViewImageColumn()
        Me.dgvLog = New ComponentFactory.Krypton.Toolkit.KryptonDataGridView()
        Me.btnBuscarActualizaciones = New ComponentFactory.Krypton.Toolkit.KryptonButton()
        CType(Me.LogEventos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ssEstado.SuspendLayout()
        CType(Me.mnuInfoActualizacion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvLog, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnIniciar
        '
        Me.btnIniciar.Location = New System.Drawing.Point(116, 38)
        Me.btnIniciar.Name = "btnIniciar"
        Me.btnIniciar.Size = New System.Drawing.Size(75, 24)
        Me.btnIniciar.TabIndex = 2
        Me.btnIniciar.Values.Text = "Iniciar"
        '
        'lblTitulo
        '
        Me.lblTitulo.Location = New System.Drawing.Point(12, 12)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(47, 20)
        Me.lblTitulo.TabIndex = 0
        Me.lblTitulo.Values.Text = "Estado"
        '
        'btnReiniciar
        '
        Me.btnReiniciar.Location = New System.Drawing.Point(197, 38)
        Me.btnReiniciar.Name = "btnReiniciar"
        Me.btnReiniciar.Size = New System.Drawing.Size(75, 24)
        Me.btnReiniciar.TabIndex = 3
        Me.btnReiniciar.Values.Text = "Reiniciar"
        '
        'lblEstado
        '
        Me.lblEstado.Location = New System.Drawing.Point(12, 40)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(53, 20)
        Me.lblEstado.TabIndex = 1
        Me.lblEstado.Values.Text = "Iniciado"
        '
        'tmrRefrescar
        '
        Me.tmrRefrescar.Interval = 500
        '
        'LogEventos
        '
        Me.LogEventos.EnableRaisingEvents = True
        Me.LogEventos.Log = "Application"
        Me.LogEventos.Source = "SincronizadorCG_Log"
        Me.LogEventos.SynchronizingObject = Me
        '
        'btnConfiguracion
        '
        Me.btnConfiguracion.Location = New System.Drawing.Point(278, 38)
        Me.btnConfiguracion.Name = "btnConfiguracion"
        Me.btnConfiguracion.Size = New System.Drawing.Size(75, 24)
        Me.btnConfiguracion.TabIndex = 4
        Me.btnConfiguracion.Values.Text = "Configurar"
        '
        'ssEstado
        '
        Me.ssEstado.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.ssEstado.GripStyle = System.Windows.Forms.ToolStripGripStyle.Visible
        Me.ssEstado.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuInfoActualizacion})
        Me.ssEstado.Location = New System.Drawing.Point(0, 280)
        Me.ssEstado.Name = "ssEstado"
        Me.ssEstado.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode
        Me.ssEstado.Size = New System.Drawing.Size(505, 22)
        Me.ssEstado.TabIndex = 13
        Me.ssEstado.Text = "StatusStrip1"
        '
        'mnuInfoActualizacion
        '
        Me.mnuInfoActualizacion.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.mnuInfoActualizacion.ApplicationName = "Monitor Sincronización"
        Me.mnuInfoActualizacion.ApplyUpdateOptions = Kjs.AppLife.Update.Controller.ApplyUpdateOptions.AutoClose
        Me.mnuInfoActualizacion.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.mnuInfoActualizacion.Name = "mnuInfoActualizacion"
        Me.mnuInfoActualizacion.NoUpdateCustomText = "Su software está actualizado"
        Me.mnuInfoActualizacion.NoUpdateImageLarge = CType(resources.GetObject("mnuInfoActualizacion.NoUpdateImageLarge"), System.Drawing.Image)
        Me.mnuInfoActualizacion.NoUpdateImageSmall = CType(resources.GetObject("mnuInfoActualizacion.NoUpdateImageSmall"), System.Drawing.Image)
        Me.mnuInfoActualizacion.ReadyToApplyCustomText = "Actualización lista para ser aplicada"
        Me.mnuInfoActualizacion.ReadyToApplyImageLarge = CType(resources.GetObject("mnuInfoActualizacion.ReadyToApplyImageLarge"), System.Drawing.Image)
        Me.mnuInfoActualizacion.ReadyToApplyImageSmall = CType(resources.GetObject("mnuInfoActualizacion.ReadyToApplyImageSmall"), System.Drawing.Image)
        Me.mnuInfoActualizacion.RecheckDelayMinutes = 30
        Me.mnuInfoActualizacion.Size = New System.Drawing.Size(125, 20)
        Me.mnuInfoActualizacion.SummaryCustomHtml = Nothing
        Me.mnuInfoActualizacion.SummaryCustomText = Nothing
        Me.mnuInfoActualizacion.UpdateController = Me.upActualizador
        '
        'upActualizador
        '
        Me.upActualizador.ApplicationId = New System.Guid("f7338d53-18dc-4f3b-8298-cef6a5e6a35c")
        Me.upActualizador.BypassProxyOnLocal = True
        Me.upActualizador.EnableAutoChaining = True
        Me.upActualizador.EnableOfflineUpdates = True
        Me.upActualizador.PublicKeyToken = resources.GetString("upActualizador.PublicKeyToken")
        Me.upActualizador.UpdateLocation = "http://update.quadralia.com/qLotes/Monitor"
        Me.upActualizador.UseHostAssemblyVersion = True
        '
        'colMensaje
        '
        Me.colMensaje.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colMensaje.HeaderText = "Mensaje"
        Me.colMensaje.Name = "colMensaje"
        Me.colMensaje.ReadOnly = True
        Me.colMensaje.Width = 222
        '
        'colIdEvento
        '
        Me.colIdEvento.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colIdEvento.HeaderText = "Id. del evento"
        Me.colIdEvento.Name = "colIdEvento"
        Me.colIdEvento.ReadOnly = True
        Me.colIdEvento.Width = 107
        '
        'colFechaHora
        '
        Me.colFechaHora.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        DataGridViewCellStyle1.Format = "G"
        DataGridViewCellStyle1.NullValue = Nothing
        Me.colFechaHora.DefaultCellStyle = DataGridViewCellStyle1
        Me.colFechaHora.HeaderText = "Fecha y Hora"
        Me.colFechaHora.Name = "colFechaHora"
        Me.colFechaHora.ReadOnly = True
        Me.colFechaHora.Width = 105
        '
        'colTipo
        '
        Me.colTipo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colTipo.HeaderText = "Nivel"
        Me.colTipo.Name = "colTipo"
        Me.colTipo.ReadOnly = True
        Me.colTipo.Width = 63
        '
        'colImagne
        '
        Me.colImagne.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colImagne.HeaderText = ""
        Me.colImagne.Name = "colImagne"
        Me.colImagne.ReadOnly = True
        Me.colImagne.Width = 7
        '
        'dgvLog
        '
        Me.dgvLog.AllowUserToAddRows = False
        Me.dgvLog.AllowUserToDeleteRows = False
        Me.dgvLog.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvLog.ColumnHeadersHeight = 20
        Me.dgvLog.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colImagne, Me.colTipo, Me.colFechaHora, Me.colIdEvento, Me.colMensaje})
        Me.dgvLog.Location = New System.Drawing.Point(0, 68)
        Me.dgvLog.Name = "dgvLog"
        Me.dgvLog.ReadOnly = True
        Me.dgvLog.RowHeadersVisible = False
        Me.dgvLog.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLog.Size = New System.Drawing.Size(505, 212)
        Me.dgvLog.TabIndex = 5
        '
        'btnBuscarActualizaciones
        '
        Me.btnBuscarActualizaciones.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBuscarActualizaciones.Location = New System.Drawing.Point(381, 38)
        Me.btnBuscarActualizaciones.Name = "btnBuscarActualizaciones"
        Me.btnBuscarActualizaciones.Size = New System.Drawing.Size(112, 24)
        Me.btnBuscarActualizaciones.TabIndex = 4
        Me.btnBuscarActualizaciones.Values.Text = "Buscar Actualiz."
        '
        'frmPrincipal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(505, 302)
        Me.Controls.Add(Me.ssEstado)
        Me.Controls.Add(Me.btnBuscarActualizaciones)
        Me.Controls.Add(Me.btnConfiguracion)
        Me.Controls.Add(Me.dgvLog)
        Me.Controls.Add(Me.lblEstado)
        Me.Controls.Add(Me.lblTitulo)
        Me.Controls.Add(Me.btnReiniciar)
        Me.Controls.Add(Me.btnIniciar)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(521, 340)
        Me.Name = "frmPrincipal"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Monitor Sincronización"
        CType(Me.LogEventos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ssEstado.ResumeLayout(False)
        Me.ssEstado.PerformLayout()
        CType(Me.mnuInfoActualizacion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvLog, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnIniciar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents lblTitulo As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents btnReiniciar As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents lblEstado As ComponentFactory.Krypton.Toolkit.KryptonLabel
    Friend WithEvents scControlador As System.ServiceProcess.ServiceController
    Friend WithEvents tmrRefrescar As System.Windows.Forms.Timer
    Friend WithEvents LogEventos As System.Diagnostics.EventLog
    Friend WithEvents btnConfiguracion As ComponentFactory.Krypton.Toolkit.KryptonButton
    Friend WithEvents ssEstado As System.Windows.Forms.StatusStrip
    Friend WithEvents mnuInfoActualizacion As Kjs.AppLife.Update.Controller.StatusStripUpdateDisplay
    Friend WithEvents dgvLog As ComponentFactory.Krypton.Toolkit.KryptonDataGridView
    Friend WithEvents colImagne As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents colTipo As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents colFechaHora As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents colIdEvento As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents colMensaje As ComponentFactory.Krypton.Toolkit.KryptonDataGridViewTextBoxColumn
    Friend WithEvents upActualizador As Kjs.AppLife.Update.Controller.UpdateController
    Friend WithEvents btnBuscarActualizaciones As ComponentFactory.Krypton.Toolkit.KryptonButton

End Class
