﻿Public Class frmPrincipal
#Region " ENUMERADOS "
    Private Enum EstadosServicio
        Iniciado = 0
        Detenido = 1
        Progreso = 2
        NoInstalado = 3
    End Enum
#End Region
#Region " CONSTANTES "
    Private Const NOMBRE_SERVICIO As String = "qLotesSyncCG"
#End Region
#Region " PROPIEDADES "
    Private _EstadoServicio As EstadosServicio = EstadosServicio.Iniciado
    Private Property EstadoServicio As EstadosServicio
        Get
            Return _EstadoServicio
        End Get
        Set(value As EstadosServicio)
            _EstadoServicio = value

            Select Case value
                Case EstadosServicio.Detenido
                    lblEstado.Text = "Detenido"
                    lblEstado.ForeColor = Color.FromArgb(169, 68, 66)

                    btnIniciar.Enabled = True
                    btnReiniciar.Enabled = True
                    btnIniciar.Text = "Iniciar"
                Case EstadosServicio.Iniciado
                    lblEstado.Text = "Iniciado"
                    lblEstado.ForeColor = Color.FromArgb(60, 118, 61)

                    btnIniciar.Enabled = True
                    btnReiniciar.Enabled = True
                    btnIniciar.Text = "Detener"
                Case EstadosServicio.Progreso
                    lblEstado.Text = "En Progreso"
                    lblEstado.ForeColor = Color.FromArgb(49, 112, 143)

                    btnIniciar.Enabled = False
                    btnReiniciar.Enabled = False
                Case EstadosServicio.NoInstalado
                    lblEstado.Text = "No instalado"
                    lblEstado.ForeColor = Color.FromArgb(169, 68, 66)

                    btnIniciar.Enabled = False
                    btnReiniciar.Enabled = False
                    btnIniciar.Text = "Iniciar"
            End Select
        End Set
    End Property
#End Region
#Region " ACTUALIZACIONES "
    ''' <summary>
    ''' Obtiene los parámetros necesarios para comunicarse con el servidor
    ''' </summary>
    Public Function ObtenerParametroActualizacion() As Dictionary(Of String, Object)
        Dim Resultado As New Dictionary(Of String, Object)

        Resultado.Add("CadenaConexionSQLite", cConfiguracionBBDD.CadenaConexionUtilizada)
        Resultado.Add("CodigoPrograma", "QLT-MON")

        Return Resultado
    End Function
#End Region
#Region " INICIO / FIN "
    ''' <summary>
    ''' Rutina del formulario
    ''' </summary>
    Private Sub Inicio(sender As Object, e As EventArgs) Handles MyBase.Load
        ' Cargo las configuraciones
        cConfiguracionBBDD.Instancia.Cargar(1)
        Configuracion.CadenaDeConexionPropia = cConfiguracionBBDD.CadenaConexionUtilizada

        Quadralia.Actualizaciones.ComprobarActualizacionesDescargadas(upActualizador, ObtenerParametroActualizacion())

        ' Configuro el actualizador
        Quadralia.Actualizaciones.ConfigurarActualizador(mnuInfoActualizacion, upActualizador, ObtenerParametroActualizacion())

        scControlador.ServiceName = NOMBRE_SERVICIO

        RefrescarEstado(Nothing, Nothing)
        RefrescarLog(Nothing, Nothing)

        tmrRefrescar.Start()
    End Sub
#End Region
#Region " SERVICIO "
    ''' <summary>
    ''' Refresca el estado en el que se encuentra el servicio
    ''' </summary>
    Private Sub RefrescarEstado(sender As Object, e As EventArgs) Handles tmrRefrescar.Tick
        Try
            scControlador.Refresh()

            Select Case scControlador.Status
                Case ServiceProcess.ServiceControllerStatus.Stopped, ServiceProcess.ServiceControllerStatus.Paused
                    Me.EstadoServicio = EstadosServicio.Detenido
                Case ServiceProcess.ServiceControllerStatus.Running
                    Me.EstadoServicio = EstadosServicio.Iniciado
                Case Else
                    Me.EstadoServicio = EstadosServicio.Progreso
            End Select
        Catch ex As Exception
            Me.EstadoServicio = EstadosServicio.NoInstalado
        End Try
    End Sub

    ''' <summary>
    ''' Refresca el log del servicio
    ''' </summary>
    Private Sub RefrescarLog(sender As Object, e As EntryWrittenEventArgs) Handles LogEventos.EntryWritten
        Dim Entradas As New List(Of EventLogEntry)
        If e Is Nothing OrElse e.Entry Is Nothing Then
            dgvLog.Rows.Clear()

            For Each Entrada As EventLogEntry In LogEventos.Entries
                If Entrada.Source <> "qLotesSyncCG" AndAlso Entrada.Source <> "SincronizadorCG_Log" Then Continue For

                Entradas.Add(Entrada)
            Next
        Else
            If e.Entry.Source = "qLotesSyncCG" OrElse e.Entry.Source = "SincronizadorCG_Log" Then Entradas.Add(e.Entry)
        End If

        Dim Imagen As Image = Nothing
        For Each Entrada As EventLogEntry In Entradas
            Select Case Entrada.EntryType
                Case EventLogEntryType.Error
                    Imagen = My.Resources.error_16
                Case EventLogEntryType.Information
                    Imagen = My.Resources.info_16
                Case EventLogEntryType.Warning
                    Imagen = My.Resources.warning_16
                Case Else
                    Imagen = My.Resources.Llave_16
            End Select


            dgvLog.Rows.Insert(0, Imagen, Entrada.EntryType, Entrada.TimeWritten, Entrada.InstanceId, Entrada.Message)
        Next
    End Sub

    ''' <summary>
    ''' Inicia o para el servicio
    ''' </summary>
    Private Sub IniciarDetener(sender As Object, e As EventArgs) Handles btnIniciar.Click
        If Me.EstadoServicio = EstadosServicio.Detenido Then
            IniciarServicio()
        ElseIf Me.EstadoServicio = EstadosServicio.Iniciado Then
            DetenerServicio()
        End If
    End Sub

    ''' <summary>
    ''' Detiene el servicio si estaba iniciado
    ''' </summary>
    Private Sub DetenerServicio()
        If scControlador.Status = ServiceProcess.ServiceControllerStatus.Running Then scControlador.Stop()
    End Sub

    ''' <summary>
    ''' Inicia el servicio si estaba detenido
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub IniciarServicio()
        If scControlador.Status = ServiceProcess.ServiceControllerStatus.Paused OrElse scControlador.Status = ServiceProcess.ServiceControllerStatus.Stopped Then scControlador.Start()
    End Sub

    ''' <summary>
    ''' Detiene el servicio para iniciarlo nuevamente
    ''' </summary>
    Private Sub RestarServicio()
        DetenerServicio()
        scControlador.WaitForStatus(ServiceProcess.ServiceControllerStatus.Stopped)
        IniciarServicio()
    End Sub
#End Region
#Region " CONFIGURACION "
    ''' <summary>
    ''' Abre el formulario de configuración de los parámetros
    ''' </summary>
    Private Sub AbrirConfiguracion(sender As Object, e As EventArgs) Handles btnConfiguracion.Click
        Dim FormularioConfiguracion As New frmConfiguracion
        frmConfiguracion.ShowDialog()
    End Sub
#End Region
#Region " ACTUALIZACIONES "
    ''' <summary>
    ''' Busca si existen actualizaciones disponibles
    ''' </summary>
    Private Sub BuscarActualizaciones(sender As Object, e As EventArgs) Handles btnBuscarActualizaciones.Click
        If Not upActualizador.IsBusy Then upActualizador.UpdateInteractive(Me, Kjs.AppLife.Update.Controller.ErrorDisplayLevel.ShowExceptionMessage, Kjs.AppLife.Update.Controller.ApplyUpdateOptions.AutoClose, ObtenerParametroActualizacion())
    End Sub
#End Region
End Class
