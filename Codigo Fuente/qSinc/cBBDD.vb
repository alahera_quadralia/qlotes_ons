﻿Imports MySql.Data.MySqlClient

Friend Class cConnectionManager
#Region " CONSTRUCTOR "
    Friend Sub New()
    End Sub

    Friend Sub New(CadenaConexion As String, TimeOut As Integer)
        Me.CadenaConexion = CadenaConexion
        Me.TimeOut = TimeOut
    End Sub
#End Region
#Region " PROPIEDADES "
    Friend Property CadenaConexion As String
    Friend Property TimeOut As Integer
#End Region
#Region " METODOS PRIVADOS "
    ''' <summary>
    ''' Función que abre una conexión
    ''' </summary>
    ''' <returns>True si se estableció la conexión</returns>
    Friend Function AbrirConexion(ByVal conn As MySqlConnection) As Boolean
        If Not conn Is Nothing Then
            If conn.State <> ConnectionState.Connecting AndAlso conn.State <> ConnectionState.Open Then
                conn.Open()
            End If

            Return True
        Else
            Return False
        End If
    End Function

    ''' <summary>
    ''' Función que cierra una conexión existente
    ''' </summary>
    Friend Sub CerrarConexion(ByVal conn As MySqlConnection)
        If Not conn Is Nothing Then
            If conn.State <> ConnectionState.Closed Then conn.Close()
        End If
    End Sub
#End Region
#Region " METODOS friendOS "
    ''' <summary>
    ''' Intenta realizar una conexión con la base de datos especificada
    ''' </summary>
    Friend Function SePuedeConectar(Optional ByRef ErrorDevuelto As Exception = Nothing) As Boolean
        Dim Conexion As New MySqlConnection(CadenaConexion)

        Try
            'Creo la conexión en intento conectarme            
            AbrirConexion(Conexion)

            'Si todo ha ido bien, devuelvo true
            Return True
        Catch ex As Exception
            ErrorDevuelto = ex
            Return False
        Finally
            'Pase lo que pase, cierro la conexión
            CerrarConexion(Conexion)
        End Try

    End Function

    ''' <summary>
    ''' Intenta realizar una conexión con la base de datos especificada
    ''' </summary>
    Friend Function SePuedeConectar(ByVal CadenaConexion As String, Optional ByRef ErrorDevuelto As Exception = Nothing) As Boolean
        Dim Conexion As New MySqlConnection(CadenaConexion)

        Try
            'Creo la conexión en intento conectarme
            AbrirConexion(Conexion)

            'Si todo ha ido bien, devuelvo true
            Return True
        Catch ex As Exception
            ErrorDevuelto = ex
            Return False
        Finally
            'Pase lo que pase, cierro la conexión
            CerrarConexion(Conexion)
        End Try

    End Function

    ''' <summary>
    ''' Ejecuta las consultas que se le pasan por parametro
    ''' </summary>
    ''' <param name="Consulta">Consultas a ejecutar</param>
    Friend Function EjecutarConsulta(ByVal Consulta() As String) As Long
        Dim Conexion As New MySqlConnection(CadenaConexion)
        If AbrirConexion(Conexion) Then
            Dim Transaccion As MySqlTransaction = Conexion.BeginTransaction()
            Dim Resultado As Integer

            Try
                For Each Cons As String In Consulta
                    Dim Comando As New MySqlCommand(Cons, Conexion, Transaccion)
                    Comando.CommandTimeout = TimeOut
                    Resultado = Comando.ExecuteNonQuery()
                Next

                Transaccion.Commit()

            Catch ex As Exception
                Transaccion.Rollback()
            Finally
                CerrarConexion(Conexion)
                EjecutarConsulta = Resultado
            End Try
        Else
            Return -1
        End If
    End Function

    ''' <summary>
    ''' Ejecuta la consulta que se le pasa por parametro
    ''' </summary>
    ''' <param name="Consulta">Consulta a ejecutar</param>
    ''' <returns>-1 si se produce un error, o el número de filas afectadas en otro caso</returns>
    Friend Function EjecutarConsulta(ByVal Consulta As String) As Long
        Dim ListaConsultas() As String = {Consulta}
        Return EjecutarConsulta(ListaConsultas)
    End Function

    ''' <summary>
    ''' Ejecuta el comando que se le pasa por parametro
    ''' </summary>
    ''' <param name="Comando">Comando a ejecutar</param>
    Friend Function EjecutarComando(ByRef Comando As MySqlCommand) As Object
        Dim Conexion As New MySqlConnection(CadenaConexion)

        If AbrirConexion(Conexion) Then
            Try
                Comando.Connection = Conexion
                Comando.CommandTimeout = TimeOut
                Comando.Prepare()
                Return Comando.ExecuteScalar
            Catch ex As Exception
                Throw ex
            Finally
                CerrarConexion(Comando.Connection)
            End Try
        Else
            Return Nothing
        End If
    End Function

    ''' <summary>
    ''' Ejecuta el comando que se le pasa por parametro
    ''' </summary>
    ''' <param name="Comando">Comando a ejecutar</param>
    Friend Function EjecutarComandoNonQuery(ByRef Comando As MySqlCommand, Optional ByVal MantenerConexionAbierta As Boolean = False) As Integer
        Dim Conexion As New MySqlConnection(CadenaConexion)
        If AbrirConexion(Conexion) Then
            Try
                Comando.Connection = Conexion
                Comando.CommandTimeout = TimeOut
                Comando.Prepare()
                Return Comando.ExecuteNonQuery()
            Catch ex As Exception
                Throw ex
                Return -1
            Finally
                If Not MantenerConexionAbierta Then CerrarConexion(Comando.Connection)
            End Try
        End If

        Return -1
    End Function

    ''' <summary>
    ''' Ejecuta la consulta que se le pasa por parametro
    ''' </summary>
    ''' <param name="Consulta">Consulta a ejecutar</param>
    ''' <returns>El valor de la primera columna de la primera fila del conjunto de datos obtenido por la consulta</returns>
    Friend Function ObtenerPrimerValor(ByVal Consulta As String) As Object
        Return ObtenerPrimerValor(New MySqlCommand(Consulta))
    End Function

    ''' <summary>
    ''' Ejecuta la consulta que se le pasa por parametro
    ''' </summary>
    ''' <param name="Comando">Comando que se va a ejecutar</param>
    ''' <returns>El valor de la primera columna de la primera fila del conjunto de datos obtenido por la consulta</returns>
    Friend Function ObtenerPrimerValor(ByVal Comando As MySqlCommand) As Object
        If Comando.Connection Is Nothing Then Comando.Connection = New MySqlConnection(CadenaConexion)
        If AbrirConexion(Comando.Connection) Then
            Try
                Return Comando.ExecuteScalar
            Catch ex As Exception
                Throw ex
            Finally
                CerrarConexion(Comando.Connection)
            End Try
        Else
            Return Nothing
        End If
    End Function
#End Region
End Class