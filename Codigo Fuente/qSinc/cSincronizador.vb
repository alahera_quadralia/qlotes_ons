﻿Imports MySql.Data.MySqlClient
Imports System.Net.FtpClient
Imports System.IO
Imports System.Net
Imports Newtonsoft.Json
Imports qSinc
Imports System.Linq
Imports DAL
Imports System.Threading
Imports System.Windows.Forms
Imports System.Text

Public Class Sincronizador
#Region " ESTRUCTURAS "
    Public Structure ConexionBBDD
        Public Servidor As String
        Public Usuario As String
        Public Clave As String
        Public BaseDatos As String
        Public Puerto As Integer
        Public Timeout As Integer
    End Structure

    Public Structure ConexionFTP
        Public Servidor As String
        Public Usuario As String
        Public Clave As String
        Public RutaCarpeta As String
        Public Puerto As Integer
    End Structure

    Public Structure ServidorTrazamare
        Public Servidor As String
        Public Clave As String
        Public Ruta As String
    End Structure
#End Region
#Region " DECLARACIONES "
    Private Shared _Instancia As Sincronizador = Nothing
    Private _Ejecutando As Boolean = False

    Public WithEvents Timer As Timers.Timer
#End Region
#Region " PROPIEDADES "
    ''' <summary>
    ''' Especifica las creedenciales de conexión con la base de datos remota
    ''' </summary>
    Public Property DatosConexionBBDD As ConexionBBDD

    ''' <summary>
    ''' Especifica las creedenciales de conexión con el servidor FTP Remoto
    ''' </summary>
    Public Property DatosConexionFTP As ConexionFTP

    Public Property DatosConexionTrazamare As ServidorTrazamare

    ''' <summary>
    ''' Indica si se está ejecutando el proceso de sincronización
    ''' </summary>
    Public ReadOnly Property Ejecutando As Boolean
        Get
            Return _Ejecutando
        End Get
    End Property

    Private ReadOnly Property CadenaConexion As String
        Get
            Dim puerto As Integer = DatosConexionBBDD.Puerto
            If puerto <= 0 Then puerto = 3306

            Return String.Format("Server={0};Database={1};Uid={2};Pwd={3};Port={4}", DatosConexionBBDD.Servidor, DatosConexionBBDD.BaseDatos, DatosConexionBBDD.Usuario, DatosConexionBBDD.Clave, DatosConexionBBDD.Puerto)
        End Get
    End Property

    Private ReadOnly Property UrlDestino As String
        Get
            Return String.Format("{0}/{1}", DatosConexionTrazamare.Servidor, DatosConexionTrazamare.Ruta)
        End Get
    End Property

    Private ReadOnly Property UrlZonas As String
        Get
            Return String.Format("{0}/api/v1/zona-fao", DatosConexionTrazamare.Servidor)
        End Get
    End Property
    Private ReadOnly Property UrlSubzonas As String
        Get
            Return String.Format("{0}/api/v1/zona-gsa", DatosConexionTrazamare.Servidor)
        End Get
    End Property

    Public Property intervalo As Int32

#End Region
#Region " EVENTOS "
    Public Event Progreso(ProgresoGeneral As Integer, ProgresoParticular As Integer, ElementoProcesado As String)
    Public Event SincronizacionIniciada()
    Public Event SincronizacionFinalizada()
    Public Event SincronizacionAcabada(eti_Sincro As List(Of Etiqueta), eti_Error As List(Of Etiqueta))
#End Region
#Region " SINGLETON "
    ''' <summary>
    ''' Acceso único al sistema de sincronización
    ''' </summary>
    Public Shared ReadOnly Property Instancia As Sincronizador
        Get
            If _Instancia Is Nothing Then _Instancia = New Sincronizador
            Return _Instancia
        End Get
    End Property
#End Region
#Region " METODOS PUBLICOS "
    ''' <summary>
    ''' Sincroniza todos los elementos con la web de trazamare
    ''' </summary>
    Public Sub Sincronizar(ByVal Etiquetas As List(Of DAL.Etiqueta), ByVal Embarcaciones As List(Of DAL.Barco), ByVal Especies As List(Of DAL.Especie),
                           ByVal Artes As List(Of DAL.MetodoProduccion), ByVal Empaquetados As List(Of DAL.Empaquetado),
                           Optional ByVal pSincronizarEtiquetasPaquetes As Boolean = False)
        _Ejecutando = True
        RaiseEvent SincronizacionIniciada()
        Try
            SincronizarArtes(Artes, 20)
            SincronizarEmbarcaciones(Embarcaciones, 40)
            SincronizarEspecies(Especies, 60)
            SincronizarEtiquetas(Etiquetas, 100, pSincronizarEtiquetasPaquetes)
            SincronizarEmpaquetados(Empaquetados, 80)
        Catch ex As Exception
            EscribirEnLog(ex.Message + "-" + ex.InnerException.ToString())
        End Try

        ''ActualizarEmpaquetadosASinSincronizar()

        _Ejecutando = False

        RaiseEvent SincronizacionFinalizada()
    End Sub
    Public Sub Sincronizar(ByVal Etiquetas As List(Of DAL.Etiqueta))
        _Ejecutando = True
        RaiseEvent SincronizacionIniciada()
        Try

            SincronizarEtiquetas(Etiquetas, 100)
        Catch ex As Exception
            EscribirEnLog(ex.Message + "-" + ex.InnerException.ToString())
        End Try

        ''ActualizarEmpaquetadosASinSincronizar()

        _Ejecutando = False

        RaiseEvent SincronizacionFinalizada()
    End Sub
    Public Sub SincronizarEtiquetas(ByVal Etiquetas As List(Of DAL.Etiqueta), Optional ByVal pSincronizarEtiquetasPaquetes As Boolean = False)
        _Ejecutando = True
        RaiseEvent SincronizacionIniciada()
        Try
            SincronizarEtiquetas(Etiquetas, 100, pSincronizarEtiquetasPaquetes)
        Catch ex As Exception
            Throw ex
        End Try

        _Ejecutando = False

        RaiseEvent SincronizacionFinalizada()
    End Sub
    Public Sub SincronizarOns(ByVal codigoTrazamare As String, ByVal Etiquetas As List(Of DAL.Etiqueta))
        _Ejecutando = True
        RaiseEvent SincronizacionIniciada()
        Dim etiquetasSincronizadas As New List(Of Etiqueta)
        Dim etiquetasErroneas As New List(Of Etiqueta)
        Try

            SincronizarEtiquetasOns(codigoTrazamare, Etiquetas, 100, etiquetasSincronizadas, etiquetasErroneas)
        Catch ex As Exception
            'MessageBox.Show(ex.Message)
            'Throw ex
        End Try

        _Ejecutando = False

        RaiseEvent SincronizacionAcabada(etiquetasSincronizadas, etiquetasErroneas)
    End Sub

    Public Sub SincronizarEmbarcaciones(ByVal Embarcaciones As List(Of DAL.Barco))
        _Ejecutando = True
        RaiseEvent SincronizacionIniciada()
        SincronizarEmbarcaciones(Embarcaciones, 100)
        _Ejecutando = False

        RaiseEvent SincronizacionFinalizada()
    End Sub

    Public Sub SincronizarEspecies(ByVal Especies As List(Of DAL.Especie))
        _Ejecutando = True
        RaiseEvent SincronizacionIniciada()
        SincronizarEspecies(Especies, 100)
        _Ejecutando = False

        RaiseEvent SincronizacionFinalizada()
    End Sub

    Public Sub SincronizarArtes(ByVal Artes As List(Of DAL.MetodoProduccion))
        _Ejecutando = True
        RaiseEvent SincronizacionIniciada()
        SincronizarArtes(Artes, 100)
        _Ejecutando = False

        RaiseEvent SincronizacionFinalizada()
    End Sub

    Public Sub SincronizarEmpaquetados(ByVal Empaquetados As List(Of DAL.Empaquetado))
        _Ejecutando = True
        RaiseEvent SincronizacionIniciada()
        SincronizarEmpaquetados(Empaquetados, 100)
        _Ejecutando = False

        RaiseEvent SincronizacionFinalizada()
    End Sub
#End Region
#Region " METODOS PRIVADOS "
    ''' <summary>
    ''' Lanza el evento previas validaciones de rangos
    ''' </summary>
    Private Sub LanzarEvento(ProgresoGeneral As Double, ProgresoParticular As Double, ElementoProcesado As String)
        Dim General As Integer = Math.Truncate(ProgresoGeneral)
        Dim Particular As Integer = Math.Truncate(ProgresoParticular)

        If General < 0 Then General = 0
        If General > 100 Then General = 100

        If Particular < 0 Then Particular = 0
        If Particular > 100 Then Particular = 100

        RaiseEvent Progreso(General, Particular, ElementoProcesado)
    End Sub

    ''' <summary>
    ''' Sincroniza las etiquetas con el servidor de trazamare
    ''' </summary>
    Private Sub SincronizarEtiquetasOns(ByVal codigoTrazamare As String, ByVal Etiquetas As List(Of DAL.Etiqueta), IndiceGeneral As Integer, ByRef sincronizadas As List(Of Etiqueta), ByRef erroneas As List(Of Etiqueta))
        ' Validaciones       
        If Etiquetas Is Nothing OrElse Etiquetas.Count = 0 Then Exit Sub

        Dim Timeout As Integer = DatosConexionBBDD.Timeout
        If Timeout <= 0 Then Timeout = 30
        'Dim Sincronizador As New cConnectionManager(Me.CadenaConexion, Timeout)
        'Dim Conexion As New MySqlConnection(Me.CadenaConexion)

        'Try
        ' Abro la conexión
        'If Not Sincronizador.AbrirConexion(Conexion) Then Exit Sub
        ' Ejecuto las consultas
        For i As Integer = 0 To Etiquetas.Count - 1
            Try
                Dim etq As DAL.Etiqueta = Etiquetas(i)
                If etq IsNot Nothing Then
                    'Dim request As WebRequest = WebRequest.Create(UrlDestino)
                    'request.Method = "POST"
                    'request.ContentType = "application/json; charset=utf-8"

                    'token ons
                    'request.Headers.Add("Authorization", "Bearer TRZMR_opgc123wqcbl0048srtBCG1988qdr4lIamstrs")

                    Dim request As HttpWebRequest = HttpWebRequest.Create(UrlDestino)
                    request.Method = "POST"
                    'request.ContentType = "application/json; charset=utf-8"
                    request.Headers.Add(HttpRequestHeader.Authorization, "Bearer " & DatosConexionTrazamare.Clave)
                    request.Accept = "application/json"
                    request.ContentType = "application/x-www-form-urlencoded"


                    Dim etiq As New EtiquetaTrazamare(Etiquetas(i))
                    If DatosConexionTrazamare.Clave = "TESTR_opgc123wqcbl0048srtBCG1988qdr4lIamstrs" Then
                        etiq.especie_id = "4"
                        etiq.arte_pesca_id = "1"
                    End If

                    'Dim dict As New Dictionary(Of String, String)
                    'dict.Add("especie_id", etiq.especie_id)
                    'dict.Add("fecha", etiq.fecha)
                    'dict.Add("etiqueta_id", String.Join(Environment.NewLine, etiq.etiqueta_id))
                    'Dim str = String.Join(Environment.NewLine, dict)
                    Dim encoding As New ASCIIEncoding
                    Dim data As Byte() = encoding.GetBytes(etiq.GetCadenaPOst)
                    'Dim obj = JsonConvert.SerializeObject(etiq)

                    'Dim json_bytes() As Byte = System.Text.Encoding.UTF8.GetBytes(obj)
                    'request.ContentLength = json_bytes.Length
                    request.ContentLength = data.Length
                    Dim stream As IO.Stream = request.GetRequestStream

                    'stream.Write(json_bytes, 0, json_bytes.Length)
                    stream.Write(data, 0, data.Length)

                    Dim response As Net.HttpWebResponse = request.GetResponse
                    Dim datastream As Stream = response.GetResponseStream()
                    Dim reader As New StreamReader(datastream)
                    Dim a = reader.ReadToEnd()
                    'EscribirEnLog(a)
                    'MessageBox.Show(response.StatusCode)

                    EscribirEnLog(etq.Codigo & ": " & response.StatusCode)

                    If response.StatusCode = HttpStatusCode.Created Then
                        etq.Sincronizando = True
                        etq.sincronizadoWeb = True
                    End If
                End If
                LanzarEvento(IndiceGeneral, ((i + 1) * 100) / Etiquetas.Count, String.Format("[{1} de {2}] Sincronizando Etiqueta {0}", etq.Codigo, i + 1, Etiquetas.Count))
                sincronizadas.Add(Etiquetas(i))
            Catch ex As WebException

                Dim datastream As Stream = ex.Response.GetResponseStream()
                Dim reader As New StreamReader(datastream)
                Dim a = reader.ReadToEnd()

                Dim respuesta = JsonConvert.DeserializeObject(Of RespuestaTrazamare)(a)

                If respuesta.etiqueta Is Nothing Then
                    erroneas.Add(Etiquetas(i))
                Else
                    If respuesta.etiqueta.peso_neto.Replace(".", ",") = Etiquetas(i).cantidad.ToString _
                        AndAlso respuesta.etiqueta.lote = Etiquetas(i).LoteSalida _
                        AndAlso respuesta.etiqueta.etiqueta_items(0).codigo = (codigoTrazamare & Etiquetas(i).Codigo) Then
                        Dim etq As DAL.Etiqueta = Etiquetas(i)
                        etq.Sincronizando = True
                        etq.sincronizadoWeb = True
                        sincronizadas.Add(Etiquetas(i))
                    Else
                        erroneas.Add(Etiquetas(i))
                    End If
                End If
                'Dim algo = JsonConvert.DeserializeObject < EtiquetaTrazamare > (objecto.)
                Dim b = 0
            Catch ex As Exception
                erroneas.Add(Etiquetas(i))
                EscribirEnLog(ex.Message)
            End Try
        Next
        'Catch ex As Exception
        '    'Dim datastream As Stream = DirectCast(ex, System.Net.WebException).Response.GetResponseStream()
        '    'Dim reader As New StreamReader(datastream)
        '    'Dim a = reader.ReadToEnd()

        '    'MessageBox.Show(ex.Message)
        '    EscribirEnLog(ex.Message)

        'Finally
        '    'Sincronizador.CerrarConexion(Conexion)
        'End Try
    End Sub
    Private Sub SincronizarEtiquetas(ByVal Etiquetas As List(Of DAL.Etiqueta), IndiceGeneral As Integer, Optional ByVal pSincronizarEtiquetasPaquetes As Boolean = False)
        ' Validaciones       
        If Etiquetas Is Nothing OrElse Etiquetas.Count = 0 Then Exit Sub

        Dim Timeout As Integer = DatosConexionBBDD.Timeout
        If Timeout <= 0 Then Timeout = 30
        Dim Sincronizador As New cConnectionManager(Me.CadenaConexion, Timeout)
        Dim Conexion As New MySqlConnection(Me.CadenaConexion)

        Try
            ' Abro la conexión
            If Not Sincronizador.AbrirConexion(Conexion) Then Exit Sub
            ' Ejecuto las consultas
            For i As Integer = 0 To Etiquetas.Count - 1
                Dim etq As DAL.Etiqueta = Etiquetas(i)
                Dim Comando As New MySqlCommand("REPLACE INTO `etiqueta` (`Id`, `IdBarco`, `IDEspecie`, `IdEmpaquetado`, `Validada`, `dataValidacion`, `Peso`, `Lote`, `Formato`) VALUES (@Id, @IdBarco, @IDEspecie, @IdEmpaquetado, @Validada, @dataValidacion, @Peso, @Lote, @Formato);")
                Comando.Parameters.AddWithValue("Id", etq.id)
                Comando.Parameters.AddWithValue("IdBarco", etq.IdBarco)
                Comando.Parameters.AddWithValue("IDEspecie", etq.IdEspecie)
                Comando.Parameters.AddWithValue("IdEmpaquetado", etq.IdentificadorEmpaquetado)
                Comando.Parameters.AddWithValue("Validada", Not etq.fechaBaja.HasValue)
                Comando.Parameters.AddWithValue("dataValidacion", etq.fecha)
                Comando.Parameters.AddWithValue("Peso", etq.cantidad)
                Comando.Parameters.AddWithValue("Lote", "*%" + etq.LoteEntradaLinea.Numero_Lote + "%*")
                Comando.Parameters.AddWithValue("Formato", etq.DisenhoPred)

                Comando.Connection = Conexion
                Comando.CommandTimeout = Timeout
                Comando.Prepare()
                Comando.ExecuteNonQuery()

                etq.Sincronizando = True
                ''si estamos sincronizando etiquetas de paquetes no tocamos sincronizadoWeb
                If (Not pSincronizarEtiquetasPaquetes) Then
                    etq.sincronizadoWeb = True
                End If
                LanzarEvento(IndiceGeneral, ((i + 1) * 100) / Etiquetas.Count, String.Format("[{1} de {2}] Sincronizando Etiqueta {0}", etq.Codigo, i + 1, Etiquetas.Count))
            Next
        Catch ex As Exception
            EscribirEnLog(ex.Message + "-" + ex.InnerException.ToString())
            Throw ex
        Finally
            Sincronizador.CerrarConexion(Conexion)
        End Try
    End Sub

    ''' <summary>
    ''' Sincroniza las etiquetas con el servidor de trazamare
    ''' </summary>
    Private Sub SincronizarEmbarcaciones(ByVal Barcos As List(Of DAL.Barco), IndiceGeneral As Integer)
        ' Validaciones
        If Barcos Is Nothing OrElse Barcos.Count = 0 Then Exit Sub

        Dim Timeout As Integer = DatosConexionBBDD.Timeout
        If Timeout <= 0 Then Timeout = 30

        Dim Sincronizador As New cConnectionManager(Me.CadenaConexion, Timeout)
        Dim Conexion As New MySqlConnection(Me.CadenaConexion)

        Try
            ' Abro la conexión
            If Not Sincronizador.AbrirConexion(Conexion) Then Exit Sub

            ' Ejecuto las consultas
            For i As Integer = 0 To Barcos.Count - 1
                Dim bar As DAL.Barco = Barcos(i)
                Dim Comando As New MySqlCommand("REPLACE INTO `barco` (`Id`, `Nombre`, `idAlternancia`) VALUES (@Id, @Nombre, @idAlternancia);", Conexion)

                Comando.Parameters.AddWithValue("Id", bar.id)
                Comando.Parameters.AddWithValue("Nombre", bar.descripcion)
                Comando.Parameters.AddWithValue("idAlternancia", 1)

                Comando.CommandTimeout = Timeout
                Comando.Prepare()

                Comando.ExecuteNonQuery()

                ' Miro si tengo que sincronizar imagen
                If Not (SincronizarImagen(bar, "Barcos")) Then Continue For
                bar.Sincronizando = True
                bar.sincronizadoWeb = True
                LanzarEvento(IndiceGeneral, (i + 1 * 100) / Barcos.Count, String.Format("[{1} de {2}] Sincronizando Embaración {0}", bar.descripcion, i + 1, Barcos.Count))
            Next

        Catch ex As Exception
            EscribirEnLog(ex.Message + "-" + ex.InnerException.ToString())
            Throw ex
        Finally
            Sincronizador.CerrarConexion(Conexion)
        End Try
    End Sub

    ''' <summary>
    ''' Sincroniza las especies con el servidor de trazamare
    ''' </summary>
    Private Sub SincronizarEspecies(ByVal Especies As List(Of DAL.Especie), IndiceGeneral As Integer)
        ' Validaciones
        If Especies Is Nothing OrElse Especies.Count = 0 Then Exit Sub

        Dim Timeout As Integer = DatosConexionBBDD.Timeout
        If Timeout <= 0 Then Timeout = 30

        Dim Sincronizador As New cConnectionManager(Me.CadenaConexion, Timeout)
        Dim Conexion As New MySqlConnection(Me.CadenaConexion)

        Try
            ' Abro la conexión
            If Not Sincronizador.AbrirConexion(Conexion) Then Exit Sub

            ' Ejecuto las consultas
            For i As Integer = 0 To Especies.Count - 1
                Dim esp As DAL.Especie = Especies(i)
                Dim Comando As New MySqlCommand("REPLACE INTO `especies` (`Id`, `Nome`) VALUES (@Id, @Nome);")

                Comando.Parameters.AddWithValue("Id", esp.id)
                Comando.Parameters.AddWithValue("Nome", esp.denominacionComercial)

                Comando.Connection = Conexion
                Comando.CommandTimeout = Timeout
                Comando.Prepare()
                Comando.ExecuteNonQuery()

                If Not (SincronizarImagen(esp, "Especies")) Then Continue For

                esp.Sincronizando = True
                esp.sincronizadoWeb = True
                LanzarEvento(IndiceGeneral, ((i + 1) * 100) / Especies.Count, String.Format("[{1} de {2}] Sincronizando Especie {0}", esp.denominacionComercial, i + 1, Especies.Count))
            Next
        Catch ex As Exception
            ' Throw ex
        Finally
            Sincronizador.CerrarConexion(Conexion)
        End Try
    End Sub

    ''' <summary>
    ''' Sincroniza las artes con el servidor de trazamare
    ''' </summary>
    Private Sub SincronizarArtes(ByVal Artes As List(Of DAL.MetodoProduccion), IndiceGeneral As Integer)
        ' Validaciones
        If Artes Is Nothing OrElse Artes.Count = 0 Then Exit Sub

        Dim Timeout As Integer = DatosConexionBBDD.Timeout
        If Timeout <= 0 Then Timeout = 30

        Dim Sincronizador As New cConnectionManager(Me.CadenaConexion, Timeout)
        Dim Conexion As New MySqlConnection(Me.CadenaConexion)

        Try
            ' Abro la conexión
            If Not Sincronizador.AbrirConexion(Conexion) Then Exit Sub

            ' Ejecuto las consultas
            For i As Integer = 0 To Artes.Count - 1
                Dim art As DAL.MetodoProduccion = Artes(i)
                Dim Comando As New MySqlCommand("REPLACE INTO `tAlternancia` (`Id`, `Nombre`) VALUES (@Id, @Nombre);")

                Comando.Parameters.AddWithValue("Id", art.id)
                Comando.Parameters.AddWithValue("Nombre", art.descripcion)

                Comando.Connection = Conexion
                Comando.CommandTimeout = Timeout
                Comando.Prepare()
                Comando.ExecuteNonQuery()

                If Not (SincronizarImagen(art, "Alternancias")) Then Continue For

                art.Sincronizando = True
                art.sincronizadoWeb = True
                LanzarEvento(IndiceGeneral, ((i + 1) * 100) / Artes.Count, String.Format("[{1} de {2}] Sincronizando Arte {0}", art.descripcion, i + 1, Artes.Count))
            Next
        Catch ex As Exception
            Throw ex
        Finally
            Sincronizador.CerrarConexion(Conexion)
        End Try
    End Sub

    ''' <summary>
    ''' Sincroniza las especies con el servidor de trazamare
    ''' </summary>
    Private Sub SincronizarEmpaquetados(ByVal Empaquetados As List(Of DAL.Empaquetado), IndiceGeneral As Integer)
        ' Validaciones
        If Empaquetados Is Nothing OrElse Empaquetados.Count = 0 Then Exit Sub

        Dim Timeout As Integer = DatosConexionBBDD.Timeout
        If Timeout <= 0 Then Timeout = 30

        Dim Sincronizador As New cConnectionManager(Me.CadenaConexion, Timeout)
        Dim Conexion As New MySqlConnection(Me.CadenaConexion)

        Try
            ' Abro la conexión
            If Not Sincronizador.AbrirConexion(Conexion) Then Exit Sub

            ' Ejecuto las consultas
            For i As Integer = 0 To Empaquetados.Count - 1
                Dim emp As DAL.Empaquetado = Empaquetados(i)
                Dim Comando As New MySqlCommand("REPLACE INTO `Empaquetados` (`Id`, `Fecha`) VALUES (@Id, @Fecha);")

                Comando.Parameters.AddWithValue("Id", emp.id)
                Comando.Parameters.AddWithValue("Fecha", emp.fecha)

                Comando.Connection = Conexion
                Comando.CommandTimeout = Timeout
                Comando.Prepare()
                Comando.ExecuteNonQuery()

                emp.Sincronizando = True
                emp.sincronizadoWeb = True
                LanzarEvento(IndiceGeneral, ((i + 1) * 100) / Empaquetados.Count, String.Format("[{1} de {2}] Sincronizando Empaquetado {0}", emp.Codigo, i + 1, Empaquetados.Count))
            Next
        Catch ex As Exception
            ' Throw ex
        Finally
            Sincronizador.CerrarConexion(Conexion)
        End Try
    End Sub

    ''' <summary>
    ''' De momento no se usa Pone el campo sincronizado = 0 para los que se acaban de insertar (Sincronizado = 2)
    ''' </summary>
    Private Sub ActualizarEmpaquetadosASinSincronizar()
        Dim Timeout As Integer = DatosConexionBBDD.Timeout
        If Timeout <= 0 Then Timeout = 30

        Dim Sincronizador As New cConnectionManager(Me.CadenaConexion, Timeout)
        Dim Conexion As New MySqlConnection(Me.CadenaConexion)

        Try
            ' Abro la conexión
            If Not Sincronizador.AbrirConexion(Conexion) Then Exit Sub

            Dim Comando As New MySqlCommand("UPDATE `Empaquetados` SET Sincronizado = 0 WHERE Sincronizado = 2;")

            Comando.Connection = Conexion
            Comando.CommandTimeout = Timeout
            Comando.Prepare()
            Comando.ExecuteNonQuery()
        Catch ex As Exception
            ' Throw ex
        Finally
            Sincronizador.CerrarConexion(Conexion)
        End Try
    End Sub

    Private Function SincronizarImagen(Registo As DAL.IImagen, SubCarpeta As String) As Boolean
        ' Validaciones
        If Not Registo.TieneImagen Then Return True
        If String.IsNullOrEmpty(DatosConexionFTP.Servidor) Then Return True

        Dim RutaCarpeta As String = DatosConexionFTP.RutaCarpeta

        Using Cliente As New FtpClient
            Cliente.Host = Me.DatosConexionFTP.Servidor
            Cliente.Credentials = New Net.NetworkCredential(DatosConexionFTP.Usuario, DatosConexionFTP.Clave)
            Cliente.Port = DatosConexionFTP.Puerto

            Try
                Cliente.Connect()

                ' Miro si existe el directorio
                If Not Cliente.DirectoryExists(RutaCarpeta) Then Cliente.CreateDirectory(RutaCarpeta)
                Cliente.SetWorkingDirectory(RutaCarpeta)

                ' Subcarpeta
                If Not Cliente.DirectoryExists(SubCarpeta) Then Cliente.CreateDirectory(SubCarpeta)
                Cliente.SetWorkingDirectory(SubCarpeta)

                ' Si existe el archivo, lo elimino
                If Cliente.FileExists(Registo.Identificador) Then Cliente.DeleteFile(Registo.Identificador)

                Using ms As New System.IO.MemoryStream
                    Registo.Imagen.Save(ms, System.Drawing.Imaging.ImageFormat.Png)

                    Using ftpStream As System.IO.Stream = Cliente.OpenWrite(Registo.Identificador, FtpDataType.Binary)
                        ftpStream.Write(ms.ToArray(), 0, ms.Length)
                    End Using
                End Using

                Return True
            Catch ex As Exception
                EscribirEnLog(ex.Message + "-" + ex.InnerException.ToString())
                Return False
            Finally
                Cliente.Disconnect()
            End Try
        End Using
    End Function

    Public Sub EscribirEnLog(pMensaje As String)
        Dim strFile As String = Application.StartupPath & "\ErroresLog.txt"

        If Not File.Exists(strFile) Then
            File.Create(strFile)
        End If
        ''Dim fileExists As Boolean = File.Exists(strFile)
        File.AppendAllText(strFile, Date.Now & ": " & pMensaje + Environment.NewLine)
    End Sub



    Public Function ObtenerZonas() As List(Of DAL.ZonaFao)
        ' Validaciones       
        Dim res As New List(Of DAL.ZonaFao)

        Dim Timeout As Integer = DatosConexionBBDD.Timeout
        If Timeout <= 0 Then Timeout = 30
        Dim Sincronizador As New cConnectionManager(Me.CadenaConexion, Timeout)
        'Dim Conexion As New MySqlConnection(Me.CadenaConexion)

        Try

            Dim request As WebRequest = WebRequest.Create(UrlZonas)
            request.Method = "GET"
            request.ContentType = "application/json"
            request.Headers.Add("Authorization", "Bearer TRZMR_opgc123wqcbl0048srtBCG1988qdr4lIamstrs")



            Dim response As Net.HttpWebResponse = request.GetResponse

            Dim datastream As Stream = response.GetResponseStream()
            Dim reader As New StreamReader(datastream)
            Dim jObject As Newtonsoft.Json.Linq.JObject = Newtonsoft.Json.Linq.JObject.Parse(reader.ReadLine())


            For Each z In jObject("data")
                Dim zon As New DAL.ZonaFao

                With zon
                    .idTrazamare = z("id")
                    .descripcion = "FAO " & .idTrazamare & " - " & z("area").ToString
                End With
                res.Add(zon)
            Next



        Catch ex As Exception
            EscribirEnLog(ex.Message + "-" + ex.InnerException.ToString())
            Throw ex
        Finally
            'Sincronizador.CerrarConexion(Conexion)
        End Try
        Return res
    End Function

    Public Function ObtenerSubZonas() As List(Of DAL.SubZona)
        ' Validaciones       
        Dim res As New List(Of DAL.SubZona)

        Dim Timeout As Integer = DatosConexionBBDD.Timeout
        If Timeout <= 0 Then Timeout = 30
        Dim Sincronizador As New cConnectionManager(Me.CadenaConexion, Timeout)
        'Dim Conexion As New MySqlConnection(Me.CadenaConexion)

        Try

            Dim request As WebRequest = WebRequest.Create(UrlSubzonas)
            request.Method = "GET"
            request.ContentType = "application/json"
            request.Headers.Add("Authorization", "Bearer TRZMR_opgc123wqcbl0048srtBCG1988qdr4lIamstrs")



            Dim response As Net.HttpWebResponse = request.GetResponse

            Dim datastream As Stream = response.GetResponseStream()
            Dim reader As New StreamReader(datastream)
            Dim jObject As Newtonsoft.Json.Linq.JObject = Newtonsoft.Json.Linq.JObject.Parse(reader.ReadLine())


            For Each z In jObject("data")
                Dim zon As New DAL.SubZona

                With zon
                    .idTrazamare = z("id")
                    .descripcion = z("subzona")
                End With
                res.Add(zon)
            Next



        Catch ex As Exception
            EscribirEnLog(ex.Message + "-" + ex.InnerException.ToString())
            Throw ex
        Finally
            'Sincronizador.CerrarConexion(Conexion)
        End Try
        Return res
    End Function

#End Region
#Region "TIMER"
    ''' <summary>
    ''' Iniciamos el timer que controla la actualizacion de registros
    ''' </summary>
    Public Sub IniciarTimer()
        If Timer IsNot Nothing Then
            Timer.Stop()
            Timer = Nothing
        End If
        Timer = New Timers.Timer(intervalo * 60 * 1000)
        Timer.AutoReset = True
        Timer.Start()
    End Sub
    ''' <summary>
    ''' Actualizamos la BD con los registros telemáticos
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub TimerA0(sender As Object, e As Timers.ElapsedEventArgs) Handles Timer.Elapsed

        Sincronizar(Nothing, Nothing)
        IniciarTimer()
    End Sub
#End Region
#Region " CONTROL DEL TIMER "
    Public Sub Sincronizar(sender As Object, e As Timers.ElapsedEventArgs)
        If _Ejecutando Then Exit Sub

        Try
            Dim Contexto As DAL.Entidades = DAL.cDAL.Instancia.getContext()
            Dim Configuracion As Configuracion = (From cnf As Configuracion In Contexto.Configuraciones Select cnf).FirstOrDefault

            ' Validaciones
            If Configuracion Is Nothing Then Exit Sub

            Dim DatosTrazamare = New qSinc.Sincronizador.ServidorTrazamare
            With DatosTrazamare
                .Servidor = Configuracion.TrazamareFTPServidor
                .Ruta = Configuracion.TrazamareFTPRuta
                .Clave = Configuracion.TrazamareFTPClave
            End With

            qSinc.Sincronizador.Instancia.DatosConexionTrazamare = DatosTrazamare

            Dim Etiquetas As List(Of Etiqueta) = Nothing
            ''si pSincronizarSoloPaquetes se cogen etiquetas de los paquetes no las no sincronizadas                    

            Etiquetas = (From etq As Etiqueta In Contexto.Etiquetas Where Not etq.sincronizadoWeb AndAlso etq.fechaBaja Is Nothing Select etq).ToList


            'Create a generic Objects Array
            Dim param_obj(2) As Object
            param_obj(0) = Etiquetas
            param_obj(1) = Contexto
            param_obj(2) = Configuracion.CodigoTrazamare

            Dim worker_thread As New Thread(Sub() auxOns(param_obj))
            worker_thread.Start()

            Exit Sub
        Catch ex As Exception
            MessageBox.Show(ex.Message + ex.InnerException.ToString())
            Exit Sub
        End Try
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="params"></param>
    ''' <param name="pSincronizarEtiquetasPaquetes"></param>
    Private Sub auxOns(params As Object, Optional ByVal pSincronizarEtiquetasPaquetes As Boolean = False)

        qSinc.Sincronizador.Instancia.SincronizarOns(params(2), params(0)) ', params(1), params(2), params(3), params(4), pSincronizarEtiquetasPaquetes)

        Dim Contexto As Entidades = params(1)
        Contexto.SaveChanges()
    End Sub

#End Region
End Class
