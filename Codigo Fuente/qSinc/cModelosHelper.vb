﻿
Imports DAL

Module cModelosHelper
    Public Class EtiquetaTrazamare
        Public Property arte_pesca_id As String
        Public Property especie_id As String
        Public Property fecha As String
        Public Property consumo_preferente As String
        Public Property metodo_produccion_id As String
        Public Property presentacion_id As String
        Public Property peso_neto As String
        Public Property lote As String
        Public Property zona_fao As String
        Public Property zona_gsa As String
        Public Property zona_fao_id As String
        Public Property zona_gsa_id As String
        'Public Property num_etiquetas As Int32
        Public Property etiqueta_id As String()

        Public Sub New(etiq As Etiqueta)
            arte_pesca_id = "3"
            especie_id = "3"
            fecha = etiq.fecha.ToString("dd-MM-yyyy")
            consumo_preferente = etiq.fecha.AddMonths(6).ToString("dd-MM-yyyy")
            metodo_produccion_id = "3"
            presentacion_id = "6"
            peso_neto = etiq.cantidad.ToString().Replace(",", ".")
            zona_fao = etiq.NombreZona
            zona_gsa = etiq.NombreSubzona
            zona_fao_id = etiq.IdZonaTrazamare
            zona_gsa_id = etiq.IdSubzonaTrazamare
            lote = etiq.LoteSalida
            'num_etiquetas = 1
            etiqueta_id = New String() {etiq.Codigo}
        End Sub
        Public Function GetCadenaPOst() As String
            Dim res = String.Empty
            res &= "arte_pesca_id=" & arte_pesca_id
            res &= "&" & "especie_id=" & especie_id
            res &= "&" & "fecha=" & fecha
            res &= "&" & "consumo_preferente=" & consumo_preferente
            res &= "&" & "metodo_produccion_id=" & metodo_produccion_id
            res &= "&" & "presentacion_id=" & presentacion_id
            res &= "&" & "peso_neto=" & peso_neto
            res &= "&" & "zona_fao=" & zona_fao
            res &= "&" & "zona_gsa=" & zona_gsa
            res &= "&" & "zona_fao_id=" & zona_fao_id
            res &= "&" & "zona_gsa_id=" & zona_gsa_id
            res &= "&" & "lote=" & lote
            'Dim dict As New Dictionary(Of String, String)
            'For i = 0 To etiqueta_id.Length - 1
            '    dict.Add(i, etiqueta_id(i))
            'Next
            res &= "&" & "etiqueta_id[]=" & etiqueta_id(0)
            Return res
        End Function

    End Class

    Public Class RespuestaTrazamare
        Public Property result As String
        Public Property mensaje As String
        Public Property etiqueta As EtiquetaTrazamareResponse
    End Class

    Public Class EtiquetaTrazamareResponse
        Public Property arte_pesca As artePescaResponse
        Public Property especie As especieResponse
        Public Property fecha As String
        Public Property consumo_preferente As String
        Public Property metodo_produccion As metodoResponse
        Public Property presentacion As presentacionResponse
        Public Property peso_neto As String
        Public Property lote As String
        Public Property zona_fao As zonaFaoResponse
        Public Property zona_gsa As zonaGsaResponse

        'Public Property num_etiquetas As Int32
        Public Property etiqueta_items As etiquetaResponse()
    End Class

    Public Class artePescaResponse
        Public Property nombre As String
    End Class
    Public Class especieResponse
        Public Property nombre As String
    End Class
    Public Class metodoResponse
        Public Property nombre As String
    End Class
    Public Class presentacionResponse
        Public Property nombre As String
    End Class
    Public Class zonaFaoResponse
        Public Property nombre As String
    End Class
    Public Class zonaGsaResponse
        Public Property nombre As String
    End Class
    Public Class etiquetaResponse
        Public Property codigo As String
    End Class
End Module

