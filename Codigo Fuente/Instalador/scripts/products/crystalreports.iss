// Instala el runtime de crystal reports

[CustomMessages]
crystalReports_title=Crystal Reports Runtime (x86)
crystalReports_size=75 MB

[Code]	
const
	crystalReports_url = 'http://update.quadralia.com/componentes/CRRuntime_32bit_13_0_13.msi';
	CrystalReportsKey = 'SOFTWARE\SAP BusinessObjects\Crystal Reports for .NET Framework 4.0\Crystal Reports';

procedure crystalreports();
begin
	if ((IsWin64 and (not RegKeyExists(HKLM64, CrystalReportsKey) or not RegKeyExists(HKLM32,CrystalReportsKey))) or (not IsWin64 and not RegKeyExists(HKLM, CrystalReportsKey))) then begin
			AddProduct('CRRuntime_32bit_13_0_13.msi',
			'/q',
			CustomMessage('crystalReports_title'),
			CustomMessage('crystalReports_size'),
			crystalReports_url, false, false);
	end;
end;
