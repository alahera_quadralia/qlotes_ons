<?php

/**
 * Clase para trabajar con base de datos
 */
class BaseDatos {

    /**
     * Obtiene un cliente de conexion con base de datos valido
     * @return mysqli
     */
    protected static function getConnection() {

        $Connection = new mysqli(Conf::DB_HOST, Conf::DB_USER, Conf::DB_PASS, Conf::DB_DATABASE);

        if ($Connection->connect_errno) {
            printf("No pudo conectarse al servidor mysql: %s\n", $Connection->connect_error);
            die();
            return null;
        } else {
            return $Connection;
        }
    }

    /**
     * Realiza una consulta en la base de datos
     * @param mysqli $Connection
     * @return mysqli_stmt
     */
    private static function run($query, &$Connection, &$Params = null) {
        // Si no tengo cadena de conexión, no hago nada
        $query = trim($query);
        if (empty($query)) {
            return null;
        }

        if ($Connection == null)
            die("No hay conexión con el servidor de base de datos");

        // Realizamos la consulta
        $Comando = BaseDatos::GetCommand($query, $Connection, $Params);
        if ($Comando->execute() == FALSE && Conf::APP_DEBUG) {
            printf("Error en la consulta: \n  Texto consulta: %s\n  ErrorDevuelto: %s\n", $query, $Comando->error);
            printf("Parametros: %s\n", print_r($Params));
            return;
        }

        return $Comando;
    }

    public static function runQuery($query, &$Params = null) {
        $Resultado = array();

        $Connection = BaseDatos::getConnection();

        $Comando = BaseDatos::run($query, $Connection, $Params);

        if (isset($Comando) && !is_null($Comando)) {
            // Obtengo los resultados y los devuelvo
            while ($fila = BaseDatos::fetchArray($Comando)) {
                $Resultado[] = $fila;
            }
        }
        $Comando->close();
        $Connection->close();

        // Devuelvo los datos
        return $Resultado;
    }

    /**
     * Ejecuta una consulta de la que no se espera resultado
     * @param string $query Consulta a ejecutar
     * @return int Numero de filas afectadas
     */
    public static function runQueryNoResults($query, &$Params = null) {
        $Connection = BaseDatos::getConnection();
        $Comando = BaseDatos::run($query, $Connection, $Params);
        $Resultado = $Comando->affected_rows;
        $Comando->close();
        $Connection->close();
    }

    /**
     * Ejecuta una consulta de la que se desea obtener el último id insertado
     * @param string $query Consulta a ejecutar
     * @return int Último Id Insertado
     */
    public static function runQueryInsertedId($query, &$Params = null) {
        $Connection = BaseDatos::getConnection();
        $Comando = BaseDatos::run($query, $Connection, $Params);
        $Resultado = $Comando->insert_id;
        $Comando->close();
        $Connection->close();

        return $Resultado;
    }

    /**
     * Cambia aquellos parámetros que sean nulos por NULL en la sentencia SQL
     * @param string $Query Consulta SQL
     * @param ArrayObject $Params Listado de parámetros de la consulta
     */
    private static function parseNullParameters(&$Query, &$Params) {
        // Compruebo parámetros
        // Si no hay parámetros, no hago nada
        if (!is_array($Params) || !count($Params))
            return;

        $PosicionParametro = 0;
        foreach ($Params as $clave => $Parametro) {

            // Si no tenemos más parametros, no tengo que hacer nada
            if (($PosicionParametro = strpos($Query, '?', $PosicionParametro + 1)) === false)
                break;

            // Parseo solamente los parámetros que son null
            if (!is_null($Parametro))
                continue;
            else
                $Query = substr_replace($Query, 'NULL', $PosicionParametro, 1);
            unset($Params[$clave]);
        }
    }

    private static function getParametersTypes(&$Params) {
        $Resultado = '';

        // Si no hay parámetros, no hago nada
        if (!is_array($Params) || !count($Params))
            return $Resultado;

        // Recorro los parámetros viendo de qué tipo son
        foreach ($Params as $Parametro) {
            if (is_int($Parametro)) {
                $Resultado .= 'i';
            } else if (is_double($Parametro)) {
                $Resultado .= 'd';
            } else {
                $Resultado .= 's';
            }
        }

        return $Resultado;
    }

    /**
     * Asigna los parámetros
     * @param ArrayObject $Params
     * @param mysqli_stmt $stmt 
     */
    private static function AssignParameters(&$Params, &$stmt) {
        if ($Params == null || empty($Params) || !is_array($Params))
            return;
        array_unshift($Params, BaseDatos::getParametersTypes($Params));
        array_unshift($Params, $stmt);
        call_user_func_array('mysqli_stmt_bind_param', $Params);
    }

    /**
     * Obtiene un comando para ejecutar en la base de datos
     * @param string $Query Consulta SQL a ejecutar en el comando
     * @param mysqli $Connection Conexión con la base de datos
     * @param ArrayObject $Params Parámetros de la consulta SQL
     * @return mysqli_stmt Comando con los parámetros establecidos 
     */
    public static function GetCommand(&$Query, &$Connection, &$Params) {
        // Compruebo Parámetros
        $Query = trim($Query);
        if (empty($Query) || !isset($Connection) || empty($Connection))
            return null;

        // Corrijo la cadena SQL
        BaseDatos::parseNullParameters($Query, $Params);

        /* @var $Resultado mysqli_stmt */
        /* $Resultado; */

        $Resultado = $Connection->stmt_init();
        /* echo ("<hr>"); */
        if ($Resultado->prepare($Query)) {
            BaseDatos::AssignParameters($Params, $Resultado);
        } else {
            if (Conf::APP_DEBUG)
                die($Resultado->error);

            $Resultado = null;
        }

        return $Resultado;
    }

    /**
     * Obtiene los resultados de una consulta y los pone en un array asociativo
     * @param mysqli_stmt $Comando Comando del que deseamos obtener los datos
     * @return ArrayObject Array con los resultados de la consulta
     */
    private static function fetchArray(&$Comando) {
        $Comando->store_result();
        $data = $Comando->result_metadata();
        $fields = array();
        $out = array();

        $fields[] = &$Comando;

        while ($field = mysqli_fetch_field($data)) {
            $fields[] = &$out[$field->name];
        }

        call_user_func_array('mysqli_stmt_bind_result', $fields);
        $temp = $Comando->fetch();

        return ($temp) ? $out : false;
    }

}

/**
 * Clase para trabajar con objeto y bases de datos
 */
class ObjetosBaseDatos extends BaseDatos {

    /**
     * Obtiene los campos de la base de datos
     * @param type $Table Nombre de la tabla de la que deseamos obtener los campos
     */
    private static function getTableFields($Table) {
        $Consulta = 'SHOW COLUMNS FROM ' . $Table;
        return BaseDatos::runQuery($Consulta);
    }

    /**
     * Inserta un objeto en la base de datos y establece su id en la instancia
     * @param string $Item objeto que queremos insertar en la base de datos
     * @param string $TableName = Nombre de la tabla a la que pertenece el objeto
     */
    public static function insertObject($Item, $TableName) {
// Valido los parámetro de entrada
        $TableName = trim($TableName);
        if (empty($TableName) || empty($Item)) {
            if (Conf::APP_DEBUG) {
                echo("Llamada no válida a la función. \n  Variable Item: " . var_dump($Item) . "\n  Variable TableName: " . $TableName);
            }
            return;
        }

// Obtengo los campos de la base de datos y del objeto
        $CamposBD = ObjetosBaseDatos::getTableFields($TableName);
        $CamposClase = get_object_vars($Item);
        $Consulta = 'INSERT INTO ' . $TableName . '(';

// Recorro los campos de la base de datos y miro si la clase los implementa
        foreach ($CamposBD as $Campo) {
// Miro si existe el campo en la clase
            if (!array_key_exists($Campo["Field"], $CamposClase))
                continue;

// Miro si se trata de un valor autoincremental
            if (strpos($Campo["Extra"], 'auto_increment') !== FALSE)
                continue;

// Construyo la cadena 
            $Consulta .= $Campo["Field"] . ', ';
            $Valores[] = $CamposClase[$Campo["Field"]];
        }
// Elimino la última coma y termino la consulta
        if (ObjetosBaseDatos::stringEndsWith($Consulta, ', '))
            $Consulta = substr($Consulta, 0, -2);

        $Consulta .= ') VALUES (';

// Tengo que crear la conexión para que funciones mysqli_real_escape_string
        $Conexion = BaseDatos::getConnection();

// Meto los valores
        foreach ($Valores as $Valor) {
// Miro si tiene un valor establecido, si no, lo pongo en null
            if (!isset($Valor) || empty($Valor))
                $Consulta.= "NULL, ";
            else
                $Consulta.= "'" . $Conexion->real_escape_string($Valor) . "', ";
        }

// Elimino la última coma y termino la consulta
        if (ObjetosBaseDatos::stringEndsWith($Consulta, ', '))
            $Consulta = substr($Consulta, 0, -2);
        $Consulta .= ');';

// Ejecuto la consulta y escribo el id
        if ($Conexion->query($Consulta)) {
            foreach ($CamposBD as $Campo) {
                if ($Campo["Key"] == 'PRI') { //¿Clave primaria?
                    if (array_key_exists($Campo["Field"], $CamposClase)) { // ¿Existe el campo en la clase?
                        eval('$Item->' . $Campo["Field"] . '= ' . $Conexion->insert_id . ";");
                    }
                }
            }
        } else {
            printf("Error en la consulta: \n  Texto consulta: %s\n  ErrorDevuelto: %s\n", $Consulta, $Conexion->error);
            return;
        }

        // Cierro la conexion
        $Conexion->close();
        var_dump($Item);
    }

    private static function stringEndsWith($whole, $end) {
        return (strpos($whole, $end, strlen($whole) - strlen($end)) !== false);
    }

}

?>