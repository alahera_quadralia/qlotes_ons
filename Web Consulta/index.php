<?php
/**
 * Transforma cualquier objeto en codificación UTF-8
 * @param object $in
 */
 function to_utf8($in) {
    if (is_array($in)) {
        foreach ($in as $key => $value) {
            $out[to_utf8($key)] = to_utf8($value);
        }
    } elseif (is_string($in)) {
        if (mb_detect_encoding($in, "auto", true) != "UTF-8")
            return utf8_encode($in);
        else
            return $in;
    } else {
        return $in;
    }
    return $out;
}

if (!isset($_REQUEST["id"])) {
    MostarPaginaInicial();
    die();
}

$etiqueta = $_REQUEST["id"];
$etiquetaSolicitada = substr($etiqueta, -8);  /* Extraemos el "id" de la etiqueta */

if (!is_numeric($etiquetaSolicitada)) { /* Si no es numerico error */
    MostarPaginaInicial("El código introducido no tiene el formato correcto");
    die();
}

$etiquetaSolicitada = intval($etiquetaSolicitada);
$checketiqueta = strtolower(substr($etiqueta, 0, 2));

if ($checketiqueta != 'bd') {
    MostarPaginaInicial("El código introducido no tiene el formato correcto");
    die();
}

$basededatos = strtolower(substr($etiqueta, 0, 2)); /* Se usan los dos caracteres iniciales del código de etiqueta para identificar el archivo de configuración de BBDD */
if (file_exists("config/conf$basededatos.php")) {
    require_once "config/conf$basededatos.php";
} else { /* Si no existe el  el archivo de configuración */
    MostarPaginaInicial("No se han podido obtener la configuración del lote solicitado");
    die();
}

require_once 'dbManager.php';

$params = array(&$etiquetaSolicitada);

$consulta = "SELECT * FROM Empaquetados WHERE Empaquetados.Id = ?";
$resultado = BaseDatos::runQuery($consulta, $params);

if ($resultado == NULL) {
    MostarPaginaInicial("No se ha localizado ningún lote con esa numeración");
    die();
}

// Obtengo las etiquetas que tiene ese lote
$consulta = " SELECT 
        etiqueta.id AS Id, 
        etiqueta.dataValidacion AS fecha,
        etiqueta.Validada AS validada,
        etiqueta.Peso AS Peso,
        especies.Nome as nombreespecie
    FROM etiqueta
        LEFT JOIN especies ON etiqueta.IDEspecie = especies.Id
    WHERE etiqueta.IdEmpaquetado = ?";
$params = array(&$etiquetaSolicitada);

try {
    $resultadoConsulta = BaseDatos::runQuery($consulta, $params);

    MostarPaginaLotes($resultado[0]["Fecha"], $etiqueta, $resultadoConsulta);
} catch (Exception $e) {
    MostarPaginaInicial($e->getMessage());
    die();
}

function MostarPaginaLotes($FechaEmpaquetado, $Codigo, $Etiquetas) {
    ?>
    <!doctype html>
    <!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
    <!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
    <!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
    <!--[if gt IE 8]><!--> 
    <html class="no-js" lang=""> <!--<![endif]-->
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
            <title></title>
            <meta name="description" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">

            <link rel="stylesheet" href="css/bootstrap.min.css">
            <link rel="stylesheet" href="css/bootstrap-theme.min.css">
            <link rel="stylesheet" href="css/maindesglose.css">

            <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        </head>
        <body class="bodyDesglose">
            <div class="container outer-section">
                <div id="print-area">
                    <div class="row ">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <h2>Información Lote</h2>
                            <h4>Lote:  <strong><?php echo strtoupper($Codigo); ?></strong></h4>
                            <h4>Fecha: <strong><?php echo date("d-m-Y", strtotime($FechaEmpaquetado)); ?></strong></h4>
                        </div>
                    </div>
                    <hr>
                    <br>
                    <?php if ((is_array($Etiquetas)) && (count($Etiquetas) > 0)) { ?>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Etiqueta</th>
                                                <th>Fecha</th>
                                                <th>Especie</th>
                                                <th>Peso</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $PesoTotal = 0;
                                            foreach ($Etiquetas as $Etiqueta) {
                                                echo"<tr>
                                                    <td>" .$Etiqueta["Id"] . "</td>
                                                    <td>" . date("d-m-Y", strtotime($Etiqueta["fecha"])) . "</td>
                                                    <td>" . to_utf8($Etiqueta["nombreespecie"]) . "</td>
                                                    <td>" . $Etiqueta["Peso"] . "</td>
                                                </tr>
                                                <tr>";
                                                $PesoTotal += $Etiqueta["Peso"];
                                            }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-9 col-md-9 col-sm-9" style="text-align: right; padding-right: 30px;">
                                Número de Etiquetas:
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <strong><?php echo count($Etiquetas); ?></strong>
                            </div>
                            <hr>
                            <div class="col-lg-9 col-md-9 col-sm-9" style="text-align: right; padding-right: 30px;">
                                Peso Total:
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <strong><?php echo $PesoTotal; ?></strong>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="alert alert-danger" role="alert">El lote solicitado no contiene etiquetas</div>
                    <?php } ?>
                    <hr>
                    <div class="row pad-bottom">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <a class="btn btn-primary " href="/index.php">Consultar otro lote</a>
                        </div>
                    </div>
                </div>




                <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
                <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

                <script src="js/vendor/bootstrap.min.js"></script>

                <script src="js/main.js"></script>
        </body>
    </html>
    <?php
}

function MostarPaginaInicial($Error = '') {
    ?>
    <!doctype html>
    <!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
    <!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
    <!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
    <!--[if gt IE 8]><!--> 
    <html class="no-js" lang=""> <!--<![endif]-->
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
            <title></title>
            <meta name="description" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">

            <link rel="stylesheet" href="css/bootstrap.min.css">
            <link rel="stylesheet" href="css/main.css">

            <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        </head>
        <body>
            <!--[if lt IE 8]>
                <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
            <![endif]-->    

            <div class="site-wrapper">

                <div class="site-wrapper-inner">

                    <div class="cover-container">
                        <?php
                        if ($Error != '')
                            echo '<div class="alert alert-danger" role="alert"> <strong>Error</strong> ' . $Error . '</div>';
                        ?>
                        <div class="inner cover">
                            <h1 class="cover-heading">Consulta de Lotes</h1>
                            <p class="lead">Introduzca el código del lote que desea consultar.</p>
                            <form class="form-inline">
                                <div class="form-group-lg">
                                    <input type="text" class="form-control" id="txtEtiquetas" name="id" placeholder="Etiqueta" />
                                    <button type="submit" class="btn btn-lg btn-default">Consultar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
            <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

            <script src="js/vendor/bootstrap.min.js"></script>

            <script src="js/main.js"></script>
        </body>
    </html>
    <?php
}
?>